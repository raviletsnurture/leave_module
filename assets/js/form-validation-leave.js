var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#leave_start_date';
var ele1 = '#leave_end_date';

var today = new Date();
var lastDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);


var checkin = $(ele).fdatepicker({
    autoclose: true,
    //daysOfWeekDisabled: [0, 6],
    onRender: function(date) {
        //return date.valueOf() < now.valueOf() ? 'disabled' : '';

    }
}).on('changeDate', function(ev) {
    //if (ev.date.valueOf() > checkout.date.valueOf())
    //{
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate());
    checkout.update(newDate);
    //}
    checkin.hide();
    $('#leave_end_date')[0].focus();
    //$('#leave_start_date').datepicker('hide');

}).data('datepicker');

var checkout = $(ele1).fdatepicker({
    //daysOfWeekDisabled: [0, 6],
    onRender: function(date) {
        //return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
}).on('changeDate', function(ev) {
    checkout.hide();
}).data('datepicker');

$(document).ready(function() {
    // validate signup form on keyup and submit
    $("#addLeave").validate({
        rules: {
            user_id: "required",
			//department_id: "required",
            leave_type: "required",
            leave_end_date: "required",
            leave_start_date: "required",
            leave_status: "required",
            leave_reason:{
                  required: true,
                  minlength: 100,
                  maxlength: 200
                },
            leave_critical_task: { // <- NAME attribute of the input
                required: {
                    depends: function () {
                        return ($("#leave_type").val() != "14");
                    }
                }
            },
            leave_res_assignee: { // <- NAME attribute of the input
                required: {
                    depends: function () {
                        return ($("#leave_type").val() != "14");
                    }
                }
            },
            //leave_critical_task: "required",
            //leave_res_assignee: "required",
        },
        messages: {
            user_id: "Please select user",
			//department_id: "Please select department",
            leave_type: "Please select leave type",
            leave_end_date: "Please select end date",
            leave_start_date: "Please select start date",
            leave_status: "Please select leave status",
            leave_reason: {
                              required: "Please enter reason for leave",
                              minlength: "Please enter at least 100 letters for leave reason",
                              maxlength: "Please enter less than 200 letters for leave reason"
                          },
            leave_critical_task: "Please enter critical task name",
            leave_res_assignee: "Please select responsible",
        }
    });

});
