function goto(url) {
    window.location = url;

}

function goBack(num) {
    window.history.go(-num);
}
function removeParameterUrl(url, parameter)
{
	var urlparts= url.split('?');

	if (urlparts.length>=2){
		var urlBase=urlparts.shift(); //get first part, and remove from array
		var queryString=urlparts.join("?"); //join it back up

		var prefix = encodeURIComponent(parameter)+'=';
		var pars = queryString.split(/[&;]/g);
		for (var i= pars.length; i-->0;)               //reverse iteration as may be destructive
			if (pars[i].lastIndexOf(prefix, 0)!==-1)   //idiom for string.startsWith
				pars.splice(i, 1);
		url = urlBase+'?'+pars.join('&');
	}
	return url;
}

function changePerPage(val){
	var newUrl = removeParameterUrl(window.location.href,'perpage');
	var queryStr = window.location.search;
	var pageUrl = '';

	if(queryStr == ''){
		pageUrl = '?perpage=' + val;
	}else if(queryStr != ''){
		pageUrl = '&perpage=' + val;
	}
	window.location = newUrl+pageUrl;
	return false;
}
$( document ).ready(function() {
	$("#searchPrice").click(function(){
		var newUrl = removeParameterUrl(window.location.href,'pricefrom');
		var newUrl = removeParameterUrl(newUrl,'priceto');
		var newUrl = newUrl.replace('productdetail','');
		var newUrl = newUrl.replace('#','');
		var queryStr = window.location.search;
		var priceUrl = '';
		var priceto = $('#priceto').val();
		var pricefrom = $("#pricefrom").val();
		if(queryStr == ''){
			priceUrl = '?pricefrom=' + pricefrom + '&priceto=' + priceto;
		}else if(queryStr != ''){
			priceUrl = '&pricefrom=' + pricefrom + '&priceto=' + priceto;
		}
		//alert(newUrl + priceUrl);
		window.location = newUrl + priceUrl;
		return false;
	});
	$(".subImage").click(function() {
		var img = $("img",this).attr('src');
		$("#mainImageId").attr('src',img);
	});
	$(".deleteRec").click(function() {
		var del = confirm("Are you sure delete this records?");
		if(del == false){
			return false;
		}
	});

    //Set footer on bottom
    var height = $(window).innerHeight() - $('.site-footer').innerHeight();
    $('#main-content').css("min-height",height);

});
