var Sliders = function () {

   // // default sliders
//    $("#default-slider").slider();
//
//    // snap inc
//    $("#snap-inc-slider").slider({
//        value: 50,
//        min: 0,
//        max: 1000,
//        step: 100,
//        slide: function (event, ui) {
//            $("#snap-inc-slider-amount").text("$" + ui.value);
//        }
//    });
//
//    $("#snap-inc-slider-amount").text("$" + $("#snap-inc-slider").slider("value"));

    // range slider for price
    $("#slider-range").slider({
        range: true,
        min: pricefrom,
        max: priceto,
        values: [pricefromVal, pricetoVal],
        slide: function (event, ui) {
			$("#pricefrom").val(ui.values[0]);
			$("#priceto").val(ui.values[1]);
            $("#slider-range-amount").text("AED " + ui.values[0] + " - AED " + ui.values[1]);
        }
    });
	$("#pricefrom").val($("#slider-range").slider("values", 0));
	$("#priceto").val($("#slider-range").slider("values", 1));
    $("#slider-range-amount").text("AED " + $("#slider-range").slider("values", 0) + " - AED " + $("#slider-range").slider("values", 1));

    //range max

    //$("#slider-range-max").slider({
//        range: "max",
//        min: 1,
//        max: 10,
//        value: 2,
//        slide: function (event, ui) {
//            $("#slider-range-max-amount").text(ui.value);
//        }
//    });
//
//    $("#slider-range-max-amount").text($("#slider-range-max").slider("value"));

    // range min
   // $("#slider-range-min").slider({
//        range: "min",
//        value: 37,
//        min: 1,
//        max: 700,
//        slide: function (event, ui) {
//            $("#slider-range-min-amount").text("$" + ui.value);
//        }
//    });
//
//    $("#slider-range-min-amount").text("$" + $("#slider-range-min").slider("value"));

    //
    // setup graphic EQ
    //$( "#eq > span" ).each(function() {
//    // read initial values from markup and remove that
//        var value = parseInt( $( this ).text(), 10 );
//        $( this ).empty().slider({
//            value: value,
//            range: "min",
//            animate: true,
//            orientation: "vertical"
//        });
//    });

    // bound to select

  //  var select = $( "#minbeds" );
//    var slider = $( "<div id='slider'></div>" ).insertAfter( select ).slider({
//        min: 1,
//        max: 6,
//        range: "min",
//        value: select[ 0 ].selectedIndex + 1,
//        slide: function( event, ui ) {
//            select[ 0 ].selectedIndex = ui.value - 1;
//        }
//    });
//    $( "#minbeds" ).change(function() {
//        slider.slider( "value", this.selectedIndex + 1 );
//    });

    // vertical slider
    //$("#slider-vertical").slider({
//        orientation: "vertical",
//        range: "min",
//        min: 0,
//        max: 100,
//        value: 60,
//        slide: function (event, ui) {
//            $("#slider-vertical-amount").text(ui.value);
//        }
//    });
//    $("#slider-vertical-amount").text($("#slider-vertical").slider("value"));
//
//    // vertical range sliders
//    $("#slider-range-vertical").slider({
//        orientation: "vertical",
//        range: true,
//        values: [17, 67],
//        slide: function (event, ui) {
//            $("#slider-range-vertical-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
//        }
//    });
//
//    $("#slider-range-vertical-amount").text("$" + $("#slider-range-vertical").slider("values", 0) + " - $" + $("#slider-range-vertical").slider("values", 1));


}();