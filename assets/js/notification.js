
// [START get_messaging_object]
// Retrieve Firebase Messaging object.

// var config = {
//     apiKey: "AIzaSyBbkpFtkpZBsX7nA-owaVnL1nq0FUbt2vc",
//     authDomain: "api-project-393067052124.firebaseapp.com",
//     databaseURL: "https://api-project-393067052124.firebaseio.com",
//     storageBucket: "api-project-393067052124.appspot.com",
//     messagingSenderId: "393067052124"
// };

var config = {
  apiKey: "AIzaSyDYjZUSXERI-tKa17Li1wXlIPgXJqpF5pg",
  authDomain: "hrms-65381.firebaseapp.com",
  databaseURL: "https://hrms-65381.firebaseio.com",
  projectId: "hrms-65381",
  storageBucket: "hrms-65381.appspot.com",
  messagingSenderId: "6373806937",
  appId: "1:6373806937:web:6e549ddf7be7438b"
};

firebase.initializeApp(config);

//useServiceworker('notification.js');

const messaging = firebase.messaging();
// [END get_messaging_object]
//messaging.useServiceWorker('sdfasd.js');

messaging.requestPermission()
.then(function() {
  //console.log('Notification permission granted.');
  messaging.getToken()
  .then(function(refreshedToken) {
    //console.log('Token refreshed.');
    // Indicate that the new Instance ID token has not yet been sent to the
    // app server.
    setTokenSentToServer(false);
    // Send Instance ID token to app server.
    sendTokenToServer(refreshedToken);
    // [START_EXCLUDE]
    // Display new Instance ID token and clear UI of all previous messages.

    // [END_EXCLUDE]
  })
  .catch(function(err) {
    //console.log('Unable to retrieve refreshed token ', err);

  });
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  // ...
})
.catch(function(err) {
  //console.log('Unable to get permission to notify.', err);
});


// [START refresh_token]
// Callback fired if Instance ID token is updated.


function setTokenSentToServer(sent) {
  if (sent) {
    window.localStorage.setItem('sentToServer', 1);
  } else {
    window.localStorage.setItem('sentToServer', 0);
  }
}

function isTokenSentToServer() {
  if (window.localStorage.getItem('sentToServer') == 1) {
        return true;
  }
  return false;
}

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
  if (!isTokenSentToServer()) {
    //console.log('Sending token to server...');
    // TODO(developer): Send the current token to your server.
    //console.log("currentToken: " + currentToken);
    setTokenSentToServer(true);
    $.post("dashboard/deviceToken",
    {
        deviceToken: currentToken,
        _token : $('input[name=_token]').val()
    },
    function(data, status){
        //console.log("Data: " + data + "\nStatus: " + status);
    });
  } else {
    //console.log('Token already sent to server so won\'t send it again ' +  'unless it changes');
  }

}


messaging.onTokenRefresh(function() {
  messaging.getToken()
  .then(function(refreshedToken) {
    console.log('Token refreshed.');
    // Indicate that the new Instance ID token has not yet been sent to the
    // app server.
    setTokenSentToServer(false);
    // Send Instance ID token to app server.
    sendTokenToServer(refreshedToken);
    // [START_EXCLUDE]
    // Display new Instance ID token and clear UI of all previous messages.
    //resetUI();
    // [END_EXCLUDE]
  })
  .catch(function(err) {
    console.log('Unable to retrieve refreshed token ', err);

  });
});
// [END refresh_token]

messaging.onMessage(function(payload) {
  console.log("Message received. ", JSON.stringify(payload));
  // [START_EXCLUDE]
  // Update the UI to include the received message.

  // [END_EXCLUDE]
});
// [END receive_message]

function resetUI() {


  // [START get_token]
  // Get Instance ID token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging.getToken()
  .then(function(currentToken) {
    if (currentToken) {
      sendTokenToServer(currentToken);

    } else {
      // Show permission request.
      console.log('No Instance ID token available. Request permission to generate one.');
      // Show permission UI.
      setTokenSentToServer(false);
    }
  })
  .catch(function(err) {
    console.log('An error occurred while retrieving token. ', err);

    setTokenSentToServer(false);
  });
}
// [END get_token]

function requestPermission() {
  console.log('Requesting permission...');
  // [START request_permission]
  messaging.requestPermission()
  .then(function() {
    console.log('Notification permission granted.');
    // TODO(developer): Retrieve an Instance ID token for use with FCM.
    // [START_EXCLUDE]
    // In many cases once an app has been granted notification permission, it
    // should update its UI reflecting this.
    resetUI();
    // [END_EXCLUDE]
  })
  .catch(function(err) {
    console.log('Unable to get permission to notify.', err);
  });
  // [END request_permission]
}

function deleteToken() {
  // Delete Instance ID token.
  // [START delete_token]
  messaging.getToken()
  .then(function(currentToken) {
    messaging.deleteToken(currentToken)
    .then(function() {
      console.log('Token deleted.');
      setTokenSentToServer(false);
      // [START_EXCLUDE]
      // Once token is deleted update UI.
      resetUI();
      // [END_EXCLUDE]
    })
    .catch(function(err) {
      console.log('Unable to delete token. ', err);
    });
    // [END delete_token]
  })
  .catch(function(err) {
    console.log('Error retrieving Instance ID token. ', err);

  });

}
