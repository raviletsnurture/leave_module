$(document).ready(function() {
        // validate login form on keyup and submit
        $("#loginForm").validate({
            rules: {
				uUserId: {
                    required: true,                    
					remote: {
					  url: "login/checkUsername",
					  type: "post",
					  data: {
						username: function() {
						  return $( "#uUserId" ).val();
						}
					  }
					}
                },
                password: {
                    required: true,
                },
            },
            messages: {               
                uUserId: {
                    required: "Please enter a username",
					remote: "Username does not exist"
                },
                password: {
                    required: "Please provide a password",
                },
            }
        });
		 $("#forgotForm").validate({
            rules: {
				userName: {
                    required: true,                    
					remote: {
					  url: baseUrl + "shop/login/checkUsername",
					  type: "post",
					  data: {
						username: function() {
						  return $( "#userName" ).val();
						}
					  }
					}
                },
            },
            messages: {               
                userName: {
                    required: "Please enter a username",
					remote: "Username does not exist"
                },
                
            },
			submitHandler: function(form) {
				msgHide();
				var Url = baseUrl+"shop/login/forgotpassword";
				$.ajax({
				  type: 'post',
				  url: Url,  
				  cache: false,  
				  data: "username=" + $( "#userName" ).val(),     
				  success: function(data){
					//alert(data);
					if(data == 'true'){
						msgHide();
						$(".forgot-success").show();
						$(".success_snap").html("Mail sent successfully. Please check your email for reset password");
					}
					if(data == 'false'){
						msgHide();
						$(".error-success").show();
						$(".error_snap").html("Account with this username was not found!");
					}
				  }
				});  
				//$(".success_snap").html("ad");$(".error_snap").html("ad");
				//alert("hurrey!");return false;
			  //form.submit();
			}
        });
    });
	function msgHide(){
		$(".forgot-error").hide();
		$(".forgot-success").hide();
	}