
$(document).ready(function() {	
  // validate signup form on keyup and submit
  $("#addProduct").validate({
	  rules: {
		  productName: {
			  required:true
		  },   
		  productPrice: {
			  required:true,
			  number: true
		  },   
		  productCode: {
			  required:true
		  },            
		  mainImage: {
			  accept: "jpg|jpeg|png|gif"
		  },     
		  image1: {
			  accept: "jpg|jpeg|png|gif"
		  },
		  image2: {
			  accept: "jpg|jpeg|png|gif"
		  },
		  image3: {
			  accept: "jpg|jpeg|png|gif"
		  },
	  },
	  messages: { 
		  productName: {
			  required: "Please enter product name",
		  },
		  productPrice: {
			  required: "Please enter product price",
			  number: "Please enter numeric price"
		  },   
		  productCode: {
			  required: "Please enter product code",
		  },             
		  mainImage: {
			  accept: "Please upload valid image file (jpg|jpeg|png|gif)"
		  },   
		  image1: {
			  accept: "Please upload valid image file (jpg|jpeg|png|gif)"
		  },
		  image2: {
			  accept: "Please upload valid image file (jpg|jpeg|png|gif)"
		  },
		  image3: {
			  accept: "Please upload valid image file (jpg|jpeg|png|gif)"
		  },         
	  }
  });
});