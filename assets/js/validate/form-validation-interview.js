var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#app_receive_date';

var today = new Date();



// var checkin = $(ele).fdatepicker({
//     autoclose: true,
//     //daysOfWeekDisabled: [0, 6],
//     onRender: function(date) {
//         //return date.valueOf() < now.valueOf() ? 'disabled' : '';

//     }
// }).on('changeDate', function(ev) {
//     //if (ev.date.valueOf() > checkout.date.valueOf())
//     //{
//     var newDate = new Date(ev.date)
//     newDate.setDate(newDate.getDate());
//     //checkout.update(newDate);
//     //}
//     checkin.hide();
//     //$('#leave_end_date')[0].focus();
//     //$('#leave_start_date').datepicker('hide');

// }).data('datepicker');

// var checkin = $('#interview_date').fdatepicker({
//     autoclose: true,
//     //daysOfWeekDisabled: [0, 6],
//     onRender: function(date) {
//         //return date.valueOf() < now.valueOf() ? 'disabled' : '';

//     }
// }).on('changeDate', function(ev) {
//     //if (ev.date.valueOf() > checkout.date.valueOf())
//     //{
//     var newDate = new Date(ev.date)
//     newDate.setDate(newDate.getDate());
//     //checkout.update(newDate);
//     //}
//     checkin.hide();
//     //$('#leave_end_date')[0].focus();
//     //$('#leave_start_date').datepicker('hide');

// }).data('datepicker');


$(document).ready(function() {
    // validate signup form on keyup and submit

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');

    jQuery.validator.addMethod("date", function(date, element) {
        return this.optional(element) || date.match(/^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$/);
    }, "Please specify a valid date");

    $("#addInterview").validate({
      errorElement: "p",
        rules: {
            candidate_name: "required",
            reference_name: "required",
            app_receive_date: {
              required: true,
              // date: true
            },/*
            interview_date: {
              required: true,
              date: true
            },*/
            interview_taken_by: "required",
            qualification: "required",
            current_company: "required",
            current_position: "required",
            applied_position: "required",
            current_ctc: {
              required: true,
              number: true
            },
            expected_ctc: {
              required: true,
              number: true
            },
            total_experience: {
              required: true,
              //number: true
            },
            notice_period: {
              required: true,
              //number: true
            },
            current_location: "required",
            contact_number: {
              required: true,
              number: true,
              minlength: 8,
              maxlength: 15,
            },
            email: {
              required: true,
              email: true
            },
            resume: {
              required: false,
              accept: "application/pdf, application/xml-dtd, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document",
              filesize: 5242880,
              extension: "pdf,docx,doc",
            },
            feedback: "required",
            status: "required",
            cat_id: "required",
        },
        messages: {
			//department_id: "Please select department",
            candidate_name: "Please enter candidate name",
            reference_name: "Please enter reference name",
            // app_receive_date: "Please enter valid application received date",
            interview_date: "Please enter valid next action date",
            interview_taken_by: "Please select a user who taken interview",
            qualification: "Please enter qualification",
            current_company: "Please enter current company",
            current_position: "Please enter current position",
            applied_position: "Please enter applied position",
            current_ctc: {
                required:"Please enter current CTC",
                number:"Please enter numbers only"
            },
            expected_ctc: {
                required:"Please enter expected CTC",
                number:"Please enter numbers only"
            },
            total_experience: "Please enter total experience",
            notice_period: "Please enter period in days",
            current_location: "Please enter current location",
            contact_number: {
              required: "Please enter contact number",
              minlength: "Please enter minimum 8 digit contact number",
              maxlength: "The contact number should not exceed 15 digits",
            },
            email: "Please enter a valid email",
            resume:
            {
              accept:"Please select candidate's resume (PDF, DOC, DOCX)",
              filesize: "File Must be under 5 MB",
            },
            feedback: "Please enter feedback",
            status: "Please select status",
            cat_id: "Please select a category",
        },
        errorPlacement: function(error, element) {
          if(element.attr("id") == "app_receive_date2" || element.attr("id") == "interview_date2") {
            error.insertAfter(element.parent("div"));
          } else {            
            error.insertAfter(element);
          }
          
        }
    });
});
