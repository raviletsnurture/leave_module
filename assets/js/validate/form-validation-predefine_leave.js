var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#leave_start_date';
var ele1 = '#leave_end_date';
var checkin = $(ele).fdatepicker({
	 autoclose: true,
    onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		if (ev.date.valueOf() > checkout.date.valueOf()) 
		{
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate());
			checkout.update(newDate);
		}
		checkin.hide();
		$('#leave_end_date')[0].focus();
		//$('#leave_start_date').datepicker('hide');
		
	}).data('datepicker');
	
var checkout = $(ele1).fdatepicker({
    onRender: function(date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		//return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		checkout.hide();
    }).data('datepicker');

$(document).ready(function() {
   // validate signup form on keyup and submit
  $("#addPredefine_Leave").validate({
	  rules: {
		  leave_name: "required",
		  leave_start_date: "required",
		  leave_end_date: "required",
	  },
	  messages: {
		  leave_name: "Please enter Holiday Name",
		  leave_start_date: "Enter Start date",
		  leave_end_date: "Enter End date",
	  }
  });
  
});