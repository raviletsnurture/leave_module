var today = new Date();
$(document).ready(function() {
    // validate signup form on keyup and submit
    $("#addReward").validate({
        rules: {
            user_id: "required",
            reward_type: "required",
            date: "required",
            description:{
                  required: true,
                //  minlength: 50,
                //  maxlength: 100
            },
        },
        messages: {
            user_id: "Please select user",
            reward_type: "Please select reward type",
            date: "Please select date",
            description: {
                          required: "Please enter description for reward",
                        //  minlength: "Please enter at least {0} letters for reward description",
                      //    maxlength: "Please enter less than {0} letters for reward description"
            },
        }
    });

    $("#editReward").validate({
        rules: {
            user_id: "required",
            reward_type: "required",
            date: "required",
            description:{
                  required: true,
                //  minlength: 50,
                 // maxlength: 100
            },
        },
        messages: {
            user_id: "Please select user",
            reward_type: "Please select reward type",
            date: "Please select date",
            description: {
                          required: "Please enter description for reward",
                        //  minlength: "Please enter at least {0} letters for reward description",
                        //  maxlength: "Please enter less than {0} letters for reward description"
            },
        }
    });

});
