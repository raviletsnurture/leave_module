    $(document).ready(function() {
		 $(".aSMS").click(function(){
			  var id = $(this).attr('dir');
			  var type = $(this).attr('type');
			  //alert(id);
			  $("#SMSId").val(id);			  
			  $("#SMSType").val(type); 
			  msgHide();
			  document.forms['smsModelForm'].reset();
		  });
		   
        // validate login form on keyup and submit

		 $("#smsModelForm").validate({
            rules: {
				smsContent: {
                    required: true,                    
                },
            },
            messages: {               
                smsContent: {
                    required: "Please enter a sms text"
                },
            },
			submitHandler: function(form) {
				msgHide();
				var Url = baseUrl + "dashboard/sendsmsmodel";
				$.ajax({
				  type: 'post',
				  url: Url,  
				  cache: false,  
				  data: $("#smsModelForm").serialize(),     
				  success: function(data){
					//alert(data);
					if(data == 'true'){
						msgHide();
						$(".forgot-success").show();
						$(".success_snap").html("SMS sent successfully");
						
					}
					if(data == 'false'){
						msgHide();
						$(".forgot-error").show();
						$(".error_snap").html("Error while sending SMS!, Please check your SMS pack.");
					}
				  }
				});  
				//$(".success_snap").html("ad");$(".error_snap").html("ad");
				//alert("hurrey!");return false;
			  //form.submit();
			}
        });
/***********************************| email validation |*****************************************************/		
		 $(".aEmail").click(function(){
			  var id = $(this).attr('dir');
			  var type = $(this).attr('type');
			  //alert(id);
			  $("#emailId").val(id);			  
			  $("#emailType").val(type); 
			  msgHide();
 			  document.forms['emailModelForm'].reset();

		  });
		   
        // validate login form on keyup and submit

		 $("#emailModelForm").validate({
            rules: {
				emailSubject: {
                    required: true,                    
                },
				emailContent: {
                    required: true,                    
                },
            },
            messages: {               
                emailSubject: {
                    required: "Please enter a email subject"
                },
				emailContent: {
                    required: "Please enter a email text"
                },
            },
			
			submitHandler: function(form) {
				msgHide();
				var Url = baseUrl + "dashboard/sendemailmodel";
				$.ajax({
				  type: 'post',
				  url: Url,  
				  cache: false,  
				  data: $("#emailModelForm").serialize(),     
				  success: function(data){
					//alert(data);
					if(data == 'true'){
						msgHide();
						$(".email-success").show();
						$(".email_snap").html("Email sent successfully");
						
					}
					if(data == 'false'){
						msgHide();
						$(".email-error").show();
						$(".email_snap").html("Error while sending Email!");
					}
				  }
				});  
			}
        });
		
    });
	
	function msgHide(){
		$(".forgot-error").hide();
		$(".forgot-success").hide();
		$(".email-error").hide();
		$(".email-success").hide();
	}
	