$(document).ready(function() {
        // validate login form on keyup and submit
        $("#loginForm").validate({
            rules: {
				UserName: {
                    required: true,                    
					remote: {
					  url: baseUrl+"referrer/login/checkUsername",
					  type: "post",
					  data: {
						Username: function() {
						  return $( "#Username" ).val();
						}
					  }
					}
                },
                rPassword: {
                    required: true,
                },
            },
            messages: {               
                UserName: {
                    required: "Please enter a username",
					remote: "Username does not exist"
                },
                rPassword: {
                    required: "Please provide a password",
                },
            }
        });
		 $("#forgotForm").validate({
            rules: {
				userName: {
                    required: true,                    
					remote: {
					  url: baseUrl+"referrer/login/checkUsername",
					  type: "post",
					  data: {
						UserName: function() {
						  return $( "#userName" ).val();
						}
					  }
					}
                },
            },
            messages: {               
                userName: {
                    required: "Please enter a username",
					remote: "Username does not exist"
                },
                
            },
			submitHandler: function(form) {
				msgHide();
				var Url = baseUrl+"referrer/login/forgotpassword";
				$.ajax({
				  type: 'post',
				  url: Url,  
				  cache: false,  
				  data: "username=" + $( "#userName" ).val(),     
				  success: function(data){
					//alert(data);
					if(data == 'true'){
						msgHide();
						$(".forgot-success").show();
						$(".success_snap").html("Mail sent successfully. Please check your email for reset password");
					}
					if(data == 'false'){
						msgHide();
						$(".error-success").show();
						$(".error_snap").html("Account with this username was not found!");
					}
				  }
				});  
				//$(".success_snap").html("ad");$(".error_snap").html("ad");
				//alert("hurrey!");return false;
			  //form.submit();
			}
        });
		
    // validate signup form on keyup and submit
    $("#signupForm").validate({
        rules: {
            rUsername: {
                required: true,
                minlength: 2,
                remote: {
                    url: baseUrl + "home/referrer/checkUsername",
                    type: "post",
                    data: {
                        UserName: function () {
                            return $("#rUsername").val();
                        }
                    }
                }
            },
            rPassword: {
                required: true,
                minlength: 5
            },
            rRepassword: {
                required: true,
                minlength: 5,
                equalTo: "#rPassword"
            },
            agree: "required"
        },
        messages: {

            rUsername: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters",
                remote: "Username does not exist"
            },
            rPassword: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            rRepassword: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },

        }
    });

		
    });
	function msgHide(){
		$(".forgot-error").hide();
		$(".forgot-success").hide();
	}