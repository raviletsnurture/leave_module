$(document).ready(function() {
        // validate login form on keyup and submit
        $("#loginForm").validate({
            rules: {
				username: {
                    required: true,                    
					remote: {
					  url: "home/checkUsername",
					  type: "post",
					  data: {
						username: function() {
						  return $( "#username" ).val();
						}
					  }
					}
                },
                password: {
                    required: true,
                },
            },
            messages: {               
                username: {
                    required: "Please enter a username",
					remote: "Username does not exist"
                },
                password: {
                    required: "Please provide a password",
                },
            }
        });
		

		 $("#forgotForm").validate({
            rules: {
				username1: {
                    required: true,                    
					remote: {
					  url: "home/checkUsername_forgot",
					  type: "post",
					  data: {
						username: function() {
						  return $("#username1").val();
						}
					  }
					}
                },
            },
            messages: {               
                username1: {
                    required: "Please enter a username",
					remote: "Username does not exist"
                },
                
            },
			submitHandler: function(form) {
				msgHide();
				var Url = "home/forgotpassword";
				$.ajax({
				  type: 'post',
				  url: Url,  
				  cache: false,  
				  data: "username1=" + $( "#username1" ).val(),     
				  success: function(data){
					//alert(data);
					if(data == 'true'){
						msgHide();
						$(".forgot-success").show();
						$(".success_snap").html("Mail sent successfully. Please check your email for reset password");
					}
					if(data == 'false'){
						msgHide();
						$(".error-success").show();
						$(".error_snap").html("Account with this username was not found!");
					}
				  }
				});  
				//$(".success_snap").html("ad");$(".error_snap").html("ad");
				//alert("hurrey!");return false;
			  //form.submit();
			}
        });
    });
	function msgHide(){
		$(".forgot-error").hide();
		$(".forgot-success").hide();
	}