$(document).ready(function() {
   // validate signup form on keyup and submit
  $("#distForm").validate({
	  rules: {
		  firstName: "required",
		  lastName: "required",
		  email: {
			  required: true,
			  email: true
		  },
		  country: {
			  required: true,
		  },
		  phone:{
			   required: true,
			   number: true
		  },
	  },
	  messages: {
		  firstName: "Please enter your firstname",
		  lastName: "Please enter your lastname",
		  email:{
			  required: "Please enter your email address",
			  email: "Please enter a valid email address",
		  },
		  country: {
			  required: "Please select signup country",
		  },
		  phone:{
			   required: "Please enter phone no",
			   number : "Phone number must be numeric"
		  },
	  }
  });
  // validate signup form on keyup and submit
  $("#updatePass").validate({
	  rules: {
		  currPassword: {
			  required:true
		  },                
		  password: {
			  required: true,
			  minlength: 5
		  },
		  repassword: {
			  required: true,
			  minlength: 5,
			  equalTo: "#password"
		  },   
		  distImage: {
			  accept: "jpg|jpeg|png|gif"
		  },                           
	  },
	  messages: { 
		  currPassword: {
			  required: "Please provide a current password",
		  },               
		  password: {
			  required: "Please provide a password",
			  minlength: "Your password must be at least 5 characters long"
		  },
		  repassword: {
			  required: "Please provide a re-type password",
			  minlength: "Your password must be at least 5 characters long",
			  equalTo: "Please enter the same password as above"
		  },
		  distImage: {
			  accept: "Please upload valid image file"
		  },              
	  }
  });
});