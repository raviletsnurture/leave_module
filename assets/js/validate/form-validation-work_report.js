var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#task_start_date';
var ele1 = '#task_end_date';

var checkin = $(ele).datepicker({
    onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		if (ev.date.valueOf() > checkout.date.valueOf()) 
		{
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate());
			checkout.setValue(newDate);
		}
		checkin.hide();
		$('#task_end_date')[0].focus();
    }).data('datepicker');
	
var checkout = $(ele1).datepicker({
    onRender: function(date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		checkout.hide();
    }).data('datepicker');

$(document).ready(function() {
   // validate signup form on keyup and submit
  $("#addWork_Report").validate({
	  rules: {
		  user_id: "required",
		  project_id: "required",
		  milestone_id: "required",
		  task_id: "required",
		  task_start_date: "required",
		  task_end_date: "required",
		  work_hours: {
				required:true,
				number: true
			},
	  },
	  messages: {
		  user_id: "Please select any user",
		  project_id: "Please select Project",
		  milestone_id: "Select milestone",
		  task_id: "Select task",
		  task_start_date: "Enter Start date",
		  task_end_date: "Enter End date",
		  work_hours: {
				required: "Please Enter work hours",
				number: "Enter Numeric Values"
			},
	  }
  });
  
});