 $(document).ready(function() {
        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                firstName: "required",
                lastName: "required",
				email: {
                    required: true,
                    email: true
                },
				uUserId: {
                    required: true,
                    minlength: 2,
					remote: {
					  url: _page == 'checkout' ? baseUrl + 'shop/login/checkUsername' : baseUrl + "shop/registration/checkuserid",
					  type: "post",
					  data: {
						username: function() {
						  return $( "#uUserId" ).val();
						}
					  }
					}
                },
                password: {
                    required: true,
                    minlength: 5
                },
                repassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                }, 
				signupcountry: {
                    required: true,
                },
				address: {
                    required: true,
                },
				country: {
                    required: true,
                },
				phone: {
                    required: true,
					number : true,
                },              
                agree: "required"
            },
            messages: {
                firstName: "Please enter your firstname",
                lastName: "Please enter your lastname",
                uUserId: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters",
					remote: _page == 'checkout' ? "Username not exist or invalid" : "Username already exist", 
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                repassword: {
                    required: "Please provide a re-type password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
				signupcountry: {
                    required: "Please select signup country",
                },
                email:{
					required: "Please enter your email address",
					email: "Please enter a valid email address",
				},
				address: {
                    required: "Please enter your address",
                },
				country: {
                    required: "Please select country",
                },
				phone: {
                    required: "Please enter your phone number",
					number : "Please enter valid phone number",
                },
                agree: "Please accept our policy"
            }
        });


    });

