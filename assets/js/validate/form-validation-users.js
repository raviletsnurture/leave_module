var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#birthdate';
var dd = '#anniversary';



/*var checkin = $(ele).datepicker({
 autoclose: true,
endDate: now,
  onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
  }
}).on('changeDate', function(ev) {
  //checkin.hide();
  //(ev.viewMode == 'days') ? $(this).datepicker('hide') : '';
  //$(ele).datepicker('hide');

}).data('datepicker');*/

$('#birthdate,#joining_date').datepicker({
      //format: 'yyyy-mm-dd',
     	endDate: '+1y',
    	autoclose: true,
			todayHighlight: true,
    });
      $('#reliving_date,#trainee_complete_at,#probation_complete_at').datepicker({
          //format: 'yyyy-mm-dd',
         //endDate: now,
				todayHighlight: true,
        autoclose: true
        });

var dd1 = $(dd).datepicker({
            autoclose: true,
            onRender: function(date) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
}).on('changeDate', function(ev) {
  //checkin.hide();
  //$(ele).datepicker('hide');
}).data('datepicker');

$(document).ready(function() {
   // validate signup form on keyup and submit

   $.validator.addMethod("accept", function(value, element, param) {
  return value.match(new RegExp("." + param + "$"));
});
	$.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");

    $.validator.addMethod("userImage", function(val, element) {
        if($('#mainImage').val() !== '')
        {
            var ext = $('#mainImage').val().split('.').pop().toLowerCase();
            var allow = new Array('png','jpg','jpeg');
            //var allow = new Array('gif','png','jpg','jpeg');
            if(jQuery.inArray(ext, allow) == -1) {
                //document.getElementById("channel_image").src= "product_small.jpg"
               return false
            }else{
                //document.getElementById("channel_image").src= "282116628.jpg"
                return true
              }
        }else{
            return true;
        }
    }, "File trype error");
  $("#addUsers").validate({
	  rules: {
		  role_id : "required",
		  department_id : "required",
		  first_name: {
				required: true,
				loginRegex: true,
			},
		  last_name: {
				required: true,
				loginRegex: true,
			},
		  country_id: "required",
		  state_id: "required",
		  city_id: "required",
		  street: "required",
		  zipcode: "required",
		  gender: "required",
		  birthdate: "required",
		  mobile: {
				required: false,
				number: true,
			},
		  landline: {
				required: false,
				number: true,
			},
		  //religion_id: "required",
		  username: {
                    required: true,
					loginRegex: true,
					remote: {
					  url: "checkUsername",
					  type: "post",
					  data: {
						username: function() {
						  return $( "#username" ).val();
						}
					  }
					}
                },
		 /* password: {
                    required: true,
                },*/
		  email: {
			  required: true,
			  email: true
		  },
      gmail_id: {
			  required: false,
			  email: true
		  },
		  alternate_email: {
			  required: false,
			  email: true
		  },
		  skype: {
				required: false,
				//accept: "[a-zA-Z]+",
			},
            mainImage:{
                userImage:true
,            },
	  },
	  messages: {
		  role_id: "Please select role",
		  department_id: "Please select department",
		  first_name: {
				required: "Please enter your firstname",
				loginRegex: "Enter Alphanumeric value",
				},
		  last_name: {
				required: "Please enter your lastname",
				loginRegex: "Enter Alphanumeric value",
				},
		  country_id: "Country is required",
		  state_id: "State is required",
		  city_id: "City is required",
		  street: "Add your address",
		  zipcode: "Zipcode is required",
		  gender: "Please Enter your gender",
		  birthdate :"please Enter your birthdate",
		  mobile: {
				number: "Please Enter number Only"
			},
		  landline: {
				number: "Please Enter number Only"
			},
		 // religion_id: "Select your religion",
		  username: {
                    required: "Please enter a username",
					remote: "Username already exist",
					loginRegex: "Enter Alphanumeric value",
                },
          /*password: {
                    required: "Please provide a password",
                },*/
		  email:{
			  required: "Please enter your email address",
			  email: "Please enter a valid email address",
		  },
		  alternate_email:{
			  email: "Please enter a valid email address",
		  },
		  skype: {
				//accept: "Enter only characters",
			},
        mainImage:{
            userImage:"Please select an Image"
,       },
	  }
  });

  $("#updatePass").validate({
	  rules: {
		  currPassword: {
			  required:true
		  },
		  password1: {
			  required: true,
			  minlength: 5
		  },
		  repassword: {
			  required: true,
			  minlength: 5,
			  equalTo: "#password1"
		  },
		  distImage: {
			  accept: "jpg|jpeg|png|gif"
		  },
	  },
	  messages: {
		  currPassword: {
			  required: "Please provide a current password",
		  },
		  password1: {
			  required: "Please provide a password",
			  minlength: "Your password must be at least 5 characters long"
		  },
		  repassword: {
			  required: "Please provide a re-type password",
			  minlength: "Your password must be at least 5 characters long",
			  equalTo: "Please enter the same password as above"
		  },
		  distImage: {
			  accept: "Please upload valid image file"
		  },
	  }
  });



});
