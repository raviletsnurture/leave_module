
$(document).ready(function() {
   // validate signup form on keyup and submit

    $.validator.addMethod("accept", function(value, element, param) {
	  return value.match(new RegExp("." + param + "$"));
	});
	$.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
    $.validator.addMethod("regularcustom", function(value, element) {
        return this.optional(element) || /^[A-Za-z ]+$/i.test(value);
    }, "Enter only character.");

   
    $("#addEdittechnology").validate({
		rules: {
		  	technology: {
					required: true,
					regularcustom: true,
				},
		    },
		messages: {
			technology: {
					required: "Please enter technology",
					regularcustom: "Enter only characters",
			},	
		}
    });

    $("#addEditvacancy").validate({
		rules: {
		  	technology_name: {
					required: true,					
				},		    
		    vacancy: {
					required: true,
					number: true,
				},
		    },		    
		messages: {
			technology_name: {
					required: "Please enter vacancy",					
			},
			vacancy: {
					required: "Please enter vacancy",					
			},	
		}
    });

});
