    $(document).ready(function() {
		jQuery.validator.addMethod("greaterThan", 
		function(value, element, params) {
			if (!/Invalid|NaN/.test(new Date(value))) {
			 new Date(value) > new Date($(params).val());
		}
		
		return isNaN(value) && isNaN($(params).val()) 
		|| (Number(value) > Number($(params).val())); 
		},'Must be greater than {0}.');
		
        // validate signup form on keyup and submit
        $("#addReferrer").validate({
            rules: {
                rUserName: {
                    required: true,
                    minlength: 2,
					remote: {
					  url: baseUrl + "referrer/checkUsername",
					  type: "post",
					  data: {
						rUserName: function() {
						  return $( "#rUserName" ).val();
						},
						rUserNameOld: function() {
						  return $( "#rUserNameOld" ).val();
						}
					  }
					}
                },
				password: {
                    //required: true,
                    minlength: 5
                },
                repassword: {
                  //  required: true,
                    minlength: 5,
                    equalTo: "#password"
                },  
				firstName: "required",
                lastName: "required",
				phone:{
					required: true,
					number:true
				},
				email: {
                    required: true,
                    email: true
                },
				serialnumberFrom: {
                    number: true,
					/*remote: {
					  url: baseUrl + "referrer/checkserial",
					  type: "post",
					  data: {
						serial: function() {
						  return $( "#serialnumberFrom" ).val();
						}
					  }
					}*/
                },
				serialnumberTo: { 
					greaterThan: "#serialnumberFrom", 
					number: true,
					/*remote: {
					  url: baseUrl + "referrer/checkserial",
					  type: "post",
					  data: {
						serial: function() {
						  return $( "#serialnumberTo" ).val();
						}
					  }
					}*/
				}
            },
            messages: {
                rUserName: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters",
					remote: "Username already exist"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                repassword: {
                    required: "Please provide a re-type password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
				firstName: "Please enter your firstname",
                lastName: "Please enter your lastname",
               	phone:{
					required: "Please enter your phone number",
					number: "Phone number must be numeric"
				},
                email:{
					required: "Please enter your email address",
					email: "Please enter a valid email address",
				},
				serialnumberFrom : {
					number: "Serial number from must be numeric",
					/*remote: "Serial number already used",*/
				},
				serialnumberTo: {
					number: "Serial number from must be numeric", 
					greaterThan: "Please enter greater than value to serial number from",
					/*remote: "Serial number already used",*/
  
				}
            }
        });
    });

