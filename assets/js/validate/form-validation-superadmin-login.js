$(document).ready(function() {
        // validate login form on keyup and submit
        $("#loginForm").validate({
            rules: {
				sUsername: {
                    required: true,                    
                },
                sPassword: {
                    required: true,
                },
            },
            messages: {               
                sUsername: {
                    required: "Please enter a username",
                },
                sPassword: {
                    required: "Please provide a password",
                },
            }
        });
		 $("#forgotForm").validate({
            rules: {
				userName: {
                    required: true,                    
					remote: {
					  url: baseUrl+"home/referrer/checkUsername",
					  type: "post",
					  data: {
						UserName: function() {
						  return $( "#userName" ).val();
						}
					  }
					}
                },
            },
            messages: {               
                userName: {
                    required: "Please enter a username",
					remote: "Username does not exist"
                },
                
            },
			submitHandler: function(form) {
				msgHide();
				var Url = "forgotpassword";
				$.ajax({
				  type: 'post',
				  url: Url,  
				  cache: false,  
				  data: "username=" + $( "#userName" ).val(),     
				  success: function(data){
					//alert(data);
					if(data == 'true'){
						msgHide();
						$(".forgot-success").show();
						$(".success_snap").html("Mail sent successfully. Please check your email for reset password");
					}
					if(data == 'false'){
						msgHide();
						$(".error-success").show();
						$(".error_snap").html("Account with this username was not found!");
					}
				  }
				});  
				//$(".success_snap").html("ad");$(".error_snap").html("ad");
				//alert("hurrey!");return false;
			  //form.submit();
			}
        });
		
    // validate signup form on keyup and submit

    });
	function msgHide(){
		$(".forgot-error").hide();
		$(".forgot-success").hide();
	}