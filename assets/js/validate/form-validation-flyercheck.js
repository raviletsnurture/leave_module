function shopPopup(){
	var _url = baseUrl + 'shop/checkflayer';
	var daTa = $("#flyerForm").serialize();
	 
	var ret = false;
	if($("#flyernumber").val() == '' && $("#uUserId").val() == '' && $("#uPassword").val() == ''){ 
		$(".flyer-error").show();
		$(".error_snap").html("Please fill out below field(s) to enter shop!");
		return false;
	}else{
		$(".flyer-error").hide();
		$(".error_snap").html('');
	}
	if($("#flyernumber").val() != '' || $("#uUserId").val() != '' || $("#uPassword").val() != ''){ 
		$.ajax({
			type: 'post',
			url: _url,  
			cache: false,		
			data: daTa,
			async:false,
			dataType: "json",
			success: function(data){
				if(data.boolean == 'false' && data.msg != ''){
					$(".flyer-error").show();
					$(".error_snap").html(data.msg);
				}else if(data.boolean == 'true'){
					$("#flyerForm").submit();
				}
			}
		});
	}
	return ret;	
}
 $(document).ready(function() {
        // validate signup form on keyup and submit
		 
		
		
        $("#flyerForm1").validate({
            rules: {
                flyernumber: {
                    required: true,
					remote: {
					  url: baseUrl + "shop/checkflayer",
					  type: "post",
					  data: {
							flyernumber: function() {
 								return $('#flyernumber').val();
						    },
							uUserId: function() {
 								return $('#uUserId').val();
						    },
							
					  },
					}
				},
				uUserId:{
					required: true,                    
					remote: {
					  url: "login/checkUsername",
					  type: "post",
					  data: {
						username: function() {
						  return $( "#uUserId" ).val();
						}
					  }
					}
				},
				uPassword:{
				}
            },
			
            messages: {
                flyernumber: {
                    required: "Please enter a flyer number",
					remote: "Invalid flyer number!"
				// remote: jQuery.format("{0} is already taken")
                },
				uUserId: {
					remote: "Username does not exist"
                },
                password: {
                     
                },
            }
        });
    });

