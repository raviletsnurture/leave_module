var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#start_date';
var ele1 = '#end_date';
var checkin = $(ele).datepicker({
    onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		if (ev.date.valueOf() > checkout.date.valueOf()) 
		{
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate());
			checkout.setValue(newDate);
		}
		checkin.hide();
		$('#end_date')[0].focus();
    }).data('datepicker');
	
var checkout = $(ele1).datepicker({
    onRender: function(date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		checkout.hide();
    }).data('datepicker');

$(document).ready(function() {
   // validate signup form on keyup and submit
  $("#addMilestone").validate({
	  rules: {
		  milestone_name: "required",
		  project_id: "required",
		  milestone_description: "required",
		  start_date: "required",
		  end_date: "required",
	  },
	  messages: {
		  milestone_name: "Please enter Project Name",
		  project_id: "Please select Department",
		  milestone_description: "Enter Description",
		  start_date: "Enter Start date",
		  end_date: "Enter End date",
	  }
  });
  
});