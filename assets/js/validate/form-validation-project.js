var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#start_date';
var ele1 = '#end_date';


var checkin = $(ele).datepicker({
    onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		if (ev.date.valueOf() > checkout.date.valueOf()) 
		{
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate());
			checkout.setValue(newDate);
		}
		checkin.hide();
		$('#end_date')[0].focus();
    }).data('datepicker');
	
var checkout = $(ele1).datepicker({
    onRender: function(date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		checkout.hide();
    }).data('datepicker');

$(document).ready(function() {
   // validate signup form on keyup and submit
  $("#addProject").validate({
	  rules: {
		  project_name: "required",
		  department_id: "required",
		  customer_id: "required",
		  project_description: "required",
		  live_url: {
				required: false,
				url: true
				},
		  start_date: "required",
		  end_date: "required",
		  estimated_hours: {
				required: false,
				number: true
		  }
		  
	  },
	  messages: {
		  project_name: "Please enter Project Name",
		  department_id: "Please select Department",
		  customer_id: "Please select Customer",
		  project_description: "Enter Description",
		  live_url: {
				//required: "Please Enter live url",
				url: "Please enter valid URL"
				},
		  start_date: "Enter Start date",
		  end_date: "Enter End date",
		  estimated_hours: {
				number: "Please enter numeric value"
		  }
	  }
  });
  
});