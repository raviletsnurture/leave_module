var today = new Date();
$(document).ready(function() {
    // validate signup form on keyup and submit
    $("#addReward").validate({
        rules: {
            user_id: "required",
            reward_point: {
                  required: true,
                //  minlength: 50,
                  number: true,
                  max: 9999
            },
            description:{
                  required: true,
                //  minlength: 50,
                  maxlength: 200
            },
        },
        messages: {
            user_id: "Please select user",
            reward_point: {
                          required: "Please enter reward points",
                        //  minlength: "Please enter at least {0} letters for reward description",
                          max: "Please enter less than {0}"
            },
            description: {
                          required: "Please enter description for reward",
                        //  minlength: "Please enter at least {0} letters for reward description",
                          maxlength: "Please enter less than {0} letters for reward description"
            },
        }
    });

});
