var today = new Date();
$(document).ready(function() {
    // validate signup form on keyup and submit
    $("#addLeave").validate({
        rules: {
            product_name: {
              required: true,
            //minlength: 50,
              maxlength: 50
            },
            point: {
              required: true,
            //minlength: 50,
              number: true,
              max: 9999999
            },
            /*prod_image : "required",  */
            description:{
                  required: true,
                //  minlength: 50,
                  maxlength: 200
            },
        },
        messages: {
            product_name: {
                required: "Please enter product name",
              //  minlength: "Please enter at least {0} letters for reward description",
                maxlength: "Please enter less than {0} letters"
            },
            point: {
              required: "Please enter product points",
            //  minlength: "Please enter at least {0} letters for reward description",
              max: "Please enter less than {0}"
            },
            /*prod_image:{
              required: "Please image is required",
            },*/
            description: {
              required: "Please enter description for reward",
            //  minlength: "Please enter at least {0} letters for reward description",
              maxlength: "Please enter less than {0} letters for product"
            },
        }
    });

});
