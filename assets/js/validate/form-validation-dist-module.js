var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var lele = '#birthday';
	var checkin = $(lele).datepicker({
	onRender: function(date) {
		return date.valueOf() > now.valueOf() ? 'disabled' : '';
	}
	}).on('changeDate', function(ev) { 
		checkin.hide();
		$(lele).datepicker('hide');
	}).data('datepicker');

    $(document).ready(function() {
        // validate signup form on keyup and submit
        $("#addDistributor").validate({
            rules: {
                userId: {
                    required: true,
                    minlength: 2,
					remote: {
					  url: baseUrl + "distributor/checkUsername",
					  type: "post",
					  data: {
						  
						username: function() {
						  return $( "#userId" ).val();
						},
						usernameOld: function() {
						  return $( "#userIdOld" ).val();
						}
					  }
					}
                },
				password: {
                    //required: true,
                    minlength: 5
                },
                repassword: {
                  //  required: true,
                    minlength: 5,
                    equalTo: "#password"
                },  
				firstName: "required",
                lastName: "required",
				phone:{
					required: true,
					number:true
				},
				email: {
                    required: true,
                    email: true
                },
				localOrderEmail: {
                    email: true
                },
				signupcountry:{
					required: true,
				},
        		
            },
            messages: {
                 userId: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters",
					remote: "Username already exist"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                repassword: {
                    required: "Please provide a re-type password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
				firstName: "Please enter your firstname",
                lastName: "Please enter your lastname",
               	phone:{
					required: "Please enter your phone number",
					number: "Phone number must be numeric"
				},
                email:{
					required: "Please enter your email address",
					email: "Please enter a valid email address",
				},
				localOrderEmail: {
					email: "Please enter a valid email address",
                },
				signupcountry:{
					required: "Please select signup country",
				}
            }
        });
    });

