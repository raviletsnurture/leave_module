$(document).ready(function () {
    // validate signup form on keyup and submit
    $("#signupForm").validate({
        rules: {
            username: {
                required: true,
                minlength: 2,
                remote: {
                    url: baseUrl + "home/checkUsername",
                    type: "post",
                    data: {
                        username: function () {
                            return $("#username").val();
                        }
                    }
                }
            },
            password2: {
                required: true,
                minlength: 5
            },
            repassword2: {
                required: true,
                minlength: 5,
                equalTo: "#password2"
            },
            agree: "required"
        },
        messages: {

            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters",
                remote: "Username does not exist"
            },
            password2: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            repassword2: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },

        }
    });
});