$(document).ready(function () {
    // validate signup form on keyup and submit
    $("#signupForm").validate({
        rules: {
            uUserId: {
                required: true,
                minlength: 2,
                remote: {
                    url: baseUrl + "shop/login/checkUsername",
                    type: "post",
                    data: {
                        username: function () {
                            return $("#uUserId").val();
                        }
                    }
                }
            },
            password: {
                required: true,
                minlength: 5
            },
            repassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            agree: "required"
        },
        messages: {

            uUserId: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters",
                remote: "Username does not exist"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            repassword: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },

        }
    });
});