
$(document).ready(function() {
   // validate signup form on keyup and submit
	$.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Please only letters, numbers, or dashes");

  $("#addInventoryDevices").validate({
	  rules: {
		  system_tag :{
				required: true,
                maxlength: 100
			},
		  inventory_type :{
				required: true
			},
		  manufacturer: {
				required: true,
                maxlength: 100
			},
		  serial_number: {
				required: true,
				
                maxlength: 100
			},
		  modal: {
				required: true,

                maxlength: 100
			},
		  os: {

                maxlength: 100
		  },
          ram: {

                maxlength: 100
		  },
          os_version: {

                maxlength: 100
		  },
          hdd_modal: {

                maxlength: 100
		  },
          hdd_capacity: {

                maxlength: 100
		  },
          hdd_manf: {

                maxlength: 100
		  },
          hdd_serial_number: {

                maxlength: 100
		  },
          imei_number: {

                maxlength: 100
		  },
          imei2_number: {

                maxlength: 100
		  },
          mac_address_ethernet: {

                maxlength: 100
		  },
          mac_address_wlan: {

                maxlength: 100
		  },
          configuration: {

                maxlength: 200
		  },
          software_name: {

                maxlength: 100
		  },
          licence: {

                maxlength: 100
		  },
          soft_expiration: {

                maxlength: 100
		  },
          working_status: {
                required:true
		  },
          assign_status: {
                required:true
		  },

	  },
	  messages: {
          system_tag :{
				required: "Please type system tag",
				loginRegex: "Please enter  only letters, numbers, or dashes",
                maxlength: "Please enter at most {0} characters"
			},
		  inventory_type :{
              required: "Please type Inventory type"
			},
		  manufacturer: {
				required: "Please enter manufacturer",
                maxlength: 100
			},
		  serial_number: {
              required: "Please type serial number",
              maxlength: "Please enter at most {0} characters"
			},
		  modal: {
              required: "Please type modal name",
              maxlength: "Please enter at most {0} characters"
			},
		  os: {
              maxlength: "Please enter at most {0} characters"
		  },
          ram: {
              maxlength: "Please enter at most {0} characters"
		  },
          os_version: {
              maxlength: "Please enter at most {0} characters"
		  },
          hdd_modal: {
              maxlength: "Please enter at most {0} characters"
		  },
          hdd_capacity: {
              maxlength: "Please enter at most {0} characters"
		  },
          hdd_manf: {
              maxlength: "Please enter at most {0} characters"
		  },
          hdd_serial_number: {
                maxlength: "Please enter at most {0} characters"
		  },
          imei_number: {
              maxlength: "Please enter at most {0} characters"
		  },
          imei2_number: {
              maxlength: "Please enter at most {0} characters"
		  },
          mac_address_ethernet: {
              maxlength: "Please enter at most {0} characters"
		  },
          mac_address_wlan: {
              maxlength: "Please enter at most {0} characters"
		  },
          configuration: {
              maxlength: "Please enter at most {0} characters"
		  },
          software_name: {
              maxlength: "Please enter at most {0} characters"
		  },
          licence: {
              maxlength: "Please enter at most {0} characters"
		  },
          soft_expiration: {
              maxlength: "Please enter at most {0} characters"
		  },
          working_status: {
                required:"Please select working status"
		  },
          assign_status: {
                required:"Please select assign status"
		  },
	  }
  });

  $("#updatePass").validate({
	  rules: {
		  currPassword: {
			  required:true
		  },
		  password1: {
			  required: true,
			  minlength: 5
		  },
		  repassword: {
			  required: true,
			  minlength: 5,
			  equalTo: "#password1"
		  },
		  distImage: {
			  accept: "jpg|jpeg|png|gif"
		  },
	  },
	  messages: {
		  currPassword: {
			  required: "Please provide a current password",
		  },
		  password1: {
			  required: "Please provide a password",
			  minlength: "Your password must be at least 5 characters long"
		  },
		  repassword: {
			  required: "Please provide a re-type password",
			  minlength: "Your password must be at least 5 characters long",
			  equalTo: "Please enter the same password as above"
		  },
		  distImage: {
			  accept: "Please upload valid image file"
		  },
	  }
  });



});
