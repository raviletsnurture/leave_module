var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#birthdate';
var checkin = $(ele).datepicker({
  onRender: function(date) {
    return date.valueOf() > now.valueOf() ? 'disabled' : '';
  }
}).on('changeDate', function(ev) { 
  checkin.hide();
  $(ele).datepicker('hide');
	
}).data('datepicker');

$(document).ready(function() {
   // validate signup form on keyup and submit
   
   // $.validator.addMethod('regex', function(value, element, param) {
        // return this.optional(element) ||
            // value.match(typeof param == 'string' ? new RegExp(param) : param);
    // },
    // 'Please enter a value in the correct format.');
  $("#addCustomer").validate({
	  rules: {
		  first_name: {
			required: true,
			//regex: "/^[a-zA-Z0-9-_]+$/",
			},
		  //middle_name: "required",
		  last_name: "required",
		  country_id: "required",
		  state_id: "required",
		  city_id: "required",
		  street: "required",
		  zipcode: "required",
		  gender: "required",
		  birthdate: "required",
		  religion: "required",
		  personal_email: {
			  required: true,
			  email: true
		  },
		   linkedin: {
				required: false,
				url: true
			},
		  company_linkedin: {
				required: false,
				url: true
			},
		  twitter_id: {
				required: false,
				url: true
			},
		  company_twitter_id: {
				required: false,
				url: true
			},
		  facebook_id: {
				required: false,
				url: true
			},
		  facebook_page: {
				required: false,
				url: true
			},
		  other_personal_email: {
			  required: false,
			  email: true
		  },
		  company_email: {
			  required: false,
			  email: true
		  },
		  other_company_email: {
			  required: false,
			  email: true
		  },
		   
		   company_phone: {
			   required: false,
			   number: true
		   },
		   company_website: {
			   required: false,
			   url: true
		   },
		   personal_website: {
			   required: false,
			   url: true
		   },
		  signupcountry: {
			  required: true,
		  },
		  personal_phone:{
			   required: true,
			   number: true
		  },
		  status_id:{
			   required: true,
			   
		  },
	  },
	  messages: {
		  first_name: {
				required: "Please enter your firstname",
				//regex: "<br />Please enter a value in the correct format.." 
			},
		  //middle_name: "Please enter your firstname",
		  last_name: "Please enter your lastname",
		  country_id: "Country is required",
		  state_id: "State is required",
		  city_id: "city is required",
		  street: "Add your address",
		  zipcode: "Zipcode is required",
		  gender: "Please Enter your gender",
		  birthdate :"please Enter your birthdate",
		  religion: "Select your religion",
		  personal_email:{
			  required: "Please enter your email address",
			  email: "Please enter a valid email address",
		  },
		  company_email:{
			  email: "Please enter a valid email address",
		  },
		  other_company_email:{
			  email: "Please enter a valid email address",
		  },
		  other_personal_email:{
			  email: "Please enter a valid email address",
		  },
		  linkedin: {
				url: "Enter valid URL",
			},
		  company_linkedin: {
				url: "Enter valid URL",
			},
		  twitter_id: {
				url: "Enter valid URL",
			},
		  company_twitter_id: {
				url: "Enter valid URL",
			},
		  facebook_id: {
				url: "Enter valid URL",
			},
		  facebook_page: {
				url: "Enter valid URL",
			},
			company_website: {
				url: "Enter valid URL",
			},
			personal_website: {
				url: "Enter valid URL",
			},
		  
		  signupcountry: {
			  required: "Please select signup country",
		  },
		  status_id:{
			   required: "Please Select customer type"
		  },
		  company_phone:{
			   number: "Please Select only numbers"
		  },
	  }
  });
  
});