var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var lele = '#lBirthDate';
	var checkin = $(lele).datepicker({
	onRender: function(date) {
		return date.valueOf() > now.valueOf() ? 'disabled' : '';
	}
	}).on('changeDate', function(ev) { 
		checkin.hide();
		$(lele).datepicker('hide');
	}).data('datepicker');
$(document).ready(function() {


	// validate signup form on keyup and submit
	$("#addLead").validate({
		rules: {
			lFirstName: "required",
			lLastName: "required",
			lEmail: {
				//required: true,				
				email: true
			},
			lPhone:{
				required: true,
				number : true
			},
			leadType:"required",
			leadStatus:"required",
	
		},
		messages: {
			lFirstName: "Please enter first name",
			lLastName: "Please enter last name",
			lEmail:{
				//required: "Please enter email address",				
				email: "Please enter a valid email address",
			},
			lPhone:{
				required: "Please enter phone no",
				number : "Phone number must be numeric"
			},
			leadType:"Plese select lead type",
			leadStatus:"Please select lead status	",
		}
	});

	


});

