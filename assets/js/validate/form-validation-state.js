$(document).ready(function() {
	// validate signup form on keyup and submit
	
	$("#addState").validate({
		rules: {
			country_id: "required",
			state_name: "required",
		},
		messages: {
		    country_id : "Select Country",
			state_name: "Please enter state",
		}
	});
	
	$("#addCity").validate({
		rules: {
			country_id: "required",
			state_id: "required",
			city_name: "required",
		},
		messages: {
		    country_id : "Select Country",
			state_id: "Please enter state",
			city_name: "Please Enter city",
		}
	});
	
	$("#addCountry").validate({
		rules: {
			short_name: "required",
		},
		messages: {
		    short_name : "Please Enter country",
		}
	});
	
	$("#addRole").validate({
		rules: {
			role_name: "required",
			lettersonly: true,
		},
		messages: {
			role_name: "Please enter role",
			lettersonly: "Enter only letters",
		}
	});
	
	$("#addLeave_Status").validate({
		rules: {
			leave_status: "required",
		},
		messages: {
			leave_status: "Please enter status",
		}
	});
	
	$("#addLeave_Type").validate({
		rules: {
			leave_type: "required",
		},
		messages: {
			leave_type: "Please enter Type",
		}
	});
	
	$("#addCustomer_Status").validate({
		rules: {
			customer_status_name: "required",
		},
		messages: {
			customer_status_name: "Please enter name",
		}
	});
	
	$("#addTemplates").validate({
		rules: {
			email_name: "required",
			email_shortcode: {
						required: true,
						maxlength: 20,
						},
			email_description: "required",
		},
		messages: {
			email_name: "Please enter name",
			email_shortcode: {
						required: "Please Enter shortcode",
						maxlength: "Only 10 Characters Required",
						},
			email_description: "Enter Description",
		}
	});
	
	$("#updatePass").validate({
		rules: {
			  currPassword: {
				  required:true
			  },                
			  password1: {
				  required: true,
				  minlength: 5
			  },
			  repassword: {
				  required: true,
				  minlength: 5,
				  equalTo: "#password1"
			  },   
			  distImage: {
				  accept: "jpg|jpeg|png|gif"
			  },                           
		  },
		  messages: { 
			  currPassword: {
				  required: "Please provide a current password",
			  },               
			  password1: {
				  required: "Please provide a password",
				  minlength: "Your password must be at least 5 characters long"
			  },
			  repassword: {
				  required: "Please provide a re-type password",
				  minlength: "Your password must be at least 5 characters long",
				  equalTo: "Please enter the same password as above"
			  },
			  distImage: {
				  accept: "Please upload valid image file"
			  },              
		  }
	});
	
});

