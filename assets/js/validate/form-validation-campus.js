
$(document).ready(function() {
   // validate signup form on keyup and submit

    $.validator.addMethod("accept", function(value, element, param) {
	  return value.match(new RegExp("." + param + "$"));
	});
	$.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
    $.validator.addMethod("regularcustom", function(value, element) {
        return this.optional(element) || /^[A-Za-z ]+$/i.test(value);
    }, "Enter only character.");

    $.validator.addMethod("userImage", function(val, element) {
        if($('#mainImage').val() !== '')
        {
            var ext = $('#mainImage').val().split('.').pop().toLowerCase();
            var allow = new Array('png','jpg','jpeg');
            //var allow = new Array('gif','png','jpg','jpeg');
            if(jQuery.inArray(ext, allow) == -1) {
                //document.getElementById("channel_image").src= "product_small.jpg"
               return false
            }else{
                //document.getElementById("channel_image").src= "282116628.jpg"
                return true
              }
        }else{
            return true;
        }
    }, "File trype error");
  $("#addCampus").validate({
	  rules: {
	  	    college_name: {
				required: true,
				regularcustom: true,
			},
			university_name: {
				required: true,
				regularcustom: true,
			},
			email: {
			   required: true,
			   email: true
		    },
			/*alternate_email: {
				required: false,
				email: true
			},*/
			mobile: {
				required: true,
				number: true,
			},
			/*emergency_number: {
				required: true,
				number: true,
			},*/
			contact_person_name_1: {
				required: true,
				regularcustom: true,
			},
			contact_person_name_2: {
				regularcustom: true,
			},
			/*fax: "required",*/
			street: "required",
			country_id: "required",
			state_id: "required",
			city_id: "required",			
			/*zipcode: "required",*/
			gender: "required",
			recruitment_status: "required",
			tier: "required",
	    },
	    messages: {
			college_name: {
				required: "Please enter college name",
				regularcustom: "Enter only characters",
			},		 
			university_name: {
				required: "Please enter university name",
				regularcustom: "Enter only characters",
			},
			email:{
				required: "Please enter email address",
				email: "Please enter a valid email address",
			},
			/*alternate_email:{
				email: "Please enter a valid email address",
			},*/
			mobile: {
				required: "Please enter mobile number",
				number: "Please enter number Only"
			},
			/*emergency_number: {
				required: "Please enter emergency number",
				number: "Please enter number Only"
			},*/	
			contact_person_name_1: {
				required: "Please enter contact person name",
				regularcustom: "Enter only characters",
			},
			contact_person_name_2: {
				regularcustom: "Enter only characters",
			},
			/*fax: "Please enter fax",*/
			street: "Please enter street",
			country_id: "Please select country",
			state_id: "Please select state",
			city_id: "Please select city",			
			/*zipcode: "Please enter zipcode",*/
			gender: "Please selct gender",
			recruitment_status: "Please select status",
			tier: "Please select tier",
	  }
  });

});
