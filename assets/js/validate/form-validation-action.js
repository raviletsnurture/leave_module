var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var lele = '#aDate';
var checkin = $(lele).datepicker({
onRender: function(date) {
	//return date.valueOf() < now.valueOf() ? 'disabled' : '';
}
}).on('changeDate', function(ev) { 
	checkin.hide();
	$(lele).datepicker('hide');
}).data('datepicker');
$(document).ready(function() {
	
	$('.timepicker-24').timepicker({
		autoclose: true,
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		defaultTime: false,
	});
	
	// validate signup form on keyup and submit
	
	$("#addAction").validate({
		rules: {
			aActionRecipients :"required",
			aLeadId: "required",
			aActionOfDistId: "required",
			//aDistId: "required",			
			actionType:"required",
			actionStatus:"required",
			aOccasion: "required",
			//aOccurrence: "required",			
			//aReminder:"required",
			//aHeadline:"required",
			aDate:"required",
			aTime:"required",	
		},
		messages: {
			aActionRecipients :"Please select action recipient",
			aLeadId: "Please select lead",
			aActionOfDistId: "Please select distributor",
			//aDistId: "Please enter dist",			
			actionType:"Plese select action type",
			actionStatus:"Please select action status",
			aOccasion: "Please enter occasion",
			//aOccurrence: "Please enter occurrence",			
			//aReminder:"Plese enter reminder",
			//aHeadline:"Please enter headline",		
			aDate:"Plese enter date",
			aTime:"Please enter time",
		}
	});
	
	selectDropDown(selectedType);
	$("#aActionRecipients").change(function() {	
		var selectedRec = $(this).val();
		selectDropDown(selectedRec);
		
	});
	
});
function selectDropDown(selectedRec){
	if(selectedRec == 'lead'){
		$(".distSelect").hide();
		$(".leadSelect").show();
	}
	if(selectedRec == 'dist'){
		$(".distSelect").show();
		$(".leadSelect").hide();
	}
	if(selectedRec != 'dist' && selectedRec != 'lead'){
		$(".distSelect").hide();
		$(".leadSelect").hide();
	}
}
