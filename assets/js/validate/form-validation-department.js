$(document).ready(function() {
	// validate signup form on keyup and submit
	$("#addDepartment").validate({
		rules: {
			"department_name": "required",
			"seniors[]": "required",
		},
		messages: {
			"department_name": "Please enter Department",
			"seniors[]": "Please select Seniors",
		}
	});
});
