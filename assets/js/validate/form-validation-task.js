var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var ele = '#start_date';
var ele1 = '#end_date';

var checkin = $(ele).datepicker({
    onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		if (ev.date.valueOf() > checkout.date.valueOf()) 
		{
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate());
			checkout.setValue(newDate);
		}
		checkin.hide();
		$('#end_date')[0].focus();
    }).data('datepicker');
	
var checkout = $(ele1).datepicker({
    onRender: function(date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
		checkout.hide();
    }).data('datepicker');

$(document).ready(function() {
   // validate signup form on keyup and submit
  $("#addTask").validate({
	  rules: {
		  task_name: "required",
		  project_id: "required",
		  task_description: "required",
		  assigned_hours: {
				required: true,
				number: true
			},
		  milestone_id: "required",
	  },
	  messages: {
		  task_name: "Please enter Task Name",
		  project_id: "Please select Project",
		  task_description: "Enter Task Description",
		  assigned_hours: {
				required: "Enter Assigned Hours",
				number: "Enter Numeric values"
			},
		  milestone_id: "Select the Milestone",
	  }
  });
  
  
  $("#addAssignTask").validate({
	  rules: {
		  user_id: "required",
		  task_id: "required",
		  start_date: "required",
		  end_date: "required",
	  },
	  messages: {
		  user_id: "Please select user",
		  task_id: "Please select tasks",
		  start_date: "Enter start date",
		  end_date: "Enter end date",
	  }
  });
  
});