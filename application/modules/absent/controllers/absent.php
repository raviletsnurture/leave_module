<?php
class Absent extends CI_Controller {
  function __construct(){
		parent::__construct();
    $this->template->set('controller', $this);
		$this->load->library('email');
		$this->load->helper('general_helper');
		$this->load->model('absent_model');
	}

  public function index(){
    $dataLeave["leave"] = $this->absent_model->getAllAbsentLeaves();
    $finalEmailHtml = $this->load->view("absent/cancel_tmp",$dataLeave, true);
    $subject="Leaves today";
    $toEmail = "hr@letsnurture.com";
    send_mail($toEmail, $subject, $finalEmailHtml);
    echo "mail sent successfully & i will run in CLI so i will never be visible";
  }
}?>
