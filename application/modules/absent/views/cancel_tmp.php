<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<title>HRMS</title>
</head>
<body style="font-family:Arial, Helvetica, sans-serif; margin:0; padding:0; ">
<table style="width:1000px; margin:0 auto; background:#303f56; padding:0px 20px 0 20px;">
	<tr>
    	<td style="text-align:center; padding:30px 0 30px 0;">
			<a style="font-size:13px; color:#FFF; font-weight:normal;" href="hrms.letsnurture.com">
				<img src="http://localhost/ln-projects/demo/hrms/assets/img/hrms_logo.png" alt="HRMS" />
			</a>
		</td>
    </tr>
    <tr>
    	<td style="padding:35px 0px; background:#303f56;">
        	<table width="100%">
            	<tr>
                	<td colspan="2" style="font-size:16px; color:#FFF; padding:0 40px;">Hello,</td>
                </tr>
                <tr>
									<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong>Following are the leave requests on <b style="color:#03A9F4;"><?php echo date('d-M-Y'); ?></b></strong></p></td>
								</tr>
								<tr>
									<td colspan="2" style="padding:0 40px;">
									 <?php if(isset($leave) && count($leave)){ ?>
										<table cellspacing="0" cellpadding="0" width="100%" border="0" align="left">
										   <thead>
										      <tr>
										         <td style="color: #fff;font-size: 14px;">Employee</td>
										         <td style="color: #fff;font-size: 14px;">Type</td>
										         <td style="color: #fff;font-size: 14px;">Status</td>
										         <td style="color: #fff;font-size: 14px;">Dates</td>
										         <td style="color: #fff;font-size: 14px;">Approval</td>
										         <td style="color: #fff;font-size: 14px;">Reason</td>
										      </tr>
										   </thead>
										   <tbody>
										      <?php for($i=0; $i<count($leave); $i++) { ?>
                          <tr>
                            <td height="15"></td>
                          </tr>
										      <tr>
										         <td style="color: #fff;font-size: 14px;" width="20%">
										            <b><?php echo $leave[$i]->first_name; ?>  <?php echo $leave[$i]->last_name; ?> </b>&nbsp;
										         </td>
										         <td style="color: #fff;font-size: 14px;" width="12%">
										            <?php echo $leave[$i]->ltype; ?> &nbsp;
										         </td>
										         <td style="<?php if($leave[$i]->lstatus =="Pending" || $leave[$i]->lstatus =="Rejected" ){ echo 'color: #F00;';} else { echo 'color: #fff;';}?>font-size: 14px;" width="15%">
										            <?php echo $leave[$i]->lstatus; ?> &nbsp;
										         </td>
										         <td style="color: #fff;font-size: 14px;" width="23%">
										            <?php echo date('d-M',strtotime($leave[$i]->leave_start_date)); ?> to <?php echo date('d-M',strtotime($leave[$i]->leave_end_date)); ?> <br>
										         </td>
										         <td style="color: #fff;font-size: 14px;" width="15%">
										            <b><?php if($leave[$i]->approved_byFirstName != ''){echo "(".$leave[$i]->approved_byFirstName.")";}?></b>
										         </td>
										         <td style="color: #fff;font-size: 12px;" width="15%">
										            <?php echo $leave[$i]->leave_reason; ?>
										         </td>
										      </tr>
										      <?php } ?>
										   </tbody>
										</table>
										<?php } else { ?>
										<b style="color:#03A9F4;">All present today :)</b></strong>
										<?php } ?>
									</td>
					      </tr>
								<tr>
                	<td style="font-size:16px; line-height:20px; color:#FFF; font-weight:normal; padding:50px 0 0 40px;">
                    	Thank you,<br>
											<strong style="color:#ffac3e;">HR Department</strong>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="font-size:11px; color:#FFF; font-weight:normal; text-transform:uppercase; text-align:center; padding:14px 0; "> &#9400; 2017 HRMS, ALL RIGHTS RESERVED</td>
    </tr>
</table>
</body>
</html>
