<?php
class Absent_model extends CI_Model
{

  function getAllAbsentLeaves($flag =NULL){
    $dateToday = date('Y-m-d');

    /*if($flag){
      $dateAfter = date('Y-m-d',strtotime('+1 days', strtotime($dateToday)));
    } else{
      $dateAfter = $dateToday;
    } 
    $this->db->select('u.first_name, u.last_name, d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,au.first_name as approved_byFirstName,au.last_name as approved_byLastName,l.*');
    $this->db->from('leaves as l');
    $this->db->join('users as u','u.user_id = l.user_id', 'left');
    $this->db->join('users as au','au.user_id = l.leave_approved_by', 'left');
    $this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
    $this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
    $this->db->join('department as d','d.department_id = u.department_id', 'left');
    $this->db->join('users as up','up.user_id = l.leave_approved_by', 'left');
    $this->db->order_by("d.department_id","desc");*/
    //$this->db->where("l.leave_end_date BETWEEN '2016-03-18' AND '2016-03-31'");
    $query = $this->db->query("SELECT u.first_name, u.last_name, d.department_name, ls.leave_status as lstatus, lt.leave_type as ltype, au.first_name as approved_byFirstName, au.last_name as approved_byLastName, l.* FROM crm_leaves as l LEFT JOIN crm_users as u ON u.user_id = l.user_id LEFT JOIN crm_users as au ON au.user_id = l.leave_approved_by LEFT JOIN crm_leave_status as ls ON ls.leave_status_id = l.leave_status LEFT JOIN crm_leave_type as lt ON lt.leave_type_id = l.leave_type LEFT JOIN crm_department as d ON d.department_id = u.department_id LEFT JOIN crm_users as up ON up.user_id = l.leave_approved_by WHERE '$dateToday' BETWEEN l.leave_start_date AND l.leave_end_date ORDER BY d.department_id desc");
    return $query->result();
  }
}

?>
