<?php
class User_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	public function getUserinfo($id)
	{
		$this->db->select('d.department_name,ro.role_name,r.religion_name,cc.short_name,s.state_name,ci.city_name,u.*');
		$this->db->from('users as u');
		$this->db->join('country as cc','cc.country_id = u.country_id', 'left');
		$this->db->join('state as s','s.state_id = u.state_id', 'left');
		$this->db->join('city as ci','ci.city_id = u.city_id', 'left');
		$this->db->join('religion as r','r.religion_id = u.religion_id', 'left');
		$this->db->join('role as ro','ro.role_id = u.role_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('u.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function geteditstate($id)
	{
		$this->db->select('s.*');
		$this->db->from('state as s');
		$this->db->join('users as c','s.country_id = c.country_id', 'left');
		$this->db->where('c.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function geteditcity($id)
	{
		$this->db->select('cc.*');
		$this->db->from('city as cc');
		$this->db->join('users as c','c.state_id = cc.state_id', 'left');
		$this->db->where('c.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	public function updateUser($addData)
	{
		$this->db->where('user_id',$addData['user_id']);
		if($this->db->update('users',$addData)){
			return true;
		}else{
			return false;
		}
	}
	function getState_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('state');
		$this->db->where('country_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		$dd = "<option value=''>Select State</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->state_id.">".$row->state_name."</option>";
        }
		return $dd;
	}
	function getCity_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('city');
		$this->db->where('state_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		//$dd = "<option value=''>Select City</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->city_id.">".$row->city_name."</option>";
        }
		return $dd;
	}


	function getSavedProperties($limit, $start){
		$id=$this->session->userdata('login_session');
 		$data=$id[0]->iid;

		if($limit != '' && ($start != '' || $start == 0)){
			$this->db->limit($limit, $start);
		}
		$this->db->order_by("pro_id", "desc");
		$this->db->select('*,property_details.property_id as pro_id');
		$this->db->from('property_details');
		$this->db->where('property_details.pro_status',"1");
		$this->db->join('user','user.iid= property_details.pro_agent_id', 'left');
		$this->db->join('property_save','property_details.property_id = property_save.property_id');
		$this->db->where('property_save.user_id',$data);

		$query = $this->db->get();
		return $query->result();
	}
	function getcontactProperties($id){
		$this->db->where('property_id',$id);
		$query = $this->db->get('property_details');
		return $query->result();
	}

	public function getDocument($id){
		$this->db->where('user_id',$id);
		$query = $this->db->get('crm_user_document');
		return $query->result();
	}
	public function addDocument($data){
		$this->db->insert('crm_user_document',$data);
		return $insert_id = $this->db->insert_id();
	}
	public function deleteDocument($document_id){
		$this->db->where('document_id',$document_id);
		$query = $this->db->delete('crm_user_document');
	}


	public function getSkill(){
		$query = $this->db->get('crm_skill');
		return $query->result();
	}
	public function getAllSkills($id){
		$this->db->select('us.*,t.name');
		$this->db->from('crm_users_skill as us');
		$this->db->join('crm_skill as t','t.skill_id= us.skill_id', 'left');
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	public function getSkillDetail($skillid){
		$this->db->select('*');
		$this->db->from('crm_users_skill');
		$this->db->where('uskill_id',$skillid);
		$query = $this->db->get();
		return $query->row();
	}
	public function addSkill($data){
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('skill_id',$data['skill_id']);
		$query = $this->db->get('users_skill');
		$result = $query->row_array();

		$this->db->select('count(*) as count');
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('skill_id',$data['skill_id']);
		$countQuery = $this->db->get('users_skill');
		$count = $countQuery->row();
		if($count->count == 0){
			$this->db->insert('users_skill',$data);
			$insert_id = $this->db->insert_id();
			return 1;
		}else{
			$this->db->where('user_id',$data['user_id']);
			$this->db->where('skill_id',$result['skill_id']);
			$this->db->update('users_skill',$data);
			return 2;
		}
	}
	public function deleteSkill($uskill_id){
		$this->db->where('uskill_id',$uskill_id);
		$query = $this->db->delete('crm_users_skill');
	}
	public function updateImage($data){
		$this->db->set('user_pic',$data['mainImageName']);
		$this->db->where('user_id',$data['user_id']);
		if($this->db->update('crm_users')){
			return true;
		}else{
			return false;
		}
	}
	function getAllUserDetails()
		{
			$this->db->select('u.first_name, u.last_name, u.email, u.user_pic, u.ex_year, u.ex_month, d.department_name, r.role_name');
			$this->db->from('crm_users as u');
			$this->db->join('crm_department as d','u.department_id = d.department_id');
			$this->db->join('crm_role as r','u.role_id = r.role_id');
			$this->db->where('u.status','Active');
			$this->db->order_by('u.first_name');
			$query = $this->db->get();
			return $query->result();
		}

}
?>
