<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <?php echo $this->load->view("left_panel");?>
      <aside class="profile-info col-lg-9">
        <section class="panel">
          <div class="panel-body bio-graph-info">
            <h1>Skill Information</h1>

            <div class="alert alert-block alert-danger fade in" style="display:none">
               <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
               <p></p>
            </div>
            <div class="alert alert-success fade in" style="display:none">
               <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
               <p></p>
            </div>

            <div class="panel-body">
               <div class="form">
                  <?php
                  $form_attributes = array('name' => 'addEditPolicies', 'id' => 'addEditPolicies', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
                 echo form_open_multipart(base_url().'user/skills',$form_attributes);
                     ?>
                     <div class="form-group">
                       <div class="col-lg-3">
                        <label for="skill">Skill</label>
                         <select name="skill" id="skill" class="form-control selectpicker js-example-basic-single select2-hidden-accessible" required>
                           <option value="">Select Skill</option>
                          <?php foreach ($skills as $skill) {?>
                            <option value="<?php echo $skill->skill_id;?>" <?php if(isset($skillsdetail->skill_id) && $skillsdetail->skill_id == $skill->skill_id){echo "selected=selected";}?>><?php echo $skill->name;?></option>
                          <?php }?>
                         </select>
                       </div>
                        <div class="col-lg-3">
                         <label for="ex_year">Year</label>
                          <select name="ex_year" id="ex_year" class="form-control" required>
                           <?php for ($i=0; $i < 30; $i++) { ?>
                             <option value="<?php echo $i;?>" <?php if(isset($skillsdetail->ex_year) && $skillsdetail->ex_year == $i){echo "selected=selected";}?>><?php echo $i;?> Year</option>
                           <?php }?>
                          </select>
                        </div>
                        <div class="col-lg-3">
                         <label for="ex_month">Month</label>
                          <select name="ex_month" id="ex_month" class="form-control" required>
                            <?php for ($i=0; $i <= 11; $i++) { ?>
                               <option value="<?php echo $i;?>" <?php if(isset($skillsdetail->ex_month) && $skillsdetail->ex_month == $i){echo "selected=selected";}?>><?php echo $i;?> Month</option>
                            <?php }?>
                          </select>
                        </div>
                        <div class="col-lg-2">
                          <label for="ex_month">&nbsp;</label>
                           <button class="btn btn-danger form-control addSkillBtn" type="button" name="addSkillBtn">Submit</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
          </div>
        </section>
        <section class="panel">
          <div class="panel-body bio-graph-info">
            <div class="row">
                <div class="col-lg-12">
                  <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="fa fa-bullhorn"></i> Skill</th>
                        <th>Year</th>
                        <th>Month</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($skillsdata as $skilldata) { ?>
                        <tr>
                          <td><?php echo $skilldata->name;?></td>
                          <td><?php echo $skilldata->ex_year;?> Year</td>
                          <td><?php echo $skilldata->ex_month;?> Month</td>
                          <td width="30%">
                            <button type="button" onclick="delete_skill(<?php echo $skilldata->uskill_id; ?>)" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </button>
                            <a href="<?php echo base_url();?>user/skills/<?php echo $skilldata->uskill_id;?>" class="btn btn-success"><i class="fa fa-edit"></i> Edit </a>
                          </td>
                        </tr>
                      <?php }?>
                    </tbody>
                </table>
                </div>
            </div>
          </div>
        </section>
      </aside>
    </div>
  </section>
</section>
<script>
$(".addSkillBtn").on("click",function(){
  var skill = $('#skill option:selected').val();
  var ex_year = $('#ex_year option:selected').val();
  var ex_month = $('#ex_month option:selected').val();
  $.ajax({
     url: '<?php echo base_url(); ?>user/addSkill',
     data: 'skill='+skill+'&ex_year='+ex_year+'&ex_month='+ex_month,
     dataType: 'html',
     type: 'POST',
     success: function(data){
      if(data == 1){
        $(".alert-success").show();
        $(".alert-danger").hide();
        $(".alert-success p").html("Skill added successfully.");
        location.reload();
      }else if(data == 2){
        $(".alert-success").show();
        $(".alert-danger").hide();
        $(".alert-success p").html("Skill updated successfully.");
        location.reload();
      }else{
        $(".alert-success").hide();
        $(".alert-danger").show();
        $(".alert-danger p").html("Something went wrong. Please try again");
      }
    }
  });
});

function delete_skill(uskill_id){
	var uskill_id = uskill_id;
  var r = confirm("Are you sure delete this skill?");
	if (r == true){
  	$.ajax({
  	 url: '<?php echo base_url(); ?>user/deleteSkill',
  	 data: { uskill_id: uskill_id},
  	 dataType: 'html',
  	 type: 'POST',
  	 success: function(data){
       $(".alert-success").show();
       $(".alert-danger").hide();
       $(".alert-success p").html("Skill deleted successfully!");
       location.reload();
  	 }
  	});
  }
}
</script>
