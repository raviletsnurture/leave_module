<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <?php echo $this->load->view("left_panel");?>
      <aside class="profile-info col-lg-9">
        <section class="panel">
          <div class="panel-body bio-graph-info">
            <h1>Document Information</h1>
            <?php
            if($this->session->flashdata('error')){?>
            <div class="alert alert-block alert-danger fade in">
               <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
               <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('success')){?>
            <div class="alert alert-success fade in">
               <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
               <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
            </div>
            <?php }?>

            <div class="panel-body">
               <div class="form">
                  <?php
                     $form_attributes = array('name' => 'addEditPolicies', 'id' => 'addEditPolicies', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
                     echo form_open_multipart(base_url().'user/uploaddocument',$form_attributes);
                     ?>
                  <div class="form-group ">
                     <label for="firstName" class="control-label col-lg-2">Document Name <span class="red">*</span></label>
                     <div class="col-lg-10">
                        <input class="form-control" id="document_name" name="document_name" type="text" maxlength="250" required/>
                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-2 control-label">Document file <span class="red">*</span></label>
                    <div class="col-lg-6">
                      <input type="file" class="file-pos" name="documentFile" id="documentFile" required>
                    </div>
                  </div>
                  <div class="form-group">
                     <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-danger" type="submit" name="addDocumentBtn">Submit</button>
                     </div>
                  </div>
                  </form>
               </div>
            </div>
          </div>
        </section>
        <section class="panel">
          <div class="panel-body bio-graph-info">
            <div class="row">
                <div class="col-lg-12">
                  <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="fa fa-bullhorn"></i> Document</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($documentDatas as $documentData) { ?>
                        <tr>
                          <td><?php echo $documentData->document_name;?></td>
                          <td width="30%">
                            <a href="<?php echo base_url(); ?>uploads/document/<?php echo $documentData->file_name;?>" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i> View </a>
                            <button type="button" onclick="delete_document(<?php echo $documentData->document_id; ?>)" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </button>
                          </td>
                        </tr>
                      <?php }?>
                    </tbody>
                </table>
                </div>
            </div>
          </div>
        </section>
      </aside>
    </div>
  </section>
</section>
<script>
function delete_document(document_id){
	var document_id = document_id;
  var r = confirm("Are you sure delete this document?");
	if (r == true){
  	$.ajax({
  	 url: '<?php echo base_url(); ?>user/deleteDocument',
  	 data: { document_id: document_id},
  	 dataType: 'html',
  	 type: 'POST',
  	 success: function(data){
  		 alert("Document delete successfully!");
  		 location.reload();
  		 }
  	});
  }
}
</script>
