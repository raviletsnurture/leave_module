<?php
$country = getAllCountry();
$religion = getAllReligion();
$department = getAllDepartment();
$role = getAllRoles();?>
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->

    <div class="row">
      <?php echo $this->load->view("left_panel");?>
      <aside class="profile-info col-lg-9">
        <section class="panel">
          <div class="bio-graph-heading bio-graph-heading-2"></div>
          <div class="panel-body bio-graph-info">

			<?php if($this->session->flashdata('error')){?>
            <div class="alert alert-block alert-danger fade in">
              <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
              <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
            <?php } ?>
            <?php if($this->session->flashdata('success')){?>
            <div class="alert alert-success fade in">
              <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
              <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
            <?php }?>

			<h1> User Profile Edit</h1>
            <form class="commonForm form-horizontal cmxform" action="<?php echo $url;?>" role="form" id="addUsers" name="addUsers" method="post" enctype="multipart/form-data">
			  
			  <div class="form-group ">
                <label for="role_id" class="control-label col-lg-2">Role <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="role_id" id="role_id" disabled="disabled" class="form-control m-bot15">
				    <option value="">Select Role</option>
                  	<?php foreach($role as $rr){ ?>
						<option value="<?php echo $rr->role_id; ?>" <?php $value1 = (isset($userdata[0]->role_id)) ? $userdata[0]->role_id : 0; echo selectedVal($value1,$rr->role_id)?>> <?php echo $rr->role_name; ?></option>-->
                    <?php } ?>
                  </select>
                </div>
              </div>


			  <div class="form-group ">
                <label for="department_id" class="control-label col-lg-2">Department<span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="department_id" id="department_id" disabled="disabled" class="form-control">
                    <option value="">Select Department</option>
                    <?php foreach($department as $row1){?>
                    <option value="<?php echo $row1->department_id;?>" <?php $value1 = (isset($userdata[0]->department_id)) ? $userdata[0]->department_id : 0; echo selectedVal($value1,$row1->department_id)?>><?php echo $row1->department_name;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="firstname" class="control-label col-lg-2">First Name <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="first_name" maxlength="25" name="first_name" type="text"  value="<?php if(isset($userdata[0]->first_name)){ echo $userdata[0]->first_name;} ?>"/>
                </div>
              </div>


              <div class="form-group">
                <label for="middlename" class="control-label col-lg-2">Middle Name</label>
                <div class="col-lg-10">
                  <input class="form-control" id="middle_name" maxlength="25" name="middle_name" type="text"  value="<?php if(isset($userdata[0]->middle_name)){ echo $userdata[0]->middle_name;} ?>"/>
                </div>
              </div>

              <div class="form-group">
                <label for="lastname" class="control-label col-lg-2">Last Name <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="last_name" maxlength="25" name="last_name" type="text"  value="<?php if(isset($userdata[0]->last_name)){ echo $userdata[0]->last_name;} ?>"/>
                </div>
              </div>



              <div class="form-group ">
                <label for="countryid" class="control-label col-lg-2">Country <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="country_id" id="country_id" class="form-control m-bot15">
                  	<?php foreach($country as $row2){ ?>
                    <option value="<?php echo $row2->country_id; ?>" <?php $value1 = (isset($userdata[0]->country_id)) ? $userdata[0]->country_id : 0; echo selectedVal($value1,$row2->country_id)?>> <?php echo $row2->short_name; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group ">
                <label for="state_id" class="control-label col-lg-2">State <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="state_id" id="state_id" class="form-control">
                    <option value="">Select State</option>
					<?php if(isset($userdata[0]->state_id)) { ?>
						<?php foreach($state as $dd){?>
						<option value="<?php echo $dd->state_id;?>" <?php $value1 = (isset($userdata[0]->state_id)) ? $userdata[0]->state_id : 0; echo selectedVal($value1,$dd->state_id)?>><?php echo $dd->state_name;?></option>
						<?php } ?>
					<?php } ?>
                  </select>
                </div>
              </div>




               <div class="form-group ">
                <label for="state_id" class="control-label col-lg-2">City <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="city_id" id="city_id" class="form-control">
                    <option value="">Select City</option>
					<?php if(isset($userdata[0]->city_id)) { ?>
						<?php foreach($city as $dd1){?>
						<option value="<?php echo $dd1->city_id;?>" <?php $value1 = (isset($userdata[0]->city_id)) ? $userdata[0]->city_id : 0; echo selectedVal($value1,$dd1->city_id)?>><?php echo $dd1->city_name;?></option>
						<?php } ?>
					<?php } ?>
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label for="street" class="control-label col-lg-2">Street <span class="red">*</span></label>
                <div class="col-lg-10">
                  <textarea class="form-control" id="street" name="street" rows="3" cols="40"><?php if(isset($userdata[0]->street)){ echo $userdata[0]->street;} ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="zipcode" class="control-label col-lg-2">Zipcode <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="zipcode" maxlength="10" name="zipcode" type="text"  value="<?php if(isset($userdata[0]->zipcode)){ echo $userdata[0]->zipcode;} ?>"/>
                </div>
              </div>
              <div class="form-group ">
                <label for="gender" class="control-label col-lg-2">Gender <span class="red">*</span></label>
                <div class="col-lg-6">
                  <select name="gender" id="gender" class="form-control">
                    <option value="">Select Gender</option>
                    <option value="Male" <?php echo isset($userdata[0]->gender)? selectedVal($userdata[0]->gender,"Male"): '';?> >Male</option>
                    <option value="Female" <?php echo isset($userdata[0]->gender)? selectedVal($userdata[0]->gender,"Female"): '';?>>Female</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label  class="col-lg-2 control-label">Birthday <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" readonly="readonly" id="birthdate" name="birthdate" data-date-format="yyyy-mm-dd" placeholder=" "  value="<?php  if(isset($userdata[0]->birthdate)){ echo $userdata[0]->birthdate;} ?>">
                </div>
              </div>


              <div class="form-group">
                <label for="mobile" class="control-label col-lg-2">Mobile Number </label>
                <div class="col-lg-10">
                  <input class="form-control" id="mobile" maxlength="10" name="mobile" type="text"  value="<?php if(isset($userdata[0]->mobile)){ echo $userdata[0]->mobile;} ?>"/>
                </div>
              </div>

              <div class="form-group">
                <label for="landline" class="control-label col-lg-2">Landline Number</label>
                <div class="col-lg-10">
                  <input class="form-control" id="landline" maxlength="10" name="landline" type="text"  value="<?php if(isset($userdata[0]->landline)){ echo $userdata[0]->landline;} ?>"/>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="control-label col-lg-2">Personal Email <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="email" name="email" type="text"  value="<?php if(isset($userdata[0]->email)){ echo $userdata[0]->email;} ?>"/>
                </div>
              </div>
			  <div class="form-group">
                <label for="alternate_email" class="control-label col-lg-2">Alternate Email </label>
                <div class="col-lg-10">
                  <input class="form-control" id="alternate_email" name="alternate_email" type="text"  value="<?php if(isset($userdata[0]->alternate_email)){ echo $userdata[0]->alternate_email;} ?>"/>
                </div>
              </div>

			  <div class="form-group ">
                <label for="religion_id" class="control-label col-lg-2">Religion<span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="religion_id" id="religion_id" class="form-control">
                    <option value="">Select Religion</option>
                    <?php foreach($religion as $row){?>
                    <option value="<?php echo $row->religion_id;?>" <?php $value1 = (isset($userdata[0]->religion_id)) ? $userdata[0]->religion_id : 0; echo selectedVal($value1,$row->religion_id)?>><?php echo $row->religion_name;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

			  <!-----------------------------social media ------------------------>
			  <div class="form-group">
                <label for="facebook_id" class="control-label col-lg-2">Skype Id </label>
                <div class="col-lg-10">
                  <input class="form-control" id="skype" maxlength="20" name="skype" type="text"  value="<?php if(isset($userdata[0]->skype)){ echo $userdata[0]->skype;} ?>"/>
                </div>
              </div>
			  <div class="form-group">
                <label for="facebook_page" class="control-label col-lg-2">Gtalk</label>
                <div class="col-lg-10">
                  <input class="form-control" id="gtalk" maxlength="20" name="gtalk" type="text"  value="<?php if(isset($userdata[0]->gtalk)){ echo $userdata[0]->gtalk;} ?>"/>
                </div>
              </div>
			  <div class="form-group">
                <label for="twitter_id" class="control-label col-lg-2">Pancard Number </label>
                <div class="col-lg-10">
                  <input class="form-control" id="pancard_no" maxlength="20" name="pancard_no" type="text"  value="<?php if(isset($userdata[0]->pancard_no)){ echo $userdata[0]->pancard_no;} ?>"/>
                </div>
              </div>
			  <div class="form-group">
                <label for="pf_no" class="control-label col-lg-2">PF Number </label>
                <div class="col-lg-10">
                  <input class="form-control" id="pf_no" maxlength="20" name="pf_no" type="text"  value="<?php if(isset($userdata[0]->pf_no)){ echo $userdata[0]->pf_no;} ?>"/>
                </div>
              </div>


              <!--<div class="form-group">
                <label  class="col-lg-2 control-label">Change Avatar</label>
                <div class="col-lg-6">
                  <input type="file" class="file-pos" name="distImage" id="distImage">
                </div>
              </div>-->



              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <input type="submit" name="update" class="btn btn-success" value="Update">
                  <button type="button" class="btn btn-default" onclick="window.location='<?php echo base_url();?>user'">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </section>


        <section>
          <div class="panel panel-primary">
            <div class="panel-heading" id="focusPass"> Set New Password</div>

            <div class="panel-body">

			  <?php if($this->session->flashdata('errorPass')){?>
              <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('errorPass');?> </div>
              <?php } ?>
              <?php if($this->session->flashdata('successPass')){?>
              <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('successPass');?> </div>
              <?php }?>

			  <form class="commonForm form-horizontal cmxform" role="form" enctype="multipart/form-data" method="post" name="updatePass" id="updatePass">
                <div class="form-group">
                  <label  class="col-lg-2 control-label">Current Password <span class="red">*</span></label>
                  <div class="col-lg-6">
                    <input type="password" class="form-control" id="currPassword" name="currPassword" placeholder=" ">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-lg-2 control-label">New Password <span class="red">*</span></label>
                  <div class="col-lg-6">
                    <input type="password" class="form-control" id="password1" name="password1" placeholder=" ">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-lg-2 control-label">Re-type New Password <span class="red">*</span></label>
                  <div class="col-lg-6">
                    <input type="password" class="form-control" id="repassword" name="repassword" placeholder=" ">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                  	<input type="submit" name="updatePassword" class="btn btn-info" value="Update">
                    <button type="button" class="btn btn-default" onclick="window.location='<?php echo base_url();?>user'">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </aside>
    </div>
    <!-- page end-->
  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-users.js" ></script>
<script>
$('#country_id').change(function(){

    var country_id = $('#country_id').val();

    $.ajax({
            url: '<?php echo base_url();?>user/getStates',
            type: 'POST',
            data: {
                country_id:country_id,
            },
            success:function(data){
                //alert(data);
				//var obj = JSON.parse(data);
                //alert(obj.yourfield);
				$('#state_id').html(data);
            },
            error:function(){
                alert('Data not Found')
            }


        });

 });
 $('#state_id').change(function(){

    var state_id = $('#state_id').val();

    $.ajax({
            url: '<?php echo base_url();?>user/getCities',
            type: 'POST',
            data: {
                state_id:state_id,
            },
            success:function(data){
                //alert(data);
				//var obj = JSON.parse(data);
                //alert(obj.yourfield);
				$('#city_id').html(data);
            },
            error:function(){
                alert('Data not Found')
            }


        });

 });
</script>
