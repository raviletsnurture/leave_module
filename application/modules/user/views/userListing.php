<style>
.margin{
    margin-bottom:30px;
}
</style>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
            <?php echo $this->load->view("left_panel");?>
            <aside class="profile-nav col-lg-9">
                <section class="panel">
                    <div class="panel-body bio-graph-info">
                        <h1>Employees</h1>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body bio-graph-info">
                        <div class="row">
                            <?php             
                            foreach($allUsers as $allUsers){                            
                                ?>                                
                                <div class="col-md-4 col-sm-6">
                                    <div class="user-heading round margin">
                                    <a  id="upload_link">
                                        <?php if(!empty($allUsers->user_pic) || $allUsers->user_pic != ''){?>
                                            <img name="userProfile" class="userImage" src="<?php echo base_url().'/uploads/userProfileImage/'.$allUsers->user_pic; ?>" height="100" alt=""></td>              </tr>
                                        <?php }else{ ?>
                                            <img  class="userImage" src="<?php echo base_url().'assets/img/avatar.jpg' ?>" height="100" alt=""></td>              </tr>
                                        <?php }?>
                                    </a>
                                    <h5><b><?php echo $allUsers->first_name.' '.$allUsers->last_name?></b></h5>
                                    <p><?php echo $allUsers->email?></p>
                                    <p><b>Role : </b><?php echo $allUsers->role_name;?></p>
                                    <p><b>Department : </b><?php echo $allUsers->department_name;?></p>
                                    <p><b>Experience : </b><?php echo $allUsers->ex_year.'.'.$allUsers->ex_month.' years';?></p>
                                    </div>
                                </div>                                        
                           <?php }?>                                   
                        </div>
                    </div>
                </section>
            </aside>
    </div>
  </section>
</section>
  