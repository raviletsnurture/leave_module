<?php
$uri1 = $this->uri->segment(1);
$uri2 = $this->uri->segment(2);
$uri3 = $this->uri->segment(3);
?>
<aside class="profile-nav col-lg-3">
  <section class="panel">
    <div class="user-heading round">

        <form enctype="multipart/form-data" method="POST" id="submitProfile"  action="<?php echo base_url().'user/changeImage' ?>">
            <input name="profileImage" id="uploadimage" type="file" accept="image/jpeg, image/png, image/jpg, image/JPEG, image/PNG, image/JPEG"/>
              <a title="change your profile picture here" id="upload_link" href="<?php echo base_url();?>user/profile">
                  <?php if(!empty($userdata[0]->user_pic) || $userdata[0]->user_pic != ''){?>
                      <img name="userProfile" class="userImage" src="<?php echo base_url().'/uploads/userProfileImage/'.$userdata[0]->user_pic; ?>" height="100" alt=""></td>              </tr>
                  <?php }else{ ?>
                      <img  class="userImage" src="<?php echo base_url().'assets/img/avatar.jpg' ?>" height="100" alt=""></td>              </tr>
                  <?php }?>
              </a>
          </form>  
          <h1><?php echo $userdata[0]->first_name . ' ' . $userdata[0]->last_name;?></h1>
          <p><?php echo $userdata[0]->email;?></p>
    </div>
    <ul class="nav nav-pills nav-stacked">
      <li class="<?php echo $uri2 == 'profile' && ($uri3 != 'edit') ? 'active' : '';?>">
        <a href="<?php echo base_url();?>user/profile"> <i class="fa fa-user"></i> Profile</a>
      </li>
      <li class="<?php echo $uri2 == 'uploaddocument' && ($uri3 != 'uploaddocument') ? 'active' : '';?>">
        <a href="<?php echo base_url();?>user/uploaddocument"> <i class="fa fa-edit"></i>Document</a>
      </li>
      <li class="<?php echo $uri2 == 'skills' && ($uri3 != 'uploaddocument') ? 'active' : '';?>">
        <a href="<?php echo base_url();?>user/skills"> <i class="fa fa-user"></i> Skill</a>
      </li>
      <li class="<?php echo $uri2 == 'users' && ($uri3 != 'uploaddocument') ? 'active' : '';?>">
        <a href="<?php echo base_url();?>user/employees"> <i class="fa fa-user"></i> Employees</a>
      </li>
    </ul>
  </section>
</aside>
<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
<!-- <script src="<?php echo base_url();?>assets/js/additional-methods.min.js"></script> -->
<script>

  $(document).ready( function(){        
          $("#uploadimage").on('change', function (e) {              
              var imageName = $('#uploadimage').val().split('\\').pop();            
              if(imageName.match(/jpg.*/)||imageName.match(/jpeg.*/)||imageName.match(/png.*/)||imageName.match(/JPEG.*/)||imageName.match(/PNG.*/)||imageName.match(/JPG.*/)){

                  var r = confirm("Are you sure you want to change your profile picture");
                  if (r == true){
                    $( "#submitProfile" ).submit();
                  }
              }else{                  
                e.preventDefault();
                window.alert("Invalid image");
                }
          });


// POP up the input field
          $(function(){
              $("#upload_link").on('click', function(e){
                  e.preventDefault();
                  $("#uploadimage:hidden").trigger('click');
              });
          });
  });
</script>