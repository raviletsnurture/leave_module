<?php $uri2 = $this->uri->segment(3);?>

<section id="main-content">
    <section class="wrapper">
    <?php /*if($uri2){echo*/ //$this->load->view('user/head_box.php');/*}*/?>
    <!-- page start-->

<!-- Validation message -->
<?php if($this->session->flashdata('error')){?>
      <div class="alert alert-block alert-danger fade in">
          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
          <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
      </div>
  <?php } ?>

  <?php if($this->session->flashdata('success')){?>
      <div class="alert alert-success fade in">
          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
          <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
      </div>
  <?php }?>
    <div class="row">
      <?php echo $this->load->view("left_panel", $userdata);?>
      <?php //send_mail("ravi@letsnurture.com", "hello this", "this is a test message");?>

      <aside class="profile-info col-lg-9">
        <section class="panel">

          <div class="bio-graph-heading bio-graph-heading-2"></div>
          <div class="panel-body bio-graph-info">

		    <span style="width:100%; float:left; margin-bottom:10px; color:#FE4545;">If you want to make changes in your profile send  REQUEST to HR. <input type="submit" style="margin-bottom: 15px; float: right;" onclick="chk_profile(<?php echo $userdata[0]->user_id; ?>)" name="updatePassword" class="btn btn-info" value="Update Request"></span>


            <h1>Detail Information</h1>
            <div class="row">
              <div class="bio-row">
                <h5><b>First Name </b>: <?php echo $userdata[0]->first_name;?></h5>
              </div>
			  <div class="bio-row">
                <h5><b>Middle Name </b>: <?php echo $userdata[0]->middle_name;?></h5>
              </div>
              <div class="bio-row">
                <h5><b>Last Name </b>: <?php echo $userdata[0]->last_name;?></h5>
              </div>


              <div class="bio-row">
                <h5><b>Gender </b>: <?php echo $userdata[0]->gender;?></h5>
              </div>
              <div class="bio-row">
                <h5><b>Birthday</b>: <?php echo toBirthDate($userdata[0]->birthdate);?></h5>
              </div>
              <div class="bio-row">
                      <h5><b>Anniversary </b>: <?php echo $userdata[0]->anniversary; ?></h5>
                    </div>
              <div class="bio-row">
                <h5><b>Mobile</b>: <?php echo base64_decode($userdata[0]->mobile);?></h5>
              </div>
			   <div class="bio-row">
                <h5><b>Landline </b>: <?php echo $userdata[0]->landline;?></h5>
              </div>


			   <div class="bio-row">
                <h5><b>Religion </b>: <?php echo $userdata[0]->religion_name;?></h5>
              </div>
                <div class="bio-row">
                      <h5><b>Blood Group </b>: <?php echo $userdata[0]->blood_group; ?></h5>
                </div>

                <div class="bio-row">
                      <h5><b>Employee Insurance Number </b>: <?php echo (isset($userdata[0]->employee_insurance_number) && $userdata[0]->employee_insurance_number != '')?base64_decode($userdata[0]->employee_insurance_number):'' ; ?></h5>
                </div>
                <div class="bio-row">
                      <h5><b>Aadhaar Number </b>: <?php echo (isset($userdata[0]->aadhar_no) && $userdata[0]->aadhar_no != '')?base64_decode($userdata[0]->aadhar_no):'' ; ?></h5>
                </div>
             <div class="bio-row">
                      <h5><b>License </b>: <?php echo base64_decode($userdata[0]->license); ?></h5>
                    </div>
                    <div class="bio-row">
                     <h5><b>Street </b>: <?php echo $userdata[0]->street;?></h5>
                   </div>
              <div class="bio-row">
                     <h5><b>City </b>: <?php echo $userdata[0]->city_name;?></h5>
                   </div>
                   <div class="bio-row">
                          <h5><b>State </b>: <?php echo $userdata[0]->state_name;?></h5>
                        </div>
			   <div class="bio-row">
                <h5><b>Country </b>: <?php echo $userdata[0]->short_name;?></h5>
              </div>



			   <div class="bio-row">
                <h5><b>Zipcode </b>: <?php echo $userdata[0]->zipcode; ?></h5>
              </div>



			  <!-- <div class="bio-row">
                <h5><b>UDID </b>: <?php echo base64_decode($userdata[0]->gtalk);?></h5>
              </div> -->
              <div class="panel panel-primary">
                <div class="panel-heading" id="focusPass">Job Details</div>
             </div>
              <div class="bio-row">
                      <h5><b>Department </b>: <?php echo $userdata[0]->department_name;?></h5>
                    </div>

                    <div class="bio-row">
                      <h5><b>Role </b>: <?php echo $userdata[0]->role_name;?></h5>
                    </div>
                    <div class="bio-row">
                      <h5><b>Email </b>: <?php echo $userdata[0]->email;?></h5>
                    </div>
                    <div class="bio-row">
                      <h5><b>Offical Gmail </b>: <?php echo $userdata[0]->gmail_id;?></h5>
                    </div>
                     <div class="bio-row">
                      <h5><b>Alternate Email </b>: <?php echo $userdata[0]->alternate_email;?></h5>
                    </div>
                    <div class="bio-row">
                     <h5><b>Experience </b>: <?php echo $userdata[0]->ex_year;?><b>Year</b> <?php echo $userdata[0]->ex_month;?> <b>Month</b></h5>
                   </div>
                   <div class="bio-row">
                    <h5><b>Joining</b>: <?php if($userdata[0]->joining_date != '0000-00-00' && $userdata[0]->joining_date != NULL)echo date("d-m-Y",strtotime($userdata[0]->joining_date));?>
                  </div>
                  <div class="bio-row">
                   <h5><b>Reliving</b>: <?php if($userdata[0]->reliving_date != '0000-00-00' && $userdata[0]->reliving_date != NULL)echo date("d-m-Y",strtotime($userdata[0]->reliving_date));?>
                 </div>
              <div class="panel panel-primary">
                <div class="panel-heading" id="focusPass">Bank Details</div>
             </div>
			   <div class="bio-row">
                <h5><b>Bank Name </b>: <?php echo base64_decode($userdata[0]->bank_name);?></h5>
              </div>
			   <div class="bio-row">
                <h5><b>Account Number </b>: <?php echo base64_decode($userdata[0]->account_number);?></h5>
              </div>
			   <div class="bio-row">
                <h5><b>IFSC Code </b>: <?php echo base64_decode($userdata[0]->ifsc_code);?></h5>
              </div>
			   <div class="bio-row">
                <h5><b>Branch </b>: <?php echo base64_decode($userdata[0]->branch);?></h5>
              </div>

			   <div class="bio-row">
                <h5><b>Pancard Number </b>: <?php echo base64_decode($userdata[0]->pancard_no);?></h5>
              </div>
			  <div class="bio-row">
                <h5><b>PF Number </b>: <?php echo base64_decode($userdata[0]->pf_no);?></h5>
              </div>
              <div class="bio-row">
                      <h5><b>UAN No. </b>: <?php echo base64_decode($userdata[0]->skype);?></h5>
                    </div>
            <div class="panel panel-primary">
              <div class="panel-heading" id="focusPass">Reward Details</div>
            </div>
            <div class="bio-row">
                   <h5><b>Reward Points </b>: <?php echo number_format($userdata[0]->reward_points);?></h5>
                 </div>

                 <div class="bio-row">
                        <h5><b>Karma Points </b>: <?php echo number_format($userdata[0]->karma_points);?></h5>
                      </div>
            </div>
          </div>
        </section>
      </aside>
    </div>
    <!-- page end-->

    <?php //$this->load->view('user/chart.php');//}?>
  </section>
  </section>
  <?php ///pr($distChart);?>
<script>
function chk_profile(id)
{
	var id = id;
	var r = confirm("Are You Sure? Click on Yes.");

	if (r == true)
	{
		$.ajax({
		 url: '<?php echo base_url(); ?>user/updatemailrequest/',
		 data: { user_id: id},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 //$('.modal-body').html(data);
			 alert("Your update request is send successfully");
			 location.reload();
			 }
		});
	}
	else
	{
		//alert('no');
	}
}

</script>
