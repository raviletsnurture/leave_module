<!--Leads Overview-->
<div class="btn  btn-block chart-title">Leads Overview</div>
<div>&nbsp;</div>
<div class="row">
  <div class="col-lg-4">
    <section class="panel">
      <div id="lead-type-chart"></div>
    </section>
  </div>
  <div class="col-lg-4">
    <section class="panel">

      <div id="lead-chart"></div>
    </section>
  </div>
  <div class="col-lg-4">
    <section class="panel">
      <div id="lead-status-chart"></div>
    </section>
  </div>
</div>
<!--Actions Overview-->
<div class="btn  btn-block chart-title">Actions Overview</div>
<div>&nbsp;</div>
<div class="row">
  <div class="col-lg-4">
    <section class="panel">
      <div id="action-type-chart"></div>
    </section>
  </div>
  <div class="col-lg-4">
    <section class="panel">
      <div id="action-chart"></div>
    </section>
  </div>
  <div class="col-lg-4">
    <section class="panel">
      <div id="action-status-chart"></div>
    </section>
  </div>
</div>
<!--Actions Overview-->
<div class="btn  btn-block chart-title">Opportunity Overview</div>
<div>&nbsp;</div>
<div class="row">
  <div class="col-lg-4">
    <section class="panel">
      <div id="oppt-type-chart"></div>
    </section>
  </div>
  <div class="col-lg-4">
    <section class="panel">
      <div id="oppt-chart"></div>
    </section>
  </div>
  <div class="col-lg-4">
    <section class="panel">
      <div id="oppt-status-chart"></div>
    </section>
  </div>
</div>
<script src="<?php echo base_url();?>assets/js/high-chart/highcharts.js"></script> 
<script src="<?php echo base_url();?>assets/js/high-chart/exporting.js"></script>
<script type="text/javascript">
$(function () {
	var distData = [<?php foreach($distChart as $m=>$v){echo $v.",";}?> ];
	var distCategory = [<?php foreach($distChart as $m=>$v){echo "'".$m."',";}?> ];
	Highcharts.setOptions({
	 colors: ["#BEC2CD"]
    });
    $('#dist-chart-12').highcharts({
        chart: {
            type: 'column',
			height: 300,
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			}
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
		credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        xAxis: {
            categories: distCategory
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
		legend: {
			enabled : false
		},
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Distributor',
            data: distData 

        }]
    });
	
	var leadData = [<?php foreach($leadChart as $m=>$v){echo $v.",";}?> ];
	var leadCategory = [<?php foreach($leadChart as $m=>$v){echo "'".$m."',";}?> ];
	Highcharts.setOptions({
	 colors: ["#FFFFFF"]
    });
	$('#lead-chart').highcharts({
        chart: {
            type: 'column',
			height: 300,
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
			backgroundColor : "#A9D96C"
        },
        title: {
            text: 'TOTAL LEADS MONTHLY, LAST 12 MONTHS',
			align:'left',
			style:{
				 fontSize : "13.5px",
				 color : "#FFFFFF"
			}
        },
        subtitle: {
            text: ''
        },
		credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        xAxis: {
            categories: leadCategory,
			labels: {
                style: {
                    color: '#FFFFFF',
                     
                },
            },

        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
			labels: {
                style: {
                    color: '#FFFFFF',
                     
                },
            },
        },
		legend: {
			enabled : false
		},
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Leads',
            data: leadData 

        }]
    });	
	
	var leadTypeData = [<?php foreach($leadTypeChart as $k=>$v){echo "['".$k."',".$v."],";} ?>];
	Highcharts.setOptions({
	 colors: ['#ED4749','#54C0C0','#F7B45B','#95A0B2']
    });
	var leadTypechart = new Highcharts.Chart({	
        chart: {
			renderTo: 'lead-type-chart',
			height: 300,
            plotBackgroundColor: null,
            plotShadow: false,
			type: 'pie',
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			}
        },
        credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        title: {
            text: 'LEAD TYPES',
			align : "left",
			style:{
				 "fontSize": "13.5px"
			}
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {					 
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					distance:20,
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                } 
            }
        },
		tooltip: {
            headerFormat: '<span style="font-size:10px">Lead Type :<br> {point.key} :<b>{point.percentage:.1f}%</b> </span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">  </td>' +
                '<td style="padding:0"><b></b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
			enabled: false
        },
        series: [{    
            name: 'Lead Types',
			innerSize: '90%',
			size: "35%",
            data: leadTypeData,
			 
        }]
    });
	
	Highcharts.setOptions({
	 colors: ['#ED4749','#54C0C0','#F7B45B','#95A0B2',"#A9D96C"]
    });
	var leadStatusData  = [<?php foreach($leadStatusChart as $k=>$v){echo "['".str_replace(array(' ', '-'),'<br>',$k)."',".$v."],";} ?>];
	var leadStatuschart = new Highcharts.Chart({ 
        chart: {
			renderTo: 'lead-status-chart',
			height: 300,
            plotBackgroundColor: null,
            plotShadow: false,
			type: 'pie',
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
			 
        },
        credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        title: {
            text: 'LEAD STATUS',
			align : "left",
			style:{
				 "fontSize": "13.5px"
			}
        },
        subtitle: {
           // text: '3D donut in Highcharts'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {					 
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					distance:40,
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                } 
            }
        },
		tooltip: {
            headerFormat: '<span style="font-size:10px">Lead Status :<br> {point.key} :<b>{point.percentage:.1f}%</b> </span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">  </td>' +
                '<td style="padding:0"><b></b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
			enabled: false
        },
        series: [{    
            name: 'Lead Status',
			innerSize: '90%',
			size: "35%",
            data: leadStatusData,
			 
        }]
    });
	
	Highcharts.setOptions({
	 colors: ['#ED4749','#54C0C0','#F7B45B']
    });
	var actionTypeData  = [<?php foreach($actionTypeChart as $k=>$v){echo "['".str_replace(array(' ', '-'),'<br>',$k)."',".$v."],";} ?>];
	var actionTypechart = new Highcharts.Chart({ 
        chart: {
			renderTo: 'action-type-chart',
			height: 300,
            plotBackgroundColor: null,
            plotShadow: false,
			type: 'pie',
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
        },
        credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        title: {
            text: 'ACTION TYPE',
			align : "left",
			style:{
				 "fontSize": "13.5px"
			}
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {					 
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					distance:40,
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                } 
            }
        },
		tooltip: {
            headerFormat: '<span style="font-size:10px">Lead Status :<br> {point.key} :<b>{point.percentage:.1f}%</b> </span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">  </td>' +
                '<td style="padding:0"><b></b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
			enabled: false
        },
        series: [{    
            name: 'Lead Status',
			innerSize: '90%',
			size: "35%",
            data: actionTypeData,
			 
        }]
    });
	
	var actionStatusData  = [<?php foreach($actionStatusChart as $k=>$v){echo "['".str_replace(array(' ', '-'),'<br>',$k)."',".$v."],";} ?>];
	var actionStatuschart = new Highcharts.Chart({ 
        chart: {
			renderTo: 'action-status-chart',
			height: 300,
            plotBackgroundColor: null,
            plotShadow: false,
			type: 'pie',
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
        },
        credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        title: {
            text: 'ACTION STATUS',
			align : "left",
			style:{
				 "fontSize": "13.5px"
			}
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {                
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {					 
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					distance:40,
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                } 
            }
        },
		tooltip: {
            headerFormat: '<span style="font-size:10px">Lead Status :<br> {point.key} :<b>{point.percentage:.1f}%</b> </span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">  </td>' +
                '<td style="padding:0"><b></b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
			enabled: false
        },
        series: [{    
            name: 'Lead Status',
			innerSize: '90%',
			size: "35%",
            data: actionStatusData,
			 
        }]
    });
	
	var actionData = [<?php foreach($actionAllChart as $m=>$v){echo $v.",";}?> ];
	var actionCategory = [<?php foreach($actionAllChart as $m=>$v){echo "'".$m."',";}?> ];	
	Highcharts.setOptions({
	 colors: ["#FFFFFF"]
    });
	$('#action-chart').highcharts({
        chart: {
            type: 'column',
			height: 300,
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
			backgroundColor : "#A9D96C"
        },
        title: {
            text: 'TOTAL ACTIONS MONTHLY, LAST 12 MONTHS',
			align:'left',
			style:{
				 fontSize : "13px",
				 color : "#FFFFFF"
			}
        },       
		credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        xAxis: {
            categories: actionCategory,
			labels: {
                style: {
                    color: '#FFFFFF',
                },
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
			labels: {
                style: {
                    color: '#FFFFFF',

                },
            },
        },
		legend: {
			enabled : false
		},
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Actions',
            data: actionData 

        }]
    });
	
	Highcharts.setOptions({
	 colors: ['#ED4749','#54C0C0','#F7B45B','#95A0B2']
    });
	var opptTypeData  = [<?php foreach($opptTypeChart as $k=>$v){echo "['".str_replace(array(' ', '-'),'<br>',$k)."',".$v."],";} ?>];
	var opptTypechart = new Highcharts.Chart({ 
        chart: {
			renderTo: 'oppt-type-chart',
			height: 300,
            plotBackgroundColor: null,
            plotShadow: false,
			type: 'pie',
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
        },
        credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        title: {
            text: 'OPPORTUNITIES TYPE',
			align : "left",
			style:{
				 "fontSize": "13.5px"
			}
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {					 
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					distance:40,
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                } 
            }
        },
		tooltip: {
            headerFormat: '<span style="font-size:10px">Lead Status :<br> {point.key} :<b>{point.percentage:.1f}%</b> </span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">  </td>' +
                '<td style="padding:0"><b></b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
			enabled: false
        },
        series: [{    
            name: 'Lead Status',
			innerSize: '90%',
			size: "35%",
            data: opptTypeData,
			 
        }]
    });
	
	Highcharts.setOptions({
	 colors: ['#ED4749','#54C0C0','#F7B45B']
    });
	var opptStatusData  = [<?php foreach($opptStatusChart as $k=>$v){echo "['".str_replace(array(' ', '-'),'<br>',$k)."',".$v."],";} ?>];
	var opptStatuschart = new Highcharts.Chart({ 
        chart: {
			renderTo: 'oppt-status-chart',
			height: 300,
            plotBackgroundColor: null,
            plotShadow: false,
			type: 'pie',
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
        },
        credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        title: {
            text: 'OPPORTUNITIES STATUS',
			align : "left",
			style:{
				 "fontSize": "13.5px"
			}
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {                
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {					 
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					distance:40,
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                } 
            }
        },
		tooltip: {
            headerFormat: '<span style="font-size:10px">Lead Status :<br> {point.key} :<b>{point.percentage:.1f}%</b> </span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">  </td>' +
                '<td style="padding:0"><b></b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true,
			enabled: false
        },
        series: [{    
            name: 'Lead Status',
			innerSize: '90%',
			size: "35%",
            data: opptStatusData,
			 
        }]
    });
	
	var opptData = [<?php foreach($opptAllChart as $m=>$v){echo $v.",";}?> ];
	var opptCategory = [<?php foreach($opptAllChart as $m=>$v){echo "'".$m."',";}?> ];
	Highcharts.setOptions({
	 colors: ["#FFFFFF"]
    });
	$('#oppt-chart').highcharts({
        chart: {
            type: 'column',
			height: 300,
			style:{
				fontFamily : "'Open Sans', sans-serif"	
			},
			backgroundColor : "#A9D96C"
        },
        title: {
            text: 'TOTAL OPPORTUNITIES MONTHLY, LAST 12 MONTHS',
			align:'left',
			style:{
				 fontSize : "11.5px",
				 color : "#FFFFFF"
			}
        },       
		credits: {
			enabled: false
		},
		exporting: { 
			enabled: false 
		}, 
        xAxis: {
            categories: opptCategory,
			labels: {
                style: {
                    color: '#FFFFFF',
                },
            },
        },
        yAxis: {
            min: 0,
			title: {
                	text: ''
            	},            
			labels: {
                style: {
                    color: '#FFFFFF',
                },
            },
        },
		legend: {
			enabled : false
		},
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Actions',
            data: opptData 

        }]
    });
});
</script>