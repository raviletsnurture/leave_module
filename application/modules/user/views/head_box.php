<div class="row state-overview">
  <?php 
  $i=0; 
  $piechart = "";
  $butColorArr = array("terques","red","yellow","terques");
  
  foreach($boxArr as $k=>$v){ 
  ?>
  <div class="col-lg-3 col-sm-6">
    <section class="panel">
      <div class="symbol symbol-btn <?php echo $butColorArr[$i]?>">
        <div><h1><?php echo $v;?></h1></div>
        <div style=" font-size:13px;"><?php echo strtoupper($k);?></div>
      </div>
    </section>
  </div>
  <?php 
  $i++;
  }
  ?>
</div>