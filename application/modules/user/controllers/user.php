<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller {
	var $logSession;
	//var $loginUser;
    function __construct()
    {
        parent::__construct();
        $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->logSession = $this->session->userdata("login_session");
		$this->load->model('user_model');
		$this->load->helper('general2_helper');
    }

	function index(){
		redirect(base_url()."user/profile");
	}
	function changeImage(){
		$userData = $this->logSession;     
		$data['user_id'] = $userData[0]->user_id;
		$userDetail = getUserByid($data['user_id']);
		$imagePath = 'uploads/userProfileImage';
		makeDir($imagePath);
		$mainImage = 'profileImage';
		$encodedName = rtrim(strtr(base64_encode($data['user_id'].time()), '+/', '-_'), '=');
		$ext = array("pdf", "PDF", "jpeg", "JPEG","PNG", "png","JPG", "jpg");
		if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != ''){
			if(file_exists(FCPATH.$imagePath.'/'.$userDetail[0]['user_pic'])){				
					unlink(FCPATH.$imagePath.'/'.$userDetail[0]['user_pic']);
				}
			$mainImageName = savePDF($mainImage,$imagePath,$ext,$encodedName);						
			$data['mainImageName'] = $mainImageName;
			if($mainImageName !== false){
				$updateImage = $this->user_model->updateImage($data);
				if($updateImage){
					$this->session->set_flashdata('success', 'Profile pic uploaded!');
					redirect(base_url().'user/profile');
                    exit;
				}else{
					$this->session->set_flashdata('error','On Snap ! Profile pic not uploaded, verify image and upload again!');
                    redirect(base_url().'user/profile');
                    exit;
				}
			}else{
				$this->session->set_flashdata('error','On Snap ! Forgot to upload Image? please upload again!');
                redirect(base_url().'user/profile');
                exit;
			}
		}
		redirect(base_url() . 'user');
	}
	function updatemailrequest(){
		$dd = $this->input->post('user_id');
		$info = $this->user_model->getUserinfo($dd);
		$finalEmailHtml = $this->load->view("user/userupdate_tmp",$info[0], true);
		$this->db->select('sEmail as email');
		$this->db->from('super_admin');
		$query=$this->db->get()->result();
		$subject="User Detail Update Request";
		for($i=0;$i<sizeof($query);$i++){
			$toEmail = $query[$i]->email;;
			send_mail($toEmail, $subject, $finalEmailHtml);
			$toEmail = '';
		}
	}

	function profile($did = ''){
		//pr($this->loginUser[0]->distId);
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		if($did != ''){
			$distId = $did;
		}

		$data['userdata'] = $this->user_model->getUserinfo($id);
		$data['titalTag'] = ' - Profile';
		$this->template->load_partial('dashboard_master','profile',$data);
	}

	function edit($id = '')
	{
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['userdata'] = $this->user_model->getUserinfo($userData[0]->user_id);

		$data['url'] = base_url().'user/profile/edit';



		/*For time beng added this line*/
		redirect(base_url()."user/profile");exit;



		if($this->input->post("updatePassword") !== false)
		{
			$udata2['user_id'] = $userData[0]->user_id;

			//$currPassword = md5($this->input->post('currPassword'));
			//$repassword = md5($this->input->post('repassword'));

			$currPassword = $this->input->post('currPassword');
			$repassword = $this->input->post('repassword');


			//$udata2['password'] = md5($this->input->post('password1'));
			$udata2['password'] = $this->input->post('password1');

			if($udata2['password'] != '' && $repassword != '' && $currPassword != '')
			{
				if($data['userdata'][0]->password != $currPassword)
				{
					$this->session->set_flashdata('errorPass','Your current password is incorrect! Try again');
					redirect(base_url()."user/profile/edit#focusPass");exit;
				}
				if($repassword != $udata2['password'])
				{
					$this->session->set_flashdata('errorPass','Password and Confirm Password are not matched!');
					redirect(base_url()."user/profile/edit#focusPass");exit;
				}
				$data2 = $this->user_model->updateUser($udata2);
				if($data2 == true)
				{
					$this->session->set_flashdata('successPass','Your password updated successfully!');
					redirect(base_url()."user/profile/edit#focusPass");exit;
				}
			}
		}

		if($this->input->post("update") !== false)
		{
			$addData['user_id'] = $userData[0]->user_id;


			$addData['first_name'] = $this->input->post('first_name');
			$addData['middle_name'] = $this->input->post('middle_name');
			$addData['last_name'] = $this->input->post('last_name');




			$addData['country_id'] = $this->input->post('country_id');
			$addData['state_id'] = $this->input->post('state_id');
			$addData['city_id'] = $this->input->post('city_id');
			$addData['street'] = $this->input->post('street');
			$addData['zipcode'] = $this->input->post('zipcode');

			$addData['gender'] = $this->input->post('gender');
			$addData['birthdate'] = $this->input->post('birthdate');
			$addData['mobile'] = $this->input->post('mobile');
			$addData['landline'] = $this->input->post('landline');


			$addData['skype'] = $this->input->post('skype');
			$addData['gtalk'] = $this->input->post('gtalk');
			$addData['pancard_no'] = $this->input->post('pancard_no');
			$addData['pf_no'] = $this->input->post('pf_no');

			$addData['email'] = $this->input->post('email');
			$addData['alternate_email'] = $this->input->post('alternate_email');
			$addData['religion_id'] = $this->input->post('religion_id');


			$addData['user_updated'] = date('Y-m-d H:i:s');

			/*if(isset($_FILES['distImage']['name']) && $_FILES['distImage']['name'] != ''){
				$path = './uploads/product_img/';

				unlink($path.$data['userdata'][0]->distImage);

				$imgdata = upload_image('distImage',$_FILES['distImage'],$path,$udata['distId'],'dist_','resize','140','140','gif|jpg|png|jpeg');
				$udata['distImage'] = $imgdata['file_name'];
			}
			*/


			$data1 = $this->user_model->updateUser($addData);

			if($data1 == true){
				$this->session->set_flashdata('success','Your profile update successfully!');
				redirect(base_url()."user/profile/edit");
			}
		}

		$data['titalTag'] = ' - Edit Profile';
		$data["state"] = $this->user_model->geteditstate($id);
		$data["city"] = $this->user_model->geteditcity($id);
		$this->template->load_partial('dashboard_master','edit_profile',$data);
	}

	public function getStates()
	{
		$country_id = $this->input->post('country_id');
		$state_dropdown = $this->user_model->getState_drop($country_id);

		echo $state_dropdown;
	}
	public function getCities()
	{
		$state_id = $this->input->post('state_id');
		$city_dropdown = $this->user_model->getCity_drop($state_id);

		echo $city_dropdown;
	}

	function logout()
	{
		$this->session->unset_userdata("login_session");
		redirect(base_url());
		exit;
	}

	function uploaddocument($did = ''){
		//pr($this->loginUser[0]->distId);
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['userdata'] = $this->user_model->getUserinfo($id);
		$data['documentDatas'] = $this->user_model->getDocument($id);
		$data['titalTag'] = ' - Upload Document';
		if($this->input->post("document_name") !== false){
					$addData['document_name'] = $this->input->post('document_name');
					$addData['user_id'] = $id;
					$pdfPath = './uploads/document';
					$mainImage = 'documentFile';
					if($_FILES[$mainImage]['size'] > 2097152){
						$this->session->set_flashdata('error','File too large. File must be less than 2Mb.');
						redirect(base_url().'user/uploaddocument');exit;
					}
					// avoid _ / = in encodedname as it might result in 404 error for filenames
					$encodedName = rtrim(strtr(base64_encode(time()), '+/', '-_'), '=');
					$ext = array("pdf","docx","doc","jpg","jpeg","png","odt","ods");
					if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != ''){
						$mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);
						if($mainImageName !== false){
							$addData['file_name'] = $mainImageName;
						}else{
								$this->session->set_flashdata('error','On Snap ! Document not uploaded, verify file and upload again!');
								redirect(base_url().'user/uploaddocument');exit;
						}
					} else {
							$this->session->set_flashdata('error','On Snap ! Forgot to upload file? please upload again!');
							redirect(base_url().'user/uploaddocument');exit;
					}

					$data = $this->user_model->addDocument($addData);
					$this->session->set_flashdata('success','Document added successfully!');
					redirect(base_url().'user/uploaddocument');exit;

		}
	  $this->template->load_partial('dashboard_master','uploaddocument',$data);
	}

	function deleteDocument(){
		if($_POST['document_id'] !== false){
			$data = $this->user_model->deleteDocument($_POST['document_id']);
		}
	}

	function skills($skillid = 0){

		if($skillid){
			$data['skillsdetail'] = $this->user_model->getSkillDetail($skillid);
		}else {
			$data['skillsdetail'] = '';
		}
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['userdata'] = $this->user_model->getUserinfo($id);
		$data['skillsdata'] = $this->user_model->getAllSkills($id);
		$data['skills'] = $this->user_model->getSkill($id);
		$data['titalTag'] = ' - Skill';
	  $this->template->load_partial('dashboard_master','user/userskill',$data);
	}

	function addSkill(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['user_id'] = $id;
		if($_POST['skill'] != ''){
			$data['skill_id'] = $_POST['skill'];
			$data['ex_year'] = $_POST['ex_year'];
			$data['ex_month'] = $_POST['ex_month'];
			$data = $this->user_model->addSkill($data);
			print_r($data);
			exit;
			if($data == 1){
				echo "1";
			}else {
				echo "2";
			}
		}else {
			echo "0";
		}
	}

	function deleteSkill(){
		if($_POST['uskill_id'] !== false){
			$data = $this->user_model->deleteSkill($_POST['uskill_id']);
			echo "1";
		}else{
			echo "0";
		}
	}

	// User listing
	function employees(){
		$data['titalTag'] = ' - Employees';
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['userdata'] = $this->user_model->getUserinfo($id);
		$data['allUsers'] = $this->user_model->getAllUserDetails();
		$this->template->load_partial('dashboard_master','user/userListing',$data);
	}
}
?>
