<section id="main-content">
   <section class="wrapper">
      <div class="row">
         <div class="col-lg-12">
            <section class="panel">
               <header class="panel-heading">
                   <?php if(isset($devicesdata[0]->id)){ echo "Edit Device";} else{echo "Add Device";}?>
               </header>
               <?php if($this->session->flashdata('error')){?>
               <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
               </div>
               <?php } ?>
               <?php if($this->session->flashdata('success')){?>
               <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
               </div>
               <?php }?>
               <div class="panel-body">
                  <div class="form">
                     <?php
                        $form_attributes = array('name' => 'addInventoryDevices', 'id' => 'addInventoryDevices', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form","enctype"=>"multipart/form-data" );
                        if(isset($devicesdata[0]->id)){
                        	echo form_open(base_url().'inventory_devices/add/'.$devicesdata[0]->id,$form_attributes);
                        }else{
                        echo form_open(base_url().'inventory_devices/add',$form_attributes);
                        }
                        ?>

                       <div class="form-group">
                           <div class="col-lg-6">
                             <label for="system_tag">System Tag<span class="red">*</span></label>
                              <input class="form-control" id="system_tag" maxlength="100" name="system_tag" type="text"  value="<?php if(isset($devicesdata[0]->system_tag)){ echo $devicesdata[0]->system_tag;} ?>" placeholder="System Tag"/>
                           </div>

                          <div class="col-lg-6">
                            <label for="role_id">Inventory Type <span class="red">*</span></label>
                             <select name="inventory_type" id="inventory_type" class="form-control m-bot15">
                                 <option value="" >Select Inventory Type</option>
                                 <?php if(count($inventory_types) != 0){
                                     foreach($inventory_types as $type){?>}
                                <option value="<?php echo $type->id;?>" <?php $value1 = (isset($devicesdata[0]->inventory_type) && $devicesdata[0]->inventory_type == $type->id) ? $devicesdata[0]->inventory_type : 0; echo selectedVal($value1,$type->id)?>><?php echo $type->type_name;?></option>
                                <?php }} ?>
                             </select>
                          </div>
                       </div>

                       <div class="form-group ">
                          <div class="col-lg-6">
                            <label for="manufacturer">Manufacturer<span class="red">*</span></label>
                            <input class="form-control" id="manufacturer" maxlength="100" name="manufacturer" type="text"  value="<?php if(isset($devicesdata[0]->manufacturer)){ echo $devicesdata[0]->manufacturer;} ?>" placeholder="Manufacturer"/>
                          </div>
                          <div class="col-lg-6">
                            <label for="serial_number">Serial Number<span class="red">*</span></label>
                             <input class="form-control" id="serial_number" maxlength="100" name="serial_number" type="text"  value="<?php if(isset($devicesdata[0]->serial_number)){ echo $devicesdata[0]->serial_number;} ?>" placeholder="Serial Number"/>
                          </div>
                       </div>

                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="model">Model<span class="red">*</span></label>
                          <input class="form-control" id="model" maxlength="100" name="model" type="text"  value="<?php if(isset($devicesdata[0]->model)){ echo $devicesdata[0]->model;} ?>" placeholder="Modal"/>
                        </div>
                        <div class="col-lg-6">
                          <label for="os">OS</label>
                           <input class="form-control" id="os" maxlength="100" name="os" type="text"  value="<?php if(isset($devicesdata[0]->os)){ echo $devicesdata[0]->os;} ?>" placeholder="OS"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="ram">RAM</label>
                           <input class="form-control" id="ram" maxlength="100" name="ram" type="text"  value="<?php if(isset($devicesdata[0]->ram)){ echo $devicesdata[0]->ram;} ?>" placeholder="RAM"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="os_version">OS Version</label>
                           <input class="form-control" id="os_version" maxlength="100" name="os_version" type="text"  value="<?php if(isset($devicesdata[0]->os_version)){ echo $devicesdata[0]->os_version;} ?>" placeholder="OS Version"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="hdd_model" >HDD Modal</label>
                           <input class="form-control" id="hdd_model" maxlength="100" name="hdd_model" type="text"  value="<?php if(isset($devicesdata[0]->hdd_model)){ echo $devicesdata[0]->hdd_model;} ?>" placeholder="HDD Modal"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="hdd_capacity">HDD Capacity</label>
                           <input class="form-control" id="hdd_capacity" maxlength="100" name="hdd_capacity" type="text"  value="<?php if(isset($devicesdata[0]->hdd_capacity)){ echo $devicesdata[0]->hdd_capacity;} ?>" placeholder="HDD Capacity"/>
                        </div>
                      </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="hdd_manf">HDD Manufacturer</label>
                           <input class="form-control" id="hdd_manf" maxlength="100" name="hdd_manf" type="text"  value="<?php if(isset($devicesdata[0]->hdd_manf)){ echo $devicesdata[0]->hdd_manf;} ?>" placeholder="HDD Manufacturer"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="hdd_serial_number">HDD Serial Number</label>
                           <input class="form-control" id="hdd_serial_number" maxlength="100" name="hdd_serial_number" type="text"  value="<?php if(isset($devicesdata[0]->hdd_serial_number)){ echo $devicesdata[0]->hdd_serial_number;} ?>" placeholder="HDD Serial Number"/>
                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-12">
                          <label for="sim">SIM</label>
                           <select name="sim" id="sim" class="form-control">
                               <option value="" >Select SIM Status</option>
                              <option value="0" <?php echo isset($devicesdata[0]->sim)? selectedVal($devicesdata[0]->sim,"0"): '';?> >No SIM</option>
                              <option value="1" <?php echo isset($devicesdata[0]->sim)? selectedVal($devicesdata[0]->sim,"1"): '';?> >Single SIM</option>
                              <option value="2" <?php echo isset($devicesdata[0]->sim)? selectedVal($devicesdata[0]->sim,"2"): '';?>>Dual SIM</option>
                           </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-lg-6">
                            <label for="imei_number">IMEI Number</label>
                             <input class="form-control" id="imei_number" maxlength="100" name="imei_number" type="text"  value="<?php if(isset($devicesdata[0]->imei_number)){ echo $devicesdata[0]->imei_number;} ?>" placeholder="IMEI Number "/>
                        </div>
                        <div class="col-lg-6">
                            <label for="imei2_number">IMEI Number(2)</label>
                             <input class="form-control" id="imei2_number" maxlength="100" name="imei2_number" type="text"  value="<?php if(isset($devicesdata[0]->imei2_number)){ echo $devicesdata[0]->imei2_number;} ?>" placeholder="IMEI Number(2) "/>
                        </div>
                     </div>

                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="mac_address_ethernet">Mac Address-Ethernet</label>
                           <input class="form-control" id="mac_address_ethernet" maxlength="100" name="mac_address_ethernet" type="text"  value="<?php if(isset($devicesdata[0]->mac_address_ethernet)){ echo $devicesdata[0]->mac_address_ethernet;} ?>" placeholder="Mac Address-Ethernet"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="mac_address_wlan">Mac Address-WLAN</label>
                           <input class="form-control" id="mac_address_wlan" maxlength="100" name="mac_address_wlan" type="text"  value="<?php if(isset($devicesdata[0]->mac_address_wlan)){ echo $devicesdata[0]->mac_address_wlan;} ?>" placeholder="Mac Address-WLAN "/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12">
                          <label for="configuration">Configuration</label>
                           <input class="form-control" id="configuration" name="configuration" type="text"  value="<?php if(isset($devicesdata[0]->configuration)){ echo $devicesdata[0]->configuration;} ?>" placeholder="Configuration" maxlength="200"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12">
                          <label for="software_name">Software Name</label>
                           <input class="form-control" id="software_name" name="software_name" type="text"  value="<?php if(isset($devicesdata[0]->software_name)){ echo $devicesdata[0]->software_name;} ?>" placeholder="Software Name"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="licence">Software Licence</label>
                           <input class="form-control" id="licence" name="licence" type="text"  value="<?php if(isset($devicesdata[0]->licence)){ echo $devicesdata[0]->licence;} ?>" placeholder="Software Licence "/>
                        </div>

                        <div class="col-lg-6">
                          <label for="soft_expiration">Licence Expiration</label>
                          <input class="form-control" id="soft_expiration" name="soft_expiration" type="text"  value="<?php if(isset($devicesdata[0]->soft_expiration)){ echo $devicesdata[0]->soft_expiration;} ?>" placeholder="Licence Expiration "/>
                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                            <label for="working_status">Working Status<span class="red">*</span></label>
                             <select name="working_status" id="working_status" class="form-control">
                                <option value="" >Select Working Status</option>
                                <option value="0" <?php echo isset($devicesdata[0]->working_status)? selectedVal($devicesdata[0]->working_status,"0"): '';?> >Damaged</option>
                                <option value="1" <?php echo isset($devicesdata[0]->working_status)? selectedVal($devicesdata[0]->working_status,"1"): '';?> >Working</option>
                             </select>
                        </div>

                        <div class="col-lg-6">
                            <label for="assign_status">Assignee Status<span class="red">*</span></label>
                             <select name="assign_status" id="assign_status" class="form-control">
                                 <option value="" >Select Assignee Status</option>
                                <option value="0" <?php echo isset($devicesdata[0]->assign_status)? selectedVal($devicesdata[0]->assign_status,"0"): '';?> >Free</option>
                                <option value="1" <?php echo isset($devicesdata[0]->assign_status)? selectedVal($devicesdata[0]->assign_status,"1"): '';?> >Assigned</option>
                             </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12">
                           <button class="btn btn-danger" type="submit" name="addEditInventoryDevices">
                           <?php if(isset($devicesdata[0]->id)){ echo "Update";} else{echo "Submit";}?>
                           </button>
                           <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                        </div>
                     </div>
                     </form>
                    </div>
                  </div>
               </div>
            </section>
         </div>
   </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<!--<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-inventory-devices.js" ></script>
