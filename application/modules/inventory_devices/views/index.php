<?php
    $this->load->model("inventory_devices_model");
 ?>
<style media="screen">
  #engines,#engines1,#engines2,#engines3,#engines4,#engines5,#engines6,#engines7,#engines8{
    width:15%;
    margin: 10px 0;
  }
  #example_wrapper .row-fluid .span6:first-of-type{
    float: left;
    width:20%;
  }
  #example_wrapper .row-fluid .span6:last-of-type{
    float: left;
    width:80%;
  }
  #myModal .modal-body{
    overflow-y: scroll;
    height: 500px;
  }
  #myModal .modal-body::-webkit-scrollbar {
      width: 10px;
  }

  #myModal .modal-body::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(232, 64, 63,0.3);
      border-radius: 10px;
  }

  #myModal .modal-body::-webkit-scrollbar-thumb {
      border-radius: 10px;
      -webkit-box-shadow: inset 0 0 6px rgba(232, 64, 63,0.5);
  	background:rgb(232, 64, 63);
  }
  table{
    width: 100% !important;
  }

    .editable-table .dataTables_filter{width: 23%;}
  </style>
<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
          <header class="panel-heading"> All Inventory Devices </header>
          <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
            <div class="row">
               <div class="col-lg-4">
                <div id="editable-sample_length" class="dataTables_length">
                  <div class="btn-group">
                    <a href="<?php echo base_url()?>inventory_devices/add">
                    <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
                <div class="adv-table editable-table ">
                  <?php if($this->session->flashdata('error')){?>
                  <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
                  <?php } ?>
                  <?php if($this->session->flashdata('success')){?>
                  <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
                  <?php }?>
                  <table class="table table-striped table-hover table-bordered" id="example">
                    <thead>
                      <tr>
                        <th>System Tag</th>
                        <th>Inventory Type</th>
                        <th>Serial No</th>
                        <th>Manufacturer</th>
                        <th>Working Status</th>
                        <th>Assign Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($devices as $row){ ?>
                      <tr>
                        <td><?php echo $row->system_tag;?></td>
                        <td>
                            <?php if($row->inventory_type){
                                $inventory_type = $this->inventory_devices_model->getInventoryDeviceTypeName($row->inventory_type);
                                echo $inventory_type[0]->type_name;
                            } ?>
                        </td>
                        <td><?php echo $row->serial_number; ?></td>
                        <td><?php echo $row->manufacturer; ?></td>
                        <td><?php echo ($row->working_status == 0)?'Damaged':'Working'; ?></td>
                        <td><?php echo ($row->assign_status == 0)?'Free':'Assigned'; ?></td>
                        <td>
                          <a href="<?php echo base_url()?>inventory_devices/edit/<?php echo $row->id;?>" >
                          <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Inventory" title=""><i class="fa fa-pencil"></i></button>
                          </a>
                          <a href="<?php echo base_url()?>inventory_devices/delete/<?php echo $row->id;?>" class="deleteRec">
                          <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Inventory"><i class="fa fa-trash-o "></i></button>
                          </a>
                          <a value="<?php echo $row->id;?>" onclick="getchek(<?php echo $row->id;?>)" href="<?php echo base_url()?>inventory_devices/view/<?php echo $row->id;?>" >
                            <button class="btn btn-primary btn-xs tooltips" data-original-title="View&nbsp;Inventory" title=""><i class="fa fa-eye"></i></button>
                          </a>
                          </td>
                      </tr>
                      <?php }?>
                    </tbody>
                  </table>

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">User Information</h4>
                                        <h4 class="modal-title" id="myModalLabel1">User Leave Status</h4>
                                </div>
                                <div class="modal-body">
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
        </section>
    </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script> -->
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
function getchek(id)
{
	$.ajax({
		 url: '<?php echo base_url(); ?>inventory_devices/view/',
		 data: { id: id},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('.modal-body').html(data);
			 $('#myModalLabel').show();
			 $('#myModalLabel1').hide();
			 $('#myModal').modal('show');
		 }
	});
}


  $(document).ready(function() {

	var dt =  $('#example').DataTable( {
		  "aaSorting": [[ 0, "asc" ]],
          "scrollX": true
	  } );
	  $(".dataTables_filter input").addClass('form-control');
  } );
</script>
