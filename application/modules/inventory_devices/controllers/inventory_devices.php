<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class inventory_devices extends MX_Controller {
  var $logSession;
	function __construct(){
		parent::__construct();
        $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('inventory_devices_model');
	}

	function index(){
		$data['titalTag'] = ' - Inventory Devices List';
		$data["devices"] = $this->inventory_devices_model->getAllInventoryDevices();
        $this->template->load_partial('dashboard_master','inventory_devices/index',$data);
	}

    function add($deviceId = '')
    {

        $data['titalTag'] = ' - Inventory Device Add';
        $data["inventory_types"] = $this->inventory_devices_model->getAllInventoryDeviceTypes();
        if($this->input->post("addEditInventoryDevices") !== false)
        {
            $addData['system_tag'] = $this->input->post('system_tag');
            $addData['inventory_type'] = $this->input->post('inventory_type');
            $addData['manufacturer'] = $this->input->post('manufacturer');
            $addData['serial_number'] = $this->input->post('serial_number');
            $addData['model'] = $this->input->post('model');
            $addData['os'] = $this->input->post('os');
            $addData['ram'] = $this->input->post('ram');
            $addData['os_version'] = $this->input->post('os_version');
            $addData['hdd_model'] = $this->input->post('hdd_model');
            $addData['hdd_capacity'] = $this->input->post('hdd_capacity');
            $addData['hdd_manf'] = $this->input->post('hdd_manf');
            $addData['hdd_serial_number'] = $this->input->post('hdd_serial_number');
            $addData['sim'] = $this->input->post('sim');
            $addData['imei_number'] = $this->input->post('imei_number');
            $addData['imei2_number'] = $this->input->post('imei2_number');
            $addData['mac_address_ethernet'] = $this->input->post('mac_address_ethernet');
            $addData['mac_address_wlan'] = $this->input->post('mac_address_wlan');
            $addData['configuration'] = $this->input->post('configuration');
            $addData['software_name'] = $this->input->post('software_name');
            $addData['licence'] = $this->input->post('licence');
            $addData['soft_expiration'] = $this->input->post('soft_expiration');
            $addData['working_status'] = $this->input->post('working_status');
            $addData['assign_status'] = $this->input->post('assign_status');
            if(is_numeric($deviceId) && $deviceId != '')
            { /*when update*/
                $addData['id'] = $deviceId;
                $addData['updated_at'] = date('Y-m-d H:i:s');
                $data = $this->inventory_devices_model->updateInventoryDevices($addData);
                $this->session->set_flashdata('success','Inventory Device updated successfully!');
            }else{
                $data = $this->inventory_devices_model->addInventoryDevices($addData);
                $this->session->set_flashdata('success','Inventory Device added successfully!');

            }


            redirect(base_url().'inventory_devices');exit;
        }

        $this->template->load_partial('dashboard_master','inventory_devices/add_edit_inventory_devices',$data);
    }

    function edit($deviceId = '')
	{
		$data['titalTag'] = ' - Edit Inventory Device';
		$data['devicesdata'] = $this->inventory_devices_model->getAllInventoryDevices();
        $data["inventory_types"] = $this->inventory_devices_model->getAllInventoryDeviceTypes();
		$this->template->load_partial('dashboard_master','inventory_devices/add_edit_inventory_devices',$data);

	}

    public function delete($deviceId)
	{
		$this->inventory_devices_model->deleteInventoryDevice($deviceId);

		$this->session->set_flashdata('success','Inventory Device deleted successfully!');
		redirect(base_url().'inventory_devices');
		exit;
	}

}
?>
