<?php
class inventory_devices_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}


	function getAllInventoryDevices()
	{
		$this->db->select("*");
		$this->db->from("crm_device_inventory");
		$this->db->where("is_delete = '0'");
		$result = $this->db->get()->result();
		return $result;
	}

	function addInventoryDevices($data){
		if($this->db->insert('crm_device_inventory',$data)){
			return true;
		}else{
			return false;
		}
	}

	function updateInventoryDevices($data){
		$this->db->where('id',$data['id']);
		if($this->db->update('crm_device_inventory',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getAllInventoryDeviceTypes()
	{
		$this->db->select("*");
		$this->db->from("crm_inventory_type");
		$result = $this->db->get()->result();
		return $result;
	}

	function getInventoryDeviceTypeName($id)
	{
		$this->db->select("type_name");
		$this->db->from("crm_inventory_type");
		$this->db->where('id',$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function deleteInventoryDevice($id)
	{
		$data['is_delete'] = '1';
		$this->db->where('id',$id);
		if($this->db->update('crm_device_inventory',$data)){
			return true;
		}else{
			return false;
		}
	}

}
?>
