<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Interview extends MX_Controller{

    function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->database();
        $this->logSession = $this->session->userdata("login_session");
        $this->load->helper('url');
		$this->load->helper('download');
        $this->load->helper('general_helper');
        $this->load->helper('general2_helper');
		$this->load->model('interview_model');
        user_auth(0);
        $this->load->library('email');
        date_default_timezone_set("Asia/Kolkata");
    }
    function index(){
        $data['titalTag'] = ' - Interview';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        $data["all_interview_list"] = $this->interview_model->getAllInterviews($id);
        $this->template->load_partial('dashboard_master','interview/interview_list',$data);
    }
    function putComment()
	{
		$data['userId'] = $this->input->post('id');	
		$data['userData'] = $this->interview_model->getRecruitmentUserforComment($data['userId']);
		$this->load->view('interview/comment', $data);
    }
    function recruitmentComment(){
        $userData = $this->logSession;
        $reviewer_id = $userData[0]->user_id;
        $id = $this->input->post('applicant_id');
		$comment = $this->input->post('comment');
 		if($this->interview_model->RecruitmentcommentSubmit($comment,$id,$reviewer_id))
		{
			echo 1;
		}else{
			echo 0;
		}
	}
}

?>