<?php
class interview_model extends CI_Model
{ 
    function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
    }
    
    function getAllInterviews($id){
		$this->db->select('*');
        $this->db->from('crm_interview');
		$this->db->WHERE ('FIND_IN_SET("'.$id.'",interviewers )');
		$this->db->where("interview_created > CURDATE()");
		$this->db->order_by("interview_created","asc");
		$query1 = $this->db->get()->result_array();

		$this->db->select('*');
        $this->db->from('crm_interview');
		$this->db->WHERE ('FIND_IN_SET("'.$id.'",interviewers )');
		$this->db->where("interview_created < CURDATE()");
		$this->db->order_by("interview_created","desc");
		$query2 = $this->db->get()->result_array();

		$query = array_merge($query1, $query2);

		return $query;
    }
    function getRecruitmentUserforComment($id)
	{
		$this->db->select('*');
        $this->db->from('crm_req_applicants_comment as r');
        $this->db->join('crm_users as u','u.user_id = r.reviewer_id');			
        $this->db->where("r.req_applicants_id = $id");	
        $this->db->order_by("r.created_at","Desc");	
		$query=$this->db->get()->result();
		return $query;
    }
    function RecruitmentcommentSubmit($comment, $req_applicants_id, $reviewer_id){    	
        $data = array(
        	    'req_applicants_id'=>$req_applicants_id,
                'comment'=>$comment,
                'reviewer_id'=>$reviewer_id
        	    );        
        $query = $this->db->insert('crm_req_applicants_comment',$data);
		if($query){
			return true;
		}else{
			return false;
		}
    }
    function updateInterviewSechduleDate($data){
		$this->db->where('id',$data['id']);
		if($this->db->update('req_applicants',$data)){
			return true;
		}else{
			return false;
		}
	}
}
?>