
<div class="row-fluid">
    <div class="span9">
        <div class="new-list">
            <div class="item panel" id="myprofile">
                <?php if(!empty($userData)){      ?>
                <div class="form-group"><label class="control-label">Previous Comment</label></div>
                <div style="overflow-y:scroll;height:153px;">
                <table class="table table-striped table-hover table-bordered" style="border:1px solid #E1E1E3;"id="example">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Comment</th>
                            <th>Date</th>                            
                        </tr>
                    </thead>                    
                    <tbody>  
                    <?php                                    
                      foreach($userData as $value){ ?>
                      <tr>
                      <td><?php echo $value->first_name.' '.$value->last_name; ?></td>
                      <td><?php echo $value->comment; ?></td>
                      <td><?php echo $value->created_at; ?></td>
                     </tr>
                <?php } ?>                    
                    </tbody>
                  </table>
                  </div>
                  <br/>
                  <?php } ?>
                <form>
                    <div class="form-group">
                        <label class="control-label">Add Comment</label>
                    </div>
                    <div class="form-group">                        
    					<textarea name="comment" id="comment" class="form-control leave_comment" ></textarea>
    				</div>
                    <div id="error" style="display: none;">
                        <div class="alert alert-block alert-danger fade in" >
                        <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                        <strong>Oh snap!</strong> Comment not added. </div>
                    </div>
                    <div id="success" style="display: none;">
                        <div class="alert alert-success fade in" >
                        <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                        <strong>Success!</strong> comment added successfully! </div>
                    </div>
                    <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $userId; ?>">
        			<button type="submit" class="btn btn-success" id="commentadd">Submit</button>
                </form>
        	</div>
            
        </div>
    </div>
</div>

    <script>
        
        $('form').on('submit', function (e) {
          e.preventDefault();
          $.ajax({
            type: 'post',
            url: '<?php echo base_url(); ?>interview/recruitmentComment',
            data: $('form').serialize(),
            success: function (data) {
              if(data === '1'){
                 $("#error").hide();
                 $("#success").show();
       			 setTimeout(function() {$('#putCommentModal').modal('hide');}, 1000);
              }else{
                 $("#error").show();
                 $("#success").hide();
                 setTimeout(function() {$('#putCommentModal').modal('hide');}, 1000);
              }
            }
          });
        });
    </script>
