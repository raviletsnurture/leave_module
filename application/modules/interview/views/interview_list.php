<?php
$user = getAllUsers();
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
?>

<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">
      <header class="panel-heading">Your interviews with the candidates</header>
        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered" id="example">
                <thead>
                    <tr>
                        <th>Candidate Name</th>
                        <th>Qualification</th>
                        <th>Applied Position</th>
                        <th>Total Experience(in year)</th>
                        <th>Resume</th>
                        <th>Interview date</th>
                        <th>Current position</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
         <?php
         foreach( $all_interview_list as $all_interview_list){?>
             <tr>
                <td><?php echo $all_interview_list['candidate_name']?></td>
                <td><?php echo $all_interview_list['qualification']?></td>
                <td><?php echo $all_interview_list['applied_position']?></td>
                <td><?php echo $all_interview_list['total_experience']?></td>
                <td>
                    <a href="http://recruitment.letsnurture.com/CV/<?php echo $all_interview_list['resume'];?>" target="_blank">View resume</a>
                </td>

                <td                 
                <?php 
                    if( (date("Y-m-d h:i:s")) <= $all_interview_list['interview_date'])
                        {echo 'class="greeny"';}?> >
                <?php echo date("d-M-Y h:i:s", strtotime($all_interview_list['interview_date']))
                ?>
                </td>

                <td><?php echo $all_interview_list['current_position']?></td>

                <td>
                    <a value="<?php echo $all_interview_list['interview_id'];?>" onclick="putComment(<?php echo $all_interview_list['interview_id'];?>)" href="#" ><button class="btn btn-primary btn-xs tooltips" data-original-title="give review" title=""><i class="fa fa-comment"></i></button></a>
                </td>
               
                
             </tr>
         <?php
         }
         ?>

         </tbody>
            </table>
        </div>
    </section>
  </section>
</section>

<div class="modal fade" id="putCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="putCommentModalLabel">Comment</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script>
$(document).ready(function(){
  var dt =  $('#example').dataTable( {
            "aaSorting": [],
            "iDisplayLength": 20,
            "pagingType": "full_numbers",
            "dom": 'Cfrtip',
            "destroy": true,
            //"bFilter": false,
            "bPaginate": true,
            "bInfo" : true,

            "oSearch": { "bSmart": false, "bRegex": true },
            "aoColumnDefs": [
                {
                'bSortable': false, 'aTargets': [ -1, 2, 4, 6, 7 ]
                }
            ]
            });               
});
function putComment(id){
        $.ajax({
            url: '<?php echo base_url(); ?>interview/putComment',
            data: {id: id},
            dataType: 'html',
            type: 'POST',
            success: function(data){
                $('#putCommentModal .modal-body').html(data);
                $('#putCommentModal').modal('show');
            }
        });
    } 
</script>

