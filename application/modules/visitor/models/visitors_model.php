<?php
class visitors_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllVisitor($id){
		$this->db->select("u.first_name, u.last_name, v.id as visit_id, v.name as visitor_name, v.visit_type, v.description, v.visit_date");
		$this->db->from("crm_visitor as v");
		$this->db->join('crm_users as u','u.user_id = v.user_id', 'left');
		$this->db->where("v.user_id = $id");
		$this->db->order_by("visit_date","desc");
		$result = $this->db->get()->result();
		//echo $sql = $this->db->last_query();
		//exit;
		return $result;
	}

	function add($data){
	    if($this->db->insert('crm_visitor',$data)){
			return true;
		}else{
			return false;
		}
	}
	function update($data,$r_id){
		$this->db->where('id',$r_id);
	    if($this->db->update('crm_visitor',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getVisitors($date = ''){
		if($date == '' && empty($date)) {
			$date = date("Y-m-d");
		}
		$this->db->select("u.first_name, u.last_name, v.id as visit_id, v.name as visitor_name, v.visit_type, v.description, v.visit_date");
		$this->db->from("crm_visitor as v");
		$this->db->join('crm_users as u','u.user_id = v.user_id', 'left');
		$this->db->where("Date(v.visit_date) = '$date'");
		$this->db->order_by("created_at","desc");
		$result = $this->db->get()->result();
		//echo $sql = $this->db->last_query();
		//exit;
		return $result;
	}

	function deleteVisitor($id){
		if($this->db->delete("crm_visitor" ,['id' => $id])){
			return true;
		} else {
			return false;
		}
	}

	function deleteMyVisitor($id, $user_id){
		$this->db->select("*");
		$this->db->from("crm_visitor");
		$this->db->where("id = $id");
		$row = $result = $this->db->get()->result();
		if($row[0]->user_id == $user_id){
			if($this->db->delete("crm_visitor" ,['id' => $id, 'user_id' => $user_id])){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
?>
