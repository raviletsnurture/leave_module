
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
.reward_description {
    border: 1px solid #E1E1E3;
    width: 47%;
    margin-left: 16px;
    font-family: sans-serif;
    border-radius: 4px;
    line-height: 25px;
    color: #6B6B6B;
    font-size: 13px;
}
</style>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Add Visitor
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
								<?php
									$form_attributes = array('name' => 'addVisitor', 'id' => 'addVisitor', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
									echo form_open(base_url().'visitor/store',$form_attributes);
								?>
								<div class="form-group">
									<label for="visitor_name" class="control-label col-lg-2">Visitor Name <span class="red">*</span></label>
									<div class="col-lg-6">
										<input type="text" name="visitor_name" id="visitor_name" class="form-control" required>
									</div>
									
								</div>

								<div class="form-group">
									<label for="project_id" class="control-label col-lg-2">Visitor Type <span class="red">*</span></label>
									<div class="col-lg-6">
										<select name="visitor_type" id="visitor_type" class="form-control" required>
											<option disabled selected value> -- Select An Option -- </option>
											<option value="parcel">Parcel</option>
											<option value="client">Client</option>
											<option value="interview">Interview</option>
											<option value="bank">Bank</option>
											<option value="relative">Relative</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Date <span class="red">*</span></label>
									<div class="col-lg-4">
										<input size="16" type="text" id="datetime" name="datetime" placeholder=" Ex. 2018-07-31 14:45" class="form_datetime form-control" readonly>
										<span id="date-error" class="text text-danger"></span>
									</div>
								</div>

								<div class="form-group">
									<label  class="col-lg-2 control-label">Description <span class="red">*</span></label>
									<div class="col-lg-6">
										<textarea class="form-control" style="resize:none;" id="description" name="description" rows="3" cols="40" placeholder="Description for Visit Purpose" required></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-danger" type="submit" id="addRewardBtn">Submit
										</button>
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-reward.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$(".form_datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		startDate: today,
		autoclose: true,
		Default: false
	});

	$('#addVisitor').submit(function(event) {
		if($('#datetime').val() == '') {
			event.preventDefault();
			$('#date-error').html(
				"Please Select Date and Time"
			);
			return false;
		}
	});

	$('#datetime').change(function(){
		$('#date-error').html("");
	});
</script>

<script type="text/javascript">
$(document).ready(function(){
	$('#addVisitorBtn').click(function(event){
		$(':input[type="submit"]').prop('disabled', true);
        $('#addVisitor').submit();
	});
});
</script>
