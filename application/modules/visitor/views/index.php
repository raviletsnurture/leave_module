<style type="text/css">
.dataTables_filter {
     display: none;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading"> My Visitors</header>
        <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
			<div class="row">
        <div class="col-lg-12">
          <div id="editable-sample_length" class="dataTables_length">
            <div class="btn-group">
              <a href="<?php echo base_url()?>visitor/add">
                <button class="btn btn-info" id="editable-sample_new"> Add Visitor <i class="fa fa-plus"></i> </button>
              </a>
            </div>
            <?php if($id == 75 || $id == 213): ?>
              <div class="btn-group pull-right">
                <a href="<?php echo base_url()?>visitor/all">
                  <button class="btn btn-primary" id="editable-sample_new"> View All Visitors </button>
                </a>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th width="5%">ID</th>
                <th width="10%">Visitor Name</th>
        				<th width="10%">Type</th>
                <th width="10%">Visit Person</th>
        				<th width="10%">Date</th>
                <th width="10%">Time</th>
        				<th width="30%">Description</th>
                <th width="5%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($visitors as $row){ ?>
                <tr>
                  <td><?php echo $cnt; ?></td>
                  <td><?php echo $row->visitor_name ?></td>
                  <td><?php echo $row->visit_type; ?></td>
                  <td><?php echo $row->first_name. " " .$row->last_name; ?></td>
                  <td><?php echo date("d-m-Y", strtotime($row->visit_date)); ?></td>
                  <td><?php echo date("g:i a", strtotime($row->visit_date)); ?></td>
                  <td><?php echo $row->description; ?></td>
                  <?php
                    if(strtotime($row->visit_date) > strtotime((new DateTime())->format('Y-m-d H:i:s'))){
                  ?>
                  <td><a href="<?php echo base_url()?>visitor/delete/<?php echo $row->visit_id; ?>" class="btn btn-danger"><span class="fa fa-close"></span></a></td>
                  <?php
                    } else {
                  ?>
                  <td></td>
                  <?php
                    }
                  ?>
                </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>
			  <!-- My Leaves end-->
    <!-- page end-->
  </section>
</section>
</section>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script>
  $(document).ready(function() {

    var dt =  $('#example').dataTable( {
      "aaSorting": [[ 0, "asc" ]],
      "iDisplayLength": 20,
      "pagingType": "full_numbers",
      "dom": 'Cfrtip',
      "destroy": true,
      //"bFilter": false,
      "bPaginate": true,
      "bInfo" : false,

      "oSearch": { "bSmart": false, "bRegex": true },
      /*
      "aoColumnDefs": [
        {
        'bSortable': true, 'aTargets': [ 3 ]
        }
      ]*/
      });
  });

</script>
