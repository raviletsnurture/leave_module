<style type="text/css">
.dataTables_filter {
     display: none;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading"> All Visitors</header>
        <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
			<div class="row">
                <div class="col-lg-6">
                    <br>
                    <div class="form">
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="date">Date <span class="red">*</span></label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control" readonly="readonly" id="date" name="date" data-date-format="yyyy-mm-dd" required>
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-primary" id="filter" name="filter">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
			    <div class="col-lg-6">
					<div id="editable-sample_length" class="dataTables_length">
						<div class="btn-group pull-right">
                            <a href="<?php echo base_url()?>visitor" class="btn btn-primary" id="editable-sample_new"> Back </a>
						</div>
					</div>
				</div>
            </div>
        </div>
        <div class="panel-body">
            <div class="adv-table editable-table ">
            <?php if($this->session->flashdata('error')){ ?>
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> 
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('success')){ ?>
                <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                </div>
            <?php }?>

                <table class="table table-striped table-hover table-bordered" id="example">
                <thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="10%">Visitor Name</th>
                    <th width="10%">Type</th>
                    <th width="10%">Visit Person</th>
                    <th width="10%">Date</th>
                    <th width="10%">Time</th>
                    <th width="30%">Description</th>
                </tr>
                </thead>
                <tbody id="filtered-data">
                    <?php $cnt = 1;
                    foreach($visitors as $row){ ?>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><?php echo $row->visitor_name ?></td>
                            <td><?php echo $row->visit_type; ?></td>
                            <td><?php echo $row->first_name. " " .$row->last_name; ?></td>
                            <td><?php echo date("d-m-Y", strtotime($row->visit_date)); ?></td>
                            <td><?php echo date("g:i a", strtotime($row->visit_date)); ?></td>
                            <td><?php echo $row->description; ?></td>
                        </tr>
                    <?php $cnt++; }?>
                </tbody>
            </table>
            </div>
        </div>
			  <!-- My Leaves end-->
    <!-- page end-->
  </section>
</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />

<script type="text/javascript">
	$('#date').datepicker({
        autoclose: true,
	});
</script>

<script>
    $("#filter").click(function(){
        var date = $("#date").val();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url()?>visitor/dateSort",
            data: {
                date: date,
            },
            success: function(data1){
                // alert(data1);
                $("tbody").html(data1);
            },
            error: function(){
                alert("error");
            }
        });
    });

    $(document).ready(function() {

        var dt =  $('#example').dataTable( {
            "aaSorting": [[ 0, "desc" ]],
            "iDisplayLength": 10,
            "pagingType": "full_numbers",
            
            "destroy": true,
            //"bFilter": false,
            "bPaginate": true,
            "bInfo" : false,
            // "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            "oSearch": { "bSmart": false, "bRegex": true },		 
            "aoColumnDefs": [{ "bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6] }]
        });
    });
</script>
