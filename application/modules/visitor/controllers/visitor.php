<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visitor extends MX_Controller {
    var $logSession;
	function __construct(){
		parent::__construct();
        $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('visitors_model');
        $this->load->library('email');
	}

	function index(){
        $data['titalTag'] = ' - Visitors';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        $data['id'] = $id;
        $data["visitors"] = $this->visitors_model->getAllVisitor($id);
        $this->template->load_partial('dashboard_master','visitor/index',$data);
	}

    function add(){
        $data['titalTag'] = ' - Visitors';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        $this->template->load_partial('dashboard_master','visitor/add_visitor',$data);
    }

    function store(){
        $loginSession = $this->session->userdata("login_session");
        $user_id = $loginSession[0]->user_id;
        
        if( $this->input->post('visitor_type') &&  $this->input->post('visitor_type') != ''){
            $visitor_name = $this->input->post('visitor_name');
            $visitor_type = $this->input->post('visitor_type');
            $date = $this->input->post('datetime');
            $description = $this->input->post('description');
            
            $addvisitor['user_id'] = $user_id;
            $addvisitor['name'] = $visitor_name;
            $addvisitor['visit_type'] = $visitor_type;
            $addvisitor['visit_date'] = $date;
            $addvisitor['description'] = $description;
            $timestamp = (new DateTime())->format('Y-m-d H:i:s');
            $addvisitor['created_at'] = $timestamp;
            $addvisitor['updated_at'] = $timestamp;

            $addDatasuccess = $this->visitors_model->add($addvisitor);
            if($addDatasuccess){
                redirect(base_url() . 'visitor');
            }
        }
    }

    function all() {
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        if($id == 75 || $id == 213) {
            $data['titalTag'] = ' - All Visitors';
            $data["visitors"] = $this->visitors_model->getVisitors();
            $this->template->load_partial('dashboard_master','visitor/all',$data);
        } else {
            $data['titalTag'] = ' - Visitors';
            $data["visitors"] = $this->visitors_model->getAllVisitor($id);
            $data['id'] = $id;
            $this->template->load_partial('dashboard_master','visitor/index',$data);
        }
    }

    function dateSort() {
        if($this->input->post('date') != '' && !empty($this->input->post('date'))) {
            // echo $this->input->post('date');
            $date = $this->input->post('date');
            $visitors = $this->visitors_model->getVisitors($date);
            $data = '';
            $cnt = 1;
            if($visitors != '' && !empty($visitors)) {
                foreach($visitors as $row) {
                    $data = $data."<tr>";
                    $data = $data."<td>$cnt</td>";
                    $data = $data."<td>$row->visitor_name</td>";
                    $data = $data."<td>$row->visit_type</td>";
                    $data = $data."<td>".$row->first_name. " " .$row->last_name."</td>";
                    $data = $data."<td>".date("d-m-Y", strtotime($row->visit_date))."</td>";
                    $data = $data."<td>".date("g:i a", strtotime($row->visit_date))."</td>";
                    $data = $data."<td>$row->description</td>";
                    // $visit_date = date("d-m-Y", strtotime($row->visit_date));
                    // if(strtotime($row->visit_date) < (new DateTime())->format('Y-m-d H:i:s')){
                    //     $data = $data."<td><a href='".base_url()."visitor/delete/".$row->visit_id."' class='btn btn-danger'><span class='fa fa-close'></span></a></td>";
                    // }
                    $data = $data."</tr>";
                    $cnt++;
                }
                echo  $data;
            } else {
                echo "<tr class='odd'><td valign='top' colspan='7' class='dataTables_empty'>No data available in table</td></tr>";
            }
        } else {
            echo "<tr class='odd'><td valign='top' colspan='7' class='dataTables_empty'>Please Select Date</td></tr>";
        }
    }

    function delete($id){
        $userData = $this->logSession;
        $user_id = $userData[0]->user_id;
        if($user_id == 75 || $user_id == 213) {
            $delete = $this->visitors_model->deleteVisitor($id);
            $message = '';
            if($delete) {
                $message = 'Visitor Deleted Successfully !!';
                $this->session->set_flashdata('success', $message);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $message = 'Error Deleting Data. Please Try Again.';
                $this->session->set_flashdata('error', $message);
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            $delete = $this->visitors_model->deleteMyVisitor($id, $user_id);
            $message = '';
            if($delete) {
                $message = 'Visitor Deleted Successfully !!';
                $this->session->set_flashdata('success', $message);
                redirect(base_url().'visitor');
            } else {
                $message = 'Error Deleting Data. Please Try Again.';
                $this->session->set_flashdata('error', $message);
                redirect(base_url().'visitor');
            }
        }
    }
}
?>
