<style type="text/css">
.dataTables_filter {
     /* display: none; */
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading">User Reward Requests</header>
      <div class="panel-body">                                           
      <select class="pull-right" id="engines">
              <option value="">Select Status</option> 
              <option value="Pending">Pending</option>         
              <option value="Approved">Approved</option>         
              <option value="Rejected">Rejected</option>
            </select>
        <div class="adv-table ">
          
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>                                                              
		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th width="10%">ID</th>
                <th width="20%">Full Name</th>
                <th>Department</th>
                <th width="25%">Request Type</th>
                <th width="25%">Description</th>
                <th width="5%">Request Points</th>
                <th>Reward Date</th>
                <th>Status</th>
                <th width="13%">Request Status</th>
                <th>Approved By/Rejected By</th>
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($rewards as $row){                
                  $intStatus = '';
                  if($row->reward_status==0)
                  {                   
                    $intStatus = 'Pending';
                  }else if($row->reward_status==1)
                  {                   
                    $intStatus = 'Approved';
                  }else if($row->reward_status==2)
                  {                   
                    $intStatus = 'Rejected';
                  }                                
                ?>
              <tr>
                <td><?php echo $row->rh_id; ?></td>
        				<td><?php echo $row->first_name.' '.$row->last_name;?></td>
                <td><?php echo $row->department_name;?></td>
                <td><?php echo $row->reward_name; ?></td>
                <td><?php echo $row->reward_description; ?></td>
                <td><?php echo $row->point; ?></td>
                <td><?php echo '<span>'.date("d-m-Y", strtotime($row->date)).'</span>'; ?></td>
                <td><?php echo $intStatus; ?></td>
        				<td>                 
                <?php if(!empty($login_role_id) && $login_role_id != 20){ ?>
                    <select id="reward_status" name="reward_status" onchange="ff(this)" <?php if($row->reward_id == '' || $row->approved_by == $user_id || $row->reward_status == '3' || $row->approved_by == 69 || $row->role_id == 13) echo 'disabled';?> user_id="<?php echo $row->user_id;?>" row_id="<?php echo $row->rh_id;?>">
                      <?php if($row->reward_status == '3'){ ?>
                      <option value="">Cancelled</option>                  
                      <?php }else{ ?>
                      <option value="">--Select--</option>                  
                      <?php } ?>          
                      <option value="<?php echo $row->reward_id; ?>,1" <?php if($row->reward_status == 1 ) { ?>selected="selected"<?php } ?>>Approved</option>
                      <option value="<?php echo $row->reward_id; ?>,2" <?php if($row->reward_status == 2 ) { ?>selected="selected"<?php } ?>>Rejected</option>
                          
                    </select>
                    <?php }else{ ?>
                                
                    <select id="reward_status" name="reward_status" onchange="ff(this)" <?php if($row->reward_id == '' || $row->approved_by == $user_id || $row->reward_status == '3'){ echo 'disabled'; } ?> user_id="<?php echo $row->user_id;?>" row_id="<?php echo $row->rh_id;?>">
                      <?php if($row->reward_status == '3'){ ?>
                      <option value="">Cancelled</option>                  
                      <?php }else{ ?>
                      <option value="">--Select--</option>                  
                      <?php } ?>          
                      <option value="<?php echo $row->reward_id; ?>,1" <?php if($row->reward_status == 1 ) { ?>selected="selected"<?php } ?>>Approved</option>
                      <option value="<?php echo $row->reward_id; ?>,2" <?php if($row->reward_status == 2 ) { ?>selected="selected"<?php } ?>>Rejected</option>
               
                  </select>

               <?php }?>                              
                </td>
                <td><?php echo $row->afirst_name.' '.$row->alast_name; ?></td>
                <!--
                <td><?php if($row->reward_status == '0'){ ?>
                <a href="#" class="" onclick="cancel(<?php echo $row->rh_id; ?>)">
                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Cancel&nbsp;Request"><i class="fa fa-exclamation-triangle"></i></button>
                </a>
                <?php }?></td>
              -->
              </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>
			  <!-- My Leaves end-->
    <!-- page end-->
  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />

<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script> -->
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script>
$(document).ready(function() {
	 var dt =  $('#example').DataTable( {
		 "aaSorting": [[ 0, "desc" ]],				
     "ordering": true,
		 "bFilter": true,
		 "bPaginate": true,
		 "bInfo" : true,
    		 
		 "aoColumnDefs": [
			{
			  'bSortable': true, 'aTargets': [0]
			}
		 ]
		});
    dt.column(7).visible(false);
  $('select#engines').change( function() {
      dt.columns(7).search($(this).val()).draw();      
        
	});  
    
});


function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel Reward!");
	if (r == true){
			$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/cancel',
		 data: { reward_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 location.reload();
	     },
    error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
		});
		//location.reload();
	} else {
	}
}

function ff(sel)
{
	var ff = sel.value.split(",");
	if(ff == '')
	{
		alert("Please Select an Option");
		location.reload();
		//return false;
	}
	else
	{
    var r = false;
    if(ff[1] == 2)
    {
      $('#h_id').val($(sel).attr('row_id'));
      $('#u_id').val($(sel).attr('user_id'));
      $('#reward_id').val(ff[0]);
      $('#myModal').modal();
    }else{
		    r = confirm("Are You Sure for this operation");
    }
    if (r == true)
		{
			// show loader
			$('#loaderAjax').show();
			$.ajax({
			 url: '<?php echo base_url(); ?>rewards/updateRewardStatus/',
			 data: { reward_id: ff[0], rh_id: $(sel).attr('row_id'), status: ff[1], user_id: $(sel).attr('user_id')},
			 dataType: 'html',
			 type: 'POST',
			 success: function(data){
				 // hide loader
				 $('#loaderAjax').hide();
				 //$('.modal-body').html(data);
				 //$('#myModal').modal('show');
				 //alert("Leave Status Has Been Changed");
				 location.reload();
				 }
			});
		}
		else {

		}
	 }

}

function reasonSubmit()
{ 
  $desc = $("#reason_desc").val(); 
  if($desc == ''){
     $("#reason_error").text("Please enter reason for reject.");
    return false;
  }
  //$('#loaderAjax').show();
  $.ajax({
   url: '<?php echo base_url(); ?>rewards/updateRewardStatus/',
   data: { reward_id: $('#reward_id').val(), rh_id: $('#h_id').val(), status: 2, user_id: $('#u_id').val(), reason_desc: $('#reason_desc').val()},
   //dataType: 'html',
   type: 'POST',
   success: function(data){
     // hide loader
     $('#loaderAjax').hide();
     $('#myModal').modal('hide');
     location.reload();
     }
  });
}
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reject Reason</h4>
      </div>
      <div class="modal-body">
        <form name="reason" id="reason">
        <h5>Reason for Reject</h5>
        <textarea name="reason" id="reason_desc" class="form-control"></textarea>
        <span id="reason_error" style="color:red;"></span><br /><br />
        <input type="hidden" id="h_id" name="h_id" value="" />
        <input type="hidden" id="u_id" name="u_id" value="" />
        <input type="hidden" id="reward_id" name="reward_id" value="" />
        <button class="btn btn-danger" type="button" name="addRewardBtn" id="addRewardBtn" onclick="reasonSubmit(); return false;">Submit
        </button>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
