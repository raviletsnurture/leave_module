<?php
$user = getAllUsers();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($leaveEdit[0]->leave_id)){ echo "Edit Exceptional Reward";} else{echo "Distribute Exceptional Reward to Team: Your Balance: <b>". $pmpoints[0]->pm_reward_points . '</b>';}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addReward', 'id' => 'addReward', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($leaveEdit[0]->leave_id)){
			  	echo form_open('rewards/addexpointsteam/'.$leaveEdit[0]->leave_id,$form_attributes);
			  }else{
				echo form_open(base_url().'rewards/addexpointsteam',$form_attributes);
			  }
			  ?>

			  <div class="form-group">
                <label  class="col-lg-2 control-label">User Name <span class="red">*</span></label>
                <div class="col-lg-6">

                  <select class="form-control" id="user_id" name="user_id[]" placeholder="User Name " multiple style='height: 200px;'>
                    <option value="">Select User</option>
                    <?php  foreach($users as $u): ?>
                    <option value="<?php echo $u->user_id; ?>"><?php echo $u->first_name . ' ' . $u->last_name; ?> (<?php echo $u->department_name; ?>)</option>
                  <?php endforeach; ?>
                  </select>
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Reward Point <span class="red">*</span></label>
                <div class="col-lg-6" id="pointsField">



                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Description <span class="red">*</span></label>
                <div class="col-lg-6">
                  <textarea class="form-control" id="description" name="description"   rows="3" cols="40" placeholder="Reward Description"><?php if(isset($leaveEdit[0]->leave_reason)){ echo $leaveEdit[0]->leave_reason;} ?></textarea>
                </div>
              </div>

               <!--<div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>-->
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <input type="hidden" name="pmpoints" id="pmpoints" value="<?php echo $pmpoints[0]->pm_reward_points;?>" />
                  <button class="btn btn-danger" type="submit" name="addEditLeave" id="addEditLeave">
                  <?php if(isset($leaveEdit[0]->leave_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-exreward.js"></script>
<script>
$(function() {
  $('#user_id').click(function () {

    $('option:selected', $(this)).each(function () {
      var data = '';
      var uid = $(this).val();
      //alert($('#reward_point_' +uid).length);
      if($('#rp_' +uid).length==0)
      {
        data = data + '<div id="rp_' +uid+ '"><b>' + $(this).text() + '</b><input type="text" class="form-control pmpoints_count" id="reward_point_' +uid+ '" name="reward_point_' +uid+ '" placeholder="Reward Point "  value="0"><br /></div>';
      }

      $('#pointsField').append(data);
    });

    $('option:not(:selected)', $(this)).each(function () {
        var uid = $(this).val();
        $('#rp_' +uid).remove();
    });
  });

  $('#addEditLeave').click(function(){
    //alert('this');
    var pmpoints = parseInt($('#pmpoints').val());
    var pmpoints_count = 0; //$('#pmpoints').val();
    $(".pmpoints_count").each(function(i, v){
        pmpoints_count = pmpoints_count + parseInt($(this).val());
    });
    if(pmpoints_count>pmpoints)
    {
      alert('The distributed points is more than the points you have.');
      return false;
    }else{
        return true;
    }

  });
});
</script>
