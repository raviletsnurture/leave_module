<?php
$user = getAllUsers();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($leaveEdit[0]->leave_id)){ echo "Edit Exceptional Reward";} else{echo "Distribute Exceptional Reward to Team: Your Balance: <b>". $pmpoints[0]->pm_reward_points . '</b>';}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
        <?php
  			  $form_attributes = array('name' => 'addReward', 'id' => 'addReward', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
  			  if(isset($leaveEdit[0]->leave_id)){
  			  echo form_open('rewards/addexpointsteam/'.$leaveEdit[0]->leave_id,$form_attributes);
  			  }else{
  				echo form_open(base_url().'rewards/addexpointsteam',$form_attributes);
  			  }
			  ?>
        <div class="form-group">
          <div class="col-lg-12">
              <button class="btn btn-default add_field_button">ADD MORE</button> 
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
                <label  class="col-lg-6 control-label">User Name <span class="red">*</span></label>               
                <label  class="col-lg-6 control-label">Reward Point <span class="red">*</span></label>
          </div> 
        </div>
			  <div class="form-group input_fields_wrap">
            <div class="input_fields_individual">
              <div class="col-lg-12">                
                <div class="col-lg-6">
                  <select class="form-control dropdown" id="user_id" name="user_id[]">
                    <option value="selectedValue">Select User</option>
                    <?php  foreach($users as $u): ?>
                    <option value="<?php echo $u->user_id; ?>"><?php echo $u->first_name . ' ' . $u->last_name; ?> (<?php echo $u->department_name; ?>)</option>
                  <?php endforeach; ?>
                  </select>
                </div>                
                <div class="col-lg-3">
                <input type="text" name="reward_point[]" onkeypress="javascript:return isNumber(event)" class="form-control reward_point">
                </div>
                <div class="col-lg-3 remove_button_add">                
                </div>
              </div>              
            </div>    
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Description <span class="red">*</span></label>
                <div class="col-lg-6">
                  <textarea class="form-control" id="description" name="description"   rows="3" cols="40" placeholder="Reward Description"><?php if(isset($leaveEdit[0]->leave_reason)){ echo $leaveEdit[0]->leave_reason;} ?></textarea>
                </div>
              </div>

               <!--<div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>-->
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <input type="hidden" name="pmpoints" id="pmpoints" value="<?php echo $pmpoints[0]->pm_reward_points;?>" />
                  <button class="btn btn-danger" type="submit" name="addEditLeave" id="addEditLeave">
                  <?php if(isset($leaveEdit[0]->leave_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-exreward.js"></script>
<script>
$(function() {  
  $(document).on('change', '.dropdown', function(event){         
    var selectedValue = $(event.currentTarget).val();    
    var matchedDropdowns = $('.dropdown').filter(function (index) {     
      if($(this).val() > 0 && selectedValue > 0){         
         return $(this).val() === selectedValue;       
      }      
    });    
    if (matchedDropdowns.length > 1) {
      $(event.currentTarget).prop('selectedIndex',0);
      alert("User Name already selected");
    }
  });  

    var wrapper = $(".input_fields_wrap"); //Fields wrapper    
    var x = 1; //initlal text box count
    $(".add_field_button").click(function(e){ //on add input button click      
        e.preventDefault();
        $anotherClonedContent = $(".input_fields_individual").eq(0).clone().addClass('remove_html'+x);         
        $(".input_fields_wrap").append($anotherClonedContent);     
        $(".remove_html"+x+" .remove_button_add").append('<a href="javascript:;" class="remove" id="'+x+'">Remove</a>');        
        $(".remove_html"+x+" #user_id").prop('selectedIndex',0);
        $(".remove_html"+x+" .reward_point").val('');
        $(".remove_html"+x+" select").css('border','1px solid #e2e2e4');
        $(".remove_html"+x+" .reward_point").css('border','1px solid #e2e2e4');
    x++; 
    });    
    $(document).on('click', '.remove', function(e){    
        e.preventDefault();                         
        $id = $(this).attr('id');        
        $('.remove_html'+$id).remove();        
    });    

 // $('#addEditLeave').click(function(){  
    $( "#addReward" ).submit(function( event ) {
    $(".input_fields_wrap select").each(function(){        
        if($(this).val() === 'selectedValue'){
            $(this).css('border','1px solid red');            
            event.preventDefault();
           return false;
        }else{
          $(this).css('border','1px solid #e2e2e4');
        }
    });
    $(".input_fields_wrap .reward_point").each(function(){        
        if($(this).val() == ''){
          $(this).css('border','1px solid red');
            event.preventDefault();
            return false;
        }else{
            $(this).css('border','1px solid #e2e2e4');
        }
    });
    var pmpoints = parseInt($('#pmpoints').val());
    var pmpoints_count = 0; //$('#pmpoints').val();
    $(".reward_point").each(function(i, v){
        pmpoints_count = pmpoints_count + parseInt($(this).val());
    });    
    if(pmpoints_count>pmpoints)
    {
      alert('The distributed points is more than the points you have.');
      event.preventDefault();
      return false;
    }else{
      return true;
    }

  });
});
function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if ((iKeyCode < 48 || iKeyCode > 57 ) && (iKeyCode != 8))
            return false;

        return true;
    }   
</script>
