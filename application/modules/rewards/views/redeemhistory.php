<style type="text/css">
.dataTables_filter {
     display: none;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	  <section class="panel">
        <header class="panel-heading">My History</header>
          <div class="panel-body">
            <div class="adv-table editable-table ">
              <?php if($this->session->flashdata('error')){?>
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> 
                </div>
              <?php } ?>
              <?php if($this->session->flashdata('success')){?>
              <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> 
              </div>
              <?php }?>
              <table class="table table-striped table-hover table-bordered" id="example">
                <thead>
                  <tr>
                    <th width="40%">Product</th>
                    <th width="5%">Points</th>
                    <th width="15%">Request Status</th>
                    <th width="15%">Applied Date</th>
                    <th width="20%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $cnt=1; foreach($redeemhostory as $row){?>
                  <tr>
                          <td><?php echo $row->name; ?></td>
                                  <td><?php echo $row->point; ?></td>
                          <td><?php if($row->status == '0') {
                                echo "Pending";
                            } else if($row->status == '1') {
                                echo "Accepted";
                            } else if($row->status == '2')  {
                                echo "Declined";
                            } else if($row->status == '3')  {
                                echo "Cancelled";
                            } ?>
                          </td>
                          <td><?php echo date("d-m-Y", strtotime($row->created_at)) ?></td>
                          <td><?php if($row->status == '0'){ ?>
                                <a href="#" class="" onclick="cancel(<?php echo $row->id; ?>)">
                                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Cancel&nbsp;Request"><i class="fa fa-exclamation-triangle"></i></button>
                                </a>
                              <?php }?>
                          </td>
                  </tr>
                  <?php $cnt++; }?>
                </tbody>
              </table>
            </div>
          </div>
			  <!-- My Leaves end-->
    <!-- page end-->
    </section>
  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script>
$(document).ready(function() {

	 var dt =  $('#example').dataTable( {
		 "aaSorting": [[ 3, "asc" ]],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : true,

		 "oSearch": { "bSmart": false, "bRegex": true },
		 "aoColumnDefs": [
			{
			'bSortable': true, 'aTargets': [ 3 ]
			}
		 ]
		});
});

function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel this Redeem request?");
	if (r == true){
			$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/redeemcancel',
		 data: { r_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 location.reload();
	     },
    error: function(xhr) { // if error occured
            alert("Error occured. Please try again");
        },
		});
		//location.reload();
	} else {
	}
}
</script>
