<style type="text/css">
.dataTables_filter {
     display: none;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading"> My Rewards</header>
        <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
			<!-- <div class="row">
			    <div class="col-lg-12">
					<div id="editable-sample_length" class="dataTables_length">
						<div class="btn-group">
							<a href="<?php echo base_url()?>rewards/add">
								<button class="btn btn-info" id="editable-sample_new"> Reward Request <i class="fa fa-plus"></i> </button>
							</a>
						</div>
					</div>
				</div>
            </div> -->
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th width="5%">ID</th>
                <th width="15%">Reward</th>
        				<th width="5%">Points</th>
                <th width="20%">Description</th>
        				<th width="10%">Request Status</th>
        				<th width="10%">Applied Date</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($rewards as $row){

              ?>
              <tr>
				<td><?php echo $row->rh_id; ?></td>
        <td>
          <?php
          if(isset($row->reward_name) && strlen($row->reward_name)>0)
          {
            echo $row->reward_name;
          }else{
            echo $row->reward_type_name;
          }
          ?>
        </td>
        <td>
            <?php
            if(isset($row->history_point) && strlen($row->history_point)>0)
            {
              echo $row->history_point;
            }else{
              echo $row->reward_type_point;
            }
             ?>
        </td>

        <td>
            <?php
            echo $row->description;
             ?>
        </td>
				<td><?php if($row->reward_status == '0') {
                            echo "Pending";
                        } else if($row->reward_status == '1') {
                            echo "Accepted";
                        } else if($row->reward_status == '2')  {
                            echo "Declined";
                        } else if($row->reward_status == '3')  {
                            echo "Cancelled";
                        } ?></td>
				<td><?php echo date("d-m-Y", strtotime($row->date)) ?></td>
				<td>
          <?php if($row->reward_status == '0'){ ?>
                <a href="#" class="" onclick="cancel(<?php echo $row->rh_id; ?>)">
                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Cancel&nbsp;Request"><i class="fa fa-exclamation-triangle"></i></button>
                </a>
                <?php }?>
                <?php if($row->reward_status == '0'){ ?>
                <a href="<?php echo base_url(); ?>rewards/editReward/<?php echo $row->rh_id; ?>" class="" >
                <button class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Edit
                &nbsp;Request"><i class="fa fa-pencil"></i></button>
                </a>
                <?php }?>
          <?php if(strlen($row->reply_description)>0){ ?>
            <button class="btn btn-info btn-xs tooltips" data-toggle="tooltip" data-original-title="<?php echo $row->reply_description; ?>" title=""><i class="fa fa-comment"></i></button>
          <?php }?>
        </td>
              </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>
			  <!-- My Leaves end-->
    <!-- page end-->
  </section>
</section>
</section>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script>
$(document).ready(function() {

	 var dt =  $('#example').dataTable( {
		 "aaSorting": [[ 0, "desc" ]],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : false,

		 "oSearch": { "bSmart": false, "bRegex": true },
     /*
		 "aoColumnDefs": [
			{
			'bSortable': true, 'aTargets': [ 3 ]
			}
    ]*/
		});
});

function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel Reward!");
	if (r == true){
			$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/cancel',
		 data: { reward_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 location.reload();
	     },
    error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
		});
		//location.reload();
	} else {
	}
}
</script>
