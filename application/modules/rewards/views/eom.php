<style type="text/css">
.dataTables_filter {
     display: none;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading">Employee of the Month</header>
        <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
			<div class="row">
			    <div class="col-lg-12">
					<div id="editable-sample_length" class="dataTables_length">
					<!--	<div class="btn-group">
							<a href="<?php echo base_url()?>rewards/add">
								<button class="btn btn-info" id="editable-sample_new"> Reward Request <i class="fa fa-plus"></i> </button>
							</a>
						</div>
          -->
					</div>
				</div>
            </div>
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
        <th width="5%">ID</th>
        <th width="15%">Employee Name</th>
				<th width="5%">Points</th>
        <th width="20%">Description</th>
				<th width="10%">Month</th>

              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($eomData as $row){
              //  echo '<pre>';
              //  print_r($row);
              ?>
              <tr>
				<td><?php echo $row->id; ?></td>
        <td>
          <?php
            echo $row->first_name . ' ' . $row->last_name;
          ?>

        </td>
        <td>
            <?php
            if(isset($row->reward_type_point) && strlen($row->reward_type_point)>0)
            {
              echo $row->reward_type_point;
            }else{
              echo $row->point;
            }
             ?>
        </td>

        <td>
            <?php
            echo $row->description;
             ?>
        </td>
				<td>
        <?php
          echo date('F Y', strtotime($row->month));
        ?>
        </td>
				      </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>
			  <!-- My Leaves end-->
    <!-- page end-->
    </section>  
  </section>
</section>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script>
$(document).ready(function() {

	 var dt =  $('#example').dataTable( {
		 "aaSorting": [[ 0, "desc" ]],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : false,

		 "oSearch": { "bSmart": false, "bRegex": true },
     /*
		 "aoColumnDefs": [
			{
			'bSortable': true, 'aTargets': [ 3 ]
			}
    ]*/
		});
});

function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel Reward!");
	if (r == true){
			$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/cancel',
		 data: { reward_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 location.reload();
	     },
    error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
		});
		//location.reload();
	} else {
	}
}
</script>
