<?php
	$user = getAllUsers();
	$all_department = getAllDepartment();
	for ($i=0; $i<sizeof($permission); $i++) {
		$nm = $permission[$i]->module_name;
		$arr[$nm] = $permission[$i];
	}
?>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
.reward_description {
    border: 1px solid #E1E1E3;
    width: 47%;
    margin-left: 16px;
    font-family: sans-serif;
    border-radius: 4px;
    line-height: 25px;
    color: #6B6B6B;
    font-size: 13px;
}
</style>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Add Reward
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
								<?php
									$form_attributes = array('name' => 'addReward', 'id' => 'addReward', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
									echo form_open(base_url().'rewards/add',$form_attributes);
								?>
								<div class="form-group">
									<label for="firstName" class="control-label col-lg-2">User <span class="red">*</span></label>
									<div class="col-lg-6">
										<select name="user_id" id="user_id" disabled="disabled" class="form-control">
											<option value="">Select User</option>
											<?php foreach($user as $dd) { ?>
												<option value="<?php echo $dd->user_id; ?>" <?php $value1 = (isset($login_session[0]->user_id)) ? $login_session[0]->user_id : 0; echo selectedVal($value1,$dd->user_id); ?>>
													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
												</option>
											<?php } ?>
										</select>
									</div>

								</div>

								<div class="form-group ">
									<label for="project_id" class="control-label col-lg-2">Reward Type <span class="red">*</span></label>
									<div class="col-lg-6">
										<select name="reward_type" id="reward_type" class="form-control">
											<option value="">Select Reward Type</option>
											<?php foreach($total_reward_types as $dd1) { ?>
												<option value="<?php echo $dd1->id; ?>"><?php echo $dd1->reward_name . ' - ' . $dd1->point . ' Pts'; ?></option>
											<?php } ?>
										</select>
									</div>

								</div>
								<div class="form-group rewardDes" style="display: none;">
								<label class="col-lg-2 control-label">Description</label>
								    <div class="col-lg-6 reward_description" ></div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Date <span class="red">*</span></label>
									<div class="col-lg-2">
										<input type="text" class="form-control" readonly="readonly" id="date" name="date" data-date-format="yyyy-mm-dd">
									</div>
								</div>

								<div class="form-group">
									<label  class="col-lg-2 control-label">Description <span class="red">*</span></label>
									<div class="col-lg-6">
										<textarea class="form-control" style="resize:none;" id="description" name="description"   rows="3" cols="40" placeholder="Description for Reward"></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-danger" type="submit" name="addRewardBtn" id="addRewardBtn">Submit
										</button>
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
									</div>
								</div>
								<!-- Hidden field for json data -->
								<input type="hidden" name="reward_message" id="reward_message" value="" >
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-reward.js"></script>

<script type="text/javascript">
	$('#date').datepicker({
    endDate: today,
   	autoclose: true,
	});

</script>

<script type="text/javascript">
// $(document).ready(function(){
// 	$('#addRewardBtn').click(function(event){
// 		$(':input[type="submit"]').prop('disabled', true);
//         $('#addReward').submit();
// 	});
// });

$("#reward_type").change(function () {
        var val = this.value;
        if(val == ''){
           $(".rewardDes").hide();
        }else{
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>rewards/getRewarddescription',
			data: 'id='+ val,
			beforeSend: function(){
			},
			success:function(data){
        data = JSON.parse(data);
        if(data.description != ''){
        	$(".rewardDes").show();
                $(".reward_description").html(data.description);
        }else{
                $(".rewardDes").hide();
        }
			}
 		});
        }
    });
</script>
