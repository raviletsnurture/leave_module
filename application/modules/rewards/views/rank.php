<style type="text/css">
.dataTables_filter {
     display: none;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading">User Reward Rank</header>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th width="20%">Full Name</th>
                <th>Department</th>
                <th width="30%">Karma Points</th>
                <th width="10%">Rank</th>
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1;
              $rankNumber = 0;
              $prevPoints = 0;
              //header('Content-Type: application/json');
              foreach($rankWiseData as $row){
                if($prevPoints != $row->karma_points)
                {
                  $prevPoints = $row->karma_points;
                  $rankNumber++;
                }else{
                  $prevPoints = $row->karma_points;
                }
                ?>
              <tr>
        		<td><?php echo $row->first_name.' '.$row->last_name;?></td>
                <td><?php echo $row->department_name;?></td>
                <td><?php echo $row->karma_points; ?></td>
                <td><?php echo $rankNumber; ?></td>
              </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>
			  <!-- My Leaves end-->
    <!-- page end-->
	</section>
  </section>
</section>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script>
$(document).ready(function() {

	 var dt =  $('#example').dataTable( {
		 "aaSorting": [[ 3, "desc" ]],
		 "iDisplayLength": 25,
		 "pagingType": "full_numbers",
		 
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : false,
         "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
		 "oSearch": { "bSmart": false, "bRegex": true },		 
		  "aoColumnDefs": [{ "bSortable": true, "aTargets": [0, 1, 2, 3] }]
		});
});

function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel Reward!");
	if (r == true){
			$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/cancel',
		 data: { reward_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 location.reload();
	     },
    error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
		});
		//location.reload();
	} else {
	}
}

function ff(sel)
{
	var ff = sel.value.split(",");
	if(ff == '')
	{
		alert("Please Select an Option");
		location.reload();
		//return false;
	}
	else
	{
		var r = confirm("Are You Sure for this operation");
		if (r == true)
		{
			// show loader
			$('#loaderAjax').show();
			$.ajax({
			 url: '<?php echo base_url(); ?>rewards/updateRewardStatus/',
			 data: { reward_id: ff[0], status: ff[1], user_id: $(sel).attr('user_id')},
			 dataType: 'html',
			 type: 'POST',
			 success: function(data){
				 // hide loader
				 $('#loaderAjax').hide();
				 //$('.modal-body').html(data);
				 //$('#myModal').modal('show');
				 //alert("Leave Status Has Been Changed");
				 location.reload();
				 }
			});
		}
		else {

		}
	 }

}
</script>
