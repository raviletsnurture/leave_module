<?php
	$user = getAllUsers();
	$all_department = getAllDepartment();

?>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Redeem Reward
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">

							<div class="container" style="width:auto;">
							    <div class="row">
							    	<div class="col-md-10">
							<?php foreach ($products as $product):
								if($product->image)
								{
									$img = base_url() . 'uploads/products/' . $product->image;
								}else{
									$img = base_url() . 'assets/img/default_product.jpg';
								}
								?>
										<div class="col-sm-6 col-md-4" style="height: 394px !important;">
											<div class="thumbnail" >
												<img src="<?php echo $img; ?>" width="300" style="height:190px !important" height="300" class="img-responsive">
												<div class="caption">
													<div class="row">
														<div class="col-md-8 col-xs-8">
															<h5><?php echo $product->name; ?></h5>
														</div>
														<div class="col-md-4 col-xs-4 price">
															<h6>
															<label><?php echo number_format($product->point); ?> Pts</label></h6>
														</div>
													</div>
													<p><?php echo $product->description; ?></p>
													<div class="row">
														<!--
														<div class="col-md-6">
															<a href="#" class="btn btn-success btn-product"><span class="glyphicon glyphicon-shopping-cart"></span> Details</a>
														</div>
													-->
														<div class="col-md-6">
															<a href="#" class="btn btn-success btn-product" onclick="claimReward(this); return false;" p_id="<?php echo $product->id;?>" user_id="<?php echo $user_id;?>"><span class="glyphicon glyphicon-shopping-cart"></span> Redeem</a>
														</div>
													</div>

													<p> </p>
												</div>
											</div>
										</div>


									<?php endforeach; ?>
							        </div>

								</div>
							</div>

					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-reward.js"></script>

<script type="text/javascript">
	$('#date').datepicker({
    endDate: today,
   	autoclose: true,
	});

</script>

<script type="text/javascript">
$(document).ready(function(){
	$('#addRewardBtn').click(function(event){
        $('#addReward').submit();
	});
});

function claimReward(self)
{
	var r = confirm("Are You Sure. You want to redeem this product?");
	if (r == true)
	{
		// show loader
		$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/redeemreward/',
		 data: { p_id: $(self).attr('p_id'), user_id: $(self).attr('user_id')},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 if(data==1)
			 {
				 alert("Your request has been sent.");
				 location.reload();
			 }else if(data==0){
				 alert('You dont have enough reward points to redeem this product');
			 }else if(data==2){
				 alert('You have already redeemed reward for this month');
			 }
			 }
		});
	}
}
</script>
