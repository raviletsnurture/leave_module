<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rewards extends MX_Controller {
  var $logSession;
	function __construct(){
		parent::__construct();
    $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('rewards_model');
    $this->load->library('email');
	}

	function index(){
		   $data['titalTag'] = ' - Rewards';
       $userData = $this->logSession;
		   $id = $userData[0]->user_id;
	     $data["rewards"] = $this->rewards_model->getAllRewardsHistory($id);
       $this->template->load_partial('dashboard_master','rewards/index',$data);
	}


  function eom(){
		   $data['titalTag'] = ' - Employee of the Month';
       $userData = $this->logSession;
		   $id = $userData[0]->user_id;
	     $data["eomData"] = $this->rewards_model->getAllEOMHistory($id);
       $this->template->load_partial('dashboard_master','rewards/eom',$data);
	}

  function redeemhistory(){
		   $data['titalTag'] = ' - Redeem History';
       $userData = $this->logSession;
		   $id = $userData[0]->user_id;
	     $data["redeemhostory"] = $this->rewards_model->getAllRewardsRedeemHistory($id);
       $this->template->load_partial('dashboard_master','rewards/redeemhistory',$data);
	}

  function claim()
  {
      $data['titalTag'] = ' - Redeem Product';
      $data["products"] = $this->rewards_model->getRewardProducts();
      $userData = $this->logSession;
      $data["user_id"] = $userData[0]->user_id;
      $this->template->load_partial('dashboard_master', 'rewards/redeem', $data);
  }

    function add($rewardId = ''){
        $data['titalTag'] = ' - Add Reward';
		    $loginSession = $this->session->userdata("login_session");
		    $role_id = $loginSession[0]->role_id;
        $user_id = $loginSession[0]->user_id;
		    $department_id = $loginSession[0]->department_id;
		    $per = getUserpermission($role_id);

		    $data['login_session'] = $loginSession;
		    $data['permission'] = $per;
        $data["rewards"] = $this->rewards_model->getAllRewardsHistory($user_id);
        $data["total_reward_types"] = $this->rewards_model->getAllTotalRewardTypes($user_id);

        if($this->input->post('reward_type') &&  $this->input->post('reward_type') != ''){
            $reward_id = $this->input->post('reward_type');
            $reward_date = $this->input->post('date');
            $description = $this->input->post('description');
            $rewardDetails = $this->rewards_model->getRewardDetailsById($reward_id);
            $addData['user_id'] = $user_id;
            $addData['reward_id'] = $reward_id;
            $addData['date'] = $reward_date;
            $addData['description'] = $description;
            $addData['reward_name'] = $rewardDetails[0]->reward_name;
            $addData['point'] = $rewardDetails[0]->point;

            $addDatasucess = $this->rewards_model->add($addData);

            if($addDatasucess){
                $rewardDetails = $this->rewards_model->getRewardDetailsById($reward_id);
                $rewardName = $rewardDetails[0]->reward_name;
                //Send Alert to TLs , PM & HR.
                $managementEmails = getSeniorEmails($addData['user_id'], 'feedback_users');
                //$managementEmails = 'hitendra@letsnurture.com';

                $mailData['body'] = '<tr>
                              <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$loginSession[0]->first_name.' '.$loginSession[0]->last_name.'</strong> has requested reward for <strong style="color:#03A9F4;">'.$rewardName.'</strong></p></td>
                              </tr>
                              <tr>
                                <td colspan="2" style="font-size:14px; color:#FFF;
                                padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'.$description.'
                                </td>
                              </tr>';
                $mailData['name'] = '';

                $this->email->from('hrms@letsnurture.com', "HRMS");
                $this->email->to('hrms@letsnurture.com');
                //$this->email->to('rajendra.letsnurture@gmail.com');
                $this->email->cc($managementEmails);
                $this->email->subject("Request for Reward");
                $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                $this->email->message($body);
                //$this->email->send();
                redirect(base_url() . 'rewards');
                exit();
            }
        }
        $this->template->load_partial('dashboard_master','rewards/add_reward',$data);
    }

    function editReward($edit_id){
        $data['titalTag'] = ' - Edit Reward';
        $loginSession = $this->session->userdata("login_session");
        $role_id = $loginSession[0]->role_id;
        $user_id = $loginSession[0]->user_id;

        $match_data = $this->rewards_model->match_data($edit_id,$user_id);
        if($match_data){

        $department_id = $loginSession[0]->department_id;
        $per = getUserpermission($role_id);

        $data['login_session'] = $loginSession;
        $data['permission'] = $per;
        $data["rewards"] = $this->rewards_model->getAllRewardsHistory($user_id);
        $data["total_reward_types"] = $this->rewards_model->getAllTotalRewardTypes($user_id);
        $data["user_reward_detail"] = $this->rewards_model->getRewardHistory($edit_id);
        $this->template->load_partial('dashboard_master','rewards/edit_reward',$data);


       }else{
          redirect(base_url() . 'rewards');
       }
    }

    function updateReward(){
      if( $this->input->post('reward_type') &&  $this->input->post('reward_type') != ''){

            $loginSession = $this->session->userdata("login_session");
            $user_id = $loginSession[0]->user_id;

            $reward_id = $this->input->post('reward_type');
            $reward_date = $this->input->post('date');
            $description = $this->input->post('description');
            $reward_history_id = $this->input->post('reward_history_id');
            $rewardDetails = $this->rewards_model->getRewardDetailsById($reward_id);
            $addData['user_id'] = $user_id;
            $addData['reward_id'] = $reward_id;
            $addData['date'] = $reward_date;
            $addData['description'] = $description;
            $addData['reward_name'] = $rewardDetails[0]->reward_name;
            $addData['point'] = $rewardDetails[0]->point;


            $addDatasucess = $this->rewards_model->update($addData,$reward_history_id);

            if($addDatasucess){
                $rewardDetails = $this->rewards_model->getRewardDetailsById($reward_id);
                $rewardName = $rewardDetails[0]->reward_name;
                //Send Alert to TLs , PM & HR.
                $managementEmails = getSeniorEmails($addData['user_id'], 'feedback_users');
                //$managementEmails = 'hitendra@letsnurture.com';

                $mailData['body'] = '<tr>
                              <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$loginSession[0]->first_name.' '.$loginSession[0]->last_name.'</strong> has requested reward for <strong style="color:#03A9F4;">'.$rewardName.'</strong></p></td>
                              </tr>
                              <tr>
                                <td colspan="2" style="font-size:14px; color:#FFF;
                                padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'.$description.'
                                </td>
                              </tr>';
                $mailData['name'] = '';

                $this->email->from('hrms@letsnurture.com', "HRMS");
                $this->email->to('hrms@letsnurture.com');
                //$this->email->to('rajendra.letsnurture@gmail.com');
                $this->email->cc($managementEmails);

               /* $this->email->from('dipika.letsnurture@gmail.com', "HRMS");
                $this->email->to('dipika.letsnurture@gmail.com');*/

                $this->email->subject("Change Request for Reward");
                $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                $this->email->message($body);
                //$this->email->send();
                redirect(base_url() . 'rewards');
                exit();
            }
        }
    }


    function cancel(){
        $reward_id = $this->input->post('reward_id');
        $data['status'] = '3';

        $cancelRewardSucess = $this->rewards_model->cancelReward($data, $reward_id);
        if($cancelRewardSucess){
          $this->session->set_flashdata('success','Reward cancelled successfully!');
        }else{
          $this->session->set_flashdata('error','You cannot cancel this request type.');
        }

    }

    function redeemcancel(){
        $reward_id = $this->input->post('r_id');
        $data['status'] = '3';

        $loginSession = $this->session->userdata("login_session");
        $role_id = $loginSession[0]->role_id;
        $user_id = $loginSession[0]->user_id;

        $reward_user = $this->rewards_model->getRewardUser($user_id);
        $reward_data = $this->rewards_model->getUserRedeemHistory($reward_id, $user_id);
        $data2['reward_points'] =  $reward_user[0]->reward_points + $reward_data[0]->point;
        $res = $this->rewards_model->updateRewardUser($data2, $user_id);

        $cancelRewardSucess = $this->rewards_model->cancelRedeem($data, $reward_id);
        if($cancelRewardSucess){
          $this->session->set_flashdata('success','Redeem Request cancelled successfully!');
        }else{
          $this->session->set_flashdata('error','You cannot cancel this Redeem Request');
        }

    }

    function requets(){
      $data['titalTag'] = ' - Reward Requests';
      $loginSession = $this->session->userdata("login_session");
      $getUser = getUserByid($loginSession[0]->user_id);
      $data['user_id_array'] = explode("," ,$getUser[0]['reimbursement_users']);
      $role_id = $loginSession[0]->role_id;
      $user_id = $loginSession[0]->user_id;
      $department_id = $loginSession[0]->department_id;
      $data["rewards"] = $this->rewards_model->getAllRewardRequests($user_id, $department_id, $data['user_id_array']);
      $data["user_id"] = $user_id;
      $data["department_id"] = $department_id;
      $data["login_role_id"] = $role_id;
      $this->template->load_partial('dashboard_master','rewards/reward_requests',$data);
    }

    function rank(){
      $data['titalTag'] = ' - Reward Ranks';
      $loginSession = $this->session->userdata("login_session");
      $user_id = $loginSession[0]->user_id;
      $data["rank"] = getUserRank($user_id);
      $data["rankWiseData"] = $this->rewards_model->getRewardUserRankWise($user_id);
      $this->template->load_partial('dashboard_master','rewards/rank',$data);
    }


    function redeemreward(){

      $loginSession = $this->session->userdata("login_session");
      $user_id = $loginSession[0]->user_id;
      $p_id = $this->input->post('p_id');

      $data['user_id'] = $user_id;
      $data['product_id'] = $p_id;
      $data['status'] = 0;
      $data['created_at'] = date('Y-m-d h:m:s');

      $reward_user = $this->rewards_model->getRewardUser($user_id);
      $product = $this->rewards_model->getProduct($p_id);

      if($reward_user[0]->reward_points-$product[0]->point >= 0)
      {
        /*if($this->rewards_model->getThisMonthRewardCount($user_id) > 0)
        {
            echo "2";
        }else{*/
          $data2['reward_points'] = $reward_user[0]->reward_points - $product[0]->point;
          // $data2['karma_points'] = $reward_user[0]->karma_points - $product[0]->point;
          $res = $this->rewards_model->updateRewardUser($data2, $user_id);

          $this->rewards_model->addRedeemRequest($data);
          //Send Alert to HR.
          $mailData['body'] = '<tr><td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">
                        <strong style="color:#03A9F4;">'.$reward_user[0]->first_name.' '.$reward_user[0]->last_name.'</strong> has claimed
                        <strong style="color:#03A9F4;">'.$product[0]->name.'</strong> Reward</p></td>
                        </tr>';
          $mailData['name'] = 'Admin';

          $this->email->from('hrms@letsnurture.com', "HRMS");
          $this->email->to('hr@letsnurture.com');
          $this->email->subject("Claim Reward");
          $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
          $this->email->message($body);
          //$this->email->send();

          echo "1";
       /* }*/
      }else{
          echo "0";
      }
    }

    function updateRewardStatus(){
        $reward_id = $this->input->post('reward_id');
        $status = $this->input->post('status');
        $user_id = $this->input->post('user_id');
        $rh_id = $this->input->post('rh_id');

        $loginSession = $this->session->userdata("login_session");
    		$user_id2 = $loginSession[0]->user_id;
        $data['status'] = $status;
        $data['reward_type'] = 1;
        if($this->input->post('reason_desc'))
        {
          $data['reply_description'] = $this->input->post('reason_desc');
        }

        $reward_data = $this->rewards_model->getReward($reward_id);
        $reward_user = $this->rewards_model->getRewardUser($user_id);
        $reward_status = $this->rewards_model->getRewardHistory($rh_id);
        $data2 = array();
        if($status == 1)
        {

          if($reward_status->status != 1){
          $data2['karma_points'] =  $reward_user[0]->karma_points + $reward_data[0]->point;
          $data2['reward_points'] = $reward_user[0]->reward_points + $reward_data[0]->point;
          $res = $this->rewards_model->updateRewardUser($data2, $user_id);
          if($res){
              $rewardDetails = $this->rewards_model->getRewardDetailsById($reward_id);
              $rewardName = $rewardDetails[0]->reward_name;
              //Send Alert to TLs , PM & HR.
              $managementEmails = getSeniorEmails($user_id, 'feedback_users');
              //$managementEmails = 'hitendra@letsnurture.com';

              $mailData['body'] = '<tr>
                            <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$reward_user[0]->first_name.' '.$reward_user[0]->last_name.'</strong> your request for <strong style="color:#03A9F4;">'.$rewardName.'</strong> reward has beed approved<br />Reward Point: '.$reward_data[0]->point.'<br />Your Reward Balance: '.$data2['reward_points'].'</p></td>
                            </tr>
                            <tr>
                              <td colspan="2" style="font-size:14px; color:#FFF;
                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'. ' Reward Approved'.'
                              </td>
                            </tr>';
              $mailData['name'] = '';

              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
              //$this->email->to('rajendra.letsnurture@gmail.com');
              $this->email->cc($managementEmails);
              $this->email->subject("Reward Approved");
              $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
              $this->email->message($body);
              //$this->email->send();
          }
          }
        }else if($status == 2){
          if($reward_status->status == 1){
              $data3['reward_points'] =  $reward_user[0]->reward_points - $reward_data[0]->point;
              $data3['karma_points'] =  $reward_user[0]->reward_points - $reward_data[0]->point;
              $res = $this->reward_model->updateRewardUser($data3, $user_id);
          }
        }
        if($status == 1 || $status == 0){
          $data['approved_by'] = $user_id2;
        }else{
          $data['approved_by'] = $user_id2;
        }

        $updateRewardStatusSucess = $this->rewards_model->updateRewardStatus($data, $rh_id);

    }

    function expoints()
    {
        $data['titalTag'] = ' - Exceptional Rewards';
        $loginSession = $this->session->userdata("login_session");
		    $role_id = $loginSession[0]->role_id;
        $user_id = $loginSession[0]->user_id;
		    $department_id = $loginSession[0]->department_id;
		    $per = getUserpermission($role_id);

        $data["users"] = $this->rewards_model->getAllUsers($department_id);

        $this->template->load_partial('dashboard_master', 'rewards/exreward', $data);
    }

    function expointsteam()
    {
        $data['titalTag'] = ' - Exceptional Rewards';
        $loginSession = $this->session->userdata("login_session");
		    $role_id = $loginSession[0]->role_id;
        $user_id = $loginSession[0]->user_id;
		    $department_id = $loginSession[0]->department_id;
		    $per = getUserpermission($role_id);

        $data["users"] = $this->rewards_model->getAllUsers($department_id);
        $data["pmpoints"] = $this->rewards_model->getPMPoints($user_id);

        $this->template->load_partial('dashboard_master', 'rewards/exrewardteam', $data);
    }

    public function addexpointsteam(){
        if($this->input->post('user_id'))
        {
          $user_ids        = $this->input->post('user_id');
          $reward_point        = $this->input->post('reward_point');
          $loginSession = $this->session->userdata("login_session");
  		    $role_id = $loginSession[0]->role_id;
          $user_id = $loginSession[0]->user_id;
  		    $department_id = $loginSession[0]->department_id;
          $pmpoints = 0;
          for($i=0;$i<count($user_ids);$i++)
          {
            $data = array();
            $data['user_id'] = $user_ids[$i];
      			$data['description']   = trim($this->input->post('description'));
            $data['point']   = $reward_point[$i];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['date'] = $data['created_at'];
            $data['status'] = 1;
            $data['reward_name'] = 'Exeptional Reward';
            $data['approved_by'] = $user_id;
            $data['reward_type'] = 1;
            $reward_user = $this->rewards_model->getRewardUser($user_ids[$i]);
            $data2 = array();

            $data2['reward_points'] = $reward_user[0]->reward_points + $data['point'];
            $data2['karma_points'] = $reward_user[0]->karma_points + $data['point'];
            $res = $this->rewards_model->updateRewardUser($data2, $user_ids[$i]);
            $pmpoints = $pmpoints + $data['point'];
            $this->rewards_model->add($data);
          }
          $pmdata['pm_reward_points'] = trim($this->input->post('pmpoints')) - $pmpoints;
          $this->rewards_model->updateRewardUser($pmdata, $user_id);
    			$this->session->set_flashdata('success', 'Team Exeption Reward Added Sucessfully');
        }else{
          $this->session->set_flashdata('error', 'Please provide data');
        }

  			redirect(base_url() . 'rewards/expointsteam');
  			exit;

      }

    public function exaddReward(){
        if($this->input->post('user_id'))
        {
          $data = array();
    			$data['user_id']        = trim($this->input->post('user_id'));
    			$data['description']   = trim($this->input->post('description'));
          $data['point']   = trim($this->input->post('reward_point'));
          $data['created_at'] = date('Y-m-d H:i:s');
          $data['date'] = $data['created_at'];
          $data['status'] = 1;
          $data['reward_name'] = 'Exeptional Reward';
          $data['reward_type'] = 0;

          $loginSession = $this->session->userdata("login_session");
  		    $role_id = $loginSession[0]->role_id;
          $user_id = $loginSession[0]->user_id;
  		    $department_id = $loginSession[0]->department_id;
          $data['approved_by'] = $user_id;

          $reward_user = $this->rewards_model->getRewardUser($data['user_id']);
          $data2 = array();

          $data2['reward_points'] = $reward_user[0]->reward_points + $data['point'];
          $res = $this->rewards_model->updateRewardUser($data2, $data['user_id']);

          //$this->rewards_model->addExReward($data);
          $this->rewards_model->add($data);

    			$this->session->set_flashdata('success', 'Exeption Reward Added Sucessfully');
        }else{
          $this->session->set_flashdata('error', 'Please provide data');
        }

        //Push Notification Logic
  			redirect(base_url() . 'rewards/expoints');
  			exit;

      }

      function negative()
      {
          $data['titalTag'] = ' - Nagative Rewards';
          $loginSession = $this->session->userdata("login_session");
          $role_id = $loginSession[0]->role_id;
          $user_id = $loginSession[0]->user_id;
          $department_id = $loginSession[0]->department_id;
          $per = getUserpermission($role_id);
          $data["users"] = $this->rewards_model->getAllUsers($department_id);

          //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

          $this->template->load_partial('dashboard_master', 'rewards/nagative', $data);
      }


    public function addNegativeReward(){

      if($this->input->post('user_id'))
      {
        $data = array();
  			$data['user_id']        = trim($this->input->post('user_id'));
  			$data['description']   = trim($this->input->post('description'));
        $data['point']   = trim($this->input->post('reward_point')) * -1;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['date'] = $data['created_at'];
        $data['status'] = 1;
        $data['reward_name'] = 'Negative Reward';
        $data['reward_type'] = 1;

        $loginSession = $this->session->userdata("login_session");
        $role_id = $loginSession[0]->role_id;
        $user_id = $loginSession[0]->user_id;
        $department_id = $loginSession[0]->department_id;
        $data['approved_by'] = $user_id;

        $reward_user = $this->rewards_model->getRewardUser($data['user_id']);
        $data2 = array();

        $data2['reward_points'] = $reward_user[0]->reward_points + $data['point'];
        $data2['karma_points'] = $reward_user[0]->karma_points + $data['point'];
        $res = $this->rewards_model->updateRewardUser($data2, $data['user_id']);

        $this->rewards_model->add($data);

  			$this->session->set_flashdata('success', 'Negative Reward Added Sucessfully');
      }else{
        $this->session->set_flashdata('error', 'Please provide data');
      }

      //Push Notification Logic
			redirect(base_url() . 'rewards/negative');
			exit;

    }

    function getRewarddescription(){
      $reward_id = $this->input->post('id');
      $reward_data = $this->rewards_model->getReward($reward_id);
      if($reward_data){
        $data['description'] =  $reward_data[0]->description;
      }else{
        $data['description'] =  "0";
      }
      echo json_encode($data);
    }
}
?>
