<?php
class rewards_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}


	function getAllRewardsHistory($id){
		$this->db->select("rh.*, u.reward_points,rh.point as history_point, rt.point as reward_type_point, rt.reward_name as reward_type_name, rh.status as reward_status, rh.id as rh_id, rt.id as reward_type_id, rt.point as reward_given_point");
		$this->db->from("crm_reward_history as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->join('crm_reward_type as rt','rt.id = rh.reward_id', 'left');
		$this->db->where("rh.user_id = $id");
		$this->db->order_by("date","desc");
		$result = $this->db->get()->result();
		//echo $sql = $this->db->last_query();
		//exit;
		return $result;
	}

	function getAllEOMHistory($id){
		$this->db->select("rh.*, u.first_name, u.last_name");
		$this->db->from("crm_reward_eom as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->order_by("rh.id","desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRewardsRedeemHistory($id){
		$this->db->select("rh.*, rp.name, rp.point, rp.description, rp.status as status2");
		$this->db->from("crm_reward_redeems as rh");
		$this->db->join('crm_reward_products as rp','rp.id = rh.product_id', 'left');
		$this->db->where("rh.user_id = $id");
		$this->db->order_by("rh.created_at","desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllTotalRewardTypes($id){
		$this->db->select("*");
		$this->db->from("crm_reward_type");
		$this->db->where("status = '1'");
		$result = $this->db->get()->result();
		return $result;
	}

	function add($data){
	    if($this->db->insert('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}
	function update($data,$r_id){
		$this->db->where('id',$r_id);
	    if($this->db->update('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}


	function addRedeemRequest($data){
			if($this->db->insert('crm_reward_redeems',$data)){
			return true;
		}else{
			return false;
		}
	}

	function cancelReward($data, $reward_id){
		$this->db->where('id',$reward_id);
		if($this->db->update('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function cancelRedeem($data, $r_id){
		$this->db->where('id',$r_id);
		if($this->db->update('crm_reward_redeems',$data)){
			return true;
		}else{
			return false;
		}
	}

	function updateRewardStatus($data, $reward_id){
		$this->db->where('id',$reward_id);
		if($this->db->update('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getRewardDetailsById($id){
		$this->db->select("*");
		$this->db->from("crm_reward_type");
		$this->db->where("id = $id");
		$result = $this->db->get()->result();
		return $result;
	}

	function getRewardProducts(){
		$this->db->select("*");
		$this->db->from("crm_reward_products");
		$this->db->where("status = 1");
		$this->db->order_by("point","ASC");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRewardRequests($userid, $deptid, $ids){
			$this->db->select('h.id as rh_id,au.first_name as afirst_name,au.last_name as alast_name,d.department_name,h.status as reward_status, h.description as reward_description, u.first_name,u.last_name,h.*,rt.*,au.role_id as role_id,rt.id as reward_id');
			$this->db->from('crm_reward_history as h');
			$this->db->join('users as u','u.user_id = h.user_id', 'left');
			$this->db->join('users as au','au.user_id = h.approved_by', 'left');
			$this->db->join('crm_reward_type as rt','rt.id = h.reward_id', 'left');
			$this->db->join('department as d','d.department_id = u.department_id', 'left');
			$this->db->order_by("h.id","desc");
			$this->db->where('h.user_id != '.$userid);
			$this->db->where_in('u.user_id', $ids);
			// if($deptid != 10 && $deptid != 11)
			// {
			// 	$this->db->where('u.department_id = '.$deptid);
			// }
			$query=$this->db->get();
			return $query->result();
	}

	function getReward($id){
		$this->db->select("*");
		$this->db->from("crm_reward_type");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function match_data($row_id,$user_id){
		$this->db->select("*");
		$this->db->from("crm_reward_history");
		$this->db->where("id = ".$row_id);
		$this->db->where("user_id = ".$user_id);
		$result = $this->db->get()->row();
		return $result;
	}

	function getRewardHistory($id){
		$this->db->select("*");
		$this->db->from("crm_reward_history");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->row();
		return $result;
	}

	function getUserRank($id){
		$query = $this->db->query('SELECT (SELECT COUNT(DISTINCT ui.karma_points) FROM crm_users as ui WHERE ui.karma_points >= uo.karma_points ) AS rank FROM crm_users as uo WHERE user_id = '.$id);
		return $query->result_array();
	}

	function getRewardUserRankWise($id){
		$this->db->select("u.*, d.department_name");
		$this->db->from("crm_users as u");
		$this->db->join('crm_department as d','d.department_id = u.department_id');
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("karma_points", 'desc');
		$result = $this->db->get()->result();
		return $result;
	}

	function getProduct($id){
		$this->db->select("*");
		$this->db->from("crm_reward_products");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function getRewardUser($id){
		$this->db->select("*");
		$this->db->from("crm_users");
		$this->db->where("user_id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function getThisMonthRewardCount($id){
		$this->db->select("*");
		$this->db->from("crm_reward_redeems");
		$this->db->where("YEAR(created_at) = YEAR(NOW())");
		$this->db->where("MONTH(created_at) = MONTH(NOW())");
		$this->db->where("user_id = ".$id);
		$result = $this->db->get()->result();
		return count($result);
	}

	function getPMPoints($id){
		$this->db->select("pm_reward_points");
		$this->db->from("crm_users");
		$this->db->where("user_id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function updateRewardUser($data, $user_id){
		$this->db->where('user_id',$user_id);
		if($this->db->update('crm_users',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getAllUsers($id){ 		
		$this->db->select("p.user_id, p.first_name, p.last_name, d.department_name");
		$this->db->from("crm_users as p");
		$this->db->join('crm_department as d','d.department_id = p.department_id', 'left');
		$this->db->where("p.status","Active");
		if($id != 11 )
		{
			$this->db->where("p.department_id = ".$id);
		}
		$this->db->order_by("p.first_name", "asc");
		$result = $this->db->get()->result();
		return $result;
	}

	function addExReward($data)
	{
		if($this->db->insert('crm_reward_exeptional', $data)){
			return true;
		}else{
			return false;
		}
	}
	function getUserRedeemHistory($row_id, $user_id){
	    $this->db->select("rh.status,p.point");
		$this->db->from("crm_reward_redeems as rh");
		$this->db->join('crm_reward_products as p','p.id = rh.product_id');		
		$this->db->where('rh.user_id',$user_id);
        $this->db->where('rh.id',$row_id);		
		$result = $this->db->get()->result();
		return $result;
	}
}
?>
