<?php
class holidays_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}


	function getAllFixedHolidays()
	{
		$this->db->select("*");
		$this->db->from("crm_holidays_lists");
		$this->db->where("status = '0'");
		$this->db->order_by("festival_date","ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getAllFlexibleHolidays()
	{
		$this->db->select("*");
		$this->db->from("crm_holidays_lists");
		$this->db->where("status = '1'");
		$this->db->order_by("festival_date","ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getAllSwappedHolidays()
	{
		$this->db->select("*");
		$this->db->from("crm_holidays_lists");
		$this->db->where("status = '2'");
		$this->db->order_by("festival_date","ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

}
?>
