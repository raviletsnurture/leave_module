<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Holidays extends MX_Controller {
  var $logSession;
	function __construct(){
		parent::__construct();
    $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('holidays_model');
	}

	function index(){
		$data['titalTag'] = ' - Holidays List';
		$data["fixedHolidays"] = $this->holidays_model->getAllFixedHolidays();
		$data["flexibleHolidays"] = $this->holidays_model->getAllFlexibleHolidays();
		$data["swappedHolidays"] = $this->holidays_model->getAllSwappedHolidays();
    $this->template->load_partial('dashboard_master','holidays/index',$data);
	}

}
?>
