
<section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-sm-12 col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                            Fixed Holidays
                          </header>
                          <table class="table">
                              <thead>
                              <tr>
																	<th>No</th>
                                  <th>Name of the Festival</th>
                                  <th>Date</th>
                                  <th>Day</th>
                              </tr>
                              </thead>
                              <tbody>
																<?php $i=1; foreach ($fixedHolidays as $fixedHoliday) { ?>
																	<tr>
																		<td><?php echo $i;?></td>
	                                  <td><?php echo $fixedHoliday['festival_name'];?></td>
	                                 	<td><?php echo date("dS, F Y",strtotime($fixedHoliday['festival_date']));?></td>
																		<td><?php echo date("l",strtotime($fixedHoliday['festival_date']));?></td>
		                              </tr>
																<?php $i++; }?>
                              </tbody>
                          </table>
                      </section>
                  </div>
              </div>
							<div class="row">
								<div class="col-sm-12 col-lg-6">
									<section class="panel">
										<header class="panel-heading">
											Flexible festivals
										</header>
										<table class="table table-striped">
											<thead>
												<tr>
														<th>No</th>
														<th>Name of the Festival</th>
														<th>Date</th>
														<th>Day</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<?php $i=1;foreach ($flexibleHolidays as $flexibleHoliday) { ?>
														<tr>
															<td><?php echo $i;?></td>
															<td><?php echo $flexibleHoliday['festival_name'];?></td>
															<td><?php echo date("dS, F Y",strtotime($flexibleHoliday['festival_date']));?></td>
															<td><?php echo date("l",strtotime($flexibleHoliday['festival_date']));?></td>
														</tr>
													<?php $i++; }?>
												</tr>
											</tbody>
										</table>
									</section>
								</div>
								<div class="col-sm-12 col-lg-6">
									<section class="panel">
										<header class="panel-heading">
											Can be swapped with
										</header>
										<table class="table table-striped">
											<thead>
												<tr>
														<th>No</th>
														<th>Name of the Festival</th>
														<th>Date</th>
														<th>Day</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<?php $i=1; foreach ($swappedHolidays as $swappedHoliday) { ?>
														<tr>
															<td><?php echo $i;?></td>
															<td><?php echo $swappedHoliday['festival_name'];?></td>
															<td><?php echo date("dS, F Y",strtotime($swappedHoliday['festival_date']));?></td>
															<td><?php echo date("l",strtotime($swappedHoliday['festival_date']));?></td>
														</tr>
													<?php $i++; }?>
												</tr>
											</tbody>
										</table>
									</section>
								</div>
							</div>
          </section>
      </section>
