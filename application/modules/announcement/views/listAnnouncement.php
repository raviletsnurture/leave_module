<section id="main-content">
    <section class="wrapper">
        <section class="panel">
            <header class="panel-heading margin">
                All Announcements
            </header>
            <div class=" profile-activity">
                <div class="activity blue margin">
                    <div class="activity-desk">
                        <div class="row load-data">
                            <?php foreach($all_announcement as $all_announcement){?>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <div class="panel white">
                                            <div class="panel-body height">
                                                <span><i class="fa fa-bullhorn"></i></span>     
                                                <h5><?php echo $all_announcement->title?> <em style="font-size:14px; color: #c3c3c3;">On
                                                    <?php echo date('d-M-Y',strtotime($all_announcement->created_time))?></em>
                                                </h5>                        
                                                <div class="clearfix"></div>
                                                <p><?php echo $all_announcement->description ?></p>
                                            </div>
                                        </div>
                                    </div>
                            <?php }?>  
                        </div>
                        <input type="hidden" name="offset" class="offset" value="12"/>
                        <div id="remove-row" class="center">
                            <button id="btn-more" class="btn btn-success" > Load More </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</section>
<script>
 $(document).ready(function(){
    $(document).on('click','#btn-more',function(){
        var offset = parseInt($('.offset').val());
        $("#btn-more").html("Loading....");
        $.ajax({
            url : '<?php echo base_url()?>announcement/loadDataAjax',
            method : "POST",
            data : {offset:offset},
            dataType : "text",
            success : function (data)
            {                       
               if(data != '') 
               {                
                   $('.offset').val(offset+12);
                   $("#btn-more").html("Load More");
                   $('.load-data').append(data);
               }
               else
               {
                   $('#btn-more').html("No more announcement");
               }
            }
        });
    });  
 });
</script>

