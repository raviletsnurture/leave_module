<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Announcement extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->template->set('controller', $this);
        $this->load->database();
        $this->logSession = $this->session->userdata("login_session");
        $this->load->helper('download');
        $this->load->helper('general_helper');
	      $this->load->library('email');
        $this->load->model('announcement_model');
        user_auth(0);
    }

    function index()
    {
        $data['titalTag'] = ' - Announcements';
        $data["announcements"] = $this->announcement_model->getAllAnnouncements();
        $this->template->load_partial('dashboard_master', 'announcement/select_announcement', $data);
    }

    function add()
    {
        $data['titalTag']      = ' - Add Announcements';
        $this->template->load_partial('dashboard_master', 'announcement/add_announcement', $data);
    }

	function addAnnouncement(){
      if ($this->input->post('title') != '' && $this->input->post('description') != '') {
			$data['title']              = trim($this->input->post('title'));
			$data['description']        = trim($this->input->post('description'));
			$data['notify_via_email']   = trim($this->input->post('notify_via_email'));
			$data['published_status']   = trim($this->input->post('published_status'));

			$dataSubmit = $this->announcement_model->addAnnouncement($data);
			$this->session->set_flashdata('success', 'Announcement added successfully!');

      //Push Notification Logic
      $deviceTokens = $this->announcement_model->getAllDeviceToken();
      $Tokenlist = array();
      foreach($deviceTokens as $value){
        array_push($Tokenlist,$value->deviceToken);
      }
      SendNotificationFCMWeb('HRMS - Announcement ', $data['description'],$Tokenlist);
      //Push Notification Logic

			if($dataSubmit == 1 && $this->input->post('notify_via_email') == 1) {
				$data['description'] = '<tr>
            <td style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">'.trim($this->input->post('description')).'</td>
          </tr>';

			  $this->email->from('hrms@letsnurture.com', "HRMS");
				$this->email->to('hrms@letsnurture.com');
        $this->email->bcc('lnteam@letsnurture.com,ketan@letsnurture.com');
				$this->email->subject('LetsNurture Announcement');
				$body = $this->load->view('mail_layouts/announcement/index.php', $data, TRUE);
				$this->email->message($body);
				if ($this->email->send()):
					//echo "Mail sent!"; // Mail sent!
				else:
					//echo "There is error in sending mail!"; // There is error in sending mail!
				endif;
			}

			redirect(base_url() . 'announcement');
			exit;
        }
    }

    public function delete($annId){
  		$this->announcement_model->deleteAnnouncement($annId);
  		$this->session->set_flashdata('success', 'Announcement deleted successfully!');
  		redirect(base_url().'announcement');
  		exit;
  	}

  public function all_announcements(){
    $data['titalTag'] = ' - All Announcements';
    $data['all_announcement'] = $this->announcement_model->allAnnouncement();
    $this->template->load_partial('dashboard_master', 'announcement/listAnnouncement',$data);
  }

  public function loadDataAjax()
    {
        $offset=$this->input->post('offset');
        $posts = $this->announcement_model->allAnnouncement($offset);
        if($posts != ''&& $posts != NULL){?>    
           <?php foreach($posts as $post){?>      
              <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="panel white">
                        <div class="panel-body height">                        
                            <span><i class="fa fa-bullhorn"></i></span>
                            <h5> <?php echo $post->title;?> <em style="font-size:14px; color: #c3c3c3;"><?php echo date('d-M-Y',strtotime($post->created_time));?></em></h5>
                            <div class="clearfix"></div>
                            <p><?php echo $post->description; ?></p>
                        </div>
                    </div>
              </div>
            <?php }  
      }
   }
}
?>
