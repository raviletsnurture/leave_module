<?php
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
$department_id = $loginSession[0]->department_id;

$user_id= $loginSession[0]->user_id;

//**View feedback
$user = getJuniorUsersList($user_id, 'feedback_users');
//Average Rating
$averageRating = array();
$overallRating=0;

if(!empty($feedbackMonthRanking)){
	foreach ($feedbackMonthRanking as $ranking) {
		if($ranking->overall_score != ""){
			$rankingData = unserialize($ranking->overall_score);
			foreach ($rankingData as $key => $value){
				$data = $value;
				$averageRating[] = $data;
			}
		}
	}

}

$total = count($averageRating);
$ratting =  array_sum($averageRating);
if($ratting != 0){
		$overallRating = round($ratting/$total,2);
}

$years = getAllFeedbackYear();
end($years);         // move the internal pointer to the end of the array
$key = key($years);  // fetches the key of the element pointed to by the internal pointer

$object = new stdClass();
$object->year = $years[$key]->year - 1;
$years[] = $object;

//Checking user permission
for($i=0;$i<sizeof($permission);$i++)
{
	$nm = $permission[$i]->module_name;
	$arr[$nm] = $permission[$i];
}


?>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<style type="text/css">
.dataTables_filter {
     display: none;
}
.panel-default > .panel-heading {
	background-color:#A9D86E;
}
.radio_set p{
	padding: 5px;
}
.radio_set input[type="radio"]{
	margin: 0 5px;
	vertical-align: sub;
}
.incidenceList ul {margin-left: 24px;}
.incidenceList ul li {
    list-style: disc;}
</style>
<section id="main-content" class="feedback_table">
  <section class="wrapper site-min-height">
		<?php
		if(($role_id==13) || ($role_id==15) || ($role_id==20) || ($role_id==1) || $department_id == 12 || $department_id == 7 || $department_id == 10){?>

		<section class="panel">
      <header class="panel-heading">Select User to View Feedbacks</header>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }
					?>

		          <form method="post" id="feedback-type" action="<?php echo base_url()?>developerfeedback/viewFeedback">
									<div class="col-md-2 radio_set">
										<select id="selectFeedbackType" name="selectFeedbackType" class="form-control">
											<option value="viewfeedback">View Feedback</option>
											<option value="addfeedback">Add Feedback</option>
											<option value="addIncidence">Add Incidence</option>
										</select>
									</div>
		              <div class="col-md-4">
		                  <select id="selectUser" name="selectUser" class="form-control selectpicker js-example-basic-single" required="" oninvalid="setCustomValidity('Please select Name.')" onchange="try{setCustomValidity('')}catch(e){}" >
		                    <option value="">Select Users</option>
		                    <?php foreach($user as $row){?>
		                    <option value="<?php echo $row->user_id; ?>" <?php if(isset($_POST['selectUser']) && $row->user_id == $_POST['selectUser']){echo 'selected=selected'; } ?>><?php echo $row->first_name.'&nbsp;'.$row->last_name; ?></option>
		                    <?php } ?>
		                  </select>
		              </div>
		              <div class="col-md-4 view_feedback_user">
		        		  <select id="selectYear" name="selectYear" class="form-control selectpicker js-example-basic-single" required="" oninvalid="setCustomValidity('Please select Year')" onchange="try{setCustomValidity('')}catch(e){}">
		                    <option value="">Select Year</option>
		                    <?php foreach($years as $row1){?>
		                    		<option value="<?php echo $row1->year; ?>" <?php if(isset($_POST['selectYear']) && $row1->year == $_POST['selectYear']){echo 'selected=selected'; } ?>><?php echo $row1->year;?>-<?php echo ($row1->year + 1);?></option>
		                    <?php } ?>
		              </select>
		              </div>
									<!-- SElect month -->
						<div class="col-md-4 add_feedback_month" style="display:none;">
							<div class="input-group date form_datetime-component">
								<input type="text" name="created_time" id="created_time" class="form-control" size="36" onkeydown="return false">
							</div>
						</div>
						<div class="col-md-4 add_incidence_month" style="display:none;">
							<div class="input-group date form_datetime-component">
								<input type="text" name="incidence_time" id="incidence_time" class="form-control" size="36" onkeydown="return false">
							</div>
						</div>
									<!-- month -->

									<div class="col-md-2">
			              <div id="editable-sample_length" class="dataTables_length" style="padding:0">
			                <div class="btn-group">
			                    <button class="btn btn-info" id="editable-sample_new" type="submit">Submit</button>
			                </div>
			              </div>
		            </div>
		        </form>


        </div>
      </div>
    </section>
		<?php } ?>
    <!-- page end-->



<!-- List of Feedback -->
<!-- page end-->

<!-- View Feedback View -->

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<section class="panel">
			<header class="panel-heading"><b>
				<?php
					if(isset($feedback) && !empty($feedback)){
						echo $feedback[0]->first_name.' '.$feedback[0]->last_name.' Feedbacks';
					}else{
						echo $firstname.' '.$lastname.' Feedbacks';
					}
				?></b>
			</header>
		 </section>
			<div id="accordion" class="panel-group m-bot20">
				<?php
					if(empty($feedback)){ ?>
						<div class="alert alert-block alert-danger fade in">
							<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
							<strong>Sorry!</strong> You have no Feedback this year. </div>
					<?php }
				else if((isset($QA) && !empty($QAfeedback)) || (isset($BA)  && !empty($BAfeedback))){
					$i = 1;
					foreach($feedback as $row)
					{  ?>
								<div class="panel panel-default">
										<div class="panel-heading">
											<a href="#<?php echo $i;?>_feedback" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
													<h4 class="panel-title">
															<i class="fa fa-bullhorn"></i> Feedbacks <?php  echo date('F Y', strtotime($row->created_time));?> <!--By--> <?php //if($row->reviewer_id == 1){ echo "Admin"; } echo $row->r_first_name; ?> <?php //echo $row->r_last_name; ?>
													</h4>
											</a>
										</div>
										<div class="panel-collapse collapse" id="<?php echo $i;?>_feedback">
											<div class="panel-body">
												<div class="msg-time-chat">
													<?php
													$src = './uploads/user_images/resized/'.$row->reviewer_pic;
													if (@getimagesize($src)) {
														$userImage = '/uploads/user_images/resized/'.$row->reviewer_pic;
													} else {
														$userImage = '//hrms.letsnurture.com/uploads/images/chat-avatar.jpg';
													}?>
													<a class="message-img" href="javascript:void(0);">
														<img alt="" src="<?php echo $userImage;?>" class="avatar"></a>
													<div class="message-body msg-in">
														<span class="arrow"></span>
														<div class="text">
															<div class="media-body">
																<table>
																	<?php
																			if(isset($QAfeedback)){
																			$qa_remark = unserialize($row->qa_remark);
																				if(!empty($qa_remark)){
																					$qa_name = $this->developerfeedback_model->getUserInformation($qa_remark[1]);
																					$fullName_qa = 'By '.$qa_name->first_name.' '.$qa_name->last_name;
																				} else{
																					$fullName_qa = "";
																				}
																				?>
																			<tr><td><b>QA Remarks <?php echo $fullName_qa; ?></b></td><td colspan="3"> : <?php echo $qa_remark[0];?></td>
																				</td>
																			</tr>
																	<?php }else if(isset($BAfeedback)){
																		$ba_remark = unserialize($row->ba_remark);
																		if(!empty($ba_remark)){
																			$ba_name = $this->developerfeedback_model->getUserInformation($ba_remark[1]);
																			$fullName_ba = 'By '.$ba_name->first_name.' '.$ba_name->last_name;
																		}else{
																			$fullName_ba = "";
																		}?>
																		<tr><td><b>BA Remarks <?php echo $fullName_ba; ?></b></td><td colspan="3"> : <?php echo $ba_remark[0];?></td>
																		</td>
																	</tr>
																	<?php } ?>
																</table>
																</div>

														</div>
													</div>
												</div>
											</div>
										</div>
							</div>
						<?php
						$i++;
				}?>
				<?php }
				else if(isset($QA) && empty($QAfeedback))
				{?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
						<strong>Sorry!</strong> You have no QA Feedback this year. </div>
				<?php
				}else if(isset($BA) && empty($BAfeedback))
					{ ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
						<strong>Sorry!</strong> You have no BA Feedback this year. </div>
					<?php }else{ ?>
					<?php $i = 1; foreach($feedback as $row){  ?>
					<div class="panel panel-default">
					<div class="panel-heading">
												<a href="#<?php echo $i;?>_feedback" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
						<h4 class="panel-title">
								<i class="fa fa-bullhorn"></i> Feedbacks <?php  echo date('F Y', strtotime($row->created_time));?> <!--By--> <?php //if($row->reviewer_id == 1){ echo "Admin"; } echo $row->r_first_name; ?> <?php //echo $row->r_last_name; ?>
						</h4>
												</a>
					</div>
					<div class="panel-collapse collapse" id="<?php echo $i;?>_feedback">
						<div class="panel-body">
							<div class="msg-time-chat">
								<?php
								$src = './uploads/user_images/resized/'.$row->reviewer_pic;
								 if (@getimagesize($src)) {
									$userImage = '/uploads/user_images/resized/'.$row->reviewer_pic;
								} else {
									$userImage = '//hrms.letsnurture.com/uploads/images/chat-avatar.jpg';
								}?>
								<a class="message-img" href="javascript:void(0);">
									<img alt="" src="<?php echo $userImage;?>" class="avatar"></a>
								<div class="message-body msg-in">
									<span class="arrow"></span>
									<div class="text">
										<p class="attribution">
										<a href="javascript:void(0);"><?php if($row->reviewer_id == 1){ echo "Admin"; } echo $row->r_first_name; ?> <?php echo $row->r_last_name; ?></a>at <?php  echo date('jS F Y', strtotime($row->created_time));?>
										</p>

											<div class="media-body">
											 <table>
																									 <?php
														if(isset($row->overall_score) && $row->overall_score != ""){
															$scoreValue = unserialize($row->overall_score);
															$odd=2;
															foreach ($scoreValue as $key => $value){
																if($odd%2 == 0){
																	echo "<tr class='half-row'><td width=30%><b>".ucwords(str_replace('_', ' ', $key))."</b></td><td><input class='feedbackRating' value='".$value."'></td>";
																}else{
																	echo "<td width=30%><b>".ucwords(str_replace('_', ' ', $key))."</b></td><td><input class='feedbackRating' value='".$value."'></td></tr>";
																}
																if($odd==sizeof($scoreValue)+1){
																	echo "</tr>";
																}

															$odd++;}
														} else {
															$scoreValue = 0;
														} ?>
													<tr><td><b>Comments/Remarks/Action Items (+ves/-ive):</b></td>
													<td colspan="3"> : <?php echo $row->comment;?></td>
														<?php
															$ba_remark = unserialize($row->ba_remark);
															if(!empty($ba_remark)){
																$ba_name = $this->developerfeedback_model->getUserInformation($ba_remark[1]);
																$fullName_ba = 'By '.$ba_name->first_name.' '.$ba_name->last_name;
															}else{
																$fullName_ba = "";
															}
															$qa_remark = unserialize($row->qa_remark);
															if(!empty($qa_remark)){
																$qa_name = $this->developerfeedback_model->getUserInformation($qa_remark[1]);
																$fullName_qa = 'By '.$qa_name->first_name.' '.$qa_name->last_name;
															} else{
																$fullName_qa = "";
															}
															$hr_remark = unserialize($row->hr_remark);
															if(!empty($hr_remark)){
																$hr_name = $this->developerfeedback_model->getUserInformation($hr_remark[1]);
																$fullName_hr = 'By '.$hr_name->first_name.' '.$hr_name->last_name;
															}else{
																$fullName_hr = "";
															}
															?>
														<tr><td><b>Event Remarks:</b></td><td colspan="3"> : <?php echo $row->event_remark;?></td>
														<tr><td><b>BA Remarks <?php echo $fullName_ba; ?>:</b></td><td colspan="3"> : <?php echo $ba_remark[0];?></td>
														<tr><td><b>QA Remarks <?php echo $fullName_qa; ?>:</b></td><td colspan="3"> : <?php echo $qa_remark[0];?></td>
														<tr><td><b>HR Remarks <?php echo $fullName_hr; ?>:</b></td><td colspan="3"> : <?php echo $hr_remark[0];?></td>
														<tr><td><b>Incidence :</b></td>
														<td colspan="5" class="incidenceList">
														<?php
														if(isset($row->incidence)){
															$incidences = unserialize($row->incidence);
															foreach($incidences as $inciden){
															$incidenceBy = $this->developerfeedback_model->getUserInformation($inciden['loginUserId']); ?>
															<b><?php echo $incidenceBy->first_name.' '.$incidenceBy->last_name; ?></b>
															<ul>
														<?php
															$comments = $inciden['incidence'];
															 foreach ( $comments as $comment) {?>
																	<li><?php echo $comment; ?></li>
															 <?php } ?>
															 </ul>
													 <?php } } ?>
															</td>
														</tr>
											</table>
											</div>
											<div class="media-body">
											<table>
											<?php
											if(isset($row->projects_and_ratings) && $row->projects_and_ratings != ""){
												$projects = unserialize($row->projects_and_ratings);
												$colSpan = count($projects[0])+2;
												echo '<tr><td rowspan="'.$colSpan.'"><b>Projects</b></td></tr>';
												echo '<tr><td><b>Projects Name</b></td><td><b>Rating</b></td><td><b>Comment</b></td></tr>';

												for($project=0;$project<count($projects[0]);$project++){
													$projectName = 'N/A';
													$projectRate = '';
													$projectDetail = 'N/A';

													if($projects[0][$project] != ""){
														$projectName = $projects[0][$project];
													}
													if($projects[1][$project] != ""){
														$projectRate = $projects[1][$project];
													}
													if($projects[2][$project] != ""){
														$projectDetail = $projects[2][$project];
													}
													echo '<tr><td>'.$projectName.'</td><td><input class="feedbackRating" value="'.$projectRate.'"></td><td width=50%>'.$projectDetail.'</td></tr>';
												}
											 }
											?>
											</table>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php $i++; }}?>
			</div>
	 </div>
</div>
<!-- View Feedback View -->


</section>
</section>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/star-rating.min.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/star-rating.min.js"></script>
<script src="<?php echo base_url()?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/js/exporting.js"></script>
<script type="text/javascript" charset="utf-8">

$('#created_time').datepicker({
    //format: 'yyyy-mm-dd',
    endDate: '-1m',
	  startDate: '-2m',
    autoclose: true,
	  format: "yyyy-mm-dd",
    startView: "months",
    minViewMode: "months",
});

$('#incidence_time').datepicker({
    //format: 'yyyy-mm-dd',
	endDate: '0m',
  	startDate: '-2m',
    autoclose: true,
	format: "yyyy-mm-dd",
    startView: "months",
    minViewMode: "months",
});


$('#selectFeedbackType').on("change",function(){
	var val = $(this).val();
	if(val == 'viewfeedback'){
		$('.view_feedback_user').show();
		$('.add_feedback_month').hide();
		$('.add_incidence_month').hide();
		$('#selectYear').prop('required',true);
		$('#created_time').prop('required',false);
		$('#incidence_time').prop('required',false);
		var action = '<?php echo base_url()?>developerfeedback/viewFeedback';
		$('#feedback-type').attr("action",action);
	}else if(val == 'addfeedback'){
		$('.add_feedback_month').show();
		$('.add_incidence_month').hide();
		$('.view_feedback_user').hide();
		$('#selectYear').prop('required',false);
		$('#incidence_time').prop('required',false);
		$('#created_time').prop('required',true);
		var action = '<?php echo base_url()?>feedback/add';
		$('#feedback-type').attr("action",action);
	}else if(val == 'addIncidence'){
		$('.add_incidence_month').show();
		$('.add_feedback_month').hide();
		$('.view_feedback_user').hide();
		$('#selectYear').prop('required',false);
		$('#created_time').prop('required',false);
		$('#incidence_time').prop('required',true);
		var action = '<?php echo base_url()?>feedback/incidence';
		$('#feedback-type').attr("action",action);
	}
});

$('#editable-sample_new').click(function(){

});
function recordActivity(sel,uploadId)
{
   //alert(uploadId);
	  var id = sel;
			$('#loaderAjax').show();
			$.ajax({
			 url: '<?php echo base_url(); ?>salary/logaudit/',
			 data: { user_id: id, upload_id: uploadId},
			 type: 'POST',
			 success: function(data){
				 // hide loader
				 $('#loaderAjax').hide();
				 //location.reload();
         if(data !="OK"){
           alert("You are spoofing someone's account - Reported!");
           window.location = '<?php echo base_url(); ?>dashboard';
           return false;
         } else{
           //alert("all good");
           return true;
         }

       }

			});
      $('#loaderAjax').hide();
}
$(document).ready(function(){
	$('.feedbackRating').rating({
	   step: 1,
	   size: 'xs',
	   starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
       showClear:false,
       disabled:true,
   	});
});

</script>
