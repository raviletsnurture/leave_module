<?php
$user = getAllUsers();
$department = getAllDepartment();

if(isset($arr['users'])){
$role_id = $arr['users']->role_id;
}
?>
<style type="text/css">
.dataTables_filter {
     display: none;
}
</style>
<?php
//Average Rating
$averageRating = array();
$overallRating=0;

if(!empty($feedbackMonthRanking)){
	foreach ($feedbackMonthRanking as $ranking) {
		if($ranking->overall_score != ""){
			$rankingData = unserialize($ranking->overall_score);
			foreach ($rankingData as $key => $value){
				$data = $value;
				$averageRating[] = $data;
			}
		}
	}

}

$total = count($averageRating);
$ratting =  array_sum($averageRating);
if($ratting != 0){
		$overallRating = round($ratting/$total,2);
}

//Average Rating
?>
<section id="main-content" class="feedback_table">
  <section class="wrapper site-min-height">
      <div class="row">
          <div class="col-lg-12">
              <section class="panel">

                <?php if( isset($arr['feedback']) && $arr['feedback']->module_add == 'yes') { ?>
                <div class="row">
                    <div class="col-lg-4">
                    <div id="editable-sample_length" class="dataTables_length">
                      <div class="btn-group">
                        <a href="<?php echo base_url()?>feedback/add">
                          <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                        </a>
                      </div>
                    </div>
                  </div>
              </div>
                <?php } ?>
                <?php /* Hide chart for temp.
                <header class="panel-heading"><b><?php echo $feedback[0]->first_name;?> <?php echo $feedback[0]->last_name;?> Feedbacks</b></header>

                <div class="panel-body">
                      <div class="adv-table editable-table ">
                        <?php if($this->session->flashdata('error')){?>
                        <div class="alert alert-block alert-danger fade in">
                          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                          <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
                        <?php } ?>
                        <?php if($this->session->flashdata('success')){?>
                        <div class="alert alert-success fade in">
                          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
                        <?php }?>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9">
                              <div class="analyticsub">
                                  <div id="feedbackChart" style=" height: 400px; margin: 0 auto"></div>
                              </div>
                            </div>
                            <div class="col-lg-3 col-sm-3 text-center">
                                <div class="chart-info chart-position">
                                      <span class="increase"></span>
                                      <span>Overall Rating</span>
                                  </div>
                                <div class="easy-pie-chart">
                                    <div data-percent="<?php echo ($overallRating*100)/5;?>" class="percentage easyPieChart" style="width: 135px; height: 135px; line-height: 135px;"><span><?php echo $overallRating;?></span><canvas height="135" width="135"></canvas></div>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
                  */ ?>
              </section>
          </div>
      </div>
        <!-- page end-->
      <!-- page end-->
	<div class="row">
	  <div class="col-lg-12 col-md-12 col-sm-12">
		  <section class="panel">
			  <header class="panel-heading"><b><?php echo $feedback[0]->first_name;?> <?php echo $feedback[0]->last_name;?> Feedbacks</b></header>
		   </section>
			  <div id="accordion" class="panel-group m-bot20">
			   <?php $i = 1; foreach($feedback as $row){  ?>
				  <div class="panel panel-default">
					  <div class="panel-heading">
                          <a href="#<?php echo $i;?>_feedback" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
						  <h4 class="panel-title">
							    <i class="fa fa-bullhorn"></i> Feedbacks <?php  echo date('F Y', strtotime($row->created_time));?> <!--By--> <?php //if($row->reviewer_id == 1){ echo "Admin"; } echo $row->r_first_name; ?> <?php //echo $row->r_last_name; ?>
						  </h4>
                          </a>
					  </div>
					  <div class="panel-collapse collapse" id="<?php echo $i;?>_feedback">
						  <div class="panel-body">
							  <div class="msg-time-chat">
								  <?php
									$src = './uploads/user_images/resized/'.$row->reviewer_pic;
									 if (@getimagesize($src)) {
									  $userImage = '/uploads/user_images/resized/'.$row->reviewer_pic;
									} else {
									  $userImage = '//hrms.letsnurture.com/uploads/images/chat-avatar.jpg';
									}?>
								  <a class="message-img" href="javascript:void(0);">
									  <img alt="" src="<?php echo $userImage;?>" class="avatar"></a>
								  <div class="message-body msg-in">
									  <span class="arrow"></span>
									  <div class="text">
										  <p class="attribution">
											<a href="javascript:void(0);"><?php if($row->reviewer_id == 1){ echo "Admin"; } echo $row->r_first_name; ?> <?php echo $row->r_last_name; ?></a>at <?php  echo date('jS F Y', strtotime($row->created_time));?>
										  </p>

											  <div class="media-body">
												 <table>
                                                     <?php
       											  if(isset($row->overall_score) && $row->overall_score != ""){
       												  $scoreValue = unserialize($row->overall_score);
       												  $odd=2;
       												  foreach ($scoreValue as $key => $value){
       													  if($odd%2 == 0){
       														  echo "<tr class='half-row'><td width=30%><b>".ucwords(str_replace('_', ' ', $key))."</b></td><td><input class='feedbackRating' value='".$value."'></td>";
       													  }else{
       														  echo "<td width=30%><b>".ucwords(str_replace('_', ' ', $key))."</b></td><td><input class='feedbackRating' value='".$value."'></td></tr>";
       													  }
       													  if($odd==sizeof($scoreValue)+1){
       														  echo "</tr>";
       													  }

       												  $odd++;}
       											  } else {
       												  $scoreValue = 0;
       											  } ?>
                                                    <tr><td><b>Comments/Remarks/Action Items (+ves/-ive):</b></td>
    												<td colspan="3"> : <?php echo $row->comment;?></td>
                                                    <?php
                                                            $ba_remark = unserialize($row->ba_remark);
                                                            if(!empty($ba_remark)){
                                                              $ba_name = $this->developerfeedback_model->getUserInformation($ba_remark[1]);
                                                                $fullName_ba = 'By '.$ba_name->first_name.' '.$ba_name->last_name;
                                                            }else{
                                                                $fullName_ba = "";
                                                            }

                                                            $qa_remark = unserialize($row->qa_remark);
                                                            if(!empty($qa_remark)){
                                                              $qa_name = $this->developerfeedback_model->getUserInformation($qa_remark[1]);
                                                              $fullName_qa = 'By '.$qa_name->first_name.' '.$qa_name->last_name;
                                                            } else{
                                                                $fullName_qa = "";
                                                            }
                                                            $hr_remark = unserialize($row->hr_remark);
                                                            if(!empty($hr_remark)){
                                                              $hr_name = $this->developerfeedback_model->getUserInformation($hr_remark[1]);
                                                              $fullName_hr = 'By '.$hr_name->first_name.' '.$hr_name->last_name;
                                                            }else{
                                                                $fullName_hr = "";
                                                            }


                                                     ?>
    											    <tr><td><b>Event Remarks:</b></td><td colspan="3"> : <?php echo $row->event_remark;?></td>
                                                    <tr><td><b>BA Remarks <?php echo $fullName_ba; ?>:</b></td><td colspan="3"> : <?php echo $ba_remark[0];?></td>
                                                    <tr><td><b>QA Remarks <?php echo $fullName_qa; ?>:</b></td><td colspan="3"> : <?php echo $qa_remark[0];?></td>
                                                    <tr><td><b>HR Remarks <?php echo $fullName_hr; ?>:</b></td><td colspan="3"> : <?php echo $hr_remark[0];?></td>
											  </table>
											  </div>
											  <div class="media-body">
											  <table>
											  <?php
											  if(isset($row->projects_and_ratings) && $row->projects_and_ratings != ""){
												  $projects = unserialize($row->projects_and_ratings);
												  $colSpan = count($projects[0])+2;
												  echo '<tr><td rowspan="'.$colSpan.'"><b>Projects</b></td></tr>';
												  echo '<tr><td><b>Projects Name</b></td><td><b>Rating</b></td><td><b>Comment</b></td></tr>';

												  for($project=0;$project<count($projects[0]);$project++){
                            $projectName = 'N/A';
														$projectRate = '';
														$projectDetail = 'N/A';

														if($projects[0][$project] != ""){
															$projectName = $projects[0][$project];
														}
														if($projects[1][$project] != ""){
															$projectRate = $projects[1][$project];
														}
														if($projects[2][$project] != ""){
															$projectDetail = $projects[2][$project];
														}
														echo '<tr><td>'.$projectName.'</td><td><input class="feedbackRating" value="'.$projectRate.'"></td><td width=50%>'.$projectDetail.'</td></tr>';
												  }
											   }
											  ?>
											  </table>
											  </div>
									  </div>
								  </div>
							  </div>
						  </div>
					  </div>
				  </div>
				<?php $i++; }?>
			  </div>
	   </div>
	</div>
  </section>
</section>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/star-rating.min.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/star-rating.min.js"></script>
<script src="<?php echo base_url()?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/js/exporting.js"></script>
<script type="text/javascript" charset="utf-8">

function recordActivity(sel,uploadId)
{
   //alert(uploadId);
	  var id = sel;
			$('#loaderAjax').show();
			$.ajax({
			 url: '<?php echo base_url(); ?>salary/logaudit/',
			 data: { user_id: id, upload_id: uploadId},
			 type: 'POST',
			 success: function(data){
				 // hide loader
				 $('#loaderAjax').hide();
				 //location.reload();
         if(data !="OK"){
           alert("You are spoofing someone's account - Reported!");
           window.location = '<?php echo base_url(); ?>dashboard';
           return false;
         } else{
           //alert("all good");
           return true;
         }

       }

			});
      $('#loaderAjax').hide();
}
$(document).ready(function(){
	$('.feedbackRating').rating({
	   step: 1,
	   size: 'xs',
	   starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
       showClear:false,
       disabled:true,
   	});
});
</script>
<?php

//echo "<pre>";
//print_r($feedbackMonthRanking);
/* Hide chart for temp.
$punctuality =array();
$discipline =array();
$grooming =array();
$attendance =array();
$communication_skills =array();
$attitude =array();
$job_knowledge =array();
$quality_of_work =array();
$cooperation_and_relation_with_colleagues =array();
$ability_to_work_with_a_group =array();
$leadership_qualities =array();
$innovativeness_and_creativeness =array();
$ability_to_anticipate_problems_and_handle =array();
$ability_to_motivate_subordinates =array();
$quality_consciousness =array();
$relations_with_manager_and_staff =array();
$work_planning =array();
foreach ($feedbackMonthRanking as $ranking) {
	$rankingData = unserialize($ranking->overall_score);
	$datapunctuality = $rankingData['punctuality'];
	$datadiscipline = $rankingData['discipline'];
	$datagrooming = $rankingData['grooming'];
	$dataattendance = $rankingData['attendance'];
	$datacommunication_skills = $rankingData['communication_skills'];
	$dataattitude = $rankingData['attitude'];
	$datajob_knowledge = $rankingData['job_knowledge'];
	$dataquality_of_work = $rankingData['quality_of_work'];
	$datacooperation_and_relation_with_colleagues = $rankingData['cooperation_and_relation_with_colleagues'];
	$dataability_to_work_with_a_group = $rankingData['ability_to_work_with_a_group'];
	$dataleadership_qualities = $rankingData['leadership_qualities'];
	$datainnovativeness_and_creativeness = $rankingData['innovativeness_and_creativeness'];
	$dataability_to_anticipate_problems_and_handle = $rankingData['ability_to_anticipate_problems_and_handle'];
	$dataability_to_motivate_subordinates = $rankingData['ability_to_motivate_subordinates'];
	$dataquality_consciousness = $rankingData['quality_consciousness'];
	$datarelations_with_manager_and_staff = $rankingData['relations_with_manager_and_staff'];
	$datawork_planning = $rankingData['work_planning'];


	$punctuality[] = $datapunctuality;
	$discipline[] = $datadiscipline;
	$grooming[] = $datagrooming;
	$attendance[] = $dataattendance;
	$communication_skills[] = $datacommunication_skills;
	$attitude[] = $dataattitude;
	$job_knowledge[] = $datajob_knowledge;
	$quality_of_work[] = $dataquality_of_work;
	$cooperation_and_relation_with_colleagues[] = $datacooperation_and_relation_with_colleagues;
	$ability_to_work_with_a_group[] = $dataability_to_work_with_a_group;
	$leadership_qualities[] = $dataleadership_qualities;
	$innovativeness_and_creativeness[] = $datainnovativeness_and_creativeness;
	$ability_to_anticipate_problems_and_handle[] = $dataability_to_anticipate_problems_and_handle;
	$ability_to_motivate_subordinates[] = $dataability_to_motivate_subordinates;
	$quality_consciousness[] = $dataquality_consciousness;
	$relations_with_manager_and_staff[] = $datarelations_with_manager_and_staff;
	$work_planning[] = $datawork_planning;
}
$punctuality = implode(",", $punctuality);
$discipline = implode(",", $discipline);
$grooming = implode(",", $grooming);
$attendance = implode(",", $attendance);
$communication_skills = implode(",", $communication_skills);
$attitude = implode(",", $attitude);
$job_knowledge = implode(",", $job_knowledge);
$quality_of_work = implode(",", $quality_of_work);
$cooperation_and_relation_with_colleagues = implode(",", $cooperation_and_relation_with_colleagues);
$ability_to_work_with_a_group = implode(",", $ability_to_work_with_a_group);
$leadership_qualities = implode(",", $leadership_qualities);
$innovativeness_and_creativeness = implode(",", $innovativeness_and_creativeness);
$ability_to_anticipate_problems_and_handle = implode(",", $ability_to_anticipate_problems_and_handle);
$ability_to_motivate_subordinates = implode(",", $ability_to_motivate_subordinates);
$quality_consciousness = implode(",", $quality_consciousness);
$relations_with_manager_and_staff = implode(",", $relations_with_manager_and_staff);
$work_planning = implode(",", $work_planning);*/

?>
<?php /*
<script>
$(function () {
	var temp = JSON.parse('<?php echo json_encode($feedbackMonth); ?>');
	var monthArray = [];
	for(var i in temp){
		monthArray.push(temp[i].month);
	}
	//console.log(monthArray);
    $('#feedbackChart').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Feedbacks'
        },
        subtitle: {
            text: ' '
        },
        xAxis: [{
            categories: monthArray,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: ' ',

            },
            title: {
                text: ' ',

            }
        }, { // Secondary yAxis
            title: {
                text: ' ',

            },
            labels: {
                format: ' ',

            },
            opposite: true
        }],
        tooltip: {
            shared: false
        },

        series: [{
            name: 'Punctuality',
            type: 'spline',
            data: [<?php echo $punctuality;?>],
        },{
            name: 'Discipline',
            type: 'spline',
            data: [<?php echo $discipline;?>],
        },{
            name: 'Grooming',
            type: 'spline',
            data: [<?php echo $grooming;?>],
        },{
            name: 'Attendance',
            type: 'spline',
            data: [<?php echo $attendance;?>],
        },{
            name: 'Communication Skills',
            type: 'spline',
            data: [<?php echo $communication_skills;?>],
        },{
            name: 'Attitude',
            type: 'spline',
            data: [<?php echo $attitude;?>],
        },{
            name: 'job_knowledge',
            type: 'spline',
            data: [<?php echo $job_knowledge;?>],
        },{
            name: 'quality_of_work',
            type: 'spline',
            data: [<?php echo $quality_of_work;?>],
        },{
            name: 'cooperation_and_relation_with_colleagues',
            type: 'spline',
            data: [<?php echo $cooperation_and_relation_with_colleagues;?>],
        },{
			name: 'ability_to_work_with_a_group',
			type: 'spline',
			data: [<?php echo $ability_to_work_with_a_group;?>],
		},{
			name: 'leadership_qualities',
			type: 'spline',
			data: [<?php echo $leadership_qualities;?>],
		},{
			name: 'innovativeness_and_creativeness',
			type: 'spline',
			data: [<?php echo $innovativeness_and_creativeness;?>],
		},{
			name: 'ability_to_anticipate_problems_and_handle',
			type: 'spline',
			data: [<?php echo $ability_to_anticipate_problems_and_handle;?>],
		},{
			name: 'ability_to_motivate_subordinates',
			type: 'spline',
			data: [<?php echo $ability_to_motivate_subordinates;?>],
		},{
			name: 'quality_consciousness',
			type: 'spline',
			data: [<?php echo $quality_consciousness;?>],
		},{
			name: 'relations_with_manager_and_staff',
			type: 'spline',
			data: [<?php echo $relations_with_manager_and_staff;?>],
		},{
			name: 'work_planning',
			type: 'spline',
			data: [<?php echo $work_planning;?>],
		}]
    });
});
</script>
*/ ?>
