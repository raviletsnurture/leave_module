<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Developerfeedback extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('download');
		$this->load->helper('general_helper');
		$this->load->model('developerfeedback_model');
		$this->load->model('feedback/feedback_model');
		user_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Feedbacks';

		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);
		$data['permission'] = $per;

		$userData = $this->logSession;
		$id = $userData[0]->user_id;
    auditUserActivity('Developer Feedback', 'Index', $id, date('Y-m-d H:i:s'),$id);

		$this->template->load_partial('dashboard_master','developerfeedback/select_feedbacks',$data);
	}

	function viewFeedback(){
		$data['titalTag'] = ' - Feedbacks';

		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$user_id = $loginSession[0]->user_id;
		$department_id = $loginSession[0]->department_id;
		$per = getUserpermission($role_id);
		$data['permission'] = $per;

		if($this->input->post('selectUser') != '' && $this->input->post('selectYear') != ''){
			$data["feedback"] = $this->developerfeedback_model->getAllFeedbacks($this->input->post('selectUser'),$this->input->post('selectYear'));
			if($data["feedback"]){
				$data['feedbackMonth'] = $this->developerfeedback_model->getMyfeedMonth($data["feedback"][0]->user_id);
				$data['feedbackMonthRanking'] = $this->developerfeedback_model->getMyfeedMonthRanking($data["feedback"][0]->user_id);
				$selectId = $this->input->post('selectUser');
				$selectDepartment = $this->developerfeedback_model->getUserDepartment($selectId);
				if($department_id == 7 && $selectDepartment[0]->department_id != 7){
					foreach ($data['feedback'] as $feedback) {
						if(!empty($feedback->qa_remark)){
						$data['QAfeedback'][] = $feedback->qa_remark;
						}
						$data['QA'] = 1;
					}

				}else if($department_id == 12 && $selectDepartment[0]->department_id != 12){
					foreach ($data['feedback'] as $feedback) {
						if(!empty($feedback->ba_remark)){
						$data['BAfeedback'][] = $feedback->ba_remark;
					}
					$data['BA'] = 1;
					}
				}
			}else{
				$this->session->set_flashdata('error',' Team member no review this year!');
				redirect(base_url().'developerfeedback/viewFeedback');exit;
			}
		}else{
				$data["feedback"] = $this->developerfeedback_model->getCurrentUserAllFeedbacks($user_id);
				if($data["feedback"]){
					$data['feedbackMonth'] = $this->developerfeedback_model->getMyfeedMonth($data["feedback"][0]->user_id);
					$data['feedbackMonthRanking'] = $this->developerfeedback_model->getMyfeedMonthRanking($data["feedback"][0]->user_id);
				}else{
					$data["firstname"] = $loginSession[0]->first_name;
					$data["lastname"] = $loginSession[0]->last_name;
					$this->session->set_flashdata('error',' Year & Team member selection is mandatory!!!');
				}

			// $this->session->set_flashdata('error',' Year & Team member selection is mandatory!!!');
			// redirect(base_url().'developerfeedback/index');exit;
		}
		$this->template->load_partial('dashboard_master','developerfeedback/select_feedbacks',$data);


		//$this->template->load_partial('dashboard_master','developerfeedback/select_feedbacks',$data);
		// $this->template->load_partial('dashboard_master','developerfeedback/list_feedbacks',$data);
	}
}

?>
