<?php
class meeting_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}
	function addMeeting($data){
		if($this->db->insert('crm_meeting',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getMembers($id){
		$this->db->select('u.user_id,u.first_name,u.last_name,u.email,d.department_name,u.gmail_id');
		$this->db->from('crm_users u,crm_department d');
		$this->db->where('d.department_id = u.department_id');
		$this->db->where("u.status = 'Active' ");
		$this->db->where("user_id !=",$id);
		$query=$this->db->get();
		return $query->result_array();
	}
	function deleteMeetingData($id){
		$this->db->where('meeting_id', $id);
		$query = $this->db->delete('crm_meeting');
		if($query){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      :
	 * @created		  :
	 * @Modified by	: Jignasa(28-07-2017)
	 * @Status		  :
	 * @Comment		  : To check whether the user has meeting on given date and time
	 **/
	function checkMeetingTime($schedule_date,$schedule_time,$id){
		$this->db->select('*');
		$this->db->from('crm_meeting');
		$this->db->where('schedule_date',$schedule_date);
		$this->db->where('schedule_time',$schedule_time);
		$this->db->where('user_id',$id);
		$this->db->where('status = 0');
		$query=$this->db->get();
		return $query->row();
	}
	function getMyInvitedMettings($id){
		$this->db->select('m.*,u.first_name,u.last_name');
		$this->db->from('crm_meeting m, crm_users u');
		$this->db->where("m.user_id = u.user_id");
		$this->db->where("FIND_IN_SET ($id,m.members)");
		$query=$this->db->get();
		return $query->result_array();
	}

	function getMyMeetingsCalendar($id)
	{
		$this->db->select(" m.*,u.first_name,u.last_name, GROUP_CONCAT( u.first_name ) AS members_name ,(select CONCAT((first_name),(' '),(last_name)) from crm_users where user_id = $id) as user_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.user_id",$id);
		$this->db->where("FIND_IN_SET( u.user_id, m.members)");
		//$this->db->where("m.user_id = u.user_id");
		$this->db->group_by("m.meeting_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getAllMeetings($id)
	{
		$this->db->select(" m.*,u.first_name,u.last_name, GROUP_CONCAT( u.first_name ) AS members_name ,(select CONCAT((first_name),(' '),(last_name)) from crm_users where user_id = m.user_id) as user_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.user_id != ",$id);
		$this->db->where("FIND_IN_SET( u.user_id, m.members)");
		//$this->db->where("m.user_id = u.u");
		$this->db->group_by("m.meeting_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}
	//Metting reminder
	function getMeetingReminder(){
		$this->db->select(" m.*,u.first_name,u.last_name,GROUP_CONCAT( u.email ) AS members_email, GROUP_CONCAT( u.first_name ) AS members_name,(select email from crm_users where user_id = m.user_id) as user_email ,(select CONCAT((first_name),(' '),(last_name)) from crm_users where user_id = m.user_id) as user_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("FIND_IN_SET( u.user_id, m.members)");
		//$this->db->where("m.user_id = u.u");
		$this->db->group_by("m.meeting_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}
	function getHrMeetingsCalendar()
	{
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.hr_role_id",'22');
		$this->db->where("m.user_id = u.user_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getMyMeetings($id,$date)
	{
		$startDate = $date.'-01';
		$endDate = $date.'-31';
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.user_id",$id);
		$this->db->where("m.user_id = u.user_id");
		$this->db->where("schedule_date >= '$startDate'");
		$this->db->where("schedule_date <= '$endDate'");
		$this->db->order_by("schedule_date DESC,schedule_time DESC");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}
	function getMyMeetingsDetails($id)
	{
		$this->db->select(" m.*,u.first_name,u.last_name,u.email");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.meeting_id",$id);
		$this->db->where("m.user_id = u.user_id");
		$milestone = $this->db->get()->row();
		return $milestone;
	}

	function getHrMeetings($date)
	{	$startDate = $date.'-01';
		$endDate = $date.'-31';
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.hr_role_id",'22');
		$this->db->where("m.user_id = u.user_id");
		$this->db->where("schedule_date >= '$startDate'");
		$this->db->where("schedule_date <= '$endDate'");
		$this->db->order_by("schedule_time","ASC");
		$milestone = $this->db->get()->result_array();
		// $tr = $this->db->last_query();
		// print_r($tr);
		return $milestone;

	}

	function updateMeetingStatus($data){
		$this->db->where('meeting_id',$data['meeting_id']);
		if($this->db->update('crm_meeting',$data)){
			return true;
		}else{
			return false;
		}
	}
	function updateMeeting($data){
		$this->db->where('meeting_id',$data['meeting_id']);
		if($this->db->update('crm_meeting',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getupcominguserleaves($userid)
	{
		$this->db->select('au.first_name as afirst_name,au.last_name as alast_name,d.department_name,ls.leave_status as leave_status_name,lt.leave_type as leave_name,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('users as au','au.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("l.is_cancelled != 1");
		$this->db->where("l.leave_start_date >= CURDATE()");
		$this->db->where("(l.leave_status = 1 OR l.leave_status = 2)");
		//$this->db->order_by("leave_id","desc");
		$this->db->where('l.user_id != '.$userid);
		$this->db->order_by("l.leave_start_date","asc");
		//$this->db->where('u.department_id != '.$id);
		$query=$this->db->get()->result_array();
		return $query;
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      :Jignasa
	 * @created		  :(28-07-2017)
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : To check whether the member user has meeting on given date and time
	 **/
	function checkMemberMeetingTime($schedule_date,$schedule_time,$id){
		$this->db->select("IF(user_id = '$id',members,user_id ) as member, schedule_date, schedule_time, meeting_duration");
		$this->db->from('crm_meeting');
		$this->db->where('schedule_date',$schedule_date);
		$this->db->where('schedule_time',$schedule_time);
		$this->db->where("(user_id = '$id' OR  FIND_IN_SET('$id',members)) ");
		$result = $this->db->get()->result_array();
	  return $result;
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      :Jignasa
	 * @created		  :(28-07-2017)
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : Get the detail of user
	 **/
	function getUserDetail($id){
		$this->db->select('first_name,last_name');
		$this->db->from('crm_users');
		$this->db->where("status = 'Active' ");
		$this->db->where("user_id =",$id);
		$query=$this->db->get();
		return $query->result_array();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      :Jignasa
	 * @created		  :(29-07-2017)
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : To check whether the user(for update) has meeting on given date and time
	 **/
	function checkUpdateMeetingTime($schedule_date,$schedule_time,$id,$meeting_id){
		$this->db->select('*');
		$this->db->from('crm_meeting');
		$this->db->where('schedule_date',$schedule_date);
		$this->db->where('schedule_time',$schedule_time);
		$this->db->where('user_id',$id);
		$this->db->where('meeting_id != ',$meeting_id);
		$this->db->where('status = 0');
		$query=$this->db->get();
		return $query->row();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      :Jignasa
	 * @created		  :(29-07-2017)
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : To check whether the member user(for update) has meeting on given date and time
	 **/
	function checkUpdateMemberMeetingTime($schedule_date,$schedule_time,$id,$meeting_id){
		$this->db->select("IF(user_id = '$id',members,user_id ) as member, schedule_date, schedule_time, meeting_duration");
		$this->db->from('crm_meeting');
		$this->db->where('schedule_date',$schedule_date);
		$this->db->where('schedule_time',$schedule_time);
		$this->db->where("(user_id = '$id' OR  FIND_IN_SET('$id',members)) ");
		$this->db->where('meeting_id != ',$meeting_id);
		$result = $this->db->get()->result_array();
	  return $result;
	}

}
?>
