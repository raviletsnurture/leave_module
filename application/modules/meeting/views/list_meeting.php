<style>
/*.fc-border-separate .fc-widget-content
{
    cursor:pointer;
}*/
.ui-dialog-buttonpane{
    margin-right: 100px;
}
</style>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <aside class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">Meeting Request
                      <?php
                      if (isset($_SESSION['token'])) {
                        global $authUrl,$client;
                        $client->setAccessToken($_SESSION['token']);
                      	echo "<a class='btn btn-info pull-right' href='$authUrl?logout=1'><i class='fa fa-calendar'></i> Calendar Logout</a>";
                        echo "<a class='btn btn-success pull-right' id='add_new' href='".base_url()."meeting/add' style='margin-right: 12px;'><i class='glyphicon glyphicon-plus'></i> Add New</a>";
                      }else{
                        global $authUrl,$client;
                        $authUrl = $client->createAuthUrl();
                        echo "<a class='btn btn-info pull-right' href='$authUrl'><i class='fa fa-calendar'></i> Google Calendar</a>";

                      }
                      ?>

                      <!-- <a class="btn btn-success pull-right" href="<?php echo base_url()?>statuslist" style=" margin-right: 12px;"><i class="glyphicon glyphicon-list-alt"></i> Meeting List</a> -->
                    </header>
                    <div class="panel-body">
                        <div id="leavecalendar" class="has-toolbar"></div>
                    </div>
                </section>
            </aside>
        </div>

        <div id="eventContent" title="Event Details">
            <div id="eventInfo"></div>
            <p><strong><a id="eventLink" target="_blank"></a></strong></p>
        </div>
        <div id="dialog" style="display: none" align = "center">
           Please Login To Google Calendar!
        </div>
    </section>
</section>
<script>
$(document).ready(function() {
    $('#scroll-announcements').niceScroll({
        styler: "fb",
        cursorcolor: "#e8403f",
        cursorwidth: '6',
        cursorborderradius: '10px',
        background: '#404040',
        spacebarenabled: false,
        cursorborder: '',
        zindex: '1000'
    });

	var date = new Date();
    var endDate = new Date();
    var endDate = endDate.setTime( endDate.getTime() + 90 * 86400000 );
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#leavecalendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'basicDay,basicWeek,month'
        },
        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar !!!

        events: [
			<?php for($i=0;$i<sizeof($my_create_meetings);$i++) { ?>
            {
				title: '<?php echo $my_create_meetings[$i]['user_name'].' ('.$my_create_meetings[$i]['schedule_time'].')'; ?>',
                start: '<?php echo $my_create_meetings[$i]['schedule_date']; ?> 00:00:00',
                end: '<?php echo $my_create_meetings[$i]['schedule_date']; ?> 00:00:00',
				description: '<div id="<?php echo $my_create_meetings[$i]['meeting_id']; ?>">Subject: <?php echo addslashes($my_create_meetings[$i]['subject']); ?><br />Description: <?php echo addslashes($my_create_meetings[$i]['description']); ?><br />Location: <?php echo addslashes($my_create_meetings[$i]['location']); ?><br />Time: <?php echo $my_create_meetings[$i]['schedule_time'];?><br  />Duration: <?php echo $my_create_meetings[$i]['meeting_duration'].' Min';?><br  />Members: <?php echo $my_create_meetings[$i]['members_name']; ?><?php if($role == 1){?><br  />Hidden Members: Ketan <?php }?><br/><a class="btn btn-primary" style="color:#fff;" href="<?php echo base_url()?>meeting/editMeeting?id=<?php echo $my_create_meetings[$i]['meeting_id']; ?>">Edit</a> <input type="submit" value="Delete" onClick="confirmbox()" class="btn btn-danger delete-meeting" /></div>',
				backgroundColor: '<?php if($my_create_meetings[$i]['status'] == 1){echo '#6DBB4A';}elseif ($my_create_meetings[$i]['status'] == 2){echo '#FF6C60';}else{echo '#53BEE6';}?>',
				/*imageurl: 'img',*/
				textColor: '#fff',
			},
			<?php } ?>

      <?php for($i=0;$i<sizeof($invited_meetings);$i++) { ?>
            {
        title: '<?php echo $invited_meetings[$i]['first_name'].' '.$invited_meetings[$i]['last_name'].' ('.$invited_meetings[$i]['schedule_time'].')'; ?>',
                start: '<?php echo $invited_meetings[$i]['schedule_date']; ?> 00:00:00',
                end: '<?php echo $invited_meetings[$i]['schedule_date']; ?> 00:00:00',
        description: '<div>Subject: <?php echo addslashes($invited_meetings[$i]['subject']); ?><br />Description: <?php echo addslashes($invited_meetings[$i]['description']); ?><br />Location: <?php echo addslashes($invited_meetings[$i]['location']); ?><br />Time: <?php echo $invited_meetings[$i]['schedule_time']; ?><br  />Duration: <?php echo $invited_meetings[$i]['meeting_duration'],' Min'; ?><?php if($role == 1){?><br  />Hidden Members: Ketan <?php }?><br  /></div>',
        backgroundColor: '<?php if($invited_meetings[$i]['status'] == 1){echo '#6DBB4A';}elseif ($invited_meetings[$i]['status'] == 2){echo '#FF6C60';}else{echo '#FF6C60';}?>',
        /*imageurl: 'img',*/
        textColor: '#fff',
      },
      <?php } ?>
      // FOR CEO
      <?php
      if($role == 1){
      for($i=0;$i<sizeof($all_mettings);$i++) { ?>
            {
				title: '<?php echo $all_mettings[$i]['user_name'].' ('.$all_mettings[$i]['schedule_time'].')'; ?>',
                start: '<?php echo $all_mettings[$i]['schedule_date']; ?> 00:00:00',
                end: '<?php echo $all_mettings[$i]['schedule_date']; ?> 00:00:00',
				description: '<div id="<?php echo $all_mettings[$i]['meeting_id']; ?>">Subject: <?php echo addslashes($all_mettings[$i]['subject']); ?><br />Description: <?php echo addslashes($all_mettings[$i]['description']); ?><br />Location: <?php echo addslashes($all_mettings[$i]['location']); ?><br />Time: <?php echo $all_mettings[$i]['schedule_time'];?><br />Duration: <?php echo $all_mettings[$i]['meeting_duration'].' Min';?><br  />Members: <?php echo $all_mettings[$i]['members_name']; ?><?php if($role == 1){?><br  />Hidden Members: Ketan <?php }?></div>',
				backgroundColor: '<?php if($all_mettings[$i]['status'] == 1){echo '#6DBB4A';}elseif ($all_mettings[$i]['status'] == 2){echo '#FF6C60';}else{echo '#51C468';}?>',
				/*imageurl: 'img',*/
				textColor: '#fff',
			},
			<?php }} ?>
        ],
		eventRender: function(event, eventElement) {
			if (event.imageurl)
			{
				eventElement.find("div.fc-event-inner").prepend("<i class='fa fa-comment' style='font-size:16px;'></i>&nbsp;");
			}
			if (event.image)
			{
				eventElement.find("div.fc-event-inner").prepend("<i class='fa fa-smile-o' style='font-size:16px;'></i>&nbsp;");
				var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
				$('.fc-day[data-date="'+ dateString + '"]').addClass('orange');
			}

		},
		eventClick:  function(event, jsEvent, view) {
			if(!event.image && !event.imageurl) {
			$("#eventInfo").html(event.description);
			$("#eventLink").attr('href', event.url);
			$("#eventContent").dialog({ modal: true, title: event.title });
			}
            if (event.url) {
                $("#eventInfo").html(event.description);
    			$("#eventLink").attr('href', event.url);
    			$("#eventContent").dialog({ modal: true, title: event.title });
                window.open(event.url, "_self");
                return false;
            }
		},
		/*eventMouseover: function(event, jsEvent, view) {
			$('.fc-event-inner', this).append('<div id=\"'+event.id+'\" class=\"hover-end\">'+event.description+'</div>');
		},
		eventMouseout: function(event, jsEvent, view) {
			$('#'+event.id).remove();
		}*/
    });

    //Delte meeting procedure
    $('body').on('click','.delete-meeting',function(){

      var txt;
      var r = confirm("Are you sure?");
      if (r == true) {

        var meeting_id = $(this).parent().attr("id");
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url()?>meeting/deleteMeeting',
          data: 'meeting_id='+ meeting_id,
          cache: false,
          async:false,
          processData: false,
          beforeSend: function(){

          },
          success:function(data){
            console.log(data);
            if(data == 1){
              alert('Meeting Deleted');
            setTimeout(function(){
              window.location.href= '<?php echo base_url()?>meeting';
             },2000);
           }else{
              alert('Cannot Delete meeting at now.');
           }
           return false;
          }
        });
      }
    });
    var base_url = $('#base_url').val();
    $("body").on("mouseenter",".fc-border-separate .fc-widget-content",function() {
        var date = $(this).attr("data-date");
        var date1 =  new Date(date);
        var date2 = new Date();
        date2 = date2.setHours(0, 0, 0, 0);
        if(date1 >= date2){
           $(".fc-border-separate .fc-widget-content").css("cursor","pointer");
       }else{
           $(".fc-border-separate .fc-widget-content").css("cursor","default");
       }
      });
    $("body").on("click",".fc-border-separate .fc-widget-content",function() {
        var date = $(this).attr("data-date");
        var date1 =  new Date(date);
        var date2 = new Date();
        date2 = date2.setHours(0, 0, 0, 0);
        if(date1 >= date2){
            if ($('#add_new').length > 0)
            {
                var date = $(this).attr("data-date");
                var date1 =  new Date(date);
                var date2 = new Date();
                date2 = date2.setHours(0, 0, 0, 0);
                if(date1 >= date2){
                    window.location.replace(base_url+'meeting/add?d='+date);
                }
            }else{
                 $('#dialog').dialog('open');
            }
        }
    });

    $(function () {
        var agreed = false;
        $("#dialog").dialog({
            modal: true,
            autoOpen: false,
            title: "jQuery Dialog",
            width: 300,
            height: 100,
            buttons: {
                        'OK': function () {
                            agreed = true;
                            $(this).dialog("close");
                        }
                    },
            beforeClose: function () {
                        return agreed;
                    }
        });
        $(".ui-dialog-titlebar").hide();
    });
});

</script>
