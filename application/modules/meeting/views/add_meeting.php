<?php
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
$department_id = $loginSession[0]->department_id;
$user = getAllUsers();

/*if($role_id ==20){

} else{
	$user = getAllUsersFromDepartment($department_id);
}*/
if(isset($_GET['d']) && !empty($_GET['d'])){?>
	<input type="hidden" id="cal_date" value="<?php echo $_GET['d']; ?>">
<?php } ?>
<style>
.btn span.glyphicon {
	opacity: 0;
}
.btn.active span.glyphicon {
	opacity: 1;
}
</style>

<link href="<?php  echo base_url();?>assets/css/bootstrap-select.css" rel="stylesheet" >
<script src="<?php  echo base_url();?>assets/js/bootstrap-select.js"></script>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Add Meeting
            <a class="btn btn-success pull-right" href="<?php echo base_url()?>meeting" style=" margin-right: 12px;"><i class="glyphicon glyphicon-list-alt"></i> Meeting</a>
					</header>

						<div class="panel-body">
							<div class="form">
					<form class="commonForm  cmxform form-horizontal tasi-form" id="addMeeting" name="addMeeting"  method="post" action="">
                        <input type="hidden" value="<?php echo $loginSession[0]->user_id?>" name="huser_id" id="huser_id">

                <div class="col-lg-6">
    								<div class="form-group">
    									<label for="date" class="control-label col-lg-4">Start Date<span class="red">*</span></label>
    									<div class="col-lg-8">
    										<div class="input-group date form_datetime-component">
                                            	<input type="text" name="created_time" id="created_time" class="form-control" size="36" required>
    										</div>
    									</div>
    								</div>
								</div>
								<div class="col-lg-6">
										<div class="form-group">
											<label for="date" class="control-label col-lg-4">End Date<span class="red">*</span></label>
											<div class="col-lg-8">
												<div class="input-group date form_datetime-component">
																							<input type="text" name="end_date" id="end_date" class="form-control" size="36" required>
												</div>
											</div>
										</div>
								</div>

                <div class="col-lg-6">
    								<div class="form-group">
    									<label for="date" class="control-label col-lg-4">Time<span class="red">*</span></label>
    									<div class="col-lg-8">
														<?php
														//DateTime
														$start = new DateTime();
														$start->setTime(9,00,00);// $start->setDate;
														$end = new DateTime();
														$end->setTime(20,15,00);
														// $end->setDate;
														$interval = new DateInterval('PT15M');
														$daterange = new DatePeriod($start,$interval,$end);
														?>
														<select name="meeting_time" id="meeting_time" class="form-control" required disabled="disabled">
															<option value="">Time</option>
														<?php
															 foreach($daterange as $date){ ?>
																<option value=<?php echo $date->format('H:i'); ?> ><?php echo $date->format('H:i'); ?> </option>
														<?php }?>
														</select>

    									</div>
    								</div>
								</div>
								<div class="col-lg-6">
    								<div class="form-group">
    									<label for="date" class="control-label col-lg-4">Duration<span class="red">*</span></label>
    									<div class="col-lg-8">
														<select name="duration" id="duration" class="form-control" required>
															<option value="">Duration</option>
															<option value="15">15 Minutes</option>
															<option value="30" selected="selected">30 Minutes</option>
															<option value="45">45 Minutes</option>
															<option value="60">60 Minutes</option>
															<option value="75">75 Minutes</option>
															<option value="90">90 Minutes</option>
															<option value="105">105 Minutes</option>
															<option value="120">120 Minutes</option>
														</select>
    									</div>
    								</div>
								</div>
                  <div class="clearfix"></div>
									<div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label col-lg-2">Recurring</label>
                        <div class="col-lg-8">
													<select name="recurring" id="recurring" class="form-control">
														<option value="daily">Every Day</option>
														<option value="weekly">Every Week</option>
														<option value="biweekly">Bi Weekly</option>
														<option value="monthly">Every Month</option>
													</select>
                        </div>
												<div class="col-lg-2">
													(optional)
												</div>
                    </div>
                   </div>
                                <div class="col-lg-12">
                                  <div class="form-group">
                                      <label class="control-label col-lg-2">Subject<span class="red">*</span></label>
                                      <div class="col-lg-10">
                                          <input type="text" placeholder="Subject" maxlength="50" id="Subject" class="form-control" required name="Subject">Max length 50
                                      </div>
                                  </div>
                                 </div>
																 <div class="clearfix"></div>
																 <div class="col-lg-12">
																	 <div class="form-group">
																			 <label class="control-label col-lg-2">Description<span class="red">*</span></label>
																			 <div class="col-lg-10">
																				 <textarea class="form-control" style="resize:none;" required name="description" rows="8" maxlength="800"></textarea>Max length 800

																			 </div>
																	 </div>
																	</div>
																	<div class="clearfix"></div>
																	<div class="col-lg-12">
 																	 <div class="form-group">
 																			 <label class="control-label col-lg-2">Decisions to take?<span class="red">*</span></label>
 																			 <div class="col-lg-10">
 																				 <textarea class="form-control" style="resize:none;" required name="decisionToTake" rows="4" maxlength="800"></textarea>Max length 800

 																			 </div>
 																	 </div>
 																	</div>
 																	<div class="clearfix"></div>
	                                <div class="col-lg-12">
	                                  <div class="form-group">
	                                      <label class="control-label col-lg-2">Location<span class="red">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input type="text" placeholder="Location" class="form-control" required name="location" maxlength="50">Max length 50
	                                      </div>
	                                  </div>
	                                 </div>

																	 <div class="clearfix"></div>
	                                 <div class="col-lg-12">
	                                   <div class="form-group">
	                                       <label class="control-label col-lg-2">Add Members<span class="red">*</span></label>
	                                       <div class="col-lg-10">

																					 	<div style="width:100%;height:auto;">

																							<div style="margin:0;">
																								<select name="check_members[]" required class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
																									<?php
																										foreach($members as $members_data){?>

																											<option value="<?php echo $members_data['user_id'].','.$members_data['gmail_id']; ?>" ><?php echo $members_data['first_name'].' '.$members_data['last_name'].' ('.$members_data['department_name'].')'; ?>

																											</option>
																										<?php }	?>
																								</select>
																							</div>
																						</div>
																							<!-- -->
																						</div>

	                                       </div>
	                                   </div>
	                                  </div>


								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-2">
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
										<button class="btn btn-danger" type="submit" id="addMeetingBtn">Submit Meeting</button>
									</div>
									<div class="col-lg-1">
										<div class="loader">
											<img src="<?php echo base_url()?>assets/img/loader.gif" alt="loading..." style="width: 20px;height: 20px;">
										</div>
									</div>
								</div>
								<div class="form-group">
		   						   <div class="col-lg-offset-2 col-lg-10">
		   							   <span class="frmmessage"><ul id="busymsg"></ul></span>

		   						   </div>
		   						</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>

<script language="javascript" type="text/javascript">

var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
var dNow = new Date();
var hours = dNow.getHours();
//if(hours > 12){hours = hours - 12;}

// var myDate="26-02-2012";
// myDate=myDate.split("-");
// var newDate=myDate[1]+"/"+myDate[0]+"/"+myDate[2];
// alert(new Date(newDate).getTime());

var localdate= (dNow.getMonth()+1)+"/"+dNow.getDate()+"/"+dNow.getFullYear();
var currentDate = new Date(localdate).getTime();
var localtime = hours + ':' + (dNow.getMinutes()+ 30);
//	console.log(localtime);
//get current date in format of '2017-08-23'
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!

var yyyy = today.getFullYear();
if(dd<10){
    dd='0'+dd;
}
if(mm<10){
    mm='0'+mm;
}
var today = yyyy+'-'+mm+'-'+dd;
//
if($('#cal_date').val()){
	var cal_date = $('#cal_date').val();
	$('#created_time').val(cal_date);
	$('#end_date').val(cal_date);
	$('#meeting_time').removeAttr('disabled');
	$("#meeting_time option").each(function(){
		var obj = $(this);
		var val = $(this).val();
		var date1 = Date.parse('01/01/2001 '+ val);
		var date2 = Date.parse('01/01/2001 '+ localtime);
		if(date1 < date2){
			obj.prop('disabled',true);
			obj.prop('hidden',true);
		}
	});
}
var base_url = $('#cal_date').val();
$('#recurring').prop('disabled',true);
$('#created_time, #end_date').datepicker({
    format: 'yyyy-mm-dd',
    daysOfWeekDisabled: [0,6],
    startDate: today,
   	autoclose: true,

}).on("changeDate", function(date) {
	var selectedDate=(date.target.value);

		var start_date = $('#created_time').val();
		if($('#end_date').val() == "" || $('#end_date').val() == null){
			$('#end_date').val(start_date);
		}

		var end_date = $('#end_date').val();
		//Date Comparision
		var date_one = new Date(start_date);
		var date_two = new Date(end_date);

		if(date_two >date_one){
			$('#recurring').prop('disabled',false);
		}else if(date_one >date_two){
			$('#created_time').val(end_date);
			$('#recurring').prop('disabled',true);
		}else{
			$('#recurring').prop('disabled',true);
		}

	selectedDate=selectedDate.split("-");
	var selectDate=new Date(selectedDate[1]+"/"+selectedDate[2]+"/"+selectedDate[0]).getTime();

	if(selectDate == currentDate){
		$("#meeting_time option").each(function(){
			var obj = $(this);
			var val = $(this).val();
			var date1 = Date.parse('01/01/2001 '+ val);
			var date2 = Date.parse('01/01/2001 '+ localtime);
			if(date1 < date2){
				obj.prop('disabled',true);
				obj.prop('hidden',true);
			}
		});
	}else{
			$("#meeting_time option").each(function(){
				$(this).prop('disabled',false);
				$(this).prop('hidden',false);
			});
	}
	$('#meeting_time').removeAttr('disabled');

});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$.validator.addMethod("PhoneNumberRegex", function(value, element) {
				return this.optional(element) || /^[0-9-()+ ]{8,20}$/i.test(value);
			}, "Please enter only numeric characters for your Phone number.");

	$("#addMeeting").validate({
	   ignore: [],
	   rules: {
	   },
	   messages: {
	   },
	  errorElement: "div",
	  wrapper: "div",  // a wrapper around the error message
	  errorPlacement: function(error, element) {
		  offset = element.offset();
		  error.insertAfter(element);
		  error.addClass('errormessage');  // add a class to the wrapper
		  error.css('position', 'relative');
		  //error.css('left', offset.left + element.outerWidth());
		  //error.css('top', offset.top);
	  },
	   submitHandler: function(form) {
		   var formData = $("#addMeeting").serialize();
			 $.ajax({
			 	type: 'POST',
			 	data: formData,
			 	url: '<?php echo base_url()?>meeting/checkleavestatus',
			 	cache: false,
			 	async:false,
			 	processData: false,
			 	success:function(data){
		 		 if(data != ""){
			 			var isGood=confirm(data + ' Do you want to continue?');
			 			if (isGood) {
							$.ajax({
							 type: 'POST',
							 data: formData,
							 url: '<?php echo base_url()?>meeting/checkMeeting',
							 cache: false,
							 async:false,
							 processData: false,
							 success:function(data){
									if(typeof busyData =='object')
	  								{
	 									var isMeeting=confirm(busyData+' Do you want to continue?');
	 								  if(isMeeting)
	 								  {
	 										 addMeetingAJAX();
	 								  }
	 							  }else{
									addMeetingAJAX();
								  }
 			 			 	}
 			 			 });
			 			}
			 	 }else{
					 $.ajax({
						type: 'POST',
						data: formData,
						url: '<?php echo base_url()?>meeting/checkMeeting',
						cache: false,
						async:false,
						processData: false,
						success:function(data){
							if(data != ""){
								 var busyData = jQuery.parseJSON(data);
								 if(typeof busyData =='object'){
										var isMeeting=confirm(busyData+' Do you want to continue?');
									  if(isMeeting)
									  {
											 addMeetingAJAX();
									  }
							  }else{
									addMeetingAJAX();
								};
							}else{
								addMeetingAJAX();
							}
					 }
					 });
			 	 }
			 	 return false;
			 	}
			 });
	}
});

function addMeetingAJAX(){
	 var formData = $("#addMeeting").serialize();
	$.ajax({
		type: 'POST',
		url: '<?php echo base_url()?>meeting/addMeeting',
		data: formData,
		cache: false,
		async:false,
		processData: false,
		beforeSend: function(){
			$('.loader').show();
			$('#addMeetingBtn').hide();
		},
		success:function(data){
		$('.loader').hide();
		$('#addMeetingBtn').show();
		 if(data == 1){
			 $('#addMeeting')[0].reset();
			 $(".frmmessage").html("Meeting set successfully!");
			 $(".frmmessage").css("color","#29B6F6");
			 setTimeout(function(){
				window.location.href= '<?php echo base_url()?>meeting';
			 },2000);
		  }else if(data == 2){
				$(".frmmessage").html("You already have meeting on this time.");
				$(".frmmessage").css("color","#d32f2f");
			}else if(data == 3){
				$(".frmmessage").html("Google calendar not found for this email. Please login with same gmail id of HRMS.");
				$(".frmmessage").css("color","#d32f2f");
			}else if(data == 4){
				$(".frmmessage").html("You are not login with google Calander.");
				$(".frmmessage").css("color","#d32f2f");
			}else{
				$(".frmmessage").html("Please try again!");
				$(".frmmessage").css("color","#d32f2f");
			}
		 return false;
		}
});
}

});//document ready
</script>
<script type="text/javascript">
//Manage content with height and width when selecting values from select picker.................
  $('.selectpicker').on('change', function(){
		$('.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn)').css("width","auto").css('max-width','100%');
		$(".filter-option").css("height","auto").css("white-space","normal");
	});
//.........................
// $('.selectpicker').on('change', function(){
// 	 var selected = $(this).find("option:selected").text();
// 	 alert(selected);
// });
$('.bootstrap-select.btn-group .dropdown-menu li a').css('background','red');


</script>
