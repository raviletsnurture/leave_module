<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('max_execution_time', 0);
//Google calendar api code
require_once 'vendor/autoload.php';
session_start();
//Google calendar api code ends here
$client_id = '129765785360-ht4vp2pk3l0jvso9p1o1hvq485al2fg3.apps.googleusercontent.com';
$client_secret = 'YXabklaVS-EFR5l-253VdTly';
$redirect_uri = 'https://'.$_SERVER['HTTP_HOST'].'/meeting';
//$redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].'/ln-projects/demo/hrms/meeting';

$client = new Google_Client();
$client->setApplicationName("My Project");
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->setAccessType('offline');   // Gets us our refreshtoken

$client->setScopes(array('https://www.googleapis.com/auth/calendar'));
// Google calendar API
//For loging out.
if (isset($_GET['logout'])) {
	unset($_SESSION['token']);
}


// Step 2: The user accepted your access now you need to exchange it.
if (isset($_GET['code'])) {

	$client->authenticate($_GET['code']);
	$_SESSION['token'] = $client->getAccessToken();
	$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'].'/meeting';
	header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

// Step 1:  The user has not authenticated we give them a link to login
if (!isset($_SESSION['token'])) {
	$authUrl = $client->createAuthUrl();
	//print "<a class='login' href='$authUrl'>Connect Me!</a>";
}
// Step 3: We have access we can now create our service
if (isset($_SESSION['token'])) {
	$client->setAccessToken($_SESSION['token']);
	//@print "<a class='logout' href='$authUrl?logout=1' >LogOut</a><br>";
}
// Google calendar API



class meeting extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('download');
		$this->load->helper('general_helper');
		$this->load->library('email');
		$this->load->model('meeting_model');
		user_auth(0);
	}

	function index(){
		$userData = $this->logSession;
		$role = $userData[0]->role_id;
		$user_id = $userData[0]->user_id;
		$data['my_create_meetings'] = $this->meeting_model->getMyMeetingsCalendar($user_id);
		$data['invited_meetings'] = $this->meeting_model->getMyInvitedMettings($user_id);

		if($role == '1'){
			//get all meeting
			$data['all_mettings'] = $this->meeting_model->getAllMeetings($user_id);
		}else{
			$data['all_mettings'] = [];
		}

		$data['role'] = $role;

		$data['titalTag'] = ' - Meeting Request';
		$this->template->load_partial('dashboard_master','meeting/list_meeting',$data);
	}

	function add(){
		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
		$data['titalTag'] = ' - Add Meeting';
		$data['members'] = $this->meeting_model->getMembers($user_id);
		$this->template->load_partial('dashboard_master','meeting/add_meeting',$data);
	}

	function checkleavestatus(){
		if(!isset($_POST['recurring']) && empty($_POST['recurring'])){
			$userData = $this->logSession;
			$user_id = $userData[0]->user_id;
			$leave_data = $this->meeting_model->getupcominguserleaves($user_id);

			$leave_taken_users = array();
			if(!empty($leave_data)){

				$total_leave_days = array();
				foreach ($leave_data as $leaves) {
					$leave_start_date = $leaves['leave_start_date'];
					$leave_end_date = $leaves['leave_end_date'];
					$leave_user_id = $leaves['user_id'];
					$period = new DatePeriod(new DateTime($leave_start_date),new DateInterval('P1D'),new DateTime($leave_end_date));

				foreach($period as $list_dates){
	        array_push($total_leave_days,$list_dates->format('Y-m-d'));
	      	}
				  array_push($total_leave_days,$leave_end_date);

					$members = $_POST['check_members'];
					$i=0;

					//Converting values into array
					foreach($members as $member){
						$selected_users[$i] = explode(",",$member);
						$i++;
					}
					//Getting meeting members
					$user_list = array();
					foreach ($selected_users as $get_user) {
						array_push($user_list,$get_user[0]);
					}
					//Adding leave user into variable
					foreach($user_list as $userid){
						if($leave_user_id == $userid){
							foreach($total_leave_days as $user_leaves){
								if($_POST['created_time'] == $user_leaves){
									array_push($leave_taken_users,$leave_user_id);
								}
							}
						}
					}
				$total_leave_days = array();
				}
				$leave_body = '';
				//Getting user informarion
					if(!empty($leave_taken_users)){
					foreach($leave_taken_users as $user_on_leave){
						$data = $this->meeting_model->getLoggedInUser($user_on_leave);
						$leave_body .= $data[0]->first_name.' '.$data[0]->last_name.',';
					}

					$leave_body = rtrim($leave_body,",");
					$meeting_date = new DateTime($_POST['created_time']);
					$leave_body .= ' is on leave for '.$meeting_date->format('d-M-Y').'.';
					echo $leave_body;
					}
			}
			exit;
		}
	}

	function addMeeting(){

		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
		$login_user_details = $this->meeting_model->getLoggedInUser($user_id);

		if($_POST['huser_id'] != '' && $_POST['created_time'] != ''){
			$checkMeetingTime = $this->meeting_model->checkMeetingTime($_POST['created_time'], $_POST['meeting_time'],$user_id);
			if(empty($checkMeetingTime)) {
				// $members = implode (",", $_POST['check_members']);
				$members = $_POST['check_members'];
				$i=0;
				//Converting values into array
				foreach($members as $member){
					$selected_users[$i] = explode(",",$member);
					$i++;
				}
        //Getting members of meeting
				$final_members = '';
				$busy = array();
				foreach($selected_users as $values){
					$final_members .= $values[0].',';
				}
				if(isset($_POST['user_id'])){
					//hr add metting
					$data['user_id'] =$_POST['user_id'];
					$userInfo = $this->meeting_model->getLoggedInUser($data['user_id']);
				}else{
					//employ add metting
					$data['user_id'] =$_POST['huser_id'];
					$userInfo = $this->meeting_model->getLoggedInUser(90);
				}
				$data['hr_role_id'] = 22;
				$data['schedule_date'] =$_POST['created_time'];
				$data['end_date'] = $_POST['end_date'];
				$data['schedule_time'] =$_POST['meeting_time'];
				$data['meeting_duration'] =$_POST['duration'];
				$data['subject'] = str_replace("\n",' ',$_POST['Subject']);
				$data['description'] = str_replace("\n", ' ',$_POST['description']);
				$data['decisionToTake'] = str_replace("\n", ' ',$_POST['decisionToTake']);
				$data['location'] = str_replace("\n",' ',$_POST['location']);
				$data['updated_at'] = date("Y-m-d H:i:s");

				//Recuuring
				if(isset($_POST['recurring']) && !empty($_POST['recurring'])){
						$recurring = $_POST['recurring'];

					//Recurring logic
					if($recurring == 'daily'){
						$period = new DatePeriod(new DateTime($_POST['created_time']),new DateInterval('P1D'),new DateTime($_POST['end_date']));
						$daily_recuuring = array();
					//Creating date list for daily recuuring
						foreach($period as $list_dates){
							array_push($daily_recuuring,$list_dates->format('Y-m-d'));
						}
						array_push($daily_recuuring,$_POST['end_date']);
						//Removing Saturay and Sunday From Array
						$final_daily = array();
						foreach($daily_recuuring as $listofdates){
							$newdate = new DateTime($listofdates);
							$newdate->format('D-m-y').'<br>';
							if($newdate->format('D') != 'Sat' && $newdate->format('D') != 'Sun'){
								array_push($final_daily,$listofdates);
							}
						}

					}
					//Weekly
					if($recurring == 'biweekly'){
						$period = new DatePeriod(new DateTime($_POST['created_time']),new DateInterval('P14D'),new DateTime($_POST['end_date']));
						$weekly_recuuring = array();
					//Creating date list for weekly recuuring
						foreach($period as $list_dates){
							array_push($weekly_recuuring,$list_dates->format('Y-m-d'));
						}
					}
					//Weekly
					if($recurring == 'weekly'){
						$period = new DatePeriod(new DateTime($_POST['created_time']),new DateInterval('P7D'),new DateTime($_POST['end_date']));
						$weekly_recuuring = array();
					//Creating date list for weekly recuuring
						foreach($period as $list_dates){
							array_push($weekly_recuuring,$list_dates->format('Y-m-d'));
						}
					}
					if($recurring == 'monthly'){
						$period = new DatePeriod(new DateTime($_POST['created_time']),new DateInterval('P1M'),new DateTime($_POST['end_date']));
						$monthly_recuuring = array();
						//Creating date list for Monthly recuuring
						foreach($period as $list_dates){
							array_push($monthly_recuuring,$list_dates->format('Y-m-d'));
						}
					}

				}

				//******http://localhost/hrms/meeting**********************
				//Gettin Emails of members
				$j=0;
				foreach($selected_users as $gmail_accounts){
					$gmail_acc_id[$j] = $gmail_accounts[1];
					$j++;
				}
				// Add static Gmail id
				array_push($gmail_acc_id,'lnsmarttv@gmail.com');
				//*****************************

				//google calendar api send email
				if (!isset($_SESSION['token'])) {
					echo 4; // User is not login wiht google calander.
					exit;
				}

				global $client;

				$service = new Google_Service_Calendar($client);
				$calendarList  = $service->calendarList->listCalendarList();


				$calName = $userData[0]->gmail_id;
				$event = new Google_Service_Calendar_Event();
				$event->setSummary($_POST['Subject']);
				$event->setDescription($_POST['description']);
				$event->setLocation($_POST['location']);
				$start = new Google_Service_Calendar_EventDateTime();
				$start->setTimeZone('Asia/Kolkata');
				$start->setDateTime($_POST['created_time'].'T'.$_POST['meeting_time'].':00');
				$event->setStart($start);
				$end = new Google_Service_Calendar_EventDateTime();
				$end->setTimeZone('Asia/Kolkata');
				$end->setDateTime($_POST['created_time'].'T'.$_POST['meeting_time'].':00');
				$event->setEnd($end);
				//Add praticipant
				$attendees = array();

				$attendee = '';
				$i=1;
				$j=0;
				foreach($gmail_acc_id as $calendar_gmail){
							if($calendar_gmail!=="" && !empty($calendar_gmail)){
								${"attendee$i"} = new Google_Service_Calendar_EventAttendee();
								${"attendee$i"}->setEmail($calendar_gmail);
								$attendees[$j] = ${"attendee$i"};
								$i++;
								$j++;
							}
						}

						if((!empty($attendees)) && ($calName!="")){
								$event->attendees = $attendees;
								$optionaArguments = array("sendNotifications"=>true);
								//$createdEvent = $service->events->insert($calName, $event,$optionaArguments);
								try{
										$service->events->insert($calName, $event,$optionaArguments);
								}catch (Exception $e ) {
									//print_r($e);exit;
										//echo 3; // GOOLE CALENDAR EMAIL MISMATCH ERROR
								}
						}

			$data['members'] = rtrim($final_members,',');

			//recurring
			if(isset($_POST['recurring']) && !empty($_POST['recurring'])){

					//Daily Recuuring
					if($recurring == 'daily'){
						foreach($final_daily as $insert_daily_recurring){
							$data['schedule_date'] =$insert_daily_recurring;
							$dataSubmit = $this->meeting_model->addMeeting($data);
						}
					}elseif($recurring == 'weekly'){
						foreach($weekly_recuuring as $insert_weekly_recurring){
							$data['schedule_date'] =$insert_weekly_recurring;
							$dataSubmit = $this->meeting_model->addMeeting($data);
						}
					}elseif($recurring == 'biweekly'){
						foreach($weekly_recuuring as $insert_weekly_recurring){
							$data['schedule_date'] =$insert_weekly_recurring;
							$dataSubmit = $this->meeting_model->addMeeting($data);
						}
					}elseif($recurring == 'monthly'){
						foreach($monthly_recuuring as $insert_monthly_recurring){
							$data['schedule_date'] =$insert_monthly_recurring;
							$dataSubmit = $this->meeting_model->addMeeting($data);
						}
					}

			}else{
				$dataSubmit = $this->meeting_model->addMeeting($data);
			}

			$i=0;
			foreach($_POST['check_members'] as $member_id){
					$data = $this->meeting_model->getLoggedInUser($member_id);
					$email_add[$i] = $data[0]->email;
					$members_name[$i] = $data[0]->first_name.' '.$data[0]->last_name;
					$i++;
			}


			$date_format =date_create($_POST['created_time']);
			$final_meeting_date = date_format($date_format,"d-M-Y");
			$mailData['description'] = '<tr>
      	<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong>'.$login_user_details[0]->first_name.' '.$login_user_details[0]->last_name.' has a meeting with you.</strong></p></td>
      	</tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Date: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$final_meeting_date.'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Time: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.date('g:i a', strtotime($_POST['meeting_time'])).'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Duration: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$_POST['duration'].' Minutes</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Subject: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$_POST['Subject'].'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
	          <p style="margin:4px 0;"></p>
						<p><strong>Agenda: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$_POST['description'].'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Location: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$_POST['location'].'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
	          <p style="margin:4px 0;"></p>
						<p><strong>Participants: </strong> </p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
					<p style="margin:4px 0;"></p><p>';
			$mailData['description'] .= implode(', ', $members_name);
			$mailData['description'] .= '</p></td></tr>';
			$mailData['subject_title'] = $_POST['Subject'];

			//Recuuring mail data
			if(isset($_POST['recurring']) && !empty($_POST['recurring'])){
				$date_format_start =date_create($_POST['created_time']);
				$final_meeting_date_start = date_format($date_format_start,"d-M-Y");
				$date_format_end =date_create($_POST['end_date']);
				$final_meeting_date_end = date_format($date_format_end,"d-M-Y");
					//Daily Recuuring
					$mailData['description'] .= '<tr><td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;"><p><strong>Recuuring Type: </strong></p></td>';
					if($recurring == 'daily'){
							$mailData['description'] .= '<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;"><p>'.ucfirst($recurring).' From '. $final_meeting_date_start.' to '.$final_meeting_date_end.' as scheduled.</p></td>';
					}elseif($recurring == 'weekly'){
							$mailData['description'] .= '<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;"><p>'.ucfirst($recurring).' From '. $final_meeting_date_start.' to '.$final_meeting_date_end.' as scheduled.</p></td>';
					}elseif($recurring == 'biweekly'){
							$mailData['description'] .= '<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;"><p>'.ucfirst($recurring).' From '. $final_meeting_date_start.' to '.$final_meeting_date_end.' as scheduled.</p></td>';
					}elseif($recurring == 'monthly'){
							$mailData['description'] .= '<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;"><p>'.ucfirst($recurring).' From '. $final_meeting_date_start.' to '.$final_meeting_date_end.' as scheduled.</p></td>';
					}

					$mailData['description'] .= '</tr>';
			}

			//Push Notification Logic
			$i=0;
			foreach($_POST['check_members'] as $member_detail_array){
				$member_detail = explode(',', $member_detail_array);
				$memberIds[$i] = $member_detail[0];
				$i++;
			}
			$finalmemberId = implode(',',$memberIds);
			$deviceTokens = getUserDeviceToken($finalmemberId, 0);
			$Tokenlist = array();
			foreach($deviceTokens as $value){
				array_push($Tokenlist,$value->deviceToken);
			}
			$notification_message = '';
			$notification_message .= $_POST['Subject'].' on '.$final_meeting_date.' at '.date('g:i a', strtotime($_POST['meeting_time'])).' @ '.$_POST['location'];
			SendNotificationFCMWeb('HRMS - Meeting',$notification_message,$Tokenlist);
      //Push Notification Logic end


			$activeUserEmails = activeUserEmails(); // Get all active user emails with comma separated
			//$user_emails = array('krunal.letsnurture@gmail.com','kalpesh.letsnurture@gmail.com');
			$this->email->from('hrms@letsnurture.com', "HRMS");
			//$this->email->to($userInfo[0]->email);
			$this->email->to($email_add);
			$this->email->subject('Invitation - '.$_POST['Subject'].' Meeting');
			$body = $this->load->view('mail_layouts/meetingshedule/send_meeting_invitation.php', $mailData, TRUE);
			$this->email->message($body);
			//$this->email->send();
			echo 1; //successfully MEETING CREATED
			}else{
				echo 2; // USER IS ALREADY IN MEETING AT THIS TIME
			}
		} else {
			echo 0;
		}
	}

	function editMeeting(){
		$meeting_id = $_GET['id'];
		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
		$data['titalTag'] = ' - Edit Meeting';
		$data['members'] = $this->meeting_model->getMembers($user_id);
		$data['meeting_id'] = $meeting_id;
		// print_r($data);
		$this->template->load_partial('dashboard_master','meeting/edit_meeting',$data);
	}

	function updateMeeting(){

		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
		$login_user_details = $this->meeting_model->getLoggedInUser($user_id);


		if($_POST['huser_id'] != '' && $_POST['created_time'] != ''){
			$meeting_id = $_POST['meeting_id'];
			$checkMeetingTime = $this->meeting_model->checkUpdateMeetingTime($_POST['created_time'], $_POST['meeting_time'],$user_id, $meeting_id);
			if(empty($checkMeetingTime)) {
				// $members = implode (",", $_POST['check_members']);
				$members = $_POST['check_members'];
				$i=0;
				//Converting values into array
				foreach($members as $member){
					$selected_users[$i] = explode(",",$member);
					$i++;
				}
        //Getting members of meeting
				$final_members = '';
				$busy = array();
				foreach($selected_users as $values){
						$final_members .= $values[0].',';
				}
				if(isset($_POST['user_id'])){
					//hr add metting
					$data['user_id'] =$_POST['user_id'];
					$userInfo = $this->meeting_model->getLoggedInUser($data['user_id']);
				}else{
					//employ add metting
					$data['user_id'] =$_POST['huser_id'];
					$userInfo = $this->meeting_model->getLoggedInUser(90);
				}
				if(isset($_POST['meeting_time']) && !empty($_POST['meeting_time'])){
					$meeting_time = $_POST['meeting_time'];
				}else{
					$meeting_time = $_POST['meeting_expiretime'];
				}

				$data['hr_role_id'] = 22;
				$data['schedule_date'] =$_POST['created_time'];
				$data['end_date'] =$_POST['end_date'];
				$data['schedule_time'] =$meeting_time;
				$data['meeting_duration'] =$_POST['duration'];
				$data['subject'] = str_replace("\n",' ',$_POST['Subject']);
				$data['description'] = str_replace("\n", ' ',$_POST['description']);
				$data['decisionToTake'] = str_replace("\n", ' ',$_POST['decisionToTake']);
				$data['location'] = str_replace("\n",' ',$_POST['location']);
				$data['meeting_id'] =$_POST['meeting_id'];
				$data['mom'] =$_POST['mom'];
				$members = implode (",", $_POST['check_members']);
				$data['members'] =$members;
				$data['updated_at'] = date("Y-m-d H:i:s");

				$dataSubmit = $this->meeting_model->updateMeeting($data);

				$i=0;
				foreach($_POST['check_members'] as $member_id){
						$data = $this->meeting_model->getLoggedInUser($member_id);
						$email_add[$i] = $data[0]->email;
						$members_name[$i] = $data[0]->first_name.' '.$data[0]->last_name;
						$i++;
				}

				//send mail
				$date_format =date_create($_POST['created_time']);
				$final_meeting_date = date_format($date_format,"d-M-Y");
				$mailData['description'] = '<tr>
		      	<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong>'.$login_user_details[0]->first_name.' '.$login_user_details[0]->last_name.' has updated meeting schedule with you.</strong></p></td>
		      </tr>
					<tr>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
		          <p style="margin:4px 0;"></p>
							<p><strong>Date: </strong></p>
						</td>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
		          <p style="margin:4px 0;"></p>
							<p>'.$final_meeting_date.'</p>
						</td>
		      </tr>
					<tr>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
		          <p style="margin:4px 0;"></p>
							<p><strong>Time: </strong></p>
						</td>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
		          <p style="margin:4px 0;"></p>
							<p>'.date('g:i a', strtotime($meeting_time)).'</p>
						</td>
		      </tr>
					<tr>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
		          <p style="margin:4px 0;"></p>
							<p><strong>Duration: </strong></p>
						</td>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
		          <p style="margin:4px 0;"></p>
							<p>'.$_POST['duration'].' Minutes</p>
						</td>
		      </tr>
					<tr>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
		          <p style="margin:4px 0;"></p>
							<p><strong>Subject: </strong></p>
						</td>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
		          <p style="margin:4px 0;"></p>
							<p>'.$_POST['Subject'].'</p>
						</td>
		      </tr>
					<tr>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
		          <p style="margin:4px 0;"></p>
							<p><strong>Agenda: </strong></p>
						</td>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
		          <p style="margin:4px 0;"></p>
							<p>'.$_POST['description'].'</p>
						</td>
		      </tr>
					<tr>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
		          <p style="margin:4px 0;"></p>
							<p><strong>Location: </strong></p>
						</td>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
		          <p style="margin:4px 0;"></p>
							<p>'.$_POST['location'].'</p>
						</td>
		      </tr>';
				if(!empty($_POST['mom'])){
					$mailData['description'] .= '<tr>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
		          <p style="margin:4px 0;"></p>
							<p><strong>MOM: </strong></p>
						</td>
						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
		          <p style="margin:4px 0;"></p>
							<p>'.$_POST['mom'].'</p>
						</td>
		      </tr>';
				}
				$mailData['description'] .= '<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
						<p style="margin:4px 0;"></p>
						<p><strong>Participants: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
						<p style="margin:4px 0;"></p>
						<p>';
				$mailData['description'] .= implode(', ', $members_name);
				$mailData['description'] .= '</p></td></tr>';
				$mailData['subject_title'] = $_POST['Subject'];
				$activeUserEmails = activeUserEmails(); // Get all active user emails with comma separated
				//$user_emails = array('krunal.letsnurture@gmail.com','kalpesh.letsnurture@gmail.com');

				//Push Notification Logic
				$i=0;
				foreach($_POST['check_members'] as $member_detail_array){
				  $member_detail = explode(',', $member_detail_array);
				  $memberIds[$i] = $member_detail[0];
				  $i++;
				}
				$finalmemberId = implode(',',$memberIds);
				$deviceTokens = getUserDeviceToken($finalmemberId);
				$Tokenlist = array();
				foreach($deviceTokens as $value){
					array_push($Tokenlist,$value->deviceToken);
				}
				$notification_message = '';
				$notification_message .= $_POST['Subject'].' on '.$final_meeting_date.' at '.date('g:i a', strtotime($meeting_time)).' @ '.$_POST['location'];
				SendNotificationFCMWeb('HRMS - Meeting Updated',$notification_message,$Tokenlist);
	      //Push Notification Logic

				$this->email->from('hrms@letsnurture.com', "HRMS");
				//$this->email->to($userInfo[0]->email);

				$this->email->to($email_add);
				$this->email->subject('Meeting Schedule Updated- '.$_POST['Subject'].' Meeting');
				$body = $this->load->view('mail_layouts/meetingshedule/send_meeting_invitation.php', $mailData, TRUE);
				$this->email->message($body);
				if ($this->email->send()):
					//echo "Mail sent!"; // Mail sent!
				else:
					//echo "There is error in sending mail!"; // There is error in sending mail!
				endif;
				echo 1;
			 }else{
			 	echo 2;
			 }
		} else {
			echo 0;
		}
	}

	function deleteMeeting(){

		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
		$login_user_details = $this->meeting_model->getLoggedInUser($user_id);

		$meeting_id = $_POST['meeting_id'];

		//Get meeting Details
		$meeting_details = $this->meeting_model->getMyMeetingsDetails($meeting_id);
		$members_id = explode(",",$meeting_details->members);
		$date_format =date_create($meeting_details->schedule_date);
		$final_meeting_date = date_format($date_format,"d-M-Y");
		//helper pass single id or multiple comma seperations id give all email in comma
		$usersEmail = getUserEmail($meeting_details->members);
		$mailData['description'] = '<tr>
	      	<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong>'.ucfirst($login_user_details[0]->first_name).' '.ucfirst($login_user_details[0]->last_name).' has canceled meeting.</strong></p></td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Date: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$final_meeting_date.'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Time: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.date('g:i a', strtotime($meeting_details->schedule_time)).'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Duration: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$meeting_details->meeting_duration.' Minutes</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Subject: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$meeting_details->subject.'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
	          <p style="margin:4px 0;"></p>
						<p><strong>Agenda: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$meeting_details->description.'</p>
					</td>
	      </tr>
				<tr>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
	          <p style="margin:4px 0;"></p>
						<p><strong>Location: </strong></p>
					</td>
					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
	          <p style="margin:4px 0;"></p>
						<p>'.$meeting_details->location.'</p>
					</td>
	      </tr>';
		//send mail for cancel meeting
		$this->email->from('hrms@letsnurture.com', "HRMS");
		$this->email->to('hrms@letsnurture.com');
		$this->email->bcc($usersEmail->usersEmail);
		$this->email->subject('Meeting Cancellation');
		//$body = 'Your meeting has been canceled. Please check your account for more';
		$body = $this->load->view('mail_layouts/meetingshedule/cancel_meeting.php',$mailData, TRUE);
		$this->email->message($body);
		//$this->email->send();

		if(!empty($meeting_id)){
				$this->meeting_model->deleteMeetingData($meeting_id);

				//Push Notification Logic
				$finalmemberId = implode(',',$members_id);
				$deviceTokens = getUserDeviceToken($finalmemberId);
				$Tokenlist = array();
				foreach($deviceTokens as $value){
					array_push($Tokenlist,$value->deviceToken);
				}
				$notification_message = '';
				$notification_message .= $meeting_details->subject.' on '.$final_meeting_date.' at '.date('g:i a', strtotime($meeting_details->schedule_time)).' @ '.$meeting_details->location;
				SendNotificationFCMWeb('HRMS - Meeting Canceled',$notification_message,$Tokenlist);
				//Push Notification Logic

			echo 1;
		}else{
			echo 0;
		}
	}

	function statuslist(){
		$data['titalTag'] = ' - Status Meeting';
		$this->template->load_partial('dashboard_master','meeting/statuslist',$data);
	}

	function getMonthMeeting(){
		if($_POST['date'] != ''){
			$userData = $this->logSession;
			$date = $_POST['date'];
			$role = $userData[0]->role_id;
			$user_id = $userData[0]->user_id;
			if($role != 22){
			//Developer meeting data
			  $datas = $this->meeting_model->getMyMeetings($user_id,$date);
			  foreach ($datas as $data) {?>
				  <div class="col-lg-6">
					<section class="panel">
						<header class="panel-heading">
							<?php echo $data['first_name'].' '.$data['last_name'];?>
						</header>
						<div class="panel-body">
							<form class="form-horizontal frmUpdate" name="frmUpdate<?php echo $data['meeting_id'];?>" id="frmUpdate<?php echo $data['meeting_id'];?>">
								<input type="hidden" value="<?php echo $data['meeting_id'];?>" name="meeting_id" id="meeting_id">
								<div class="form-group">
								  <label class="col-lg-2 col-sm-2 control-label" >Subject</label>
								  <div class="col-lg-10">
									<input type="text" class="form-control" id="subject" name="subject" value="<?php echo $data['subject'];?>" readonly="">
								  </div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 col-sm-2 control-label" >Date</label>
									<div class="col-lg-10">
										<input type="text" class="form-control date" id="date<?php echo $data['meeting_id'];?>"  name="date" value="<?php echo date("m/d/Y",strtotime($data['schedule_date']));?>" readonly="">
									</div>
								</div>
								<div class="form-group">
								   <label class="col-lg-2 col-sm-2 control-label" >Time</label>
								   <div class="col-lg-10">
									   <input type="text" class="form-control" id="meeting_time" name="meeting_time" value="<?php echo $data['schedule_time'];?>" readonly="">
   					   				</div>
								</div>
								<div class="form-group">
									 <label class="col-lg-2 col-sm-2 control-label" >Description</label>
									 <div class="col-lg-10">
										 <textarea rows="5" class="form-control" readonly="" style="resize: none;"><?php echo trim($data['description']);?></textarea>
										</div>
								</div>
								<div class="form-group">
								   <label class="col-lg-2 col-sm-2 control-label" >Location</label>
								   <div class="col-lg-10">
									   <input type="text" class="form-control" id="meeting_time" name="meeting_time" value="<?php echo $data['location'];?>" readonly="">
   					   				</div>
								</div>


								<div class="form-group" >
								   <label class="col-lg-2 col-sm-2 control-label" >Members</label>
								   <div class="col-lg-10">

											 <?php
											 	$members = explode(", ", $data['members']);

												$data_members = array();
												foreach($members as $member_id){
														$meeting_array = $this->meeting_model->getLoggedInUser($member_id);
														$data_members[] = $meeting_array[0]->first_name.' '.$meeting_array[0]->last_name." ";

												}?>
												<textarea rows="5" class="form-control" readonly="" style="resize: none;"><?php echo implode(",",$data_members);?></textarea>
   					   			</div>
								</div>

							</form>
						</div>
					</section>
				</div>
			  <?php
			  }
			}else {
				//Hr meeting data
			  $datas = $this->meeting_model->getHrMeetings($date);
				foreach ($datas as $data) {?>
					<div class="col-lg-6">
	                  <section class="panel">
	                      <header class="panel-heading">
	                          <?php echo $data['first_name'].' '.$data['last_name'];?>
	                      </header>
	                      <div class="panel-body">
	                          <form class="form-horizontal frmUpdate" name="frmUpdate<?php echo $data['meeting_id'];?>" id="frmUpdate<?php echo $data['meeting_id'];?>">
								  <script>
					  			$('#frmUpdate<?php echo $data['meeting_id'];?> #date<?php echo $data['meeting_id'];?>').datepicker({
					  			   //format: 'yyyy-mm-dd',
					  			   daysOfWeekDisabled: [0,6],
					  			   autoclose: true,
					  		   });
					  		   </script>
								  <input type="hidden" value="<?php echo $data['meeting_id'];?>" name="meeting_id" id="meeting_id">
								  <input type="hidden" value="<?php echo $data['user_id'];?>" name="user_id" id="user_id">
								  <div class="form-group">
		                            <label class="col-lg-2 col-sm-2 control-label" >Subject</label>
									<div class="col-lg-10">
	                                  <input type="text" class="form-control" id="subject" name="subject" value="<?php echo $data['subject'];?>" required="">
									</div>
	                              </div>
	                              <div class="form-group">
		                             <label class="col-lg-2 col-sm-2 control-label" >Time</label>
									 <div class="col-lg-10">
										<select name="meeting_time" id="meeting_time" class="form-control" required>
											<option value="">Time</option>
											<option value="09:00" <?php if($data['schedule_time'] == '09:00'){ echo "selected";}?>>09:00</option>
											<option value="09:15" <?php if($data['schedule_time'] == '09:15'){ echo "selected";}?>>09:15</option>
											<option value="09:30" <?php if($data['schedule_time'] == '09:30'){ echo "selected";}?>>09:30</option>
											<option value="09:45" <?php if($data['schedule_time'] == '09:45'){ echo "selected";}?>>09:45</option>
											<option value="10:00" <?php if($data['schedule_time'] == '10:00'){ echo "selected";}?>>10:00</option>
											<option value="10:15" <?php if($data['schedule_time'] == '10:15'){ echo "selected";}?>>10:15</option>
											<option value="10:30" <?php if($data['schedule_time'] == '10:30'){ echo "selected";}?>>10:30</option>
											<option value="10:45" <?php if($data['schedule_time'] == '10:45'){ echo "selected";}?>>10:45</option>
											<option value="11:00" <?php if($data['schedule_time'] == '11:00'){ echo "selected";}?>>11:00</option>
											<option value="11:15" <?php if($data['schedule_time'] == '11:15'){ echo "selected";}?>>11:15</option>
											<option value="11:30" <?php if($data['schedule_time'] == '11:30'){ echo "selected";}?>>11:30</option>
											<option value="11:45" <?php if($data['schedule_time'] == '11:45'){ echo "selected";}?>>11:45</option>
											<option value="12:00" <?php if($data['schedule_time'] == '12:00'){ echo "selected";}?>>12:00</option>
											<option value="12:15" <?php if($data['schedule_time'] == '12:15'){ echo "selected";}?>>12:15</option>
											<option value="12:30" <?php if($data['schedule_time'] == '12:30'){ echo "selected";}?>>12:30</option>
											<option value="12:45" <?php if($data['schedule_time'] == '12:45'){ echo "selected";}?>>12:45</option>
											<option value="01:00" <?php if($data['schedule_time'] == '01:00'){ echo "selected";}?>>01:00</option>
											<option value="01:15" <?php if($data['schedule_time'] == '01:15'){ echo "selected";}?>>01:15</option>
											<option value="01:30" <?php if($data['schedule_time'] == '01:30'){ echo "selected";}?>>01:30</option>
											<option value="01:45" <?php if($data['schedule_time'] == '01:45'){ echo "selected";}?>>01:45</option>
											<option value="02:00" <?php if($data['schedule_time'] == '02:00'){ echo "selected";}?>>02:00</option>
											<option value="02:15" <?php if($data['schedule_time'] == '02:15'){ echo "selected";}?>>02:15</option>
											<option value="02:30" <?php if($data['schedule_time'] == '02:30'){ echo "selected";}?>>02:30</option>
											<option value="02:45" <?php if($data['schedule_time'] == '02:45'){ echo "selected";}?>>02:45</option>
											<option value="03:00" <?php if($data['schedule_time'] == '03:00'){ echo "selected";}?>>03:00</option>
											<option value="03:15" <?php if($data['schedule_time'] == '03:15'){ echo "selected";}?>>03:15</option>
											<option value="03:30" <?php if($data['schedule_time'] == '03:30'){ echo "selected";}?>>03:30</option>
											<option value="03:45" <?php if($data['schedule_time'] == '03:45'){ echo "selected";}?>>03:45</option>
											<option value="04:00" <?php if($data['schedule_time'] == '04:00'){ echo "selected";}?>>04:00</option>
											<option value="04:15" <?php if($data['schedule_time'] == '04:15'){ echo "selected";}?>>04:15</option>
											<option value="04:30" <?php if($data['schedule_time'] == '04:30'){ echo "selected";}?>>04:30</option>
											<option value="04:45" <?php if($data['schedule_time'] == '04:45'){ echo "selected";}?>>04:45</option>
											<option value="05:00" <?php if($data['schedule_time'] == '05:00'){ echo "selected";}?>>05:00</option>
											<option value="05:15" <?php if($data['schedule_time'] == '05:15'){ echo "selected";}?>>05:15</option>
											<option value="05:30" <?php if($data['schedule_time'] == '05:30'){ echo "selected";}?>>05:30</option>
											<option value="05:45" <?php if($data['schedule_time'] == '05:45'){ echo "selected";}?>>05:45</option>
											<option value="06:00" <?php if($data['schedule_time'] == '06:00'){ echo "selected";}?>>06:00</option>
											<option value="06:15" <?php if($data['schedule_time'] == '06:15'){ echo "selected";}?>>06:15</option>
											<option value="06:30" <?php if($data['schedule_time'] == '06:30'){ echo "selected";}?>>06:30</option>
											<option value="06:45" <?php if($data['schedule_time'] == '06:45'){ echo "selected";}?>>06:45</option>
											<option value="07:00" <?php if($data['schedule_time'] == '07:00'){ echo "selected";}?>>07:00</option>
											<option value="07:15" <?php if($data['schedule_time'] == '07:15'){ echo "selected";}?>>07:15</option>
											<option value="07:30" <?php if($data['schedule_time'] == '07:30'){ echo "selected";}?>>07:30</option>
											<option value="07:45" <?php if($data['schedule_time'] == '07:45'){ echo "selected";}?>>07:45</option>
										</select>
									 </div>
	                              </div>
	                              <div class="form-group">
	                                  <label class="col-lg-2 col-sm-2 control-label" >Date</label>
									  <div class="col-lg-10">
	                                  	<input type="text" class="form-control dates" id="date<?php echo $data['meeting_id'];?>"  name="date" value="<?php echo date("m/d/Y",strtotime($data['schedule_date']));?>">
									  </div>
	                              </div>
								  <div class="form-group">
	                                  <label class="col-lg-2 col-sm-2 control-label" >Status</label>
									  <div class="col-lg-10">
	                                  	<select name="status" id="status" class="form-control" required>									        				      <option value="0" <?php if($data['status'] == '0'){ echo "selected";}?>>Inactive</option>
											<option value="1" <?php if($data['status'] == '1'){ echo "selected";}?>>Accepted</option>
											<option value="2" <?php if($data['status'] == '2'){ echo "selected";}?>>Reject</option>
										</select>

									  </div>
	                              </div>
	                              <button class="btn btn-info btnUpdate" id="<?php echo $data['meeting_id'];?>" type="submit">Update</button>
								  <div class="loader">
									<img src="<?php echo base_url()?>assets/img/input-spinner.gif" alt="loading...">
								  </div>
	                          </form>
	                      </div>
	                  </section>
	              </div>
				<?php
				}
			}
		}
	}

	function updateMeetingStatus(){

		if($_POST['meeting_id'] != '' && $_POST['date'] != ''){
			$checkMeetingTime = $this->meeting_model->checkMeetingTime($_POST['date'], $_POST['meeting_time']);
			if(empty($checkMeetingTime)) {
				$data['meeting_id'] = $_POST['meeting_id'];
				$data['subject'] = $_POST['subject'];
				$data['schedule_time'] =$_POST['meeting_time'];
				$data['schedule_date'] = date("Y-m-d",strtotime($_POST['date']));
				$data['status'] =$_POST['status'];
				$dataSubmit = $this->meeting_model->updateMeetingStatus($data);

					//send mail
					$userInfo = $this->meeting_model->getLoggedInUser($_POST['user_id']);
					if($data['status'] == 1){
						$status = 'Accepted';
					}else{
						$status = 'Reject';
					}
					$mailData['description'] = 'Your meeting with HR scheduled on '.$data['schedule_date'].' has been '.$status;
          $activeUserEmails = activeUserEmails(); // Get all active user emails with comma separated
					//$user_emails = array('krunal.letsnurture@gmail.com','kalpesh.letsnurture@gmail.com');
					$this->email->from('hrms@letsnurture.com', "HRMS");
					//$this->email->to($userInfo[0]->email);
					$this->email->to('parth.letsnurture@gmail.com');
					$this->email->subject('Meeting Schedule');
					$body = $this->load->view('mail_layouts/meetingshedule/send_meeting_invitation.php', $mailData, TRUE);
					$this->email->message($body);
					if ($this->email->send()):
						//echo "Mail sent!"; // Mail sent!
					else:
						//echo "There is error in sending mail!"; // There is error in sending mail!
					endif;
				echo 1;
			}else{
				echo 2;
			}
		} else {
			echo 0;
		}
	}
	/**
	   * @Method      :  POST
	   * @Params      :
	   * @author      : Jignasa
	   * @created     : (31-07-2017)
	   * @Modified by :
	   * @Status      :
	+
	   * @Comment      : To check whether the member user has meeting on given date and time
	   **/
	function checkMeeting(){

		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
		$login_user_details = $this->meeting_model->getLoggedInUser($user_id);
		if($_POST['huser_id'] != '' && $_POST['created_time'] != ''){
			// $members = implode (",", $_POST['check_members']);
			$members = $_POST['check_members'];
			$i=0;
			//Converting values into array
			foreach($members as $member){
				$selected_users[$i] = explode(",",$member);
				$i++;
			}
			//Getting members of meeting
			$final_members = '';
			$busy = array();
			foreach($selected_users as $values){
				$checkMemberMeetingTime = $this->meeting_model->checkMemberMeetingTime($_POST['created_time'], $_POST['meeting_time'],$values[0]);
				if(!empty($checkMemberMeetingTime)){

					$busyUser = $this->meeting_model->getUserDetail($values[0]);
					$memberUser = $this->meeting_model->getUserDetail($checkMemberMeetingTime[0]['member']);
					$busy[] = $busyUser[0]['first_name']." ".$busyUser[0]['last_name']." has meeting with ".$memberUser[0]['first_name']." ".$memberUser[0]['last_name']." on ".$checkMemberMeetingTime[0]['schedule_date']." at ".$checkMemberMeetingTime[0]['schedule_time']." for ".$checkMemberMeetingTime[0]['meeting_duration']." minutes";
				}
					$final_members .= $values[0].',';
			}
			if(!empty($busy)){
				foreach ($busy as $busyData) {
					$html[] = $busyData;
				}
				print_r(json_encode($html));
				return json_encode($html);
			}
		}
	}

	/**
	   * @Method      :  POST
	   * @Params      :
	   * @author      : Jignasa
	   * @created     : (31-07-2017)
	   * @Modified by :
	   * @Status      :
		 	*
	   * @Comment      : To check whether the member user(for update) has meeting on given date and time
	   **/
	function checkUpdateMeeting(){
		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
		$login_user_details = $this->meeting_model->getLoggedInUser($user_id);
		if($_POST['huser_id'] != '' && $_POST['created_time'] != ''){
			$meeting_id = $_POST['meeting_id'];
			// $members = implode (",", $_POST['check_members']);
			$members = $_POST['check_members'];
			$i=0;
			//Converting values into array
			foreach($members as $member){
				$selected_users[$i] = explode(",",$member);
				$i++;
			}
			//Getting members of meeting
			$final_members = '';
			$busy = array();
			foreach($selected_users as $values){
				$checkMemberMeetingTime = $this->meeting_model->checkUpdateMemberMeetingTime($_POST['created_time'], $_POST['meeting_time'],$values[0],$meeting_id);
				if(!empty($checkMemberMeetingTime)){

					$busyUser = $this->meeting_model->getUserDetail($values[0]);
					$memberUser = $this->meeting_model->getUserDetail($checkMemberMeetingTime[0]['member']);
					$busy[] = $busyUser[0]['first_name']." ".$busyUser[0]['last_name']." has meeting with ".$memberUser[0]['first_name']." ".$memberUser[0]['last_name']." on ".$checkMemberMeetingTime[0]['schedule_date']." at ".$checkMemberMeetingTime[0]['schedule_time']." for ".$checkMemberMeetingTime[0]['meeting_duration']." minutes";
				}
					$final_members .= $values[0].',';
			}
			if(!empty($busy)){
				foreach ($busy as $busyData) {
					$html[] = $busyData;
				}
				//print_r(json_encode($html));
				return json_encode($html);
			}
		}
	}

}

?>
