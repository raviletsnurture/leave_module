<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MX_Controller {
	var $logSession;
    function __construct(){
        parent::__construct();
        $this->template->set('controller', $this);
    		$this->load->model('dashboardmodel');
    		$this->load->model('user/user_model');
    		$this->logSession = $this->session->userdata("login_session");
    		user_auth(0);
    }

    public function index(){
		$dashData['titalTag'] = ' - Dashboard';
		$userData = $this->logSession;
		$user_id = $userData[0]->user_id;
    $today = date('Y-m-d H:i:s');
		$dashData['userData'] = $userData;
    $dashData['leaves'] = $this->dashboardmodel->getuserleaves($user_id);
    $dashData['otherleaves'] = $this->dashboardmodel->getotheruserleaves($user_id);
    $dashData['otherleaves_section'] = $this->dashboardmodel->getotheruserleaves($user_id);
    $dashData['upcomingleaves_section'] = $this->dashboardmodel->getupcominguserleaves($user_id);
  	$dashData['upcomingmeetings_section'] = $this->dashboardmodel->getupcomingmeetings();
		$dashData['newJoinings'] = $this->dashboardmodel->getNewJoiningUsers();	
		$dashData['lateEntryList'] = $this->dashboardmodel->getUserByLateEntry($user_id);	
	
		$up['last_login']=date('Y-m-d H:i:s');
		$this->db->where('user_id',$user_id);
		$this->db->update('crm_users',$up);
		//Total padding leave
		$checkPendingLeavetotal = $this->dashboardmodel->getPendingLeaveTotal();
		if($checkPendingLeavetotal->pendingLeavetotal){
			$dashData['pendingLeavetotal'] =  $checkPendingLeavetotal->pendingLeavetotal;
		}else{
			$dashData['pendingLeavetotal'] = 0;
		}

        //Early leaves of current month
        $getEarlyPendingLeave = $this->dashboardmodel->getEarlyPendingLeave($today);
        $dashData['monthEarlyLeave'] = $getEarlyPendingLeave->monthEarlyLeave;


		//Padding leaves of current month
		$getMonthPendingLeave = $this->dashboardmodel->getMonthPendingLeave($today);
		if($getMonthPendingLeave->monthPendingLeave){
			$dashData['monthPendingLeave'] = $getMonthPendingLeave->monthPendingLeave;
		}else{
			$dashData['monthPendingLeave'] = 0;
		}


		//Birthday and Anniversary leaves of current month
		$getMonthBirthAnniLeave = $this->dashboardmodel->getMonthBirthAnniLeave($today);
		if($getMonthBirthAnniLeave->monthBirthAnniLeave){
			$dashData['monthBirthAnniLeave'] = $getMonthBirthAnniLeave->monthBirthAnniLeave;
		}else{
			$dashData['monthBirthAnniLeave'] = 0;
		}


		//Total current month leave
	    $checkTotalNumberLeave = $this->dashboardmodel->checkTotalNumberLeave($today);
		if($checkTotalNumberLeave->totalLeave){
			$dashData['totalThisMonthLeave'] =  $checkTotalNumberLeave->totalLeave;
		}else{
			$dashData['totalThisMonthLeave'] = 0;
		}


		if($this->dashboardmodel->getActiveEmployee()){
			$dashData['getActiveEmployee'] = $this->dashboardmodel->getActiveEmployee();
		}else{
			$dashData['getActiveEmployee'] = 0;
		}

		$this->template->load_partial('dashboard_master','dashboard',$dashData);


		$userData = $this->logSession;
		$id = $userData[0]->user_id;
    auditUserActivity('Dashboard', 'Index', $id, date('Y-m-d H:i:s'),$id);

    }

		public function getTotalLeave(){
			$today = date("Y-m-01 H:i:s", strtotime($_POST['month']));
			//Total current month leave
			$checkTotalNumberLeave = $this->dashboardmodel->checkTotalNumberLeave($today);
			$getMonthPendingLeave = $this->dashboardmodel->getMonthPendingLeave($today);
			$getMonthBirthAnniLeave = $this->dashboardmodel->getMonthBirthAnniLeave($today);

             //Early leaves of current month
            $getEarlyPendingLeave = $this->dashboardmodel->getEarlyPendingLeave($today);
            $data['monthEarlyLeave'] = $getEarlyPendingLeave->monthEarlyLeave;


			if($checkTotalNumberLeave->totalLeave){
				$data['totalLeave'] = $checkTotalNumberLeave->totalLeave;
			}else{
				$data['totalLeave'] = "0";
			}
			if($getMonthPendingLeave->monthPendingLeave){
				$data['monthPendingLeave'] = $getMonthPendingLeave->monthPendingLeave;
			}else{
				$data['monthPendingLeave'] =  "0";
			}
			if($getMonthBirthAnniLeave->monthBirthAnniLeave){
				$data['monthBirthAnniLeave'] = $getMonthBirthAnniLeave->monthBirthAnniLeave;
			}else{
				$data['monthBirthAnniLeave'] =  "0";
			}
			echo json_encode($data);
		}


    /*
     * @Author:  krunal
     * @Created: September 02 2015
     * @Modified By:
     * @Modified at:
     * @Comment: Announcements sidebar with paggination
     */
    function getAnnouncementsSidebar() {
     $CI = & get_instance();
     $sql = "SELECT count(*) as count
             FROM crm_announcement
             WHERE published_status = '1'
             ORDER BY created_time DESC";
     $query = $CI->db->query($sql);
     $totalresults = $query->result();
     $count= $totalresults[0]->count;
     // For pagination
     $page = (int) (!isset($_POST['pageId']) ? 1 :$_POST['pageId']);
     $page = ($page == 0 ? 1 : $page);
     $recordsPerPage = 2;
     $start = ($page-1) * $recordsPerPage;
     $class='meat_page';
     //pass pagination value in pagination function
     $pagination= pagination($count,$page,$recordsPerPage,$class);

     $sql = "SELECT *
             FROM crm_announcement
             WHERE published_status = '1'
             ORDER BY created_time DESC LIMIT $start, $recordsPerPage";
     $query = $CI->db->query($sql);
     $results = $query->result(); ?>
         <ul>
         <?php for($a=0; $a<count($results); $a++): ?>
             <li>
                 <div class="subdiv">
                     <span><?php echo $results[$a]->title; ?></span>
                     <span class="date"><?php echo date("d-m-Y", strtotime($results[$a]->created_time)); ?></span>
                     <div class="clearfix"></div>
                     <p><?php echo $results[$a]->description; ?></p>
                 </div>
             </li>
         <?php endfor;?>
        </ul>
     <?php echo $pagination;
    }

    function deviceToken()
    {
      $userData = $this->logSession;
  		$user_id = $userData[0]->user_id;
      $deviceToken = $_POST['deviceToken'];
      $checkToken = $this->dashboardmodel->checkDeviceToken($deviceToken);
      if($checkToken->total == 0){
        $data['deviceToken'] = $deviceToken;
        $data['user_id'] = $user_id;
				$data['deviceType'] = 0;
        $data['device_created'] = date("Y-m-d H:i:s");
        $data['device_modified'] = date("Y-m-d H:i:s");
        $this->dashboardmodel->saveDeviceToken($data);
      }
    }

		function testnotification(){
			 $userId = 137;
			 //$deviceTokens = getSeniorTokens($userId, 'leave_users');
			 $deviceTokens = getAllDeviceToken();//send to all user
			 $Tokenlist = array();
			 foreach($deviceTokens as $value){
				 array_push($Tokenlist,$value->deviceToken);
			 }
			 $notification_message = 'HRMS - Announcement';
			 SendNotificationFCMWeb('Welcome to HRMS',$notification_message,$Tokenlist);
		}
}
?>
