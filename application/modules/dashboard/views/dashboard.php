<?php
    $allbirthday = getAllUserBirthday();
    $preleave = getAllPredefineLeave();
    $getAnnouncements = getAnnouncements();
    $gettop5rewardWinner = getTop5rewardWinner();
    $getToprewardWinnerMonthly = getToprewardWinnerMonthly();
?>

<style>
.fc-sat,
.fc-sun {
    background: #F1F790;
}

.orange {
    background: #8271FF;
}

.fc-event-hori {
    border: none !important;
}

.fc-state-default {
    padding: 8px 12px 8px 12px;
    border-left-style: solid;
    border-right-style: solid;
    border-color: #ddd;
    border-width: 0 1px;
}

.scroll-height {
    height: auto !important;
}

} .panel {
    margin-bottom: 0;
}
.upcommingleavespanel .activity .activity-desk,
.announcementspanel .activity .activity-desk {
    margin-left: 53px;
}
.fixed_height {
    height: 212px;
}
.scroll-vertical{
    overflow-y: scroll;
}
</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <!-- Top Reward Points winner forever -->
            <!-- <div class="col-lg-<?php if($userData[0]->role_id==20 || $userData[0]->role_id==15){echo "3";}else{echo "4";}?> col-md-6 col-sm-12">
                <section class="panel announcementspanel">
                    <header class="panel-heading">
                        Top 3 reward winners
                    </header>
                    <?php if (!empty($gettop5rewardWinner) && $gettop5rewardWinner != ''){ ?>
                    <div class="panel-body profile-activity">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>Reward Points</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $x=1;
                        for($a=0; $a<count($gettop5rewardWinner); $a++): ?>
                                <tr>
                                    <td style="font-size:14px; color: #7B787B;">
                                        <?php echo $x.'.&nbsp;'; echo $gettop5rewardWinner[$a]["first_name"];echo " "; echo $gettop5rewardWinner[$a]["last_name"];?>
                                    </td>
                                    <td style="font-size:14px; color: #7B787B;">
                                        <?php echo $gettop5rewardWinner[$a]["reward_points"];?></td>
                                </tr>
                                <?php $x++;
                        endfor; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php }else{?>
                    <div style="min-height:254px;">
                        <h5 style="margin:0;padding-top:95px;text-align:center;">No winners yet</h5>
                    </div>
                    <?php } ?>

                </section>
                <section class="panel announcementspanel">
                    <header class="panel-heading">
                        Top 3 reward winners this month
                    </header>
                    <?php if (!empty($getToprewardWinnerMonthly) && $getToprewardWinnerMonthly != ''){ ?>
                    <div class="panel-body profile-activity fixed_height">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Reward Points</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php $x=1;
                           for($a=0; $a<count($getToprewardWinnerMonthly); $a++): ?>
                                <tr>
                                    <td style="font-size:14px; color: #7B787B;">
                                        <?php echo $x.'.&nbsp';echo $getToprewardWinnerMonthly[$a]->first_name;echo " ";echo $getToprewardWinnerMonthly[$a]->last_name;?>
                                    </td>
                                    <td style="font-size:14px; color: #7B787B;">
                                        <?php echo $getToprewardWinnerMonthly[$a]->totalpoints;?></td>
                                </tr>
                                <?php $x++;
                          endfor; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php }else{?>
                    <div class="fixed_height">
                        <h5 style="margin:0;padding-top:95px;text-align:center;">No winners this month</h5>
                    </div>
                    <?php } ?>
                </section>
            </div> -->
            <?php if(!empty($getAnnouncements)): ?>
            <div class="col-lg-<?php if($userData[0]->role_id==20 || $userData[0]->role_id==15){echo "4";}else{echo "8";}?> col-md-6 col-sm-12">
                <section class="panel announcementspanel">
                    <header class="panel-heading">
                        Announcements
                    </header>
                    <div class="panel-body profile-activity leave_scroll">
                        <?php for($a=0; $a<count($getAnnouncements); $a++): ?>
                        <div class="activity blue">
                            <div class="activity-desk">
                                <div class="panel">
                                    <div class="panel-body">
                                        <!-- <div class="arrow"></div> -->
                                        <span>
                                            <i class="fa fa-bullhorn"></i>
                                        </span>
                                        <!-- <i class=" fa fa-clock-o"></i> -->
                                        <h5><?php echo $getAnnouncements[$a]->title; ?> <em
                                                style="font-size:14px; color: #c3c3c3;">On
                                                <?php echo date("d-m-Y", strtotime($getAnnouncements[$a]->created_time)); ?></em>
                                        </h5>
                                        <div class="clearfix"></div>
                                        <p><?php echo $getAnnouncements[$a]->description; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endfor; ?>
                    </div>
                </section>
            </div>
            <!-- Show upcomming leaves -->
            <div class="col-lg-<?php if($userData[0]->role_id==20 || $userData[0]->role_id==15){echo "4";}else{echo "4";}?> col-md-6 col-sm-12">
                <!--add removed class "leave_scroll" -->
                <section class="panel upcommingleavespanel">
                    <header class="panel-heading">
                        Upcoming leaves
                    </header>
                    <div class="panel-body profile-activity fixed_height scroll-vertical">
                        <?php
                    $curr_date = new DateTime();
                    $curr_date->format('Y-m-d');
                    $count = count($upcomingleaves_section);
                    $date_count = 0;
                    for($i=0;$i<$count;$i++){
                        $user_date = new DateTime($upcomingleaves_section[$i]['leave_start_date']);
                        if($user_date > $curr_date){
                          $start_date = new DateTime($upcomingleaves_section[$i]['leave_start_date']);
                          $end_date = new DateTime($upcomingleaves_section[$i]['leave_end_date']);

                          $date_diff = $start_date->diff($end_date);
                          if((int)$date_diff->format('%a ') > 0){
                              $date_diff_text = '('.((int)$date_diff->format('%a ') + 1).' days)';
                          }else{
                              $date_diff_text = '('.((int)$date_diff->format('%a ') + 1).' day)';
                          }
                          ?>
                        <div class="activity blue">
                            <div class="activity-desk">
                                <div class="panel">
                                    <div class="panel-body">
                                        <!-- <div class="arrow"></div> -->
                                        <span><i class="fa fa-calendar"></i></span>
                                        <h5 style="color:#797979;">
                                            <?php echo $upcomingleaves_section[$i]['first_name'].' '.$upcomingleaves_section[$i]['last_name']; echo ' '.$date_diff_text; echo " - ".$upcomingleaves_section[$i]['leave_name'];?>
                                            On
                                            <em
                                                style="font-size:14px; color: #c3c3c3;"><?php echo $start_date->format('dS, M Y').' - '.$end_date->format('dS, M Y');?></em>
                                        </h5>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                          $date_count++;
                        }else{
                        continue;
                        }
                    }
                    ?>
                    </div>
                </section>
                <section class="panel upcommingleavespanel">
                    <header class="panel-heading">
                        Late entry list for current month (<?php echo date('M')?>)
                    </header>
                    <div class="panel-body profile-activity fixed_height scroll-vertical">
                    <?php if(sizeof($lateEntryList)>=1){?>
                        <table class="table table-striped">
                            <thead>
                                    <tr>
                                        <th>Slno</th>
                                        <th>Day</th>
                                        <th>Date</th>
                                    </tr>
                            </thead>
                            <tbody>
                                <?php $x=1; foreach($lateEntryList as $list){?>
                                    <tr>
                                        <td><?php echo $x?></td>
                                        <td><?php echo date('l',strtotime($list['log_date']))?></td>
                                        <td><?php echo date('d-M-Y',strtotime($list['log_date']))?></td>
                                    </tr>
                                <?php $x++; }?>
                            </tbody>
                        </table>
                  <?php  }else{
                      echo "No late entry this month";
                  }?>
                    </div>
                </section>
            </div>

            <?php if($userData[0]->role_id==20 || $userData[0]->role_id==15){ ?>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <section class="panel upcommingleavespanel">
                        <header class="panel-heading">
                            List of employees that will be joining this month
                        </header>
                        <div class="panel-body profile-activity leave_scroll">
                            <?php if(sizeof($newJoinings)>=1){?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Department</th>
                                            <th>Joining date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($newJoinings as $newJoinings): ?>
                                        <tr>
                                            <td><?php echo $newJoinings['first_name'].' '.$newJoinings['last_name']?></td>
                                            <td><?php echo $newJoinings['department_name']?></td>
                                            <td><?php echo date("d-M-Y", strtotime($newJoinings['joining_date'])); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                           <?php }else{
                               echo "No new joinings this month";
                           }?>
                        </div>
                    </section>
                </div>
            <?php }?>
            <!-- Show upcomming meetings -->
            <!-- <div class="col-lg-3 col-md-6">
              <section class="panel">
                  <header class="panel-heading">
                      Upcomming Meetings
                  </header>
                  <div class="panel-body profile-activity leave_scroll">
                      <?php for($a=0; $a<count($upcomingmeetings_section); $a++):
                          $memberName = [];
                          $array = explode(',',$upcomingmeetings_section[$a]['members']);
                          foreach ($array as $member) {
                      		$memberData = getUserDetail($member);
                          if(isset($memberData->first_name) && isset($memberData->last_name))
                          {
                            $memberName[] = $memberData->first_name." ".$memberData->last_name;
                          }
                          }
                          $allMembers = implode(', ',$memberName);?>
                          <div class="activity blue">
                              <span>
                                  <i class="fa fa-bullhorn"></i>
                              </span>
                              <div class="activity-desk">
                                  <div class="panel">
                                      <div class="panel-body">
                                          <div class="arrow"></div> -->
            <!-- <i class=" fa fa-clock-o"></i> -->
            <!-- <h5><?php echo $upcomingmeetings_section[$a]['subject']; ?> <em style="font-size:14px; color: #c3c3c3;">On <?php echo date("d-m-Y", strtotime($upcomingmeetings_section[$a]['schedule_date'])); ?> <?php echo date("g:i a", strtotime($upcomingmeetings_section[$a]['schedule_time'])); ?></em></h5>
                                          <p><?php echo $upcomingmeetings_section[$a]['first_name']." ".$upcomingmeetings_section[$a]['last_name']; ?> has meeting with <?php echo $allMembers; ?></p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        <?php endfor; ?>
                </div>
              </section>
            </div> -->
        </div>
        <?php endif; ?>

        <div class="row">
            <aside class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <h4 style="float: left; margin-right:20px;"><b>Employee Leaves</b></h4>
                        <?php if($userData[0]->role_id == 1 || $userData[0]->role_id == 13 || $userData[0]->role_id == 15 || $userData[0]->role_id == 20 || $userData[0]->role_id == 22){
                              $totalClass = '';
                            }else{
                              $totalClass = 'hidden';
                            }?>
                        <h4 class="<?php echo $totalClass;?>" style="float: left;">Total : <span
                                class="totalMonth"><?php echo $totalThisMonthLeave;?></span> Leaves,
                            <span class="totalEarlyMonth"><?php echo $monthEarlyLeave;?></span> Early Leave, <span
                                class="totalPendingMonth"><?php echo $monthPendingLeave;?></span> Pending, <span
                                class="monthBirthAnniLeave"><?php echo $monthBirthAnniLeave;?></span> Birthday &
                            Anniversary Leaves, <?php echo $pendingLeavetotal;?> Pending(all),
                            <?php echo $getActiveEmployee;?> Employees</h4>

                    </header>
                    <div class="panel-body">
                        <div id="leavecalendar" class="has-toolbar"></div>
                    </div>
                </section>
            </aside>
        </div>
        <div id="eventContent" title="Event Details">
            <div id="eventInfo"></div>
            <p><strong><a id="eventLink" target="_blank"></a></strong></p>
        </div>
    </section>
</section>

<script>
$(document).ready(function() {
    $('#scroll-announcements').niceScroll({
        styler: "fb",
        cursorcolor: "#e8403f",
        cursorwidth: '6',
        cursorborderradius: '10px',
        background: '#404040',
        spacebarenabled: false,
        cursorborder: '',
        zindex: '1000'
    });

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#leavecalendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'basicDay,basicWeek,month'
        },
        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar !!!

        events: [
            <?php for($i=0;$i<sizeof($leaves);$i++) { ?> {
                title: '<?php echo $leaves[$i]['first_name'].' '.$leaves[$i]['last_name']; if($leaves[$i]['is_cancelled'] == 1) { echo " (Cancelled)"; } else { echo " (".$leaves[$i]['leave_status_name'].")"; } ?>',
                start: '<?php echo $leaves[$i]['leave_start_date']; ?> 00:00:00',
                end: '<?php echo $leaves[$i]['leave_end_date']; ?> 00:00:00',
                description: '<div>Department: <?php echo $leaves[$i]['department_name']; ?><br />Leave Type: <?php echo $leaves[$i]['leave_name']; ?><br />Leave Status: <?php if($leaves[$i]['is_cancelled'] == 1) { echo "Cancelled"; } else { echo $leaves[$i]['leave_status_name']; } ?></div>',
                backgroundColor: '<?php if($leaves[$i]['is_cancelled'] == 1) { echo '#ED5757'; } else { if($leaves[$i]['leave_status_name'] == 'Pending') { echo '#6883a3'; } elseif($leaves[$i]['leave_status_name'] == 'Approved') { echo '#378006'; } else { echo '#FA5578'; } }?>',
                textColor: '#fff',
            },
            <?php } ?>
            <?php for($i=0;$i<sizeof($otherleaves);$i++) { ?> {
                title: '<?php echo $otherleaves[$i]['first_name'].' '.$otherleaves[$i]['last_name']; if($otherleaves[$i]['is_cancelled'] == 1) { echo " (Cancelled)"; } else { echo " (".$otherleaves[$i]['leave_status_name'].")"; } ?>',
                start: '<?php echo $otherleaves[$i]['leave_start_date']; ?> 00:00:00',
                end: '<?php echo $otherleaves[$i]['leave_end_date']; ?> 00:00:00',
                description: '<div>Department: <?php echo $otherleaves[$i]['department_name']; ?><br />Leave Type: <?php echo $otherleaves[$i]['leave_name']; ?><br />Leave Status: <?php if($otherleaves[$i]['is_cancelled'] == 1) { echo "Cancelled"; } else { echo $otherleaves[$i]['leave_status_name']; } ?></div>',
                backgroundColor: '<?php if($otherleaves[$i]['is_cancelled'] == 1) { echo '#ED5757'; } else { if($otherleaves[$i]['leave_status_name'] == 'Pending') { echo '#6883a3'; } elseif($otherleaves[$i]['leave_status_name'] == 'Approved') { echo '#378006'; } else { echo '#FA5578'; } }?>',
                textColor: '#fff',
            },
            <?php } ?>
            <?php foreach($allbirthday as $birth) { if($birth['month'] < 10) { $d = '0'; } else { $d = ''; }  if($birth['day'] < 10) { $d1 = '0'; } else { $d1 = ''; } ?> {
                title: '<?php echo $birth['fullname']; ?>',
                start: '<?php echo Date("Y"); ?>-<?php echo $d.$birth['month']; ?>-<?php echo $d1.$birth['day']; ?>',
                imageurl: 'img',
                description: '',
                backgroundColor: '#41cac0',
                textColor: '#fff',
            },
            <?php } ?>
            <?php foreach($preleave as $prel) { ?> {
                title: '<?php echo $prel['festival_name']; ?>',
                start: '<?php echo $prel['festival_date']; ?>',
                end: '<?php echo $prel['festival_date']; ?>',
                image: 'img',
                description: '',
                backgroundColor: '#8271FF',
                textColor: '#fff',
            },
            <?php } ?>
        ],
        eventRender: function(event, eventElement) {
            if (event.imageurl) {
                eventElement.find("div.fc-event-inner").prepend(
                    "<i class='fa fa-gift' style='font-size:16px;'></i>&nbsp;");
            }
            if (event.image) {
                eventElement.find("div.fc-event-inner").prepend(
                    "<i class='fa fa-smile-o' style='font-size:16px;'></i>&nbsp;");
                var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
                $('.fc-day[data-date="' + dateString + '"]').addClass('orange');
            }

        },
        eventClick: function(event, jsEvent, view) {

            if (!event.image && !event.imageurl) {
                $("#eventInfo").html(event.description);
                $("#eventLink").attr('href', event.url);
                $("#eventContent").dialog({
                    modal: true,
                    title: event.title
                });
            }
        },
        /*eventMouseover: function(event, jsEvent, view) {
        	$('.fc-event-inner', this).append('<div id=\"'+event.id+'\" class=\"hover-end\">'+event.description+'</div>');
        },
        eventMouseout: function(event, jsEvent, view) {
        	$('#'+event.id).remove();
        }*/
    });

    $("body").on('click', ".fc-button-next,.fc-button-prev", function() {
        var month = $(".fc-header-title h2").html();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url()?>dashboard/getTotalLeave',
            data: 'month=' + month,
            beforeSend: function() {},
            success: function(data) {
                data = JSON.parse(data);
                $(".panel-heading h4 .totalMonth").html(data.totalLeave);
                $(".panel-heading h4 .totalPendingMonth").html(data.monthPendingLeave);
                $(".panel-heading h4 .monthBirthAnniLeave").html(data.monthBirthAnniLeave);
                $(".panel-heading h4 .totalEarlyMonth").html(data.monthEarlyLeave);
            }
        });
    });
});
</script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase.js"></script>
<script src="<?php echo base_url();?>assets/js/notification.js"></script>
