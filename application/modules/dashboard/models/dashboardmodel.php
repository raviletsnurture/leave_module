<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboardmodel extends MX_Controller{

	function __construct(){
		parent::__construct();
	}

	function getuserleaves($id)
	{
		$this->db->select("ls.leave_status as leave_status_name,d.department_name,lt.leave_type as leave_name,u.first_name,u.last_name,l.*");
		$this->db->from("leaves as l");
		$this->db->join("users as u","u.user_id = l.user_id","left");
		$this->db->join("leave_type as lt","lt.leave_type_id = l.leave_type","left");
		$this->db->join("leave_status as ls","ls.leave_status_id = l.leave_status","left");
		$this->db->join("department as d","d.department_id = u.department_id","left");
		$this->db->where("l.user_id",$id);
		$this->db->where("u.status = 'Active'");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getotheruserleaves($userid)
	{
		$this->db->select('au.first_name as afirst_name,au.last_name as alast_name,d.department_name,ls.leave_status as leave_status_name,lt.leave_type as leave_name,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('users as au','au.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		// $this->db->where("l.is_cancelled != 1");
		// $this->db->where("l.leave_status = 1 OR l.leave_status = 2");
		//$this->db->order_by("leave_id","desc");
		$this->db->order_by("l.leave_start_date","asc");
		$this->db->where('l.user_id != '.$userid);
		$this->db->where("u.status = 'Active'");
		//$this->db->where('u.department_id != '.$id);
		$query=$this->db->get()->result_array();
		return $query;
	}

	function getupcominguserleaves($userid){
		$this->db->select('ls.leave_status as leave_status_name,lt.leave_type as leave_name,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		//$this->db->join('users as au','au.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		//$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("l.is_cancelled != 1");
		$this->db->where("l.leave_start_date > now()");
		$this->db->where("(l.leave_status = 1 OR l.leave_status = 2)");
		//$this->db->order_by("leave_id","desc");
		$this->db->order_by("l.leave_start_date","asc");
		$this->db->where('l.user_id != '.$userid);
		//$this->db->where('u.department_id != '.$id);
		$this->db->limit(5);
		$query=$this->db->get()->result_array();
		return $query;
	}

	function saveDeviceToken($data){
	  if($this->db->insert('device',$data)){
				return true;
		}else{
			return false;
		}
	}

	function checkDeviceToken($deviceToken)
	{
		$this->db->select("count(device_id) as total");
		$this->db->from("device");
		$this->db->where("deviceToken",$deviceToken);
		$this->db->limit(1);
		$query = $this->db->get()->row();
		return $query;
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	leave type = EL, Urgent, Birthday Leave, Anniversary Leave, Marriage Leave, Early Leave.
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function checkTotalNumberLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		/*$this->db->select('SUM( l.leave_days) as totalLeave');
		$this->db->from('leaves as l');
		//$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		//$this->db->where('l.leave_type in (3,4,6,8,9,10)');
		//$this->db->where('l.leave_status = 2');
		$this->db->where("l.is_cancelled != '1'");
		$query=$this->db->get();
		return $query->row();*/
		$this->db->select('SUM(days) as totalLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}



	/*
	* Developed By : Krunal
	* Date : 06/04/2017
	* Modified By :
	* Description :  get Pending leave Total
	*/
	function getPendingLeaveTotal(){
		$datetime = new DateTime();
		$datetime->setTimezone(new DateTimeZone('Asia/Kolkata'));
		$datetime->add(new DateInterval("P1D"));
		$startDate = $datetime->format('Y-m-d');
		$this->db->select("sum(leave_days) as pendingLeavetotal");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where("l.leave_status = '1'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("l.leave_start_date <= '$startDate'");
		$this->db->where("u.status =  'Active'");
		$result = $this->db->get()->row();
		return $result;
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function getActiveEmployee(){
		$this->db->select('COUNT(user_id)');
		$this->db->from('crm_users');
		$this->db->where("status =  'Active'");
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Jignasa
	* Date : 31/07/2017
	* Modified By :
	* Description :  get Pending leaves of current month
	*/
	function getMonthPendingLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		$this->db->select('SUM(l.leave_days) as monthPendingLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		$this->db->where("leave_status = '1'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : Jignasa
	* Date : 31/07/2017
	* Modified By :
	* Description :  get sum of total birthday and anniversary leaves of current month
	*/
	function getMonthBirthAnniLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		$this->db->select('SUM( l.leave_days) as monthBirthAnniLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		$this->db->where('l.leave_type in (8,9)');
		$this->db->where("leave_status = '2'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : Jignasa
	* Date : 20/09/2017
	* Modified By :
	* Description :  get list of upcomming leaves of current month
	*/
	function getupcomingmeetings(){
		$todayDate = date('Y-m-d H:i');
		$this->db->select('u.first_name, u.last_name, m.subject, m.members, m.schedule_date, m.schedule_time');
		$this->db->from('crm_meeting as m');
		$this->db->join('users as u','u.user_id = m.user_id', 'left');
		$this->db->where('u.status = "Active"');
		$this->db->where("TIMESTAMP(`schedule_date`,`schedule_time`) BETWEEN '$todayDate' AND ('$todayDate' + INTERVAL 7 DAY)");
		$this->db->order_by("schedule_date","asc");
		$this->db->order_by("schedule_time","asc");
		$query=$this->db->get()->result_array();
		return $query;
	}

	/*
	* Developed By : Dipika
	* Date : 12/02/2018
	* Modified By :
	* Description :  get early leaves of current month
	*/
	function getEarlyPendingLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		/*$this->db->select('SUM( l.leave_days) as monthEarlyLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where('l.leave_type in (6)');
		$this->db->where("leave_status = '2'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");
		$query=$this->db->get();
		return $query->row();*/

		$this->db->select('SUM(days) as monthEarlyLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('l.leave_type in (6)');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : Md Mashroor
	* Date : 13/05/2019
	* Modified By :
	* Description :  get the list of new employees joining this month
	*/
	function getNewJoiningUsers(){
		$month = date('m', strtotime('0 month'));
		$year = date('Y');
		$day = date('d');
		$this->db->select('u.first_name, u.last_name, d.department_name, u.joining_date');
		$this->db->from('crm_users as u');
		$this->db->join('crm_department as d','u.department_id = d.department_id');
		$this->db->where('YEAR(u.joining_date) ='.$year);
		$this->db->where('MONTH(u.joining_date)='.$month);
		$this->db->where('DAY(u.joining_date)>='.$day);
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("u.joining_date","ASC");
		$query=$this->db->get()->result_array();
		return $query;
	}
	// Name : Md Mashroor
	// developed : 15/05/2019
	// Description : For retriveing the list of days for late entries by user id
	function getUserByLateEntry($id){
		$month = date('m', strtotime('0 month'));
		$year = date('Y');
		$this->db->select('*');
		$this->db->from('crm_employ_log');
		$this->db->where('YEAR(log_date) ='.$year);
		$this->db->where('MONTH(log_date)='.$month);
		$this->db->where("late_entry = '1'");
		$this->db->where("punch_id = $id");
		$this->db->where("WEEKDAY(log_date) != 5 AND WEEKDAY(log_date) != 6");
		$this->db->order_by("log_date","DESC");
		$query=$this->db->get()->result_array();
		return $query;
	}
}

?>
