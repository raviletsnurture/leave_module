<?php
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
$department_id = $loginSession[0]->department_id;
$user_id= $loginSession[0]->user_id;

//**Add Incedence
$user = getJuniorUsersList($user_id, 'feedback_users');
for($i=0;$i<sizeof($permission);$i++){
	$nm = $permission[$i]->module_name;
	$arr[$nm] = $permission[$i];
}
//check user access this page
if($arr['feedback']->module_add == 'no'){
	header('Location: '.base_url());
}
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Add Incidence
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
					<form class="commonForm  cmxform form-horizontal tasi-form" id="addFeedback" name="addFeedback"  method="post" action="">
						<div class="form-group">
								<div class="col-lg-6">
								<div class="form-group">
									<label for="firstName" class="control-label col-lg-4">Team Member <span class="red">*</span></label>
									<div class="col-lg-8">
										<select name="user_id" id="user_id" class="form-control selectpicker js-example-basic-single" required>
											<option value="">Select Member</option>
											<?php foreach($user as $dd) { ?>
												<?php  if($dd->user_id != $loginSession[0]->user_id){?>
												<option <?php if(isset($_POST['selectUser']) && $dd->user_id == $_POST['selectUser']){echo 'selected=selected'; } ?> value="<?php echo $dd->user_id; ?>">
													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
												</option>
												<?php } ?>
											<?php } ?>
										</select>
									</div>
								</div>
								</div>
								<div class="col-lg-6">
								<div class="form-group">
									<label for="date" class="control-label col-lg-4">Incidence Month<span class="red">*</span></label>
									<div class="col-lg-8">
										<div class="input-group date form_datetime-component">
            						<input type="text" name="created_time" id="created_time" class="form-control" size="36" required onkeydown="return false" readonly='true' value="<?php if(isset($_POST['incidence_time'])){ echo $_POST['incidence_time']; } ?>">
										</div>
									</div>
								</div>
								</div>
								</div>
								<input class="form-control" id="publish_status_hidden" name="publish_status_hidden" type="hidden"  value="1"/>
						    <input class="form-control" name="loginUserId" type="hidden"  value="<?php echo $loginSession[0]->user_id; ?>"/>
								<div class="col-md-12 rem-pad-left" id="add-row">
									<div class="form-group">
										<div class="col-lg-2">
											<label  class="control-label">Incidence<span class="red">*</span></label>
										</div>
										<div class="col-lg-8">
											<input class="form-control threeRow" id="incidence" maxlength="200" name="incidence[]" type="text" placeholder="Incidence" required />
										</div>
										<div class="col-lg-2">
											<button class="add_field_button btn green margintopbottom"><i class="fa fa-plus"></i> Add Incidence</button>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-default" onclick="location.href='<?php echo base_url()?>developerfeedback/viewFeedback';" type="button">Cancel</button>
										<button class="btn btn-danger" type="submit" id="addFeedbackBtn">Add Incidence</button>
										<div class="loader">
											<img src="<?php echo base_url()?>assets/img/input-spinner.gif" alt="loading...">
										</div>
									</div>
								</div>
								</div>
								<div class="form-group">
		   						   <div class="col-lg-offset-2 col-lg-10">
		   							   <span class="frmmessage"></span>
		   						   </div>
		   						</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/star-rating.min.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/star-rating.min.js"></script>
<script language="javascript" type="text/javascript">

$('#created_time').datepicker({
    //format: 'yyyy-mm-dd',
		endDate: '0m',
    startDate: '-2m',
    autoclose: true,
		format: "yyyy-mm-dd",
    startView: "months",
    minViewMode: "months",
});
$(document).ready(function() {
    var x = 1; //initlal text box count
    $("body").on("click", ".add_field_button",function(e){ //on add input button click
        e.preventDefault();
        if(x < 10){ //max input box allowed
            x++; //text box increment
            $("body #add-row").append('<div class="form-group"><label  class="col-lg-2 control-label">Incidence</label><div class="col-lg-8"><input class="form-control threeRow" id="incidence" name="incidence[]" type="text" placeholder="Incidence"/></div><a href="#" class="remove_field btn red"><i class="fa fa-times"></i></a></div>');
			$('.projectRating').rating({
			   step: 1,
			   size: 'xs',
			   starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
		   	});
		}
	});
$("body").on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault();
		$(this).parent('div').remove();
		x--;
})
});
</script>
<script type="text/javascript">
$(document).ready(function(){

	$("#addFeedbackBtn").on("click",function(){
		$("#addFeedback").validate({
		   ignore: [],
		   rules: {
				 'incidence[]': {
				   required: true
        }
		   },
		   messages: {
		   },
		  errorElement: "span",
		  errorPlacement: function(error, element) {
			  offset = element.offset();
			  error.addClass('errormessage');  // add a class to the wrapper

			  if(element.hasClass('feedbackRating'))
			  {
				error.insertAfter(element);
				error.css({'bottom': '-15px', 'font-size': '13px', 'left': '15px', 'position': 'absolute'});  // add a class to the wrapper
				error.text('This field is required.');
			  }else if($(element).attr('id') == 'user_id'){
				error.insertAfter(".select2");
				error.css('position', 'relative');
			  }else{
				error.insertAfter(element);
				error.css('position', 'relative');
			  }
		  },
		   submitHandler: function(form) {
				var formData = $("#addFeedback").serialize();
				$.ajax({
				   type: 'POST',
				   url: '<?php echo base_url();?>feedback/addIncidence',
				   data: formData,
				   dataType:"json",
				   beforeSend: function(){
					   $('.loader').show();
					   //$('#addFeedbackBtn').hide();
				   },
				   success:function(data){
					  $('.loader').hide();
					  $('#addFeedbackBtn').show();
					   if(data == 1){
							$('#addFeedback')[0].reset();
							$(".frmmessage").html("Incidence added successfully!");
							$(".frmmessage").css("color","#29B6F6");
							setTimeout(function(){
							  window.location.href= '<?php echo base_url()?>developerfeedback/viewFeedback';
						  },2000);
					  }else if(data == 2){
						  $(".frmmessage").html("Seems its already added for the employee & month.");
						  $(".frmmessage").css("color","#d32f2f");
					  }else {
						   $(".frmmessage").html("Member selection and comments are mandatory!");
						   $(".frmmessage").css("color","#d32f2f");
					  }
						return false;
				   }
			});
		}
	  });
	});
});//document ready
</script>
