<?php

$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
$department_id = $loginSession[0]->department_id;
$user_id= $loginSession[0]->user_id;

//**Add feedback
$user = getJuniorUsersList($user_id, 'feedback_users');
for($i=0;$i<sizeof($permission);$i++)
{

	$nm = $permission[$i]->module_name;
	$arr[$nm] = $permission[$i];
}

//check user access this page

if($arr['feedback']->module_add == 'no'){
	header('Location: '.base_url());
}

?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Add Feedback
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
				<?php
					//$form_attributes = array('name' => 'addFeedback', 'id' => 'addFeedback', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
					//echo form_open(base_url().'feedback/addFeedback',$form_attributes);
					?>
					<form class="commonForm  cmxform form-horizontal tasi-form" id="addFeedback" name="addFeedback"  method="post" action="">
								<div class="col-lg-6">

								<div class="form-group">
									<label for="firstName" class="control-label col-lg-4">Team Member <span class="red">*</span></label>
									<div class="col-lg-8">
										<select name="user_id" id="user_id" class="form-control selectpicker js-example-basic-single" required>
											<option value="">Select Member</option>
											<?php foreach($user as $dd) { ?>
												<?php  if($dd->user_id != $loginSession[0]->user_id){?>
												<option <?php if(isset($_POST['selectUser']) && $dd->user_id == $_POST['selectUser']){echo 'selected=selected'; } ?> value="<?php echo $dd->user_id; ?>" <?php //$value1 = (isset($login_session[0]->user_id)) ? $login_session[0]->user_id : 0; echo selectedVal($value1,$dd->user_id); ?> >
													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
												</option>
												<?php } ?>
											<?php } ?>
										</select>
									</div>
								</div>
								</div>
								<div class="col-lg-6">
								<div class="form-group">
									<label for="date" class="control-label col-lg-4">Feedback Month<span class="red">*</span></label>
									<div class="col-lg-8">
										<div class="input-group date form_datetime-component">
                    						<input type="text" name="created_time" id="created_time" class="form-control" size="36" required onkeydown="return false" readonly='true' value="<?php if(isset($_POST['created_time'])){ echo $_POST['created_time']; } ?>">
										</div>
									</div>
								</div>
								</div>


								<?php
								$is_required ="";
								if(!empty($user)){
								//if($role_id ==1 || $role_id ==15 || $role_id ==20 || $role_id ==13){
									$is_required ="required";
									 ?>
								<div id="masterFeedbackFields" style="clear:both;"></div>

								<?php } ?>
								<div class="clearfix"></div>
								<input class="form-control" id="publish_status_hidden" name="publish_status_hidden" type="hidden"  value="1"/>
								<div class="col-lg-12">
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-default" onclick="location.href='<?php echo base_url()?>developerfeedback/viewFeedback';" type="button">Cancel</button>
										<button class="btn btn-danger" type="submit" id="addFeedbackBtn">Submit Feedback</button>

									</div>
									<div class="col-lg-offset-5 col-lg-7">
										<div class="loader">
											<img src="<?php echo base_url()?>assets/img/input-spinner.gif" alt="loading...">
										</div>
									</div>
								</div>
								</div>
								<div class="form-group">
		   						   <div class="col-lg-offset-2 col-lg-10">
		   							   <span class="frmmessage"></span>
		   						   </div>
		   						</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/star-rating.min.css" rel="stylesheet"/>

<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/star-rating.min.js"></script>

<script language="javascript" type="text/javascript">

$('#created_time').datepicker({
    //format: 'yyyy-mm-dd',
    endDate: '-1m',
		startDate: '-2m',
    autoclose: true,
		format: "yyyy-mm-dd",
    startView: "months",
    minViewMode: "months",
});
$(document).ready(function() {
    var x = 1; //initlal text box count
    $("body").on("click", ".add_field_button",function(e){
			//on add input button click
        e.preventDefault();

        if(x < 10){ //max input box allowed
            x++; //text box increment
						$("body #add-row").append('<div class="form-group"><label  class="col-lg-2 control-label">Project</label><div class="col-lg-2"><input class="form-control" id="project_name" name="project_name[]" type="text" placeholder="Project Name" /></div><div class="col-lg-2"><input class="form-control" id="project_comment" name="project_comment[]" type="text" placeholder="Extra Comment?" /></div><div class="col-lg-3"><input name="project_feedback[]" id="project_feedback" class="projectRating"></div><a href="#" class="remove_field btn red"><i class="fa fa-times"></i></a></div>');

						$('.projectRating').rating({
							step: 1,
							size: 'xs',
							starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
						});
		}
	});

	    $("body").on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault();
					$(this).parent('div').remove();
					x--;
	    })


});
</script>
<script type="text/javascript">
$(document).ready(function(){

	//$('#select2-user_id-result-wq5j-116').trigger('click');


	$("#addFeedbackBtn").on("click",function(){
		$("#addFeedback").validate({
		   ignore: [],
		   rules: {
		   },
		   messages: {
		   },
		  errorElement: "span",
		  errorPlacement: function(error, element) {
			  offset = element.offset();
			  error.addClass('errormessage');  // add a class to the wrapper
			  if(element.hasClass('feedbackRating'))
			  {
						error.insertAfter(element);
						error.css({'bottom': '-15px', 'font-size': '13px', 'left': '15px', 'position': 'absolute'});  // add a class to the wrapper
						error.text('This field is required.');
			  }
				else if($(element).attr('id') == 'user_id'){
						error.insertAfter(".select2");
						error.css('position', 'relative');
			  }
				else{
						error.insertAfter(element);
						error.css('position', 'relative');
			  }
			  //error.css('left', offset.left + element.outerWidth());
			  //error.css('top', offset.top);
		  },
		   submitHandler: function(form) {
				var formData = $("#addFeedback").serialize();
				// console.log(formData);
				$.ajax({
				   type: 'POST',
				   url: '<?php echo base_url();?>feedback/addFeedback',
				   data: formData,
				   dataType:"json",
				   beforeSend: function(){
					   $('.loader').show();
					   $('#addFeedbackBtn').hide();
				   },
				   success:function(data){
					  $('.loader').hide();
					  $('#addFeedbackBtn').show();
					   if(data == 1){

							$('#addFeedback')[0].reset();
							$(".frmmessage").html("Feedback added successfully!");
							$(".frmmessage").css("color","#29B6F6");
							setTimeout(function(){
							  window.location.href= '<?php echo base_url()?>developerfeedback/viewFeedback';
						  },2000);
						}
						else if(data == 2){
						  $(".frmmessage").html("Seems its already added for the employee & month.");
						  $(".frmmessage").css("color","#d32f2f");
						}
						else if(data == 4){
						  $(".frmmessage").html("Feedback Updated successfully!");
						  $(".frmmessage").css("color","#29B6F6");
						}
						else {
						   $(".frmmessage").html("Member selection and comments are mandatory!");
						   $(".frmmessage").css("color","#d32f2f");
					  }
						return false;
				   }
			});
		}
	  });


	});

	var userid = '<?php if(isset($_POST['selectUser'])){ echo $_POST['selectUser'];} ?>';
	//alert(userid);
		//$('#user_id').trigger('click');

		//$('#user_id').select('116', true);
		//$('#user_id').select2('data', {id: 100, a_key: 'Lorem Ipsum'});

		var formKRA = $("#user_id").serialize();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>feedback/getFeedbackFields',
			data: formKRA,
			beforeSend: function(){
				$('.loader').show();
				$('#addFeedbackBtn').hide();
			},
			success:function(data){
			 $('.loader').hide();
			 $('#addFeedbackBtn').show();
			 $('#masterFeedbackFields').html(data);
			 $('[data-toggle="tooltip"]').tooltip();
			 $('.feedbackRating,.projectRating').rating({
				step: 1,
				size: 'xs',
				starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
			});
			}
		});



	$("#user_id").on("change",function(){
		var formKRA = $("#user_id").serialize();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>feedback/getFeedbackFields',
			data: formKRA,
			beforeSend: function(){
				$('.loader').show();
				$('#addFeedbackBtn').hide();
			},
			success:function(data){
				$("html").niceScroll().resize();
				$("html").getNiceScroll().resize();
			 $('.loader').hide();
			 $('#addFeedbackBtn').show();
			 $('#masterFeedbackFields').html(data);
			 $('[data-toggle="tooltip"]').tooltip();
			 $('.feedbackRating,.projectRating').rating({
	 			step: 1,
	 			size: 'xs',
				starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
	 		});
			}
 		});
	});
});//document ready
</script>
