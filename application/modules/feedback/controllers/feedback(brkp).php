<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//TO DO : needs a rearchitecture of the whole feedback Logic
// Logic seems spread among different layers which should not
// RAVI @19/07/2016
// Action : Kalpesh

class Feedback extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('download');
		$this->load->helper('general_helper');
		$this->load->model('feedback_model');
		user_auth(0);
	}
	function index(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['titalTag'] = ' - Team Leader Feedback';

		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		$data["feedback"] = $this->feedback_model->getAllFeedbacks($id);
		$data['feedbackMonth'] = $this->feedback_model->getMyfeedMonth($id);
		$data['feedbackMonthRanking'] = $this->feedback_model->getMyfeedMonthRanking($id);
		$data['permission'] = $per;
		//var_dump($data); die;

		$userData = $this->logSession;
		$id = $userData[0]->user_id;
    auditUserActivity('Feedback', 'Index', $id, date('Y-m-d H:i:s'),$id);

	  $this->template->load_partial('dashboard_master','feedback/list_feedbacks',$data);
	}

	function logaudit(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$userClicked = $this->input->post('user_id');
		$userUpload = $this->input->post('upload_id');

		if($userClicked != $id) {
			auditUserActivity('Feedback', 'ALERT: download not allowed', $id, date('Y-m-d H:i:s'),'#');
			$data ="fake";
			}else{
			$data = $this->feedback_model->updateDownloadedAt($userClicked, $userUpload);
			$data ="OK";
		  }
    echo $data;
		//$data[""] = $this->feedback_model->getAllFeedbackUploads($id);
		//var_dump($data); die;

	}

	function add(){
		$data['titalTag'] = ' - Add Feedback';
		$loginSession = $this->session->userdata("login_session");
		// var_dump($loginSession);
		$role_id = $loginSession[0]->role_id;
		$department_id = $loginSession[0]->department_id;
		$per = getUserpermission($role_id);
		$data['login_session'] = $this->session->userdata("login_session");
		$data['permission'] = $per;
		$data['role'] = $role_id;
		$data['feedback_keys'] = $this->feedback_model->getFeedbackKeys();
		$this->template->load_partial('dashboard_master','feedback/add_feedback',$data);
	}

	function addFeedback(){
		$userData = $this->logSession;
		$login_dp_id = $userData[0]->department_id;
		$roleEditor = $userData[0]->role_id;
		// echo "<---Login userData--->";
		echo "<pre>";
		print_r("roleEditor->".$roleEditor);
		echo "</pre>";

		// user_id is the use to whom feedback is given
		if($_POST['user_id'] != '' && $_POST['created_time'] != ''){

			$time = $_POST['created_time'];
			$month = date("m", strtotime($time));
			$year = date("Y", strtotime($time));
			$user_id = $_POST['user_id'];

			//user's department for mapping role with BA+TL
			$userDeptt = getDepartmentFromUserId($user_id);
			// Return-> DepID, DepNAME, Seniors

			$deptSr = GetDepartmentSeniors($userDeptt[0]['department_id']);
			// Only id-28 exists in php department
			// Return->The Senior Complete Details

			$userDeptt = $userDeptt[0]['department_id'];
			// Storing Department id of the senior

			// print_r("<br>userDeptt(departID of the senior)->".$userDeptt);

			$reviewer_id = isset($_POST['reviewer_id'])?$_POST['reviewer_id']: null;
			// Reviewr Id is the ID of the User who is giving the review
			// (reviewer_id and user_id are same)
			// print_r("<br>Reviewer ID(giving review to)->".$reviewer_id);


			if($reviewer_id == null){
					$checkUserFeedback = checkUserFeedback($month, $year,$user_id, null); // Check user feedback is exists or not for specific month
			}
			else{
				$checkUserFeedback = checkUserFeedback($month, $year, $user_id, $reviewer_id); // Check user feedback is exists or not for specific month
			}

			if($roleEditor ==1 || $roleEditor == 13 || $roleEditor == 15 || $roleEditor == 23 || $roleEditor == 20 || $login_dp_id == 12 || $login_dp_id == 7 || $login_dp_id == 10){
						$data['reviewer_id'] =isset($_POST['reviewer_id'])?$_POST['reviewer_id']:0;
						$data['overall_score'] =isset($_POST['feedback_keys'])?serialize($_POST['feedback_keys']):"";
						$data['event_remark'] =isset($_POST['event_remark'])?$_POST['event_remark']:"";
						$data['comment'] = isset($_POST['feedbackcomment'])?$_POST['feedbackcomment']:"";

						if( isset($_POST['project_name'][0]) && ($_POST['project_feedback'][0]) !=0 &&  isset($_POST['project_comment'][0]) ){
										$data['projects_and_ratings'] =serialize(array($_POST['project_name'],$_POST['project_feedback'],$_POST['project_comment']));
						}
						else
						{
							$data['projects_and_ratings'] ="";
						}

			}

			$data['published_status'] =isset($_POST['publish_status_hidden'])?$_POST['publish_status_hidden']:"";
			$data['user_id'] =isset($_POST['user_id'])?$_POST['user_id']:0;
			$data['created_time'] =isset($_POST['created_time'])?$_POST['created_time']:"";

			echo "<pre>";
			print_r($data);
			echo "</pre>";

			$data1['published_status'] =isset($_POST['publish_status_hidden'])?$_POST['publish_status_hidden']:"";
			$data1['user_id'] =isset($_POST['user_id'])?$_POST['user_id']:0;
			$data1['created_time'] =isset($_POST['created_time'])?$_POST['created_time']:"";

			$users_email = $this->feedback_model->getUserInformation($user_id);

			//Mail format
			if(isset($data['reviewer_id']) && $data['reviewer_id'] > 0){

			$date = new DateTime($data['created_time']);
			$addData['month'] = $date->format('F Y');
			$addData['user'] = $userData[0]->first_name.' '.$userData[0]->last_name;

			$finalEmailHtml = $this->load->view("feedback/feedback_notification_mail",$addData, true);
			$toEmail = $users_email->email;
			$subject="Feedback - ".$addData['month'];
			}


			if( empty($checkUserFeedback) ) {
					//insert record if not exists
					// echo "<br>Insert the record<br>";
					//$data['feedback_id'] = "";
					// if its login of HR /BA/QA then get their respective feedback_fields
					switch ($roleEditor){
						case 13:
							if($login_dp_id==12 && $userDeptt != 12){
								$data['ba_remark'] = serialize($_POST['ba_remarks']);
								$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);

							}else if($login_dp_id==10 && $userDeptt != 10){
								$data['hr_remark'] = serialize($_POST['hr_remarks']);
								$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);

							}else if($login_dp_id==7 && $userDeptt != 7){
								$data['qa_remark'] = serialize($_POST['qa_remarks']);
								$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);

							}else{

								$dataSubmit = $this->feedback_model->addFeedbackOfTL($data);
								// send_mail($toEmail, $subject, $finalEmailHtml);
							}

							echo 1;

						break;
						case 15:
							$dataSubmit = $this->feedback_model->addFeedbackOfTL($data);
							// send_mail($toEmail, $subject, $finalEmailHtml);
							echo 1;

						break;
						case 20:
							$dataSubmit = $this->feedback_model->addFeedbackOfTL($data);
							// send_mail($toEmail, $subject, $finalEmailHtml);
							echo 1;

						break;
						case 23:
                             if($login_dp_id==12 && $userDeptt != 12){
							 $data['ba_remark'] = serialize($_POST['ba_remarks']);
							 $dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);
							//  send_mail($toEmail, $subject, $finalEmailHtml);
							 echo 1;
							}
						break;

						case 24:
                            if($login_dp_id==7 && $userDeptt != 7){
							$data['qa_remark'] = serialize($_POST['qa_remarks']);
							$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);
							// send_mail($toEmail, $subject, $finalEmailHtml);
							echo 1;
                            }
						break;

						case 22:
						    if($login_dp_id==10 && $userDeptt != 10){
							$data['hr_remark'] = serialize($_POST['hr_remarks']);
							$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);
							echo 1;
						    }
						break;

						case 1:

						$dataSubmit = $this->feedback_model->addFeedbackOfTL($data);
						// send_mail($toEmail, $subject, $finalEmailHtml);
						echo 1;
						break;
					}
			}
			else {

				echo "<br>Update the record as it already exists<br>";
				//Update record if already exists

				// get the feedback ID for update
				 $data['feedback_id'] = $checkUserFeedback[0]->feedback_id;
				 $data1['feedback_id'] = $checkUserFeedback[0]->feedback_id;
					echo "login_dp_id->".$login_dp_id."<br>";
					echo "userDeptt(Senior dept id)->".$userDeptt."<br>";
				// if its login of HR /BA/QA then get their respective feedback_fields
				switch ($roleEditor){
					case 13:
							// echo "13";
								if($login_dp_id==12 && $userDeptt != 12){

									if(strlen($checkUserFeedback[0]->ba_remark) <1){
										$data1['ba_remark'] = serialize($_POST['ba_remarks']);
										$dataSubmit = $this->feedback_model->addFeedbackOfBA($data1, $roleEditor);
										echo 1;
									} else {
										echo 2;
								}

							}else if($login_dp_id==10 && $userDeptt != 10){

								if(strlen($checkUserFeedback[0]->hr_remark) <1){
									$data1['hr_remark'] = serialize($_POST['hr_remarks']);
									$dataSubmit = $this->feedback_model->addFeedbackOfBA($data1, $roleEditor);
									echo 1;
								} else {
									echo 2;
								}

							}else if($login_dp_id==7 && $userDeptt != 7){

								if(strlen($checkUserFeedback[0]->qa_remark) <1){
									$data1['qa_remark'] = serialize($_POST['qa_remarks']);
									$dataSubmit = $this->feedback_model->addFeedbackOfBA($data1, $roleEditor);
									echo 1;
								} else {
									echo 2;
								}

							}else{

									if($checkUserFeedback[0]->reviewer_id == 0){
										$dataSubmit = $this->feedback_model->updateFeedbackOfTL($data);
										// send_mail($toEmail, $subject, $finalEmailHtml);
										echo 1;
									}else {
										echo 2;
									}

								}

					break;
					case 15:

								if($checkUserFeedback[0]->reviewer_id == 0){
									$dataSubmit = $this->feedback_model->updateFeedbackOfTL($data);
									// send_mail($toEmail, $subject, $finalEmailHtml);
									echo 1;
								}else {
									echo 2;
								}

					break;
					case 20:

								if($checkUserFeedback[0]->reviewer_id == 0){
									$dataSubmit = $this->feedback_model->updateFeedbackOfTL($data);
									// send_mail($toEmail, $subject, $finalEmailHtml);
									echo 1;
								}
								else {
									echo 2;
								}

					break;
					case 23:

								if($login_dp_id==12 && $userDeptt != 12){
									if(strlen($checkUserFeedback[0]->ba_remark) <1){
										$data['ba_remark'] = serialize($_POST['ba_remarks']);
										$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);
										echo 1;
									} else {
										echo 2;
									}
								}else{
									echo 1;
								}
					break;

					case 24:

							if($login_dp_id==7 && $userDeptt != 7){
								if(strlen($checkUserFeedback[0]->qa_remark) <1){
									$data['qa_remark'] = serialize($_POST['qa_remarks']);
									$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);
									echo 1;
								} else {
									echo 2;
								}
								}else{
											echo 1;
								}
					break;

					case 22:
							if($login_dp_id==10 && $userDeptt != 10){
								if(strlen($checkUserFeedback[0]->hr_remark) <1){
									$data['hr_remark'] = serialize($_POST['hr_remarks']);
									$dataSubmit = $this->feedback_model->addFeedbackOfBA($data, $roleEditor);
									echo 1;
									}
									else {
										echo 2;
									}
								}
						else{
				    	echo 1;
				    }
					break;

					case 1:

						if($checkUserFeedback[0]->reviewer_id == 0){
								$dataSubmit = $this->feedback_model->updateFeedbackOfTL($data);
								// send_mail($toEmail, $subject, $finalEmailHtml);
								echo 1;
							}
							else
							{
						echo 2;
					}
					break;
				}

			}
		}
		else {
			echo 0;
		}
	}



	function getFeedbackFields(){

		$userData = $this->logSession;
		$login_dp_id = $userData[0]->department_id;
		$id = $userData[0]->user_id;
		if($_POST['user_id'] != ''){
			$html = '';
			$user_id = $_POST['user_id'];
			// user id of the user to whom review is given

			$role_id = $userData[0]->role_id;
			// $role_id = $this->feedback_model->getRoleId($user_id);

			$feedback_keys = $this->feedback_model->getFeedbackKeys();

			/*
			Department id
			HR = 10
			BA = 12
			QA = 7
			*/
				 if($login_dp_id == 12){
						echo '<div class="col-md-12 rem-pad-left"><div class="form-group">
							<label class="col-lg-2 control-label"> BA remarks<span class="red">*</span></label>
							<div class="col-lg-8">
								<textarea class="form-control" id="ba_remarks" maxlength="500" name="ba_remarks[]" style="resize:none;"  rows="5" cols="40" placeholder="BA team remarks" required></textarea>
							</div>
							<input class="form-control" name="ba_remarks[]" type="hidden"  value="'.$id .'"/>
						</div></div>';
					}
					else if($login_dp_id == 10) {
						echo '<div class="col-md-12 rem-pad-left"><div class="form-group">
							<label class="col-lg-2 control-label"> HR remarks<span class="red">*</span></label>
							<div class="col-lg-8">
								<textarea class="form-control" id="hr_remarks" maxlength="500" name="hr_remarks[]" style="resize:none;" rows="5" cols="40" placeholder="HR team remarks" required></textarea>
							</div>
							<input class="form-control" name="hr_remarks[]" type="hidden"  value="'.$id .'"/>
						</div></div>';
					}
					else if($login_dp_id == 7) {
						echo '<div class="col-md-12 rem-pad-left"><div class="form-group">
							<label class="col-lg-2 control-label"> QA remarks<span class="red">*</span></label>
							<div class="col-lg-8">
								<textarea class="form-control" maxlength="500" id="qa_remarks" name="qa_remarks[]" style="resize:none;" rows="5" cols="40" placeholder="QA team remarks" required></textarea>
							</div>
							<input class="form-control" name="qa_remarks[]" type="hidden"  value="'.$id .'"/>
						</div></div>';
					}
					else
					{
							for($fk=0; $fk< count($feedback_keys); $fk++)
							{

									$field_name = strtolower(str_replace(' ', '_', $feedback_keys[$fk]->feedback_key));
									$info = "";

									if($feedback_keys[$fk]->helptext != ""){
										$info = '<i class="fa fa-info" data-toggle="tooltip" data-placement="top" title="'.ucfirst($feedback_keys[$fk]->helptext).'"></i>';
									}

									$selected_user_sub_role = $userData[0]->sub_role_id;
									$feedback_sub_role = $feedback_keys[$fk]->sub_role_id;
									// print_r("(Role ID)".$userData[0]->role_id."==".$feedback_keys[$fk]->role_id."  ANd  (DepartmentID)".$userData[0]->department_id."  ==  ".$feedback_keys[$fk]->dept_id ."  ANd  sel_sub_role== ".$selected_user_sub_role. "  OR (feedbackkey)" . $feedback_keys[$fk]->role_id ."==0");

									if((preg_match('/\b' . $userData[0]->role_id . '\b/', $feedback_keys[$fk]->role_id) && preg_match('/\b' . $userData[0]->department_id . '\b/', $feedback_keys[$fk]->dept_id) && ($selected_user_sub_role=='')) || $feedback_keys[$fk]->role_id == '0' )
									{
									$html .= '<div class="col-lg-6">';
									$html .='<div class="form-group">';
									$html .= '<label  class="col-lg-4 control-label">'.ucfirst($feedback_keys[$fk]->feedback_key).'<span class="red">* </span>'.$info.'</label>';
									$html .= '<div class="col-lg-8">';
									$html .= '<input name="feedback_keys['.$field_name.']" id="'.$field_name.'" class="feedbackRating" required min="1" max="5">';
									//$html .= '<input name="feedback_keys['.$field_name.']" id="'.$field_name.'" class="feedbackRating" >';  <-dont uncomment
									$html .= '</div></div></div>';
									}

									if(($selected_user_sub_role!='' && $userData[0]->role_id==20) && (preg_match('/\b' . $selected_user_sub_role . '\b/', $feedback_sub_role)) || ($feedback_sub_role=='0' && $selected_user_sub_role!='' && $userData[0]->role_id==20)){

										$html .= '<div class="col-lg-6">';
										$html .='<div class="form-group">';
										$html .= '<label  class="col-lg-4 control-label">'.ucfirst($feedback_keys[$fk]->feedback_key).'<span class="red">* </span>'.$info.'</label>';
										$html .= '<div class="col-lg-8">';
										$html .= '<input name="feedback_keys['.$field_name.']" id="'.$field_name.'" class="feedbackRating" required min="1" max="5">';
										//$html .= '<input name="feedback_keys['.$field_name.']" id="'.$field_name.'" class="feedbackRating" >';
										$html .= '</div></div></div>';
									}

							}
				echo $html; ?>

				<div id="normalFeedbackFields">
					<div class="clearfix"></div>
							<div class="col-lg-12">
									<div class="form-group">
											<label class="col-lg-2 control-label"> Event Remarks<span class="red">*</span></label>
											<div class="col-lg-8">
												<textarea class="form-control" id="event_remark" maxlength="500" name="event_remark" style="resize:none;" rows="5" cols="40" placeholder="Specify if any event attended or not" required></textarea>
											</div>
									</div>
							</div>
							<div class="col-lg-12">
									<div class="form-group">
										<label class="col-lg-2 control-label"> Comments/Remarks/Action Items (+ves/-ive)<span class="red">*</span></label>
										<div class="col-lg-8">
												<textarea class="form-control" id="feedbackcomment" maxlength="500" style="resize:none;" name="feedbackcomment"   rows="5" cols="40" placeholder="Other concerns and comments, remarks" required></textarea>
										</div>
										<input class="form-control" id="reviewer_id" name="reviewer_id" type="hidden"  value="<?php echo $id;?>"/>
									</div>
							</div>
					<div class="col-md-12 rem-pad-left" id="add-row">
								<div class="form-group">
										<div class="col-lg-2">
											<label  class="control-label">Project</label>
										</div>
										<div class="col-lg-2">
											<input class="form-control" id="project_name" maxlength="50" name="project_name[]" type="text" placeholder="Project Name" />
										</div>
										<div class="col-lg-2">
											<input class="form-control" id="project_comment" maxlength="50" name="project_comment[]" type="text" placeholder="Extra Comment?" />
										</div>
										<div class="col-lg-3">
											<input name="project_feedback[]" id="project_feedback" class="projectRating">
										</div>
										<div class="col-lg-2">
											<button class="add_field_button btn green margintopbottom"><i class="fa fa-plus"></i> Add Project</button>
										</div>
								</div>
					</div>
				</div>

		<?php
	}
	}
	}


	function incidence(){
		$data['titalTag'] = ' - Add Incidence';
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$department_id = $loginSession[0]->department_id;
		$per = getUserpermission($role_id);
		$data['login_session'] = $this->session->userdata("login_session");
		$data['permission'] = $per;
		$data['role'] = $role_id;
		$data['feedback_keys'] = $this->feedback_model->getFeedbackKeys();
		$this->template->load_partial('dashboard_master','feedback/add_incidence',$data);
	}

	function addIncidence(){
		$f= array();
		$time = $_POST['created_time'];
		$month = date("m", strtotime($time));
		$year = date("Y", strtotime($time));
		$user_id = $_POST['user_id'];
		$loginUserId = $_POST['loginUserId'];
		$incidenceArray = array_values(array_filter($_POST['incidence']));

		$checkUserFeedback = checkUserFeedback($month, $year,$user_id, null); // Check user feedback is exists or not for specific month
		if(empty($checkUserFeedback)) {
			$newArray = array('loginUserId' => $loginUserId, 'incidence' => $incidenceArray);
			array_push($f, $newArray);
			$data['incidence'] =serialize($f);
			$data['created_time'] =$time;
			$data['user_id'] =$user_id;
			$data['published_status'] =1;
			$dataSubmit = $this->feedback_model->addIncidenceData($data);
			echo $dataSubmit;
		}else{
			if($checkUserFeedback[0]->incidence != NULL){
				$oldDaat = unserialize($checkUserFeedback[0]->incidence);
			}else{
				$oldDaat =array();
			}
			$newArray = array('loginUserId' => $loginUserId, 'incidence' =>$incidenceArray);
			array_push($oldDaat, $newArray);

			$data['incidence'] =serialize($oldDaat);
			$data['created_time'] =$time;
			$data['user_id'] =$user_id;
			$dataSubmit = $this->feedback_model->updateIncidenceData($data,$checkUserFeedback[0]->feedback_id);
			echo $dataSubmit;
		}
	}
}
?>
