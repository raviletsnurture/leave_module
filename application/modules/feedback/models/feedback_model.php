<?php
class Feedback_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllFeedbacks($user_id)
	{
		$start_date = date('Y-m-d');
		//$condition_data = array('l.user_id'=>$user_id,'year(l.created_time) IN '=> $yearData);
		$this->db->select('u.first_name, u.last_name,r.first_name as r_first_name, r.last_name as r_last_name,r.user_pic as reviewer_pic, l.*');
		$this->db->from('feedback as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('users as r','r.user_id = l.reviewer_id', 'left');
		$this->db->where('l.user_id',$user_id);
		$this->db->where('l.published_status',1);
		$this->db->where('l.created_time BETWEEN "'. date('Y-m-d H:i:s', strtotime('-12 months', strtotime("now"))). '" and "'. date('Y-m-d H:i:s', strtotime("now")).'"');
		$this->db->order_by("l.created_time","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function getUserInformation($user_id){
		$this->db->select('first_name,last_name,email');
		$this->db->from('crm_users');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->row();
	}


	function addFeedbackOfTL($data)
	{
		if($this->db->insert('feedback',$data)){
			return true;
		}else{
			return false;
		}
	}

	function updateFeedbackOfTL($data)
	{
		$this->db->where('feedback_id',$data['feedback_id']);
		if($this->db->update('feedback',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getFeedbackKeys()
	{
		$this->db->select('*');
		$this->db->from('feedback_fields_master as l');
		$query=$this->db->get();
		return $query->result();
	}

	function getMyfeedMonth($id)
	{
		$this->db->select("DATE_FORMAT(created_time, '%M-%y') as month, DATE_FORMAT( created_time, '%Y') as year",false);
		$this->db->from("crm_feedback");
		$this->db->where('user_id',$id);
		$this->db->group_by("month(created_time)");
		$this->db->order_by("created_time","year");
		$query=$this->db->get();
		return $query->result();
	}

	function getMyfeedMonthRanking($id)
	{
		$this->db->select("overall_score",false);
		$this->db->from("crm_feedback");
		$this->db->where('user_id',$id);
		$this->db->group_by("month(created_time)");
		$this->db->order_by("created_time","year");
		$query=$this->db->get();
		return $query->result();
	}

	function getRoleId($id)
	{
		$this->db->select("role_id, department_id, sub_role_id",false);
		$this->db->from("crm_users");
		$this->db->where('user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}

	function addFeedbackOfBA($data,$role_id){
		$allowedRoles = array(22,23,24,13);
		if(!in_array($role_id,$allowedRoles)){ return false; }
		//make sure you are passing only the DELTA of data which is to be updated
		// i.e. additional feedback from BA / HR

		if(isset($data['feedback_id']) && $data['feedback_id'] != ""){
			$this->db->where('feedback_id',$data['feedback_id']);
			if($this->db->update('feedback',$data)){
				return true;
			}else{
				return false;
			}
		}else{
			if($this->db->insert('feedback',$data)){
				return true;
			}else{
				return false;
			}
		}
	}


	function addIncidenceData($data){
		if($this->db->insert('feedback',$data)){
			return true;
		}else{
			return false;
		}
	}

	function updateIncidenceData($data,$feedback_id){
		$this->db->where('feedback_id',$feedback_id);
		if($this->db->update('feedback',$data)){
			return true;
		}else{
			return false;
		}
	}
}
?>
