<?php
class reimbursement_model extends CI_Model
{ 
    function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllRequest($id){
		$this->db->select('r.*,u.first_name,u.last_name');
		$this->db->from('crm_reimbursement_request as r');
		$this->db->join('crm_users as u','u.user_id = r.user_id');
		$this->db->where('r.user_id = ', $id);
		$this->db->order_by("r.created_at","desc");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function addReimbursementRequest($data)
	{
		if($this->db->insert('reimbursement_request', $data)){
			return true;
		}else{
			return false;
		}	
	}

	function getrequestById($id){
		$this->db->select('*');
		$this->db->from('crm_reimbursement_request');
		$this->db->where('reimbursement_id = ', $id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	function deleteRequest($id){
		$this->db->where('reimbursement_id',$id);
	    if($this->db->delete('crm_reimbursement_request')){
			return true;
		}else{
			return false;
		}
	}

	// 20->Management, 22->HR Executive, 13->team leader, 38->technical lead
	function getUserByRoleId(){
		$role_array = array('22');
		// $dept_array=array('');
		$this->db->select("*");
		$this->db->from('crm_users');
		$this->db->where_in('role_id',$role_array);
		// $this->db->where_in('department_id = ',$dept_id);
		$this->db->where('status = "Active"');
		$query = $this->db->get()->result_array();
		return $query;
	}
	function getReimbursementListById($user_id){
		$this->db->select("r.*,u.first_name, u.last_name");
		$this->db->from('crm_reimbursement_request as r');
		$this->db->join('crm_users as u','u.user_id = r.user_id');
		$this->db->where_in('r.user_id', $user_id);
		$this->db->order_by("r.created_at","desc");
		$query = $this->db->get()->result_array();
		return $query;

	}
	function updateStatus($data, $fullname){
		$this->db->set('seniorApproval',$data['status']);
		$this->db->set('approvedBy',$fullname);
		$this->db->where('reimbursement_id',$data['request_id']);
		$this->db->where('user_id',$data['user_id']);
		if($this->db->update('crm_reimbursement_request'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>