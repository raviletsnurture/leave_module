<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reimbursement extends MX_Controller{

    function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->database();
        $this->logSession = $this->session->userdata("login_session");
        $this->load->helper('url');
		$this->load->helper('download');
        $this->load->helper('general_helper');
        $this->load->helper('general2_helper');
		$this->load->model('reimbursement_model');
        user_auth(0);
        $this->load->library('email');
        date_default_timezone_set("Asia/Kolkata");
    }
    function index(){
        $data['titalTag'] = ' - Reimbursement';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        $data['userRole'] = $userData[0]->role_id;        
        $data['userId'] = $userData[0]->user_id;
        $data["request_list"] = $this->reimbursement_model->getAllRequest($id);
        $data["getUser"] = getUserByid($id);
        $user_id_array = explode("," ,$data["getUser"][0]['reimbursement_users']);
        $data["reimbursementListById"] = $this->reimbursement_model->getReimbursementListById($user_id_array);
        $this->template->load_partial('dashboard_master','reimbursement/all_request',$data);
    }


    function add(){
        $data['titalTag'] = ' - Add Request';
        $this->template->load_partial('dashboard_master','reimbursement/add',$data);  
    }
    function addReimbursement(){ 
         $userData = $this->logSession;          
         $getSeniorEmails = getSeniorEmails($userData[0]->user_id, "reimbursement_users");
        if ($this->input->post('datetime') != '') {            
            $userData = $this->logSession;
            $data['dept_id'] = $userData[0]->department_id;
            $data['role_id'] = $userData[0]->role_id;
            $data['user_id'] = $userData[0]->user_id;
            $data['datetime'] = trim($this->input->post('datetime'));
            $data['amount'] = trim($this->input->post('amount'));
            $data['description'] = trim($this->input->post('description'));
            $data['payment_mode'] = trim($this->input->post('payment_mode'));
            
            if($_FILES['reciept']['name'] && $_FILES['reciept']['name'] != ''){
                $time=strtotime($data['datetime']);
                $pdfPath = 'uploads/cash_reimbursement';
                makeDir($pdfPath);
                $mainImage = 'reciept';
                $encodedName = rtrim(strtr(base64_encode($data['user_id'].time()), '+/', '-_'), '=');
                $ext = array("pdf", "PDF", "DOCX", "docx", "jpeg", "JPEG","PNG", "png","JPG", "jpg");
                $mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);               
                $data['reciept'] = $mainImageName;
                $filepathOnline = base_url().$pdfPath.'/'.$mainImageName;  
            }   
            
            $dataSubmit = $this->reimbursement_model->addReimbursementRequest($data);
            if($dataSubmit){
                
                
                $mailData['name'] = 'All'; 
                $subject = 'Reimbursement Request From '.$userData[0]->first_name.' '.$userData[0]->last_name;
                $mailData['body']='<tr>
                <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">
                '.$userData[0]->first_name.' '.$userData[0]->last_name.' has requested for cash reimbursement.
                </p></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">Reimbursement Date : '.$data['datetime'].'</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">Reimbursement Amount : Rs.'.$data['amount'].'/-</td>
                </tr>
                
                ';
                if(isset($filepathOnline) && $filepathOnline!=''){
                    $mailData['body'].='<tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><a href='.$filepathOnline.'>Click here</a> to view the reciept.</p></td>
                    </tr>';
                }else{
                    $mailData['body'].='<tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"> No reciept uploaded </p></td>
                    </tr>';
                }

                $this->email->from('hrms@letsnurture.com', "HRMS");                
                $this->email->to('hrms@letsnurture.com');                   
                $this->email->bcc($getSeniorEmails);
                $this->email->subject($subject);
                $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                $this->email->message($body);
                // if($this->email->send()){
                //     echo '<br>mail sent successfully!';
                //   }
                // else {
                //     echo '<br>Mail sending failed!';
                // }
                // echo $this->email->print_debugger();
                
                $deviceTokens = getSeniorTokens($userData[0]->user_id, "reimbursement_users");                                
                $Tokenlist = array();
                foreach($deviceTokens as $value){
                    array_push($Tokenlist,$value->deviceToken);
                }
                $notification_message = ''.$userData[0]->first_name.' '.$userData[0]->last_name.' has requested for reimbursement';
                // SendNotificationFCMWeb('New Reimbursement Request' ,$notification_message,$Tokenlist);                                
                $this->session->set_flashdata('success', 'Cash Reimbursement added successfully!');
            }else {
            	$this->session->set_flashdata('error','On Snap ! there was a problem adding request!');
                redirect(base_url().'reimbursement/add');
                exit;
            }        
        }
        redirect(base_url() . 'reimbursement');
    }

    function deleteRequest(){
        $id = $this->input->post('id');
        $userData = $this->logSession;        
        $getrequestById = $this->reimbursement_model->getrequestById($id); 
        if($userData[0]->user_id === $getrequestById[0]['user_id'] ){
            $res = $this->reimbursement_model->deleteRequest($id);  
            if($res){
                if($getrequestById[0]['reciept'] != null){
                    $filepathOnline = FCPATH.'uploads/cash_reimbursement/'.$getrequestById[0]['reciept'];
                    unlink($filepathOnline);
                }                 
                echo 1;         
            }
        }
        else{
            echo 2;
        }
    }
    function seniorAction(){
        $userData = $this->logSession;
        $fullname = $userData[0]->user_id;
        $data = $this->reimbursement_model->updateStatus($_POST, $fullname);
        if($data == true){
            if($_POST['status']==0){
                echo 0;
            }
            elseif($_POST['status']==1){
                echo 1;
            }
            else{
                echo 2;
            }
        }
        else{
            echo 3;
        }
    }
}

?>