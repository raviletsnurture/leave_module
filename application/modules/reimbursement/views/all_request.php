<?php
// $user = getAllUsers();
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
?>

<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">
      <header class="panel-heading">Cash Reimbursement Request</header>
      <div class="panel-body">
        <div class="btn-group">
              <a href="<?php echo base_url()?>reimbursement/add">
                <button class="btn btn-info" id="editable-sample_new">Add Request <i class="fa fa-plus"></i> </button>
              </a>
        </div>

        <?php if($this->session->flashdata('error')){?>
              <div class="alert alert-block alert-danger fade in margin-top">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
              </div>
          <?php } ?>

          <?php if($this->session->flashdata('success')){?>
              <div class="alert alert-success fade in margin-top">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
              </div>
          <?php }?>
      </div>
    </section>
            <?php if(!empty($getUser[0]['reimbursement_users']) && $getUser[0]['reimbursement_users'] != NULL){?>

            <!-- Datatable for User reimbursement request -->
              <!-- Panel Start -->
              <section class="panel">
                <header class="panel-heading">Employee reimbursement requests</header>
                <div class="panel-body">
                    <table class="table table-striped table-hover table-bordered" id="employeeRequest">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Amount</th>
                        <th width="15%">Reimbursement date</th>
                        <th width="20%">Description</th>
                        <th>Payment mode</th>
                        <th>Your Approval</th>
                        <th>Approval by/<br>Rejected by</th>
                        <th>Hr Approval</th>
                        <th>Created at</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php foreach($reimbursementListById as $rld){?>
                    <?php 
          if($rld['seniorApproval'] != '1'){
            $seniorApprovalDetail = getUserDetail($rld['approvedBy']);
            $seniorApprovalName = $seniorApprovalDetail->first_name.' '.$seniorApprovalDetail->last_name;
        }else{
          $seniorApprovalName = "-";
        }
         ?>
                    <tr>
                          <td><?php echo $rld['first_name'].' '.$rld['last_name']?></td>
                          <td><?php echo $rld['amount']?></td>
                          <td><?php echo date("d-M-Y h:i a", strtotime($rld['datetime']))?></td>
                          <td><?php echo $rld['description']?></td>
                          <td><?php echo $rld['payment_mode']?></td>
                          <td>
                          <select <?php if($rld['seniorApproval']==2) {echo "disabled";}?> name="status" class="status" onchange="ff(this);">
                              <option <?php if($rld['seniorApproval']==0){echo "selected='selected'";}?> value="0, <?php echo $rld['reimbursement_id']; ?>, <?php echo $rld['user_id']; ?>">Reject</option>
                              <option <?php if($rld['seniorApproval']==1){echo "selected='selected'";}?> value="1, <?php echo $rld['reimbursement_id']; ?>, <?php echo $rld['user_id']; ?>">Pending</option>
                              <option <?php if($rld['seniorApproval']==2){echo "selected='selected'";}?> value="2, <?php echo $rld['reimbursement_id']; ?>, <?php echo $rld['user_id']; ?>">Approve</option>
                          </select>
                          </td>
                          <td><?php echo $seniorApprovalName;?></td>
                          <td><?php
                          if($rld['status']=='0'){
                            echo "<span style='color:red'>Rejected</span>";
                          }
                          elseif($rld['status']=='1'){
                            echo "<span>Pending</span>";
                          }
                          else{
                            echo "<span style='color:green'>Approved</span>";
                          }
                          ?></td>
                          <td><?php echo date("d-M-Y", strtotime($rld['created_at']))?></td>
                          <td>
                          <?php if($rld['reciept'] != null){?>
                            <a href="<?php echo base_url()."uploads/cash_reimbursement/".$rld['reciept']?>" target="_blank"><button class="btn btn-info btn-xs tooltips" data-toggle="tooltip" data-original-title="view&nbsp;reciept"><i class="fa fa-eye "></i></button></a>                           

                        <?php  }?>
                          </td>
                    </tr>

                 <?php }?>
                  </tbody>
                    </table>  
                </div>      
              </section>        
              <!-- Panel end -->

           <?php }?>

<!-- Datatable for own request -->
    <!-- Panel start-->
     <section class="panel">
     <header class="panel-heading">Your Requests</header>
     <div class="panel-body">
    
       <?php if(sizeof($request_list)>=1){?>
         <table class="table table-striped table-hover table-bordered" id="yourRequest">
         <thead>
            <tr>
              <th>Amount</th>
              <th width="15%">Reimbursement date</th>
              <th width="20%">Description</th>
              <th>Payment mode</th>
              <?php if($userRole==13 || $userRole==38){
                echo "<th>Approval by PM</th>";
              }if($userRole!=13 && $userRole!=38 && $userRole!=20 && $userRole!=15){
                echo "<th>Approval by TL</th>";
              }?>     
              <th>Approval by HR</th>
              <th>Created at</th>
              <th>Action</th>
            </tr>
         </thead>
         <tbody>
         <?php
         foreach( $request_list as $request){?>
         <?php 
          if($request['seniorApproval'] != '1'){
            $seniorApprovalDetail = getUserDetail($request['approvedBy']);
            $seniorApprovalName = $seniorApprovalDetail->first_name.' '.$seniorApprovalDetail->last_name;
        }
         ?>
             <tr>
                <td><?php echo $request['amount']?></td>
                <td><?php echo date("d-M-Y h:i a", strtotime($request['datetime']))?></td>
                <td><?php echo $request['description']?></td>
                <td><?php echo $request['payment_mode']?></td>
                <?php if($userRole!=20 && $userRole!=15){?>
                  <td><?php
                          if($request['seniorApproval']=='0'){    
                            echo "<span style='color:red'>Rejected</span>".' by '.$seniorApprovalName;
                        }
                        elseif($request['seniorApproval']=='1'){
                            echo "<span>Pending</span>";
                        }
                        else{
                            echo "<span style='color:green'>Approved</span>".' by '.$seniorApprovalName;
                        }
                      ?>
                  </td>
              <?php } ?>
                <td><?php
                    if($request['status']=='0'){
                      echo "<span style='color:red'>Rejected</span>";
                    }
                    elseif($request['status']=='1'){
                      echo "<span>Pending</span>";
                    }
                    else{
                      echo "<span style='color:green'>Approved</span>";
                    }?>
                </td>
                <td><?php echo date("d-M-Y", strtotime($request['created_at']))?></td>
                <td>

                <?php if($request['reciept'] != null){?>
                <a href="<?php echo base_url()."uploads/cash_reimbursement/".$request['reciept']?>" target="_blank"><button class="btn btn-info btn-xs tooltips" data-toggle="tooltip" data-original-title="view&nbsp;reciept"><i class="fa fa-eye "></i></button></a>
                <?php  }?>
                <?php if($request['status']=='1' && $request['seniorApproval']=='1'){?>
                  <a href="#" value="<?php echo $request['reimbursement_id'];?>" class="delete">
                      <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;request"><i class="fa fa-trash-o "></i></button>
                  </a>                  
               <?php }?>

          </td>
             </tr>
         <?php
         }
         ?>

         </tbody>
         </table>
         <?php }else{?>
          <p>No requests found</p>
        <?php }?>
     </div>
     </section>
     <!-- Panel End -->
  </section>
</section>

<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script>
$(document).ready(function(){
  $(".delete").click(function(){
  var r = confirm("Are you sure you want to delete your reimbursement request?");
  if(r == true){
    var id = $(this).attr("value");
    $.ajax({
        url: '<?php echo base_url(); ?>reimbursement/deleteRequest',
        data: { id:id },
        type: 'POST',
        success: function(data){        
          if(data == 1){
            alert("Request deleted successfully!");
            location.reload();
          }else{
            alert("Reimbursement cannot be deleted!");
            location.reload();
          }
          }
      });
  }

});



  var dt =  $('#yourRequest').dataTable( {
		 "aaSorting": [],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : true,

		 "oSearch": { "bSmart": false, "bRegex": true },
		 "aoColumnDefs": [
			{
			'bSortable': false, 'aTargets': [ -1, 2 ]
			}
		 ]
		});


    var dt =  $('#employeeRequest').dataTable( {
		 "aaSorting": [],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : true,

		 "oSearch": { "bSmart": false, "bRegex": true },
		 "aoColumnDefs": [
			{
			'bSortable': false, 'aTargets': [ -1, 5 ]
			}
		 ]
		}); 
});
function ff(sel){
    var ff = sel.value.split(",");
    var r = confirm("Are you sure you want to change the status of the request?");
    if (r == true){
        $.ajax({
            url: '<?php echo base_url(); ?>reimbursement/seniorAction',
            data: { request_id: ff[1], status: ff[0], user_id: ff[2]},
            type: 'POST',
            success: function(data){
                if(data == 0){
                    alert("Request Rejected successfully!");
                }
                else if(data == 1){
                    alert("Request Pending successfully!");
                }
                else if(data == 2){
                    alert("Request approved successfully!");
                }
                else{
                    alert("Oops! Error occured.");
                }
                location.reload();
            }
        });
    }
}  
</script>
