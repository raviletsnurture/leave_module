<?php 
$user = getAllUsers();
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
?>
<style>
.btn-dark {
    color: #fff;
    background-color: #343a40;
    border-color: #343a40;
}
.mr-2, .mx-2 {
    margin-right: .5rem!important;
}
.btn:hover, .btn:focus {
    color: #333;
    text-decoration: none;
    background-color:white;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
/* span.error{
    color:red;
} */
</style>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                        Add Request
					</header>
                    <div class="panel-body">
					<?php if($this->session->flashdata('error')){?>
                        <div class="alert alert-block alert-danger fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
                        </div>
                    <?php } ?>

                    <?php if($this->session->flashdata('success')){?>
                        <div class="alert alert-success fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                        </div>
                    <?php }?>
                        <div class="form">
                            <form enctype="multipart/form-data" method="POST" id="submitRequest" class="commonForm  cmxform form-horizontal tasi-form" action="<?php echo base_url().'reimbursement/addReimbursement' ?>">   

                                <div class="form-group">
									<label class="col-lg-2 control-label">Reimbursement date <span class="red">*</span></label>
									<div class="col-lg-6">
										<input size="16" type="text" id="datetime2" name="datetime" placeholder=" Ex. 2018-07-31 14:45" class="form_datetime form-control" readonly required>
										<p>*You can only request the reimbursement within 7 days.</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Amount <span class="red">*</span></label>
									<div class="col-lg-6">
										<input type="number" class="form-control" name="amount" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
									</div>
								</div>
                                
                                <div class="form-group">
									<label class="col-lg-2 control-label">Reciept</label>
									    <div class="col-lg-6">
                                            <input type="file" name="reciept" accept=".jpg,.jpeg,.png,.docx,.pdf">
                                            <p>*You can only upload png|jpeg|jpg|docx|pdf files</p>
                                        </div>
								</div>
                                
                                <div class="form-group">
									    <label class="col-lg-2 control-label">Description <span class="red display-star">*</span></label>
									    <div class="col-lg-6">
                                            <textarea class="form-control" name="description" id="" rows="3" required></textarea>
                                        </div>
								</div>
                                <div class="form-group">
									    <label class="col-lg-2 control-label">Mode of payment <span class="red display-star">*</span></label>
									    <div class="col-lg-6">
                                            <select class="form-control" name="payment_mode" required id="">
                                                <option value="">Select Request Type</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Card">Card</option>
                                                <option value="PayTM">PayTM</option>
                                            </select>

                                        </div>
								</div>
                                <div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-danger" type="submit" name="Add Reimbursement" id="addRequest">Submit</button>
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
									</div>
								</div>
                            </form> 
                        </div> 
                    </div>               
				</section>
            </div>
        </div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(document).ready( function(){

	$("#datetime2").datetimepicker({
		endDate: new Date,
		startDate: '-1w',
		autoclose: true
	});

    $('#submitRequest').validate({
        errorClass: "error",
        errorElement: "span",
        rules:{
            datetime:"required",
            amount:{
                required:true,
                number: true
            },
            reciept:{
       
                extension: "jpg|png|jpeg|pdf|docx|JPG|PNG|JPEG|PDF|DOCX"
            },
            description:"required",
            payment_mode:"required"
        },
        messages:{
            datetime:{
                required:"Date is required"
            },
            amount:{
                required:"Please enter your reimbursement amount",
                number:"Amount can only be a number!"
            },
            reciept:{
                extension:"Invalid file!"
            },
            description:{
                required:"Description is required"
            },
            payment_mode:{
                required:"Please select your payment mode"
            }    
        },
        submitHandler: function(form) {
            $('#addRequest').prop('disabled', true);   
            form.submit();
        }
    });
});
</script>