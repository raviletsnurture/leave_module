<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Salary extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('download');
		$this->load->helper('general_helper');
		$this->load->model('salary_model');
		user_auth(0);
	}
	function index(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['titalTag'] = ' - Salary Slips';

		$data["salary"] = $this->salary_model->getAllSalaryUploads($id);

		auditUserActivity('Salary', 'Index', $id, date('Y-m-d H:i:s'),$id);

		//var_dump($data); die;
	  $this->template->load_partial('dashboard_master','salary/list_salaryuploads',$data);
	}

	function logaudit(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$userClicked = $this->input->post('user_id');
		$userUpload = $this->input->post('upload_id');

		if($userClicked != $id) {
			auditUserActivity('Salary', 'ALERT: download not allowed', $id, date('Y-m-d H:i:s'),'#');
			$data ="fake";
			}else{
			$data = $this->salary_model->updateDownloadedAt($userClicked, $userUpload);
			auditUserActivity('Salary', 'download', $userClicked, date('Y-m-d H:i:s'),$id);
			$data ="OK";
		  }
    echo $data;
		//$data["salary"] = $this->salary_model->getAllSalaryUploads($id);
		//var_dump($data); die;

	}

	function forceSalaryDownload(){

		header('Content-Disposition: attachment; filename="file.pdf"');
		$userClicked = $this->input->get('user_id');
		$userUpload = $this->input->get('upload_id');
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		if($id != $userClicked){
			redirect('/salary', 'refresh');
			exit;
		}
		$mySalary = $this->salary_model->getSalarySlip($userUpload, $userClicked);
		//print_r($mySalary);
		foreach($mySalary as $row){
			$fileDownload = base_url().'uploads/salary_slips/'.$row->year.'/'.$row->month.'/'.$row->filename;
		}
		$pathDownload = file_get_contents($fileDownload);
		$nameFile = 'SalarySlip.pdf';
		force_download($nameFile,$pathDownload);
		//print_r($fileDownload); exit;

	}


}
?>
