<?php
$user = getAllUsers();
$department = getAllDepartment();
?>
<style type="text/css">
.dataTables_filter {
     display: none;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">
      <header class="panel-heading">My Salary Slips </header>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th>Full Name</th>

				<th>Year</th>
        <th>Month</th>
				<th>FileName</th>
				<th>Uploaded At</th>
        <th>Last Accessed</th>
        <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($salary as $row){ ?>
              <tr>
                <td><?php echo $row->first_name.' '.$row->last_name;?></td>

				<td><?php echo $row->year; ?></td>
				<td><?php echo date("M", mktime(null, null, null, $row->month, 1));?></td>
        <td><?php echo $row->filename;?></td>
				<td><?php echo $row->uploaded_at;?></td>
        <td><?php echo $row->last_accessed;?></td>
				<td>
          <!-- a href="<?php //echo base_url()?>superadmin/salary/edit/<?php //echo //$row->upload_id;?>" >
          <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;" title=""><i class="fa fa-pencil"></i></button>
        </a <?php //echo $row->upload_id;?>" href="<?php //echo base_url()?>uploads/salary_slips/<?php //echo //$row->year.'/'.$row->month.'/'.$row->filename;?>" -->
        <?php
              /*$mySalary = getSalarySlip($upload_id, $user_id);
        			print_r($mySalary); exit;
        			$fileDownload = base_url().'uploads/salary_slips/'.$row->year.'/'.$row->month.'/'.$row->filename;
        			$pathDownload = file_get_contents($fileDownload);
        			$nameFile = 'SalarySlip.pdf';*/
              $linkSalary = base_url().'salary/forceSalaryDownload?upload_id='.$row->upload_id.'&user_id='.$row->user_id;

        ?>
          <a value="<?php echo $row->upload_id;?>" href="<?php echo base_url()?>uploads/salary_slips/<?php echo $row->year.'/'.sprintf("%02d", $row->month).'/'.$row->filename;?>">
          <button class="btn btn-primary btn-xs tooltips" data-original-title="Download&nbsp;" title="" onClick="return recordActivity(<?php echo $row->user_id.",".$row->upload_id ;?>);"><i class="fa fa-eye"></i></button>
          </a>
        </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- page end-->

  </section>
</section>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">

function recordActivity(sel,uploadId)
{
   //alert(uploadId);
	  var id = sel;
			$('#loaderAjax').show();
			$.ajax({
			 url: '<?php echo base_url(); ?>salary/logaudit/',
			 data: { user_id: id, upload_id: uploadId},
			 type: 'POST',
			 success: function(data){
				 // hide loader
				 $('#loaderAjax').hide();
				 //location.reload();
         if(data !="OK") {
           alert("You are spoofing someone's account - Reported!");
           window.location = '<?php echo base_url(); ?>dashboard';
           return false;
         } else{
           //alert("all good");
           return true;
         }

       }

			});
      $('#loaderAjax').hide();
}
/*
          $(document).ready(function() {
             var dt =  $('#example').dataTable( {
                  //"aaSorting": [[ 3, "desc" ]],
				   "iDisplayLength": 20,
				   "pagingType": "full_numbers",
				   "dom": 'Cfrtip',
				   "destroy": true,
				   //"bFilter": false,
				   "bPaginate": true,
				   "bInfo" : false,

				   "oSearch": { "bSmart": false, "bRegex": true },
				   "aoColumnDefs": [
					  {
						'bSortable': true, 'aTargets': [ 5 ]
					  }
				   ]
              } );

			    $('select#engines').change( function() {
					dt.fnFilter( $(this).val(), 0 );
				} );
				$('select#engines1').change( function() {
					dt.fnFilter( $(this).val(), 1 );
				} );

			  $(".dataTables_filter input").addClass('form-control');
		  } ); */
</script>
