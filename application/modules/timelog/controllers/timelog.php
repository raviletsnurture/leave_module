<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timelog extends MX_Controller {
  var $logSession;
	function __construct(){
		parent::__construct();
    $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('timelog_model');
	}

	function index(){
		$data['titalTag'] = ' - Time Log';
    $loginSession = $this->session->userdata("login_session");
    $role_id = $loginSession[0]->role_id;
    $user_id = $loginSession[0]->user_id;
    $department_id = $loginSession[0]->department_id;
    $per = getUserpermission($role_id);
    $data['login_session'] = $loginSession;
    $data['permission'] = $per;
    $data['user_id'] = $user_id;
    $firstDate = date('Y-m-01');
    $lastDate = date('Y-m-t');
    $data["timelogs"] = $this->timelog_model->getAllTimelog($user_id,$firstDate,$lastDate);
    $data["totalHours"] = $this->timelog_model->getAllTotalTimelog($user_id,$firstDate,$lastDate);
    $data["leaveTaken"] = $this->timelog_model->getLeaveTakenInTimelog($user_id,$firstDate,$lastDate);
    $data["lateEntryCount"] = $this->timelog_model->getLateEntryInTimelog($user_id,$firstDate,$lastDate);
    auditUserActivity('Timelog', 'Index', $user_id, date('Y-m-d H:i:s'),$user_id);
    $this->template->load_partial('dashboard_master','timelog/index',$data);
	}

 function getLogByDate(){
    $userData = $this->logSession;
    $firstDate = $_POST['start_date'];
    $lastDate = $_POST['end_date'];
    if(isset($_POST['user_id']) && $_POST['user_id'] != ''){
      $id = $_POST['user_id'];
    }else{
      $id = $userData[0]->user_id;
    }
    $timelogs = $this->timelog_model->getAllTimelog($id,$firstDate,$lastDate);
    if($timelogs){
      $i=1; foreach ($timelogs as $timelog) { ?>
        <?php if($i == 1){ ?>
          <div class="row">
        <?php }?>

        <?php 
        $employ_logs = $timelog['employ_log'];
        $employ_logs = json_decode($employ_logs);
        $dayCode = date('D', strtotime($timelog['created_at']));
        ?>
 
       <div class="col-sm-4">
             <section class="panel">
               <div class="panel-body progress-panel">
                   <div class="task-progress <?php if($timelog['late_entry']==1 && $dayCode != 'Sun' && $dayCode != 'Sat' ){echo "red-color";}?>">
                       <h1>Time Log -  <?php echo date("d F, Y",strtotime($timelog['log_date']));?></h1>
                   </div>
               </div>
               <table class="table table-striped logTable">
                   <thead>
                   <tr>
                       <th><i class="fa fa-bookmark"></i> Door</th>
                       <th><i class="fa fa-bookmark"></i> IO Type</th>
                       <th><i class=" fa fa-edit"></i> Time</th>
                   </tr>
                   </thead>
                   <tbody>
                       <?php 
                       foreach($employ_logs as $employ_log){?>
                           <tr>
                               <td><?php echo $employ_log->device_name;?></td>
                               <td><?php if(($employ_log->device_name == 'MAIN DOOR' && $employ_log->EntryExitType == '1') || ($employ_log->device_name == 'RECREATIONAL AREA' && $employ_log->EntryExitType == '1')){echo "OUT";}else{echo "IN";}?></td>
                               <td><span class="label label-<?php if(($employ_log->device_name == 'MAIN DOOR' && $employ_log->EntryExitType == '1') || ($employ_log->device_name == 'RECREATIONAL AREA' && $employ_log->EntryExitType == '1')){echo "danger";}else{echo "success";}?> label-mini"><?php echo date('H:i', strtotime($employ_log->ETime));?></span></td>
                           </tr>
                       <?php }?>
                    </tbody>
                </table>
                <div class="panel-body">
                     <div class="alert alert-success">
                         Gross Work Hours: <?php echo date('H:i', strtotime($timelog['gross_work_hours'])); ?>
                     </div>
                     <div class="alert alert-danger">
                         Total Out Time: <?php echo date('H:i', strtotime($timelog['total_out_time'])); ?><br/>
                         <small>(Including Recreational Area)</small>
                     </div>
                     <div class="alert alert-info">
                         N-Punch Work Hours: <?php echo date('H:i', strtotime($timelog['nPunch_work_hours'])); ?>
                     </div>
                     <div class="alert alert-warning">
                         Extra Work Hours: <?php echo date('H:i', strtotime($timelog['extra_work_hours'])); ?>
                     </div>
                </div>
             </section>
           </div>
        <?php if($i == 3){ ?>
          </div>
        <?php $i=0;}?>
    <?php $i++; }
    }else{
      echo "0";
    }
	}

  function getTimeLogTotal(){
    $userData = $this->logSession;

    if(isset($_POST['user_id']) && $_POST['user_id'] != ''){
      $id = $_POST['user_id'];
    }else{
      $id = $userData[0]->user_id;
    }
    $firstDate = $_POST['start_date'];
    $lastDate = $_POST['end_date'];
    $totalHours = $this->timelog_model->getAllTotalTimelog($id,$firstDate,$lastDate);
    if($totalHours){
      $response['totalTime'] = @(str_replace(".",":", (string) round($totalHours->total,2)) * 100 / 185);
      $response['totalTimeHours'] = str_replace(".",":", (string) round($totalHours->total,2));
      echo json_encode($response);
    }else{
      $response['totalTime'] = 0;
      $response['totalTimeHours'] = 0;
      echo json_encode($response);
    }
  }

    function getLeaveDetailsOnAjax(){

        $userData = $this->logSession;
        if(isset($_POST['user_id']) && $_POST['user_id'] != ''){
          $id = $_POST['user_id'];
        }else{
          $id = $userData[0]->user_id;
        }

        $firstDate = $_POST['start_date'];
        $lastDate = $_POST['end_date'];

        $leaveTaken = $this->timelog_model->getLeaveTakenInTimelog($id,$firstDate,$lastDate);

        $leaveHtml = '';
        if(!empty($leaveTaken)){
            foreach($leaveTaken as $leaveDetails) {

            $start_date = new DateTime($leaveDetails->leave_start_date);
            $end_date = new DateTime($leaveDetails->leave_end_date);

            if((int)$leaveDetails->days > 1){
                $date_diff_text = '('.(int)$leaveDetails->days.' days)';
            }else{
                $date_diff_text = '('.(int)$leaveDetails->days.' day)';
            }

            $leaveHtml .= '<p><span>'.$leaveDetails->leave_name.' '.$date_diff_text.'</span> On
                <em style="font-size:14px; color: #c3c3c3;">'.$start_date->format('dS, M Y').' - '.$end_date->format('dS, M Y').' ('.date_format(date_create($leaveDetails->YR_MTH),'M').' Month)</em></p>';
            }
        }else{
            $leaveHtml = '<p> No leaves Found</p>';
        }
        echo json_encode(array('leaveData' => $leaveHtml), JSON_UNESCAPED_SLASHES);
    }

    function getLateEntryDetailsOnAjax(){
        $userData = $this->logSession;
        if(isset($_POST['user_id']) && $_POST['user_id'] != ''){
          $id = $_POST['user_id'];
        }else{
          $id = $userData[0]->user_id;
        }

        $firstDate = $_POST['start_date'];
        $lastDate = $_POST['end_date'];

        $lateEntryCount = $this->timelog_model->getLateEntryInTimelog($id,$firstDate,$lastDate);        
        if(!empty($lateEntryCount) ){
          $count = $lateEntryCount->total;
        }else{
          $count =  0;
        }
      echo json_encode(array('lateEntryData' => $count), JSON_UNESCAPED_SLASHES);
    }
}
?>
