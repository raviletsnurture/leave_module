<?php
class timelog_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}

	function getLoggedInUserPunchId($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}


	function getAllTimelog($id,$firstDate,$lastDate){
		$this->db->select("*");
		$this->db->from("crm_employ_log");
		$this->db->where("punch_id = $id");
		$this->db->where("log_date BETWEEN '$firstDate' AND '$lastDate'");
		$this->db->order_by("log_date","asc");
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getAllTotalTimelog($id,$firstDate,$lastDate){
		$this->db->select("(SUM( TIME_TO_SEC( `extra_work_hours` ) ) + SUM( TIME_TO_SEC( `gross_work_hours` ) ) )/3600 as total");
		$this->db->from("crm_employ_log");
		$this->db->where("punch_id = $id");
		$this->db->where("log_date BETWEEN '$firstDate' AND '$lastDate'");
		$result = $this->db->get()->row();
		return $result;
	}

	function getLeaveTakenInTimelog($id,$firstDate,$lastDate){
		$this->db->select('l.id,l.user_id,l.leave_id,u.first_name,u.last_name,l.leave_type,t.leave_type as leave_name,l.leave_start_date,l.leave_end_date,l.YR_MTH,l.days');
		$this->db->from('crm_leave_month as l');
		$this->db->join('users as u','u.user_id = l.user_id');
		$this->db->join('crm_leave_type as t','t.leave_type_id = l.leave_type');

		$this->db->where("l.user_id = $id");

		if(!empty($firstDate) && !empty($lastDate)){
			// $this->db->where("DATE(YR_MTH) >= DATE('".$firstDate."')");
			// $this->db->where("DATE(YR_MTH) <= DATE('".$lastDate."')");
			$this->db->where("MONTH(YR_MTH) >= MONTH('".$firstDate."')");
			$this->db->where("MONTH(YR_MTH) <= MONTH('".$lastDate."')");
			$this->db->where("YEAR(YR_MTH) = YEAR('".$firstDate."')");
			$this->db->where("YEAR(YR_MTH) = YEAR('".$lastDate."')");
		}

		$query=$this->db->get();
		return $query->result();
	}

	function getLateEntryInTimelog($id,$firstDate,$lastDate){
		$this->db->select("SUM(`late_entry`) as total");
		$this->db->from("crm_employ_log");
		$this->db->where("punch_id = $id");
		$this->db->where("log_date BETWEEN '$firstDate' AND '$lastDate'");
		$this->db->where("WEEKDAY(log_date) != 5 AND WEEKDAY(log_date) != 6");
		$result = $this->db->get()->row();
		return $result;



		if(!empty($firstDate) && !empty($lastDate)){
			// $this->db->where("DATE(log_date) >= DATE('".$firstDate."')");
			// $this->db->where("DATE(log_date) <= DATE('".$lastDate."')");
			$this->db->where("MONTH(log_date) >= MONTH('".$firstDate."')");
			$this->db->where("MONTH(log_date) <= MONTH('".$lastDate."')");
			$this->db->where("YEAR(log_date) = YEAR('".$firstDate."')");
			$this->db->where("YEAR(log_date) = YEAR('".$lastDate."')");
		}

		$query=$this->db->get();
		return $query->result();
	}

}
?>
