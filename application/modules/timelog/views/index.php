<?php
//Jignasa
// echo "<pre>";
// print_r($timelogs);
// echo "</pre>";
// exit;
$perUser = getJuniorUsersList($user_id, 'leave_users');
?>
<section id="main-content">
          <section class="wrapper">
            <div class="row">

        			<div class="col-lg-12">
        				<section class="panel">
        					<header class="panel-heading">Time Log</header>
        						<div class="panel-body">

        							<div class="alert alert-block alert-danger fade in" style="display:none;">
        								<strong>Sorry</strong> no time log on this day.
        							</div>

        							<div class="col-lg-8">
          								<?php
          									$form_attributes = array('name' => 'getTimeLog', 'id' => 'getTimeLog', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
          									echo form_open(base_url().'timelog/add',$form_attributes);
          								?>
                          <?php if(!empty($perUser)){?>
                          <div class="form-group">
          									<label class="col-lg-2 control-label">Select Users</label>
                            <div class="col-md-6">
                                <select id="user_id" name="user_id" class="form-control selectpicker js-example-basic-single" required="">
                                  <option value="">Select Users</option>
                                  <?php foreach($perUser as $row){?>
                                  <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
                                  <?php } ?>
                                </select>
                            </div>
                          </div>
                        <?php }?>
          								<div class="form-group col-lg-4">
          									<label>Start Date</label>
          										<input type="text" class="form-control" readonly="readonly" id="start_date" name="start_date" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-01'); ?>">
                           </div>
                           <div class="form-group col-lg-1">
                           </div>
                          <div class="form-group col-lg-4">
          									<label>End Date</label>
          										<input type="text" class="form-control" readonly="readonly" id="end_date" name="end_date" data-date-format="yyyy-mm-dd" placeholder="" value="<?php echo date('Y-m-d'); ?>">
                          </div>
                          <div class="form-group col-lg-1">
                          </div>
                          <div class="form-group col-lg-2">
                            <br/>
                            <label style="display:none;"> btn &nbsp;</label>
          										<button class="btn btn-danger" type="submit" name="checkLog" id="checkLog">
          											Submit
          										</button>
                          </div>
          							</form>
                      </div>
                      <div class="col-lg-2 col-sm-12 text-center">
                      </div>
                      <div class="col-lg-2 col-sm-12 text-center">
                            <div class="easy-pie-charts">
                              <h4>Working Hours</h4>
                              <?php
                              if($totalHours->total){$totalTime = str_replace(".",":", (string) round($totalHours->total,2));}else{$totalTime = '0';}
                              ?>
                                <div class="percentage easyPieChart" data-percent="<?php echo @(str_replace(".",":", (string) round($totalHours->total,2)) * 100 / 185);?>" style="width: 135px; height: 135px; line-height: 135px;margin-left: 50px;"><span><?php echo $totalTime;?></span> Hours<canvas width="135" height="135"></canvas></div>
                            </div>
                        </div>
        					</div>
        				</section>
        			</div>
        		</div>

                <div class="row">
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">Leave Detail</header>
                            <div class="panel-body" id="leaveDetails">
                                <?php
                                    if(!empty($leaveTaken)){
                                        foreach($leaveTaken as $leaveDetails) {
                                            $start_date = new DateTime($leaveDetails->leave_start_date);
                                            $end_date = new DateTime($leaveDetails->leave_end_date);

                                            if((int)$leaveDetails->days > 1){
                                                $date_diff_text = '('.(int)$leaveDetails->days.' days)';
                                            }else{
                                                $date_diff_text = '('.(int)$leaveDetails->days.' day)';
                                            }
                                             ?>
                                            <p><span><?php echo $leaveDetails->leave_name.'  '.$date_diff_text; ?></span>
                                            On
                                            <em style="font-size:14px; color: #c3c3c3;"><?php echo $start_date->format('dS, M Y').' - '.$end_date->format('dS, M Y').' ('.date_format(date_create($leaveDetails->YR_MTH),'M').' Month)';?></em></p>
                                    <?php }
                                }else{
                                    echo "<p> No leaves Found</p>";
                                } ?>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">Late Entry<em style="font-size:14px; color: #c3c3c3;"> (After 10 AM)</em></header>
                            <div class="panel-body" id="lateEntryDetails">
                              <?php
                                  if(!empty($lateEntryCount)){?>
                                          <p><span>Total : <?php echo $lateEntryCount->total; ?></span></p>
                                  <?php
                              }else{
                                  echo "<p> No late entry Found</p>";
                              } ?>
                            </div>
                        </section>
                    </div>
                </div>
            <div id="timeLogData">
            <?php

            $i=1; foreach ($timelogs as $timelog) {
              $dayCode = date('D', strtotime($timelog['created_at']));
              ?>
                <?php if($i == 1){ ?>
                  <div class="row">
                <?php }?>
               <div class="col-sm-4">
                     <section class="panel">
                       <div class="panel-body progress-panel">
                           <div class="task-progress <?php if($timelog['late_entry']==1 && $dayCode != 'Sun' && $dayCode != 'Sat' ){echo "red-color";}?>">
                               <h1>Time Log -  <?php echo date("d F, Y",strtotime($timelog['log_date']));?></h1>
                           </div>
                       </div>
                           <table class="table table-striped logTable">
                               <thead>
                               <tr>
                                   <th><i class="fa fa-bookmark"></i> Door</th>
                                   <th><i class="fa fa-bookmark"></i> IO Type</th>
                                   <th><i class=" fa fa-edit"></i> Time</th>
                               </tr>
                               </thead>
                               <tbody>
                                     <?php
                                     $employ_logs = $timelog['employ_log'];
                                     $employ_logs = json_decode($employ_logs);
                                     foreach($employ_logs as $employ_log){?>
                                       <tr>
                                           <td><?php echo $employ_log->device_name;?></td>
                                           <td><?php if(($employ_log->device_name == 'MAIN DOOR' && $employ_log->EntryExitType == '1') || ($employ_log->device_name == 'RECREATIONAL AREA' && $employ_log->EntryExitType == '1')){echo "OUT";}else{echo "IN";}?></td>
                                           <td><span class="label label-<?php if(($employ_log->device_name == 'MAIN DOOR' && $employ_log->EntryExitType == '1') || ($employ_log->device_name == 'RECREATIONAL AREA' && $employ_log->EntryExitType == '1')){echo "danger";}else{echo "success";}?> label-mini"><?php echo date('H:i', strtotime($employ_log->ETime));?></span></td>
                                       </tr>
                                     <?php }?>
                               </tbody>
                           </table>
                           <div class="panel-body">
                                <div class="alert alert-success">
                                    Gross Work Hours: <?php echo date('H:i', strtotime($timelog['gross_work_hours'])); ?>
                                </div>
                                <div class="alert alert-danger">
                                    Total Out Time: <?php echo date('H:i', strtotime($timelog['total_out_time'])); ?><br/>
                                    <small>(Including Recreational Area)</small>
                                </div>
                                <div class="alert alert-info">
                                    N-Punch Work Hours: <?php echo date('H:i', strtotime($timelog['nPunch_work_hours'])); ?>
                                </div>
                                <div class="alert alert-warning">
                                    Extra Work Hours: <?php echo date('H:i', strtotime($timelog['extra_work_hours'])); ?>
                                </div>
                           </div>
                         </section>
                       </div>
                <?php if($i == 3){ ?>
                  </div>
                <?php $i=0;}?>
            <?php $i++; }?>
            </div>

      </section>
  </section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script type="text/javascript">
	$('#start_date,#end_date').datepicker({
    endDate: "1d",
   	autoclose: true,
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#checkLog').click(function(event){
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();

			// Work on holiday only allowed in Sat and Sun
			var week_day_start = new Date(start_date);
			var week_day_end = new Date(end_date);

			var week_start = week_day_start.getDay();
			var week_end = week_day_end.getDay();

			//Date Comparision
			var date_one = new Date(start_date);
			var date_two = new Date(end_date);

			if(date_two >= date_one){

				event.preventDefault();
				var formData = $("#getTimeLog").serialize();
				$.ajax({
					type: 'POST',
					data: formData,
					url: '<?php echo base_url()?>timelog/getLogByDate',
					cache: false,
					async:false,
					processData: false,
					success:function(data){
            if(data != '0'){
              $("#timeLogData").html(data);
              $(".alert-block").hide()
                $.ajax({
        					type: 'POST',
        					data: formData,
        					url: '<?php echo base_url()?>timelog/getTimeLogTotal',
        					cache: false,
        					async:false,
        					processData: false,
        					success:function(data){
                    var json = $.parseJSON(data);
                    $('.easyPieChart').data('easyPieChart').update(json.totalTime);
                    $('.easyPieChart span').html(json.totalTimeHours);

        					 return false;
        					}
         				});
            }else{
              $(".alert-block").show();
              $("#timeLogData").html("");
              $('.easyPieChart').data('easyPieChart').update(0);
              $('.easyPieChart span').html(0);
            }
					 return false;
					}
 				});

            // Get leave Details
            $.ajax({
                type: 'POST',
                data: formData,
                url: '<?php echo base_url()?>timelog/getLeaveDetailsOnAjax',
                cache: false,
                async:false,
                processData: false,
                success:function(data){
                    var json = $.parseJSON(data);
                    var leaveData = json['leaveData'];
                    $('#leaveDetails').html(leaveData);
                }
            });

            // Get late entry Details
            $.ajax({
                type: 'POST',
                data: formData,
                url: '<?php echo base_url()?>timelog/getLateEntryDetailsOnAjax',
                cache: false,
                async:false,
                processData: false,
                success:function(data){
                    var json = $.parseJSON(data);
                    var lateEntryData = json['lateEntryData'];
                    if(lateEntryData != 0){
                      $('#lateEntryDetails').html('<p> Total : '+lateEntryData+'</p>');
                    }else{
                      $('#lateEntryDetails').html('<p> No late entry Found</p>');
                  }
                }
            });

		}else {
			alert('End Date must be greater then Start Date');
			event.preventDefault();
		}
  });//onclick
});//document.ready
</script>
