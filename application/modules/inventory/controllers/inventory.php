<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends MX_Controller {
  var $logSession;
	function __construct(){
		parent::__construct();
        $this->template->set('controller', $this);
		user_auth(0);
        $this->load->database();
		$this->logSession = $this->session->userdata("login_session");
        $this->load->helper('general_helper');
        $this->load->helper('general2_helper');
        $this->load->library('email');
        $this->load->model('inventory_model');
	}

	function vendor(){
        $data['titalTag'] = ' - Vendors';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;                
        $data['userDepartmentId'] = $userData[0]->department_id;
        $data['getAllVendors'] = $this->inventory_model->getAllVendors();
        $this->template->load_partial('dashboard_master','inventory/vendors',$data);
	}
    function addvendor(){
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        parse_str($this->input->post('data'), $parseData);
        $addData['vendor_name'] = $parseData['vendorName'];
        $addData['address'] = $parseData['address'];
        $addData['service'] = implode(", ",$parseData['service']);
        $addData['vendorType'] = $parseData['vendorType'];
        $addData['remarks'] = $parseData['remarks'];
        if($parseData['phone'] !=''){           
            $addData['phone'] = $parseData['phone'];
        }else{
            $addData['phone'] = NULL;
        }
        $addData['altPhone'] = $parseData['altPhone'];
        $addData['person'] = $parseData['person'];
        $addData['altPerson'] = $parseData['altPerson'];
        $addData['website'] = $parseData['website'];
        $response = array();
        if($parseData['vid'] == 0){
            $addData['addedby'] = $id;
            $data = $this->inventory_model->insertVendor($addData);
            if($data){
                echo 1;    
            }else{
                echo 2;
            }            
        }else{            
            $addData['updatedBy'] = $id;
            $data = $this->inventory_model->updateVendor($addData, $parseData['vid']);
            if($data){
                echo 3;
            }else{
                echo 4;
            }
        }
        
    }
    
    function getVendor($id=null)
	{	      
        if($this->input->post('id') != null || $this->input->post('id') != ''){
            $id = $this->input->post('id');
        }
        $data = $this->inventory_model->getVendorById($id);
        echo json_encode($data);
	}
    public function deleteVendor()
	{
        $id = $this->input->post('id');
        $result = $this->inventory_model->getInventoryByVendorId($id);
        if(sizeof($result) < 1){
            $deleteVendor = $this->inventory_model->removeVendor($id);
            if($deleteVendor){
                echo 1;
            }
        }else{
            echo 0;
        }		
    }
    

    // Inventory Category starts-->
    function inventoryTypes(){
        $data['titalTag'] = ' - Inventory type';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;                
        $data['userDepartmentId'] = $userData[0]->department_id;
        $data['getOnlyParentCategory'] = $this->inventory_model->getOnlyParentCategory();
        $data['getAllCategory'] = $this->inventory_model->getAllCategory();        
        $this->template->load_partial('dashboard_master','inventory/inventory_types',$data);
    }
    function saveInventoryCategory(){
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        parse_str($this->input->post('data'), $parseData);

        if(isset($parseData['hasDetail'])){
            $data['hasDetail'] = implode(", ",$parseData['hasDetail']);
        }
        $data['cat_name'] = $parseData['categoryName'];
        if($parseData['parentCat'] != ''){
            if(isset($parseData['subCat'])){
                $subCat = $parseData['subCat'];            
                if($subCat[0] !='' && $subCat[0] != null){
                    $t = sizeof($subCat)-1;
                    if($subCat[$t] == '' || $subCat[$t] == null){
                        $finalInventory = $subCat[$t-1];
                    }else{
                        $finalInventory = $subCat[$t];
                    }
                }else{
                    $finalInventory = $parseData['parentCat'];
                } 
            }else{
                $finalInventory = $parseData['parentCat'];
            }
            $data['parent_cat'] = $finalInventory;
        }
        $data['categoryTag'] = $parseData['categoryTag'];
        $data['cat_description'] = $parseData['catDescription'];
    
        $response = array();
        if($parseData['cid'] == 0){
            $data['addedBy'] = $id;
            $data = $this->inventory_model->insertCategory($data);
            if($data){
                // $response = array("insert"=>"success");
                echo 1;
            }else{
                echo 2;
                // $response = array("insert"=>"failure");
            }
            
        }else{
            
            $data['updatedBy'] = $id;
            $data = $this->inventory_model->updateCategory($addData, $parseData['cid']);
            if($data){
                echo 3;
                // $response = array("update"=>"success");
            }else{
                echo 4;
                // $response = array("update"=>"failure");
            }
        }
        // echo json_encode($response);
    }
    function editcategory($id){
        $data['titalTag'] = ' - Edit category'; 
        $data['getCategory'] = $this->inventory_model->getCategoryById($id); 
        $subCat = $data['getCategory']->parent_cat;
        if($subCat != 0){ 
            $ar = array();
            $data2 = $this->search_subCategory($subCat);                      
            if($data2->parent_cat != 0){
                $flag = true;
            }else{
                $flag = false;
            }
            while($flag != false){
                array_push($ar,$data2);
                $tmp  = $this->search_subCategory($data2->parent_cat);
                $data2 = $tmp;
                if($tmp){
                    $flag = true;
                }else{
                    $flag = false;
                }
            }
            $data['InventoryCategories'] =array_reverse($ar, false);
        }    
        $data['getOnlyParentCategory'] = $this->inventory_model->getOnlyParentCategory();
        $data['hasDetail'] = explode(", ",$data['getCategory']->hasDetail);
        $this->template->load_partial('dashboard_master','inventory/edit_category',$data);
    }

    function updateCategoryData(){
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        if ($this->input->post() !== FALSE && isset($_POST['updateCategory']) ) { 
            $data['cat_name'] = $this->input->post('categoryName');
            if(isset($_POST['hasDetail'])){
                $data['hasDetail'] = implode(", ",$this->input->post('hasDetail'));
            }
            $data['cat_description'] = $this->input->post('catDescription');
            if(isset($_POST['categoryTag'])){
                $data['categoryTag'] = $this->input->post('categoryTag');
            }
            $catId = $this->input->post('cid');

            $parentCat = $this->input->post('parentCat');
            if($_POST['parentCat'] == null){
                $data['parent_cat'] = 0;
            }else{    
                if(isset($_POST['subCat'])){
                    $subCat = $this->input->post('subCat');           
                    if($subCat[0] !='' && $subCat[0] != null){
                        $t = sizeof($subCat)-1;
                        if($subCat[$t] == '' || $subCat[$t] == null){
                            $finalInventory = $subCat[$t-1];
                        }else{
                            $finalInventory = $subCat[$t];
                        }
                    }else{
                        $finalInventory = $this->input->post('parentCat');
                    } 
                }else{        
                    $finalInventory = $this->input->post('parentCat');
                }
                $data['parent_cat'] = $finalInventory; 
            }
            $data['updatedBy'] = $id;
            $update = $this->inventory_model->updateCategory($catId, $data);
            if($update){
                $this->session->set_flashdata('success', 'Category updated successfully!');
                redirect(base_url().'inventory/inventoryTypes');
            }else{      
                $this->session->set_flashdata('error', 'Error in updating category!');
                redirect(base_url().'inventory/inventoryTypes');    
            }
        }else{
            $this->session->set_flashdata('error', 'Access denied!');
            redirect(base_url().'inventory/inventoryTypes');
        }

    }
    function getAjaxSubcategory(){
        $id = $this->input->post('id');        
        if(isset($_POST['cid'])){
            $cid = $this->input->post('cid');
            $subCat = $this->inventory_model->subCategoryWithId($id, $cid);
        }else{
            $subCat = $this->inventory_model->subCategory($id);
        }
        // $getInventoryCategories = $this->inventory_model->getInventoryCategories($id);
        $level = $this->input->post('level');        
        if($subCat != null && $subCat != ''){?>            
            <select style="margin-top:10px;" class="form-control max-width <?php echo $level; ?>" onchange="getCategoryId(this);" name="subCat[]" id="<?php echo $level; ?>">
            <option value="">--select--</option>
                <?php foreach($subCat as $row){?>                    
                    <option value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>    
                <?php }?>
            </select>

        <?php } else{
            echo '';
        }        
    }
    function deleteCategory(){
        $id = $this->input->post('id');
        $getSubcat = $this->inventory_model->subCategory($id);
        $getInventoryBycategorId = $this->inventory_model->getInventoryBycategoryId($id);

        if( sizeof($getSubcat)>0 || sizeof($getInventoryBycategorId)>0 ){
            echo 0;
        }else{
            $delete = $this->inventory_model->deleteCat($id);
            if($delete){
                echo 1;
            }else{
                echo 2;
            }            
        }
    }

    function search_subCategory($subCat){
        $pid = $this->inventory_model->getCategoryById($subCat);
        return $pid;
    }
    function getAjaxCategory(){
        $id = $this->input->post('id');
        $res = $this->inventory_model->getCategoryById($id);
        echo json_encode($res); 
    }
    // -------------------For Inventory module-------------------
    function allInventory(){
        $data['titalTag'] = ' - All inventory';
        $userData = $this->logSession;
        $id = $userData[0]->user_id; 
        $data['userDepartmentId'] = $userData[0]->department_id;  
        $data['allInventory'] = $this->inventory_model->getAllInventory();     
        $this->template->load_partial('dashboard_master','inventory/all_inventory',$data);
    }

    function addInventory(){
        $data['titalTag'] = ' - Add inventory';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;  
        $data['getOnlyParentCategory'] = $this->inventory_model->getOnlyParentCategory();
        $data['allVendors'] = $this->inventory_model->getAllVendors ();
        $this->template->load_partial('dashboard_master','inventory/add_inventory',$data);
    }
    function getAjaxVendor(){
        $output='';
        $id = $this->input->post('id'); 
        $vendor = $this->inventory_model->vendorByType($id);
        if($vendor != '' && $vendor!=null){
            foreach($vendor as $row){
                $output.='<option value="'.$row->vendor_id.'">'.$row->vendor_name.'</option>';
            }

        }else{
            $output.='<option value="">--No inventory found--</option>';
        }
        echo $output;
    }
    function getUserAssignedHardware(){
        $id = $this->input->post('id');
        $result = $this->inventory_model->getUserAndInventoryAssigned($id);        
        if($result){
            echo json_encode($result);
        }else{
            echo 0;
        }
    }
    function getTag(){
        parse_str($this->input->post('inventoryForm'), $parseData); 
        if(isset($parseData['subCat'])){
            $subCat = $parseData['subCat'];            
            if($subCat[0] !='' && $subCat[0] != null){
                $t = sizeof($subCat)-1;
                if($subCat[$t] == '' || $subCat[$t] == null){
                    $finalInventory = $subCat[$t-1];
                }else{
                    $finalInventory = $subCat[$t];
                }
            }else{
                $finalInventory = $parseData['parentCat'];
            } 
        }else{
            $finalInventory = $parseData['parentCat'];
        }
        $getCategory = $this->inventory_model->getCategoryById($finalInventory);
        $getInventories = $this->inventory_model->getAllInventoryByCatId($finalInventory);

        if($parseData['operationStatus'] == 0){
            if(sizeof($getInventories)!=0){
                $count = sizeof($getInventories)+1;
            }else{
                $count = 1;
            }
            $tagValue = 'LN/'.$getCategory->categoryTag.'/'.sprintf("%03d", $count);
        }else{
            $tagValue = $parseData['inventoryTag'];
        }
        echo json_encode($tagValue);
        
    }
    function addInventoryData(){  
        $userData = $this->logSession;
        $id = $userData[0]->user_id;       
        $inventoryId = $this->input->post('checkAction');
        if ($this->input->post() !== FALSE && isset($_POST['saveInventory']) ) {                    
            $parentCat = $this->input->post('parentCat');
            if($parentCat == '' || $parentCat == NULL){
                $data['category_id'] = '0';
            }else{
                if(isset($_POST['subCat'])){
                    if($_POST['subCat'][0] == null || $_POST['subCat'][0] == ''){
                        $data['category_id'] = $parentCat;
                    }else{
                        $subCat = $this->input->post('subCat');
                        $t = sizeof($subCat)-1;
                        if($subCat[$t] == '' || $subCat[$t] == null){
                            $data['category_id'] = $subCat[$t-1];
                        }else{
                            $data['category_id'] = $subCat[$t];
                        }
                    }
                }else{
                    $data['category_id'] = $parentCat;
                }
    
            }
            $data['purchase_date'] = $this->input->post('purchaseDate');
            if($_POST['warrantyRenewal'] != null){
                $data['renewal_date'] = $this->input->post('warrantyRenewal');
            }
            if($_POST['paymentCycle'] != null){
                $data['payment_cycle'] =$this->input->post('paymentCycle');
            }           
            if($_POST['serial'] != null){
                $data['serial'] = $this->input->post('serial');
            }
            if($_POST['mac'] != null){
                $data['mac_id'] = $this->input->post('mac');
            }
            $data['tag'] = $this->input->post('inventoryTag');
            $data['cost_price'] = $this->input->post('costPrice');
            $data['cp_Currency'] = $this->input->post('costCurrency');
            $data['payment_status'] = $this->input->post('paymentStatus');
            $data['purchase_mode'] = $this->input->post('purchaseMode');
            $data['remarks'] = $this->input->post('remarks');
            $data['vendor'] = $this->input->post('vendor');
            if(isset($_POST['inventoryStatus'])){
                $data['assign_status'] = $this->input->post('inventoryStatus');
            }
            $data['selling_price'] = $this->input->post('sellingPrice');
            $data['sp_currency'] = $this->input->post('saleCurrency');
            if($this->input->post('soldTo')== null || $this->input->post('soldTo') == ''){
                $data['soldTo'] = 0;
            }else{
                $data['soldTo'] = $this->input->post('soldTo');
            }
            if (!empty($_FILES['bills']['name'][0])) {
                if($inventoryId == 0){
                    $impBills = implode(",", $_FILES['bills']['name']);
                    $data['bills'] = preg_replace('/\s+/', '_', $impBills);                    
                }
                else{
                    $getInventory = $this->inventory_model->getInventoryById($inventoryId);
                    if($getInventory->bills !='' || $getInventory->bills != null){
                        $explodedBill = explode(",",$getInventory->bills);     
                        $newBill = $_FILES['bills']['name'];
                        $newImplodedBill = array_merge($explodedBill, $newBill);
                    } else{
                        $newImplodedBill  = $_FILES['bills']['name'];
                    } 
                    $impBills = implode(",", $newImplodedBill);         
                    $data['bills'] = preg_replace('/\s+/', '_', $impBills);
                }
            }     
            if($inventoryId == 0){
                $data['addedBy'] = $id;
                $save = $this->inventory_model->saveInventory($data); 
            }else{
                $data['updatedBy'] = $id;
                $save = $this->inventory_model->updateInventory($data ,$inventoryId);         
            }

            if($save){
                if(!empty($_FILES['bills']['name'][0])) {
                    if($inventoryId == 0){
                        $insert_id = $this->db->insert_id();
                        $filePath = 'uploads/inventory/'.$insert_id;
                    }else{
                        $filePath = 'uploads/inventory/'.$inventoryId;
                    }
                    makeDir($filePath);
                    $number_of_files = count($_FILES['bills']['name']);
                    $files = $_FILES;
                    for($i=0; $i < $number_of_files; $i++){
                        $_FILES['bills']['name'] = $files['bills']['name'][$i];
                        $_FILES['bills']['type'] = $files['bills']['type'][$i];
                        $_FILES['bills']['tmp_name'] = $files['bills']['tmp_name'][$i];
                        $_FILES['bills']['error'] = $files['bills']['error'][$i];
                        $_FILES['bills']['size'] = $files['bills']['size'][$i];
        
                        $config['upload_path']   = $filePath;
                        $config['allowed_types'] = 'jpg|JPG|PNG|png|jpeg|JPEG|DOCX|docx|pdf|PDF';
                        $config['max_size']      = '0';
                        $config['overwrite']     = TRUE;
                        $config['remove_spaces']     = TRUE;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('bills')) {
                            $data = array('upload_data' => $this->upload->data());          
                            // $this->session->set_flashdata('success', 'Inventory added successfully!');
                            // redirect(base_url().'inventory/all_inventory');                                                           
                        } else {
                            $error = array('error' => $this->upload->display_errors());
                            // $this->session->set_flashdata('error', 'Error! Please check the bills and try again');
                            // redirect(base_url().'inventory/all_inventory');         
                        }
                    }
                }
                if($inventoryId == 0){
                    $results = getAllUsersOFDepartment(11);
                    $userIds = array(); $Tokenlist = array();
                    foreach ($results as $result) {
                        array_push($userIds, $result->user_id);                
                    }
                    $impId = implode(', ',$userIds);
                    $deviceTokens = getUserDeviceToken($impId);
                    foreach($deviceTokens as $value){
                        array_push($Tokenlist,$value->deviceToken);
                    } 
                    $notification_message = 'A new Inventory has been added by network team';
                    // SendNotificationFCMWeb('Inventory notification' ,$notification_message,$Tokenlist);
                    $update['isBoughtStatus'] = "1";
                    $changeVendorBoughtStatus = $this->inventory_model->updateVendor($update, $_POST['vendor']);


                    $this->session->set_flashdata('success', 'Inventory added successfully!');
                    redirect(base_url().'inventory/allInventory');
                }else{
                    $this->session->set_flashdata('success', 'Inventory Updated successfully!');
                    redirect(base_url().'inventory/allInventory');
                }
            }else{
                if($inventoryId == 0){
                    $this->session->set_flashdata('error', 'Error in adding inventory!');
                    redirect(base_url().'inventory/allInventory');
                    exit;
                }else{
                    $this->session->set_flashdata('error', 'Error in updating inventory!');
                    redirect(base_url().'inventory/allInventory');
                    exit;
                }
            }
        }
    }
    function editInventory($id){        
        $data['titalTag'] = ' - Update inventory';
        $data['id'] = $id;
        $data['inventoryById'] = $this->inventory_model->getInventoryById($data['id']);
        $subCat = $data['inventoryById']->category_id;
        if($subCat != 0){                    
            $ar = array();
            $data2 = $this->search_subCategory($subCat);            
            if($data2->parent_cat != 0){
                $flag = true;
            }else{
                $flag = false;
            }
            while($flag != false){
                array_push($ar,$data2);
                $tmp  = $this->search_subCategory($data2->parent_cat);
                $data2 = $tmp;
                if($tmp){
                    $flag = true;
                }else{
                    $flag = false;
                }
            }        
            $data['InventoryCategories'] =array_reverse($ar, false);
            
        }      
        $data['getOnlyParentCategory'] = $this->inventory_model->getOnlyParentCategory();
        $data['allVendors'] = $this->inventory_model->getAllVendors();
        $data['bills'] = explode(",", $data['inventoryById']->bills); 
        $checkHasDetail = $this->inventory_model->getCategoryById($data['inventoryById']->category_id);    
        $data['hasDetail'] = explode(", ",$checkHasDetail->hasDetail);  
        $this->template->load_partial('dashboard_master','inventory/add_inventory',$data);
    }

    function deleteOnlyFile(){
        $id = $this->input->post('id');
        $file = $this->input->post('file');
        $inventoryById = $this->inventory_model->getInventoryById($id);
        $explodedBills = explode(",",$inventoryById->bills);
        $filePath = FCPATH.'/uploads/inventory/'.$id.'/'.$explodedBills[$file];
        if(unlink($filePath)){
            unset($explodedBills[$file]);
            $impBills = implode(",",$explodedBills);
            $implodedBill = preg_replace('/\s+/', '_', $impBills);
            $updateFile = $this->inventory_model->updateFileOnly($id ,$implodedBill);
            if($updateFile){
                echo 1;
            }
            else{
                echo 2;
            }

        }
    }
    function deleteOnlyFileRepair(){
        $id = $this->input->post('id');
        $file  = $this->input->post('file');
        $inventoryById = $this->inventory_model->getRepairs($id);
        $explodedBills = explode(",",$inventoryById->repairBills);    
        $filePath = FCPATH.'/uploads/Inventory_repair/'.$id.'/'.$explodedBills[$file];      
        if(unlink($filePath)){
            unset($explodedBills[$file]);
            $impBills = implode(",",$explodedBills);
            $implodedBill = preg_replace('/\s+/', '_', $impBills);
            $updateFile = $this->inventory_model->updateFileForRepair($id ,$implodedBill);
            if($updateFile){
                echo 1;
            }
            else{
                echo 2;
            }

        }
    }
    function deleteInventory(){        
        $id = $this->input->post('id');
        $getInventory = $this->inventory_model->getInventoryById($id);  
        $count = $this->inventory_model->getInventoriesCountByVendorId($getInventory->vendor);
        
        if($count == 1){
            // for changing IsBought status of the vendor
            $update['isBoughtStatus'] = "0";
            $changeVendorBoughtStatus = $this->inventory_model->updateVendor($update, $getInventory->vendor);
        }
        $deleteInventory = $this->inventory_model->deleteInventory($id);
        if($deleteInventory){
            echo 1;
            // $filePath = FCPATH.'/uploads/inventory/'.$id;
            // array_map('unlink', glob("$filePath/*.*"));
            // if(is_dir($filePath)){
            //     rmdir($filePath);
            // }
        }else{
            echo 2;
        }
    }
    

    // USER HARDWARE STARTS->
    function assignInventory(){
        $data['titalTag'] = ' - Update inventory';
        $userData = $this->logSession;
        $id = $userData[0]->user_id; 
        $data['userDepartmentId'] = $userData[0]->department_id;
        $data['allActiveUsers'] = getAllUsers();
        $data['getInventoryByFilter'] = $this->inventory_model->getInventoryByFilter();
        $data['getAllAssignedUsers'] = $this->inventory_model->getAllAssignedUsers();
        $this->template->load_partial('dashboard_master','inventory/user_inventory',$data);
    }

    function assignAjaxInventory(){
        $userData = $this->logSession;
        $id = $userData[0]->user_id;
        $userId = $this->input->post('userId');
        $selInventory = $this->input->post('selInventory');
        
        foreach($selInventory as $row){
            $data['user_id'] = $userId;
            $data['inventory_id'] = $row; 
            $data['assignedBy'] = $id;           
            $result = $this->inventory_model->saveAssignedInventory($data);            
        }
        $inventories = implode(', ',$selInventory);
        $inventoryResult = $this->inventory_model->updateAssignInventory($inventories);
        if($inventoryResult){
            echo 1;
        }else{
            echo 0;
        }
    }
    function removeAjaxAssignedInventory(){
        $userId = $this->input->post('assignId');
        $inventory_id = $this->input->post('inventory_id');
        $result = $this->inventory_model->removeAssignedinventory($userId);
        if($result){
            $changeStatusinventory = $this->inventory_model->updateInventoryStatus($inventory_id);
            if($changeStatusinventory){
                echo 1;
            }else{
                echo 0;
            }
        }

    }


    // Repair module starts--->
    function repairInventory(){
        $data['titalTag'] = ' - Repair inventory';
        $userData = $this->logSession;
        $id = $userData[0]->user_id; 
        $data['userDepartmentId'] = $userData[0]->department_id;
        $data['getRepairs'] = $this->inventory_model->getRepairs();
        $this->template->load_partial('dashboard_master','inventory/repair',$data);
    }
    function addRepair(){
        $data['titalTag'] = ' - Add Inventory Repair';
        $userData = $this->logSession;
        $id = $userData[0]->user_id;  
        $data['getOnlyParentCategory'] = $this->inventory_model->getOnlyHardware();
        $data['allVendors'] = $this->inventory_model->getAllVendors();
        $this->template->load_partial('dashboard_master','inventory/add_repair',$data);
    }
    function getAjaxAllTag(){
        $output = '';
        $id = $this->input->post('id');
        $result = $this->inventory_model->getAllUnassignedInventory($id);
        if($result != null ){
            foreach($result as $row){
                $output.='<option value="'.$row->inventory_id.'">'.$row->tag.'</option>';
            }
        }else{
            $output.='<option value="">--No tags available--</option>';
        }
        echo $output;
    }
    function editRepair($id){
        $data['titalTag'] = ' - Update Inventory Repair';
        $data['id'] = $id;
        $data['getOnlyParentCategory'] = $this->inventory_model->getOnlyHardware();
        $data['allVendors'] = $this->inventory_model->getAllVendors();
        $data['getRepairById'] = $this->inventory_model->getRepairs($id);
        
        $subCat = $data['getRepairById']->inventory_category;
        if($subCat != 0){                    
            $ar = array();
            $data2 = $this->search_subCategory($subCat);            
            if($data2->parent_cat != 0){
                $flag = true;
            }else{
                $flag = false;
            }
            while($flag != false){
                array_push($ar,$data2);
                $tmp  = $this->search_subCategory($data2->parent_cat);
                $data2 = $tmp;
                if($tmp){
                    $flag = true;
                }else{
                    $flag = false;
                }
            }
            
            $data['InventoryCategories'] =array_reverse($ar, false); 
        }
        $data['bills'] = explode(",", $data['getRepairById']->repairBills);
        $data['inventoryTags'] = $this->inventory_model->getInventoryCategories($data['getRepairById']->inventory_category);
        $this->template->load_partial('dashboard_master','inventory/add_repair',$data);
    }
    function addRepairData(){ 
        $userData = $this->logSession;
        $id = $userData[0]->user_id;  
        $repairId = $this->input->post('checkAction');        
        if ($this->input->post() !== FALSE && isset($_POST['saveRepair']) ){
            $parentCat = $this->input->post('parentCat');
            if($parentCat == '' || $parentCat == NULL){
                $data['inventory_category'] = '0';
            }else{
                if(isset($_POST['subCat'])){
                    if($_POST['subCat'][0] == null || $_POST['subCat'][0] == ''){
                        $data['inventory_category'] = $parentCat;
                    }else{
                        $subCat = $this->input->post('subCat');
                        $t = sizeof($subCat)-1;
                        if($subCat[$t] == '' || $subCat[$t] == null){
                            $data['inventory_category'] = $subCat[$t-1];
                        }else{
                            $data['inventory_category'] = $subCat[$t];
                        }
                    }
                }else{
                    $data['inventory_category'] = $parentCat;
                }
    
            }
            $data['inventory_id'] = $this->input->post('inventoryTag');
            $data['details'] = $this->input->post('repairDetails');
            $data['repair_date'] = $this->input->post('repairDate');
            if($this->input->post('completionDate') != null || $this->input->post('completionDate') != ''){
                $data['completion_date'] = $this->input->post('completionDate');
            }
            $data['vendor'] = $this->input->post('vendor');
            $data['currency'] = $this->input->post('currency');
            $data['expense'] = $this->input->post('expense');
            $data['repair_type'] = $this->input->post('repairType');

            if (!empty($_FILES['bills']['name'][0])){
                if($repairId == 0){
 
                    $impBills = implode(",", $_FILES['bills']['name']);
                    $data['bills'] = preg_replace('/\s+/', '_', $impBills);                    
                }
                else{
   
                    $getInventory = $this->inventory_model->getRepairs($repairId);
                    if($getInventory->repairBills !='' && $getInventory->repairBills != null){
                        $explodedBill = explode(",",$getInventory->repairBills); 
                        $newBill = $_FILES['bills']['name'];
                        $newImplodedBill = array_merge($explodedBill, $newBill); 
                    }else{
                        $newImplodedBill  = $_FILES['bills']['name'];
                    } 
                    $impBills = implode(",", $newImplodedBill);         
                    $data['bills'] = preg_replace('/\s+/', '_', $impBills);
                }
            }
            
            
            if($repairId == 0){
                $data['addedBy'] = $id;
                $save = $this->inventory_model->saveRepair($data);
                if($save){
                    $dataset['assign_status'] = "2";
                    $getInventoryByTag = $this->inventory_model->updateInventory($dataset ,$data['inventory_id']);
                } 
                $insert_id = $this->db->insert_id();
                $filePath = 'uploads/Inventory_repair/'.$insert_id;
            }else{
                $data['updatedBy'] = $id;
                $save = $this->inventory_model->updateRepair($data ,$repairId);
                $filePath = 'uploads/Inventory_repair/'.$repairId;
            } 
            if($save){
                
                if(!empty($_FILES['bills']['name'][0])){
                    makeDir($filePath);
                    $number_of_files = count($_FILES['bills']['name']);
                    $files = $_FILES;

                    for($i=0; $i < $number_of_files; $i++){
                        $_FILES['bills']['name'] = $files['bills']['name'][$i];
                        $_FILES['bills']['type'] = $files['bills']['type'][$i];
                        $_FILES['bills']['tmp_name'] = $files['bills']['tmp_name'][$i];
                        $_FILES['bills']['error'] = $files['bills']['error'][$i];
                        $_FILES['bills']['size'] = $files['bills']['size'][$i];
        
                        $config['upload_path']   = $filePath;
                        $config['allowed_types'] = 'jpg|JPG|PNG|png|jpeg|JPEG|DOCX|docx|pdf|PDF';
                        $config['max_size']      = '0';
                        $config['overwrite']     = TRUE;
                        $config['remove_spaces']     = TRUE;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('bills')) {
                            $data = array('upload_data' => $this->upload->data());       
                            // $this->session->set_flashdata('success', 'Inventory added successfully!');
                            // redirect(base_url().'inventory/all_inventory');                                                           
                        } else {
                            $error = array('error' => $this->upload->display_errors());    
                            // $this->session->set_flashdata('error', 'Error! Please check the bills and try again');
                            // redirect(base_url().'inventory/all_inventory');         
                        }
                    }
                }

                if($repairId == 0){
                    $this->session->set_flashdata('success', 'Repair added successfully!');
                    redirect(base_url().'inventory/repairInventory');
                }else{
                    $this->session->set_flashdata('success', 'Inventory Updated successfully!');
                    redirect(base_url().'inventory/repairInventory');
                } 
                
            }else{
                if($repairId == 0){
                    $this->session->set_flashdata('error', 'Error in adding inventory to repair!');
                    redirect(base_url().'inventory/repairInventory');
                }else{
                    $this->session->set_flashdata('error', 'Error in updating inventory to repair!');
                    redirect(base_url().'inventory/repairInventory');
                }
            }
        }
    }

    function deleteRepair(){
        $id = $this->input->post('id');
        $getrepairDetails = $this->inventory_model->getRepairs($id) ;
        if($getrepairDetails->repairStatus == 0){
            $result = $this->inventory_model->deleteRepairById($id);
            if($result){ 
                echo 1;
                // $filePath = FCPATH.'/uploads/Inventory_repair/'.$id;
                // array_map('unlink', glob("$filePath/*.*"));
                // if(is_dir($filePath)){
                //     rmdir($filePath);
                // }
            }else{
                echo 2;
            }

        }else{
            echo 3;
        }
    }
    function changeRepairStatus(){
        $repairStatus = $this->input->post('repairStatus');
        
        $getRepairDetails = $this->inventory_model->getRepairs($_POST['repairId']);
        $checkInventoryStatus = $this->inventory_model->getInventoryById($getRepairDetails->inventory_id);
        
        if($checkInventoryStatus->assign_status != 1){
            $data['repairStatus'] = $repairStatus;        
            $updateRepair = $this->inventory_model->updateRepair($data , $_POST['repairId']);
                    
            $dataset['assign_status'] = $repairStatus;
            $changeInventoryStatus = $this->inventory_model->updateInventory($dataset, $getRepairDetails->inventory_id);
            if($changeInventoryStatus){
                echo 1;
            }else{
                echo 2;
            }

        }else{
            echo 3;
        }


    }
}   
?>