<?php 
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39 && $role_id != 15 && $role_id != 20){
	header('Location: '.base_url());
}
?>
<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading">Inventory Repair List</header> 

            <?php if($userDepartmentId == 16){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="editable-sample_length" class="dataTables_length">          
                                <div class="btn-group">
                                    <a href="<?php echo base_url()?>inventory/addRepair">
                                        <button class="btn btn-info">Add repair <i class="fa fa-plus"></i> </button>
                                    </a>
                                </div> 
                                <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox">
                                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                    <p id="appendError"></p>
                                </div>
                                <?php if($this->session->flashdata('error')){?>
                            <div class="alert alert-block alert-danger fade in margin-top">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
                            </div>
                        <?php } ?>

                        <?php if($this->session->flashdata('success')){?>
                            <div class="alert alert-success fade in margin-top">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                            </div>
                        <?php }?>                             
                        </div>
                    
                    </div>
                </div>
                        <?php }?>            
                <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="repairTable" class="repairTable">
                        <thead>
                            <tr>
                                <th>Inventory category</th>
                                <th>Tag</th>
                                <th>Details</th>
                                <th>Repair date</th>
                                <th>Expected completion date</th>
                                <th>Repair type</th>      
                                <th>Vendor</th> 
                                <th>Total expense</th>
                                <th>Status</th>     
                                <th>Created at</th>
                                <th>Updated at</th>
                                <?php if($userDepartmentId == 16){echo "<th>Action</th>";}?>
               
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($getRepairs as $row){?>
                                <tr>
                                <td><?php echo $row->cat_name;?></td>
                                <td><?php echo $row->tag;?></td>
                                <td><?php echo $row->details;?></td>
                                <td><?php echo date('d-M-y',strtotime($row->repair_date));?></td>
                                <td><?php if($row->completion_date != null){echo date('d-M-y',strtotime($row->completion_date));}else{echo "---";}?></td>
                                <td><?php if($row->details = 1){echo "Minor";}else{"Major";}?></td>      
                                <td><?php echo $row->vendor_name;?></td>
                                <td><?php if($row->currency==1){echo '₹ ';}elseif($row->currency==2){echo '$ ';}else{echo "£ ";}?><?php echo $row->expense?></td>
                                <td><?php if($row->repairStatus == 2){echo "<span class='red'>In-repair</span>";}else{echo "---";}?></td>      
                                <td><?php echo date('d-M-y',strtotime($row->repairCreated));?></td>
                                <td><?php echo date('d-M-y',strtotime($row->repairUpdated));?></td>
                                
                                <?php if($userDepartmentId == 16){ ?>
                                <td><a href="<?php echo base_url().'inventory/editRepair/'.$row->repair_id;?>">
                        <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip"
                            data-original-title="Edit&nbsp;Repair" title=""><i class="fa fa-pencil"></i></button>
                        </a>
                        <a onclick="deleteRepair(<?php echo $row->repair_id;?>)">
                        <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip"
                            data-original-title="Delete&nbsp;Repair"><i class="fa fa-trash-o "></i></button>
                        </a></td>

                                <?php }?>
                            </tr>
                           <?php }?>
                        </tbody>         
                    </table>
                    </div>
                </div>
        </section>
    </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script>
var dt = $('#repairTable').DataTable({
    "aaSorting": [
      [0, "asc"]
    ]
  });

function deleteRepair(id){
    var r = confirm('Are you sure you want to delete repair?');
    if(r == true){
        $.ajax({
        url: '<?php echo base_url(); ?>inventory/deleteRepair',
        data: { id: id},
        dataType: 'text',
        type: 'POST',
        success: function (res) {
                if(res == 1){
                    $('#alertBox').removeClass('hidden alert-danger');
                    $('#alertBox').addClass('alert-success');
                    $('#appendError').html("<strong>Success! </strong>Repair details deleted successfully!");
                    window.setTimeout(function(){location.reload()},2000);
                }
                else if(res == 2){
                    $('#alertBox').removeClass('hidden alert-success');
                    $('#alertBox').addClass('alert-danger');
                    $('#appendError').html("<strong>Error! </strong>Repair details cannot be deleted!");
                }
                else{
                    $('#alertBox').removeClass('hidden alert-success');
                    $('#alertBox').addClass('alert-danger');
                    $('#appendError').html("<strong>Error! </strong>Inventory cannot be deleted from repair, please change the status to stock first!");
                }
        }
    });
    }
}
</script>