<?php
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39 && $role_id != 15 && $role_id != 20){
	header('Location: '.base_url());
}
?>
<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading">Inventory List</header>

            <?php if($userDepartmentId == '16'){?>

                <div class="row">
                    <div class="col-lg-12">
                        <div id="editable-sample_length" class="dataTables_length">
                            <div class="btn-group">
                                <a href="<?php echo base_url()?>inventory/addInventory">
                                    <button class="btn btn-info">Add Inventory <i class="fa fa-plus"></i> </button>
                                </a>
                            </div>
                            <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <p id="appendError"></p>
                            </div>
                        <?php if($this->session->flashdata('error')){?>
                            <div class="alert alert-block alert-danger fade in margin-top">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
                            </div>
                        <?php } ?>

                        <?php if($this->session->flashdata('success')){?>
                            <div class="alert alert-success fade in margin-top">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                            </div>
                        <?php }?>
                        </div>
                    </div>

                </div>
           <?php }?>




            <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered" id="inventoryTable" class="inventoryTable">
            <thead>
                <tr>
                <th>ID</th>
                <th>Inventory category</th>
                <!-- <th>Purchase date</th> -->
                <th>Renewal date</th>
                <!-- <th>Payment cycle</th> -->
                <th>Unique tag</th>
                <th>Cost price</th>
                <th>Payment status</th>
                <!-- <th>Purchase mode</th> -->
                <th>Remarks</th>
                <th>Vendor</th>
                <th>Inventory status</th>
                <th>Selling price</th>
                <!-- <th>Sold to</th>  -->
                <!-- <th>Created at</th> -->
                <!-- <th>Updated at</th> -->
                <?php if($userDepartmentId == '16'){echo "<th>Action</th>";}?>
                </tr>
            </thead>
            <tbody>
            <?php foreach($allInventory as $row){?>
                <tr class="<?php if($row->assign_status == 4){echo "red";}if($row->assign_status == 0){echo "green";}?>">
                <td><?php echo $row->inventory_id?></td>
                    <td><?php echo $row->cat_name;?></td>
                    <!-- <td><?php echo date('d-M-y',strtotime($row->purchase_date));?></td> -->
                    <td><?php if($row->renewal_date != null){echo date('d-M-y',strtotime($row->renewal_date));}else{echo "---";}?></td>
                    <!-- <td><?php if($row->payment_cycle == 0){echo "<i>...</i>";}else{ if($row->payment_cycle=="1"){echo "Monthly";}elseif($row->payment_cycle=="2"){echo "Quaterly";}elseif($row->payment_cycle=="3"){echo "Half yearly";}elseif($row->payment_cycle=="4"){echo "Yearly";}else{echo "Lifetime";} }?></td> -->
                    <td><?php echo $row->tag;?></td>
                    <td><?php if($row->cp_currency==1){echo '₹ ';}elseif($row->cp_currency==2){echo '$ ';}else{echo "£ ";}?><?php echo $row->cost_price?></td>
                    <td><?php if($row->payment_status == 1){echo "Cleared";}else{echo "Pending";}?></td>
                    <!-- <td><?php if($row->purchase_mode == 1){echo "Online";}else{echo "Offline";}?></td> -->

                    <td><?php if($row->inventRemark != ''){echo $row->inventRemark;}else{echo "---";} ?></td>

                    <td><?php echo $row->vendor_name;?></td>
                    <td><?php if($row->assign_status == 0){echo "Stock";}elseif($row->assign_status==1){echo "In-use";}elseif($row->assign_status==2){echo "Repair";}elseif($row->assign_status == 3){echo "Out for selling";}elseif($row->assign_status == 4){echo "Sold";}else{echo "Faulty";}?></td>


                    <td><?php if($row->selling_price !=null || $row->selling_price !=''){
                        if($row->sp_currency==1){echo '₹ ';}elseif($row->sp_currency==2){echo '$ ';}else{echo "£ ";}
                        echo $row->selling_price;
                     }else{echo "<i>---</i>"; }?></td>

                    <!-- <td><?php if($row->soldTo !=0){
                        $soldto = (getVendorByid($row->soldTo));
                        print_r($soldto->vendor_name);
                    }else{echo "<i>not available</i>"; }?></td> -->



                    <!-- <td><?php echo date('d-M-y',strtotime($row->inventCreatedAt));?></td> -->
                    <!-- <td><?php //echo date('d-M-y',strtotime($row->updated_at));?></td> -->
                    <?php if($userDepartmentId == '16'){?>
                            <td>
                            <a id="assignedUser" class="assignedUser <?php if($row->assign_status != 1){echo "hidden";}?>" data-id="<?php echo $row->inventory_id;?>">
                                <button class="btn btn-info btn-xs tooltips" data-toggle="tooltip"
                                    data-original-title="View&nbsp;assignee" title=""><i class="fa fa-eye"></i></button>
                                </a>
                                <a href="<?php echo base_url().'inventory/editInventory/'.$row->inventory_id;?>">
                                <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip"
                                    data-original-title="Edit&nbsp;Inventory" title=""><i class="fa fa-pencil"></i></button>
                                </a>
                            <?php if($row->assign_status !=1 && $row->assign_status != 2){?>
                                <a onclick="deleteInventory(<?php echo $row->inventory_id;?>)">
                                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip"
                                    data-original-title="Delete&nbsp;Inventory"><i class="fa fa-trash-o "></i></button>
                                </a>

                           <?php }?>
                            </td>
                   <?php }?>
                </tr>

           <?php }?>
            </tbody>
            </table>
            </div>
            </div>
        </section>
    </section>
</section>
<!-- Modal -->
<div class="modal fade" id="userAssignModal" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assigned Inventory</h4>
          </div>
          <div class="modal-body">
                  <p></p>
          </div>

        </div>
      </div>
    </div>
    <!-- End Modal -->

<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.js"></script>
<script>
$(".assignedUser").click(function(){
    var id = $(this).data('id');
    $.ajax({
        url: '<?php echo base_url(); ?>inventory/getUserAssignedHardware',
        data: { id: id },
        dataType: 'text',
        type: 'POST',
        success: function (res) {
            $('.modal-body p').html('');
            var obj = JSON.parse(res);
            if(res != 0){
                var name =  obj[0].first_name +' '+ obj[0].last_name;
                $('.modal-body p').html('Inventory is assigned to '+'<b>'+name+'</b>');
            }else{
                $('.modal-body p').html('<i>Inventory is not assigned to anyone!</i>');
            }
            $('#userAssignModal').modal('show');
            }
        });


})
$('#inventoryTable').DataTable({
    "aaSorting": [
      [0, "desc"]
    ]
  });

  function deleteInventory(id){
      var r = confirm("Are you sure you want to delete this inventory");
      if(r == true){
            $.ajax({
                url: '<?php echo base_url(); ?>inventory/deleteInventory',
                data: { id: id },
                dataType: 'text',
                type: 'POST',
                success: function (res) {
                    if (res == 1) {
                        $('#alertBox').removeClass('hidden alert-danger');
                        $('#alertBox').addClass('alert-success');
                        $('#appendError').html("<strong>Success! </strong>Inventory deleted successfully!");
                    } else {
                        $('#alertBox').removeClass('hidden alert-success');
                        $('#alertBox').addClass('alert-danger');
                        $('#appendError').html("<strong>Error! </strong>there was a problem deleting inventory!");
                    }
                        window.setTimeout(function(){location.reload()},2000);
                    }
                });

      }
  }
</script>
