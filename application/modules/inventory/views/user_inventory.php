<?php 
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39 && $role_id != 15 && $role_id != 20){
	header('Location: '.base_url());
}
?>
<section id="main-content">
    <section class="wrapper site-min-height">
    <?php if($userDepartmentId == 16){ ?>
        <section class="panel">
            <header class="panel-heading">Assign Inventory</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <select name="allUsers" class="allUsers form-control" id="allUsers">
                            <option value="">--select user--</option>
                            <?php foreach($allActiveUsers as $row){ ?>

                                <option value="<?php echo $row->user_id;?>"><?php echo $row->first_name.' '.$row->last_name;?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <select id="selectedInventory" name="selectedInventory[]" required class="selectpicker selectedInventory" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">                             
                            <?php foreach($getInventoryByFilter as $row){ ?>
                                <option value="<?php echo $row->inventory_id;?>"><?php echo $row->cat_name.' ('.$row->tag.')';?></option>
                            <?php }?>

                        </select>
                    </div>
                </div>
                <button name="assign" class="btn btn-success btn-sm mb-2 margin" id="assign">Assign</button>
            </div>
        </section>

   <?php }?>
        <section class="panel">
            <header class="panel-heading">User Inventory list</header>
            <div class="panel-body">
            <div class="row">
                  <div class="col-sm-12">
                        <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <p id="appendError"></p>
                        </div>
                  </div>              
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <select name="employees" id="employees" class="employees">
                        <option value="">--Select employee--</option>
                        <?php foreach($allActiveUsers as $row){ ?>
                            <option value="<?php echo $row->first_name.' '.$row->last_name;?>"><?php echo $row->first_name.' '.$row->last_name;?></option>
                        <?php }?>
                                        
                    </select>                
                </div>                
            </div>                
            <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered" id="hardwareTable" class="hardwareTable">
            <thead>
                <tr>
                    <th>User name</th>
                    <th>Department</th>
                    <th>Inventory assigned</th>
                    <th>Inventory tag</th>
                    <?php if($userDepartmentId == 16){ 
                        echo "<th>Action</th>";                                                
                        }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($getAllAssignedUsers as $row){ ?>
                    <tr>
                    <td><?php echo $row->first_name.' '.$row->last_name;?></td>
                    <td><?php echo $row->department_name;?></td>
                    <td><?php echo $row->cat_name;?></td>
                    <td><?php echo $row->tag;?></td>
                    <?php if($userDepartmentId == 16){ ?>
                    
                    <td>
                    <a onclick="deleteInventory(<?php echo $row->assign_id?>, <?php echo $row->inventory_id;?>)">
                        <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip"
                            data-original-title="Unassign&nbsp;inventory"><i class="fa fa-trash-o "></i></button>
                        </a>
                    </td>

                    <?php }?>
                    </tr>

               <?php }?>
            </tbody> 
            </table>
            </div>
            </div>
        </section>
    </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/select2.min.css" />
<link href="<?php  echo base_url();?>assets/css/bootstrap-select.css" rel="stylesheet">

<script src="<?php  echo base_url();?>assets/js/jquery.js"></script>
<script src="<?php  echo base_url();?>assets/js/bootstrap-select.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script> -->
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/select2.min.js"></script>

<script>

function deleteInventory(assign_id, inventory_id){
    var r = confirm("Are you sure you want to unassign inventory of the user");
    if(r == true){
        $.ajax({
            url: '<?php echo base_url(); ?>inventory/removeAjaxAssignedInventory',
            data: { assignId:assign_id , inventory_id:inventory_id},
            type: 'POST',
            success: function(data){  
                if(data == 1){
                    $('#alertBox').removeClass('hidden alert-danger');
                    $('#alertBox').addClass('alert-success');
                    $('#appendError').html("<strong>Success! </strong>Inventory unassingned successful!");
                    window.setTimeout(function(){location.reload()},2000);                            
                }
                else{
                    $('#alertBox').removeClass('hidden alert-success');
                    $('#alertBox').addClass('alert-danger');
                    $('#appendError').html("<strong>Error! </strong>there was a problem in unassigning the inventory!");
                }
            }
        });

    }

}

var dt = $('#hardwareTable').DataTable({
    "aaSorting": [
      [0, "asc"]
    ]
  });
  $('select#employees').change( function() {
            dt.columns(0).search($(this).val()).draw();
	} );
$(document).ready(function() {
    $('.selectedInventory').on('change', function(){
		$('.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn)').css("width","auto").css('max-width','100%');
		$(".filter-option").css("height","auto").css("white-space","normal");
	});

    $('#assign').click(function(){
        var userId = $('#allUsers').val();
        var selectedInventory = $('#selectedInventory').val();
        $.ajax({
            url: '<?php echo base_url(); ?>inventory/assignAjaxInventory',
            data: { userId:userId,  selInventory:selectedInventory},
            type: 'POST',
            success: function(data){
                if(data == 1){
                    $('#alertBox').removeClass('hidden alert-danger');
                    $('#alertBox').addClass('alert-success');
                    $('#appendError').html("<strong>Success! </strong>Inventory assigned successfully!");
                    window.setTimeout(function(){location.reload()},2000); 
                }
                else{
                    $('#alertBox').removeClass('hidden alert-success');
                    $('#alertBox').addClass('alert-danger');
                    $('#appendError').html("<strong>Error! </strong>there is some technical problem in assigning inventory!");
                }
            }
        });
    });
});


// For Select2
$('#allUsers').select2();
$('#employees').select2();
</script>