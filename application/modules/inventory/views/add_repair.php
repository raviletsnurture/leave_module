<?php 
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39){
	header('Location: '.base_url());
}
?>
<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading"><?php if(isset($id)){echo "Update Inventory Repair Details";}else{"Inventory Repair Details";}?></header> 
                <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                            <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <p id="appendError"></p>
                            </div>
                    </div>
                </div>
                <form enctype="multipart/form-data" id="repairForm" method="post" action="<?php echo base_url().'inventory/addRepairData' ?>" >
                <div class="form-group">
                    <label class=" control-label">Inventory Category <span class="red">*</span></label>
                        <div class="row">
                            <div class="col-sm-12 flex-view" id="appendData">
                            <?php if(isset($id) && isset($InventoryCategories) && $InventoryCategories != null){?>
                                <?php $x=1;                                                                   
                                  foreach($InventoryCategories as $row){ 
                                 
                                        if($row->parent_cat==0){
                                            $parent_cat = getInventoryCatByParentCat($row->parent_cat);?> 
                                        <select required id="parentCat" class="form-control parentCat max-width" onchange="getCategoryId(this);" name="parentCat">
                                            <option value="">--Select category--</option>
                                            <?php foreach($parent_cat as $row2){?>
                                                <option <?php if($row2->cat_id == $row->cat_id){echo "selected";}?> value="<?php echo $row2->cat_id;?>"><?php echo $row2->cat_name;?></option>
                                            <?php  }?>
                                        </select>
                                        <?php }else{ ?>
                                        <select style="margin-top:10px;" class="form-control max-width <?php echo $x;?>" onchange="getCategoryId(this);" name="subCat[]" id="<?php echo $x;?>">
                                            <?php $parent_cat = getInventoryCatByParentCat($row->parent_cat);?>
                                                <option value="">--Select category--</option>
                                        <?php foreach($parent_cat as $row2){ ?>
                                                <option  <?php if($row2->cat_id == $row->cat_id){echo "selected";}?> value="<?php echo $row2->cat_id?>"><?php echo $row2->cat_name?></option>
                                        <?php }?>
                                        </select>
            
                                    <?php $x++; } 
                                    }?>                                                            
                          <?php }else{?>
                                <select required id="parentCat" class="form-control parentCat max-width" onchange="getCategoryId(this);" name="parentCat">
                                        <option value="">--Select--</option>
                                        <?php if($getOnlyParentCategory != ''){
                                        foreach($getOnlyParentCategory as $row){?>
                                            <option value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>;
                                        <?php  } 
                                        }?>
                                </select> 
                            <?php  }?>                        
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Inventory Tag <span class="red">*</span></label>
                                <select required name="inventoryTag" id="inventoryTag" class="inventoryTag form-control">
                                    <option value="">--select tag--</option>
                                    <?php foreach($inventoryTags as $row){?>
                                        <option <?php if($row->tag == $getRepairById->tag){echo "selected";}?> value="<?php echo $row->inventory_id?>"><?php echo $row->tag?></option>
                                   <?php }?>
                                </select>
                            </div>     
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Repair details</label>
                               <input value="<?php if(isset($id)){ echo $getRepairById->details;}?>" type="text" name="repairDetails" class="form-control repairDetails" id="repairDetails">
                            </div>           
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label  class="control-label">Repair date <span class="red">*</span></label>
                                <input required value="<?php if(isset($id)){ echo $getRepairById->repair_date;}?>" value="" size="16" type="text" id="repairDate" name="repairDate" placeholder=" Ex. 2018-07-31 14:45" class="form_datetime form-control" readonly >
                            </div>          
                        </div>
                    </div>

                    <div class="row">
                       <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label  class="control-label">Expected Repair Completion date</label>
                                <input value="<?php if(isset($id)){ echo $getRepairById->completion_date;}?>" size="16" type="text" id="completionDate" name="completionDate" placeholder=" Ex. 2018-07-31 14:45" class="form_datetime form-control" readonly >
                            </div> 
                       </div> 
                       <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label  class="control-label">Vendor <span class="red">*</span></label>
                                <select required name="vendor" class="form-control" id="vandor">
                                    <option value="">--select vendor--</option>
                                    <?php if($allVendors != ''){
                                    foreach($allVendors as $row){?>
                                    <option <?php if(isset($id)){ if($getRepairById->vendor == $row->vendor_id){echo "selected";} }?> value="<?php echo $row->vendor_id;?>"><?php echo $row->vendor_name;?>  (<?php echo $row->phone?>)</option>;
                                    <?php  } 
                                        }?>   
                                </select>
                            
                            </div>            
                       </div> 
                       <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="form-group">
                                    <label class="control-label">Expense incurred <span class="red">*</span></label>
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-addon currency-symbol"></div>
                                        <input required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" class="form-control currency-amount" id="expense" placeholder="0.00" name="expense" value="<?php if(isset($id)){ echo $getRepairById->expense  ;}?>">
                                        <div class="input-group-addon currency-addon">
                                            <select name="currency" class="currency-selector">
                                                <option <?php if(isset($id)){ if($getRepairById->currency == 1){echo "selected";}}?> value="1" data-symbol="₹" data-placeholder="0.00">INR</option>
                                                <option <?php if(isset($id)){ if($getRepairById->currency == 2){echo "selected";}}?> value="2" data-symbol="$"   data-placeholder="0.00">Dollar</option>
                                                <option <?php if(isset($id)){ if($getRepairById->currency == 3){echo "selected";}}?> value="3" data-symbol="£" data-placeholder="0.00">Pound</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label  class="control-label">Repair type</label>
                                <select  name="repairType" class="form-control" id="repairType">        
                                    <option <?php if(isset($id)){ if($getRepairById->repair_type == 0){echo "selected";}}?> value="0">Minor</option> 
                                    <option <?php if(isset($id)){ if($getRepairById->repair_type == 1){echo "selected";}}?> value="1">Major</option> 
                                </select>
                            
                            </div>            
                       </div>                
                       <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Bills/Documents </label>
                                <input class="form-control" accept=".jpg,.jpeg,.png,.docx,.pdf" type="file" name="bills[]" multiple>
                                <i><b>Note:</b> Don't include any special charaters in the file names</i>
                                <p><i> Only jpg, png, jpeg, pdf and docx files are accepted</i></p>
                            </div>
                       </div> 

                    </div>
                    <input type="hidden" class="level" id="level" name="level" value="<?php if(isset($id)){echo $x;}else{ echo "1"; }?>"> 
                    <input type="hidden" name="getLastCat" class="getLastCat" id="getLastCat" value="0">
                    <input type="hidden" class="checkAction" value="<?php if (isset($id)){echo $id;}else{ echo "0";}?>" name="checkAction">     
                    <button type="submit" name="saveRepair" class="btn btn-success saveRepair"><?php if(isset($id)){echo "Update";}else{echo "Save";}?></button>
                        <button onclick="history.back(-1)" type="button" name="cancel" class="btn btn-info">Cancel</button>                 
                </form>
                </div>
        </section>
        <?php if(isset($id)){?>
        <section class="panel">
        <header class="panel-heading">
                 Change repair status here                                
            </header>
            <div class="panel-body">
            <div class="col-lg-2 col-md-6 col-sm-12">
                <div class="form-group">
                    <label class="control-label">Repair Status </label>
                    <select data-id="<?php echo $id;?>" class="form-control" name="repairStatus" id="repairStatus">
                        <option <?php if($getRepairById->repairStatus == 0){echo "selected";}?> value="0">Stock</option>
                        <option <?php if($getRepairById->repairStatus == 2){echo "selected";}?> value="2">Repair</option>
                    </select>
                </div>
        </div>
            </div>          
        </section>
        <?php  }?>
        <?php if(isset($id)){?>
            <section class="panel">
            <header class="panel-heading">
                 Documents/Bills for the Repair Inventory                                
            </header>
                    <div class="panel-body">
                <?php if( $getRepairById->repairBills !='' || $getRepairById->repairBills !=null){ ?>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox2">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <p id="appendError2"></p>
                        </div>
                    </div>
                </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>         
    
                                    <th>Filename</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            <?php $x=1; 
    
                                foreach($bills as $key=>$row){?>
                                    <tr>     
                
                                        <td><?php echo $x.'.  '.$row;?></td>
                                        <td>
                                        <a target="_blank" href="<?php echo base_url().'uploads/Inventory_repair/'.$id.'/'.$row;?>">
                                            <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip"
                                                data-original-title="View&nbsp;document" title=""><i class="fa fa-eye"></i></button>
                                            </a>
                                        <a data-id="<?php echo $id;?>" value="<?php echo $key;?>" data-file="<?php echo $key;?>" class="deleteFileOnly">
                                            <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip"
                                                data-original-title="Delete&nbsp;File"><i class="fa fa-trash-o "></i></button>
                                            </a>
                                        </td>
                                    </tr>

                            <?php $x++; }?>
                            </tbody>
                        </table>
               <?php  }else{
                   echo "No documents uploaded yet!";
               }?>
                    </div>

        </section>


       <?php }?>                                



    </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>

<script>
$('#repairStatus').change(function(){
    var value = $(this).val();
    var id = $(this).data('id');
    var r = confirm('Are you sure you want to change the status if the inventory?');
        if(r == true){
            $.ajax({
                url: '<?php echo base_url(); ?>inventory/changeRepairStatus',
                data: { repairStatus : value, repairId:id},
                dataType: 'text',
                type: 'POST',
                success: function (res) {         
                    if(res == 1){
                        $('#alertBox').removeClass('hidden alert-danger');
                        $('#alertBox').addClass('alert-success');
                        $('#appendError').html("<strong>Success! </strong>Status changed successfully!");
                    }
                    else if(res == 3){ 
                        $('#alertBox').removeClass('hidden alert-success');
                        $('#alertBox').addClass('alert-danger');
                        $('#appendError').html("<strong>Error! </strong>Unable to update status. Inventory is already assigned to a user!");
                    }else{
                        $('#alertBox').removeClass('hidden alert-success');
                        $('#alertBox').addClass('alert-danger');
                        $('#appendError').html("<strong>Error! </strong>Status cannot be changed! please try after sometime!");
                    }
                    window.setTimeout(function(){location.reload();},1500);
                }
            });
        }
    
});

$(document).ready(function () {
    $('#repairForm').validate({
        errorClass:'error2',
        errorElement: "p",
        rules: 
        {
            "bills[]": 
            {
                extension: "jpg|jpeg|png|docx|JPG|JPEG|PNG|DOCX|PDF|pdf"
            },
            parentCat: 
            {
                required:true
            },
            repairDate: 
            {
                required:true
            },
            inventoryTag: 
            {
                required:true
            },
            expense: 
            {
                required:true
            },
            vendor:
            {
                required:true
            }
        },
        messages:
        {
            "bills[]":
            {
                extension:'Please select only jpg, png, jpeg, pdf and docx extension files'
            },
            parentCat: 
            {
                required:"Please select a category"
            },
            repairDate: 
            {
                required:"Select repair date of the inventory"
            },
            inventoryTag: 
            {
                required:"Select inventory tag"
            },
            expense: 
            {
                required:"Insert the expense of the inventory"
            },
            vendor:
            {
                required:"Please choose a vendor"
            } 
        },
         errorPlacement: function(error, element) {
            if (element.attr("id") == "parentCat") {                                
                error.insertAfter( element.parent("div").parent("div") );	
            } 
            else if(element.attr("name") == "expense"){  
                error.insertAfter( element.parent("div") );
            }
            else {
                error.insertAfter(element);
                
            }
        }
    });
});



$(".deleteFileOnly").click(function () {
       var id = $(this).attr("data-id");
       var file = $(this).attr("data-file");
        var r = confirm('Are you sure you want to delete the file');
        if(r == true){
            $.ajax({
                url: '<?php echo base_url(); ?>inventory/deleteOnlyFileRepair',
                data: { id: id ,file:file},
                dataType: 'text',
                type: 'POST',
                success: function (res) {                     
                    if(res == 1){
                        $('#alertBox2').removeClass('hidden alert-danger');
                        $('#alertBox2').addClass('alert-success');
                        $('#appendError2').html("<strong>Success! </strong>File removed successfully!");
                        window.setTimeout(function(){location.reload()},2000);
    
                    }
                    else{
                        $('#alertBox2').removeClass('hidden alert-danger');
                        $('#alertBox2').addClass('alert-success');
                        $('#appendError2').html("<strong>Success! </strong>File cannot be removed!");
                    }
                }
            });
        }
       
    });




function getCategoryId(data){
  var catVal = (data.value); 
  inventoryTag(catVal);
  $('.getLastCat').val(parseInt(catVal));
  common(data.value,data.id);       
}
function common(id,ids){
 if(id != ''){
   var level = parseInt($('.level').val());      
    $.ajax({
      url: '<?php echo base_url(); ?>inventory/getAjaxSubcategory',
      data: { id: id ,level:level},
      dataType: 'text',
      type: 'POST',
      success: function (res) {
          
        $('#appendData select').each(function() {  
              var currSelectValue = $(this).attr('id');                                                  
              if(ids=='parentCat'){
                $('select').nextAll('select').remove();
              }
              if(parseInt(ids) < parseInt(currSelectValue)){                      
                $("."+currSelectValue).remove();  
              }              
          });   
        
        if(res != ''){ 
          $('#appendData select').each(function() {
            var currSelectValue = parseInt($(this).attr('id'));
            if(level != currSelectValue){
              $('.level').val(parseInt(level)+1);
            }            
          });                 
          $('#appendData').append(res);   
        }
      }
    });
 }
 else{  
      $('#appendData select').each(function() {                 
            var currSelectValue = $(this).attr('id');
            if(ids=='parentCat'){
              $('select').nextAll('select').remove();
            }
            if(parseInt(ids) < parseInt(currSelectValue)){                       
              $("."+currSelectValue).remove();
            }             
        });
    }
}
function inventoryTag(data){  
    $.ajax({
            url: '<?php echo base_url(); ?>inventory/getAjaxAllTag',
            data: { id: data},
            dataType: 'text',
            type: 'POST',
            success: function (res) {                
                $('.inventoryTag').empty();
                $('.inventoryTag').html(res);
            }
        });  
    
}
$("#repairDate").datetimepicker({
    endDate: new Date,
    autoclose: true
});
$("#completionDate").datetimepicker({
    startDate: new Date,
    autoclose: true
}); 

function updateSymbol(e){
  var selected = $(".currency-selector option:selected");
  $(".currency-symbol").text(selected.data("symbol"))
  $(".currency-amount").prop("placeholder", selected.data("placeholder"))
  $('.currency-addon-fixed').text(selected.text())
}
$(".currency-selector").on("change", updateSymbol)
updateSymbol()
</script>
