<?php 
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39){
	header('Location: '.base_url());
}
?>
<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
        
            <header class="panel-heading"><?php if(isset($id)){echo "Update Inventory";}else{echo "Add Inventory";}?></header>
            <div class="panel-body">       
                    <form enctype="multipart/form-data" id="inventoryForm" method="post" action="<?php echo base_url().'inventory/addInventoryData' ?>">
                        <div class="form-group categorySelect">
                            <label class=" control-label">Inventory Category <span class="red">*</span></label>
                                <div class="row">
                                    <div class="col-sm-12 flex-view" id="appendData">

                                    <?php if(isset($id) && isset($InventoryCategories)){ 
                                        
                                         $x=1; if($InventoryCategories != null){
                                                
                                            foreach($InventoryCategories as $row){ 
                                            
                                            if($row->parent_cat==0){
                                                $parent_cat = getInventoryCatByParentCat($row->parent_cat);?> 
                                            <select id="parentCat" class="form-control parentCat max-width " onchange="getCategoryId(this);" name="parentCat">
                                                <option value="">--Select category--</option>
                                                <?php foreach($parent_cat as $row2){?>
                                                    <option <?php if($row2->cat_id == $row->cat_id){echo "selected";}?> value="<?php echo $row2->cat_id;?>"><?php echo $row2->cat_name;?></option>
                                                <?php  }?>
                                            </select>
                                            <?php }else{ ?>
                                            <select style="margin-top:10px;" class="form-control max-width <?php echo $x;?>" onchange="getCategoryId(this);" name="subCat[]" id="<?php echo $x;?>">
                                                <?php $parent_cat = getInventoryCatByParentCat($row->parent_cat);?>
                                                <option value="">--Select category--</option>
                                            <?php foreach($parent_cat as $row2){ ?>
                                                    <option  <?php if($row2->cat_id == $row->cat_id){echo "selected";}?> value="<?php echo $row2->cat_id?>"><?php echo $row2->cat_name?></option>
                                            <?php }?>
                                            </select>
                
                                        <?php $x++; } 
                                        }?>   
    
                                    <?php }else{
                                        
                                        ?>
                                    <select required id="parentCat" class="form-control parentCat max-width" onchange="getCategoryId(this);" name="parentCat">
                                            <option value="">--Select--</option>
                                            <?php if($getOnlyParentCategory != ''){
                                            foreach($getOnlyParentCategory as $row){?>
                                                <option <?php if($row->cat_id == $inventoryById->category_id){echo "selected";}?> value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>;
                                            <?php  } 
                                                }?>
                                        </select>
                                <?php  }?>                         

                                  <?php  }else{
                                      
                                      ?>
                                    <select required id="parentCat" class="form-control parentCat max-width" onchange="getCategoryId(this);" name="parentCat">
                                        <option value="">--Select--</option>
                                        <?php if($getOnlyParentCategory != ''){
                                        foreach($getOnlyParentCategory as $row){?>
                                            <option value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>;
                                        <?php  } 
                                            }?>
                                    </select> 
                                <?php  }?>

                                    </div>
                                </div>
                        </div>
                        <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group">
                                    <label required class="control-label">Date Of Purchase <span class="red">*</span></label>
                                    <input value="<?php if(isset($id)){ echo $inventoryById->purchase_date;}?>" size="16" type="text" id="purchaseDate" name="purchaseDate" placeholder=" Ex. 2018-07-31 14:45" class="form_datetime form-control" readonly >
                                </div>
                              </div>              
                              <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Warranty Expiration/Renewal Date</label>
                                        <input value="<?php if(isset($id)){ echo $inventoryById->renewal_date;}?>" size="16" type="text" id="warrantyRenewal" name="warrantyRenewal" placeholder=" Ex. 2018-07-31 14:45" class="form_datetime form-control" readonly>
                                    </div>
                              </div>    
                        </div>
                        <div class="row <?php if(isset($id)){if($inventoryById->tag != '' || $inventoryById->tag != null){echo "hidden";}}else{echo "hidden";}?> action1">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label class="control-label">Payment Cycle (<i>only for software category</i>)</label>
                                    <select required class="form-control" name="paymentCycle" id="paymentCycle">
                                        <option <?php if(isset($id)){ if($inventoryById->payment_cycle == 0){echo "selected";}}?> value="">--select--</option> 
                                        <option <?php if(isset($id)){ if($inventoryById->payment_cycle == 1){echo "selected";}}?> value="1">Monthly</option>       
                                        <option <?php if(isset($id)){ if($inventoryById->payment_cycle == 2){echo "selected";}}?> value="2">Quaterly</option>      
                                        <option <?php if(isset($id)){ if($inventoryById->payment_cycle == 3){echo "selected";}}?> value="3">6 months</option>
                                        <option <?php if(isset($id)){ if($inventoryById->payment_cycle == 4){echo "selected";}}?> value="4">Yearly</option>       
                                        <option <?php if(isset($id)){ if($inventoryById->payment_cycle == 5){echo "selected";}}?> value="5">Lifetime</option>       
                                    </select>                            
                                </div>
                            </div>                                            
                        </div>
                        <div class="row <?php if(isset($id)){if($inventoryById->payment_cycle != '' || $inventoryById->payment_cycle != null){echo "hidden";}}else{echo "hidden";}?> action2">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Inventory Tag (<i>only for hardware category</i>) <span class="red">*</span></label>  
                                    <input value="<?php if(isset($id)){ echo $inventoryById->tag;}?>"  type="text" class="form-control inventoryTag" id="inventoryTag" name="inventoryTag">  
                                </div>                              
                                <button type="button" class="btn btn-info btn-sm mb-2 generateTag" name="generateTag">Generate tag</button>                           
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Inventory Price <span class="red">*</span></label>
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-addon currency-symbol"></div>
                                        <input required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" class="form-control currency-amount" id="" placeholder="0.00" name="costPrice" value="<?php if(isset($id)){ echo $inventoryById->cost_price;}?>">
                                        <div class="input-group-addon currency-addon">
                                            <select name="costCurrency" class="currency-selector">
                                                <option <?php if(isset($id)){ if($inventoryById->cp_currency == 1){echo "selected";}}?> value="1" data-symbol="₹" data-placeholder="0.00">INR</option>
                                                <option <?php if(isset($id)){ if($inventoryById->cp_currency == 2){echo "selected";}}?> value="2" data-symbol="$" data-placeholder="0.00">Dollar</option>
                                                <option <?php if(isset($id)){ if($inventoryById->cp_currency == 3){echo "selected";}}?> value="3" data-symbol="£" data-placeholder="0.00">Pound</option>                                    
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div></div>                
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                        <label class=" control-label">Payment Status </label>                
                                        <select id="paymentStatus" class="form-control paymentStatus" name="paymentStatus">
                                            <option <?php if(isset($id)){ if($inventoryById->payment_status == 1){echo "selected";}}?> value="1">Cleared</option>                 
                                            <option <?php if(isset($id)){ if($inventoryById->payment_status == 2){echo "selected";}}?> value="2">Pending</option>
                                        </select>                                               
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 checkAction1 <?php if(isset($id)){if(!in_array(2, $hasDetail)){echo "hidden";}}else{echo "hidden";}?> ">
                                    <div class="form-group">
                                        <label class="control-label">Serial no.</label>
                                        <input value="<?php if(isset($id)){echo $inventoryById->serial;}?>" type="text" class="form-control serial" id="serial" name="serial">
                                    </div>
                            </div>
                            <div class="col-sm-6 checkAction2 <?php if(isset($id)){if(!in_array(1, $hasDetail)){echo "hidden";}}else{echo "hidden";}?>">
                            <div class="form-group ">
                                        <label class="control-label">MAC ID.</label>
                                        <input value="<?php if(isset($id)){echo $inventoryById->mac_id;}?>" type="text" class="form-control mac" id="mac" name="mac">
                                    </div>
                            </div>
                        </div>            
                        <div class="row">
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Purchase Mode</label>  
                                    <select required id="purchaseMode" class="form-control purchaseMode" name="purchaseMode">        
                                        <option value="">--select purchase mode---</option>
                                        <option <?php if(isset($id)){ if($inventoryById->purchase_mode == 1){echo "selected";}}?> value="1">Offline</option>
                                        <option <?php if(isset($id)){ if($inventoryById->purchase_mode == 2){echo "selected";}}?> value="2">Online</option>
                                    </select>                                                                            
                                </div>
                             </div>
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Bills/Documents </label>
                                    <input class="form-control" accept=".jpg,.jpeg,.png,.docx,.pdf" type="file" name="bills[]" multiple>
                                    <i><b>Note:</b> Don't include any special charaters in the file names. </i>
                                    <p><i> Only jpg, png, jpeg, pdf and docx files are accepted</i></p>
                                </div>
                             </div>                   
                        </div>
                        <div class="row">
                             
                             <div class="col-sm-<?php if(!isset($id)){echo "6";}else{echo "12";}?>">
                                <div class="form-group">
                                    <label class="control-label">Remarks</label>                
                                    <input value="<?php if(isset($id)){ echo $inventoryById->inventRemark;}?>" type="text" class="form-control remarks" id="remarks" name="remarks">
                                </div>
                             </div>                   
                        <!-- </div>

                        <div class="row"> -->
                             <div class="col-sm-<?php if(isset($id)){echo "6";}else{echo "6";}?>">
                                <div class="form-group">
                                    <label class="control-label">Vendor (<i>first select purchase mode</i>) <span class="red">*</span></label>  
                                    <select required id="vendor" class="form-control vendor" name="vendor">
                                        <option value="">--select vendor--</option>
                                        <?php if(isset($id)){
                                            $getVendor = getVendorByType($inventoryById->purchase_mode);
                                            
                                            foreach($getVendor as $row){?>
                                                <option <?php if($inventoryById->vendor == $row->vendor_id){echo "selected";}?> value="<?php echo $row->vendor_id;?>"><?php echo $row->vendor_name;?></option>
                                           <?php }                
                                        }?>
                                    </select>                            
                                </div>
                             </div>
                             <?php if(isset($id)){?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Inventory Status</label>  
                                        <select <?php if($inventoryById->assign_status == 1 || $inventoryById->assign_status == 2){echo "disabled";}?> id="inventoryStatus" class="form-control inventoryStatus" name="inventoryStatus">
                                            <?php if(isset($id)){?>
                                            <option <?php if(isset($id)){ if($inventoryById->assign_status == 0){echo "selected";}}?> value="0">Stock</option>
                                        <?php }?>
                                            <?php if($inventoryById->assign_status == 1|| $inventoryById->assign_status == 2){?>
                                            <option <?php if(isset($id)){ if($inventoryById->assign_status == 1){echo "selected";}}?> value="1">In-use</option>
                                            <option <?php if(isset($id)){ if($inventoryById->assign_status == 2){echo "selected";}}?> value="2">Repair</option>
                                          <?php  }?>    
                                            <option <?php if(isset($id)){ if($inventoryById->assign_status == 3){echo "selected";}}?> value="3">Out for selling</option>
                                            <option <?php if(isset($id)){ if($inventoryById->assign_status == 4){echo "selected";}}?> value="4">Sold</option>
                                            <option <?php if(isset($id)){ if($inventoryById->assign_status == 5){echo "selected";}}?> value="5">Faulty</option>
                                        </select>              
                                    </div>
                                </div>                   
                            <?php }?>
                        </div>

                        <div class="row <?php if(isset($id)){ if($inventoryById->assign_status != 4){echo"hidden";}}else{echo "hidden";}?> action3">
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Inventory Selling Price</label>
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-addon currency-symbol2"></div>
                                        <input required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" class="form-control currency-amount2" id="sellingPrice" placeholder="0.00" name="sellingPrice" value="<?php if(isset($id)){ echo $inventoryById->selling_price;}?>">
                                        <div class="input-group-addon currency-addon">
                                            <select name="saleCurrency" class="currency-selector2">
                                                
                                                <option  <?php if(isset($id)){ if($inventoryById->sp_currency == 1){echo "selected";}}?> value="1" data-symbol="₹" data-placeholder="0.00">INR</option>
                                                <option <?php if(isset($id)){ if($inventoryById->sp_currency == 2){echo "selected";}}?> value="2" data-symbol="$" data-placeholder="0.00">Dollar</option>
                                                <option <?php if(isset($id)){ if($inventoryById->sp_currency == 3){echo "selected";}}?> value="3" data-symbol="£" data-placeholder="0.00">Pound</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                             </div>
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Sold to</label>  
                                    <select required id="soldTo" class="form-control soldTo" name="soldTo">
                                        <option value="">--select--</option>

                                        <?php foreach($allVendors as $row){?>                                        
                                             <option <?php if(isset($id)){ 
                                                 if($inventoryById->soldTo !=0){
                                                     if($inventoryById->soldTo == $row->vendor_id){echo "selected";}}
                                                 }?> value="<?php echo $row->vendor_id?>"><?php echo $row->vendor_name?></option>;
                                      <?php }?>
                                       
                                    </select>              
                                </div>
                             </div>                   
                        </div>

                        <input type="hidden" class="level" id="level" name="level" value="<?php if(isset($id)){echo $x;}else{ echo "1";}?>"> 

                        <input type="hidden" class="operationStatus" name = "operationStatus" value="<?php if(isset($id)){echo "1";}else{ echo "0";}?>">
                        <input type="hidden" class="detailSet" value = "0">                         
                        <input type="hidden" class="checkAction" value="<?php if (isset($id)){echo $id;}else{ echo "0";}?>" name="checkAction">
                        <button type="submit" name="saveInventory" class="btn btn-success saveInventory"><?php if(isset($id)){echo "Update";}else{echo "Save";}?></button>
                        <button onclick="history.back(-1)" type="button" name="cancel" class="btn btn-info">Cancel</button>                            
                    </form>
            </div>                        
        </section>
        <?php if(isset($id)){?>
            <section class="panel">
            <header class="panel-heading">
                 Documents/Bills for the Inventory                                
            </header>
                    <div class="panel-body">
                <?php if( $inventoryById->bills !='' || $inventoryById->bills !=null){ ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox2">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <p id="appendError2"></p>
                            </div>
                        </div>
                    </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>         
    
                                    <th>Filename</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            <?php $x=1; foreach($bills as $key=>$row){?>
                                <tr>     
            
                                    <td><?php echo $x.'.  '.$row;?></td>
                                    <td>
                                    <a target="_blank" href="<?php echo base_url().'uploads/inventory/'.$id.'/'.$row;?>">
                                        <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip"
                                            data-original-title="View&nbsp;document" title=""><i class="fa fa-eye"></i></button>
                                        </a>
                                    <a data-id="<?php echo $id;?>" value="<?php echo $key;?>" data-file="<?php echo $key;?>" class="deleteFileOnly">
                                        <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip"
                                            data-original-title="Delete&nbsp;File"><i class="fa fa-trash-o "></i></button>
                                        </a>
                                    </td>
                                </tr>

                        <?php $x++; }?>
                            </tbody>
                        </table>
               <?php  }else{
                   echo "No documents uploaded yet!";
               }?>
                    </div>

        </section>


       <?php }?>

    </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
<script>
$( "#appendData select" ).last().trigger("change");
$('.generateTag').click(function(){
    var inventoryForm = $('#inventoryForm').serialize();
    $(this).html('<i class="fa fa-refresh fa-spin">');
    $.ajax({
            url: '<?php echo base_url(); ?>inventory/getTag',
            data: { inventoryForm: inventoryForm},
            dataType: 'text',
            type: 'POST',
            success: function (res) {       
                var obj = JSON.parse(res);
                $('#inventoryTag').val('');                                  
                $('#inventoryTag').val(obj);
                $('.generateTag').html('Generate tag');
            }
        });
    
    
})
$(document).ready(function () {
    $('#inventoryForm').validate({
        errorClass:'error',
        errorElement: "p",
        rules: 
        {
            "bills[]": 
            {
                extension: "jpg|jpeg|png|docx|JPG|JPEG|PNG|DOCX|PDF|pdf"
            },
            parentCat: 
            {
                required:true
            },
            purchaseDate: 
            {
                required:true
            },
            purchaseMode:
            {
                required: true
            },
            inventoryTag: 
            {
                required:true
            },
            costPrice: 
            {
                required:true
            },
            vendor:
            {
                required:true
            }
        },
        messages:
        {
            "bills[]":
            {
                extension:'Please select only jpg, png, jpeg, pdf and docx extension files'
            },
            purchaseMode :
            {
                required : "You have to select the purchase mode"
            },
            parentCat: 
            {
                required:"Please select a category"
            },
            purchaseDate: 
            {
                required:"Select purchase date of the inventory"
            },
            inventoryTag: 
            {
                required:"Insert an inventory tag"
            },
            costPrice: 
            {
                required:"Insert cost price of the inventory"
            },
            vendor:{
                required:"You have to select vendor"
            } 
        },
         errorPlacement: function(error, element) {
            if (element.attr("id") == "parentCat") {                                
                error.insertAfter( element.parent("div").parent("div") );	
            } 
            else if(element.attr("name") == "costPrice"){  
                error.insertAfter( element.parent("div") );
            }
            else {
                error.insertAfter(element);
                
            }
        }
    });
});
$(".deleteFileOnly").click(function () {
       var id = $(this).attr("data-id");
       var file = $(this).attr("data-file");
        var r = confirm('Are you sure you want to delete the file');
        if(r == true){
            $.ajax({
                url: '<?php echo base_url(); ?>inventory/deleteOnlyFile',
                data: { id: id ,file:file},
                dataType: 'text',
                type: 'POST',
                success: function (res) {
                    if(res == 1){
                        $('#alertBox2').removeClass('hidden alert-danger');
                        $('#alertBox2').addClass('alert-success');
                        $('#appendError2').html("<strong>Success! </strong>File removed successfully!");
                        window.setTimeout(function(){location.reload()},2000);
                    }
                    else{
                        $('#alertBox2').removeClass('hidden alert-danger');
                        $('#alertBox2').addClass('alert-success');
                        $('#appendError2').html("<strong>Success! </strong>File cannot be removed!");       
                    }
                }
            });
        }
       
    });


$('.parentCat').change(function(){
    var value = $(this).val();
    var text = $('.parentCat option:selected').html()
    if( text == "Software" || text == "SOFTWARE" || text == "software"){
        $(".action1").removeClass('hidden');
        $(".action2").addClass('hidden');      
    }
    else{
        $(".action2").removeClass('hidden');
        $(".action1").addClass('hidden');
    }
    if(value == null || value == ''){
        $(".action2").addClass('hidden');
        $(".action1").addClass('hidden');
    }
});

$('.purchaseMode').change(function(){
    var value = $(this).val();
    $.ajax({
        url: '<?php echo base_url(); ?>inventory/getAjaxVendor',
        data: { id: value},
        dataType: 'text',
        type: 'POST',
        success: function (res) {
                $('#vendor').empty();
                $('#vendor').html(res);
        }
    });

});
$('.inventoryStatus').change(function(){
    var value = $(this).val();
    if(value == 4){
        $(".action3").removeClass('hidden');     
    }
    else{
        $(".action3").addClass('hidden');
    }
});
function getCheck(value){
    $.ajax({
      url: '<?php echo base_url(); ?>inventory/getAjaxCategory',
      data: { id: value},
      dataType: 'text',
      type: 'POST',
      success: function (res) {          
        var obj = JSON.parse(res);        
        if(obj != '' ){
                if( obj['hasDetail']!= null){
                    var items = obj['hasDetail'].split(', ');
                    if(jQuery.inArray("2", items) !== -1){
                        $(".checkAction1").removeClass("hidden");
                    }else{
                        $(".checkAction1").addClass("hidden");
                    }
                    if(jQuery.inArray("1", items) !== -1){
                        $(".checkAction2").removeClass("hidden");            
                    }
                    else{   
                        $(".checkAction2").addClass("hidden");
                    }
                }else{
                    $(".checkAction1").addClass("hidden");
                    $(".checkAction2").addClass("hidden");
                }            
        }else{
            var last_id = $(".categorySelect select:nth-last-child(2)").val();
            getCheck(last_id);
        }
      }
    }); 
}

function getCategoryId(data){
  getCheck(data.value);
  common(data.value,data.id);       
}
function common(id,ids){  
 if(id != ''){
   var level = parseInt($('.level').val());      
    $.ajax({
      url: '<?php echo base_url(); ?>inventory/getAjaxSubcategory',
      data: { id: id ,level:level},
      dataType: 'text',
      type: 'POST',
      success: function (res) {
        $('#appendData select').each(function() {  
              var currSelectValue = $(this).attr('id');      
              if(ids=='parentCat'){
                $('select').nextAll('select').remove();
              }
              if(parseInt(ids) < parseInt(currSelectValue)){                      
                $("."+currSelectValue).remove();  
              }              
          });   
        
        if(res != ''){ 
          $('#appendData select').each(function() {
            var currSelectValue = parseInt($(this).attr('id'));
            if(level != currSelectValue){
              $('.level').val(parseInt(level)+1);
            }            
          });                 
          $('#appendData').append(res);   
        }
      }
    });
 }
 else{  
      $('#appendData select').each(function() {                 
            var currSelectValue = $(this).attr('id');
            if(ids=='parentCat'){
              $('select').nextAll('select').remove();
            }
            if(parseInt(ids) < parseInt(currSelectValue)){                       
              $("."+currSelectValue).remove();
            }             
        });
    }
}

$("#purchaseDate").datetimepicker({
    endDate: new Date,
    autoclose: true
});
$("#warrantyRenewal").datetimepicker({
    startDate: new Date,
    autoclose: true
});   

function updateSymbol(e){
  var selected = $(".currency-selector option:selected");
  $(".currency-symbol").text(selected.data("symbol"))
  $(".currency-amount").prop("placeholder", selected.data("placeholder"))
  $('.currency-addon-fixed').text(selected.text())
}
$(".currency-selector").on("change", updateSymbol)
updateSymbol()

function updateSymbol2(e){
  var selected = $(".currency-selector2 option:selected");
  $(".currency-symbol2").text(selected.data("symbol"))
  $(".currency-amount2").prop("placeholder", selected.data("placeholder"))
  $('.currency-addon-fixed').text(selected.text())
}
$(".currency-selector2").on("change", updateSymbol2)
updateSymbol2()
</script>