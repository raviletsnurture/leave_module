<?php 
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39 && $role_id != 15 && $role_id != 20){
	header('Location: '.base_url());
}
?>
<section id="main-content">
  <section class="wrapper site-min-height">
    <!-- Vendor Section -->
    <section class="panel">
      <header class="panel-heading">Vendors</header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <?php if($userDepartmentId == '16'){?>
        <div class="row">
          <div class="col-lg-12">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <button type="button" class="btn btn-info" id="toggleModal" data-toggle="modal"
                  data-target="#vendorModal">Add vendor <i class="fa fa-plus"></i></button>
              </div>
              
              <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <p id="appendError"></p>
              </div>

            </div>
          </div>
        </div>

        <?php }?>
      </div>
      <div class="panel-body">

      <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered" id="vendorTable" class="vendorTable">
          <thead>
            <tr>
              <th>Vendor name</th>
              <th width="20%">Address</th>
              <th>Phone no</th>
              <th>Services offered</th>
              <th>Type</th>
              <th>Remarks</th>
              <th>Created at</th>
              <?php if($userDepartmentId == '16'){
                          echo "<th>Action</th>";
                        }?>
            </tr>
          </thead>
          <tbody>
            <?php foreach($getAllVendors as $row){ ?>
            <tr>
                <td><?php echo $row->vendor_name;?></td>
                <td><?php echo $row->address; ?></td>
                <td><?php echo $row->phone; ?></td>
                <td><?php if($row->service != null){ $res = explodeVendorService($row->service);print_r($res); }else{ echo "---";}?></td>
                <td><?php if($row->vendorType == '1'){echo "Offline";}else{ echo "Online";}?></td>
                <td><?php if($row->remarks!=null){echo $row->remarks;}else{echo "---";} ?></td>
                <td><?php echo date('d-M-y',strtotime($row->created_at));?></td>
                <?php if($userDepartmentId == '16'){?>
                    <td>
                        <a onclick="editVendor(<?php echo $row->vendor_id;?>)">
                          <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip"
                            data-original-title="Edit&nbsp;Vendor" title=""><i class="fa fa-pencil"></i></button>
                        </a>
                        <?php if($row->isBoughtStatus == 0){?>
                          <a onclick="deleteVendor(<?php echo $row->vendor_id;?>)" class="">
                            <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip"
                              data-original-title="Delete&nbsp;Vendor"><i class="fa fa-trash-o "></i></button>
                          </a>
                       <?php }?>
                    </td>
                <?php }?>
            </tr>
            <?php }?>
          </tbody>
        </table>
        </div>
      </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="vendorModal" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Vendor</h4>
          </div>
          <div class="modal-body">

            <form id="vendorForm" method="post">
              <div class="form-group">
                <label class="control-label">Vendor name <span class="red">*</span></label>
                <input id="vendorName" type="text" class="form-control" name="vendorName" required>
              </div>

              <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Contact person name<span class="red">*</span></label>
                          <input id="person" type="text" class="form-control" name="person" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                            <div class="form-group">
                              <label class=" control-label">Contact number</label>
                              <input placeholder="ex. 1234567890" id="phone" type="number" class="form-control" name="phone"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                    </div>
              </div>            
              

              <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Alternate contact person</label>
                          <input id="altPerson" type="text" class="form-control" name="altPerson">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class=" control-label">Alternate Contact number</label>
                          <input placeholder="ex. 1234567890" id="altPhone" type="number" class="form-control" name="altPhone"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                        </div>
                    </div>
              </div>

              <div class="form-group">
                <label class="control-label">Website</label>
                <input placeholder="ex. https://letsnurture.com" id="website" type="text" class="form-control" name="website">
              </div>

              <div class="form-group">
                <label class=" control-label">Address <span class="red">*</span></label>
                <div class="">
                  <input id="address" type="text" class="form-control" name="address" required>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Services offered <span class="red">*</span></label>
                      <select id="service" class="form-control service" name="service[]" multiple="multiple">
                        <option value="1">Hardware</option>
                        <option value="2">Software</option>
                        <option value="3">Repair</option>
                      </select>
                      <p class="appendError"></p>
                    </div>
                </div>
                <div class="col-md-6">
                      <div class="form-group">
                        <label class=" control-label">Type of vendor</label>
                        <select id="vendorType" class="form-control" name="vendorType">
                          <option value="1">Offline vendor</option>
                          <option value="2">Online vendor</option>
                        </select>
                      </div>                
                </div>
              </div>


              <div class="form-group">
                <label class=" control-label">Remarks</label>
                <textarea id="remarks" class="form-control" name="remarks" rows="3"></textarea>
              </div>
              <input type="hidden" id="vid" name="vid" value="0">
              <button type="submit" name="saveVendor" class="btn btn-success saveVendor">Save</button>
            </form>
          </div>

        </div>
      </div>
    </div>
    <!-- End Modal -->
    <!-- End section -->
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/select2.min.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/select2.min.js"></script>
<!-- <script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/js/additional-methods.min.js"></script> -->
<script type="text/javascript" charset="utf-8">
  $('#toggleModal').click(function (e) {
    e.preventDefault();
    $(".error").html('');
    $('#service').val('').trigger('change')
    $(".error").removeClass("error");
    $('#vendorForm')[0].reset();
    $('#vid').val('0');
    $('.modal-title').html('Add Vendor');
    $('.saveVendor').html('Add');
    $('#vendorModal').modal('show');
  });

  function editVendor(id) {
    $(".error").html('');
    $(".error").removeClass("error");
    $('#vendorForm')[0].reset();

    $.ajax({
      url: '<?php echo base_url(); ?>inventory/getVendor',
      data: {
        id: id
      },
      dataType: 'text', 
      type: 'POST',
      success: function (data) {        
        var obj = JSON.parse(data);
        $('#vendorName').val(obj[0].vendor_name);
        $('#phone').val(obj[0].phone);
        $('#altPerson').val(obj[0].altPerson);
        $('#website').val(obj[0].website);
        $('#person').val(obj[0].person);
        $('#altPhone').val(obj[0].altPhone);
        $('#address').val(obj[0].address);
        $('#service').select2().val(obj[0].service.split(", ")).trigger('change')
        $('#service option').filter(function () {
          return ($(this).val() == obj[0].service);
        }).prop('selected', true);
        $('#vendorType option').filter(function () {
          return ($(this).val() == obj[0].vendorType);
        }).prop('selected', true);
        $('#remarks').val(obj[0].remarks);
        $('#vid').val(obj[0].vendor_id);
        $('.modal-title').html('Update Vendor');
        $('.saveVendor').html('Update');
        $('#vendorModal').modal('show');
      }
    });
  }


  $(document).ready(function () {

    var dt = $('#vendorTable').DataTable({
      "aaSorting": [
        [0, "asc"]
      ]
    });
  });

  $(document).ready(function () {
      jQuery.validator.addMethod("alphaSpace", function(value, element) {
      return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }); 
    $('.saveVendor').click(function () {
      var validator = $('#vendorForm').validate({
        errorClass: "error",
        errorElement: "span",
        ignore: [], 
        rules: {
          vendorName: "required",
          phone: {
            number: true,
            minlength: 10
          },
          address: {
            required: true
          },
          person:{
            required : true,
            alphaSpace: true
          },
          altPerson:{
            alphaSpace: true
          },
          website:{
            url: true
          },
          "service[]":{
            required:true
          }
        },
        messages: {
          vendorName: {
            required: "Vendor name is required"
          },
          person:{
            required : "Contact person is required",
            alphaSpace : "Person name should only contain alphabets and spaces"
          },
          altPerson:{
            alphaSpace : "Person name should only contain alphabets and spaces"
          },
          phone: {       
            number: "Contact number should only contain numbers!",
            minlength: "Phone number cannot be less than 10 digits"
          },
          address: {
            required: "Address is required"
          },
          website : {
            url : "Please enter a valid URL"
          },
          "service[]":{
            required:"Select atleast one service."
          }
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "service[]"){            
            error.appendTo((".appendError") );
          }else{
            error.insertAfter(element);
          }
        },
        submitHandler: function (form, event) {
          event.preventDefault();
          var data = $(form).serialize();
          $.ajax({
            url: '<?php echo base_url(); ?>inventory/addvendor',
            data: {
              data: data
            },
            type: 'POST',
            success: function (res) {
              $('#vendorModal').modal('hide');
              if(res == 1){
                  $('#alertBox').removeClass('hidden alert-danger');
                  $('#alertBox').addClass('alert-success');
                  $('#appendError').html("<strong>Success! </strong>Vendor added successfully!");       
              }else if(res == 2){
                  $('#alertBox').removeClass('hidden alert-success');
                  $('#alertBox').addClass('alert-danger');
                  $('#appendError').html("<strong>Error! </strong>there was a problem adding vendor!");         
              }else if(res == 3){
                  $('#alertBox').removeClass('hidden alert-danger');
                  $('#alertBox').addClass('alert-success');
                  $('#appendError').html("<strong>Success! </strong>Vendor updated successfully!");
              }else{
                  $('#alertBox').removeClass('hidden alert-success');
                  $('#alertBox').addClass('alert-danger');
                  $('#appendError').html("<strong>Error! </strong>there was a problem updating vendor!");
              }
              window.setTimeout(function(){location.reload()},1500);
              
            }
          });
        }
      });
    });
  });

  function deleteVendor(id) {
    var r = confirm('Are you sure you want to delete vendor');
    if(r == true){
        $.ajax({
          url: '<?php echo base_url(); ?>inventory/deleteVendor',
          data: {
            id: id
          },
          dataType: 'text',
          type: 'POST',
          success: function (data) {
            $('#vendorModal').modal('hide');
            if (data == 1) {
              $('#alertBox').removeClass('hidden alert-danger');
              $('#alertBox').addClass('alert-success');
              $('#appendError').html("<strong>Success! </strong>Vendor deleted successfully!");
            } else {
              $('#alertBox').removeClass('hidden alert-success');
              $('#alertBox').addClass('alert-danger');
              $('#appendError').html("<strong>Error! </strong>there was a problem deleting vendor!");
            }
            window.setTimeout(function(){location.reload()},1500);
          }
        });
    }
  }

  $('.service').select2({ 
    placeholder: "Select services",
    allowClear: true,
    multiple: true,});  
</script>