<?php 
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39){
	header('Location: '.base_url());
}
?>
<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading">Update category</header>
            <div class="panel-body">
              <form id="categoryForm" method="post" action="<?php echo base_url().'inventory/updateCategoryData' ?>">
                  <div class="row">
                      <div class="col-sm-12">                      
                          <label class="control-label">Parent category</label>
                            <div class="form-group flex-view" id="appendData">
                            <?php $x=1; if(isset($InventoryCategories) && $InventoryCategories != null){          
                                  foreach($InventoryCategories as $row){
                                          if($row->parent_cat==0){
                                              $parent_cat = getInventoryCatByParentCat($row->parent_cat);?>
                                            <select id="parentCat" class="form-control parentCat max-width" onchange="getCategoryId(this);" name="parentCat">
                                              <option value="">--Select category--</option>
                                                <?php foreach($parent_cat as $row2){?>                                                                              
                                                    <option <?php if($row2->cat_id == $row->cat_id){echo "selected";}?> value="<?php echo $row2->cat_id;?>"><?php echo $row2->cat_name;?></option>  
                                                <?php  }?>
                                            </select>
                                          <?php }else{ ?>                                                                                      
                                            <select style="margin-top:10px;" class="form-control max-width <?php echo $x;?>" onchange="getCategoryId(this);" name="subCat[]" id="<?php echo $x;?>">
                                              <?php $parent_cat = getInventoryCatByParentCat($row->parent_cat);?>
                                                <option value="">--Select category--</option>
                                                <?php foreach($parent_cat as $row2){ ?>
                                                      <option <?php if($row2->cat_id == $row->cat_id){echo "selected";}?> value="<?php echo $row2->cat_id?>"><?php echo $row2->cat_name?></option>
                                                <?php }?>
                                            </select>

                                        <?php $x++; }
                                        }
                          }else{?>                                                                               
                                <select id="parentCat" class="form-control parentCat max-width" onchange="getCategoryId(this);" name="parentCat">
                                  <option value="">--Select--</option>
                                  <?php if($getOnlyParentCategory != ''){
                                      foreach($getOnlyParentCategory as $row){
                                        if($row->cat_id != $getCategory->cat_id){?>
                                          <option <?php if($row->cat_id == $getCategory->parent_cat){echo "selected";}?> value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>
                                       <?php }?>
                                  <?php  }
                                      }?>
                              </select>
                        <?php  }?>
                            </div>
                      </div>
                  </div>
    
                <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                      <label class="custom-control-label"><input <?php if(in_array(1, $hasDetail)){echo "checked";}?> type="checkbox" value="1" name="hasDetail[]">MAC</label>
                      <label class="custom-control-label"><input <?php if(in_array(2, $hasDetail)){echo "checked";}?> type="checkbox" value="2" name="hasDetail[]">Serial no.</label>
                  </div>

                </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Category name <span class="red">*</span></label>
                          <input value="<?php echo $getCategory->cat_name;?>" id="categoryName" type="text" class="form-control" name="categoryName" required>
                        </div>

                    </div>                                        
                </div>
                <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Category Description </label>
                          <input value="<?php echo $getCategory->cat_description;?>" id="catDescription" type="text" class="form-control" name="catDescription">
                        </div>
                    </div>               
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Category Tag </label>
                          <input disabled value="LN/<?php echo $getCategory->categoryTag;?>" id="categoryTag" type="text" class="form-control" name="categoryTag">
                        </div>
                    </div>
                </div>
                <input type="hidden" class="level" id="level" name="level" value="<?php if(isset($x)){echo $x;}?>">
                <input type="hidden" id="cid" name="cid" value="<?php echo $getCategory->cat_id?>">
              <button type="submit" name="updateCategory" class="btn btn-success updateCategory">Update</button>
              <button onclick="history.back(-1)" type="button" name="cancel" class="btn btn-info">Cancel</button>
            </form>
            </div>
        </section>
    </section>
</section>
<script>
$(document).ready(function(){
  $('.updateCategory').click(function(){
    jQuery.validator.addMethod("alphaSpaceNum", function(value, element) {
      return this.optional(element) || /^[a-zA-Z0-9\s\-]+$/i.test(value);
    });
    $('#categoryForm').validate({
        errorClass: "error",
        errorElement: "span",
        rules: {
          categoryName: {
            required : true,
            alphaSpaceNum:true
          },
          categoryTag : {
            required : true,
            lettersonly:true
          }         
        },
        messages: {
          categoryName: {
            required: "Category name is required.",
            alphaSpaceNum : "Category name can only contain alphabet, number and spaces."
          },
          categoryTag : {
            required : "Category tag is required.",
            lettersonly : "Category tag can only contain alphabets."
          }
        }
    });
  });
});

$( "select" ).last().trigger("change");
function getCategoryId(data){
  common(data.value,data.id);
}
function common(id,ids){  
 if(id != ''){
   var level = parseInt($('.level').val());
   var cid = parseInt($('#cid').val());    
    $.ajax({
      url: '<?php echo base_url(); ?>inventory/getAjaxSubcategory',
      data: { id: id ,level:level, cid:cid},
      dataType: 'text',
      type: 'POST',
      success: function (res) {
        $('#appendData select').each(function() {

              var currSelectValue = $(this).attr('id');
              if(ids=='parentCat'){
                $('select').nextAll('select').remove();
              }
              if(parseInt(ids) < parseInt(currSelectValue)){
                $("."+currSelectValue).remove();
              }
          });

        if(res != ''){
          $('#appendData select').each(function() {
            var currSelectValue = parseInt($(this).attr('id'));
            if(level != currSelectValue){
              $('.level').val(parseInt(level)+1);
            }
          });
          $('#appendData').append(res);
        }
      }
    });
 }
 else{
      $('#appendData select').each(function() {
            var currSelectValue = $(this).attr('id');
            if(ids=='parentCat'){
              $('select').nextAll('select').remove();
            }
            if(parseInt(ids) < parseInt(currSelectValue)){
              $("."+currSelectValue).remove();
            }
        });
    }
}
</script>
