<?php 
$loginSession = $this->session->userdata("login_session");
$role_id = $loginSession[0]->role_id;
//check user access this page
if(!empty($role_id) && $role_id != 39 && $role_id != 15 && $role_id != 20){
	header('Location: '.base_url());
}
?>
<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">
      <header class="panel-heading">Inventory Category</header>
   
        <?php if($userDepartmentId == '16'){?>
        <div class="row">
          <div class="col-lg-12">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <button type="button" class="btn btn-info" id="toggleModal" data-toggle="modal"
                  data-target="#category">Add inventory category <i class="fa fa-plus"></i></button>
              </div>
                      <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox">
                          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                          <p id="appendError"></p>
                      </div>
                      <?php if($this->session->flashdata('error')){?>
                            <div class="alert alert-block alert-danger fade in margin-top">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
                            </div>
                        <?php } ?>

                        <?php if($this->session->flashdata('success')){?>
                            <div class="alert alert-success fade in margin-top">
                                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                            </div>
                        <?php }?>
            </div>
          </div>
        </div>
        <?php }?>
  

      <!-- Modal -->
      <div class="modal fade category" id="categoryModal" role="dialog">
        <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Add Inventory category</h4>
            </div>
            <div class="modal-body">

              <form id="categoryForm" method="post">
                <div class="form-group">
                  <label class="control-label">Category name <span class="red">*</span></label>
                  <input value="" id="categoryName" type="text" class="form-control" name="categoryName" required>
                </div>

                <div class="form-group">
                  <label class="control-label">Category unique tag <span class="red">*</span></label>
                  <input placeholder="ex. MOU for mouse" id="categoryTag" type="text" class="form-control" name="categoryTag" required>
                </div>
                <div class="form-group">                           
                    <label class="custom-control-label"><input type="checkbox" value="1" name="hasDetail[]">MAC</label>
                    <label class="custom-control-label"><input type="checkbox" value="2" name="hasDetail[]">Serial no.</label>
                </div>

                <div class="form-group">
                  <label class="control-label">Category Description </label>
                  <input id="catDescription" type="text" class="form-control" name="catDescription">
                </div>

                <label class=" control-label">Parent category</label>
                <div class="form-group block" id="appendData">
                  <select class="form-control parentCat" onchange="getCategoryId(this);" name="parentCat" id="parentCat">
                    <option value="">--select--</option>
                    <?php if($getOnlyParentCategory != ''){
                      foreach($getOnlyParentCategory as $row){?>
                    <option value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>;
                    <?php  } 
                        }?>
                  </select>                  
                </div>
    
                <input type="hidden" class="level" id="level" name="level" value="1">
                <input type="hidden" class="cid" id="cid" name="cid" value="0">
                <button type="submit" name="saveCategory" class="btn btn-success saveCategory">Save</button>
              </form>
            </div>

          </div>
        </div>
      </div>
      <!-- end modal -->

    <div class="panel-body">
    <div class="table-responsive">
    <table class="table table-striped table-hover table-bordered" id="CategoryTable" class="CategoryTable">
    
        <thead>
            <tr>
              <th>Category ID</th>
              <th>Category name</th>
              <th>Category tag</th>
              <th>Category description</th>
              <th width="30%">Sub category</th>
              <th>Created at</th>
              <th>Updated at</th>      
              <?php if($userDepartmentId == 16){echo "<th>Action</th>";}?>
            </tr>
        </thead> 
        <tbody>
      <?php foreach($getAllCategory as $row){?>
            <tr>
                <td><?php echo $row->cat_id; ?></td>
                <td><?php echo $row->cat_name;?></td>
                <td><?php echo "LN/".$row->categoryTag;?></td>
                <td><?php if($row->cat_description !=''){echo $row->cat_description;}else{echo "---";}?></td>
                <td><?php if(getSubCategory($row->cat_id) != ''){print_r(getSubCategory($row->cat_id));}else{echo "---";} ;?></td>
                <td><?php echo date('d-M-y',strtotime($row->created_at));?></td>
                <td><?php echo date('d-M-y',strtotime($row->updated_at));?></td> 
                <?php if($userDepartmentId == 16){?>
                  <td>            
                  <a href="<?php echo base_url().'inventory/editcategory/'.$row->cat_id;?>">
                    <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip"
                      data-original-title="Edit&nbsp;Inventory" title=""><i class="fa fa-pencil"></i></button>
                  </a>
                  <a onclick="deleteCategory(<?php echo $row->cat_id;?>)">
                    <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip"
                      data-original-title="Delete&nbsp;Category"><i class="fa fa-trash-o "></i></button>
                  </a>
                  
                  </td>    

               <?php }?>
            </tr>

     <?php   }?>
        </tbody> 
                      
    </table>
    </div>                
    </div>
    </section>
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script>
$(document).ready(function () {
  var dt = $('#CategoryTable').DataTable({
    "aaSorting": [
      [0, "asc"]
    ],
    "iDisplayLength": 10,
		"pagingType": "full_numbers",
		"dom": 'Cfrtip',
    "bPaginate": true,
		"bInfo": true
  });
});
$('#toggleModal').click(function (e) {
  e.preventDefault();
  $('#categoryForm')[0].reset();
  $('#parentCat').nextAll('select').remove();
  $('#cid').val('0');
  $('#categoryModal').modal('show');
});

function getCategoryId(data){
  common(data.value,data.id);       
}
function common(id,ids){
 if(id != ''){
   var level = parseInt($('.level').val());      
    $.ajax({
      url: '<?php echo base_url(); ?>inventory/getAjaxSubcategory',
      data: { id: id ,level:level},
      dataType: 'text',
      type: 'POST',
      success: function (res) {
        $('#appendData select').each(function() {  
  
              var currSelectValue = $(this).attr('id');                                                  
              if(ids=='parentCat'){
                $('select').nextAll('select').remove();
              }
              if(parseInt(ids) < parseInt(currSelectValue)){                      
                $("."+currSelectValue).remove();  
              }              
          });   
        
        if(res != ''){ 
          $('#appendData select').each(function() {
            var currSelectValue = parseInt($(this).attr('id'));
            if(level != currSelectValue){
              $('.level').val(parseInt(level)+1);
            }            
          });                 
          $('#appendData').append(res);   
        }
      }
    });
 }
 else{ 
      $('#appendData select').each(function() {                 
            var currSelectValue = $(this).attr('id');

            if(ids=='parentCat'){
              $('select').nextAll('select').remove();
            }         
            if(parseInt(ids) < parseInt(currSelectValue)){                       
              $("."+currSelectValue).remove();
            }             
        });
    }
}

function deleteCategory(id){
  var r = confirm('Are you sure you want to delete category');
  if(r==true){
      $.ajax({
          url: '<?php echo base_url(); ?>inventory/deleteCategory',
          data: { id: id },
          dataType: 'text',
          type: 'POST',
          success: function (res) {              
              if(res == 1){
                  $('#alertBox').removeClass('hidden alert-danger');
                  $('#alertBox').addClass('alert-success');
                  $('#appendError').html("<strong>Success! </strong>Category deleted successfully!");
                  window.setTimeout(function(){location.reload()},2000);
              }
              else if(res == 0){
                $('#alertBox').removeClass('hidden alert-success');
                $('#alertBox').addClass('alert-danger');
                $('#appendError').html("<strong>Error! </strong>Category cannot be deleted, it has one more Sub categories or an inventory is present with this category!");
              }
              else{
                $('#alertBox').removeClass('hidden alert-success');
                $('#alertBox').addClass('alert-danger');
                $('#appendError').html("<strong>Error! </strong>there was a problem deleting category!");
              }
          }
        });
  }
}

$(document).ready(function () {
      jQuery.validator.addMethod("alphaSpaceNum", function(value, element) {
      return this.optional(element) || /^[a-zA-Z0-9\-\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("alphaSpace", function(value, element) {
      return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
    }); 
    $('.saveCategory').click(function () {
      var validator = $('#categoryForm').validate({
        errorClass: "error",
        errorElement: "span",
        ignore: [], 
        rules: {
          categoryName: {
            required : true,
            alphaSpaceNum:true
          },
          categoryTag : {
            required : true
          }         
        },
        messages: {
          categoryName: {
            required: "Category name is required.",
            alphaSpaceNum : "Category name can only contain alphabet, number and spaces."
          },
          categoryTag : {
            required : "Category tag is required."
          }
        },
        errorPlacement: function(error, element) {
          error.insertAfter(element);
        },
        submitHandler: function (form, event) {
          event.preventDefault();
          var data = $(form).serialize();
          $.ajax({
                url: '<?php echo base_url(); ?>inventory/saveInventoryCategory',
                data: {
                  data: data
                },
                dataType: 'text',
                type: 'POST',
            success: function (res) {
              $('#categoryModal').modal('hide');
                if(res == 1){
                        $('#alertBox').removeClass('hidden alert-danger');
                        $('#alertBox').addClass('alert-success');
                        $('#appendError').html("<strong>Success! </strong>Category added successfully!");
                        window.setTimeout(function(){location.reload()},2000);
                }else if(res == 2){
                        $('#alertBox').removeClass('hidden alert-success');
                        $('#alertBox').addClass('alert-danger');
                        $('#appendError').html("<strong>Error! </strong>there was a problem adding category!");
                }else if(res == 3){
                        $('#alertBox').removeClass('hidden alert-danger');
                        $('#alertBox').addClass('alert-success');
                        $('#appendError').html("<strong>Success! </strong>Category updated successfully!");
                        window.setTimeout(function(){location.reload()},2000);
                }else{
                        $('#alertBox').removeClass('hidden alert-success');
                        $('#alertBox').addClass('alert-danger');
                        $('#appendError').html("<strong>Error! </strong>there was a problem updating category!");
                }
              window.setTimeout(function(){location.reload()},1500);
              
            }
          });
        }
      });
    });
  });
</script>