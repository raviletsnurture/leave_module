<?php
class inventory_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
    function insertVendor($data){
        if($this->db->insert('crm_vendor',$data)){
            return true;
        }else{
            return false;
        }
    }

	function getInventoryByVendorId($id){
		$this->db->select("*");
		$this->db->from("crm_inventory");
		$this->db->where("vendor",$id);
		$result = $this->db->get()->result();
		return $result;
	}
	function getRepairByVendorId($id){
		$this->db->select("*");
		$this->db->from("crm_inventory_repair");
		$this->db->where("vendor",$id);
		$result = $this->db->get()->result();
		return $result;
	}
    function getAllVendors(){
        $this->db->select("*");
		$this->db->from("crm_vendor");
		$result = $this->db->get()->result();
		return $result; 
    }
	function getVendorById($id){
        $this->db->select("*");
		$this->db->from("crm_vendor");
		$this->db->where("vendor_id",$id);
		$result = $this->db->get()->result();
		return $result;
    }
	function vendorByType($id){
		$this->db->select("*");
		$this->db->from("crm_vendor");
		$this->db->where("vendorType",$id);
		$result = $this->db->get()->result();
		return $result;
	}
	function updateVendor($data, $id){
		$this->db->where('vendor_id', $id);
		$updated_status = $this->db->update('crm_vendor', $data);
		// $updated_status = $this->db->affected_rows();
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}
	function removeVendor($id)
	{
		$this->db->where('vendor_id', $id);
		if($this->db->delete('crm_vendor')){
			return true;
		}else{
			return false;
		}				 
	}
	function getOnlyParentCategory(){
		$this->db->select("*");
		$this->db->from("crm_inventory_category");
		$this->db->where("parent_cat",'0');
		$result = $this->db->get()->result();
		return $result;
	}
	function getOnlyHardware(){
		$this->db->select("*");
		$this->db->from("crm_inventory_category");
		$this->db->where("parent_cat",'0');
		$this->db->where("cat_name",'Hardware');
		$this->db->or_where("cat_name",'HARDWARE');
		$this->db->or_where("cat_name",'hardware');
		$result = $this->db->get()->result();
		return $result;
	}
	function getAllCategory(){
		$this->db->select("*");
		$this->db->from("crm_inventory_category");
		$result = $this->db->get()->result();
		return $result;
	}
	function insertCategory($data){
        if($this->db->insert('crm_inventory_category',$data)){
            return true;
        }else{
            return false;
        }
	}
	function subCategory($id){
		$this->db->select("*");
		$this->db->from("crm_inventory_category");
		$this->db->where("parent_cat",$id);
		$result = $this->db->get()->result();
		return $result;
	}
	function subCategoryWithId($id, $cid){
		$this->db->select("*");
		$this->db->from("crm_inventory_category");
		$this->db->where("parent_cat",$id);
		$this->db->where("cat_id !=",$cid);
		$result = $this->db->get()->result();
		return $result;
	}
	function getSubCategory($id){
		$this->db->select('GROUP_CONCAT(cat_name SEPARATOR ", ") as Subcategory', false); 
		$this->db->from('crm_inventory_category');
		$this->db->where('parent_cat', $id);
		$result = $this->db->get()->result_array();		
		return $result[0]['Subcategory'];
	}
	function deleteCat($id){
		$this->db->where('cat_id', $id);
		if($this->db->delete('crm_inventory_category')){
			return true;
		}else{
			return false;
		}
	}
	function getCategoryById($id=NULL){
		$this->db->select("*");
		$this->db->from("crm_inventory_category");
		$this->db->where("cat_id",$id);
		$result = $this->db->get()->row();
		return $result;
	}

	function saveInventory($data){
		if($this->db->insert('crm_inventory',$data)){
            return true;
        }else{
            return false;
        }
	}

	function updateCategory($id ,$data){
		$this->db->where('cat_id', $id);
		$updated_status = $this->db->update('crm_inventory_category', $data);
		// $updated_status = $this->db->affected_rows();
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}


	// Inventory module starts-------
	function updateInventory($data, $id){
		$this->db->where('inventory_id', $id);
		$updated_status = $this->db->update('crm_inventory', $data);
 
		// $updated_status = $this->db->affected_rows();
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}

	function getAllInventory(){
		$this->db->select("*,i.remarks as inventRemark, i.created_at as inventCreatedAt");
		$this->db->from("crm_inventory as i");
		$this->db->join('crm_inventory_category as c','i.category_id = c.cat_id','left');
		$this->db->join('crm_vendor as v','i.vendor = v.vendor_id');
		// $this->db->where('i.assign_status !="4"');
		// $this->db->where('i.assign_status !="3"');
		// $this->db->or_where('c.cat_name !="Hardware"');
		// $this->db->or_where('c.cat_name !="hardware"');
		// $this->db->or_where('c.cat_name !="software"');
		// $this->db->or_where('c.cat_name !="Software"');
		$result = $this->db->get()->result();
		return $result;
	}

	function getInventoryById($id){
		$this->db->select("*,i.remarks as inventRemark");
		$this->db->from("crm_inventory as i");
		$this->db->join('crm_inventory_category as c','i.category_id = c.cat_id','left');
		$this->db->join('crm_vendor as v','i.vendor = v.vendor_id');
		$this->db->where('i.inventory_id = '.$id.'');
		$result = $this->db->get()->row();
		return $result;
	}
	function updateInventoryBytag($tag, $data){
		// $data=array('assign_status'=>$data);
		$this->db->where('tag = "'.$tag.'"');
		if($this->db->update('crm_inventory',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getInventoryBycategoryId($id){
		$this->db->select("*");
		$this->db->from("crm_inventory as i");
		$this->db->join('crm_inventory_category as c','i.category_id = c.cat_id');
		$this->db->join('crm_vendor as v','i.vendor = v.vendor_id');
		$this->db->where("i.category_id",$id);
		$result = $this->db->get()->result();
		return $result;
	}
	function getInventoriesCountByVendorId($id){
		$this->db->select("*");
		$this->db->from("crm_inventory as i");
		$this->db->where("i.vendor",$id);
		$result = $this->db->get()->num_rows();
		return $result;		
	}
	function getUserAndInventoryAssigned($id){
		$this->db->select("i.*,ia.*,u.first_name, u.last_name");
		$this->db->from("crm_inventory as i");
		$this->db->join('crm_inventory_assign as ia','i.inventory_id = ia.inventory_id');
		$this->db->join('crm_users as u','ia.user_id = u.user_id');
		$this->db->where("i.inventory_id",$id);
		$result = $this->db->get()->result();
		return $result;
	}
	function getAllUnassignedInventory($id){
		$this->db->select("*");
		$this->db->from("crm_inventory as i");
		$this->db->join('crm_inventory_category as c','i.category_id = c.cat_id');
		$this->db->join('crm_vendor as v','i.vendor = v.vendor_id');
		$this->db->where("i.category_id",$id);
		$this->db->where_in('i.assign_status', array('0','5'));
		$result = $this->db->get()->result();
		return $result;
	}
	function getAllInventoryByCatId($id){
		$this->db->select("*");
		$this->db->from("crm_inventory as i");
		$this->db->where('i.category_id =',$id);
		$result = $this->db->get()->result();
		return $result;
	}
	function saveFiles($fileObject,$filePath,$ext){
		$temp = explode(".", $fileObject["name"]);
		$extension = end($temp);		
		$fileTmpName = $fileObject["tmp_name"];
		$fileError = $fileObject["error"];

		if (in_array($extension, $ext)){		
			if ($fileError > 0)
			{
					return false;
				}
				else{
					move_uploaded_file($fileTmpName, $filePath);
					return $fileObject["name"];
				}
			} 
			else {
				return false;
			}
	}

	function updateFileOnly($id ,$implodedBill){
		$data=array('bills'=>$implodedBill);
		$this->db->where('inventory_id', $id);
		$updated_status = $this->db->update('crm_inventory',$data);
		// $updated_status = $this->db->affected_rows();
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}
	function updateFileForRepair($id ,$implodedBill){
		$data=array('bills'=>$implodedBill);
		$this->db->where('repair_id', $id);
		$updated_status = $this->db->update('crm_inventory_repair',$data);
		// $updated_status = $this->db->affected_rows();
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}
	function deleteInventory($id){
		$this->db->where('inventory_id', $id);
		if($this->db->delete('crm_inventory')){
			return true;
		}else{
			return false;
		}
	}

	// Assign Inventory module starts
	function getInventoryByFilter(){
		$this->db->select("*");
		$this->db->from("crm_inventory as i");
		$this->db->join('crm_inventory_category as c','i.category_id = c.cat_id');
		$this->db->where('i.assign_status = "0"');
		$this->db->where('i.soldTo = "0"');
		$result = $this->db->get()->result();
		return $result;
	}

	 function saveAssignedInventory($data){
		if($this->db->insert('crm_inventory_assign',$data)){
            return true;
        }else{
            return false;
        }
	}

	function updateAssignInventory($inventories){
		$expInventory = explode(', ',$inventories);
		$data=array('assign_status'=>'1');
		$this->db->where_in('inventory_id', $expInventory);
		$updated_status = $this->db->update('crm_inventory',$data);
		// $updated_status = $this->db->affected_rows();
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}
	function getAllAssignedUsers(){
		$this->db->select("*");
		$this->db->from("crm_inventory_assign as ia");
		$this->db->join('crm_users as u','ia.user_id = u.user_id');
		$this->db->join('crm_department as d','u.department_id = d.department_id');
		$this->db->join('crm_inventory as i','i.inventory_id = ia.inventory_id');
		$this->db->join('crm_inventory_category as c','i.category_id = c.cat_id');
		$result = $this->db->get()->result();
		return $result;
	}
	function removeAssignedinventory($id){
		$this->db->where('assign_id', $id);
		if($this->db->delete('crm_inventory_assign')){
			return true;
		}else{
			return false;
		}
	}
    function getInventoryCategories($id){
		$this->db->select("*");
		$this->db->from("crm_inventory as i");
		$this->db->where('i.category_id', $id);
		$result = $this->db->get()->result();
		return $result;
	}
	function getInventoryByStatusAndCategory($id){
		$this->db->select("*");
		$this->db->from("crm_inventory as i");
		$this->db->where('i.category_id', $id);
		$this->db->where('i.assign_status', 0);
		$result = $this->db->get()->result();
		return $result;
	}
	function updateInventoryStatus($inventories){
		$expInventory = explode(', ',$inventories);
		$data=array('assign_status'=>'0');
		$this->db->where_in('inventory_id', $expInventory);
		$updated_status = $this->db->update('crm_inventory',$data);
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}

	function saveRepair($data){
		if($this->db->insert('crm_inventory_repair',$data)){
            return true;
        }else{
            return false;
        }
	}
	function getRepairs($id=null){
		$this->db->select("*, r.created_at as repairCreated, r.updated_at as repairUpdated, r.bills as repairBills");
		$this->db->from("crm_inventory_repair as r");
		$this->db->join('crm_inventory as i','r.inventory_id = i.inventory_id');		
		$this->db->join('crm_inventory_category as c','i.category_id = c.cat_id');
		$this->db->join('crm_vendor as v','r.vendor = v.vendor_id');
		if($id!='' && $id!=null){
			$this->db->where("repair_id",$id);
			$result = $this->db->get()->row();
		}else{
			$result = $this->db->get()->result();
		}
		return $result;
	}
	function updateRepair($data, $id){
		$this->db->where('repair_id', $id);
		$updated_status = $this->db->update('crm_inventory_repair', $data);
		if($updated_status){
			return true;
		}else{
			return false;
		}
	}

	function getRepairByinventoryId($id){
		$this->db->select("*");
		$this->db->from("crm_inventory_repair as i");
		$this->db->where('i.inventory_category', $id);
		$result = $this->db->get()->result();
		return $result;
	}
	function deleteRepairById($id){
		$this->db->where('repair_id', $id);
		if($this->db->delete('crm_inventory_repair')){
			return true;
		}else{
			return false;
		}
	}
}
?>