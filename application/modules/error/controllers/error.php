<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

		Author : Vasant Hun

		Date   : 29th July 2013

		Action : Search Properties	

	 */


class Error extends MX_Controller {  



    function __construct()

    {

        parent::__construct();		

        $this->template->set('controller', $this);

    }

	
	function error_404()
	{
	
	 	$this->load->view('error'); 
	  
	}

} 
?>