<?php
class reimbursement_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
    }

    function reimbursementAdd($data){
	    if($this->db->insert('crm_reimbursement_request',$data)){
			return true;
		}else{
			return false;
		}
    }
    
    function reimbursementUpdate($data, $reimbursement_id){
		$this->db->where('reimbursement_id',$reimbursement_id);
		if($this->db->update('crm_reimbursement_request',$data)){
			return true;
		}else{
			return false;
		}
    }
    
    function myReimbursmentsList($id,$limit,$offset){
		$this->db->select('r.*,u.first_name,u.last_name');
		$this->db->from('crm_reimbursement_request as r');
		$this->db->join('crm_users as u','u.user_id = r.user_id');
        $this->db->where('r.user_id = ', $id);
        $this->db->limit($limit, $offset);
        $this->db->order_by("r.created_at","desc");
		$query = $this->db->get()->result_array();
		return $query;
    }
    
    function myReimbursmentsListCount($id){
		$this->db->select('r.*,u.first_name,u.last_name');
		$this->db->from('crm_reimbursement_request as r');
		$this->db->join('crm_users as u','u.user_id = r.user_id');
        $this->db->where('r.user_id = ', $id);
		$query = $this->db->get();
        return $query->num_rows();
    }
    
    function deleteReimbursement($id){
		$this->db->where('reimbursement_id',$id);
	    if($this->db->delete('crm_reimbursement_request')){
			return true;
		}else{
			return false;
		}
    }
    
    function myDepartmentReimbursmentsList($user_id,$limit,$offset){
		$this->db->select("r.*,u.first_name, u.last_name");
		$this->db->from('crm_reimbursement_request as r');
		$this->db->join('crm_users as u','u.user_id = r.user_id');
		$this->db->where_in('r.user_id', $user_id);
        $this->db->order_by("r.created_at","desc");
        $this->db->limit($limit, $offset);
		$query = $this->db->get()->result_array();
		return $query;

    }
    
    function myDepartmentReimbursmentsListCount($user_id){
		$this->db->select("r.*,u.first_name, u.last_name");
		$this->db->from('crm_reimbursement_request as r');
		$this->db->join('crm_users as u','u.user_id = r.user_id');
		$this->db->where_in('r.user_id', $user_id);
        $this->db->order_by("r.created_at","desc");
		$query = $this->db->get();
        return $query->num_rows();

	}
}