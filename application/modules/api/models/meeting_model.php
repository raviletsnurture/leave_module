<?php
class meeting_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->template->set('controller', $this);
		$this->load->database();
	}


	function getUserDetail($user_id){
		$this->db->select('user_id,first_name,last_name,email');
		$this->db->from('users');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->row();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : get Meeting Detail
	 **/
	function getMeetingDetail($meeting_id){
		$this->db->select('*');
		$this->db->from('crm_meeting');
		$this->db->where('meeting_id',$meeting_id);
		$query=$this->db->get();
		return $query->row();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : get meeting list
	 **/
	function getMeetingLists($meeting_date){
		$this->db->select('*');
		$this->db->from('crm_meeting');
		$this->db->where("DATE( schedule_date ) = '$meeting_date'");
		$this->db->order_by("meeting_id","desc");
		$query=$this->db->get();
		return $query->result();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : add Meeting
	 **/
	function addMeeting($userId,$subject,$schedule_date,$schedule_time,$end_date,$description,$location,$members,$meeting_duration){
		$data['user_id']=$userId;
		$data['hr_role_id']=22;
		$data['subject'] = $subject;
		$data['schedule_date'] = $schedule_date;
		$data['schedule_time'] = $schedule_time;
		$data['end_date'] = $end_date;
		$data['description'] = $description;
		$data['location'] = $location;
		$data['members'] = $members;
		$data['meeting_duration'] = $meeting_duration;
		if($this->db->insert('crm_meeting',$data)){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : check Meeting Time
	 **/
	function checkMeetingTime($schedule_date,$schedule_time,$id){
		$this->db->select('*');
		$this->db->from('crm_meeting');
		$this->db->where('schedule_date',$schedule_date);
		$this->db->where('schedule_time',$schedule_time);
		$this->db->where('user_id',$id);
		$this->db->where('status = 0');
		$query=$this->db->get();
		return $query->row();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : edit Meeting
	 **/
	function editMeeting($meeting_id,$userId,$subject,$schedule_date,$schedule_time,$end_date,$description,$location,$members,$meeting_duration){
		$data['user_id']=$userId;
		$data['hr_role_id']=22;
		$data['subject'] = $subject;
		$data['schedule_date'] = $schedule_date;
		$data['schedule_time'] = $schedule_time;
		$data['end_date'] = $end_date;
		$data['description'] = $description;
		$data['location'] = $location;
		$data['members'] = $members;
		$data['meeting_duration'] = $meeting_duration;
		$this->db->where('meeting_id',$meeting_id);
		if($this->db->update('crm_meeting',$data)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : cancel Meeting
	 **/
	function cancelMeeting($meeting_id){
		$data['status'] = "1";
		$this->db->where('meeting_id',$meeting_id);
		if($this->db->update('crm_meeting',$data)){
			return true;
		}else{
			return false;
		}
	}


	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : send Mail To Meeting Memeber(globle funstion for api)
	 **/
	function sendMailToMeetingMemeber($subject,$message,$meetingDetails){
		//helper pass single id or multiple comma seperations id give all email in comma
		$usersEmail = getUserEmail($meetingDetails->members);

		$members_id = explode(",",$meetingDetails->members);
		$i=0;
		foreach($members_id as $memberId){
				$data = $this->getUserDetail($memberId);
				$members_name[$i] = $data->first_name.' '.$data->last_name;
				$i++;
		}

		$mailData['body'] = '<tr>
														<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong>'.$message.'</strong></p></td>
													</tr>
													<tr>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
										          <p style="margin:4px 0;"></p>
															<p><strong>Date: </strong></p>
														</td>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
										          <p style="margin:4px 0;"></p>
															<p>'.$meetingDetails->schedule_date.'</p>
														</td>
										      </tr>
													<tr>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
										          <p style="margin:4px 0;"></p>
															<p><strong>Time: </strong></p>
														</td>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
										          <p style="margin:4px 0;"></p>
															<p>'.date('g:i a', strtotime($meetingDetails->schedule_time)).'</p>
														</td>
										      </tr>
													<tr>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
										          <p style="margin:4px 0;"></p>
															<p><strong>Duration: </strong></p>
														</td>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
										          <p style="margin:4px 0;"></p>
															<p>'.$meetingDetails->meeting_duration.' Minutes</p>
														</td>
										      </tr>
													<tr>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
										          <p style="margin:4px 0;"></p>
															<p><strong>Subject: </strong></p>
														</td>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
										          <p style="margin:4px 0;"></p>
															<p>'.$meetingDetails->subject.'</p>
														</td>
										      </tr>
													<tr>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
										          <p style="margin:4px 0;"></p>
															<p><strong>Agenda: </strong></p>
														</td>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
										          <p style="margin:4px 0;"></p>
															<p>'.$meetingDetails->description.'</p>
														</td>
										      </tr>
													<tr>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
										          <p style="margin:4px 0;"></p>
															<p><strong>Location: </strong></p>
														</td>
														<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
										          <p style="margin:4px 0;"></p>
															<p>'.$meetingDetails->location.'</p>
														</td>
										      </tr>';
		$mailData['body'] .= '<tr>
													<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
														<p style="margin:4px 0;"></p>
														<p><strong>Participants: </strong></p>
													</td>
													<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
														<p style="margin:4px 0;"></p>
														<p>';
		$mailData['body'] .= implode(', ', $members_name);
		$mailData['body'] .= '</p></td></tr>';

		$mailData['name'] = '';
		$this->email->from('hrms@letsnurture.com', "HRMS");
		$this->email->to('hrms@letsnurture.com');
		$this->email->bcc($usersEmail->usersEmail);
		$this->email->subject($subject);
		$body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
		$this->email->message($body);
		//$this->email->send();
	}



}?>
