<?php
class visitor_app extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllUser(){
		$this->db->select('user_id,first_name,last_name,email');
		$this->db->from('users');
		$this->db->where("status = 'Active'");
		$this->db->order_by("first_name","ASC");
		$query=$this->db->get();
		return $query->result();
	}

	function getUserDetail($user_id){
		$this->db->select('first_name,last_name,email');
		$this->db->from('users');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : parth
	* Date : 06/01/2017
	* Modified By :
	* Description :  Metting reminder
	*/
	function addVisitorRequest($apiData,$fileName){
		$data['user_id'] = $apiData['data']['user_id'];
		$data['vName'] = $apiData['data']['vName'];
		$data['vPhone'] = $apiData['data']['vPhone'];
		$data['vEmail'] = $apiData['data']['vEmail'];
		$data['vPurpose'] = $apiData['data']['vPurpose'];
		$data['Whomtomeet'] = $apiData['data']['Whomtomeet'];
		$data['appoinment'] = $apiData['data']['appoinment'];
		$data['cardImage'] = $fileName;
		$data['created_at'] = date("Y-m-d H:i:s");
		if($this->db->insert('visitor_app',$data)){
			return true;
		}else {
			return false;
		}
	}

}
?>
