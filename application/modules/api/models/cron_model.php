<?php
class cron_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	/*
	* Developed By : parth
	* Date : 06/01/2017
	* Modified By :
	* Description :  Metting reminder
	*/
	function getMeetingReminder(){
		$this->db->select(" m.*,u.first_name,u.last_name,GROUP_CONCAT( u.email ) AS members_email, GROUP_CONCAT( u.first_name ) AS members_name,(select email from crm_users where user_id = m.user_id) as user_email ,(select CONCAT((first_name),(' '),(last_name)) from crm_users where user_id = m.user_id) as user_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("FIND_IN_SET( u.user_id, m.members)");
		$this->db->where("m.schedule_date IN (CURDATE(), CURDATE() + INTERVAL 1 DAY)");
		$this->db->group_by("m.meeting_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	/*
	* Developed By : parth
	* Date : 06/01/2017
	* Modified By :
	* Description :  Metting reminder
	*/
	function getMeetingMoMReminder(){
		$this->db->select(" m.*,u.first_name,u.last_name,GROUP_CONCAT( u.email ) AS members_email, GROUP_CONCAT( u.first_name ) AS members_name,(select email from crm_users where user_id = m.user_id) as user_email ,(select CONCAT((first_name),(' '),(last_name)) from crm_users where user_id = m.user_id) as user_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("FIND_IN_SET( u.user_id, m.members)");
		//$this->db->where("m.schedule_date > (CURDATE() + INTERVAL 4 Hour) AND m.schedule_date < (CURDATE() + INTERVAL 1 Day)"); //
		//$this->db->where("m.schedule_date = " . date('Y-m-d')); //
		$this->db->where("m.schedule_date IN (CURDATE(), CURDATE() + INTERVAL 1 DAY)");
		//$this->db->where("m.schedule_time IN (CURTIME(), CURTIME() + INTERVAL 4 HOUR)");
		$this->db->where("(m.mom is null)");
		$this->db->group_by("m.meeting_id");
		$this->db->limit("10");
		$milestone = $this->db->get()->result_array();
		//echo $this->db->last_query();
		return $milestone;
	}


	/*
	* Developed By : Krunal
	* Date : 06/04/2017
	* Modified By :
	* Description :  check Employ Log
	*/
	function checkEmployLog($data){
		$this->db->select("*");
		$this->db->from("crm_employ_log");
		$this->db->where("user_id",$data['user_id']);
		$this->db->where('log_date',$data['log_date']);
		$result = $this->db->get()->result_array();
		return $result;
	}

	/*
	* Developed By : Krunal
	* Date : 06/04/2017
	* Modified By :
	* Description :  Employee can view their log in log out time on monthly basis
	*/
	function getEmployLogFilePath(){
		//get all file which superadmin upload
		$this->db->select("*");
		$this->db->from("crm_employ_log_file");
		$this->db->where("status = '0'");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}


	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : superadmin password expired every 2 week and new password send to superadmin email
	*/
	public function updateSuperAdminPassword($data){
		$this->db->where('sId',"1");
		if($this->db->update('super_admin',$data)){
			return true;
		}else{
			return false;
		}
	}


	/*
	* Developed By : Krunal
	* Date : 06/04/2017
	* Modified By :
	* Description :  get Pending leave Total
	*/
	function getPendingLeaveTotal($startDate){
		$this->db->select("count(*) as total");
		$this->db->from("crm_leaves");
		$this->db->where("leave_status = '1'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");
		$this->db->where("leave_start_date <= '$startDate'");
		$result = $this->db->get()->row();
		return $result;
	}

	/*
	* Developed By : Krunal
	* Date : 06/04/2017
	* Modified By :
	* Description :  get Pending leave total by Department
	*/
	function getPendingLeaveTotalDepartment($startDate){
		$this->db->select("COUNT(l.department_id) as totalLeave,l.department_id,d.department_name");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_department as d','d.department_id = l.department_id', 'left');
		$this->db->where("l.leave_status = '1'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("l.leave_start_date <= '$startDate'");
		$this->db->group_by("l.department_id");
		$result = $this->db->get()->result();
		return $result;
	}


	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get All TL,PM,MGMT,HR,Sr HR Email id
	*/
	function getmanagementEmails(){
		//TL,PM,MGMT,HR,Sr HR,1->ceo
		$roles = '13,15,20,22,49,1';
		$this->db->select('GROUP_CONCAT(email) as memberEmail');
		$this->db->from('users');
		$this->db->where('role_id in ('.$roles.')');
		$this->db->where("status =  'Active'");
		$query=$this->db->get();
		$arr = $query->row_array();

		$this->db->select('GROUP_CONCAT(sEmail) as memberEmail');
		$this->db->from('super_admin');
		$query=$this->db->get();
		$arr1 = $query->row_array();
		$dd = $arr['memberEmail'].','.$arr1['memberEmail'];
		return $dd;
	}


	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Get list who is on leave next week
	*/
	function getNextWeekLeaveList(){
		$query = $this->db->query("SELECT u.first_name, u.last_name, d.department_name, ls.leave_status as lstatus, lt.leave_type as ltype, au.first_name as approved_byFirstName, au.last_name as approved_byLastName, l.* FROM crm_leaves as l LEFT JOIN crm_users as u ON u.user_id = l.user_id LEFT JOIN crm_users as au ON au.user_id = l.leave_approved_by LEFT JOIN crm_leave_status as ls ON ls.leave_status_id = l.leave_status LEFT JOIN crm_leave_type as lt ON lt.leave_type_id = l.leave_type LEFT JOIN crm_department as d ON d.department_id = u.department_id LEFT JOIN crm_users as up ON up.user_id = l.leave_approved_by WHERE l.leave_start_date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ORDER BY d.department_id desc");
    return $query->result_array();
	}

	/*
	* Developed By : Jignasa
	* Date : 07/07/2017
	* Modified By :
	* Description : Get list who are on leave today
	*/
	function getTodayLeaveList()
	{
		$todayDate = date('Y-m-d');
		$this->db->select("u.first_name, u.last_name, d.department_name,l.leave_status, ls.leave_status as lstatus, lt.leave_type as ltype, au.first_name as approved_byFirstName, au.last_name as approved_byLastName, l.*");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id','left');
		$this->db->join('crm_users as au','au.user_id = l.leave_approved_by','left');
		$this->db->join('crm_leave_status as ls','ls.leave_status_id = l.leave_status','left');
		$this->db->join('crm_leave_type as lt','lt.leave_type_id = l.leave_type');
		$this->db->join('crm_department as d','d.department_id = u.department_id','left');
		$this->db->where("l.leave_status != '4'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("WEEKDAY('$todayDate') != 5  AND WEEKDAY('$todayDate') != 6");
		$this->db->where("DATE('$todayDate') between `l`.`leave_start_date` and `l`.`leave_end_date`");
		$this->db->order_by("l.leave_days", "desc");
		$result = $this->db->get()->result_array();
		return $result;
	}


	/*
	* Developed By : Jignasa
	* Date : 07/07/2017
	* Modified By :
	* Description : Get list who are on leave today by TL, PM
	*/
	function getTodayLeaveListByTL($usersId)
	{
		$todayDate = date('Y-m-d');
		$this->db->select("GROUP_CONCAT(u.first_name) as usersfirstName");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id','left');
		$this->db->join('crm_users as au','au.user_id = l.leave_approved_by','left');
		$this->db->join('crm_leave_status as ls','ls.leave_status_id = l.leave_status','left');
		$this->db->join('crm_leave_type as lt','lt.leave_type_id = l.leave_type');
		$this->db->join('crm_department as d','d.department_id = u.department_id','left');
		$this->db->where("l.user_id in ($usersId)");
		$this->db->where("l.leave_status != '4'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("WEEKDAY('$todayDate') != 5  AND WEEKDAY('$todayDate') != 6");
		$this->db->where("DATE('$todayDate') between `l`.`leave_start_date` and `l`.`leave_end_date`");
		$this->db->order_by("l.leave_days", "desc");
		$result = $this->db->get()->row();
		return $result;
	}


	/*
	* Developed By : Jignasa
	* Date : 10/07/2017
	* Modified By :
	* Description : Get list who are on leave on tomorrow
	*/
	function getTomorrowLeaveList()
	{
		$todayDate = date('Y-m-d');
		$this->db->select("u.first_name, u.last_name, d.department_name, ls.leave_status as lstatus, lt.leave_type as ltype, au.first_name as approved_byFirstName, au.last_name as approved_byLastName, l.*");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id','left');
		$this->db->join('crm_users as au','au.user_id = l.leave_approved_by','left');
		$this->db->join('crm_leave_status as ls','ls.leave_status_id = l.leave_status','left');
		$this->db->join('crm_leave_type as lt','lt.leave_type_id = l.leave_type');
		$this->db->join('crm_department as d','d.department_id = u.department_id','left');
		$this->db->where("l.leave_status = '2'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("WEEKDAY('$todayDate' + INTERVAL 1 DAY) != 5  AND WEEKDAY('$todayDate' + INTERVAL 1 DAY) != 6");
		$this->db->where("DATE('$todayDate' + INTERVAL 1 DAY) between `l`.`leave_start_date` and `l`.`leave_end_date`");
		$this->db->order_by("l.leave_days", "desc");
		$result = $this->db->get()->result_array();
		return $result;
	}

	/*
	* Developed By : Jignasa
	* Date : 10/07/2017
	* Modified By :
	* Description :Get list who has taken leave in last month order by leaves count desc.
	*/
	function getLastMonthLeaveList()
	{
		$todayDate = date('Y-m-d');
		$this->db->distinct();
		$this->db->select("u.first_name, u.last_name, d.department_name,l.user_id,SUM(l.leave_days) as ldays");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id','left');
		$this->db->join('crm_department as d','d.department_id = u.department_id','left');
		$this->db->where("l.leave_status = '2'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("u.status = 'Active'");
		$this->db->where("u.first_name is not NULL");
		$this->db->where("YEAR(leave_start_date) = YEAR('$todayDate')");
		$this->db->where("(MONTH(leave_start_date)  = MONTH('$todayDate' - INTERVAL 1 MONTH) AND MONTH(leave_end_date)  = MONTH('$todayDate' - INTERVAL 1 MONTH))");
		$this->db->group_by("l.user_id");
		$this->db->order_by("ldays", "desc");
		$result = $this->db->get()->result_array();
		return $result;
	}

	/*
	* Developed By : Jignasa
	* Date : 12/07/2017
	* Modified By :
	* Description :Get list who has taken leave in last month order by leaves with leave detail.
	*/
	function getLastMonthAllLeaves()
	{
		$current_date = date('Y-m-d');
		$this->db->select("u.first_name, u.last_name,l.user_id, lt.leave_type as ltype,l.leave_days,l.leave_start_date,l.leave_end_date");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id','left');
		$this->db->join('crm_leave_type as lt','lt.leave_type_id = l.leave_type','left');
		$this->db->where("l.leave_status = '2'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("u.status = 'Active'");
		$this->db->where("u.first_name is not NULL");
		$this->db->where("YEAR(leave_start_date) = YEAR('$current_date')");
		$this->db->where("(MONTH(leave_start_date)  = MONTH('$current_date' - INTERVAL 1 MONTH) AND MONTH(leave_end_date)  = MONTH('$current_date' - INTERVAL 1 MONTH))");
		$this->db->order_by("first_name", "asc");
		$result = $this->db->get()->result_array();
		return $result;
	}

	/*
	* Developed By : Jignasa
	* Date : 10/07/2017
	* Modified By :
	* Description :Get list who has taken leave in last quarter order by leaves count desc.
	*/
	function getQuarterlyLeaveList()
	{
		$todayDate = date('Y-m-d');
		$this->db->distinct();
		$this->db->select("u.first_name, u.last_name, d.department_name,l.user_id,SUM(l.leave_days) as ldays");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id','left');
		$this->db->join('crm_department as d','d.department_id = u.department_id','left');
		$this->db->where("l.leave_status = '2'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("u.status = 'Active'");
		$this->db->where("u.first_name is not NULL");
		$this->db->where("YEAR(leave_start_date) = YEAR('$todayDate')");
		$this->db->where("(quarter('$todayDate' - INTERVAL 1 QUARTER) = QUARTER(leave_start_date) OR quarter('$todayDate' - INTERVAL 1 QUARTER) = QUARTER(leave_end_date))");
		$this->db->group_by("l.user_id");
		$this->db->order_by("ldays", "desc");
		$result = $this->db->get()->result_array();
		return $result;
	}

	/*
	* Developed By : Jignasa
	* Date : 12/07/2017
	* Modified By :
	* Description :Get list of employees with total working hours of last month.
	*/
	function getMonthlyWorkingHoursCount()
	{
		$current_date = date('Y-m-d');
		$this->db->distinct();
		$this->db->select("u.first_name, u.last_name, e.user_id, (SUM( TIME_TO_SEC( `extra_work_hours` ) ) + SUM( TIME_TO_SEC( `nPunch_work_hours` ) ) )/3600 as total");
		$this->db->from("crm_users as u");
		$this->db->join('crm_employ_log as e','u.user_id = e.user_id','left');
		$this->db->where("u.status = 'Active'");
		$this->db->where("u.first_name is not NULL");
		$this->db->where("MONTH(log_date)  = MONTH('$current_date' - INTERVAL 1 MONTH)");
		$this->db->where("YEAR(log_date) = YEAR('$current_date')");
		$this->db->where("( WEEKDAY(log_date)  != 6)");
		$this->db->group_by("e.user_id");
		$this->db->order_by("total");
		$result = $this->db->get()->result_array();
		return $result;
	}
	function getMonthlyLeaveDaysCount($user_id)
	{
		$current_date = date('Y-m-d');
		$this->db->select("SUM(days) as days");
		$this->db->from("crm_leave_month as u");
		$this->db->where("user_id = ".$user_id);
		$this->db->where("MONTH(YR_MTH)  = MONTH('$current_date' - INTERVAL 1 MONTH) and leave_type != 6 and leave_type != 14");
		$this->db->where("YEAR(YR_MTH) = YEAR('$current_date')");
		$result = $this->db->get()->result_array();
		return $result;
	}

	/*
	* Developed By : Jignasa
	* Date : 13/07/2017
	* Modified By :
	* Description :Get list of employees who has not taken leave but his time entry is missing .
	*/
	function getMissingEntryCount()
	{
		$month_ini = new DateTime("first day of last month");
		$month_end = new DateTime("last day of last month");
		$start =  $month_ini->format('Y-m-d'); // 2012-02-01
		$end = $month_end->format('Y-m-d'); // 2012-02-29

		$dateQuery = $this->db->query("select date from
(select adddate('2017-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
where (date  BETWEEN   '$start'  and '$end' )and WEEKDAY(date)  != 5  and WEEKDAY(date)  != 6");
		$workingDays = $dateQuery->result_array();
		$this->db->select("u.user_id,u.first_name, u.last_name");
		$this->db->from("crm_users as u");
		$this->db->where("u.status = 'Active'");
		$this->db->where("u.first_name is not NULL");
		$this->db->order_by("first_name");
		$users = $this->db->get()->result_array();
		$result = [];
		$GetData = [];
		foreach ($workingDays as $workingDay) {
			foreach ($users as $user) {
				$date = $workingDay['date'];
				$userId = $user['user_id'];
				$userName = $user['first_name'].' '.$user['last_name'];
				$logQuery = $this->db->query("SELECT  COUNT(*) as `log`  FROM `crm_employ_log` WHERE `log_date` = '$date' AND `user_id` = '$userId'");
				$logResult = $logQuery->result_array();


				$leaveQuery = $this->db->query("SELECT  COUNT(*) as `leave` FROM `crm_leaves` WHERE `leave_days` != '0' AND `leave_status` = '2' AND `is_cancelled` != '1' AND `user_id`  = '$userId' AND
				'$date' between `leave_start_date` and `leave_end_date`");
				$leaveResult = $leaveQuery->result_array();

				if($logResult[0]['log'] == 0 && $leaveResult[0]['leave'] == 0){
					$result['userId'] = $userId;
					$result['date'] = $date;
					$result['userName'] = $userName;
					$GetData[] = $result;
				}
			}
		}
		return $GetData;
	}

  /*
  * Developed By : Jignasa
  * Date : 13/07/2017
  * Modified By :
  * Description :Get list that how many time employee came late based on time table entry.
  */
  function getLateComingEntryCount()
  {
	$current_date = date('Y-m-d');
    $this->db->select("u.first_name, u.last_name,l.user_id,count(l.in_time) as count, l.log_date");
    $this->db->from("crm_employ_log as l");
    $this->db->join('crm_users as u','u.user_id = l.user_id','left');
    $this->db->where("u.status = 'Active'");
    $this->db->where("u.first_name is not NULL");
    $this->db->where("l.in_time != ''");
    $this->db->where("l.in_time > '10:00:00'");
	$this->db->where("WEEKDAY(l.log_date) != 5  AND WEEKDAY(l.log_date) != 6");
    $this->db->where("MONTH(log_date)  = MONTH('$current_date' - INTERVAL 1 MONTH)");
    $this->db->where("YEAR(log_date) = YEAR('$current_date')");
    $this->db->group_by("u.user_id");
    $this->db->order_by("count", "desc");
    $result = $this->db->get()->result_array();
    return $result;
  }

	/*
  * Developed By : Jignasa
  * Date : 14/07/2017
  * Modified By :
  * Description :Get count showing that how many times employee came late based on time table entry to the employee.
  */
  function getLateEntryCount()
  {
  	$todayDate = date('Y-m-d');
		$this->db->select("u.user_id, u.first_name, u.last_name, u.email, count(l.in_time) as count, l.log_date");
		$this->db->from("crm_employ_log as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id','left');
		$this->db->where("status = 'Active'");
		$this->db->where("first_name is not NULL");
		$this->db->where("MONTH(log_date) = MONTH('$todayDate')");
		$this->db->where("YEAR(log_date) = YEAR('$todayDate')");
		$this->db->where("l.in_time != ''");
		$this->db->where("l.in_time > '10:00:00'");
		$this->db->where("WEEKDAY(l.log_date) != 5  AND WEEKDAY(l.log_date) != 6");
		$this->db->group_by("u.user_id");
		$this->db->order_by("in_time", "desc");
		$result = $this->db->get()->result_array();
		return $result;
	}
	
	/*
  * Developed By : Md Mashroor
  * Date : 25/04/2019
  * Modified By :
  * Description :Get the employee details going to join in future
	*/
	function getFutureJoiningEmployees(){
		$joining_previous_date = date('Y-m-d',strtotime("+1 days"));
		$this->db->select("*");
		$this->db->from("crm_users");
		$this->db->where("date(joining_date) = '".$joining_previous_date."'");
		$this->db->order_by("joining_date", "ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

	/*
  * Developed By : Md Mashroor
  * Date : 24/05/2019
  * Modified By :
  * Description :Get all birthdays for upcoming week
	*/
	function getAllBirthdaysForUpcomingWeek(){
		$this->db->select("first_name, last_name, birthdate");
		$this->db->from("crm_users");
		$this->db->where("DATE_ADD(birthdate, INTERVAL YEAR(CURDATE()) - YEAR(birthdate) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)");
		$this->db->where("status = 'Active'");
		$this->db->order_by("birthdate","asc");
		$result = $this->db->get()->result_array();
		return $result;
	}
	/*
  * Developed By : Md Mashroor
  * Date : 24/05/2019
  * Modified By :
  * Description :Get all Leaves for upcoming week
	*/
	function getallLeavesForUpcomingWeek(){
		$this->db->select('u.first_name, u.last_name, l.leave_days, l.leave_start_date, d.department_name, l.leave_end_date');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("l.is_cancelled != 1");
		$this->db->where("l.leave_start_date > now()");
		$this->db->where("(l.leave_status = 1 OR l.leave_status = 2)");
		$this->db->where('l.leave_start_date <=(DATE(NOW()) + INTERVAL +7 DAY)');
		$this->db->order_by("l.leave_start_date","asc");
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get()->result_array();
		return $query;
	}

	/*
  * Developed By : Md Mashroor
  * Date : 24/05/2019
  * Modified By :
  * Description :Get all Anniversaries for upcoming week
	*/
	function getallAnniversariesForUpcomingWeek(){
		$this->db->select('first_name, last_name, joining_date');
		$this->db->from('users');
		$this->db->order_by("joining_date","asc");
		$this->db->where("joining_date !=  'NULL'");
		$this->db->where("joining_date !=  ''");
		$this->db->where("status =  'Active'");
		$this->db->where("DATE_ADD(joining_date, INTERVAL YEAR(CURDATE()) - YEAR(joining_date) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)");
		$query=$this->db->get()->result_array();
		return $query;
	}
}
?>
