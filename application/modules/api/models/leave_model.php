<?php
class leave_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	/*
	* Developed By : parth
	* Date : 06/01/2017
	* Modified By :
	* Description :  Metting reminder
	*/
	function getLeaveRecords(){

		$resultArray = array();
		for($i=0;$i<12;$i++){

		$sqlQuery = 'select leave_id,user_id,leave_type,leave_start_date , leave_end_date , date_format(date_add(date_add(leave_start_date, interval -dayofmonth(leave_start_date)+1 day ), interval i month  ), "%Y-%m" )
			as YR_MTH, to_days(least(date_add(date_add(leave_start_date, interval -dayofmonth(leave_start_date)+1 day ), interval i+1 month  ),
			date_add(leave_end_date, interval 1 day) )) - to_days(greatest(date_add(date_add(leave_start_date, interval -dayofmonth(leave_start_date)+1 day )
			, interval i month  ), leave_start_date )) as DAYS from integers inner join crm_leaves on date_add(date_add(leave_start_date,
			interval -dayofmonth(leave_start_date)+1 day ), interval i month) between date_add(leave_start_date, interval -dayofmonth(leave_start_date)+1 day )
			and leave_end_date Where date_format(date_add(date_add(leave_start_date, interval -dayofmonth(leave_start_date)+1 day ), interval i month  ), "%m" ) = '.($i+1).'
			AND leave_status = 2 AND is_cancelled=0 group by leave_start_date , leave_end_date , YR_MTH, user_id order by leave_start_date , YR_MTH';

			$query = $this->db->query($sqlQuery);
			$resultArray = $query->result_array();

			foreach($resultArray as $leaveData){

				$yr_mth = date_format(date_create($leaveData['YR_MTH'].'-'.($i+1)),'Y-m-d');
				$checkQuery = $this->db->query("select id,user_id from crm_leave_month where user_id = ".$leaveData['user_id']." AND leave_start_date = '".$leaveData['leave_start_date']."' AND YR_MTH = '".$yr_mth."'");
				$checkQueryArray = $checkQuery->result_array();

				if(empty($checkQueryArray)){
					$data = array(
					    'leave_id' => $leaveData['leave_id'],
					    'user_id' => $leaveData['user_id'],
						'leave_type' => $leaveData['leave_type'],
					    'leave_start_date' => $leaveData['leave_start_date'],
						'leave_end_date' => $leaveData['leave_end_date'],
						'YR_MTH' => $yr_mth,
						'days' => $leaveData['DAYS']
					);
					$this->db->insert('crm_leave_month', $data);
				}
			}
		}
		return $resultArray;
	}

}
?>
