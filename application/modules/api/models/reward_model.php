<?php
class reward_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	/*
	* Developed By : dipika
	* Date : 14/12/2017
	* Modified By :
	* Description : reset PM reward point
	*/
	function updatePMrewardpoint(){
		$data=array('pm_reward_points'=>3000);
        $this->db->where('role_id',20);
        $this->db->update('crm_users',$data);
	}

	function getAllRewardsHistory($id,$limit,$offset){
		$this->db->select("rh.*, u.reward_points,rh.point as history_point, rt.point as reward_type_point, rt.reward_name as reward_type_name, rh.status as reward_status, rh.id as rh_id, rt.id as reward_type_id, rt.point as reward_given_point");
		$this->db->from("crm_reward_history as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->join('crm_reward_type as rt','rt.id = rh.reward_id', 'left');
		$this->db->where("rh.user_id = $id");
		$this->db->order_by("rh.id","desc");
		$this->db->limit($limit, $offset);
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRewardsHistoryCount($id){
		$this->db->select("rh.*, u.reward_points,rh.point as history_point, rt.point as reward_type_point, rt.reward_name as reward_type_name, rh.status as reward_status, rh.id as rh_id, rt.id as reward_type_id, rt.point as reward_given_point");
		$this->db->from("crm_reward_history as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->join('crm_reward_type as rt','rt.id = rh.reward_id', 'left');
		$this->db->where("rh.user_id = $id");
		$this->db->order_by("rh.id","desc");
		$query = $this->db->get();
        return $query->num_rows();
	}

	function getAllTotalRewardTypes(){
		$this->db->select("*");
		$this->db->from("crm_reward_type");
		$this->db->where("status = '1'");
		$result = $this->db->get()->result();
		return $result;
	}

	function getRewardDetailsById($id){
		$this->db->select("*");
		$this->db->from("crm_reward_type");
		$this->db->where("id = $id");
		$query=$this->db->get();
		return $query->row();
	}

	function rewardAdd($data){
	    if($this->db->insert('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getRewardHistory($id){
		$this->db->select("*");
		$this->db->from("crm_reward_history");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->row();
		return $result;
	}


	function getRewardProducts($limit,$offset){
		$this->db->select("*");
		$this->db->from("crm_reward_products");
		$this->db->where("status = 1");
		$this->db->order_by("point","ASC");
		$this->db->limit($limit, $offset);
		$result = $this->db->get()->result();
		return $result;
	}

	function getRewardProductsCount(){
		$this->db->select("*");
		$this->db->from("crm_reward_products");
		$this->db->where("status = 1");
		$this->db->order_by("point","ASC");
		$query = $this->db->get();
        return $query->num_rows();
	}

	function getProduct($id){
		$this->db->select("*");
		$this->db->from("crm_reward_products");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function getThisMonthRewardCount($id){
		$this->db->select("*");
		$this->db->from("crm_reward_redeems");
		$this->db->where("YEAR(created_at) = YEAR(NOW())");
		$this->db->where("MONTH(created_at) = MONTH(NOW())");
		$this->db->where("user_id = ".$id);
		$result = $this->db->get()->result();
		return count($result);
	}

	function updateRewardUser($data, $user_id){
		$this->db->where('user_id',$user_id);
		if($this->db->update('crm_users',$data)){
			return true;
		}else{
			return false;
		}
	}

	function addRedeemRequest($data){
			if($this->db->insert('crm_reward_redeems',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getUserDetail($user_id){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->row();
	}

	function getAllRewardsRedeemHistory($id,$limit,$offset){
		$this->db->select("rh.*, rp.name, rp.point, rp.description, rp.status as status2");
		$this->db->from("crm_reward_redeems as rh");
		$this->db->join('crm_reward_products as rp','rp.id = rh.product_id', 'left');
		$this->db->where("rh.user_id = $id");
		$this->db->order_by("rh.created_at","desc");
		$this->db->limit($limit, $offset);
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRewardsRedeemHistoryCount($id){
		$this->db->select("rh.*, rp.name, rp.point, rp.description, rp.status as status2");
		$this->db->from("crm_reward_redeems as rh");
		$this->db->join('crm_reward_products as rp','rp.id = rh.product_id', 'left');
		$this->db->where("rh.user_id = $id");
		$this->db->order_by("rh.created_at","desc");
		$query = $this->db->get();
        return $query->num_rows();
	}

	function getRewardUserRankWise($limit,$offset){
		$this->db->select("u.*, d.department_name");
		$this->db->from("crm_users as u");
		$this->db->join('crm_department as d','d.department_id = u.department_id', 'left');
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("karma_points", 'desc');
		$this->db->limit($limit, $offset);
		$result = $this->db->get()->result();
		return $result;
	}

	function getRewardUserRankWiseCount(){
		$this->db->select("u.*, d.department_name");
		$this->db->from("crm_users as u");
		$this->db->join('crm_department as d','d.department_id = u.department_id', 'left');
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("karma_points", 'desc');
		$query = $this->db->get();
        return $query->num_rows();
	}

	function getAllEOMHistory($limit,$offset){
		$this->db->select("rh.*, u.first_name, u.last_name");
		$this->db->from("crm_reward_eom as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->order_by("rh.id","desc");
		$this->db->limit($limit, $offset);
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllEOMHistoryCount(){
		$this->db->select("rh.*, u.first_name, u.last_name");
		$this->db->from("crm_reward_eom as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->order_by("rh.id","desc");
		$query = $this->db->get();
        return $query->num_rows();
	}

    function getUserRedeemHistory($row_id, $user_id){
	    $this->db->select("rh.status,p.point");
		$this->db->from("crm_reward_redeems as rh");
		$this->db->join('crm_reward_products as p','p.id = rh.product_id');
		$this->db->where('rh.user_id',$user_id);
        $this->db->where('rh.id',$row_id);
		$result = $this->db->get()->result();
		return $result;
	}

    function cancelRedeem($data, $r_id){
		$this->db->where('id',$r_id);
		if($this->db->update('crm_reward_redeems',$data)){
			return true;
		}else{
			return false;
		}
	}

	function addRewardtoUser($data){
	    if($this->db->insert('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function rewardUpdate($data,$r_id){
		$this->db->where('id',$r_id);
	    if($this->db->update('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function checkReward($reward_id,$user_id){
		$this->db->select("*");
		$this->db->from("crm_reward_history");
		$this->db->where("id = ".$reward_id);
		$this->db->where("user_id = ".$user_id);
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRewardRequests($userid, $deptid,$limit,$offset){
		$this->db->select('h.id as rh_id,au.first_name as afirst_name,au.last_name as alast_name,d.department_name,h.status as reward_status, h.description as reward_description, u.first_name,u.last_name,h.*,rt.*,au.role_id as role_id,h.reward_id as reward_id,u.user_id as user_id,');
		$this->db->from('crm_reward_history as h');
		$this->db->join('users as u','u.user_id = h.user_id', 'left');
		$this->db->join('users as au','au.user_id = h.approved_by', 'left');
		$this->db->join('crm_reward_type as rt','rt.id = h.reward_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->order_by("h.id","desc");
		$this->db->where('h.user_id != '.$userid);
		if($deptid != 10 && $deptid != 11)
		{
			$this->db->where('u.department_id = '.$deptid);
		}
		$this->db->limit($limit, $offset);
		$query=$this->db->get();
		return $query->result();
	}

	function getAllRewardRequestsCount($userid, $deptid){
		$this->db->select('h.id as rh_id,au.first_name as afirst_name,au.last_name as alast_name,d.department_name,h.status as reward_status, h.description as reward_description, u.first_name,u.last_name,h.*,rt.*,au.role_id as role_id,rt.id as reward_id');
		$this->db->from('crm_reward_history as h');
		$this->db->join('users as u','u.user_id = h.user_id', 'left');
		$this->db->join('users as au','au.user_id = h.approved_by', 'left');
		$this->db->join('crm_reward_type as rt','rt.id = h.reward_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->order_by("h.id","desc");
		$this->db->where('h.user_id != '.$userid);
		if($deptid != 10 && $deptid != 11)
		{
			$this->db->where('u.department_id = '.$deptid);
		}
		$query = $this->db->get();
        return $query->num_rows();
	}

	function getPMPoints($id){
		$this->db->select("pm_reward_points");
		$this->db->from("crm_users");
		$this->db->where("user_id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllUsers($id){
		$this->db->select("p.user_id, p.first_name, p.last_name, d.department_name");
		$this->db->from("crm_users as p");
		$this->db->join('crm_department as d','d.department_id = p.department_id', 'left');
		$this->db->where("p.status","Active");
		$this->db->where("p.user_id != ".$id);
		if($id != 11 )
		{
			$this->db->where("p.department_id = ".$id);
		}
		$this->db->order_by("p.first_name", "asc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getOneMonthReward(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select("u.first_name, u.last_name, SUM(rr.point) as totalpoints");
		$this->db->from('crm_reward_history as rr');
		$this->db->join('crm_users as u','rr.user_id = u.user_id', 'left');
		$this->db->where("month(`date`) = $month");
		$this->db->where("year(`date`) = $year");
		$this->db->where("rr.status = '1'");
		$this->db->group_by('rr.user_id');
		$this->db->order_by('totalpoints', 'desc');
		$this->db->limit(3);
		$result = $this->db->get()->result();
		return $result;
	}

	function getWeeklyLeaderReward(){
		$monthdate = date('Y-m-01 00:00:00');
		$this->db->select("u.first_name, u.last_name, SUM(rr.point) as totalpoints");
		$this->db->from('crm_reward_history as rr');
		$this->db->join('crm_users as u','rr.user_id = u.user_id', 'left');
		$this->db->where("`date` < now()");
		$this->db->where("`date` > '$monthdate'");
		$this->db->where("rr.status = '1'");
		$this->db->group_by('rr.user_id');
		$this->db->order_by('totalpoints', 'desc');
		$this->db->limit(1);
		$result = $this->db->get()->row();
		return $result;
	}

	function getLastMonthRecruits(){
		$time = time();
		$month = date('m', strtotime('-1 month', $time));
		$this->db->select('email');
		$this->db->from('crm_req_applicants');
		$this->db->where("month(`createdDate`) = $month");
		$result = $this->db->get()->result();
		return $result;
	}
}
?>
