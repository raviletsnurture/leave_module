<?php
/**
 * This model use for api.
 *
 */
class User_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

        public function getDocument($id){
		$this->db->where('user_id',$id);
		$query = $this->db->get('crm_user_document');
		return $query->result();
	}

	public function addDocument($data){
		$this->db->insert('crm_user_document',$data);
		return $insert_id = $this->db->insert_id();
	}

	public function deleteDocument($document_id){
		$this->db->where('document_id',$document_id);
		$query = $this->db->delete('crm_user_document');
                return $query;
	}

        public function getDocumentById($id){
		$this->db->where('document_id',$id);
		$query = $this->db->get('crm_user_document');
		return $query->result();
	}

        public function getSkillById($id){
                $this->db->where('uskill_id',$id);
		$query = $this->db->get('crm_users_skill');
		return $query->result();
	}

	public function getAllSkills($id){
		$this->db->select('us.*,t.name');
		$this->db->from('crm_users_skill as us');
		$this->db->join('crm_skill as t','t.skill_id= us.skill_id', 'left');
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllSkillsList(){
		$this->db->select('*');
		$this->db->from('crm_skill');
		$query = $this->db->get();
		return $query->result();
	}

	public function getSkillDetail($skillid,$user_id){
		$this->db->select('*');
		$this->db->from('crm_users_skill');
		$this->db->where('skill_id',$skillid);
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->row();
	}

	public function addSkill($data){
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('skill_id',$data['skill_id']);
		$query = $this->db->get('users_skill');
		$result = $query->row();
		if(empty($result)){
			$this->db->insert('users_skill',$data);
			$insert_id = $this->db->insert_id();
			return 1;
		}else{
			$this->db->where('skill_id',$result->skill_id);
			$this->db->update('users_skill',$data);
			return 2;
		}
	}

	public function deleteSkill($uskill_id){
		$this->db->where('uskill_id',$uskill_id);
		$query = $this->db->delete('crm_users_skill');
	}
}
