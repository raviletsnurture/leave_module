<?php
class apiv1_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->template->set('controller', $this);
		$this->load->database();
	}


	function getUserLogin($user,$pwd){

		$this->db->select('user_id,email,first_name,last_name, password');
		$this->db->from('users');
		$this->db->where('username',$user);
		$this->db->where('password',$pwd);
		$this -> db -> limit(1);
		$query=$this->db->get();
   			if($query -> num_rows() == 1)
  			 {
     			return $query->result();
   				} else {
     			return false;
   			}
	}
	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get department Name
	*/
	function getdepartmentName($department_id){
		$this->db->select('*');
		$this->db->from('crm_department');
		$this->db->where('department_id',$department_id);
		$query=$this->db->get();
		return $query->row();
	}

	function getemails(){
		//TL,PM,MGMT,HR,Sr HR
		$roles = '13,15,20,22,49';
		$this->db->select('email');
		$this->db->from('users');
		$this->db->where('role_id in ('.$roles.')');
		$this->db->where("status =  'Active'");
		if($rid != '13' || $rid != '15' || $rid != '20' || $rid != '22' || $rid != '49'){
			$this->db->where("email != 'ketan@letsnurture.com'");
		}
		$query=$this->db->get();
		$arr = $query->result();
		$this->db->select('sEmail as email');
		$this->db->from('super_admin');
		$query=$this->db->get();
		$arr1 = $query->result();
		$dd = array_merge($arr,$arr1);
		return $dd;
	}

	function getUserDetails($id){
		$this->db->select('first_name,last_name,department_id,role_id');
		$this->db->from('crm_users');
		$this->db->where('user_id',$id);
		$query=$this->db->get();
		return $query->row();
	}

	function getLeaveTypeText($leave_id){
		$this->db->select('leave_type');
		$this->db->from('crm_leave_type');
		$this->db->where('leave_type_id',$leave_id);
		$query=$this->db->get();
		return $query->row();
	}

	function getUserLeaves($id)
	{
		$this->db->select('ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*, d.department_name');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->where('l.user_id',$id);
		$this->db->order_by("leave_id","desc");
		$this->db->limit(6);
		$query=$this->db->get();

		return $query->result();
	}

	function getAllLeaveType()
	{
		$this->db->select('leave_type_id, leave_type, status');
		$this->db->from('leave_type');
		$this->db->where("status =  'Active'");
		$this->db->order_by("leave_type_id","desc");
		$query=$this->db->get();
		return $query->result();
	}

	function checkLeaveDate($date,$user_id){
		$this->db->select('user_id');
		$this->db->from('leaves');
		$this->db->where('leave_start_date',$date);
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->result();
	}

	function getAddLeaves($userid, $leavetype, $startdate, $enddate, $reason){
		$data['user_id']=$userid;
		$data['leave_type']=$leavetype;
		$data['leave_start_date']=$startdate;
		$data['leave_end_date']=$enddate;
		$data['leave_reason']=$reason;
		$data['leave_status'] = 1;
		//$data['leave_created'] = date("Y-m-d H:i:s");

		//if the leave is for past date it will be unapproved
		if(date('Y-m-d',strtotime($startdate)) < date('Y-m-d')){
			$data['leave_status'] = 4;
		}

		$numdays =round(abs(strtotime($startdate)-strtotime($enddate))/86400)+1;
		// Now select department id
		$this->db->select('department_id');
		$this->db->from('users');
		$this->db->where("user_id",$userid);
		$query=$this->db->get();
		$mydeptt = $query->result();
		$data['department_id']=$mydeptt[0]->department_id;
		$data['leave_days'] = getDaysCount($startdate,$enddate);

	    if($this->db->insert('leaves',$data)){
    		// EL updates in user table
	 			// get specific user whose EL details to be updated
				$this->db->select('total_leaves, earned_leaves, el_balance');
		 		$this->db->from('users');
		 		$this->db->where('user_id',$userid);
		 		$query=$this->db->get()->result();

		 		$updated_total = $query[0]->total_leaves;
		 		$updated_EL = $query[0]->earned_leaves;
	   		$dataUser['total_leaves'] = $updated_total- $numdays;
		 		$dataUser['earned_leaves'] = $updated_EL- $numdays;
	   		$this->db->where('user_id',$userid);
				if($this->db->update('users',$dataUser)){
 					return true;
				}else{
 					return false;
				}
		 }else{
		 		return false;
		 }
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : check When more than 3 team members has applied for leave on same day from same department. Send Alert to TLs , PM & HR.
	*/
	function checkTotalLeaveOnSameDays($leave_start_date,$leave_end_date,$department_id){
		$this->db->select('user_id');
		$this->db->from('leaves');
		$this->db->where('leave_start_date >=',$leave_start_date);
		$this->db->where('leave_end_date <=',$leave_end_date);
		$this->db->where('department_id',$department_id);
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : When More then 1 TL from same department has applied for Leave please send alert to TL , PM & HR ( Notification + Email )
	*/
	function checkTLTotalLeaveOnSameDays($leave_start_date,$leave_end_date,$department_id,$role_id){
		$this->db->select('l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->where('l.leave_start_date >=',$leave_start_date);
		$this->db->where('l.leave_end_date <=',$leave_end_date);
		$this->db->where('l.department_id',$department_id);
		$this->db->where('u.role_id',$role_id);
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function checkTotalNumberLeave(){
		$this->db->select('SUM( l.leave_days) as totalLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) = YEAR(NOW())');
		$this->db->where('MONTH(l.leave_start_date)=MONTH(NOW())');
		$this->db->where('l.leave_type in (3,4)');
		$this->db->where('l.leave_status = 2');
		$this->db->where('u.role_id != 10');
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function getActiveEmployee(){
		$this->db->select('COUNT(user_id)');
		$this->db->from('crm_users');
		$this->db->where("status =  'Active'");
		return $this->db->count_all_results();
	}
}
?>
