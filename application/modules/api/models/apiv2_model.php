<?php
class apiv2_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllUser(){
		$this->db->select('u.user_id,u.first_name,u.last_name,email,d.department_name');
		$this->db->from('users as u');
		$this->db->where("u.status = 'Active'");
		$this->db->join('crm_department as d','d.department_id = u.department_id', 'left');
		$query=$this->db->get();
		return $query->result();
	}

	function getUserLogin($user,$pwd){
        $this->db->select('u.*,r.role_name,d.department_name,cu.short_name as country_name,st.state_name,ct.city_name,re.religion_name');
        $this->db->from('users as u');
        $this->db->join('crm_role as r','r.role_id = u.role_id', 'left');
        $this->db->join('crm_department as d','d.department_id = u.department_id', 'left');
        $this->db->join('crm_country as cu','cu.country_id = u.country_id', 'left');
        $this->db->join('crm_state as st','st.state_id = u.state_id', 'left');
        $this->db->join('crm_city as ct','ct.city_id = u.city_id', 'left');
        $this->db->join('crm_religion as re','re.religion_id = u.religion_id', 'left');
        $this->db->where('username',$user);
        $this->db->where('password',$pwd);
        $query=$this->db->get();
        return $query->row();
    }

	function getUserDetail($user_id){
		$this->db->select('first_name,last_name,feedback_users');
		$this->db->from('users');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get department Name
	*/
	function getdepartmentName($department_id){
		$this->db->select('*');
		$this->db->from('crm_department');
		$this->db->where('department_id',$department_id);
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get Department
	*/
	function getDepartment(){
		$this->db->select('department_id,department_name');
		$this->db->from('crm_department');
		$this->db->where("status =  'Active'");
		$query=$this->db->get();
		return $query->result();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get All TL,PM,MGMT,HR,Sr HR Email id
	*/
	function getemails($rid,$did){
		//TL,PM,MGMT,HR,Sr HR
		$roles = '13,15,20,22,49';
		$this->db->select('GROUP_CONCAT(email) as memberEmail');
		$this->db->from('users');
		$this->db->where('role_id in ('.$roles.')');
		$this->db->where("status =  'Active'");
		if($rid != '13' || $rid != '15' || $rid != '20' || $rid != '22' || $rid != '49'){
			$this->db->where("email != 'ketan@letsnurture.com'");
		}
		$query=$this->db->get();
		$arr = $query->row_array();

		$this->db->select('GROUP_CONCAT(sEmail) as memberEmail');
		$this->db->from('super_admin');
		$query=$this->db->get();
		$arr1 = $query->row_array();
		$dd = $arr['memberEmail'].','.$arr1['memberEmail'];
		return $dd;
	}

	function getUserDetails($id){
		$this->db->select('first_name,last_name,department_id,role_id,email');
		$this->db->from('crm_users');
		$this->db->where('user_id',$id);
		$query=$this->db->get();
		return $query->row();
	}

	function getLeaveTypeText($leave_id){
		$this->db->select('leave_type');
		$this->db->from('crm_leave_type');
		$this->db->where('leave_type_id',$leave_id);
		$query=$this->db->get();
		return $query->row();
	}

	function getUserLeaves($id){
		$this->db->select('ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*, d.department_name');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->where('l.user_id',$id);
		$this->db->order_by("leave_start_date","desc");
		$this->db->limit(30);
		$query=$this->db->get();
		return $query->result_array();
	}

	function getAllLeaveType()
	{
		$this->db->select('leave_type_id, leave_type');
		$this->db->from('leave_type');
		$this->db->where("status =  'Active'");
		$this->db->order_by("leave_type","desc");
		$query=$this->db->get();
		return $query->result();
	}

	function checkLeaveDate($date,$user_id,$leave_type){
		$this->db->select('user_id,leave_type');
		$this->db->from('leaves');
		$this->db->where('leave_start_date',$date);
		$this->db->where('user_id',$user_id);
		$this->db->where("is_cancelled = 0");
		$query=$this->db->get();
		$result = $query->result();
		if($result){
			if(count($result) != 1){
				if(($leave_type == 14) && ($result[0]->leave_type == 14 || $result[1]->leave_type == 14)){
					return $result;
				}else if($leave_type == 6 && ($result[0]->leave_type == 14 || $result[1]->leave_type == 14)){

				}else if($leave_type == 14 && ($result[0]->leave_type == 6 || $result[1]->leave_type == 6)){

				}else if($result[0]->leave_type != 14 || $result[1]->leave_type != 14){
					return $result;
				}
			}else{
				if(($leave_type == 14) && $result[0]->leave_type == 14){
					return $result;
				}else if($leave_type == 6 && $result[0]->leave_type == 14){

				}else if($leave_type == 14 && $result[0]->leave_type == 6){

				}else if($result[0]->leave_type != 14 || $result[1]->leave_type != 14){
					return $result;
				}
			}
		}
	}

	function getearlyleavestatus($date,$user_id){
		$this->db->select('user_id,leave_start_date');
		$this->db->from('crm_leaves');
		$this->db->where('leave_type = 6');
		$this->db->where("MONTH( leave_start_date ) = MONTH('".$date."')  AND YEAR( leave_start_date) = YEAR(NOW())");
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->result();
	}

	function getAddLeaves($userid, $leavetype, $startdate, $enddate, $reason,$criticaltask,$resassignee){
		$data['user_id']=$userid;
		$data['leave_type']=$leavetype;
		$data['leave_start_date']=$startdate;
		$data['leave_end_date']=$enddate;
		$data['leave_reason']=$reason;
		$data['leave_critical_task']=$criticaltask;
		$data['leave_res_assignee']=$resassignee;
		$data['leave_status'] = 1;
		//$data['leave_created'] = date("Y-m-d H:i:s");

		//if the leave is for past date it will be unapproved
		if(date('Y-m-d',strtotime($startdate)) < date('Y-m-d')){
			$data['leave_status'] = 4;
		}

		$numdays =round(abs(strtotime($startdate)-strtotime($enddate))/86400)+1;
		// Now select department id
		$this->db->select('department_id');
		$this->db->from('users');
		$this->db->where("user_id",$userid);
		$query=$this->db->get();
		$mydeptt = $query->result();
		$data['department_id']=$mydeptt[0]->department_id;
		$data['leave_days'] = getDaysCount($startdate,$enddate);

	    if($this->db->insert('leaves',$data)){
    		// EL updates in user table
	 			// get specific user whose EL details to be updated
				$this->db->select('total_leaves, earned_leaves, el_balance');
		 		$this->db->from('users');
		 		$this->db->where('user_id',$userid);
		 		$query=$this->db->get()->result();

		 		$updated_total = $query[0]->total_leaves;
		 		$updated_EL = $query[0]->earned_leaves;
	   		$dataUser['total_leaves'] = $updated_total- $numdays;
		 		$dataUser['earned_leaves'] = $updated_EL- $numdays;
	   		$this->db->where('user_id',$userid);
				if($this->db->update('users',$dataUser)){
 					return true;
				}else{
 					return false;
				}
		 }else{
		 		return false;
		 }
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : check When more than 3 team members has applied for leave on same day from same department. Send Alert to TLs , PM & HR.
	*/
	function checkTotalLeaveOnSameDays($leave_start_date,$leave_end_date,$department_id){
		$this->db->select('user_id');
		$this->db->from('leaves');
		$this->db->where('leave_start_date >=',$leave_start_date);
		$this->db->where('leave_end_date <=',$leave_end_date);
		$this->db->where('department_id',$department_id);
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : When More then 1 TL from same department has applied for Leave please send alert to TL , PM & HR ( Notification + Email )
	*/
	function checkTLTotalLeaveOnSameDays($leave_start_date,$leave_end_date,$department_id,$role_id){
		$this->db->select('l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->where('l.leave_start_date >=',$leave_start_date);
		$this->db->where('l.leave_end_date <=',$leave_end_date);
		$this->db->where('l.department_id',$department_id);
		$this->db->where('u.role_id',$role_id);
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	leave type = EL, Urgent, Birthday Leave, Anniversary Leave, Marriage Leave, Early Leave.
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function checkTotalNumberLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		$this->db->select('SUM( l.leave_days) as totalLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		$this->db->where('l.leave_type in (3,4,6,8,9,10)');
		$this->db->where('l.leave_status = 2');
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function getActiveEmployee(){
		$this->db->select('COUNT(user_id)');
		$this->db->from('crm_users');
		$this->db->where("status =  'Active'");
		return $this->db->count_all_results();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :Jignasa
	 * @updated		  :	12-07-2017
	 * @Status		  :
	 * @Comment		  : Edit Leaves
	 **/
	function editLeaves($userId, $leaveType, $startDate, $endDate,$reason,$criticaltask,$resassignee,$leaveid){
		$this->db->select('*');
		$this->db->from('crm_leaves');
		$this->db->where('leave_status',"1");
		$this->db->where('leave_id',$leaveid);
		$query=$this->db->get();
		$checkLeaveStatus = $query->row();
		if($checkLeaveStatus){
			$this->db->set('leave_type',$leaveType);
			$this->db->set('leave_start_date',$startDate);
			$this->db->set('leave_end_date',$endDate);
			$this->db->set('leave_reason',$reason);
			$this->db->set('leave_critical_task',$criticaltask);
			$this->db->set('leave_res_assignee',$resassignee);
			$this->db->set('leave_days',getDaysCount($startDate,$endDate));
			$this->db->where('leave_id',$leaveid);
			if($this->db->update('crm_leaves')){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : cancel Leaves
	 **/
	function cancelLeaves($leaveid){
		$this->db->select('l.*,u.first_name,u.last_name,u.role_id,u.user_id as userId');
		$this->db->from('crm_leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->where('l.leave_status',"1");
		$this->db->where('l.leave_id',$leaveid);
		$query=$this->db->get();
		$checkLeaveStatus = $query->row();
		if($checkLeaveStatus){
			$this->db->set('is_cancelled','1');
			$this->db->where('leave_id',$leaveid);
			if($this->db->update('crm_leaves')){
				return $checkLeaveStatus;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : update MyDepartment Leave Status
	 **/
	function updateMyDepartmentLeaveStatus($leaveid,$userid,$leave_status){
		$this->db->select('l.*,u.first_name,u.last_name,lt.leave_type');
		$this->db->from('crm_leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('crm_leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		//$this->db->where('l.leave_status',"1");
		$this->db->where('l.is_cancelled = 0');
		$this->db->where('l.leave_id',$leaveid);
		$query=$this->db->get();
		$checkLeaveStatus = $query->row();
		if($checkLeaveStatus){
			$this->db->set('leave_status',$leave_status);
			$this->db->set('approved_by',$userid);
			$this->db->where('leave_id',$leaveid);
			if($this->db->update('crm_leaves')){
				return $checkLeaveStatus;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : get All Salary Slip by user
	 **/
	function getAllSalarySlip($userid){
		$this->db->select('*');
		$this->db->from('crm_salary');
		$this->db->where("user_id",$userid);
		$this->db->order_by("uploaded_at","desc");
		$query=$this->db->get();
		return $query->result();
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author        : krunal
	 * @created		  :	11-04-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : get LogList
	 **/
	function getLogList($userid,$startdate,$enddate){
		$this->db->select('*');
		$this->db->from('crm_employ_log');
		$this->db->where("punch_id",$userid);
		$this->db->where("log_date BETWEEN '$startdate' AND '$enddate'");
		$query=$this->db->get();
		return $query->result();
	}

	function getAllTotalTimelog($id,$firstDate,$lastDate){
		$this->db->select("(SUM( TIME_TO_SEC( extra_work_hours ) ) + SUM( TIME_TO_SEC( gross_work_hours ) ) )/3600 as total");
		$this->db->from("crm_employ_log");
		$this->db->where("punch_id = $id");
		$this->db->where("log_date BETWEEN '$firstDate' AND '$lastDate'");
		$result = $this->db->get()->row();
		return $result;
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : getFeedback
	*/
	function getFeedbackList($userid){
		$this->db->select('*');
		$this->db->from('crm_feedback');
		$this->db->where("user_id = $userid");
		$this->db->order_by("created_time","desc");
		$query=$this->db->get();
		return $query->result();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : add commnet on leave request
	*/
	function addCommentOnLeaveRq($leave_id,$comment){
			$data['comment'] = $comment;
			$this->db->where('leave_id',$leave_id);
			$update = $this->db->update('crm_leaves',$data);
			if($update){
				return 1;
			}else{
				return 2;
			}
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get Announcements
	*/
	function getAnnouncements(){
		$this->db->select('*,description as descriptions');
		$this->db->from('announcement');
		//$this->db->where('DATE(`created_time`) = CURDATE()');
		$this->db->order_by("created_time", "desc");
		$this->db->limit(10);
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get today Birthday
	*/
	function getTodayBirthday(){
		$this->db->select('first_name,last_name');
		$this->db->from('users');
		$this->db->where('MONTH(birthdate) = MONTH(NOW()) AND DAY(birthdate) = DAY(NOW())');
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get Today My Meeting
	*/
	function getTodayMyMeeting($userid){
		$this->db->select('m.*,m.description as descriptions,u.first_name,u.last_name');
		$this->db->from('meeting as m');
		$this->db->where("FIND_IN_SET('$userid', m.members)");
		$this->db->where('DATE(`schedule_date`) = CURDATE()');
		$this->db->join('users as u','u.user_id = m.user_id', 'left');
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get Announcements
	*/
	/*function getRemainingFeedbackUserList($roleid){
		$roles = '1,52,15,20,22,49';
		$userId = '102,165,10';
		$this->db->select('u.first_name,u.last_name,d.department_name,(SELECT user_id from crm_feedback as f WHERE u.user_id = f.user_id and YEAR(f.created_time) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(f.created_time) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)) as feedback');
		$this->db->from('crm_users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('u.role_id not in ('.$roles.')');
		$this->db->where('u.user_id not in ('.$userId.')');
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("feedback","ASC");
		$query=$this->db->get();
		return $query->result();
	}*/

	function getRemainingFeedbackUserList($feedback_user){
		$this->db->select('u.first_name,u.last_name,d.department_name,(SELECT user_id from crm_feedback as f WHERE u.user_id = f.user_id and YEAR(f.created_time) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(f.created_time) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)) as feedback');
		$this->db->from('crm_users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('u.user_id in ('.$feedback_user.')');
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("feedback","ASC");
		$query=$this->db->get();
		return $query->result();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get Announcements
	*/
	function getupcominguserleaves(){
		$this->db->select('ls.leave_status as leave_status_name,lt.leave_type as leave_name,u.first_name,u.last_name,d.department_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		//$this->db->join('users as au','au.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("l.is_cancelled != 1");
		$this->db->where("l.leave_start_date > now()");
		$this->db->where("(l.leave_status = 1 OR l.leave_status = 2)");
		//$this->db->order_by("leave_id","desc");
		$this->db->order_by("l.leave_start_date","asc");
		//$this->db->where('u.department_id != '.$id);
		$this->db->limit(10);
		$query=$this->db->get()->result_array();
		return $query;
	}

	function getAllFixedHolidays()
	{
		$this->db->select("*");
		$this->db->from("crm_holidays_lists");
		$this->db->where("status = '0'");
		$this->db->order_by("festival_date","ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getAllFlexibleHolidays()
	{
		$this->db->select("*");
		$this->db->from("crm_holidays_lists");
		$this->db->where("status = '1'");
		$this->db->order_by("festival_date","ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getAllSwappedHolidays()
	{
		$this->db->select("*");
		$this->db->from("crm_holidays_lists");
		$this->db->where("status = '2'");
		$this->db->order_by("festival_date","ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	24-07-2017
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : Get all Junior users' list for leave  by Seniors
	 **/
	function getAllJuniorsForLeave($user_id){
		$this->db->select('leave_users');
		$this->db->from('crm_users');
		$this->db->where('user_id',$user_id);
		$this->db->where('leave_users IS NOT NULL', null, false);
		$this->db->where('status','Active');
		$this->db->order_by("first_name","asc");
		$result = $this->db->get()->result();

		if(!empty($result)){
			$this->db->select('u.user_id,u.first_name,u.last_name,email,d.department_name');
			$this->db->from('users as u');
			$this->db->join('crm_department as d','d.department_id = u.department_id', 'left');
			$this->db->where_in('u.user_id', explode(',',$result[0]->leave_users));
			$this->db->where("u.status = 'Active'");
			$this->db->order_by("u.first_name","asc");
			return  $this->db->get()->result_array();
		}else{
		 return 0;
	  }
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	24-07-2017
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : Get all Junior users' list for feedback  by Seniors
	 **/
	function getAllJuniorsForFeedback($user_id){
		$this->db->select('feedback_users');
		$this->db->from('crm_users');
		$this->db->where('user_id',$user_id);
		$this->db->where('feedback_users IS NOT NULL', null, false);
		$this->db->where('status','Active');
		$this->db->order_by("first_name","asc");
		$result = $this->db->get()->result();

		if(!empty($result)){
			$this->db->select('u.user_id,u.first_name,u.last_name,email,d.department_name');
			$this->db->from('users as u');
			$this->db->join('crm_department as d','d.department_id = u.department_id', 'left');
			$this->db->where_in('u.user_id', explode(',',$result[0]->feedback_users));
			$this->db->where("u.status = 'Active'");
			$this->db->order_by("u.first_name","asc");
			return  $this->db->get()->result_array();
		}else{
		 return 0;
	  }
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Rajendra
	 * @created		  :	11-10-2017
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : Save device token
	 **/
	function saveDeviceToken($user_id, $deviceToken, $deviceType){
		$data = array();
		$data['user_id'] = $user_id;
		$data['deviceToken'] = $deviceToken;
		$data['deviceType'] = $deviceType;
		$data['device_created'] = date("Y-m-d H:i:s");
		$data['device_modified'] = date("Y-m-d H:i:s");

		 if($this->db->insert('crm_device',$data))
		 {
			 return true;
		 }else{
			 return false;
		 }
	}

	function addAnnouncement($data)
	{
		if($this->db->insert('announcement', $data)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	24-07-2017
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : Get my Junior users' list of leaves
	 **/
	function myJuniorsLeaveLists($user_id, $selectedUserId, $leaveStatus){
		$this->db->select('leave_users');
		$this->db->from('crm_users');
		$this->db->where('user_id',$user_id);
		$this->db->where('leave_users IS NOT NULL', null, false);
		$this->db->where('status','Active');
		$this->db->order_by("first_name","asc");
		$result = $this->db->get()->result();

		if(!empty($result)){
			$this->db->select('ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*, d.department_name');
			$this->db->from('leaves as l');
			$this->db->join('users as u','u.user_id = l.user_id', 'left');
			$this->db->join('department as d','d.department_id = u.department_id', 'left');
			$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
			$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
			$this->db->where_in('u.user_id', explode(',',$result[0]->leave_users));
			$this->db->where("u.status = 'Active'");
			$this->db->order_by("leave_id","desc");
			if (!empty($selectedUserId)) {
	        $this->db->where('l.user_id =',$selectedUserId);
	    }
			if (!empty($leaveStatus)) {
	        $this->db->where('l.leave_status',$leaveStatus);
	    }
			$this->db->limit(10);
			return  $this->db->get()->result_array();
		}else{
		 return 0;
	  }
	}

	/**
     * @Method      : POST
     * @Params      :
     * @author        : dipika
     * @created     : 10-05-2018
     * @Modified by   :
     * @Status      :
     * @Comment     : policy list
     **/
  function getAllPolicies()
  {
    $this->db->select("*");
    $this->db->from("crm_policies");
    $this->db->order_by("policiesId","ASC");
    $result = $this->db->get()->result_array();
    return $result;
  }

    /**
     * @Method      : POST
     * @Params      :
     * @author      : dipika
     * @created     : 10-05-2018
     * @Modified by :
     * @Status      :
     * @Comment     : announcement list
     **/
	function getAllAnnouncementsList($limit,$offset){
		$this->db->select('*');
		$this->db->from('crm_announcement');
		$this->db->where('published_status',1);
		$this->db->order_by('created_time',DESC);
		$this->db->limit($limit, $offset);
		$query=$this->db->get();
		return $query->result();
	}

	function getAllAnnouncementsListCount(){
		$this->db->select('*');
		$this->db->from('crm_announcement');
		$this->db->where('published_status',1);
		$this->db->order_by('created_time',DESC);
		$query=$this->db->get();
		return $query->num_rows();
	}

}?>
