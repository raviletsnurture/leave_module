<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class cronjob extends MX_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();
        $this->load->model('cron_model');
        $this->load->model('reward_model');
        $this->load->library('email');
        $this->load->database();
    }

    public function index(){
       $data ="Welcome to HRMS";
       redirect('/', 'refresh');
    }

    //Testing mail
    public function testMail(){
        $mailData['body'] = '<tr>
                             <td colspan="2" style="font-size:14px; color:#FFF;
                               padding:0 40px 20px 40px;">Superadmin password expired. Your new password is <b
                                 style="color:#03A9F4;">aaa</b></td>
                           </tr>';
        $mailData['name'] = 'Super Admin';
        $this->email->from('hrms@letsnurture.com', "HRMS");
        $this->email->to("krunal.letsnurture@gmail.com");
        $this->email->subject('Super Admin Password');
        $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
        $this->email->message($body);
        if($this->email->send(FALSE)){
          echo '<br>mail sent successfully.';
        }else {
            echo '<br>Failed sent successfully.';
        }
        echo $this->email->print_debugger();

    }


    /*
    * Developed By : parth patwala
    * Date : 26/12/2016
    * Modified By :
    * Description : Meeting Reminder for 30min, 3 hours and 24 hours
    */
    public function croneMeetingReminder(){
  	  //Meeing reminder logic
  		$data['meeting_reminder'] = $this->cron_model->getMeetingReminder();
  		echo "<pre>";

  		$sys_date = new DateTime();
  		$sys_date->setTimezone(new DateTimeZone('Asia/Kolkata'));
  		echo "sysdate:";
  		print_r($sys_date->format('Y-m-d H:i:s'));
  		// print_r($data['meeting_reminder'][0]['user_email']);
  		foreach ($data['meeting_reminder'] as $key) {

        $user_email = $key['user_email'];
  			$members_email = $key['members_email'].','.$user_email;
  			$member_name = $key['members_name'];
  			$date_time = $key['schedule_date'].' '.$key['schedule_time'];
  			$date1 = new DateTime($date_time,new DateTimeZone('Asia/Kolkata'));
  			echo "<br>created date : ";
  			print_r($date1->format('Y-m-d H:i:s'));

  			$interval = $sys_date->diff($date1);
  			echo "<br>interval:";
  			echo $interval->format('%D %H:%I:%S');
  			echo '<br>Invert : '.$interval->format('%R').''.$interval->i;
        echo '<br>Invert Month : '.$interval->format('%y years, %m months, %d days').''.$interval->i;
  			echo "<br><br>";
  			$interval_value = $interval->format('%R').''.$interval->i;

  			//Layout for mail
        $date_format =date_create($key['schedule_date']);
				$final_meeting_date = date_format($date_format,"d-M-Y");
        $mailData['description'] = '<tr>
    	      	<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong>Meeting created by '.$key['user_name'].'</strong></p></td>
    	      </tr>
    				<tr>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
    	          <p style="margin:4px 0;"></p>
    						<p><strong>Date: </strong></p>
    					</td>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    	          <p style="margin:4px 0;"></p>
    						<p>'.$final_meeting_date.'</p>
    					</td>
    	      </tr>
    				<tr>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
    	          <p style="margin:4px 0;"></p>
    						<p><strong>Time: </strong></p>
    					</td>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    	          <p style="margin:4px 0;"></p>
    						<p>'.date('g:i a', strtotime($key['schedule_time'])).'</p>
    					</td>
    	      </tr>
    				<tr>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
    	          <p style="margin:4px 0;"></p>
    						<p><strong>Duration: </strong></p>
    					</td>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    	          <p style="margin:4px 0;"></p>
    						<p>'.$key['meeting_duration'].' Minutes</p>
    					</td>
    	      </tr>
    				<tr>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
    	          <p style="margin:4px 0;"></p>
    						<p><strong>Subject: </strong></p>
    					</td>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    	          <p style="margin:4px 0;"></p>
    						<p>'.$key['subject'].'</p>
    					</td>
    	      </tr>
    				<tr>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
    	          <p style="margin:4px 0;"></p>
    						<p><strong>Agenda: </strong></p>
    					</td>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    	          <p style="margin:4px 0;"></p>
    						<p>'.$key['description'].'</p>
    					</td>
    	      </tr>
    				<tr>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
    	          <p style="margin:4px 0;"></p>
    						<p><strong>Location: </strong></p>
    					</td>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    	          <p style="margin:4px 0;"></p>
    						<p>'.$key['location'].'</p>
    					</td>
    	      </tr>';
    				if(!empty($_POST['mom'])){
    					$mailData['description'] .= '<tr>
    						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
    		          <p style="margin:4px 0;"></p>
    							<p><strong>MOM: </strong></p>
    						</td>
    						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    		          <p style="margin:4px 0;"></p>
    							<p>'.$key['mom'].'</p>
    						</td>
    		      </tr>';
    				}
    				$mailData['description'] .= '<tr>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
    						<p style="margin:4px 0;"></p>
    						<p><strong>Participants: </strong></p>
    					</td>
    					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
    						<p style="margin:4px 0;"></p><p>';

					$mailData['description'] .= $member_name;
          $mailData['description'] = rtrim($mailData['description'],",");
  				$mailData['description'] .= '</p></td></tr>';


  				//Layout for mail
            //Interval 30 minutes
  					if(($interval->format('%y') == 0) && ($interval->format('%m') == 0) && ($interval->format('%I') > 28 && $interval->format('%I') <= 33 ) && ((int)$interval_value) > 0 && ($interval->h == 0) && ($interval->d == 0) ){
  						$this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
  						$this->email->bcc($members_email);
  						$this->email->subject('Meeting Reminder - ' . getDateText($key['schedule_time']));
  						$body = $this->load->view('mail_layouts/meetingshedule/reminder_mail.php', $mailData, TRUE);
  						$this->email->message($body);
  						//$this->email->send();
  						echo '<br>mail sent successfully.';
  					}

            //Interval 3 Hour
            /*elseif(($interval->format('%y') == 0) && ($interval->format('%m') == 0) && ($interval->format('%I') > 0 && $interval->format('%I') <= 6 ) && ((int)$interval_value) > 0 && ($interval->h == 3) && ($interval->d == 0) ){
              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
  						$this->email->bcc($members_email);
              $this->email->subject('Meeting Reminder');
              $body = $this->load->view('mail_layouts/meetingshedule/reminder_mail.php', $mailData, TRUE);
              //$body = 'reminder mail for meeting.';
              $this->email->message($body);
              //$this->email->send();
              echo '<br>mail sent successfully.';
            }*/

            //Interval 24 Hour
            elseif(($interval->format('%y') == 0) && ($interval->format('%m') == 0) && ($interval->format('%I') > 0 && $interval->format('%I') <= 06 ) && ((int)$interval_value) > 0 && ($interval->h == 0) && ($interval->d == 1) ){
              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
  						$this->email->bcc($members_email);
              $this->email->subject('Meeting Reminder - ' . getDateText($key['schedule_time']));
              $body = $this->load->view('mail_layouts/meetingshedule/reminder_mail.php', $mailData, TRUE);
              $this->email->message($body);
              //$this->email->send();
              echo '<br>mail sent successfully.';
            }else{
  							echo '<br>Interval not matched.';
  					}
  		}
  		exit;
  	}

    /*
    * Developed By : Rajendra Pondel
    * Date : 16/10/2017
    * Modified By :
    * Description : Meeting MoM Reminder after 4 hours
    */
    public function croneMeetingMoMReminder(){
  	  //Meeing reminder logic

      if(true)
      {
        return true;
      }

  		$data['meeting_reminder'] = $this->cron_model->getMeetingMoMReminder();
  		echo "<pre>";

  		$sys_date = new DateTime();
  		$sys_date->setTimezone(new DateTimeZone('Asia/Kolkata'));
  		echo "sysdate:";
  		print_r($sys_date->format('Y-m-d H:i:s'));
  		 //print_r($data['meeting_reminder']);
  		foreach ($data['meeting_reminder'] as $key) {

      //  print_r($key);

        $user_email = $key['user_email'];
  			$members_email = $key['members_email'].','.$user_email;
  			$member_name = $key['members_name'];
  			$date_time = $key['schedule_date'].' '.$key['schedule_time'];
  			$date1 = new DateTime($date_time,new DateTimeZone('Asia/Kolkata'));
  			echo "<br>created date : ";
  			print_r($date1->format('Y-m-d H:i:s'));

  			$interval = $sys_date->diff($date1);
  			echo "<br>interval:";
  			echo $interval->format('%D %H:%I:%S');
  			echo '<br>Invert : '.$interval->format('%R').''.$interval->i;
        echo '<br>Invert Month : '.$interval->format('%y years, %m months, %d days ').''.$interval->i;
  			echo "<br><br>";
  			$interval_value = $interval->format('%R').''.$interval->i;
        //print_r($interval);
  			//Layout for mail
        echo '<br>' . $interval->h;
        if($interval->h > 4 && $interval->h < 6)
        {
          $date_format =date_create($key['schedule_date']);
  				$final_meeting_date = date_format($date_format,"d-M-Y");
          $mailData['description'] = '<tr>
      	      	<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong>Meeting MoM Fill reminder</strong></p></td>
      	      </tr>

      				<tr>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
      	          <p style="margin:4px 0;"></p>
      						<p><strong>Subject: </strong></p>
      					</td>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
      	          <p style="margin:4px 0;"></p>
      						<p>'.$key['subject'].'</p>
      					</td>
      	      </tr>
      				<tr>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
      	          <p style="margin:4px 0;"></p>
      						<p><strong>Agenda: </strong></p>
      					</td>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
      	          <p style="margin:4px 0;"></p>
      						<p>'.$key['description'].'</p>
      					</td>
      	      </tr>
      				<tr>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
      	          <p style="margin:4px 0;"></p>
      						<p><strong>Location: </strong></p>
      					</td>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
      	          <p style="margin:4px 0;"></p>
      						<p>'.$key['location'].'</p>
      					</td>
      	      </tr>';
      				if(!empty($_POST['mom'])){
      					$mailData['description'] .= '<tr>
      						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
      		          <p style="margin:4px 0;"></p>
      							<p><strong>MOM: </strong></p>
      						</td>
      						<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
      		          <p style="margin:4px 0;"></p>
      							<p>'.$key['mom'].'</p>
      						</td>
      		      </tr>';
      				}
      				$mailData['description'] .= '<tr>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
      						<p style="margin:4px 0;"></p>
      						<p><strong>Participants: </strong></p>
      					</td>
      					<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
      						<p style="margin:4px 0;"></p><p>';

  					$mailData['description'] .= $member_name;
            $mailData['description'] = rtrim($mailData['description'],",");
    				$mailData['description'] .= '</p></td></tr>';


    				//Layout for mail
              //Interval 30 minutes
              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
              $this->email->bcc($user_email);
              $this->email->subject('Meeting MoM Reminder');
              $body = $this->load->view('mail_layouts/meetingshedule/reminder_mail.php', $mailData, TRUE);
              $this->email->message($body);
              //$this->email->send();

              $deviceTokens = getUserDeviceToken($key['user_id'], 0);
         			$Tokenlist = array();
         			foreach($deviceTokens as $value){
         				array_push($Tokenlist,$value->deviceToken);
         			}
         			$notification_message = $key['user_name'] . ' please fill MoM for the meeting ' . $key['subject'];
         			SendNotificationFCMWeb('Meeting MoM Reminder',$notification_message,$Tokenlist);

              echo '<br>mail sent successfully.';
          }
  		}
  		exit;
  	}

    /*
    * Developed By : Krunal
    * Date : 01/04/2017
    * Modified By :
    * Description : Employee will get automatic greeting wishes on their birthday via mail
    */
    public function cronBirthdayGreeting(){
       echo "<pre>";
       //http://localhost/ln-projects/demo/hrms/api/cronjob/cronBirthdayGreeting
        $getBirthdays = getTodayUserBirthday();
        if($getBirthdays){
        foreach ($getBirthdays as $getBirthday) {
               $homeurl = base_url();
               echo $name = $getBirthday['first_name'].' '.$getBirthday['last_name'];
               echo $email = $getBirthday['email'];
               $body = birthdayTemplates($homeurl,$name);
               $this->email->from('hrms@letsnurture.com','HRMS');
             	 $this->email->to($email);
             	 $this->email->subject('Happy Birthday :)');
             	 $this->email->message($body);
             	 //$this->email->send();
             	 echo '<br>mail sent successfully.';
               if($getBirthday['gmail_id'] != ''){
                 $gmail_id = $getBirthday['gmail_id'];
                 $body = birthdayTemplates($homeurl,$name);
                 $this->email->from('hrms@letsnurture.com','HRMS');
               	 $this->email->to($gmail_id);
               	 $this->email->subject('Happy Birthday :)');
               	 $this->email->message($body);
               	 //$this->email->send();
               	 echo '<br>mail sent successfully.';
               }


               //Push Notification Logic
         			$deviceTokens = getAllDeviceToken();
         			$Tokenlist = array();
         			foreach($deviceTokens as $value){
         				array_push($Tokenlist,$value->deviceToken);
         			}
         			$notification_message = $name;
         			$notification_message .= ' Wishing you health, love, wealth, happiness and just everything your heart desires. Happy Birthday.';
         			SendNotificationFCMWeb('Happy Birthday :)',$notification_message,$Tokenlist);
               //Push Notification Logic end
       }

            //Push Notification Logic
              /*$deviceTokens = getAllDeviceToken();
              $Tokenlist = array();
              foreach($deviceTokens as $value){
                array_push($Tokenlist,$value->deviceToken);
              }
              $notification_message = $name;
              $notification_message .= ' Wishing you health, love, wealth, happiness and just everything your heart desires. Happy Birthday.';
              SendNotificationFCMWeb('Happy Birthday :)',$notification_message,$Tokenlist);*/
               //Push Notification Logic end
        }

        /* anniversary notification */
        $getAnniversary = getTodayUserAnniversary();
        if($getAnniversary){
        foreach ($getAnniversary as $getAnniversarys) {
             $homeurl = base_url();
             echo $name = $getAnniversarys['first_name'].' '.$getAnniversarys['last_name'];
             echo $email = $getAnniversarys['email'];

             $body = AnniversaryTemplates($homeurl,$name);
             $this->email->from('hrms@letsnurture.com','HRMS');
             $this->email->to($email);
             $this->email->subject('Happy Anniversary :)');
             $this->email->message($body);
             //$this->email->send();
             echo '<br>mail sent successfully.';
             if($getAnniversarys['gmail_id'] != ''){
               $gmail_id = $getAnniversarys['gmail_id'];
               $body = AnniversaryTemplates($homeurl,$name);
               $this->email->from('hrms@letsnurture.com','HRMS');
               $this->email->to($gmail_id);
               $this->email->subject('Happy Anniversary :)');
               $this->email->message($body);
               //$this->email->send();
               echo '<br>mail sent successfully.';
             }

            }

            //Push Notification Logic
            $deviceTokens = getAllDeviceToken();
            $Tokenlist = array();
            foreach($deviceTokens as $value){
              array_push($Tokenlist,$value->deviceToken);
            }
            $notification_message = $name;
            $notification_message .= ' Wishing you health, love, wealth, happiness and just everything your heart desires. Happy Anniversary.';
            SendNotificationFCMWeb('Happy Anniversary :)',$notification_message,$Tokenlist);
             //Push Notification Logic end

        }



    }

    /*
    * Developed By : Krunal
    * Date : 06/04/2017
    * Modified By :
    * Description : Employee can view their log in log out time on monthly basis
    */
    public function cronEmployLogs(){
      include("./PHPExcel/PHPExcel.php");
      $objPHPExcel = new PHPExcel();
      echo "<pre>";
      //get all file which superadmin upload
      $logFilePaths = $this->cron_model->getEmployLogFilePath();
      if($logFilePaths){
        foreach ($logFilePaths as $logFilePath) {
          //file path
          $logFolder = $logFilePath['file_path'];
          if (is_dir($logFolder)){
            $logFileNames = array_diff(scandir($logFolder), array('..', '.'));
            foreach ($logFileNames as $logFileName) {

              $logFileFullPath = $logFolder.'/'.$logFileName;//file full path
              $logFileFullName = pathinfo($logFolder.'/'.$logFileName);//get user id
              $user_id = $logFileFullName['filename'];
              //PHPExcel code
              try {
                  $inputFileType = PHPExcel_IOFactory::identify($logFileFullPath);
                  $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                  $objPHPExcel = $objReader->load($logFileFullPath);
              } catch(Exception $e) {
                  die('Error loading file "'.pathinfo($logFileFullPath,PATHINFO_BASENAME).'": '.$e->getMessage());
              }
              //  Get worksheet dimensions
              $sheet = $objPHPExcel->getSheet(0);
              $highestRow = $sheet->getHighestRow();
              $highestColumn = $sheet->getHighestColumn();

              //  Loop through each row of the worksheet in turn
              $inOutLog = array();
              $logDate = '';
              $inTime = '';
              $gross_work_hours = '';
              $total_out_time = '';
              $nPunch_work_hours = '';
              $extra_work_hours = '';
              for ($row = 2; $row <= $highestRow; $row++){
                  //  Read a row of data into an array
                  $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
                  if($row == 2){
                   $logDate = $rowData[0][2];
                   $inTime = $rowData[0][3];
                 }

                  if(strpos($rowData[0][0], 'Gross Work Hours: ') > -1){
                    $gross_work_hours = str_replace("Gross Work Hours: ","",$rowData[0][0]).":00";
                  }
                  if(strpos($rowData[0][0], 'Total Out Time: ') > -1){
                    $total_out_time = str_replace("Total Out Time: ","",$rowData[0][0]).":00";
                  }
                  if(strpos($rowData[0][0], 'N-Punch Work Hours: ') > -1){
                    $nPunch_work_hours = str_replace("N-Punch Work Hours: ","",$rowData[0][0]).":00";
                  }
                  if(strpos($rowData[0][0], 'Extra Work Hours: ') > -1){
                    $extra_work_hours = str_replace("Extra Work Hours: ","",$rowData[0][0]).":00";
                  }
                  $inOutLog[]=$rowData;
              }

              $date = DateTime::createFromFormat('d/m/Y',$logDate);
              $logDate =  $date->format('Y-m-d 00:00:00');

              $data['user_id'] = $user_id;
              $data['employ_log'] = serialize($inOutLog);
              $data['log_date'] = $logDate;
              $data['in_time'] = $inTime;
              $data['gross_work_hours'] = $gross_work_hours;
              $data['total_out_time'] = $total_out_time;
              $data['nPunch_work_hours'] = $nPunch_work_hours;
              $data['extra_work_hours'] = $extra_work_hours;
              //this is query for sum time -> SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(extra_work_hours))) FROM `crm_employ_log`
              //check already added log
              $checkEmployLog = $this->cron_model->checkEmployLog($data);
              if(empty($checkEmployLog)){
                //insert into database
                if($this->db->insert('crm_employ_log',$data)){

                  //update status for time log added in database
                  $logFilePathData['status']='1';
                  $this->db->where('id',$logFilePath['id']);
              		$this->db->update('crm_employ_log_file',$logFilePathData);
                   echo "insert <br>";
                }else{
                   echo "Error <br>";
                }
              }else{
                //update status if same file added two time
                $logFilePathData['status']='1';
                $this->db->where('id',$logFilePath['id']);
                $this->db->update('crm_employ_log_file',$logFilePathData);
                echo "All ready added <br>";
              }

            }
          }
        }
      }else{
        echo "No any new file";
      }

    }

    /*
  	* Developed By : Krunal
  	* Date : 10/04/2017
  	* Modified By :
  	* Description : superadmin password expired every 2 week and new password send to superadmin email
  	*/
    public function cronSuperAdminPasswordGenerate(){
      $len = 8;
      $password_new = generate_password($len);
      $data['sPassword'] = md5($password_new);
      $results = $this->cron_model->updateSuperAdminPassword($data);
        $this->db->select('GROUP_CONCAT(sEmail) as memberEmail');
    		$this->db->from('crm_super_admin');
        $query=$this->db->get();
        $superAdminDetail = $query->row_array();
        if($results){
            $mailData['body'] = '<tr>
                                 <td colspan="2" style="font-size:14px; color:#FFF;
                                   padding:0 40px 20px 40px;">Superadmin password expired. Your new password is <b
                                     style="color:#03A9F4;">'.$password_new.'</b></td>
                               </tr>';
            $mailData['name'] = 'Super Admin';
            $this->email->from('hrms@letsnurture.com', "HRMS");
            $this->email->to($superAdminDetail['memberEmail']);
            $this->email->cc('ketan@letsnurture.com');
            $this->email->subject('Super Admin Password');
            $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
            $this->email->message($body);
            $this->email->send();
            $password_new_test = generate_password($len);
            echo $password_new_test.'ln'.$password_new.'ln'.$password_new_test;
            echo '<br>mail sent successfully.';
      }else{
        echo "Password not generated.";
      }
    }


    /*
    * Developed By : Krunal
    * Date : 10/04/2017
    * Modified By :
    * Description : Reminder Pending leave notification if not any action
    * on hold function we are working
    */
    public function cronPendingLeaveReminder(){
      $datetime = new DateTime();
      $datetime->setTimezone(new DateTimeZone('Asia/Kolkata'));
      $datetime->add(new DateInterval("P3D"));
      $startDate = $datetime->format('Y-m-d');
      $pendingLeaveTotalesults = $this->cron_model->getPendingLeaveTotal($startDate);
      $pendingLeaveTotalDepartments = $this->cron_model->getPendingLeaveTotalDepartment($startDate);
      if($pendingLeaveTotalesults){
          //get All TL,PM,HR Email id
          $managementEmails = $this->cron_model->getmanagementEmails();
          $mailData['body'] = '<tr>
                               <td colspan="2" style="font-size:14px; color:#FFF;
                                 padding:0 40px 20px 40px;">There are <b
                                   style="color:#03A9F4;">'.$pendingLeaveTotalesults->total.'</b> pending leaves. Kindly take some action.
                                </td>
                             </tr>';
                             foreach ($pendingLeaveTotalDepartments as $pendingLeaveTotalDepartment) {
                             $mailData['body'] .='<tr>
                                 <td colspan="1" style="font-size:14px; color:#FFF;padding:0 20px 10px 40px;width:110px;">'.$pendingLeaveTotalDepartment->department_name.' :
                                 </td>
                                 <td colspan="1" style="font-size:14px; color:#FFF;padding:0 20px 10px 40px;">
                                  <b style="color:#03A9F4;">'.$pendingLeaveTotalDepartment->totalLeave.'</b>
                                 </td>
                               </tr>';
                             }
          $mailData['name'] = 'Management Team';
          $this->email->from('hrms@letsnurture.com', "HRMS");
          $this->email->to('hrms@letsnurture.com');
          $this->email->bcc($managementEmails);
          $this->email->subject('Total pending leaves');
          $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
          $this->email->message($body);
          //$this->email->send();
          echo '<br>mail sent successfully.';
      }else{
        echo "No panding leave.";
      }
    }


    /*
    * Developed By : Krunal
    * Date : 10/04/2017
    * Modified By :
    * Description : Get list who is on leave next week

    */
    public function cronNextWeekLeaveList(){
      $leaveLists = $this->cron_model->getNextWeekLeaveList();
      $mailData['body'] = '<tr>
                           <td colspan="2" style="font-size:16px; color:#03A9F4;
                             padding:0 40px 20px 40px;">Following are the leave requests on next week.</td>
                         </tr>
                         <tr>
							<td>
								<table class="taskList" cellpadding="0" cellspacing="0">
									<tbody>
                                     <tr>
  										<th>Employee</th>
  										<th>Type</th>
  										<th>Status</th>
  										<th>Date</th>
  										<th>Approval</th>
                                        <th>Reason</th>
  									</tr>';
                                     foreach ($leaveLists as $leaveList) {
                                       if($leaveList['lstatus'] =="Pending" || $leaveList['lstatus'] =="Rejected"){
                                         $color = 'color:#F00;';
                                       }else{
                                          $color = 'color:green;';
                                       }
                                         $mailData['body'].='<tr class="bgred">
      															<td>'.$leaveList['first_name'].' '.$leaveList['last_name'].'</td>
      															<td>'.$leaveList['ltype'].'</td>
      															<td style='.$color.'>'.$leaveList['lstatus'].'</td>
      															<td>'.date('d-M',strtotime($leaveList['leave_start_date'])).' to '.date('d-M',strtotime($leaveList['leave_end_date'])).'</td>
      															<td>'.$leaveList['approved_byFirstName'].'</td>
                                                                <td>'.$leaveList['leave_reason'].'</td>
      														 </tr>';
                                    }
              $mailData['body'] .= '</tbody>
                                </table>
														</td>
													</tr>';
      $managementEmails = $this->cron_model->getmanagementEmails();
      $mailData['name'] = 'Management Team';
      $this->email->from('hrms@letsnurture.com', "HRMS");
      $this->email->to('hrms@letsnurture.com');
      $this->email->bcc($managementEmails);
      $this->email->subject('Total pending leaves');
      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
      $this->email->message($body);
      $this->email->send();
    }

    /*
    * Developed By : Jignasa
    * Date : 07/07/2017
    * Modified By :
    * Description :  Today’s Leave Reminders in Push Notification in HRMS in Morning  ( Notification should only come on in working days)
    - Every Monday to Friday and 4th Saturday of every month HRMS system will send Push notification to the relevant reporting person.
    - We need to exclude our fixed holidays from this list.
    */
    public function cronTodayLeaveListSendRelevantReportingPerson(){
      echo "<pre>";
      $this->db->select("festival_date,festival_name");
      $this->db->from('crm_holidays_lists');
      $this->db->where('DATE(`festival_date`) = CURDATE()');
      $this->db->where("status = '0'");
    	$holidayslists = $this->db->get()->row();
      if(empty($holidayslists)){
      $this->db->select("leave_users,user_id");
      $this->db->from('crm_users');
      $this->db->where("leave_users != ''");
    	$results = $this->db->get()->result_array();
      foreach ($results as $result) {
        $leaveLists = $this->cron_model->getTodayLeaveListByTL($result['leave_users']);
        if(date('w', time()) > 0 && date('w', time()) != 6){ //if today is sunday (w = 0) then dont send email
          $deviceTokens = getUserDeviceToken($result['user_id']);//get token by users
          //print_r($deviceTokens);exit;
          $Tokenlist = array();
          foreach($deviceTokens as $value){
            array_push($Tokenlist,$value->deviceToken);
          }
          $notification_message = 'Following users on leave today '.$leaveLists->usersfirstName;
          SendNotificationFCMWeb('Today’s Leave Reminders' ,$notification_message,$Tokenlist);
        }
      }
    }else{
      echo "Today Holiday";
    }
    }


    /*
    * Developed By : Jignasa
    * Date : 07/07/2017
    * Modified By :
    * Description : Send daily morning 8 AM mail notification (mgmt and TL) that who is on leave for today.
    */
    public function cronTodayLeaveList(){
        echo "<pre>";
       //http://localhost/ln-projects/demo/hrms/api/cronjob/cronTodayLeaveList
       $leaveLists = $this->cron_model->getTodayLeaveList();
       //print_r($leaveLists);
       if(count($leaveLists) != 0){
           $mailData['body'] = '<tr>
                            <td colspan="2" style="font-size:16px; color:#03A9F4;
                              padding:0 40px 20px 40px;">Following is the leave list of today.</td>
                          </tr>
                          <tr>
 							<td>
 								<table class="taskList" cellpadding="0" cellspacing="0">
 									<tbody>
                                      <tr>
   										<th>Employee</th>
   										<th>Type</th>
   										<th>Status</th>
   										<th>Date</th>
   										<th>Approval</th>
                                         <th>Reason</th>
   									</tr>';
                                      foreach ($leaveLists as $leaveList) {
                                        if($leaveList['leave_status'] == 1){
                                           $color = 'color:#eb5768;';
                                        }else if($leaveList['leave_status'] == 2){
                                           $color = 'color:green;';
                                        }else{
                                           $color = 'color:darkgoldenrod;';
                                        }

                                          $mailData['body'].='<tr class="bgred">
       															<td>'.$leaveList['first_name'].' '.$leaveList['last_name'].'</td>
       															<td>'.$leaveList['ltype'].'</td>
       															<td style='.$color.'>'.$leaveList['lstatus'].'</td>
       															<td>'.date('d-M',strtotime($leaveList['leave_start_date'])).' to '.date('d-M',strtotime($leaveList['leave_end_date'])).'</td>
       															<td>'.$leaveList['approved_byFirstName'].'</td>
                                                                 <td>'.$leaveList['leave_reason'].'</td>
       														 </tr>';
                                     }
                                     $mailData['body'] .= '</tbody>
                                                              </table>
                              									</td>
                              									    </tr>';
       }else{
          $mailData['body'] = '<tr>
                    <td colspan="2" style="font-size:16px; color:#03A9F4;
                      padding:0 40px 20px 40px;">Great everyone is present. &#128079; for it. </td>
                    </tr>';
       }
       $managementEmails = $this->cron_model->getmanagementEmails();
       $mailData['name'] = 'Management Team';
       $this->email->from('hrms@letsnurture.com', "HRMS");
       $this->email->to('hrms@letsnurture.com');
       //$this->email->cc('ketan@letsnurture.com');
       $this->email->bcc($managementEmails);
       $this->email->subject("Today's Total Leaves");
       $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
       $this->email->message($body);
       print_r(date('w', time()));
       print_r($body);exit;
       if(date('w', time()) > 0 && date('w', time()) != 6){ //if today is sunday (w = 0) then dont send email
        $this->email->send();
          echo '<br>mail sent successfully.';
       }

    }

   /*
   * Developed By : Jignasa
   * Date : 10/07/2017
   * Modified By :
   * Description : Send daily notification (mgmt and TL) that who is on leave for tomorrow.
   */
   public function cronTomorrowLeaveList(){
       echo "<pre>";
      //http://localhost/ln-projects/demo/hrms/api/cronjob/cronTomorrowLeaveList
      $leaveLists = $this->cron_model->getTomorrowLeaveList();
      if(count($leaveLists) != 0){
          $mailData['body'] = '<tr>
                           <td colspan="2" style="font-size:16px; color:#03A9F4;
                             padding:0 40px 20px 40px;">Following is the leave list of tomorrow.</td>
                         </tr>
                         <tr>
              <td>
                <table class="taskList" cellpadding="0" cellspacing="0">
                  <tbody>
                                     <tr>
                      <th>Employee</th>
                      <th>Type</th>
                      <th>Status</th>
                      <th>Date</th>
                      <th>Approval</th>
                                        <th>Reason</th>
                    </tr>';
                                     foreach ($leaveLists as $leaveList) {
                                       $color = 'color:green;';
                                         $mailData['body'].='<tr class="bgred">
                                    <td>'.$leaveList['first_name'].' '.$leaveList['last_name'].'</td>
                                    <td>'.$leaveList['ltype'].'</td>
                                    <td style='.$color.'>'.$leaveList['lstatus'].'</td>
                                    <td>'.date('d-M',strtotime($leaveList['leave_start_date'])).' to '.date('d-M',strtotime($leaveList['leave_end_date'])).'</td>
                                    <td>'.$leaveList['approved_byFirstName'].'</td>
                                                                <td>'.$leaveList['leave_reason'].'</td>
                                   </tr>';
                                    }
                                    $mailData['body'] .= '</tbody>
                                                             </table>
                                              </td>
                                                  </tr>';
        }else{
           $mailData['body'] = '<tr>
           <td colspan="2" style="font-size:16px; color:#03A9F4;
             padding:0 40px 20px 40px;">Great everyone will present tomorrow. &#128079; for it. </td>
                           </tr>';
        }

      $managementEmails = $this->cron_model->getmanagementEmails();
      $mailData['name'] = 'Management Team';
      $this->email->from('hrms@letsnurture.com', "HRMS");
      $this->email->to('hrms@letsnurture.com');
      $this->email->bcc($managementEmails);
      $this->email->subject("Tomorrow's Total Leaves");
      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
      $this->email->message($body);
      if(date('w', time()) != 6) //if today is sunday (w = 0) then dont send email
      {
         $this->email->send();
         echo '<br>mail sent successfully.';
      }
   }

  /*
  * Developed By : Jignasa
  * Date : 10/07/2017
  * Modified By :
  * Description : Send monthly (every 1st date) mail notification (mgmt and TL) that who has taken leave in last month order by leaves count desc.
  */
  public function cronLastMonthLeaveList(){
      echo "<pre>";
     //http://localhost/ln-projects/demo/hrms/api/cronjob/cronLastMonthLeaveList
     $leaveLists = $this->cron_model->getLastMonthLeaveList();
     if(count($leaveLists) != 0){
         $mailData['body'] = '<tr>
                          <td colspan="2" style="font-size:16px; color:#03A9F4;
                            padding:0 40px 20px 40px;">Following is the leave list of last month.</td>
                        </tr>
                        <tr>
             <td>
               <table class="taskList" cellpadding="0" cellspacing="0">
                 <tbody>
                                    <tr>
                     <th>Employee</th>
                     <th>Department</th>
                     <th style="width:150px;">Total leave days</th>
                   </tr>';
                                    foreach ($leaveLists as $leaveList) {
                                        $mailData['body'].='<tr class="bgred">
                                   <td>'.$leaveList['first_name'].' '.$leaveList['last_name'].'</td>
                                   <td>'.$leaveList['department_name'].'</td>
                                   <td>'.$leaveList['ldays'].'</td>
                                  </tr>';
                                }
                                   $mailData['body'] .= '</tbody>
                                                            </table>
                                             </td>
                                                 </tr>';
       }else{
          $mailData['body'] = '<tr>
          <td colspan="2" style="font-size:16px; color:#03A9F4;
            padding:0 40px 20px 40px;">Great everyone were present in last month. &#128079; for it. </td>
                          </tr>';
       }

     $managementEmails = $this->cron_model->getmanagementEmails();
     $mailData['name'] = 'Management Team';
     $this->email->from('hrms@letsnurture.com', "HRMS");
     $this->email->to('hrms@letsnurture.com');
     $this->email->bcc($managementEmails);
     $this->email->subject("Leave list of last month");
     $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
     $this->email->message($body);
     //$this->email->send();
     echo '<br>mail sent successfully.';
  }

 /*
 * Developed By : Jignasa
 * Date : 12/07/2017
 * Modified By :
 * Description : Send monthly (every 1st date) mail notification (mgmt and TL) that who has taken leave in last month with leave detail.
 */
 public function cronLastMonthAllLeaves(){
     echo "<pre>";
    //http://localhost/ln-projects/demo/hrms/api/cronjob/cronLastMonthAllLeaves
    $leaveLists = $this->cron_model->getLastMonthAllLeaves();
    if(count($leaveLists) != 0){
        $mailData['body'] = '<tr>
                         <td colspan="2" style="font-size:16px; color:#03A9F4;
                           padding:0 40px 20px 40px;">All leaves of last month.</td>
                       </tr>
                       <tr>
            <td>
              <table class="taskList" cellpadding="0" cellspacing="0">
                <tbody>
                                   <tr>
                    <th>Employee</th>
                    <th>Type of Leave</th>
                    <th style="width:150px;">Total leave days</th>
                    <th>Date</th>
                  </tr>';
                                   foreach ($leaveLists as $leaveList) {
                                       $mailData['body'].='<tr class="bgred">
                                  <td>'.$leaveList['first_name'].' '.$leaveList['last_name'].'</td>
                                  <td>'.$leaveList['ltype'].'</td>
                                  <td>'.$leaveList['leave_days'].'</td>
                                  <td>'.$leaveList['leave_start_date'].' To '.$leaveList['leave_end_date'].'</td>
                                 </tr>';
                               }
                                  $mailData['body'] .= '</tbody>
                                                           </table>
                                            </td>
                                                </tr>';
      }else{
         $mailData['body'] = '<tr>
         <td colspan="2" style="font-size:16px; color:#03A9F4;
           padding:0 40px 20px 40px;">Great everyone were present in last month. &#128079; for it. </td>
                         </tr>';
      }

    $managementEmails = $this->cron_model->getmanagementEmails();
    $mailData['name'] = 'Management Team';
    $this->email->from('hrms@letsnurture.com', "HRMS");
    $this->email->to('hrms@letsnurture.com');
    $this->email->bcc($managementEmails);
    $this->email->subject("All leaves of last month");
    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
    $this->email->message($body);
    //$this->email->send();
    echo '<br>mail sent successfully.';
}

 /*
 * Developed By : Jignasa
 * Date : 10/07/2017
 * Modified By :
 * Description : Send Quarterly (every quarter 1st date) mail notification (mgmt and TL) that who has taken leave in last quarter order by leaves count desc.
 */
 public function cronQuarterlyLeaveList(){
     echo "<pre>";
    //http://localhost/ln-projects/demo/hrms/api/cronjob/cronQuarterlyLeaveList
    $leaveLists = $this->cron_model->getQuarterlyLeaveList();
    if(count($leaveLists) != 0){
        $mailData['body'] = '<tr>
                         <td colspan="2" style="font-size:16px; color:#03A9F4;
                           padding:0 40px 20px 40px;">Following is the leave list of last quarter.</td>
                       </tr>
                       <tr>
            <td>
              <table class="taskList" cellpadding="0" cellspacing="0">
                <tbody>
                                   <tr>
                                   <th>Employee</th>
                                   <th>Department</th>
                                   <th style="width:150px;">Total leave days</th>
                  </tr>';
                                   foreach ($leaveLists as $leaveList) {
                                       $mailData['body'].='<tr class="bgred">
                                       <td>'.$leaveList['first_name'].' '.$leaveList['last_name'].'</td>
                                       <td>'.$leaveList['department_name'].'</td>
                                       <td>'.$leaveList['ldays'].'</td>
                                 </tr>';
                               }
                                  $mailData['body'] .= '</tbody>
                                                           </table>
                                            </td>
                                                </tr>';
      }else{
         $mailData['body'] = '<tr>
         <td colspan="2" style="font-size:16px; color:#03A9F4;
           padding:0 40px 20px 40px;">Great everyone were present in last quarter. &#128079; for it. </td>
                         </tr>';
      }

    $managementEmails = $this->cron_model->getmanagementEmails();
    $mailData['name'] = 'Management Team';
    $this->email->from('hrms@letsnurture.com', "HRMS");
    $this->email->to('hrms@letsnurture.com');
    $this->email->bcc($managementEmails);
    $this->email->subject("Leave list of last quarter");
    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
    $this->email->message($body);
    //$this->email->send();
    echo '<br>mail sent successfully.';
  }

  /*
  * Developed By : Jignasa
  * Date : 12/07/2017
  * Modified By :
  * Description : Send Monthly (every quarter 1st date) mail notification (mgmt and TL) of Working hours Count
-  Monthly Report of Total working hours by employee.
  */
  public function cronMonthlyWorkingHoursCount(){
      echo "<pre>";
     //http://localhost/ln-projects/demo/hrms/api/cronjob/cronMonthlyWorkingHoursCount
     $logLists = $this->cron_model->getMonthlyWorkingHoursCount();
     if(count($logLists) != 0){
         $mailData['body'] = '<tr>
                          <td colspan="2" style="font-size:16px; color:#03A9F4;
                            padding:0 40px 20px 40px;">Following is the list of Total working hours of '.strtoupper(date('F-Y', time() - date("j") *24*60*60)).'.</td>
                        </tr>
                        <tr>
             <td>
               <table class="taskList" cellpadding="0" cellspacing="0">
                 <tbody>
                                    <tr>
                                    <th>Employee</th>
                                    <th>Total Working Hours</th>
                                    <th style="width:150px;">Total leave days</th>
                   </tr>';
                                    foreach ($logLists as $logList) {
                                      $leaveList = $this->cron_model->getMonthlyLeaveDaysCount($logList['user_id']);
                                      $days = "0";
                                      if(isset($leaveList[0]['days']) && !empty($leaveList[0]['days'])){
                                        $days = $leaveList[0]['days'];
                                      }

                                        $mailData['body'].='<tr class="bgred">
                                        <td>'.$logList['first_name'].' '.$logList['last_name'].'</td>
                                        <td>'.str_replace(".",":", round($logList['total'],2)).'</td>
                                        <td>'.$days.'</td>
                                  </tr>';
                                }
                                   $mailData['body'] .= '</tbody>
                                                            </table>
                                             </td>
                                                 </tr>';
       }

     $managementEmails = $this->cron_model->getmanagementEmails();
     $mailData['name'] = 'Management Team';
     $this->email->from('hrms@letsnurture.com', "HRMS");
     $this->email->to('krunal.letsnurture@gmail.com');$this->email->to('hrms@letsnurture.com');
     $this->email->bcc($managementEmails);
     $this->email->subject("Total working hours of ".strtoupper(date('F-Y', time() - date("j") *24*60*60)));
     $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
     $this->email->message($body);
     //$this->email->send();
     echo '<br>mail sent successfully.';
  }

   /*
   * Developed By : Jignasa
   * Date : 13/07/2017
   * Modified By :
   * Description : This report will only contain entries where employee has not taken leave but his time entry is missing.. This will help to find out urgent leaves or missed time entries by an employee.
   */
   public function cronMissingEntryCount(){
     echo "<pre>";
    //http://localhost/ln-projects/demo/hrms/api/cronjob/cronMissingEntryCount
    $missingLists = $this->cron_model->getMissingEntryCount();
    if(count($missingLists) != 0){
        $mailData['body'] = '<tr>
                         <td colspan="2" style="font-size:16px; color:#03A9F4;
                           padding:0 40px 20px 40px;">Following is the list of Missing Entries of '.strtoupper(date('F-Y', time() - date("j") *24*60*60)).'.</td>
                       </tr>
                       <tr>
            <td>
              <table class="taskList" cellpadding="0" cellspacing="0">
                <tbody>
                                   <tr>
                                   <th>Employee</th>
                                   <th>Date</th>
                  </tr>';
                                   foreach ($missingLists as $missingList) {
                                        $mailData['body'].='<tr class="bgred">
                                       <td>'.$missingList['userName'].'</td>
                                       <td>'.$missingList['date'].'</td>
                                 </tr>';
                               }
                                  $mailData['body'] .= '</tbody>
                                                           </table>
                                            </td>
                                                </tr>';
      }else{
         $mailData['body'] = '<tr>
         <td colspan="2" style="font-size:16px; color:#03A9F4;
           padding:0 40px 20px 40px;">Great everyone had log entry in '.strtoupper(date('F-Y', time() - date("j") *24*60*60)).'. &#128079; for it. </td>
                         </tr>';
      }

    $managementEmails = $this->cron_model->getmanagementEmails();
    $mailData['name'] = 'Management Team';
    $this->email->from('hrms@letsnurture.com', "HRMS");
    $this->email->to('hrms@letsnurture.com');
    $this->email->bcc($managementEmails);
    $this->email->subject("Missing Entries of ".strtoupper(date('F-Y', time() - date("j") *24*60*60)));
    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
    $this->email->message($body);
    //$this->email->send();
    echo '<br>mail sent successfully.';
 }

  /*
  * Developed By : Jignasa
  * Date : 13/07/2017
  * Modified By :
  * Description : Monthly Late Coming Table  that how many time employee came late based on time table entry.
  */
  public function cronLateComingEntryCount(){
      echo "<pre>";
     //http://localhost/ln-projects/demo/hrms/api/cronjob/cronLateComingEntryCount
     $lateComingLists = $this->cron_model->getLateComingEntryCount();
     if(count($lateComingLists) != 0){
         $mailData['body'] = '<tr>
                          <td colspan="2" style="font-size:16px; color:#03A9F4;
                            padding:0 40px 20px 40px;">Late Coming employees of '.strtoupper(date('F-Y', time() - date("j") *24*60*60)).'.</td>
                        </tr>
                        <tr>
             <td>
               <table class="taskList" cellpadding="0" cellspacing="0">
                 <tbody>
                                    <tr>
                     <th>Employee</th>
                     <th>Late coming days</th>
                   </tr>';
                                    foreach ($lateComingLists as $lateComingList) {
                                        $mailData['body'].='<tr class="bgred">
                                   <td>'.$lateComingList['first_name'].' '.$lateComingList['last_name'].'</td>
                                   <td>'.$lateComingList['count'].'</td>
                                  </tr>';
                                }
                                   $mailData['body'] .= '</tbody>
                                                            </table>
                                             </td>
                                                 </tr>';
       }else{
          $mailData['body'] = '<tr>
          <td colspan="2" style="font-size:16px; color:#03A9F4;
            padding:0 40px 20px 40px;">Great everyone were on time in. &#128079; for it. </td>
                          </tr>';
       }

     $managementEmails = $this->cron_model->getmanagementEmails();
     $mailData['name'] = 'Management Team';
     $this->email->from('hrms@letsnurture.com', "HRMS");
     $this->email->to('hrms@letsnurture.com');
     $this->email->bcc($managementEmails);
     $this->email->subject("Late Coming employees of ".strtoupper(date('F-Y', time() - date("j") *24*60*60)));
     $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
     $this->email->message($body);
     //$this->email->send();
     echo '<br>mail sent successfully.';
  }

  /*
  * Developed By : Jignasa
  * Date : 14/07/2017
  * Modified By :
  * Description : cronLateEntryCount
  */
  public function cronLateEntryCount(){
     //http://localhost/ln-projects/demo/hrms/api/cronjob/cronLateEntryCount
     $lateComingLists = $this->cron_model->getLateEntryCount();
     if(count($lateComingLists) != 0){
        foreach ($lateComingLists as $lateCommer) {
         if($lateCommer['count'] == 1){
           $smile = '&#128516';
         }else if($lateCommer['count'] == 2){
           $smile = '&#128528';
         }else if($lateCommer['count'] == 3){
           $smile = '&#9785';
         }else if($lateCommer['count'] > 3){
           $smile = '&#128565';
         }
        $mailData['body'] = '<tr>
                             <td colspan="2" style="font-size:14px; color:#FFF;
                               padding:0 40px 20px 40px;">You were late by <b
                                 style="color:#03A9F4;">'.$lateCommer['count'].' days </b> '.$smile.';
                              </td>
                           </tr>';
         $mailData['name'] = $lateCommer['first_name'].' '.$lateCommer['last_name'];
         $this->email->from('hrms@letsnurture.com', "HRMS");
         $this->email->to($lateCommer['email']);
         $this->email->bcc('hrms@letsnurture.com');
         $this->email->subject("You were late in ".date('F-Y'));
         $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
         $this->email->message($body);
         if($this->email->send()){
           echo '<br>mail sent successfully.';
         }else {
           echo '<br>Failed sent successfully.';
         }
       }
     }
 }

 /*
 * Developed By : Kaushal
 * Date : 01/01/2019
 * Modified By :
 * Description : send mail and push last month Rewards winner
 */
  public function latestRewardsForMonth(){
    $reward_data = $this->reward_model->getOneMonthReward();
    $mailData['body'] = '';
    $mailData['body'] .= '<tr><td colspan=2 style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">We are pleased to announce our reward systems top 3 Nurturians for month of '.date('F, Y', strtotime('-1 Month')).'.</td></tr>';
    $i=1;
    foreach($reward_data as $data) {
      $mailData['body'] .= '<tr>
                          <td style="font-size:14px; color:#FFF;
                            padding:0 40px 20px 40px; width: 150px;"><b
                              style="color:#03A9F4;">'.$i++.'. '.$data->first_name.' '.$data->last_name.' </b>
                            </td>
                            <td style="font-size:14px; color:#FFF;
                            padding:0 40px 20px 40px;">'.$data->totalpoints.' pt</td>
                        </tr>';
    }
    $mailData['body'] .= '<tr><td colspan=2 style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">We sincerely appreciate your role in contributing towards Let\'s Nurture\'s success. We wish  you to maintain the same spirit in future.</td></tr>';
    if($reward_data[0]->totalpoints == $reward_data[1]->totalpoints) {
      if($reward_data[0]->totalpoints == $reward_data[2]->totalpoints) {
        $mailData['body'] .= '<tr><td colspan=2 style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">We additionally reward the three Nurturer with a single <b style="color:#03A9F4;">movie ticket.</b></td></tr>';
      } else {
        $mailData['body'] .= '<tr><td colspan=2 style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">We additionally reward the first and second Nurturer with a single <b style="color:#03A9F4;">movie ticket.</b></td></tr>';
      }
    } else {
      $mailData['body'] .= '<tr><td colspan=2 style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">We additionally reward the first Nurturer with couple <b style="color:#03A9F4;">movie tickets.</b></td></tr>';
    }
    $mailData['name'] = 'Team';
    $this->email->from('hrms@letsnurture.com', "HRMS");
    $this->email->to('lnteam@letsnurture.com');
    $this->email->bcc('hrms@letsnurture.com');
    $this->email->subject("Reward Winner ".date('F-Y', strtotime('-1 Month')));
    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
    $this->email->message($body);
    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: <hrms@letsnurture.com>' . "\r\n";
    //if($this->email->send()){
      /*Send push Notification*/
      //Send Alert to TLs , PM & HR.
      //$deviceTokens = getSeniorTokens($userId, 'leave_users');
      $deviceTokens = getAllDeviceToken();//send to all user
      $Tokenlist = array();
      foreach($deviceTokens as $value){
        array_push($Tokenlist,$value->deviceToken);
      }
      $notification_message = 'We have announced Rewards Winner of '.date('F-Y', strtotime('-1 Month'));
      //SendNotificationFCMWeb('Announcement of Reward Winner' ,$notification_message,$Tokenlist);
      /*Send push Notification*/
      echo '<br>mail sent successfully.';
    // }else {
    //   echo '<br>Failed sent successfully.';
    // }
  }

  /*
  * Developed By : Kaushal
  * Date : 01/01/2019
  * Modified By :
  * Description : send mail and push last month Rewards winner
  */
   public function RewardWeeklyLeaderPushnotification(){
     $reward_data = $this->reward_model->getWeeklyLeaderReward();
       /*Send push Notification*/
       //Send Alert to TLs , PM & HR.
       //$deviceTokens = getSeniorTokens($userId, 'leave_users');
     if(sizeof($reward_data)>=1){
          $deviceTokens = getAllDeviceToken();//send to all user
          $Tokenlist = array();
          foreach($deviceTokens as $value){
            array_push($Tokenlist,$value->deviceToken);
          }
          $notification_message = 'With '.$reward_data->totalpoints.' pts '.$reward_data->first_name." ".$reward_data->last_name.' is leading monthly reward point';
          //SendNotificationFCMWeb('Announcement of Weekly Leader Reward Winner' ,$notification_message,$Tokenlist);
       }
   }

  /*
  * Developed By : Kaushal
  * Date : 01/01/2019
  * Modified By :
  * Description : send mail last Month Recruitments
  */
  public function lastMonthRecruitments(){
    $recruit_data = $this->reward_model->getLastMonthRecruits();
    $flag = true;
    foreach($recruit_data as $data) {
      if($flag == true) {
        $flag = false;
        $bcc = $data->email;
      } else {
        $bcc .= ','.$data->email;
      }
    }

    $mailData['body'] = '';
    $mailData['body'] .= '<tr><td colspan=2 style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">We are pleased to announce our top 3 leaders of '.date('F, Y', strtotime('-1 Month')).' month through our HRMS reward system.</td></tr>';
    $mailData['body'] .= '<tr>
                            <td style="font-size:14px; color:#FFF; padding:0 40px 20px 40px; width: 150px;">
                              <b style="color:#03A9F4;">'.$i++.'. '.$data->first_name.' '.$data->last_name.' </b>
                            </td>
                            <td style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">'.$data->totalpoints.' pt</td>
                          </tr>';
    $mailData['body'] .= '<tr><td colspan=2 style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">We sincerely appreciate your role in the success and wish that you will maintain the same spirit in future also.</td></tr>';
    $mailData['name'] = 'Team';
    $this->email->from('hrms@letsnurture.com', "HRMS");
    $this->email->to('hrms@letsnurture.com');
    $this->email->bcc($bcc);
    $this->email->subject("Reward Winner ".date('F-Y', strtotime('-1 Month')));
    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
    $this->email->message($body);
    if($this->email->send()){
      echo '<br>mail sent successfully.';
    }else {
      echo '<br>Failed sent successfully.';
    }
  }


  /*
  * Developed By : Krunal
  * Date : 21/01/2019
  * Modified By :
  * Description : send Notification Not Loging User last 15 days
  */
   public function sendNotificationNotLogingUser(){
     $this->db->select('GROUP_CONCAT(user_id) as memberId');
     $this->db->from('crm_users');
     $this->db->where('last_login < NOW() - INTERVAL 15 DAY');
     $query=$this->db->get();
     $membersIds = $query->row_array();

     /*Send push Notification*/
     //Send Alert to TLs , PM & HR.
     //$deviceTokens = getSeniorTokens($userId, 'leave_users');
     //$deviceTokens = getAllDeviceToken();//send to all user
     $deviceTokens = getUserDeviceToken($membersIds['memberId']);//get token by users
     //print_r($deviceTokens);exit;
     $Tokenlist = array();
     foreach($deviceTokens as $value){
       array_push($Tokenlist,$value->deviceToken);
     }
     $notification_message = 'You were offline from last 2 weeks, would not you like to catch up?';
     SendNotificationFCMWeb('HRMS Notification' ,$notification_message,$Tokenlist);
   }

  /*
  * Developed By : Md Mashroor
  * Date : 25/04/2019
  * Modified By :
  * Description : Send mail to Project manager and network team about the joining users list one day before the joining date of the employee
  */

  public function getNewJoiningMail(){
    $newUserList = $this->cron_model->getFutureJoiningEmployees();
    $id_array = array('20','22');
    $this->db->select("*");
    $this->db->from('crm_users');
    $this->db->where_in('role_id',$id_array);
    $this->db->where('status = "Active"');
    $query = $this->db->get()->result_array();

    $maildata['Subject'] = 'List of the employees joining tomorrow';
    $mailData['body'] = '';
    $mailData['name'] = 'All';
    $mailData['body'] .= '<tr>
    <td style="font-size:16px; color:#FFF; padding:0 40px 20px 40px;">
    These are the list of employee/s that will be joining tomorrow:-
    </td>
  </tr>';

    $email = array();
    foreach($query as $query) {
      $email[] = $query['email'];
    }
    $email[] = 'tej.upadhyay@letsnurture.com';
    $bcc = (implode(',',$email));
    $i=1;
    foreach($newUserList as $newUserList){

          $mailData['body'] .= '<tr>
          <td style="font-size:14px; color:#FFF; padding:0 40px 20px 40px; width: 150px;">
            <b style="color:#03A9F4;">'.$i++.'. '.$newUserList['first_name'].' '.$newUserList['last_name'].' </b>
          </td>
        </tr>';
    }
    if(sizeof($newUserList)>=1){
      $this->email->from('hrms@letsnurture.com', "HRMS");
      $this->email->to('hrms@letsnurture.com');
      $this->email->bcc($bcc);
      $this->email->subject($maildata['Subject']);
      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
      $this->email->message($body);
      if($this->email->send()){
        echo '<br>mail sent successfully!';
      }else {
        echo '<br>Mail sending failed!';
      }
    }
  }
  /*
  * Developed By : Md Mashroor
  * Date : 24/05/2019
  * Modified By :
  * Description :Get birthday, leaves, anniversaries for upcoming week
  */
  public function allEventsUpcomingWeek(){
    $birthdayList = $this->cron_model->getAllBirthdaysForUpcomingWeek();
    $leaveList = $this->cron_model->getallLeavesForUpcomingWeek();
    $anniversaryList = $this->cron_model->getallAnniversariesForUpcomingWeek();
    $maildata['Subject'] = 'All Birthdays, Leaves and Anniversaries in the upcoming week';
    $mailData['body'] = '';
    $mailData['name'] = 'All';
    $mailData['body'] = '
    <tr>
      <td>
        <table style="border:1px solid #585858; padding:10px; border-radius:3px;">
          <tr>
            <td colspan="2" style="font-size:16px; color:#FFF; padding:0 0 10px 40px;">
              Upcoming birthday list of the employees this week:-
            </td>
          </tr>';
      $i=1;$j=1;$k=1;
      if(sizeof($birthdayList)>=1){
          $mailData['body'].='
          <tr>
              <th style="width:20%; font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Name</th>
              <th style="width:20%; font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Birthday</th>
          </tr>';
        foreach($birthdayList as $bday){
            $mailData['body'] .= '
          <tr>
            <td style="font-size:14px; color:#FFF; padding:0 0 5px 40px;">
              <b style="color:#03A9F4;">'.$i++.'. '.$bday['first_name'].' '.$bday['last_name'].'</b>
            </td>
            <td style="font-size:14px; color:#FFF; padding:0 0 5px 40px;">
              <b style="color:#03A9F4;">'.date('d-M', strtotime($bday['birthdate'])). ' ('.date('D', strtotime($bday['birthdate'])). ')</b>
            </td>
          </tr>';
        }
      }
      else{
        $mailData['body'].='
        <tr>
          <td colspan="2" style="font-size:14px; color:#FFF; padding:0 0 5px 40px;text-align:center; font-style:italic">
              <b style="color:#03A9F4;">No birthdays this week.</b>
          </td>
        </tr>';
      }
      $mailData['body'].='
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table style="border:1px solid #585858; padding:10px; border-radius:3px;margin-top:40px">
            <tr>
              <td colspan="5" style="font-size:16px; color:#FFF; padding: 0px 0 10px 40px;">
                Upcoming Leave list of the employees this week:-
              </td>
            </tr>';
        if(sizeof($leaveList)>=1){
          $mailData['body'] .= '
            <tr>
              <th style="font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Name</th>
              <th style="width:25%;font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Leave start</th>
              <th style="width:25%;font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Leave end</th>
              <th style="font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Department</th>
              <th style="font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Total Leave</th>
            </tr>';
          foreach($leaveList as $llist){
            $mailData['body'] .= '
              <tr>
                  <td style="font-size:14px; padding:0 0 5px 40px;">
                    <b style="color:#03A9F4;">'.$j++.'. '.$llist['first_name'].' '.$llist['last_name'].'</b>
                  </td>
                  <td style=" font-size:14px; padding:0 0 5px 40px; ">
                    <b style="color:#03A9F4;">'.date('d-M-Y', strtotime($llist['leave_start_date'])). ' ('.date('D', strtotime($llist['leave_start_date'])). ')</b>
                  </td>
                  <td style=" font-size:14px; padding:0 0 5px 40px;">
                    <b style="color:#03A9F4;">'.date('d-M-Y', strtotime($llist['leave_end_date'])). ' ('.date('D', strtotime($llist['leave_end_date'])). ')</b>
                  </td>
                  <td style="font-size:14px;  padding:0 0 5px 40px; text-align:center">
                    <b style="color:#03A9F4;">'.$llist['department_name']. '</b>
                  </td>
                  <td style="font-size:14px; padding:0 0 5px 40px;text-align:center">
                    <b style="color:#03A9F4;">'.$llist['leave_days']. '</b>
                  </td>
              </tr>';
          }
        }else{
          $mailData['body'].='
        <tr>
          <td colspan="5" style="font-size:14px; color:#FFF; padding:0 0 5px 40px;text-align:center; font-style:italic">
              <b style="color:#03A9F4;">No leaves this week.</b>
          </td>
        </tr>
        ';
        }
        $mailData['body'] .= '
      </table>
    </td>
  </tr>
  <tr>
      <td>
        <table style="border:1px solid #585858; padding:10px; border-radius:3px; margin-top:40px">
          <tr>
            <td colspan="2" style="font-size:16px; color:#FFF; padding: 0px 0 10px 40px; ">
              Upcoming Anniversaries of the employees this week:-
            </td>
          </tr>
          ';
  if(sizeof($anniversaryList)>=1){
    $mailData['body'] .= '
        <tr>
              <th style="width:15%;font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Name</th>
              <th style="width:15%;font-size:16px; color:#FFF; padding:0 0 5px 40px; text-align:left">Anniversary date</th>
            </tr>
        <tr>';
    foreach($anniversaryList as $anniversary){
      $mailData['body'] .= '
            <td style="font-size:14px; padding:0 0 5px 40px;">
              <b style="color:#03A9F4;">'.$k++.'. '.$anniversary['first_name'].' '.$anniversary['last_name'].'</b>
            </td>
            <td style=" font-size:14px; padding:0 0 5px 40px; ">
              <b style="color:#03A9F4;">'.date('d-M', strtotime($anniversary['joining_date'])). ' ('.date('D', strtotime($anniversary['joining_date'])). ')</b>
            </td>
        </tr>';
    }
    }else{
      $mailData['body'].='
        <tr>
          <td colspan="2" style="font-size:14px; color:#FFF; padding:0 0 5px 40px;text-align:center; font-style:italic">
              <b style="color:#03A9F4;">No anniversaries this week.</b>
          </td>
        </tr>
        ';
  }
  $mailData['body'] .= '
    </table>
  </td>
</tr>';

      $bcc="hr@letsnurture.com,ketan@letsnurture.com,priyank@letsnurture.com";
      $this->email->from('hrms@letsnurture.com', "HRMS");
      $this->email->to('hrms@letsnurture.com');
      $this->email->bcc($bcc);
      $this->email->subject($maildata['Subject']);
      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
      $this->email->message($body);
      if($this->email->send()){
        echo '<br>mail sent successfully!';
      }else {
        echo '<br>Mail sending failed!';
      }
  }
}
