<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class apiv2 extends MX_Controller {

    function __construct(){
        // Construct the parent class
      parent::__construct();
	    $this->load->model('apiv2_model');
      $this->load->model('cron_model');
      $this->load->library('email');
      $this->load->database();
      date_default_timezone_set("Asia/Kolkata");
    }

    public function index(){
       $data ="Welcome to HRMS API";
      redirect('/', 'refresh');
    }

    /**
  	 * @Method		  :	POST
  	 * @Params		  :
  	 * @author        : krunal
  	 * @created		  :	11-04-2017
  	 * @Modified by	  : Jignasa(24-07-2017)
  	 * @Status		  :
  	 * @Comment		  : User Login
  	 **/
    public function userLogin(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userName']) && !isset($apiData['data']['userName'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the user name.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}if(empty($apiData['data']['password']) && !isset($apiData['data']['password'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the password.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}else{
         $userName = $apiData['data']['userName'];
         $passwd = $apiData['data']['password'];
         $data = $this->apiv2_model->getUserLogin($userName,$passwd);
       	 if($data){
           if(!empty($apiData['data']['deviceToken']) && isset($apiData['data']['deviceToken'])){
             $deviceToken = $apiData['data']['deviceToken'];
             $deviceType = $apiData['data']['deviceType'];
             $result = $this->apiv2_model->saveDeviceToken($data->user_id, $deviceToken, $deviceType);
           }

           unset($data->password);
           unset($data->sub_role_id);
           if($data->role_id == 13){
             $data->Department_leave_module = "1";
           }else if($data->role_id == 20){
             $data->Department_leave_module = "2";
           }  else {
             $data->Department_leave_module = "0";
           }

           if($data->role_id == 13){
             $data->Exceptional_rewards_module = "1";
           }else if($data->role_id == 20){
             $data->Exceptional_rewards_module = "2";
           }else {
             $data->Exceptional_rewards_module = "0";
           }

           if($data->leave_users === NULL){
             $data->is_leave_users = "0";
           }else{
             $data->is_leave_users = "1";
           }
           if($data->feedback_users === NULL){
             $data->is_feedback_users = "0";
           }else{
             $data->is_feedback_users = "1";
           }

           if($data->reliving_date === null){
             $data->reliving_date = "";
           }else{
             $data->reliving_date = $data->reliving_date;
           }

            $response['status'] = "1";
    				$response['message'] = "Login successfully.";
            $response['data'] = $data;
    				header('Content-Type: application/json; charset=utf-8');
    				echo json_encode($response);
    				exit();
     		 }else{
            $response['status'] = "0";
            $response['message'] = "Invalid Username or Password.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
     		 }
      }
    }

    /**
  	 * @Method		  :	POST
  	 * @Params		  :
  	 * @author        : krunal
  	 * @created		  :	11-04-2017
  	 * @Modified by	  : Jignasa(24-07-2017)
  	 * @Status		  :
  	 * @Comment		  : User Logout
  	 **/
    public function userLogout(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the user id.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}if(empty($apiData['data']['deviceToken']) && !isset($apiData['data']['deviceToken'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the deviceToken.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}else{
         $userid = $apiData['data']['userid'];
         $deviceToken = $apiData['data']['deviceToken'];
         $this->db->where('user_id',$userid);
         $this->db->where('deviceToken',$deviceToken);
     	   $this->db->delete('device');
         $response['status'] = "1";
				 $response['message'] = "Logout successfully.";
			   header('Content-Type: application/json; charset=utf-8');
				 echo json_encode($response);
				 exit();
      }
    }

    /**
    * @Method		  :	get
    * @Params		  :
    * @author        : krunal
    * @created		  :	11-04-2017
    * @Modified by	  :
    * @Status		  :
    * @Comment		  : get user List
    **/
    public function getAllUser(){
      $data = $this->apiv2_model->getAllUser();
      if($data){
        $response['status'] = "1";
        $response['message'] = "User list.";
        $response['data'] = $data;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $response['status'] = "0";
        $response['message'] = "Something went WRONG !!";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }
    }


    /**
    * @Method		  :	get
    * @Params		  :
    * @author        : krunal
    * @created		  :	11-04-2017
    * @Modified by	  :
    * @Status		  :
    * @Comment		  : get leave type
    **/
    public function getleavetype(){
      $data = $this->apiv2_model->getAllLeaveType();
      if($data){
        $response['status'] = "1";
        $response['message'] = "Leave type list.";
        $response['data'] = $data;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $response['status'] = "0";
        $response['message'] = "Something went WRONG !!";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }
    }

    /**
    * @Method		  :	POST
    * @Params		  :
    * @author        : krunal
    * @created		  :	11-04-2017
    * @Modified by	  :
    * @Status		  :
    * @Comment		  : get Department
    **/
    public function getDepartment(){
      $data = $this->apiv2_model->getDepartment();
      if($data){
        $response['status'] = "1";
        $response['message'] = "Department list.";
        $response['data'] = $data;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $response['status'] = "0";
        $response['message'] = "Something went WRONG !!";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }
    }


    /**
  	 * @Method		  :	POST
  	 * @Params		  :
  	 * @author      : krunal
  	 * @created		  :	11-04-2017
  	 * @Modified by	: Jignasa(25-07-2017)
  	 * @Status		  :
  	 * @Comment		  : get User leave list
  	 **/
  	public function leaveLists(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the userid.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}else{
         $userId = $apiData['data']['userid'];
         $leaveDatas = $this->apiv2_model->getUserLeaves($userId);
         $output = [];
          if($leaveDatas){
            foreach ($leaveDatas as $leaveData) {
              $data['lstatus'] = $leaveData['lstatus'];
              $data['ltype'] = $leaveData['ltype'];
              $data['first_name'] = $leaveData['first_name'];
              $data['last_name'] = $leaveData['last_name'];
              $data['leave_id'] = $leaveData['leave_id'];
              $data['user_id'] = $leaveData['user_id'];
              $data['department_id'] = $leaveData['department_id'];
              $data['leave_type'] = $leaveData['leave_type'];
              $data['leave_days'] = $leaveData['leave_days'];
              $data['leave_start_date'] = $leaveData['leave_start_date'];
              $data['leave_end_date'] = $leaveData['leave_end_date'];
              $data['leave_reason'] = $leaveData['leave_reason'];
              $data['leave_critical_task'] = $leaveData['leave_critical_task'].'';
              $data['leave_res_assignee'] = $leaveData['leave_res_assignee'].'';
              $data['incidence'] = $leaveData['incidence'].'';
              $data['comment'] = $leaveData['comment'].'';
              $data['leave_status'] = $leaveData['leave_status'];
              $data['approved_by'] = $leaveData['approved_by'].'';
              $data['leave_approved_by'] = $leaveData['leave_approved_by'].'';
              $data['status'] = $leaveData['status'];
              $data['is_cancelled'] = $leaveData['is_cancelled'];
              $data['leave_created'] = $leaveData['leave_created'];
              $data['department_name'] = $leaveData['department_name'];

              if($leaveData['leave_res_assignee'] != '' && $leaveData['leave_res_assignee'] != 0){
                $res_assignee = $this->apiv2_model->getUserDetail($leaveData['leave_res_assignee']);
                if(!empty($res_assignee)){
                  $data['leave_res_assignee_name'] = $res_assignee->first_name." ".$res_assignee->last_name;
                }
              }else{
                $data['leave_res_assignee_name'] = '';
              }
              if($leaveData['approved_by'] != '' && $leaveData['approved_by'] != 0){
                $res_assignee = $this->apiv2_model->getUserDetail($leaveData['approved_by']);
                if(!empty($res_assignee)){
                  $data['approved_by_name'] = $res_assignee->first_name." ".$res_assignee->last_name;
                }
              }else{
                $data['approved_by_name'] = '';
              }
              $output[]=$data;
            }

           $response['status'] = "1";
           $response['message'] = "Leaves list.";
           $response['data'] = $output;
           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();
        }else{
          $response['status'] = "0";
          $response['message'] = "No leaves.";
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }
      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author      : krunal
     * @created		  :	11-04-2017
     * @Modified by	:dipika
     * @updated		  :	dipika(22-05-2018)
     * @Comment		  : Add leave
     **/
  	public function addleaves(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['leavetype']) && !isset($apiData['data']['leavetype'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the leavetype.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['startdate']) && !isset($apiData['data']['startdate'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the startdate.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['enddate']) && !isset($apiData['data']['enddate'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the enddate.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['reason']) && !isset($apiData['data']['reason'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the reason.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['criticaltask']) && !isset($apiData['data']['criticaltask'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the critical task.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['resassignee']) && !isset($apiData['data']['resassignee'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the responsible assignee.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
           $userId = $apiData['data']['userid'];
           $leaveType = $apiData['data']['leavetype'];
           $startDate = $apiData['data']['startdate'];
           $endDate = $apiData['data']['enddate'];
           $reason = $apiData['data']['reason'];
           $criticaltask = $apiData['data']['criticaltask'];
           $resassignee = $apiData['data']['resassignee'];
           $proceed = 1;

            if($leaveType == 7){
              $date = date('Y-m-d');
              $startDate1 = date('Y-m-d',strtotime($startDate));
              if($startDate1 == $date){
                $time = date('H:i:s',strtotime("10 AM"));
                if($time < date('H:i:s')){
                     $proceed = 0;
                     $response['status'] = "0";
                     $response['message'] = "You can not applied leave after 10 AM.";
                     header('Content-Type: application/json; charset=utf-8');
                     echo json_encode($response);
                     exit();
                }else{
                    $proceed = 1;
                }
              }else{
                 $proceed = 1;
              }
            }
              if($proceed == 1){
               $text_leave = $this->apiv2_model->getLeaveTypeText($leaveType);
               $leave_type_text =$text_leave->leave_type;
               $user_details = $this->apiv2_model->getUserDetails($userId);
               $department_id = $user_details->department_id;
               $role_id = $user_details->role_id;

               $departmentName = $this->apiv2_model->getdepartmentName($department_id);
               //krunal
               //if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.They will need to discuss with HR Team for Leave. leave type = EL, Urgent, Birthday Leave, Anniversary Leave, Marriage Leave, Early Leave. ( HR team can add leave)
             	/* $checkTotalNumberLeave = $this->apiv2_model->checkTotalNumberLeave($startDate);
               $totalThisMonthLeave =  $checkTotalNumberLeave->totalLeave;
               $getActiveEmployee = $this->apiv2_model->getActiveEmployee();
               if($totalThisMonthLeave > $getActiveEmployee){
                 $response['status'] = "0";
                 $response['message'] = "You will need to discuss with HR Team for this request type.";
                 header('Content-Type: application/json; charset=utf-8');
                 echo json_encode($response);
                 exit();
               }*/


             //check already add leave same date
             $checkLeaveDate =  $this->apiv2_model->checkLeaveDate($startDate, $userId,$leaveType);
             if($leaveType == 6){
               $early_leave_count = $this->apiv2_model->getearlyleavestatus($startDate,$userId);
             }else{
               $early_leave_count = '';
             }
              //check same day apply
              if(empty($checkLeaveDate)){
               //Eraly leave status check
                if(empty($early_leave_count)){
               	  $data = $this->apiv2_model->getAddLeaves($userId, $leaveType, $startDate, $endDate,$reason,$criticaltask,$resassignee);
               		if($data){
                      $response['status'] = "1";
              				$response['message'] = "Leave added successfully.";
              				header('Content-Type: application/json; charset=utf-8');
              				echo json_encode($response);

                      /*Send push Notification*/
                      //Send Alert to TLs , PM & HR.
                      $deviceTokens = getSeniorTokens($userId, 'leave_users');
                      //$deviceTokens = getAllDeviceToken();//send to all user
                      $Tokenlist = array();
                      foreach($deviceTokens as $value){
                        array_push($Tokenlist,$value->deviceToken);
                      }
                      $notification_message = $user_details->first_name.' '.$user_details->last_name.' requested for '.$leave_type_text.' from '.date('d-M-Y',strtotime($startDate)).' to '.date('d-M-Y',strtotime($endDate));
                      SendNotificationFCMWeb('Request for '.$leave_type_text,$notification_message,$Tokenlist);
                      /*Send push Notification*/

                      //Send leave Alert to TL,PM,HR.
                      $managementEmails = getSeniorEmails($userId, 'leave_users');
                      $mailData['body'] = '<tr>
                                            <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
                                               <b style="color:#03A9F4;">'.$user_details->first_name.' '.$user_details->last_name.' </b> requested for '.$leave_type_text.' from <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($startDate)).' </b>to <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($endDate)).'</b>
                                             </td>
                                         </tr>
                                         <tr>
                                            <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">Reason: '.$reason.'</td>
                                         </tr>';
                      $mailData['name'] = '';
                      $this->email->from('hrms@letsnurture.com', "HRMS");
                      $this->email->to('hrms@letsnurture.com');
                      $this->email->bcc($managementEmails);
                      $this->email->subject("Request for ".$leave_type_text);
                      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                      $this->email->message($body);
                      $this->email->send();

                      //Mail will be send to that responsible person who is going to work or look after the work on behalf of the applicant will be filled in By Applicant.
                      $mailData['body'] = '<tr>
                                            <td colspan="2" style="font-size:14px; color:#FFF;
                                             padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.$user_details->first_name.' '.$user_details->last_name.' </b> is on leave and assigned you responsibility for ongoing projects work.
                                             </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" style="font-size:14px; color:#FFF;
                                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Leave Duration : '.'</b>From <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($startDate)).'</b> To <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($endDate)).'</b>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" style="font-size:14px; color:#FFF;
                                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Reason for leave : '.'</b>'.$reason.'
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" style="font-size:14px; color:#FFF;
                                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Critical Task : '.'</b>'.$criticaltask.'
                                              </td>
                                            </tr>';
                      $mailData['name'] = '';
                      $this->email->from('hrms@letsnurture.com', "HRMS");
                      $this->email->to('hrms@letsnurture.com');
                      $this->email->bcc($managementEmails);
                      $this->email->subject($user_details->first_name.' on leave and assigned you as responsible for work');
                      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                      $this->email->message($body);
                      $this->email->send();

                      //check When more than 3 team members has applied for leave on same day from same department. Send Alert to TLs , PM & HR.
                      $checkTotalLeaveOnSameDays = $this->apiv2_model->checkTotalLeaveOnSameDays($startDate, $endDate,$department_id);
                      if($checkTotalLeaveOnSameDays > 3){
                          $mailData['body'] = '<tr>
                                               <td colspan="2" style="font-size:14px; color:#FFF;
                                                 padding:0 40px 20px 40px;">More than 3 <b style="color:#03A9F4;">'.$departmentName->department_name.'</b> team members has applied for leave on same day <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($startDate)).'</b></td>
                                             </tr>';
                          $mailData['name'] = '';
                          $this->email->from('hrms@letsnurture.com', "HRMS");
                          $this->email->to('hrms@letsnurture.com');
                          $this->email->bcc($managementEmails);
                          $this->email->subject('More than 3 team members has applied for leave');
                          $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                          $this->email->message($body);
                          //$this->email->send();
                      }
                      //When More then 1 TL from same department has applied for Leave please send alert to TL , PM & HR ( Notification + Email )
                      if($role_id = 13){
                        $checkTLTotalLeaveOnSameDays = $this->apiv2_model->checkTLTotalLeaveOnSameDays($startDate, $endDate,$department_id,$role_id);
                        if($checkTLTotalLeaveOnSameDays >= 1){
                            $mailData['body'] = '<tr>
                                                 <td colspan="2" style="font-size:14px; color:#FFF;
                                                   padding:0 40px 20px 40px;">More than 1 <b style="color:#03A9F4;">'.$departmentName->department_name.'</b> TL has applied for leave on same day <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($startDate)).'</b></td>
                                               </tr>';
                            $mailData['name'] = '';
                            $this->email->from('hrms@letsnurture.com', "HRMS");
                            $this->email->to('hrms@letsnurture.com');
                            $this->email->bcc($managementEmails);
                            $this->email->subject('Alert - TL Leave Congestion in '.$departmentName->department_name);
                            $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                            $this->email->message($body);
                            //$this->email->send();
                        }
                      }


             			}else{
                     $response['status'] = "0";
             				 $response['message'] = $data;
             				 header('Content-Type: application/json; charset=utf-8');
             				 echo json_encode($response);
             				 exit();
             		  }
                }else{
                  $response['status'] = "0";
                  $response['message'] = 'You can apply only one early leave per month.';
                  header('Content-Type: application/json; charset=utf-8');
                  echo json_encode($response);
                  exit();
                }
              }else{
               $response['status'] = "0";
               $response['message'] = "Already applied for request on same day!";
               header('Content-Type: application/json; charset=utf-8');
               echo json_encode($response);
               exit();
              }

           }else{
             $response['status'] = "0";
             $response['message'] = "You can not applied leave";
             header('Content-Type: application/json; charset=utf-8');
             echo json_encode($response);
             exit();
            }



      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :Jignasa
     * @updated		  :	12-07-2017
     * @Status		  :
     * @Comment		  : Edit leave
     **/
    public function editLeaves(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['leavetype']) && !isset($apiData['data']['leavetype'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the leavetype.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['startdate']) && !isset($apiData['data']['startdate'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the startdate.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['enddate']) && !isset($apiData['data']['enddate'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the enddate.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['reason']) && !isset($apiData['data']['reason'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the reason.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['criticaltask']) && !isset($apiData['data']['criticaltask'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the critical task.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['resassignee']) && !isset($apiData['data']['resassignee'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the responsible assignee.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['leaveid']) && !isset($apiData['data']['leaveid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the leaveid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $userId = $apiData['data']['userid'];
        $leaveType = $apiData['data']['leavetype'];
        $startDate = $apiData['data']['startdate'];
        $endDate = $apiData['data']['enddate'];
        $reason = $apiData['data']['reason'];
        $criticaltask = $apiData['data']['criticaltask'];
        $resassignee = $apiData['data']['resassignee'];
        $leaveid = $apiData['data']['leaveid'];
        $editLeaves = $this->apiv2_model->editLeaves($userId, $leaveType, $startDate, $endDate,$reason,$criticaltask,$resassignee,$leaveid);
        if($editLeaves){
           $response['status'] = "1";
           $response['message'] = "Your leave has been updated";
           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();
        }else{
          $response['status'] = "0";
          $response['message'] = "Your leave has not been updated. Please contact your suppliers.";
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }

      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : Add leave
     **/
    public function cancelLeaves(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['leaveid']) && !isset($apiData['data']['leaveid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the leaveid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
           $leaveid = $apiData['data']['leaveid'];
           $data = $this->apiv2_model->cancelLeaves($leaveid);
           if($data){
             $userId = $data->userId;
             //Send leave Alert to TL,PM,HR.
             $managementEmails = getSeniorEmails($userId, 'leave_users');
             $mailData['body'] = '<tr>
                                   <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
                                      <b style="color:#03A9F4;">'.$data->first_name.' '.$data->last_name.' </b> requested to cancel on <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($data->leave_start_date)).' </b>to <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($data->leave_end_date)).'</b>
                                    </td>
                                </tr>';
             $mailData['name'] = '';
             $this->email->from('hrms@letsnurture.com', "HRMS");
             $this->email->to('hrms@letsnurture.com');
             $this->email->bcc($managementEmails);
             $this->email->subject("Request for cancel");
             $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
             $this->email->message($body);
             //$this->email->send();

             $result['leaveid'] = $leaveid;
             $result['cancel_status'] = "1";

             $response['status'] = "1";
             $response['message'] = "Your leave has been cancelled";
             $response['data'] = $result;
             header('Content-Type: application/json; charset=utf-8');
             echo json_encode($response);
             exit();
          }else{
            $response['status'] = "0";
            $response['message'] = "Your leave has not been cancelled. Please contact your suppliers.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
          }

      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : update My Department Leave Status
     **/
    public function updateMyDepartmentLeaveStatus(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['leaveid']) && !isset($apiData['data']['leaveid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the leaveid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the userid.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}if(empty($apiData['data']['leave_status']) && !isset($apiData['data']['leave_status'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the leave_status.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}else{
         $leaveid = $apiData['data']['leaveid'];
         $userid = $apiData['data']['userid'];
         $leave_status = $apiData['data']['leave_status'];
         $user_details = $this->apiv2_model->getUserDetails($userid);

         if($user_details){

         $department_id = $user_details->department_id;
         $role_id = $user_details->role_id;

           $data = $this->apiv2_model->updateMyDepartmentLeaveStatus($leaveid,$userid,$leave_status);
           if($data){
             if($leave_status == 2){
               $status = 'Approved';
             }elseif ($leave_status == 3) {
               $status= 'Rejected';
             }elseif ($leave_status == 4) {
               $status = 'Unapproved';
             }else {
               $status = 'Pending';
             }
             $response['status'] = "1";
             $response['message'] = "Leave status has been updated";
             header('Content-Type: application/json; charset=utf-8');
             echo json_encode($response);
             //Send leave Alert to TL,PM,HR.
             $managementEmails = getSeniorEmails($userid, 'leave_users');
             $mailData['body'] = '<tr>
                                   <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
                                      Your Request for <b style="color:#03A9F4;">'.$data->leave_type.' </b> on <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($data->leave_start_date)).' </b> - <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($data->leave_end_date)).'</b>  has been <b style="color:#03A9F4;">'.$status.'</b>
                                    </td>
                                </tr>';
             $mailData['name'] = '<b style="color:#03A9F4;">'.$data->first_name.' '.$data->last_name.'</b>';
             $this->email->from('hrms@letsnurture.com', "HRMS");
             $this->email->to($user_details->email);
             $this->email->bcc($managementEmails);
             $this->email->subject("Request ".$status);
             $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
             $this->email->message($body);
             //$this->email->send();
             exit();
          }else{
            $response['status'] = "0";
            $response['message'] = "Please try again.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
          }

          }else{
            $response['status'] = "0";
            $response['message'] = "User data not found";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
          }

      }
    }

    /**
  	 * @Method		  :	POST
  	 * @Params		  :
  	 * @author        : krunal
  	 * @created		  :	11-04-2017
  	 * @Modified by	  :
  	 * @Status		  :
  	 * @Comment		  : salary Slip
  	 **/
    public function salarySlip(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $userid = $apiData['data']['userid'];
        $salarySlips = $this->apiv2_model->getAllSalarySlip($userid);
        if($salarySlips){
          $output = array();
          foreach ($salarySlips as $salarySlip) {
            $data['filename'] = base_url().'uploads/salary_slips/'.$salarySlip->year.'/'.sprintf("%02d", $salarySlip->month).'/'.$salarySlip->filename;
            $data['year'] = $salarySlip->year;
            $data['months'] = sprintf("%02d", $salarySlip->month);
            $data['month'] = $salarySlip->month;
            $output[] = $data;
          }
          $response['status'] = "1";
          $response['message'] = "List Salary Slip.";
          $response['data'] = $output;
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }else{
          $response['status'] = "0";
          $response['message'] = "Please try again.";
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }
      }
    }


    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : getFeedback
     **/
    public function getFeedback(){
      $response = array();
      $output= array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $userid = $apiData['data']['userid'];
        $feedbacks = $this->apiv2_model->getFeedbackList($userid);
        if($feedbacks){
          foreach($feedbacks as $feedback){
            $data['feedback_id'] = $feedback->feedback_id;
            $data['user_id'] = $feedback->user_id;
            if($feedback->reviewer_id){
              $data['reviewer_id'] = $feedback->reviewer_id;
              $reviewerDetail = $this->apiv2_model->getUserDetail($feedback->reviewer_id);
              if($reviewerDetail){
                $data['reviewer_name'] = $reviewerDetail->first_name.' '.$reviewerDetail->last_name;
              }else{
                $data['reviewer_name'] = '';
              }
            }else{
              $data['reviewer_id'] = '';
              $data['reviewer_name'] = '';
            }
            $data['comment'] = $feedback->comment.'';
            $data['created_time'] = $feedback->created_time;
            $data['overall_score'] = $feedback->overall_score?unserialize($feedback->overall_score):(object)[];
            $data['improvements'] = $feedback->improvements.'';
            $data['projects_and_ratings'] = $feedback->projects_and_ratings?unserialize($feedback->projects_and_ratings):array();
            $data['event_remark'] = $feedback->event_remark.'';
            $data['hr_remark'] = $feedback->hr_remark?unserialize($feedback->hr_remark):array();
            $data['qa_remark'] = $feedback->qa_remark?unserialize($feedback->qa_remark):array();
            $data['ba_remark'] = $feedback->ba_remark?unserialize($feedback->ba_remark):array();
            $data['incidence'] = $feedback->incidence?unserialize($feedback->incidence):array();
            $output[] = $data;
          }

          $response['status'] = "1";
          $response['message'] = "Feedback list.";
          $response['data'] = $output;
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }else{
          $response['status'] = "0";
          $response['message'] = "No Feedback.";
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }
      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : salary Slip
     **/
    public function employLog(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['startdate']) && !isset($apiData['data']['startdate'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the startdate.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['enddate']) && !isset($apiData['data']['enddate'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the enddate.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $userid = $apiData['data']['userid'];
        $startdate = $apiData['data']['startdate'];
        $enddate = $apiData['data']['enddate'];
        $logLists = $this->apiv2_model->getLogList($userid,$startdate,$enddate);
        $totalHours = $this->apiv2_model->getAllTotalTimelog($userid,$startdate,$enddate);


        if($logLists){
          $output = array();
          foreach ($logLists as $logList) {
            $data['employ_log'] = unserialize('a:18:{i:0;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:2:"IN";i:2;s:10:"17/04/2017";i:3;s:5:"12:10";i:4;s:0:"";i:5;s:0:"";}}i:1;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:2:"IN";i:2;s:10:"17/04/2017";i:3;s:5:"16:37";i:4;s:0:"";i:5;s:0:"";}}i:2;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:3:"OUT";i:2;s:10:"17/04/2017";i:3;s:5:"17:24";i:4;s:0:"";i:5;s:0:"";}}i:3;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:2:"IN";i:2;s:10:"17/04/2017";i:3;s:5:"17:47";i:4;s:5:"00:23";i:5;s:0:"";}}i:4;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:3:"OUT";i:2;s:10:"17/04/2017";i:3;s:5:"20:17";i:4;s:0:"";i:5;s:0:"";}}i:5;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:3:"OUT";i:2;s:10:"17/04/2017";i:3;s:5:"20:32";i:4;s:0:"";i:5;s:0:"";}}i:6;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:2:"IN";i:2;s:10:"17/04/2017";i:3;s:5:"20:41";i:4;s:5:"00:08";i:5;s:0:"";}}i:7;a:1:{i:0;a:6:{i:0;s:14:"Device - C-404";i:1;s:3:"OUT";i:2;s:10:"17/04/2017";i:3;s:5:"20:49";i:4;s:0:"";i:5;s:0:"";}}i:8;a:1:{i:0;a:6:{i:0;s:14:"Device - D-605";i:1;s:3:"OUT";i:2;s:10:"17/04/2017";i:3;s:5:"21:17";i:4;s:0:"";i:5;s:0:"";}}i:9;a:1:{i:0;a:6:{i:0;N;i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:10;a:1:{i:0;a:6:{i:0;s:7:"Summary";i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:11;a:1:{i:0;a:6:{i:0;N;i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:12;a:1:{i:0;a:6:{i:0;s:23:"Gross Work Hours: 09:07";i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:13;a:1:{i:0;a:6:{i:0;s:21:"Total Out Time: 00:32";i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:14;a:1:{i:0;a:6:{i:0;s:25:"N-Punch Work Hours: 08:35";i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:15;a:1:{i:0;a:6:{i:0;s:18:"Extra Work Hours: ";i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:16;a:1:{i:0;a:6:{i:0;s:16:"Overtime Hours: ";i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}i:17;a:1:{i:0;a:6:{i:0;s:13:"Status: PR AB";i:1;N;i:2;N;i:3;N;i:4;N;i:5;N;}}}');
            $data['employ_logs'] = $logList->employ_log;
            $data['gross_work_hours'] = $logList->gross_work_hours;
            $data['total_out_time'] = $logList->total_out_time;
            $data['nPunch_work_hours'] = $logList->nPunch_work_hours;
            $data['extra_work_hours'] = $logList->extra_work_hours;
            $data['log_date'] = $logList->log_date;
            $output[] = $data;
          }

          $response['status'] = "1";
          $response['message'] = "Log list.";
          $response['totalHours'] = str_replace(".",":", (string) round($totalHours->total,2));
          $response['data'] = $output;
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }else{
          $response['status'] = "0";
          $response['message'] = "No Log Time.";
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }
      }
    }


    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : add Comment On Leave Rq
     **/
    function addCommentOnLeaveRq(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['leave_id']) && !isset($apiData['data']['leave_id'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the leave id.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['comment']) && !isset($apiData['data']['comment'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the comment.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $leave_id = $apiData['data']['leave_id'];
        $comment = $apiData['data']['comment'];
        $this->apiv2_model->addCommentOnLeaveRq($leave_id,$comment);
        $response['status'] = "1";
        $response['message'] = "Comment added.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
    }
  }

  /**
   * @Method		  :	POST
   * @Params		  :
   * @author        : krunal
   * @created		  :	11-04-2017
   * @Modified by	  :
   * @Status		  :
   * @Comment		  : app Dashboard
   **/
  function appDashboardListing(){
    $response = array();
    $apiData = json_decode(file_get_contents('php://input'),TRUE);
    if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
      $response['status'] = "0";
      $response['message'] = "Please pass the user id.";
      header('Content-Type: application/json; charset=utf-8');
      echo json_encode($response);
      exit();
    }if(empty($apiData['data']['roleid']) && !isset($apiData['data']['roleid'])){
      $response['status'] = "0";
      $response['message'] = "Please pass the role id.";
      header('Content-Type: application/json; charset=utf-8');
      echo json_encode($response);
      exit();
    }else{
      $userid = $apiData['data']['userid'];
      $roleid = $apiData['data']['roleid'];

      $logFilePathData['last_login']=date('Y-m-d H:i:s');
      $this->db->where('user_id',$userid);
      $this->db->update('crm_users',$logFilePathData);

      //get today announcements
      $announcements = $this->apiv2_model->getAnnouncements();

      //get today Birthday
      $birthdayLits = $this->apiv2_model->getTodayBirthday();

      //get today My meeting
      $meetingLits = $this->apiv2_model->getTodayMyMeeting($userid);

      //get user information
      $UserDetail = $this->apiv2_model->getUserDetail($userid);
      $feedback_user = $UserDetail->feedback_users;
      $remainingFeedbackUserList = array();
      if($feedback_user != ''){
         //get role wise remaining Feedback User List
        $feedbackUserLists = $this->apiv2_model->getRemainingFeedbackUserList($feedback_user);
        $remainingFeedbackUserList = array();
        foreach ($feedbackUserLists as $feedbackUserList) {
           if($feedbackUserList->feedback != ''){
             break;
           }
           $data['first_name']=$feedbackUserList->first_name;
           $data['last_name']=$feedbackUserList->last_name;
           $data['department_name']=$feedbackUserList->department_name;
           $remainingFeedbackUserList[] = $data;
        }
      }


      //get upcoming leaves
      $upcomingleaves = $this->apiv2_model->getupcominguserleaves();
      $curr_date = new DateTime();
      $curr_date->format('Y-m-d');
      $date_count = 0;
      $upcomingleavesList =array();
      foreach ($upcomingleaves as $upcomingleave) {
        $user_date = new DateTime($upcomingleave['leave_start_date']);
        if($user_date > $curr_date){
          $start_date = new DateTime($upcomingleave['leave_start_date']);
          $end_date = new DateTime($upcomingleave['leave_end_date']);
          $date_diff = $start_date->diff($end_date);
          if((int)$date_diff->format('%a ') > 0){
              $date_diff_text = '('.((int)$date_diff->format('%a ') + 1).' days)';
          }else{
              $date_diff_text = '('.((int)$date_diff->format('%a ') + 1).' day)';
          }

          $datal['first_name'] = $upcomingleave['first_name'];
          $datal['last_name'] = $upcomingleave['last_name'];
          $datal['date_diff'] = $date_diff_text;
          $datal['leave_name'] = $upcomingleave['leave_name'];
          $datal['department_name']=$upcomingleave['department_name'];
          $datal['leave_date'] = $start_date->format('dS, M Y').' - '.$end_date->format('dS, M Y');
          $upcomingleavesList[]=$datal;
          $date_count++;
        }else{
        continue;
        }
      }

      $response['status'] = "1";
      $response['message'] = "Dashboard.";
      $response['announcements'] = $announcements;
      $response['birthdayLits'] = $birthdayLits;
      $response['remainingFeedbackUserList'] = $remainingFeedbackUserList;
      $response['upcomingleavesList'] = $upcomingleavesList;
      $response['meetingLits'] = $meetingLits;
      header('Content-Type: application/json; charset=utf-8');
      echo json_encode($response);
      exit();
    }
  }

  /**
   * @Method		  :	POST
   * @Params		  :
   * @author        : krunal
   * @created		  :	11-04-2017
   * @Modified by	  :
   * @Status		  :
   * @Comment		  : When some try to login with wrong username/password mail should be send to HR with following detail.
   **/
   public function loginAttempt(){
    $response = array();
    $apiData = json_decode(file_get_contents('php://input'), TRUE);
    if (empty($apiData['data']['userName']) && !isset($apiData['data']['userName'])) {
        $response['status'] = "0";
        $response['message'] = "Please pass the user name.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
    }
    if (empty($apiData['data']['browser']) && !isset($apiData['data']['browser'])) {
        $response['status'] = "0";
        $response['message'] = "Please pass the browser name.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
    }
    if (empty($apiData['data']['os']) && !isset($apiData['data']['os'])) {
        $response['status'] = "0";
        $response['message'] = "Please pass the operating system name.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
    }
    if (empty($apiData['data']['ip']) && !isset($apiData['data']['ip'])) {
        $response['status'] = "0";
        $response['message'] = "Please pass the IP address.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
    }
    if (empty($apiData['data']['password']) && !isset($apiData['data']['password'])) {
        $response['status'] = "0";
        $response['message'] = "Please pass the password.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
    } else {
        $userName = $apiData['data']['userName'];
        $passwd = $apiData['data']['password'];
        $os = $apiData['data']['os'];
        $ipAddress = $apiData['data']['ip'];
        $browser = $apiData['data']['browser'];
        $mailData['body'] = '<tr><td colspan="2" style="font-size:14px; color:#FFF;
                               padding:0 40px 20px 40px;"> User had tried to login with more then 5 failed login attempts</td>
                           </tr>
                                        <tr>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                  <p><strong style="color:#03A9F4;">IP: </strong></p>
                </td>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                  <p>' . $ipAddress . '</p>
                </td>
              </tr>
              <tr>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                  <p><strong style="color:#03A9F4;">Browser: </strong></p>
                </td>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                  <p>' . $browser . '</p>
                </td>
              </tr>
              <tr>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                  <p><strong style="color:#03A9F4;">Operating system: </strong></p>
                </td>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                  <p>' . $os . '</p>
                </td>
              </tr>
              <tr>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
                  <p><strong style="color:#03A9F4;">Username: </strong></p>
                </td>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                  <p>' . $userName . '</p>
                </td>
                </tr>
                <tr>
                                    <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
                  <p><strong style="color:#03A9F4;">Password: </strong></p>
                </td>
                <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                  <p>' . $passwd . '</p>
                </td>
                                    </tr>
                                    <tr>
                                    <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
                  <p><strong style="color:#03A9F4;">Date and Time: </strong></p>
                </td>
                                    <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                  <p>' . date('d-M-Y H:i:s') . '</p>
                </td>
              </tr>';
        $mailData['name'] = 'HR';
        $this->email->from('hrms@letsnurture.com', "HRMS");
        $this->email->to("hrd@letsnurture.com");
        //$this->email->to("krunal.letsnurture@gmail.com");
        $this->email->subject('Failure Login attempt');
        $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
        $this->email->message($body);
        if ($this->email->send(FALSE)) {
            $response['message'] = 'Mail sent successfully.';
            $response['status'] = "1";
        } else {
            $response['message'] = 'Failed sent successfully.';
            $response['status'] = "1";
        }
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
    }
   }
   /**
   * @Method		  :	POST
   * @Params		  :
   * @author              :     kamlesh
   * @created		  :     10-07-2017
   * @Modified by	  :
   * @Status		  :
   * @Comment		  :     Get the list of holidays in year with flexible and swapped holidays leaves.
   **/
   function holidaysList() {
        $data["fixedHolidays"] = $this->apiv2_model->getAllFixedHolidays();
        $data["flexibleHolidays"] = $this->apiv2_model->getAllFlexibleHolidays();
        $data["swappedHolidays"] = $this->apiv2_model->getAllSwappedHolidays();
        if ($data) {
            $response['status'] = "1";
            $response['message'] = "Holidays list.";
            $response['data'] = $data;
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        } else {
            $response['status'] = "0";
            $response['message'] = "Something went WRONG !!";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        }
    }

    /**
    * @Method		  :	get
    * @Params		  :
    * @author     : Jignasa
    * @created		:	24-07-2017
    * @Modified by:
    * @Status		  :
    * @Comment		: Get all Junior users list for leave and feedback by Seniors
    **/
    public function getAllJuniorUsers(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the userid.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}else{
         $userId = $apiData['data']['userid'];
         $data['leave_users'] = $this->apiv2_model->getAllJuniorsForLeave($userId);
         $data['feedback_users'] = $this->apiv2_model->getAllJuniorsForFeedback($userId);

           $response['status'] = "1";
           $response['message'] = "Juniors list for leave and feedback";
           $response['data'] = $data;
           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();


      }
    }

    /**
    * @Method		  :	get
    * @Params		  :
    * @author     : Rajendra
    * @created		:	11-10-2017
    * @Modified by:
    * @Status		  :
    * @Comment		: Save Device device token for android and ios
    **/
    public function saveDeviceToken(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the userid.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}else{
         $userId = $apiData['data']['userid'];
         $deviceToken = $apiData['data']['deviceToken'];
         $deviceType = $apiData['data']['deviceType'];
         $result = $this->apiv2_model->saveDeviceToken($userId, $deviceToken, $deviceType);
         if($result){
           $response['status'] = "1";
           $response['message'] = "Device added sucessfully";
           $response['data'] = $result;
           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();
        }else{
          $response['status'] = "0";
          $response['message'] = "Error in adding device. Try again";
          $response['data'] = $data;
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }

      }
    }

    /**
    * @Method		  :	get
    * @Params		  :
    * @author     : Rajendra
    * @created		:	11-10-2017
    * @Modified by:
    * @Status		  :
    * @Comment		: Save Device device token for android and ios
    **/
    public function addAnnouncement(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['title']) && !isset($apiData['data']['title'])){
  			$response['status'] = "0";
  			$response['message'] = "Please pass the data.";
  			header('Content-Type: application/json; charset=utf-8');
  			echo json_encode($response);
  			exit();
  		}else{

         $data['title']              = trim($apiData['data']['title']);
         $data['description']        = trim($apiData['data']['description']);
         $data['notify_via_email']   = trim($apiData['data']['notify_via_email']);
         $data['published_status']   = 1;

         $result = $this->apiv2_model->addAnnouncement($data);
         if($result)
         {
           $response['status'] = "1";
           $response['message'] = "Announcement added sucessfully";
           $response['data'] = (object) []; //$data;

           $deviceTokens = getAllDeviceToken(0);
           $Tokenlist = array();
           foreach($deviceTokens as $value){
             array_push($Tokenlist,$value->deviceToken);
           }
           //SendNotificationFCMWeb('HRMS - Announcement ', $data['description'],$Tokenlist);

           $deviceTokens = getAllDeviceToken(1);
           $Tokenlist = array();
           foreach($deviceTokens as $value){
             array_push($Tokenlist,$value->deviceToken);
           }
           SendNotificationFCMWeb('HRMS - Announcement ', $data['description'],$Tokenlist);

           $deviceTokens = getAllDeviceToken(2);
           $Tokenlist = array();
           foreach($deviceTokens as $value){
             array_push($Tokenlist,$value->deviceToken);
           }
           SendNotificationFCMWeb('HRMS - Announcement ', $data['description'],$Tokenlist);


           if($data['notify_via_email']) {
     				$data['description'] = '<tr>
                 <td style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">'.trim($data['description']).'</td>
               </tr>';

     			  $this->email->from('hrms@letsnurture.com', "HRMS");
     				$this->email->to('hrms@letsnurture.com');
             $this->email->bcc('lnteam@letsnurture.com,ketan@letsnurture.com');
     				$this->email->subject('LetsNurture Announcement');
     				$body = $this->load->view('mail_layouts/announcement/index.php', $data, TRUE);
     				$this->email->message($body);
     				//if ($this->email->send()):
     					//echo "Mail sent!"; // Mail sent!
     				//else:
     					//echo "There is error in sending mail!"; // There is error in sending mail!
     				//endif;
     			}

           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();
        }else{
          $response['status'] = "0";
          $response['message'] = "Error in adding device. Try again";
          $response['data'] = $data;
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
        }

      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author      : Jignasa
     * @created		  :	24-07-2017
     * @Modified by	: Jignasa(25-07-2017)
     * @Status		  :
     * @Comment		  : My juniors' leave List
     **/
    public function myJuniorsLeaveLists(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
         $userid = $apiData['data']['userid'];
         if(isset($apiData['data']['selectedUserId'])){
           $selectedUserId = $apiData['data']['selectedUserId'];
         }else{
           $selectedUserId = '';
         }
         if(isset($apiData['data']['leaveStatus'])){
           $leaveStatus = $apiData['data']['leaveStatus'];
         }else{
           $leaveStatus = '';
         }
          $leaveDatas = $this->apiv2_model->myJuniorsLeaveLists($userid, $selectedUserId, $leaveStatus);
          $output = [];
          if($leaveDatas){
            foreach ($leaveDatas as $leaveData) {
              $data['lstatus'] = $leaveData['lstatus'];
              $data['ltype'] = $leaveData['ltype'];
              $data['first_name'] = $leaveData['first_name'];
              $data['last_name'] = $leaveData['last_name'];
              $data['leave_id'] = $leaveData['leave_id'];
              $data['user_id'] = $leaveData['user_id'];
              $data['department_id'] = $leaveData['department_id'];
              $data['leave_type'] = $leaveData['leave_type'];
              $data['leave_days'] = $leaveData['leave_days'];
              $data['leave_start_date'] = $leaveData['leave_start_date'];
              $data['leave_end_date'] = $leaveData['leave_end_date'];
              $data['leave_reason'] = $leaveData['leave_reason'];
              $data['leave_critical_task'] = $leaveData['leave_critical_task'].'';
              $data['leave_res_assignee'] = $leaveData['leave_res_assignee'].'';
              $data['incidence'] = $leaveData['incidence'].'';
              $data['comment'] = $leaveData['comment'].'';
              $data['leave_status'] = $leaveData['leave_status'];
              $data['approved_by'] = $leaveData['approved_by'].'';
              $data['leave_approved_by'] = $leaveData['leave_approved_by'].'';
              $data['status'] = $leaveData['status'];
              $data['is_cancelled'] = $leaveData['is_cancelled'];
              $data['leave_created'] = $leaveData['leave_created'];
              $data['department_name'] = $leaveData['department_name'];

             if($leaveData['leave_res_assignee'] != '' && $leaveData['leave_res_assignee'] != 0){
                $res_assignee = $this->apiv2_model->getUserDetail($leaveData['leave_res_assignee']);
                if(!empty($res_assignee)){
                  $data['leave_res_assignee_name'] = $res_assignee->first_name." ".$res_assignee->last_name;
                }
              }else{
                $data['leave_res_assignee_name'] = '';
              }
              if($leaveData['approved_by'] != '' && $leaveData['approved_by'] != 0){
                $res_assignee = $this->apiv2_model->getUserDetail($leaveData['approved_by']);
                if(!empty($res_assignee)){
                  $data['approved_by_name'] = $res_assignee->first_name." ".$res_assignee->last_name;
                }
              }else{
                $data['approved_by_name'] = '';
              }
              $output[]=$data;
            }
             $response['status'] = "1";
             $response['message'] = "Leave list of juniors.";
             $response['data'] = $output;
             header('Content-Type: application/json; charset=utf-8');
             echo json_encode($response);
             exit();
          }else{
            $response['status'] = "0";
            $response['message'] = "No leaves of juniors.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
          }
      }
    }

    /**
       * @Method      : GET
       * @Params      :
       * @author        : dipika
       * @created     : 14-05-2018
       * @
       * @Status      :
       * @Comment     : policy list
       **/
    function getAllPolicies()
    {

      $response = array();
      $policy_list = $this->apiv2_model->getAllPolicies();
      if($policy_list){
          $i = 0;
          foreach ($policy_list as $value) {
              $policy[$i] = array(
                "policies_name"  =>  $value['policies_name'],
                "policies_url"  =>  base_url()."uploads/policy/".$value['policies_file_name'],
              );
          $i++;
          }
          $response['status']  = "1";
          $response['data']  = $policy;
          $response['message'] = "Policy Listing.";
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
      }else{
          $response['status'] = "0";
          $response['message'] = "Data not found.";
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
      }
    }

    /**
       * @Method      : POST
       * @Params      :
       * @author      : dipika
       * @created     : 14-05-2018
       * @Modified by :
       * @Status      :
       * @Comment     : announcement list
       **/
    function announcementsList(){
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      $response = array();
      $page = $apiData['data']['page'];
      if(empty($page)){
        $page = 1;
      }

      // Pagination Code
      $page_number = $page;
      $limit = 15;
        if(isset($page) && $page == 1){ $offset = 0; }else{
            if(isset($page) && $page != '1') {
                $offset = ($page_number*$limit) - ($limit);
            }else{
                $offset = 0;
            }
        }
        $announcement_list = $this->apiv2_model->getAllAnnouncementsList($limit,$offset);
        $totalannouncement = $this->apiv2_model->getAllAnnouncementsListCount();

        $totalpage = ceil((($totalannouncement))/$limit);
        if($page == $totalpage ){
          $next_page = 0;
        }else{
          $next_page = $page+1;
        }
        $data['curr_page'] = (int)$page;
        $data['next_page'] = (int)$next_page;

        if ($announcement_list) {
          $i = 0;
          foreach ($announcement_list as $annou) {
              $announ_list[$i] = array(
                  'id' => $annou->announcement_id,
                  'title' => ($annou->title)?$annou->title: '',
                  'description'=>($annou->description)?$annou->description: '',
                  'date' =>($annou->created_time)?date('Y-m-d',strtotime($annou->created_time)): '',
              );
              $i++;
          }
          $data['announcement_list'] = $announ_list;
          $response["status"] = 1;
          $response["message"] = "Announcement List";
          $response["data"] = $data;
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
        } else {
            $response["status"] = 0;
            $response["message"] = "No record exists";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
        }
    }


   /**
    * @Method		  :	POST
    * @Params		  :
    * @author      : Jignasa
    * @created		  :	24-07-2017
    * @Modified by	: Jignasa(25-07-2017)
    * @Status		  :
    * @Comment		  : get list who is on leave api for Alexa and google home
    **/
   public function getLeaveListsByUserName(){
     $response = array();
     $apiData = json_decode(file_get_contents('php://input'),TRUE);
     if(empty($apiData['data']['username']) && !isset($apiData['data']['username'])){
       $response['status'] = "0";
       $response['message'] = "Please pass the username.";
       header('Content-Type: application/json; charset=utf-8');
       echo json_encode($response);
       exit();
     }else{
       $this->db->select("*");
       $this->db->from('crm_users');
       $this->db->where('username',$apiData['data']['username']);
       $getUserId = $this->db->get()->row();
       if($getUserId->leave_users){
         $leaveLists = $this->cron_model->getTodayLeaveListByTL($getUserId->leave_users);
         if($leaveLists){
              $response['status'] = "1";
              $response['message'] = "Leave list of juniors.";
              $response['data'] = $leaveLists;
              header('Content-Type: application/json; charset=utf-8');
              echo json_encode($response);
              exit();
         }else{
           $response['status'] = "0";
           $response['message'] = "No leaves of juniors.";
           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();
         }
       }else{
         $response['status'] = "0";
         $response['message'] = "Username is wrong.";
         header('Content-Type: application/json; charset=utf-8');
         echo json_encode($response);
         exit();
       }
     }
   }
}?>
