<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class reimbursement extends MX_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();
        $this->load->model('reimbursement_model');
        $this->load->helper('general_helper');
        $this->load->helper('general2_helper');
        $this->load->database();
        $this->load->library('email');
        date_default_timezone_set("Asia/Kolkata");
    }
    public function addReimbursement(){

        header('Content-Type: application/json;');
        $apiData = json_decode($_POST['data'],TRUE);
        $response = array();
        $data = array();
        $data['user_id'] = $apiData['user_id'];
        $data['datetime'] = $apiData['reimbursement_date'];
        $data['amount'] = $apiData['amount'];
        $data['description'] = $apiData['description'];
        $data['payment_mode'] = $apiData['mode_of_payment'];
        $userData = getUserByid($data['user_id']);
        $data['dept_id'] = $userData[0]['department_id'];
        $data['role_id'] = $userData[0]['role_id'];
        $getSeniorEmails = getSeniorEmails($userData[0]['user_id'], "reimbursement_users");
        if($data['user_id'] == ''){
            $response['status'] = "0";
            $response['message'] = "User-id is required.";
            echo json_encode($response);
            exit();
        }
        else if(empty($data['datetime'])){
            $response['status'] = "0";
            $response['message'] = "Reimbursement date is required.";
            echo json_encode($response);
            exit();
        }
        else if(empty($data['amount'])){
            $response['status'] = "0";
            $response['message'] = "Amount is required.";
            echo json_encode($response);
            exit();
        }
        else if(empty($data['description'])){
            $response['status'] = "0";
            $response['message'] = "Mode of payment is required.";
            echo json_encode($response);
            exit();
        }
        else{
            if (isset($_FILES['reciept']) && !empty($_FILES['reciept']) && isset($_FILES['reciept']['name'])) {
                $time=strtotime($data['datetime']);
                $pdfPath = 'uploads/cash_reimbursement';
                makeDir($pdfPath);
                $mainImage = 'reciept';
                $encodedName = rtrim(strtr(base64_encode($data['user_id'].time()), '+/', '-_'), '=');
                $ext = array("pdf", "PDF", "DOCX", "docx", "jpeg", "JPEG","PNG", "png","JPG", "jpg");
                $mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);
                $data['reciept'] = $mainImageName;
                $filepathOnline = base_url().$pdfPath.'/'.$mainImageName;
            }

            $dataSubmit = $this->reimbursement_model->reimbursementAdd($data);
            if($dataSubmit){
                $mailData['name'] = 'All';
                $subject = 'Reimbursement Request From '.$userData[0]['first_name'].' '.$userData[0]['last_name'];
                $mailData['body']='<tr>
                <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">
                '.$userData[0]['first_name'].' '.$userData[0]['last_name'].' has requested for cash reimbursement.
                </p></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">Reimbursement Date : '.$data['datetime'].'</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">Reimbursement Amount : Rs.'.$data['amount'].'/-</td>
                </tr>

                ';
                if(isset($filepathOnline) && $filepathOnline!=''){
                    $mailData['body'].='<tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><a href='.$filepathOnline.'>Click here</a> to view the reciept.</p></td>
                    </tr>';
                }else{
                    $mailData['body'].='<tr>
                    <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"> No reciept uploaded </p></td>
                    </tr>';
                }
                $this->email->from('hrms@letsnurture.com', "HRMS");
                $this->email->to('hrms@letsnurture.com');
                $this->email->bcc($getSeniorEmails);
                $this->email->subject($subject);
                $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                $this->email->message($body);
                $this->email->send();
                // if($this->email->send()){
                //     echo '<br>mail sent successfully!';
                //   }
                // else {
                //     echo '<br>Mail sending failed!';
                // }
                // echo $this->email->print_debugger();

                $deviceTokens = getSeniorTokens($userData[0]['user_id'], "reimbursement_users");
                $Tokenlist = array();
                foreach($deviceTokens as $value){
                    array_push($Tokenlist,$value->deviceToken);
                }
                $notification_message = ''.$userData[0]['first_name'].' '.$userData[0]['last_name'].' has requested for reimbursement';
                SendNotificationFCMWeb('New Reimbursement Request' ,$notification_message,$Tokenlist);
                $response['status'] = "1";
                $response['message'] = "Reimbursement added successfully.";
                echo json_encode($response);
                exit();
            }else{
                $response['status'] = "0";
                $response['message'] = "OOPS! There was a problem in adding request.";
                echo json_encode($response);
                exit();
            }
        }

        }

        public function reimbursementApprove(){

            header('Content-Type: application/json;');
            $apiData = json_decode(file_get_contents('php://input'),TRUE);

            $response = array();
            $data = array();

            $data['approvedBy'] = $apiData['data']['user_id'];
            $data['seniorApproval'] = $apiData['data']['approval_status'];
            if($apiData['data']['reimbursement_id'] == ''){
                $response['status'] = "0";
                $response['message'] = "ReimbursementID is required.";
                echo json_encode($response);
                exit();
            } elseif($data['seniorApproval'] == ''){
                $response['status'] = "0";
                $response['message'] = "Status is required.";
                echo json_encode($response);
                exit();
            }elseif($data['seniorApproval'] != '0' && $data['seniorApproval'] != '1' && $data['seniorApproval'] != '2'){
                $response['status'] = "0";
                $response['message'] = "Invalid status! you can only set 0, 1 or 2 as a status.";
                echo json_encode($response);
                exit();
            }
            else{
                $getReimbursement = getReimbursementByID($apiData['data']['reimbursement_id']);
                if($getReimbursement->seniorApproval == "2"){
                    $response['status'] = "0";
                    $response['message'] = "You cannot update status once it is approved.";
                    echo json_encode($response);
                    exit();
                }
                $updateStatus = $this->reimbursement_model->reimbursementUpdate($data, $apiData['data']['reimbursement_id']);
                if($updateStatus){
                    $response['status'] = "1";
                    $response['message'] = "Reimbursement status updated successfully.";
                    echo json_encode($response);
                    exit();
                }else{
                    $response['status'] = "0";
                    $response['message'] = "Oops! something went wrong";
                    echo json_encode($response);
                    exit();
                }

            }
        }

        public function myReimbursementList(){
            header('Content-Type: application/json; charset=utf-8');
            $apiData = json_decode(file_get_contents('php://input'),TRUE);
            $response = array();
            $page = $apiData['data']['page'];
            $user_id = $apiData['data']['user_id'];
            if(empty($page)){
              $page = 1;
            }
            if(isset($user_id) && !empty($user_id)){
                $user = getUserDetail($user_id);
                if ($user) {
                       // Pagination Code
                       $page_number = $page;
                       $limit = 15;
                        if(isset($page) && $page == 1){ $offset = 0; }else{
                            if(isset($page) && $page != '1') {
                                $offset = ($page_number*$limit) - ($limit);
                            }else{
                                $offset = 0;
                            }
                        }
                        $my_reimbursement = $this->reimbursement_model->myReimbursmentsList($user_id,$limit,$offset);
                        $totalReimbursement = $this->reimbursement_model->myReimbursmentsListCount($user_id);

                        $totalpage = ceil((($totalReimbursement))/$limit);
                        if($page == $totalpage ){
                          $next_page = 0;
                        }else{
                          $next_page = $page+1;
                        }
                        $data['curr_page'] = (int)$page;
                        $data['next_page'] = (int)$next_page;

                    if ($my_reimbursement) {
                        $i = 0;
                        foreach ($my_reimbursement as $row) {

                            // Senior Approval Status
                            if($row['seniorApproval'] == "0"){
                                $status = "Rejected";
                            }elseif($row['seniorApproval'] == "1"){
                                $status = "Pending";
                            }else{
                                $status = "Approved";
                            }

                            if($row['approvedBy'] != null && $row['approvedBy'] != ''){
                                $seniorApprovalDetail = getUserDetail($row['approvedBy']);
                                $seniorApprovalName = $seniorApprovalDetail->first_name.' '.$seniorApprovalDetail->last_name;
                            }

                            // HR Approval status
                            if($row['status'] == "0"){
                                $HR_approval_status = "Rejected";
                            }elseif($row['status'] == "1"){
                                $HR_approval_status = "Pending";
                            }else{
                                $HR_approval_status = "Approved";
                            }

                            if($row['seniorApproval'] == "1"){
                                $approve_by_TL = $status;
                            }else{
                                $approve_by_TL =  $status.' by '. $seniorApprovalName;
                            }

                            // Reciept
                            if($row['reciept']!=null && $row['reciept']!=''){
                                $reciept = site_url().'uploads/cash_reimbursement/'.$row['reciept'];
                            }else{
                                $reciept = "";
                            }
                            $reimbursement_list[$i] = array(
                                'reimbursement_id' => $row['reimbursement_id'],
                                'reimbursement_date' => date('Y-m-d h:i a', strtotime($row['datetime'])),
                                'amount' =>$row['amount'],
                                'reimbursement_description' => $row['description'],
                                'description' => $row['description'],
                                'payment_mode' =>$row['payment_mode'],
                                'reciept' =>$reciept,
                                'approve_by_TL' =>$approve_by_TL,
                                'approve_by_HR' => $HR_approval_status,
                                'created_at' => date('Y-m-d', strtotime($row['created_at']))
                            );
                            $i++;
                        }
                        $data['reimbursement_list'] = $reimbursement_list;
                        $response["status"] = 1;
                        $response["message"] = "Reimbursement List";
                        $response["data"] = $data;
                        echo json_encode($response);
                    } else {
                        $response["status"] = 0;
                        $response["message"] = "No record exists";
                        echo json_encode($response);
                    }
                } else {
                    $response["status"] = 0;
                    $response["message"] = "User not found";
                    echo json_encode($response);
                }
            } elseif ($user_id == '') {
                $response["status"] = 0;
                $response["message"] = "Enter User Id";
                echo json_encode($response);
            }
        }

        function deleteReimbursement(){
            header('Content-Type: application/json;');
            $apiData = json_decode(file_get_contents('php://input'),TRUE);
            $response = array();
            $reimbursement_id = $apiData['data']['reimbursement_id'];
            if($reimbursement_id == ''){
                $response['status'] = "0";
                $response['message'] = "Reimbursement id is required.";
                echo json_encode($response);
                exit();
            }else{
                $delete = $this->reimbursement_model->deleteReimbursement($reimbursement_id);
                if($delete){
                    $response["status"] = 1;
                    $response["message"] = "Reimbursement deleted successfully!";
                    echo json_encode($response);
                }else{
                    $response["status"] = 0;
                    $response["message"] = "Oops! something went wrong";
                    echo json_encode($response);
                }
            }

        }

        public function myDepartmentReimbursementList(){
            header('Content-Type: application/json; charset=utf-8');
            $apiData = json_decode(file_get_contents('php://input'),TRUE);
            $response = array();
            $page = $apiData['data']['page'];
            $user_id = $apiData['data']['user_id'];
            if(empty($page)){
              $page = 1;
            }
            if(isset($user_id) && !empty($user_id)){
                $user = getUserDetail($user_id);
                if ($user) {
                       // Pagination Code
                       $page_number = $page;
                       $limit = 15;
                        if(isset($page) && $page == 1){ $offset = 0; }else{
                            if(isset($page) && $page != '1') {
                                $offset = ($page_number*$limit) - ($limit);
                            }else{
                                $offset = 0;
                            }
                        }
                        $user_id_array = explode("," ,$user->reimbursement_users);
                        $my_department_reimbursement = $this->reimbursement_model->myDepartmentReimbursmentsList($user_id_array,$limit,$offset);
                        $totalReimbursement = $this->reimbursement_model->myDepartmentReimbursmentsListCount($user_id_array);

                        $totalpage = ceil((($totalReimbursement))/$limit);
                        if($page == $totalpage ){
                          $next_page = 0;
                        }else{
                          $next_page = $page+1;
                        }
                        $data['curr_page'] = (int)$page;
                        $data['next_page'] = (int)$next_page;

                    if ($my_department_reimbursement) {
                        $i = 0;
                        foreach ($my_department_reimbursement as $row) {

                            // Senior Approval Status
                            if($row['seniorApproval'] == "0"){
                                $status = "Rejected";
                            }elseif($row['seniorApproval'] == "1"){
                                $status = "Pending";
                            }else{
                                $status = "Approved";
                            }

                            if($row['approvedBy'] != null && $row['approvedBy'] != ''){
                                $seniorApprovalDetail = getUserDetail($row['approvedBy']);
                                $seniorApprovalName = $seniorApprovalDetail->first_name.' '.$seniorApprovalDetail->last_name;
                            }else{
                                $seniorApprovalName = '';
                            }

                            // HR Approval status
                            if($row['status'] == "0"){
                                $HR_approval_status = "Rejected";
                            }elseif($row['status'] == "1"){
                                $HR_approval_status = "Pending";
                            }else{
                                $HR_approval_status = "Approved";
                            }

                            if($row['seniorApproval'] == "1"){
                                $approve_by_TL = $status;
                            }else{
                                $approve_by_TL =  $status.' by '. $seniorApprovalName;
                            }

                            // Reciept
                            if($row['reciept']!=null && $row['reciept']!=''){
                                $reciept = site_url().'uploads/cash_reimbursement/'.$row['reciept'];
                            }else{
                                $reciept = "";
                            }

                            $getUserDetail = getUserDetail($row['user_id']);
                            $reimbursement_list[$i] = array(
                                'reimbursement_id' => $row['reimbursement_id'],
                                'name' => $getUserDetail->first_name.' '.$getUserDetail->last_name,
                                'amount' =>$row['amount'],
                                'reimbursement_date' => date('Y-m-d h:i a', strtotime($row['datetime'])),
                                'reimbursement_description' => $row['description'],
                                'description' => $row['description'],
                                'payment_mode' =>$row['payment_mode'],
                                'your_approval' =>$status,
                                'approval_by' => $seniorApprovalName,
                                'HR_approval' =>$HR_approval_status,
                                'reciept' =>$reciept,
                                'created_at' => date('Y-m-d', strtotime($row['created_at']))
                            );
                            $i++;
                        }
                        $data['reimbursement_list'] = $reimbursement_list;
                        $response["status"] = 1;
                        $response["message"] = "Reimbursement List";
                        $response["data"] = $data;
                        echo json_encode($response);
                    } else {
                        $response["status"] = 0;
                        $response["message"] = "No record exists";
                        echo json_encode($response);
                    }
                } else {
                    $response["status"] = 0;
                    $response["message"] = "User not found";
                    echo json_encode($response);
                }
            } elseif ($user_id == '') {
                $response["status"] = 0;
                $response["message"] = "Enter User Id";
                echo json_encode($response);
            }
        }
}
