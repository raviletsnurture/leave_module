<?php

/**
 * Use for user document and skill update.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller
{

    /**
     * Default construct.
     */
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('user_model');
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author             :     Kamlesh
     * @created		  :	10-07-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  :     upload document on the server.
     * */
    public function uploadDocument()
    {
        $response = array();
        $decodeText = html_entity_decode($_POST['data']);
        $apiData = json_decode($decodeText, true);
        if (empty($apiData['data']['userid'])) {
            $response['status'] = "0";
            $response['message'] = "Please pass the user id.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        }
        if (empty($apiData['data']['documentName'])) {
            $response['status'] = "0";
            $response['message'] = "Please enter the document name.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        }
        if (empty($_FILES['document']) && !isset($_FILES['document']['name'])) {
            $response['status'] = "0";
            $response['message'] = "Please select the document files.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        } else {
            $id = $apiData['data']['userid'];
            $documentName = $_FILES['document']['name'];
            if ($documentName) {
                $addData['document_name'] = $apiData['data']['documentName'];
                $addData['user_id'] = $id;
                $pdfPath = './uploads/document';
                $mainImage = 'document';
                if ($_FILES[$mainImage]['size'] > 2097152) {
                    $response['status'] = "0";
                    $response['message'] = "File too large. File must be less than 2Mb.";
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode($response);
                    exit();
                }
                // avoid _ / = in encodedname as it might result in 404 error for filenames
                $encodedName = rtrim(strtr(base64_encode(time()), '+/', '-_'), '=');
                $ext = array("pdf", "docx", "doc", "jpg", "jpeg", "png", "odt", "ods");
                if ($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != '') {
                    $mainImageName = savePDF($mainImage, $pdfPath, $ext, $encodedName);
                    if ($mainImageName !== false) {
                        $addData['file_name'] = $mainImageName;
                    } else {
                        $response['status'] = "0";
                        $response['message'] = "On Snap ! Document not uploaded, verify file and upload again!";
                        header('Content-Type: application/json; charset=utf-8');
                        echo json_encode($response);
                        exit();
                    }
                } else {
                    $response['status'] = "0";
                    $response['message'] = "On Snap ! Forgot to upload file? please upload again!";
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode($response);
                    exit();
                }
                $data = $this->user_model->addDocument($addData);
                $response['status'] = "1";
                $response['message'] = "Document added successfully!";
                $response['data'] = $data;
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            }
        }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author             :     Kamlesh
     * @created		  :	10-07-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  :     Get document list.
     * */
    public function getDocumentList()
    {
        $response = array();
        $apiData = json_decode(file_get_contents('php://input'), TRUE);
        if (empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])) {
            $response['status'] = "0";
            $response['message'] = "Please pass the user id.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        } else {
            $id = $apiData['data']['userid'];
            $getDocumentLists = $this->user_model->getDocument($id);
            $output=array();
            foreach ($getDocumentLists as $getDocumentList) {
              $data['document_id']=$getDocumentList->document_id;
              $data['user_id']=$getDocumentList->user_id;
              $data['document_name']=$getDocumentList->document_name;
              $data['file_name']= base_url().'uploads/document/'.$getDocumentList->file_name;
              $data['created_at']=$getDocumentList->created_at;
              $output[]=$data;
            }
            if (!empty($getDocumentLists)) {
                $response['status'] = "1";
                $response['message'] = "Documents List.";
                $response['data'] = $output;
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            } else {
                $response['status'] = "0";
                $response['message'] = "No documents found.";
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            }
        }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author             :     Kamlesh
     * @created		  :	10-07-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  :     Delete document by id.
     * */
    public function deleteDocument()
    {
        $response = array();
        $apiData = json_decode(file_get_contents('php://input'), TRUE);
        if (empty($apiData['data']['documentId']) && !isset($apiData['data']['documentId'])) {
            $response['status'] = "0";
            $response['message'] = "Please pass the document id.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        } else {
            $id = $apiData['data']['documentId'];
            $data = $this->user_model->getDocumentById($id);
            $path = './uploads/document/';
            if (!empty($data)) {
                $this->user_model->deleteDocument($id);
                unlink($path . $data[0]->file_name);
                $response['status'] = "1";
                $response['message'] = "Delete document successfully.";
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            } else {
                $response['status'] = "0";
                $response['message'] = "Record doesn't exist.";
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            }
        }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author            :     Kamlesh
     * @created		  :	10-07-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  :     Add skill.
     * */
    public function addSkill()
    {
        $response = array();
        $apiData = json_decode(file_get_contents('php://input'), TRUE);
        if (empty($apiData['data']['userid'])) {
            $response['status'] = "0";
            $response['message'] = "Please pass the user id.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        }
        if (empty($apiData['data']['skill'])) {
            $response['status'] = "0";
            $response['message'] = "Please pass the skill.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        }
         else {
            $data['user_id'] = $apiData['data']['userid'];
            if ($apiData['data']['skill'] != '') {
                $data['skill_id'] = $apiData['data']['skill'];
                $data['ex_year'] = $apiData['data']['expYear'];
                $data['ex_month'] = $apiData['data']['expMonth'];
                $getSkillAlreadyExist = $this->user_model->getSkillDetail($data['skill_id'],$data['user_id']);
                if(empty($getSkillAlreadyExist)) {
                    $data = $this->user_model->addSkill($data);
                    if ($data == 1) {
                        $response['status'] = "1";
                        $response['message'] = "Skill added successfully.";
                        header('Content-Type: application/json; charset=utf-8');
                        echo json_encode($response);
                        exit;
                    }
                }else{
                    $response['status'] = "0";
                    $response['message'] = "Skill already exist for your profile.";
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode($response);
                    exit;
                }
            } else {
                $response['status'] = "0";
                $response['message'] = "Skill not available on the list.";
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            }
        }
    }


    /**
     * @Method		  :	POST
     * @Params		  :
     * @author            :     Kamlesh
     * @created		  :	10-07-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  :     Get skill list.
     * */
    public function getSkillList()
    {
        $response = array();
        $apiData = json_decode(file_get_contents('php://input'), TRUE);
        if (empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])) {
            $response['status'] = "0";
            $response['message'] = "Please pass the user id.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        } else {
            $id = $apiData['data']['userid'];
            $getSkillList = $this->user_model->getAllSkills($id);
            if (!empty($getSkillList)) {
                $response['status'] = "1";
                $response['message'] = "Skill List.";
                $response['data'] = $getSkillList;
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            } else {
                $response['status'] = "0";
                $response['message'] = "No skill found.";
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            }
        }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author            :     Kamlesh
     * @created		  :	10-07-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  :     Delete skill.
     * */
    public function deleteSkill()
    {
        $response = array();
        $apiData = json_decode(file_get_contents('php://input'), TRUE);
        if (empty($apiData['data']['uskill_id'])) {
            $response['status'] = "0";
            $response['message'] = "Please pass the uskill_id.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
        } else {
            $uskill_id = $apiData['data']['uskill_id'];
            $getSkill = $this->user_model->getSkillById($uskill_id);
            if (!empty($getSkill)) {
                $this->user_model->deleteSkill($uskill_id);
                $response['status'] = "1";
                $response['message'] = "Skill deleted successfully.";
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            } else {
                $response['status'] = "0";
                $response['message'] = "Record doesn't exist.";
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                exit;
            }
        }
    }
    /**
     * @Method		  :	POST
     * @Params		  :
     * @author      : Jignasa
     * @created		  :	14-07-2017
     * @Modified by	:
     * @Status		  :
     * @Comment		  :Get list of all skills.
     * */
    public function getAllSkillList()
    {
        $response = array();
        $apiData = json_decode(file_get_contents('php://input'), TRUE);
        $getAllSkillList = $this->user_model->getAllSkillsList();
        if (!empty($getAllSkillList)) {
            $response['status'] = "1";
            $response['message'] = "list of all skills.";
            $response['data'] = $getAllSkillList;
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit;
        } else {
            $response['status'] = "0";
            $response['message'] = "No skill found.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit;
        }
    }

}
