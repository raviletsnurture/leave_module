<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class reward extends MX_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();
        $this->load->model('reward_model');
        $this->load->model('apiv2_model');
        $this->load->helper('general_helper');
        $this->load->database();
        $this->load->library('email');
    }
    public function index(){
        $first_day_of_month = date('m-01-Y');
        $current_date = date('m-d-Y');
        if($first_day_of_month == $current_date){
            $this->reward_model->updatePMrewardpoint();
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : get user reward list
     **/
    public function getReward(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $page = $apiData['data']['page'];
        $user_id = $apiData['data']['user_id'];
        if(empty($page)){
          $page = 1;
        }
        if(isset($user_id) && !empty($user_id)){
            $user = $this->reward_model->getUserDetail($user_id);
            if ($user) {
                   // Pagination Code
                   $page_number = $page;
                   $limit = 15;
                    if(isset($page) && $page == 1){ $offset = 0; }else{
                        if(isset($page) && $page != '1') {
                            $offset = ($page_number*$limit) - ($limit);
                        }else{
                            $offset = 0;
                        }
                    }
                    $user_reward_list = $this->reward_model->getAllRewardsHistory($user_id,$limit,$offset);
                    $totalrewards = $this->reward_model->getAllRewardsHistoryCount($user_id);

                    $totalpage = ceil((($totalrewards))/$limit);
                    if($page == $totalpage ){
                      $next_page = 0;
                    }else{
                      $next_page = $page+1;
                    }
                    $data['curr_page'] = (int)$page;
                    $data['next_page'] = (int)$next_page;

                if ($user_reward_list) {
                    $i = 0;
                    foreach ($user_reward_list as $user_rewards) {



                        $reward_list[$i] = array(
                            'id' => $user_rewards->rh_id,
                            'Reward' => (isset($user_rewards->reward_name) && strlen($user_rewards->reward_name)>0) ? $user_rewards->reward_name : $user_rewards->reward_type_name,
                            'Points' =>(isset($user_rewards->history_point) && strlen($user_rewards->history_point)>0) ? $user_rewards->history_point : $user_rewards->reward_type_point,
                            'Descriptions' => $user_rewards->description,
                            'Request_Status' =>$user_rewards->reward_status,
                            'Applied_Date' => date("d-m-Y", strtotime($user_rewards->date)),
                            'Replay_Description' => (isset($user_rewards->reply_description) && strlen($user_rewards->reply_description)>0) ? $user_rewards->reply_description : '',
                        );
                        $i++;
                    }
                    $data['reward_list'] = $reward_list;
                    $response["status"] = 1;
                    $response["message"] = "Reward List";
                    $response["data"] = $data;
                    echo json_encode($response);
                } else {
                    $response["status"] = 0;
                    $response["message"] = "No record exists";
                    echo json_encode($response);
                }
            } else {
                $response["status"] = 0;
                $response["message"] = "User not found";
                echo json_encode($response);
            }
        } elseif ($user_id == '') {
            $response["status"] = 0;
            $response["message"] = "Enter User Id";
            echo json_encode($response);
        }
    }

    /**
    * @Method         : get
    * @Params         :
    * @author        : dipika
    * @created        : 03-05-2018
    * @Modified by    :
    * @Status         :
    * @Comment        : getAllTotalRewardTypes
    **/
    public function getAllTotalRewardTypes(){
      header('Content-Type: application/json; charset=utf-8');
      $data = $this->reward_model->getAllTotalRewardTypes();
      if($data){
        $i=0;
        foreach ($data as $key => $value) {
            $reward_type[$i] = array(
                "id" => $value->id,
                "Reward_name" => $value->reward_name,
                "Point" => $value->point,
                "Descriptions" => $value->description,
                "Status" => $value->status,
                );

            $i++;
        }
        $response['status'] = "1";
        $response['message'] = "Reard Type List.";
        $response['data'] = $reward_type;
        echo json_encode($response);
        exit();
      }else{
        $response['status'] = "0";
        $response['message'] = "Something went WRONG !!";
        echo json_encode($response);
        exit();
      }
    }

     /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : add user reward
     **/
    public function addReward(){

        header('Content-Type: application/json;');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();

        $reward_id = $apiData['data']['reward_type'];
        $reward_date = $apiData['data']['date'];
        $description = $apiData['data']['description'];
        $user_id = $apiData['data']['user_id'];

        if($reward_id == ''){
            $response['status'] = "0";
            $response['message'] = "Reward type is required.";
            echo json_encode($response);
            exit();
        }else if(empty($reward_date)){
            $response['status'] = "0";
            $response['message'] = "Reward date is required.";
            echo json_encode($response);
            exit();
        }else if(empty($description)){
            $response['status'] = "0";
            $response['message'] = "Description is required.";
            echo json_encode($response);
            exit();
        }else if(empty($user_id)){
            $response['status'] = "0";
            $response['message'] = "User id is required.";
            echo json_encode($response);
            exit();
        }else{

            $rewardDetails = $this->reward_model->getRewardDetailsById($reward_id);
            if($rewardDetails){
                $addData['user_id'] = $user_id;
                $addData['reward_id'] = $reward_id;
                $addData['date'] = $reward_date;
                $addData['description'] = $description;
                $addData['reward_name'] = $rewardDetails->reward_name;
                $addData['point'] = $rewardDetails->point;

                $addDatasucess = $this->reward_model->rewardAdd($addData);
                if($addDatasucess){
                    $response['status'] = "1";
                    $response['message'] = "Reward added successfully.";
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode($response);
                    $rewardDetails = $this->reward_model->getRewardDetailsById($reward_id);
                    $rewardName = $rewardDetails->reward_name;
                    //Send Alert to TLs , PM & HR.
                    $managementEmails = getSeniorEmails($addData['user_id'], 'feedback_users');

                    $user = $this->reward_model->getUserDetail($user_id);
                    $mailData['body'] = '<tr>
                                  <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$user->first_name.' '.$user->last_name.'</strong> has requested reward for <strong style="color:#03A9F4;">'.$rewardName.'</strong></p></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2" style="font-size:14px; color:#FFF;
                                    padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'.$description.'
                                    </td>
                                  </tr>';
                    $mailData['name'] = '';
                    $this->email->from('hrms@letsnurture.com', "HRMS");
                    $this->email->to('hrms@letsnurture.com');
                    $this->email->cc($managementEmails);
                    $this->email->subject("Request for Reward");
                    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                    $this->email->message($body);
                    //$this->email->send();

                }else{
                    $response["status"] = 0;
                    $response["message"] = "Something went wrong.";
                    echo json_encode($response);
                }
            }else{
                $response["status"] = 0;
                $response["message"] = "Data not Found.";
                echo json_encode($response);
            }
           }
        }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : user cancel reward
     **/
    function userCancelReward(){
        header('Content-Type: application/json;');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();

        $reward_id = $apiData['data']['reward_id'];

        if($reward_id == ''){
            $response['status'] = "0";
            $response['message'] = "Reward id is required.";
            echo json_encode($response);
            exit();
        }else{
            $data['status'] = '3';
            $cancelRewardSucess = $this->reward_model->rewardUpdate($data, $reward_id);
            if($cancelRewardSucess){
                $response["status"] = 1;
                $response["message"] = "Reward cancelled successfully!";
                echo json_encode($response);
            }else{
                $response["status"] = 0;
                $response["message"] = "You cannot cancel this request type.";
                $response["data"] = $data;
                echo json_encode($response);
            }
        }

    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : user edit reward
     **/
    function userEditReward(){
        header('Content-Type: application/json;');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();

        $reward_id = $apiData['data']['reward_id'];
        $user_id = $apiData['data']['user_id'];
        $reward_type = $apiData['data']['reward_type'];
        $date = $apiData['data']['date'];
        $description = $apiData['data']['description'];

        if($reward_id == ''){
            $response['status'] = "0";
            $response['message'] = "Reward id is required.";
            echo json_encode($response);
            exit();
        }else if($user_id == ''){
            $response['status'] = "0";
            $response['message'] = "User id is required.";
            echo json_encode($response);
            exit();
        }else if($reward_type == ''){
            $response['status'] = "0";
            $response['message'] = "Reward type is required.";
            echo json_encode($response);
            exit();
        }else if($date == ''){
            $response['status'] = "0";
            $response['message'] = "Date is required.";
            echo json_encode($response);
            exit();
        }else if($description == ''){
            $response['status'] = "0";
            $response['message'] = "Description is required.";
            echo json_encode($response);
            exit();
        }else{
            $rewardDetails = $this->reward_model->getRewardDetailsById($reward_type);
            if($rewardDetails){
                $addData['user_id'] = $user_id;
                $addData['reward_id'] = $reward_id;
                $addData['date'] = $reward_date;
                $addData['description'] = $description;
                $addData['reward_name'] = $rewardDetails->reward_name;
                $addData['point'] = $rewardDetails->point;

                $checkReward = $this->reward_model->checkReward($reward_id,$user_id);
                if($checkReward){
                    $addDatasucess = $this->reward_model->rewardUpdate($addData,$reward_id);

                    if($addDatasucess){
                        $response['status'] = "1";
                        $response['message'] = "Reward updated successfully.";
                        echo json_encode($response);

                        $rewardDetails = $this->rewards_model->getRewardDetailsById($reward_id);
                        $rewardName = $rewardDetails->reward_name;
                        //Send Alert to TLs , PM & HR.
                        $managementEmails = getSeniorEmails($addData['user_id'], 'feedback_users');
                        //$managementEmails = 'hitendra@letsnurture.com';
                        $reward_user = $this->reward_model->getUserDetail($user_id);
                        $mailData['body'] = '<tr>
                                      <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$reward_user->first_name.' '.$reward_user->last_name.'</strong> has requested reward for <strong style="color:#03A9F4;">'.$rewardName.'</strong></p></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" style="font-size:14px; color:#FFF;
                                        padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'.$description.'
                                        </td>
                                      </tr>';
                        $mailData['name'] = '';

                        /*$this->email->from('hrms@letsnurture.com', "HRMS");
                        $this->email->to('hrms@letsnurture.com');
                        $this->email->cc($managementEmails); */

                        $this->email->from('dipika.letsnurture@gmail.com', "HRMS");
                        $this->email->to('dipika.letsnurture@gmail.com');

                        $this->email->subject("Change Request for Reward");
                        $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                        $this->email->message($body);
                        //$this->email->send();
                        exit();
                    }else{
                        $response['status'] = "0";
                        $response['message'] = "Reward detail not found.";
                        echo json_encode($response);
                        exit();
                    }
                }else{
                    $response['status'] = "0";
                    $response['message'] = "Reward detail not found.";
                    echo json_encode($response);
                    exit();
                }
            }

        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : get Reward Products
     **/
    public function getRewardProducts(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $page = $apiData['data']['page'];
        if(empty($page)){
          $page = 1;
        }
           // Pagination Code
           $page_number = $page;
           $limit = 15;
            if(isset($page) && $page == 1){ $offset = 0; }else{
                if(isset($page) && $page != '1') {
                    $offset = ($page_number*$limit) - ($limit);
                }else{
                    $offset = 0;
                }
            }
            $reward_product = $this->reward_model->getRewardProducts($limit,$offset);
            $totalProduct = $this->reward_model->getRewardProductsCount();

            $totalpage = ceil((($totalProduct))/$limit);
            if($page == $totalpage ){
              $next_page = 0;
            }else{
              $next_page = $page+1;
            }
            $data['curr_page'] = (int)$page;
            $data['next_page'] = (int)$next_page;

        if ($reward_product) {
            $i = 0;
            foreach ($reward_product as $product) {
                if($product->image)
                {
                    $img = base_url() . 'uploads/products/' . $product->image;
                }else{
                    $img = base_url() . 'assets/img/default_product.jpg';
                }
                $product_list[$i] = array(
                    'id' => $product->id,
                    'Name' => (isset($product->name) && strlen($product->name)>0) ? $product->name : '',
                    'Points' =>(isset($product->point) && strlen($product->point)>0) ? $product->point : '',
                    'Descriptions' => $product->description,
                    'Image' => $img,
                );
                $i++;
            }
            $data['product_list'] = $product_list;
            $response["status"] = 1;
            $response["message"] = "Reward List";
            $response["data"] = $data;
            echo json_encode($response);
        } else {
            $response["status"] = 0;
            $response["message"] = "No record exists";
            echo json_encode($response);
        }
    }


     function redeemreward(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $product_id = $apiData['data']['product_id'];
        $user_id = $apiData['data']['user_id'];

        if($product_id == ''){
            $response['status'] = "0";
            $response['message'] = "Product id is required.";
            echo json_encode($response);
            exit();
        }else if(empty($user_id)){
            $response['status'] = "0";
            $response['message'] = "user id is required.";
            echo json_encode($response);
            exit();
        }else{
            $data['user_id'] = $user_id;
            $data['product_id'] = $product_id;
            $data['status'] = 0;
            $data['created_at'] = date('Y-m-d h:m:s');

            $reward_user = $this->reward_model->getUserDetail($user_id);
            $product = $this->reward_model->getProduct($product_id);
            if($reward_user && $product){

                if((($reward_user->reward_points)-($product[0]->point)) >= 0)
                  {

                    $data2['reward_points'] = $reward_user->reward_points - $product[0]->point;
                    $data2['karma_points'] = $reward_user->karma_points - $product[0]->point;
                    $res = $this->reward_model->updateRewardUser($data2, $user_id);
                    $this->reward_model->addRedeemRequest($data);
                    $response["status"] = 1;
                    $response["message"] = "Your request has been sent.";
                    echo json_encode($response);

                }else{
                    $response["status"] = 0;
                    $response["message"] = "You dont have enough reward points to redeem this product";
                    echo json_encode($response);
                }
            }else{
                $response['status'] = "0";
                $response['message'] = "user not found.";
                echo json_encode($response);
                exit();
            }
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : redeem cancel
     **/
    function redeemCancel(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $redeem_id = $apiData['data']['redeem_id'];
        $user_id = $apiData['data']['user_id'];

        if($redeem_id == ''){
            $response['status'] = "0";
            $response['message'] = "Redeem id is required.";
            echo json_encode($response);
            exit();
        }else if(empty($user_id)){
            $response['status'] = "0";
            $response['message'] = "user id is required.";
            echo json_encode($response);
            exit();
        }else{
            $reward_user = $this->reward_model->getUserDetail($user_id);
            if($reward_user){
                $data['status'] = '3';
                $reward_data = $this->reward_model->getUserRedeemHistory($redeem_id, $user_id);
                if($reward_data){
                    $data2['reward_points'] =  $reward_user->reward_points + $reward_data[0]->point;
                    $res = $this->reward_model->updateRewardUser($data2, $user_id);
                    $cancelRewardSucess = $this->reward_model->cancelRedeem($data, $redeem_id);
                    if($cancelRewardSucess){
                        $response['status'] = "1";
                        $response['message'] = "Redeem Request cancelled successfully.";
                        echo json_encode($response);
                        exit();
                    }else{
                        $response['status'] = "0";
                        $response['message'] = "You cannot cancel this Redeem Request.";
                        echo json_encode($response);
                        exit();
                    }
                }else{
                    $response['status'] = "0";
                    $response['message'] = "Redeem history not found.";
                    echo json_encode($response);
                    exit();
                }
            }else{
                $response['status'] = "0";
                $response['message'] = "User not found.";
                echo json_encode($response);
                exit();
            }
        }

    }


     /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : redeemhistory
     **/
    public function redeemhistory(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $page = $apiData['data']['page'];
        $user_id = $apiData['data']['user_id'];
        if(empty($page)){
          $page = 1;
        }
        if(isset($user_id) && !empty($user_id)){
        $user = $this->reward_model->getUserDetail($user_id);
            if ($user) {
                   // Pagination Code
                   $page_number = $page;
                   $limit = 15;
                    if(isset($page) && $page == 1){ $offset = 0; }else{
                        if(isset($page) && $page != '1') {
                            $offset = ($page_number*$limit) - ($limit);
                        }else{
                            $offset = 0;
                        }
                    }
                    $reward_history = $this->reward_model->getAllRewardsRedeemHistory($user_id,$limit,$offset);
                    $totalHistory = $this->reward_model->getAllRewardsRedeemHistoryCount($user_id);

                    $totalpage = ceil((($totalHistory))/$limit);
                    if($page == $totalpage ){
                      $next_page = 0;
                    }else{
                      $next_page = $page+1;
                    }
                    $data['curr_page'] = (int)$page;
                    $data['next_page'] = (int)$next_page;

                if ($reward_history) {
                    $i = 0;
                    foreach ($reward_history as $history) {
                        $history_list[$i] = array(
                            'id' => $history->id,
                            'Name' => (isset($history->name) && strlen($history->name)>0) ? $history->name : '',
                            'Points' =>(isset($history->point) && strlen($history->point)>0) ? $history->point : '',
                            'Status' =>$history->status,
                            'Applied_Date' => date("d-m-Y", strtotime($history->created_at)),
                        );
                        $i++;
                    }
                    $data['history_list'] = $history_list;
                    $response["status"] = 1;
                    $response["message"] = "History List";
                    $response["data"] = $data;
                    echo json_encode($response);
                } else {
                    $response["status"] = 0;
                    $response["message"] = "No record exists";
                    echo json_encode($response);
                }
            }else{
                $response["status"] = 0;
                $response["message"] = "User is not found";
                echo json_encode($response);
            }
        }else{
            $response["status"] = 0;
            $response["message"] = "User id is required";
            echo json_encode($response);
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : rank list
     **/
    public function rank(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $page = $apiData['data']['page'];
        if(empty($page)){
          $page = 1;
        }

           // Pagination Code
           $page_number = $page;
           $limit = 15;
            if(isset($page) && $page == 1){ $offset = 0; }else{
                if(isset($page) && $page != '1') {
                    $offset = ($page_number*$limit) - ($limit);
                }else{
                    $offset = 0;
                }
            }
            $rank_list = $this->reward_model->getRewardUserRankWise($limit,$offset);
            $totalRank = $this->reward_model->getRewardUserRankWiseCount();
            $totalpage = ceil((($totalRank))/$limit);
            if($page == $totalRank ){
              $next_page = 0;
            }else{
              $next_page = $page+1;
            }
            $data['curr_page'] = (int)$page;
            $data['next_page'] = (int)$next_page;

        if ($rank_list) {
            $i = 0;
            $rankNumber = 0;
            $prevPoints = 0;
            foreach ($rank_list as $row) {
                if($prevPoints != $row->karma_points)
                {
                  $prevPoints = $row->karma_points;
                  $rankNumber++;
                }else{
                  $prevPoints = $row->karma_points;
                }
                $rank_list[$i] = array(
                    'Full_Name' => $row->first_name.' '.$row->last_name,
                    'Department' => $row->department_name,
                    'Karma_Points' =>$row->karma_points,
                    'Rank' =>$rankNumber,
                );
                $i++;
            }
            $data['rank_list'] = $rank_list;
            $response["status"] = 1;
            $response["message"] = "Rank List";
            $response["data"] = $data;
            echo json_encode($response);
        } else {
            $response["status"] = 0;
            $response["message"] = "No record exists";
            echo json_encode($response);
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : Employe of Month list
     **/
    public function eom(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $page = $apiData['data']['page'];
        if(empty($page)){
          $page = 1;
        }

       // Pagination Code
       $page_number = $page;
       $limit = 15;
        if(isset($page) && $page == 1){ $offset = 0; }else{
            if(isset($page) && $page != '1') {
                $offset = ($page_number*$limit) - ($limit);
            }else{
                $offset = 0;
            }
        }
        $eom_list = $this->reward_model->getAllEOMHistory($limit,$offset);
        $totalEom = $this->reward_model->getAllEOMHistoryCount();

        $totalpage = ceil((($totalEom))/$limit);

        if($page == $totalpage ){
          $next_page = 0;
        }else{
          $next_page = $page+1;
        }
        $data['curr_page'] = (int)$page;
        $data['next_page'] = (int)$next_page;

        if ($eom_list){
            $i = 0;
            foreach ($eom_list as $row) {
                $eom_lists[$i] = array(
                    'id' => $row->id,
                    'Name' => $row->first_name . ' ' . $row->last_name,
                    'Karma_Points' =>(isset($row->reward_type_point) && strlen($row->reward_type_point)>0) ? $row->reward_type_point : $row->point,
                    'Descriptions' =>$row->description,
                    'Month'=>date('F Y', strtotime($row->month))
                );
                $i++;
            }
            $data['eom_list'] = $eom_lists;
            $response["status"] = 1;
            $response["message"] = "Eom List";
            $response["data"] = $data;
            echo json_encode($response);
        } else {
            $response["status"] = 0;
            $response["message"] = "No record exists";
            echo json_encode($response);
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : get user list when add nagative point by PM
     **/
    public function getNegativeRewardUserList(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $user_id = $apiData['data']['user_id'];
        if($user_id == ''){
            $response['status'] = "0";
            $response['message'] = "User id is required.";
            echo json_encode($response);
            exit();
        }else{
            $user = $this->reward_model->getUserDetail($user_id);
            if($user){
                $department_id = $user->department_id;
                $reward_user = $this->reward_model->getAllUsers($department_id);
                if($reward_user){
                    $response['status']  = "1";
                    $response['data']  = $reward_user;
                    $response['message'] = "User List";
                    echo json_encode($response);
                    exit();
                }else{
                    $response['status'] = "0";
                    $response['message'] = "Data not found.";
                    echo json_encode($response);
                    exit();
                }
            }else{
                $response['status'] = "0";
                $response['message'] = "User Data not found.";
                echo json_encode($response);
                exit();
            }
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : add nagative point by PM
     **/
    public function addNegativeReward(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $user_id = $apiData['data']['user_id'];
        $description = $apiData['data']['description'];
        $reward_point = $apiData['data']['reward_point'];
        if($user_id == ''){
            $response['status'] = "0";
            $response['message'] = "User id is required.";
            echo json_encode($response);
            exit();
        }else if(empty($description)){
            $response['status'] = "0";
            $response['message'] = "Description is required.";
            echo json_encode($response);
            exit();
        }else if(empty($reward_point)){
            $response['status'] = "0";
            $response['message'] = "Reward point is required.";
            echo json_encode($response);
            exit();
        }else{
                $reward_user = $this->reward_model->getUserDetail($user_id);
                if($reward_user){
                    $data = array();
                    $data['user_id']        = $user_id;
                    $data['description']   = $description;
                    $data['point']   = $reward_point;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['date'] = $data['created_at'];
                    $data['status'] = 1;
                    $data['reward_name'] = 'Negative Reward';
                    $data['approved_by'] = 69;
                    $data['reward_type'] = 1;

                    $data2 = array();
                    // print_r($reward_user->reward_points - $data['point']);
                    // exit;
                    $data2['reward_points'] = $reward_user->reward_points - $data['point'];
                    $data2['karma_points'] = $reward_user->karma_points -  $data['point'];
                    $res = $this->reward_model->updateRewardUser($data2, $data['user_id']);
                    if($res){
                        $this->reward_model->addRewardtoUser($data);
                        $response['status'] = "1";
                        $response['message'] = "Negative Reward Added Sucessfully.";
                        echo json_encode($response);
                        exit();
                    }else{
                        $response['status'] = "0";
                        $response['message'] = "Something went wrong. Please try again";
                        echo json_encode($response);
                        exit();
                    }
                }else{
                    $response['status'] = "0";
                    $response['message'] = "User not found.";
                    echo json_encode($response);
                    exit();
                }
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : get user's reward list in TL section
     **/
    public function getUserRequestRewardList(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $page = $apiData['data']['page'];
        $user_id = $apiData['data']['user_id'];
        if(empty($page)){
          $page = 1;
        }

       // Pagination Code
       $page_number = $page;
       $limit = 15;
        if(isset($page) && $page == 1){ $offset = 0; }else{
            if(isset($page) && $page != '1') {
                $offset = ($page_number*$limit) - ($limit);
            }else{
                $offset = 0;
            }
        }

        $user = $this->reward_model->getUserDetail($user_id);
        if($user){
            $deptid = $user->department_id;
            $userid = $user->user_id;
            $eom_list = $this->reward_model->getAllRewardRequests($userid, $deptid,$limit,$offset);
            $totalEom = $this->reward_model->getAllRewardRequestsCount($userid, $deptid);

            $totalpage = ceil((($totalEom))/$limit);

            if($page == $totalpage ){
              $next_page = 0;
            }else{
              $next_page = $page+1;
            }
            $data['curr_page'] = (int)$page;
            $data['next_page'] = (int)$next_page;

            if ($eom_list){
                $i = 0;
                foreach ($eom_list as $row) {
                    $eom_lists[$i] = array(
                        'id' => $row->rh_id,
                        'user_id' => $row->user_id,
                        'reward_id' => ($row->reward_id)?$row->reward_id:'',
                        'FirstNname' => $row->first_name . ' ' . $row->last_name,
                        'DepartmentName' =>$row->department_name,
                        'RequestType' => ($row->reward_name)?$row->reward_name:'',
                        'Description'=>$row->reward_description,
                        'Point'=>($row->point)?$row->point:'',
                        'Status'=>$row->reward_status,
                        'RewardDate'=>date("d-m-Y", strtotime($row->date)),
                        'ApprovedBy'=>($row->afirst_name)?$row->afirst_name.' '.$row->alast_name:'',
                        'ApprovedId'=>($row->role_id)?$row->role_id:''
                    );
                    $i++;
                }
                $data['eom_list'] = $eom_lists;
                $response["status"] = 1;
                $response["message"] = "Eom List";
                $response["data"] = $data;
                echo json_encode($response);
            } else {
                $response["status"] = 0;
                $response["message"] = "No record exists";
                echo json_encode($response);
            }
        }else{
            $response["status"] = 0;
            $response["message"] = "User id not found.";
            echo json_encode($response);
        }
    }

    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 14-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : update user reward status accept or reject by TL,PM
     **/
    public function changeStatusOfUserReward(){

        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $user_id = $apiData['data']['user_id'];
        $row_id = $apiData['data']['row_id'];
        $reward_id = $apiData['data']['reward_id'];
        $status = $apiData['data']['status'];
        $login_id = $apiData['data']['login_id'];
        $reject_description = $apiData['data']['reject_description'];
        if($user_id == ''){
            $response['status'] = "0";
            $response['message'] = "User id is required.";
            echo json_encode($response);
            exit();
        }else if(empty($row_id)){
            $response['status'] = "0";
            $response['message'] = "Row id is required.";
            echo json_encode($response);
            exit();
        }else if(empty($reward_id)){
            $response['status'] = "0";
            $response['message'] = "Reward id is required.";
            echo json_encode($response);
            exit();
        }else if(empty($status)){
            $response['status'] = "0";
            $response['message'] = "Status is required.";
            echo json_encode($response);
            exit();
        }else if(empty($login_id)){
            $response['status'] = "0";
            $response['message'] = "Login id is required.";
            echo json_encode($response);
            exit();
        }else{
            $data['status'] = $status;
            $data['reward_type'] = 1;
            if($reject_description)
            {
              $data['reply_description'] = $reject_description;
            }
            $reward_data = $this->reward_model->getRewardDetailsById($reward_id);
            if($reward_data){
                $reward_user = $this->reward_model->getUserDetail($user_id);
                $reward_status = $this->reward_model->getRewardHistory($row_id);
                $data2 = array();
                if($status == 1)
                {

                  if($reward_status->status != 1){
                  $data2['karma_points'] =  $reward_user->reward_points + $reward_data->point;
                  $data2['reward_points'] = $reward_user->reward_points + $reward_data->point;
                  $res = $this->reward_model->updateRewardUser($data2, $user_id);
                  if($res){
                      $rewardDetails = $this->reward_model->getRewardDetailsById($reward_id);
                      $rewardName = $rewardDetails->reward_name;
                      //Send Alert to TLs , PM & HR.
                      $managementEmails = getSeniorEmails($user_id, 'feedback_users');
                      //$managementEmails = 'hitendra@letsnurture.com';

                       $mailData['body'] = '<tr>
                            <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$reward_user->first_name.' '.$reward_user->last_name.'</strong> your request for <strong style="color:#03A9F4;">'.$rewardName.'</strong> reward has beed approved<br />Reward Point: '.$reward_data->point.'<br />Your Reward Balance: '.$data2['reward_points'].'</p></td></tr>
                            <tr><td colspan="2" style="font-size:14px; color:#FFF;padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'. ' Reward Approved'.'</td></tr>';
                      $mailData['name'] = '';
                      $this->email->from('hrms@letsnurture.com', "HRMS");
                      $this->email->to('hrms@letsnurture.com');
                      //$this->email->to('rajendra.letsnurture@gmail.com');
                      $this->email->cc($managementEmails);
                      $this->email->subject("Reward Approved");
                      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                      $this->email->message($body);
                    //  //$this->email->send();
                      }
                  }
                }else if($status == 2){
                  if($reward_status->status == 1){
                      $data3['reward_points'] =  $reward_user->reward_points - $reward_data->point;
                      $data3['karma_points'] =  $reward_user->reward_points - $reward_data->point;
                      $res = $this->reward_model->updateRewardUser($data3, $user_id);
                  }
                }
                if($status == 1 || $status == 0){
                  $data['approved_by'] = $login_id;
                }else{
                  $data['approved_by'] = $login_id;
                }

                $updateRewardStatusSucess = $this->reward_model->rewardUpdate($data, $row_id);
                $response['status'] = "1";
                $response['message'] = "Successfully data updated";
                echo json_encode($response);
                exit();
            }else{
                $response['status'] = "0";
                $response['message'] = "Data not found.";
                echo json_encode($response);
                exit();
            }
        }

    }


    /**
     * @Method        : POST
     * @Params        :
     * @author        : Dipika
     * @created       : 03-05-2018
     * @Modified by   :
     * @Status        :
     * @Comment       : add exceptional point
     **/
    public function addexpointsteam(){
        header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $user_data = $apiData['data']['user_data'];
        $description = $apiData['data']['description'];
        $login_id = $apiData['data']['login_id'];
        if($user_data == ''){
            $response['status'] = "0";
            $response['message'] = "user_data is required.";
            echo json_encode($response);
            exit();
        }else if(empty($description)){
            $response['status'] = "0";
            $response['message'] = "Description is required.";
            echo json_encode($response);
            exit();
        }else if(empty($login_id)){
            $response['status'] = "0";
            $response['message'] = "Login id is required.";
            echo json_encode($response);
            exit();
        }else{
            $user_data   = $user_data;
            $user_id     = $login_id;
            $description = $description;
            $pmpoints = 0;
            $pmpointsDB = $this->reward_model->getPMPoints($user_id);
            if($pmpointsDB){
                foreach ($user_data as $key => $value) {
                    $data = array();
                    $data['user_id'] = $value['user_ids'];
                    $data['description']   = $description;
                    $data['point']   = $value['reward_point'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['date'] = $data['created_at'];
                    $data['status'] = 1;
                    $data['reward_name'] = 'Exeptional Reward';
                    $data['approved_by'] = $user_id;
                    $data['reward_type'] = 1;
                    $reward_user = $this->reward_model->getUserDetail($value['user_ids']);
                    $data2 = array();

                    $data2['reward_points'] = $reward_user->reward_points + $data['point'];
                    $data2['karma_points'] = $reward_user->karma_points + $data['point'];
                    $res = $this->reward_model->updateRewardUser($data2, $value['user_ids']);
                    $pmpoints = $pmpoints + $data['point'];
                    $this->reward_model->addRewardtoUser($data);
                }

                $pmpointValue = $pmpointsDB[0]->pm_reward_points;
                $pmdata['pm_reward_points'] = $pmpointValue - $pmpoints;
                $query = $this->reward_model->updateRewardUser($pmdata, $user_id);
                $pmpointsDB = $this->reward_model->getPMPoints($user_id);
                $pmpointValue = $pmpointsDB[0]->pm_reward_points;
                if($query){
                    $response['status'] = "1";
                    $response['message'] = "Team Exeption Reward Added Sucessfully";
                    $response['PMpoint'] = $pmpointValue;
                    echo json_encode($response);
                    exit();
                }
            }else{
                $response['status'] = "0";
                $response['message'] = "Record not found.";
                echo json_encode($response);
                exit();
            }
        }
    }


    function getUserRank($id){
    $this->db->query('SELECT  ( SELECT  COUNT(DISTINCT ui.karma_points) FROM    crm_users as ui WHERE   ui.karma_points >= uo.karma_points ) AS rank FROM    crm_users as uo WHERE   user_id = '.$id);
     return $query->get();
    }


    /**
    * @Method         : get
    * @Params         :
    * @author        : dipika
    * @created        : 03-05-2018
    * @Modified by    :
    * @Status         :
    * @Comment        : get user reward point detail
    **/
    public function getUserReardPointDetails(){
       header('Content-Type: application/json; charset=utf-8');
        $apiData = json_decode(file_get_contents('php://input'),TRUE);
        $response = array();
        $user_id = $apiData['data']['user_id'];
        if($user_id == ''){
            $response['status'] = "0";
            $response['message'] = "User id is required.";
            echo json_encode($response);
            exit();
        }else{
             $rank = getUserRank($user_id);
             $data['rank'] = ($rank[0]['rank'])?$rank[0]['rank']:'0';
             $this->db->select('reward_points, karma_points');
             $this->db->from('crm_users');
             $this->db->where('user_id',$user_id);
             $query = $this->db->get();
             $result = $query->row();
             $data['reward_points'] = ($result->reward_points)?$result->reward_points:'0';
             $data['karma_points'] = ($result->karma_points)?$result->karma_points:'0';

             if($result){
                    $response['status'] = "1";
                    $response['message'] = "Record point data.";
                    $response['data'] = $data;
                    echo json_encode($response);
                    exit();
             }else{
                    $response['status'] = "0";
                    $response['message'] = "Record data not found.";
                    echo json_encode($response);
                    exit();
             }
        }
    }



}
