<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class apiv1 extends MX_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
		$this->load->model('apiv1_model');
    $this->load->library('email');
    }

    public function index(){
       $data ="Welcome to HRMS API";
      redirect('/', 'refresh');
    }

    public function user(){
       $userName = $this->input->get('user');
       $passwd = $this->input->get('pwd');
       if($passwd && $userName){
          $data = $this->apiv1_model->getUserLogin($userName,$passwd);
       		if($data){
       			$this->output->set_output( json_encode(array("status"=>1,"data"=>$data)));
       			} else{
       			$this->output->set_output( json_encode(array("status"=>0, "message" =>"Invalid Credentials")));
       		}
       }else{
          $this->output->set_output( json_encode(array("status"=>0, "message" =>"Parameters missing user & pwd required in GET")));
          exit;
       }
    }

  	public function userleaves(){
       $userId = $this->input->get('userid');
       if($userId){
         $data = $this->apiv1_model->getUserLeaves($userId);
       		if($data){
       			$this->output->set_output( json_encode(array("status"=>1,"data"=>$data)));
       			} else{
       			$this->output->set_output( json_encode(array("status"=>0, "message" =>"Invalid userId or no leaves")));
       		}
       } else {
          $this->output->set_output( json_encode(array("status"=>0, "message" =>"Parameters missing useridrequired in GET")));
          exit;
       }
    }

	public function getleavetype(){
		//getAllLeaveType
		$data = $this->apiv1_model->getAllLeaveType();
 	  if($data){
 			$this->output->set_output( json_encode(array("status"=>1,"data"=>$data)));
 			} else{
 			$this->output->set_output( json_encode(array("status"=>0, "message" =>"Something went WRONG !!")));
 		}

	}
  //http://localhost/ln_projects/live/hrms/api/api/addleaves?userid=109&leavetype=4&startdate=2017-01-26&enddate=2017-01-26&reason=testing
	public function addleaves(){
     $userId = $this->input->get('userid');
     $leaveType = $this->input->get('leavetype');
     $startDate = $this->input->get('startdate');
     $endDate = $this->input->get('enddate');
     $reason = $this->input->get('reason');

     //Email Data
     $mailData['leave_start_date'] = $startDate;
     $mailData['leave_end_date'] = $endDate;
     $mailData['leave_reason'] = $reason;
     $mailData['leave_type'] = $leaveType;
     $text_leave = $this->apiv1_model->getLeaveTypeText($mailData['leave_type']);
     $leaveData['leave_text_msg'] =$text_leave->leave_type;
     $mailData['leave_text_msg'] = $leaveData;

     $user_details = $this->apiv1_model->getUserDetails($userId);
     $department_id = $user_details->department_id;
     $role_id = $user_details->role_id;
     $mailData['first_name'] =  $user_details->first_name;
     $mailData['last_name'] =$user_details->last_name;
     $emails = $this->apiv1_model->getemails();
     $departmentName = $this->apiv1_model->getdepartmentName($department_id);

     //krunal
     //if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.They will need to discuss with HR Team for Leave. ( HR team can add leave)
   	 $checkTotalNumberLeave = $this->apiv1_model->checkTotalNumberLeave();
     $totalThisMonthLeave =  $checkTotalNumberLeave->totalLeave;
     $getActiveEmployee = $this->apiv1_model->getActiveEmployee();
     if($totalThisMonthLeave > $getActiveEmployee){
       $this->output->set_output( json_encode(array("status"=>0, "message" =>"You will need to discuss with HR Team for Apply Leave.")));
       return false;
     }

     if($userId && $leaveType && $reason && $startDate && $endDate){
       /*$todayDate = date("Y-m-d");
       $user_startDate = date('Y-m-d', strtotime($startDate. ' - 3 days'));
       if((strtotime($todayDate) < strtotime($user_startDate)) && $leaveType == 4){
         echo "string";
       }else {
         echo "2255";
       }*/

         //check alreay add leave same date
         $checkLeaveDate =  $this->apiv1_model->checkLeaveDate($startDate, $userId);
         if(empty($checkLeaveDate)){

         	  $data = $this->apiv1_model->getAddLeaves($userId, $leaveType, $startDate, $endDate,$reason);
         		if($data){
         			  $this->output->set_output(json_encode(array("status"=>1,"data"=>"Leave added successfully")));
                //Send leave Alert to TLs , PM & HR.
                for($i=0;$i<sizeof($emails);$i++){
                  $finalEmailHtml = $this->load->view('mail_layouts/meetingshedule/api_leave_request_tmp.php', $mailData, TRUE);
                  $subject="Request for ".$leaveData['leave_text_msg'];
                  $toEmail = $emails[$i]->email;
                  send_mail($toEmail, $subject, $finalEmailHtml);
                }

                //check When more than 3 team members has applied for leave on same day from same department. Send Alert to TLs , PM & HR.
                $checkTotalLeaveOnSameDays = $this->apiv1_model->checkTotalLeaveOnSameDays($startDate, $endDate,$department_id);
                if($checkTotalLeaveOnSameDays > 3){
                  for($i=0;$i<sizeof($emails);$i++){
                    $mailData['body'] = '<tr>
                                         <td colspan="2" style="font-size:14px; color:#FFF;
                                           padding:0 40px 20px 40px;">More than 3 <b style="color:#03A9F4;">'.$departmentName->department_name.'</b> team members has applied for leave on same day <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($startDate)).'</b></td>
                                       </tr>';
                    $mailData['name'] = '';
                    $this->email->from('hrms@letsnurture.com', "HRMS");
                    $this->email->to($emails[$i]->email);
                    $this->email->subject('More than 3 team members has applied for leave');
                    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                    $this->email->message($body);
                    //$this->email->send();
                  }
                }
                //When More then 1 TL from same department has applied for Leave please send alert to TL , PM & HR ( Notification + Email )
                if($role_id = 13){
                  $checkTLTotalLeaveOnSameDays = $this->apiv1_model->checkTLTotalLeaveOnSameDays($startDate, $endDate,$department_id,$role_id);
                  if($checkTLTotalLeaveOnSameDays >= 1){
                    for($i=0;$i<sizeof($emails);$i++){
                      $mailData['body'] = '<tr>
                                           <td colspan="2" style="font-size:14px; color:#FFF;
                                             padding:0 40px 20px 40px;">More than 1 <b style="color:#03A9F4;">'.$departmentName->department_name.'</b> TL has applied for leave on same day <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($startDate)).'</b></td>
                                         </tr>';
                      $mailData['name'] = '';
                      $this->email->from('hrms@letsnurture.com', "HRMS");
                      $this->email->to($emails[$i]->email);
                      $this->email->subject('Alert - TL Leave Congestion in '.$departmentName->department_name);
                      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                      $this->email->message($body);
                      //$this->email->send();
                    }
                  }
                }

       			}else{
       			    $this->output->set_output( json_encode(array("status"=>0, "message" =>$data)));
       		  }
         }else{
           $this->output->set_output( json_encode(array("status"=>0, "message" =>"Already applied for request on same day!")));
         }
     }else{
       $this->output->set_output( json_encode(array("status"=>0, "message" =>"Parameters missing userid required in GET")));
     }
  }


}
