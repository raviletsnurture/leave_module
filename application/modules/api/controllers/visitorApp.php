<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class visitorApp extends MX_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();
        $this->load->model('visitor_app');
        $this->load->library('email');
        $this->load->database();
    }

    /**
    * @Method		  :	get
    * @Params		  :
    * @author        : krunal
    * @created		  :	11-04-2017
    * @Modified by	  :
    * @Status		  :
    * @Comment		  : get user List
    **/
    public function getAllUser(){
      header("Access-Control-Allow-Origin: *");
      $allUsers = $this->visitor_app->getAllUser();
      if($allUsers){
        foreach ($allUsers as $allUser) {
          $data['user_id'] =$allUser->user_id;
          $data['full_name'] = $allUser->first_name.' '.$allUser->last_name;
          $data['first_name'] = $allUser->first_name;
          $data['last_name'] = $allUser->last_name;
          $data['email'] =$allUser->email;
          $output[] = $data;
        }
        $response['status'] = "1";
        $response['message'] = "User list.";
        $response['data'] = $output;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $response['status'] = "0";
        $response['message'] = "Something went WRONG !!";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : add Visitor Request
     *{"data":{"user_id":"29","vName":"krunal","vPhone":"7600962527","vEmail":"krunal@gmail.com","vPurpose":"helping","Whomtomeet":"TL","appoinment":"No"}}
     **/
    public function addVisitorRequest(){
      header("Access-Control-Allow-Origin: *");
      $response = array();
      $decodeText = html_entity_decode($_POST['data']);
      $apiData = json_decode($decodeText,true);
      if(empty($apiData['data']['vName']) || !isset($apiData['data']['vName'])){
        $response['status'] = "0";
        $response['message'] = "Please enter Name";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['vPhone']) || !isset($apiData['data']['vPhone'])){
        $response['status'] = "0";
        $response['message'] = "Please enter Contact No";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['vEmail']) || !isset($apiData['data']['vEmail'])){
        $response['status'] = "0";
        $response['message'] = "Please enter Email ID";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(!filter_var($apiData['data']['vEmail'], FILTER_VALIDATE_EMAIL)){
        $response['status'] = "0";
        $response['message'] = "Please enter valid Email ID";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['vPurpose']) || !isset($apiData['data']['vPurpose'])){
        $response['status'] = "0";
        $response['message'] = "Please select Purpose";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['user_id']) || !isset($apiData['data']['user_id'])){
        $response['status'] = "0";
        $response['message'] = "Please select whom to meet";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['appoinment']) || !isset($apiData['data']['appoinment'])){
        $response['status'] = "0";
        $response['message'] = "Please select Have an appoinment";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{

         if(isset($_FILES["cardImage"])){
  			   $path = './uploads/visitorCard/';
  			   $fileName = time()."1.".pathinfo($_FILES['cardImage']['name'], PATHINFO_EXTENSION);
  			   move_uploaded_file($_FILES['cardImage']['tmp_name'], $path.$fileName);
  			   chmod($path.$fileName, 0777);

         }else{
           $fileName = '';
         }
         $data = $this->visitor_app->addVisitorRequest($apiData,$fileName);
         $filepath = base_url().'uploads/visitorCard/';
         if($data){
           $user_details = $this->visitor_app->getUserDetail($apiData['data']['user_id']);
           $mailData['body'] = '<tr>
                                 <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
                                    A visitor has requested for meeting on <b style="color:#03A9F4;">'.date("Y-m-d H:i:s").' </b>
                                  </td>
                              </tr>
                              <tr>
                                 <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">Name: '.$apiData['data']['vName'].'</td>
                              </tr>
                              <tr>
                                 <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">Phone: '.$apiData['data']['vPhone'].'</td>
                              </tr>
                              <tr>
                                 <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">Email: '.$apiData['data']['vEmail'].'</td>
                              </tr>
                              <tr>
                                 <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">Appoinment: '.$apiData['data']['appoinment'].'</td>
                              </tr>';
                              if($fileName != ''){
                              $mailData['body'] .= '<tr>
                                 <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;"><img src="'.$filepath.$fileName.'" width="450px;"/></td>
                              </tr>';
                              };
           $mailData['name'] = $user_details->first_name.' '.$user_details->last_name;
           $this->email->from('hrms@letsnurture.com', "HRMS");
           $this->email->to($user_details->email);
           //$this->email->bcc('hrd@letsnurture.com');
           $this->email->subject("Hi, ".$apiData['data']['vName']." is here to meet you");
           $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
           $this->email->message($body);
           //$this->email->send();


           //Push Notification Logic
           $deviceTokens = getDeviceTokenByUser($apiData['data']['user_id']);
           $Tokenlist = array();
           foreach($deviceTokens as $value){
             array_push($Tokenlist,$value->deviceToken);
           }
           $notification_message = 'Visitor '.$apiData['data']['vName'].' requested for meeting.';
           SendNotificationFCMWeb('Visitor meeting',$notification_message,$Tokenlist);
           //Push Notification Logic end


  			   $response['status'] = "1";
  			   $response['message'] = "Your request processed successfully.";
  			   header('Content-Type: application/json; charset=utf-8');
  			   echo json_encode($response);
  			   exit();
  		   }else{
  			   $response['status'] = "0";
  			   $response['message'] = "Oops! Something went wrong. Please try again.";
  			   header('Content-Type: application/json; charset=utf-8');
  			   echo json_encode($response);
  			   exit();
  		   }
      }
    }

    /**
       * @Method      : POST
       * @Params      :
       * @author      : dipika
       * @created     : 14-05-2018
       * @Modified by :
       * @Status      :
       * @Comment     : Mantry app list`
       **/
    function mantryappList(){
      $filepathOnline = base_url().'uploads/mantra/Online/';
      $filepathmeditation = base_url().'uploads/mantra/meditation/';
      $data['arti'][0]['id']='1';
      $data['arti'][0]['name']='Ganpati Dada';
      $data['arti'][0]['image']=$filepathOnline.'ganpatidada.png';
      $data['arti'][0]['audio']=$filepathOnline.'JaiGaneshDevaAnuradhaPaudwal.mp3';

      $data['arti'][1]['id']='2';
      $data['arti'][1]['name']='Gayatri Mantra';
      $data['arti'][1]['image']=$filepathOnline.'gayatrima.png';
      $data['arti'][1]['audio']=$filepathOnline.'OmJaiGayatriMaa.mp3';

      $data['arti'][2]['id']='3';
      $data['arti'][2]['name']='Hanuman Ji';
      $data['arti'][2]['image']=$filepathOnline.'hanumanji.png';
      $data['arti'][2]['audio']=$filepathOnline.'AartiKijeHanumanLalakibySureshWadkar.mp3';

      $data['arti'][3]['id']='4';
      $data['arti'][3]['name']='Krishna Aarti';
      $data['arti'][3]['image']=$filepathOnline.'krishna.png';
      $data['arti'][3]['audio']=$filepathOnline.'AartiKunjBihariKibyAnuradha.mp3';

      $data['arti'][4]['id']='5';
      $data['arti'][4]['name']='Lakshmi Mata';
      $data['arti'][4]['image']=$filepathOnline.'lakshmimata.png';
      $data['arti'][4]['audio']=$filepathOnline.'OmJaiLakshmiMataAnuradhaPaudwal.mp3';

      $data['arti'][5]['id']='6';
      $data['arti'][5]['name']='Saraswati Mata';
      $data['arti'][5]['image']=$filepathOnline.'saraswatima.png';
      $data['arti'][5]['audio']=$filepathOnline.'OmJaiSaraswatiMatabyAnuradhaPaudwal.mp3';

      $data['arti'][6]['id']='7';
      $data['arti'][6]['name']='Shani Deva';
      $data['arti'][6]['image']=$filepathOnline.'shanidev.png';
      $data['arti'][6]['audio']=$filepathOnline.'AartiShanidevJi.mp3';

      $data['arti'][7]['id']='8';
      $data['arti'][7]['name']='Shiv Ji';
      $data['arti'][7]['image']=$filepathOnline.'shivji.png';
      $data['arti'][7]['audio']=$filepathOnline.'JaiShivOmkaraAnuradhaPaudwal.mp3';

      $data['arti'][8]['id']='9';
      $data['arti'][8]['name']='Shree Ram';
      $data['arti'][8]['image']=$filepathOnline.'shreeram.png';
      $data['arti'][8]['audio']=$filepathOnline.'ShreeRamachandraKripaluBhajamanbySureshWadkar.mp3';

      $data['arti'][9]['id']='10';
      $data['arti'][9]['name']='Shree Sai baba';
      $data['arti'][9]['image']=$filepathOnline.'saibaba.png';
      $data['arti'][9]['audio']=$filepathOnline.'SaiBabaAartiInHindi.mp3';

      $data['arti'][10]['id']='11';
      $data['arti'][10]['name']='Ghantakarna Mahaveer';
      $data['arti'][10]['image']=$filepathOnline.'Ghantakarna.png';
      $data['arti'][10]['audio']=$filepathOnline.'GhantakarnaMahaveer.mp3';

      $data['meditation'][0]['id']='1';
      $data['meditation'][0]['name']='Chakra Tune';
      $data['meditation'][0]['audio']=$filepathmeditation.'ChakraTune-up.mp3';

      $data['meditation'][1]['id']='2';
      $data['meditation'][1]['name']='Deep Healing';
      $data['meditation'][1]['audio']=$filepathmeditation.'DeepHealing.mp3';

      $data['meditation'][2]['id']='3';
      $data['meditation'][2]['name']='Devine';
      $data['meditation'][2]['audio']=$filepathmeditation.'Devine.mp3';

      $data['meditation'][3]['id']='4';
      $data['meditation'][3]['name']='Inner Peace';
      $data['meditation'][3]['audio']=$filepathmeditation.'InnerPeace.mp3';

      $data['meditation'][4]['id']='5';
      $data['meditation'][4]['name']='Positive Energy';
      $data['meditation'][4]['audio']=$filepathmeditation.'PositiveEnergy.mp3';

      $data['meditation'][5]['id']='6';
      $data['meditation'][5]['name']='Powerful';
      $data['meditation'][5]['audio']=$filepathmeditation.'Powerful.mp3';

      $response['status'] = "1";
      $response['message'] = "Song list.";
      $response['data'] = $data;
      header('Content-Type: application/json; charset=utf-8');
      echo json_encode($response);
      exit();
    }
}
