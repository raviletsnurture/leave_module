<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class leave extends MX_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();
        $this->load->model('leave_model');
        $this->load->library('email');
        $this->load->database();
    }

    /*
    * Developed By : parth patwala
    * Date : 26/12/2016
    * Modified By :
    * Description : Get leave records month vise by crone from leave table and insert records in crm_leave_month table
    */
    public function index(){
        $leaveData = $this->leave_model->getLeaveRecords();
    }

}
