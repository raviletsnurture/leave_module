<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class meeting extends MX_Controller {

    function __construct(){
        // Construct the parent class
      parent::__construct();
	    $this->load->model('meeting_model');
      $this->load->library('email');
    }

    public function index(){
       $data ="Welcome to HRMS API";
      redirect('/', 'refresh');
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : get Meeting Detail
     **/
    public function getMeetingDetail(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['meeting_id']) && !isset($apiData['data']['meeting_id'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the meeting id.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{

        $meeting_id = $apiData['data']['meeting_id'];
        $data = $this->meeting_model->getMeetingDetail($meeting_id);
        $data->descriptions = $data->description;
        $response['status'] = "1";
        $response['message'] = "Meeting Detail.";
        $response['data'] = $data;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();

      }
    }



    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : get meeting list
     **/
  	public function meetingLists(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['user_id']) && !isset($apiData['data']['user_id'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the user id.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['meeting_date']) && !isset($apiData['data']['meeting_date'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the meeting date.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{

        $user_id = $apiData['data']['user_id'];
        $meeting_date = $apiData['data']['meeting_date'];
        $data = $this->meeting_model->getMeetingLists($meeting_date);
        foreach ($data as $addData) {
          //get detail who created meeting
          $getUserDetails = $this->meeting_model->getUserDetail($addData->user_id);
          $addData->member_created_by = $getUserDetails->first_name.' '.$getUserDetails->last_name;

          $addData->descriptions = $addData->description;
          unset($addData->description);
          if($user_id == $addData->user_id){
              $addData->iscreatedbyme = "1";
          }else{
              $addData->iscreatedbyme = "0";
          }
          $members = explode(",",$addData->members);
          $outputMemberDetails = array();
          $member_details= array();
          foreach ($members as $memberId) {
            $memberDetails = $this->meeting_model->getUserDetail($memberId);
            if($memberDetails){
              $member_details['first_name'] = $memberDetails->first_name;
              $member_details['last_name'] = $memberDetails->last_name;
              $member_details['user_id'] = $memberDetails->user_id;
              $outputMemberDetails[] = $member_details;
            }
          }
          $addData->memberDetails = $outputMemberDetails;



        }

        $response['status'] = "1";
        $response['message'] = "Meeting List.";
        $response['data'] = $data;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();

      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : Add meeting
     **/
    public function addmeeting(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['subject']) && !isset($apiData['data']['subject'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the subject.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['schedule_date']) && !isset($apiData['data']['schedule_date'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the schedule date.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['schedule_time']) && !isset($apiData['data']['schedule_time'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the schedule time.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['end_date']) && !isset($apiData['data']['end_date'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the end date.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['description']) && !isset($apiData['data']['description'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the description.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['location']) && !isset($apiData['data']['location'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the location.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['members']) && !isset($apiData['data']['members'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the members.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['meeting_duration']) && !isset($apiData['data']['meeting_duration'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the meeting_duration.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{
        $userId = $apiData['data']['userid'];
        $subject = $apiData['data']['subject'];
        $schedule_date = $apiData['data']['schedule_date'];
        $schedule_time = $apiData['data']['schedule_time'];
        $end_date = $apiData['data']['end_date'];
        $description = $apiData['data']['description'];
        $location = $apiData['data']['location'];
        $members = $apiData['data']['members'];
        $meeting_duration = $apiData['data']['meeting_duration'];
        $checkMeetingTime = $this->meeting_model->checkMeetingTime($schedule_date,$schedule_time,$userId);      
        if(!empty($checkMeetingTime)) {
          if($checkMeetingTime->status == 0){
            $response['status'] = "0";
            $response['message'] = "You already have meeting on this time.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
          }
        }

          $data = $this->meeting_model->addMeeting($userId,$subject,$schedule_date,$schedule_time,$end_date,$description,$location,$members,$meeting_duration);
          if($data){

            //send mail to meeting member
            $meetingDetails = $this->meeting_model->getMeetingDetail($data);
            $userDetails = $this->meeting_model->getUserDetail($userId);
            $subject = "Invitation ".$subject;
            $message = $userDetails->first_name.' '.$userDetails->last_name." has a meeting with you.";
            $data = $this->meeting_model->sendMailToMeetingMemeber($subject,$message,$meetingDetails);

            $response['status'] = "1";
            $response['message'] = "Meeting set successfully!";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
         }else{
           $response['status'] = "0";
           $response['message'] = "Unable to create meeting. Please try again!";
           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();
         }
      }
    }


    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : Add meeting
     **/
    public function editmeeting(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['meeting_id']) && !isset($apiData['data']['meeting_id'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the meeting id.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['userid']) && !isset($apiData['data']['userid'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the userid.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['subject']) && !isset($apiData['data']['subject'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the subject.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['schedule_date']) && !isset($apiData['data']['schedule_date'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the schedule date.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['schedule_time']) && !isset($apiData['data']['schedule_time'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the schedule time.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['end_date']) && !isset($apiData['data']['end_date'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the end date.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['description']) && !isset($apiData['data']['description'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the description.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['location']) && !isset($apiData['data']['location'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the location.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['members']) && !isset($apiData['data']['members'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the members.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }if(empty($apiData['data']['meeting_duration']) && !isset($apiData['data']['meeting_duration'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the meeting_duration.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{

        $meeting_id = $apiData['data']['meeting_id'];
        $userId = $apiData['data']['userid'];
        $subject = $apiData['data']['subject'];
        $schedule_date = $apiData['data']['schedule_date'];
        $schedule_time = $apiData['data']['schedule_time'];
        $end_date = $apiData['data']['end_date'];
        $description = $apiData['data']['description'];
        $location = $apiData['data']['location'];
        $members = $apiData['data']['members'];
        $meeting_duration = $apiData['data']['meeting_duration'];
        $checkMeetingTime = $this->meeting_model->checkMeetingTime($schedule_date,$schedule_time,$userId);
        if(!empty($checkMeetingTime)) {
          if($checkMeetingTime->meeting_id != $meeting_id && $checkMeetingTime->status == 0){
            $response['status'] = "0";
            $response['message'] = "You already have meeting on this time.";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
          }
        }

          $data = $this->meeting_model->editMeeting($meeting_id,$userId,$subject,$schedule_date,$schedule_time,$end_date,$description,$location,$members,$meeting_duration);
          if($data){

            //send mail to meeting member
            $meetingDetails = $this->meeting_model->getMeetingDetail($meeting_id);
            $userDetails = $this->meeting_model->getUserDetail($userId);
            $subject = "Meeting has been updated ".$subject;
            $message = $userDetails->first_name.' '.$userDetails->last_name." has a meeting with you.";
            $data = $this->meeting_model->sendMailToMeetingMemeber($subject,$message,$meetingDetails);


            $response['status'] = "1";
            $response['message'] = "Meeting has been updated successfully!";
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($response);
            exit();
         }else{
           $response['status'] = "0";
           $response['message'] = "Unable to update meeting. Please try again!";
           header('Content-Type: application/json; charset=utf-8');
           echo json_encode($response);
           exit();
         }

     }
    }


    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : krunal
     * @created		  :	11-04-2017
     * @Modified by	  :
     * @Status		  :
     * @Comment		  : cancel meeting
     **/
    public function cancelmeeting(){
      $response = array();
      $apiData = json_decode(file_get_contents('php://input'),TRUE);
      if(empty($apiData['data']['meeting_id']) && !isset($apiData['data']['meeting_id'])){
        $response['status'] = "0";
        $response['message'] = "Please pass the meeting id.";
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
        exit();
      }else{

        $meeting_id = $apiData['data']['meeting_id'];
        $data = $this->meeting_model->cancelMeeting($meeting_id);
        if($data){
          //send mail to meeting member
          $meetingDetails = $this->meeting_model->getMeetingDetail($meeting_id);
          $userDetails = $this->meeting_model->getUserDetail($meetingDetails->user_id);
          $subject = "Meeting Cancellation";
          $message = $userDetails->first_name.' '.$userDetails->last_name." has canceled meeting.";
          $data = $this->meeting_model->sendMailToMeetingMemeber($subject,$message,$meetingDetails);

          $result['meeting_id'] = $meeting_id;
          $result['cancel_status'] = "1";

          $response['status'] = "1";
          $response['message'] = "Meeting has been cancelled successfully!";
          $response['data'] = $result;
          header('Content-Type: application/json; charset=utf-8');
          echo json_encode($response);
          exit();
       }else{
         $response['status'] = "0";
         $response['message'] = "Unable to cancelled meeting. Please try again!";
         header('Content-Type: application/json; charset=utf-8');
         echo json_encode($response);
         exit();
       }

     }
   }

}
