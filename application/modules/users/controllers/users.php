<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {
    var $logSession;
	function __construct(){
		parent::__construct();
        $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('users_model');
	}

	function index()
	{

		$userData = $this->logSession;
		$id = $userData[0]->user_id;
    //var_dump($id);
    //auditUserActivity('users', 'list/index', $id, date('Y-m-d H:i:s'),0);
		$data['titalTag'] = ' - Users';
		/*----department users-----*/
		$inf['id'] = $id;
		$inf['department_id'] = $userData[0]->department_id;

                if($userData[0]->department_id == '11'){
		     $data["users"] = $this->users_model->getAllUserList();
                }else{ 
		     $data["users"] = $this->users_model->getAllUser($inf);
                }

		/*--------- end----------*/
		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		$data['permission'] = $per;
		/*------------end------------*/

	   	$this->template->load_partial('dashboard_master','users/list_users',$data);
	}
	function add($catId = ''){


		$data['titalTag'] = ' - Add User';
		$data['department_id'] = $this->logSession[0]->department_id;

		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
    if($role_id != 22){redirect('users');exit;}
		$per = getUserpermission($role_id);

		$data['permission'] = $per;
		/*------------end------------*/


		if($this->input->post("addEditUsers") !== false)
		{

			$addData['role_id'] = $this->input->post('role_id');
			$addData['department_id'] = $this->input->post('department_id');
			$addData['first_name'] = $this->input->post('first_name');
			$addData['middle_name'] = $this->input->post('middle_name');
			$addData['last_name'] = $this->input->post('last_name');

			if($this->input->post('username') != '')
			{
				$addData['username'] = $this->input->post('username');
			}
			$addData['password'] = $this->input->post('password');
			$addData['country_id'] = $this->input->post('country_id');
			$addData['state_id'] = $this->input->post('state_id');
			$addData['city_id'] = $this->input->post('city_id');
			$addData['street'] = $this->input->post('street');
			$addData['zipcode'] = $this->input->post('zipcode');

			$addData['gender'] = $this->input->post('gender');
			$addData['birthdate'] = $this->input->post('birthdate');
			$addData['mobile'] = base64_encode($this->input->post('mobile'));
			$addData['landline'] = $this->input->post('landline');


			$addData['skype'] = base64_encode($this->input->post('skype'));
			$addData['gtalk'] = base64_encode($this->input->post('gtalk'));

			$addData['pancard_no'] = base64_encode($this->input->post('pancard_no')); //base encode
			$addData['pf_no'] = base64_encode($this->input->post('pf_no')); //base encode
            $addData['employee_insurance_number'] = base64_encode($this->input->post('employee_insurance_number')); //Employee insurance number

			$addData['email'] = $this->input->post('email');
			$addData['alternate_email'] = base64_encode($this->input->post('alternate_email'));
			$addData['religion_id'] = $this->input->post('religion_id');

			$addData['employee_type'] = $this->input->post('employee_type');

			$addData['anniversary'] = base64_encode($this->input->post('anniversary')); //base encode
			$addData['blood_group'] = $this->input->post('blood_group'); //base encode
			$addData['license'] = base64_encode($this->input->post('license')); //base encode

			$addData['bank_name'] = base64_encode($this->input->post('bank_name')); //base encode
			$addData['account_number'] = base64_encode($this->input->post('account_number')); //base encode
			$addData['ifsc_code'] = base64_encode($this->input->post('ifsc_code')); //base encode
			$addData['branch'] = base64_encode($this->input->post('branch')); //base encode
			$addData['status'] = $this->input->post('status');

			if(is_numeric($catId) && $catId != '')
			{ /*when update*/
				$addData['user_id']= $catId;
				$addData['user_updated'] = date('Y-m-d H:i:s');
				$data = $this->users_model->updateUser($addData);
				$this->session->set_flashdata('success','User updated successfully!');
        // log this in history
        //auditUserActivity('users', 'edit', $id, date('Y-m-d H:i:s'), $catId);
			}else
			{
				$addData['user_created'] = date('Y-m-d H:i:s');
				$addData['user_updated'] = date('Y-m-d H:i:s');
				$data = $this->users_model->add($addData);
        //var_dump($data);
				$this->session->set_flashdata('success','User Added successfully!');
        // log this as saved values
        //auditUserActivity('users', 'add/new', $id, date('Y-m-d H:i:s'),$data['user_id']);
			}
			redirect(base_url().'users');exit;
		}

	   	$this->template->load_partial('dashboard_master','users/add_edit_users',$data);
	}
	public function edit($catId)
	{
		$data['titalTag'] = ' - Update Users';
		$data['department_id'] = $this->logSession[0]->department_id;
		$data["state"] = $this->users_model->geteditstate($catId);
		$data["city"] = $this->users_model->geteditcity($catId);
		$data['usersdata'] = $this->users_model->getUserInfo($catId);
		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		$data['permission'] = $per;
		/*------------end------------*/

		$this->template->load_partial('dashboard_master','users/add_edit_users',$data);
	}
	public function view()
	{
		$id = $this->input->post('id');
		$data["user"] = $this->users_model->getUserInfo($id);
		$this->load->view('users/view_user',$data);
	}
	public function checkUsername()
	{
		$data['username'] = $_POST['username'];
		$check = $this->users_model->verify_username($data);
		echo $check;
		exit;
	}
	public function getStates()
	{
		$country_id = $this->input->post('country_id');
		$state_dropdown = $this->users_model->getState_drop($country_id);

		echo $state_dropdown;
	}
	public function getCities()
	{
		$state_id = $this->input->post('state_id');
		$city_dropdown = $this->users_model->getCity_drop($state_id);

		echo $city_dropdown;
	}

	public function delete($catId)
	{
		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		$data['permission'] = $per;
		/*------------end------------*/
		if($per[1]->module_delete == 'yes')
		{
			$this->users_model->deleteUser($catId);

			$this->session->set_flashdata('success','Users deleted successfully!');
			redirect(base_url().'users');
		}
		else
		{
			$this->session->set_flashdata('error','Permission Denied!');
			redirect(base_url().'users');
		}
		exit;
	}

}

?>
