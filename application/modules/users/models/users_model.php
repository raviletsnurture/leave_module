<?php
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}


	function getAllUser($inf)
	{
		$this->db->select('d.department_name,r.role_name,u.*');
		$this->db->from('users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->join('role as r','u.role_id = r.role_id', 'left');
		$this->db->where('u.department_id = '.$inf['department_id']);
		$this->db->order_by("u.user_id","desc");
		$query=$this->db->get();
		return $query->result();
	}

        function getAllUserList()
	{
		$this->db->select('d.department_name,r.role_name,u.*');
		$this->db->from('users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->join('role as r','u.role_id = r.role_id', 'left');
		$this->db->order_by("u.user_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('users',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getUser($catId){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function getState_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('state');
		$this->db->where('country_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		$dd = "<option value=''>Select State</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->state_id.">".$row->state_name."</option>";
        }
		return $dd;
	}
	function getCity_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('city');
		$this->db->where('state_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		//$dd = "<option value=''>Select City</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->city_id.">".$row->city_name."</option>";
        }
		return $dd;
	}
	function verify_username($data)
	{
		$data['username'] = trim($data['username']);
		$this->db->select("username");
		$this->db->from("users");
		$this->db->where("username",$data['username']);
		$checkUserId = $this->db->get()->num_rows();

		if($checkUserId > 0)
		{
				echo 'false';
		}else{
			echo "true";
		}
	}
	function updateUser($data){

		$this->db->where('user_id',$data['user_id']);
		if($this->db->update('users',$data)){
			return true;
		}else{
			return false;
		}
	}
	function geteditstate($id)
	{
		$this->db->select('s.*');
		$this->db->from('state as s');
		$this->db->join('users as c','s.country_id = c.country_id', 'left');
		$this->db->where('c.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function geteditcity($id)
	{
		$this->db->select('cc.*');
		$this->db->from('city as cc');
		$this->db->join('users as c','c.state_id = cc.state_id', 'left');
		$this->db->where('c.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function getUserInfo($id)
	{
		$this->db->select('d.department_name,ro.role_name,r.religion_name,cc.short_name,s.state_name,ci.city_name,u.*');
		$this->db->from('users as u');
		$this->db->join('country as cc','cc.country_id = u.country_id', 'left');
		$this->db->join('state as s','s.state_id = u.state_id', 'left');
		$this->db->join('city as ci','ci.city_id = u.city_id', 'left');
		$this->db->join('religion as r','r.religion_id = u.religion_id', 'left');
		$this->db->join('role as ro','ro.role_id = u.role_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('u.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}



	function deleteUser($id)
	{
		$this->db->where('user_id',$id);
	    $this->db->delete('users');
	}
}

?>
