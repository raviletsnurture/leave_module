<?php
$role = getAllRoles();

for($i=0;$i<sizeof($permission);$i++)
{
	$nm = $permission[$i]->module_name;
	$arr[$nm] = $permission[$i];
}
$role_id = $arr['users']->role_id;

?>

<?php
if(isset($user[0]->user_id)){
$enableNormalView = showEncryptedData($user[0]->user_id);
} else {
	$enableNormalView = 0;
}
?>

<style type="text/css">
.dataTables_filter {
     display: none;
}

</style>
<section id="main-content">
  <section class="wrapper site-min-height">

    <!-- page start-->
    <?php //$this->load->view('list_header'); ?>
    <section class="panel">
      <header class="panel-heading"> Users </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">

		<?php if($arr['users']->module_add == 'yes') { ?>
		<div class="row">
		    <div class="col-lg-4">
				<div id="editable-sample_length" class="dataTables_length">

					<!--

					<div class="btn-group">
						<a href="<?php echo base_url()?>users/add">
							<button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
						</a>
					</div>
				-->
				</div>
			</div>
        </div>
		<?php } ?>



      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">

		  <?php if($arr['users']->module_view == 'yes') { ?>

          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		  <select id="engines">
            <option value="">Select Role</option>
            <?php foreach($role as $row1){?>
            <option value="<?php echo $row1->role_name;?>"><?php echo $row1->role_name; ?></option>
            <?php } ?>
          </select>

          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Department </th>
                <th>Role</th>
                <th>Email</th>
                <th>Phone</th>

                <th><?php if($arr['users']->module_edit == 'yes' || $arr['users']->module_delete == 'yes') { ?>Action<?php } ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($users as $row){ ?>
              <tr>
                <td><?php echo $row->first_name;?></td>
                <td><?php echo $row->last_name; ?></td>
                <td><?php echo $row->department_name; ?></td>
                <td><?php echo $row->role_name; ?></td>
                <td><?php echo $row->email; ?></td>
                <td><?php echo base64_decode($row->mobile);
                     //if (showEncryptedData($row->user_id) ==1) { echo base64_decode($row->mobile);} else {echo $row->mobile;}
                    ?></td>

                <td>

				  <?php if($arr['users']->module_edit == 'yes') { ?>
                  <a href="<?php echo base_url()?>users/edit/<?php echo $row->user_id;?>" >
                  <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Users" title=""><i class="fa fa-pencil"></i></button>
                  </a>
				  <?php } ?>

				  <?php if($arr['users']->module_delete == 'yes') { ?>
                  <a href="<?php echo base_url()?>users/delete/<?php echo $row->user_id;?>" class="deleteRec">
                  <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Users"><i class="fa fa-trash-o "></i></button>
                  </a>
				  <?php } ?>

				  <a value="<?php echo $row->user_id;?>" onclick="getchek(<?php echo $row->user_id;?>)" href="#" >
                  <button class="btn btn-primary btn-xs tooltips" data-original-title="View&nbsp;User" title=""><i class="fa fa-eye"></i></button>
                  </a>

                  </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
			<?php } else { ?>
			<span>Permission Denied</span>
			<?php } ?>

          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">User Information</h4>
						</div>
						<div class="modal-body">

						</div>
					</div>
				</div>
			</div>


	</section>
    <!-- page end-->

  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">

function getchek(id)
{
	$.ajax({
		 url: '<?php echo base_url(); ?>users/view/',
		 data: { id: id},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('.modal-body').html(data);
			 $('#myModal').modal('show');
		 }
	});
}

          $(document).ready(function() {

             var dt =  $('#example').dataTable( {
                  //"aaSorting": [[ 3, "desc" ]],
				   "iDisplayLength": 10,
				   "pagingType": "full_numbers",
				   "dom": 'Cfrtip',
				   "destroy": true,
				   //"bFilter": false,
				   "bPaginate": true,
				   "bInfo" : false,

				   "oSearch": { "bSmart": false, "bRegex": true },
				   "aoColumnDefs": [
					  {
						'bSortable': true, 'aTargets': [ 5 ]
					  }
				   ]
              } );




			    $('select#engines').change( function() {
					dt.fnFilter( $(this).val(), 3 );
				} );





			  $(".dataTables_filter input").addClass('form-control');
		  } );
      </script>
