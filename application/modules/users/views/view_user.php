
<?php
if(isset($user[0]->user_id)){
$enableNormalView = showEncryptedData($user[0]->user_id);
} else {
	$enableNormalView = 0;
}
?>

  <div class="row-fluid">
    <div class="span9">
      <div class="new-list">
        <div class="item panel" id="myprofile">

          <table class="table myprofile_tbl">
            <tr>
              <td width="250"><b>User Full Name :</b></td>
              <td><?php echo $user[0]->first_name.'&nbsp;'.$user[0]->middle_name.'&nbsp;'.$user[0]->last_name;?></td>
            </tr>
			<tr>
              <td><b>Role :</b></td>
              <td><?php echo($user[0]->role_name);?></td>

            </tr>
			<tr>
              <td><b>Department :</b></td>
              <td><?php echo ($user[0]->department_name)?></td>
            </tr>
			<tr>
              <td><b>Username :</b></td>
              <td><?php echo ($user[0]->username)?></td>
            </tr>

              <td><b>Country :</b></td>
              <td><?php echo ($user[0]->short_name)?></td>
            </tr>
            <tr>
              <td><b>State :</b></td>
              <td><?php echo ($user[0]->state_name)?></td>
            </tr>
            <tr>
              <td><b>City :</b></td>
              <td><?php echo ($user[0]->city_name)?></td>
            </tr>
            <tr>
              <td><b>Address :</b></td>
              <td><?php echo ($user[0]->street)?><br /><?php echo $user[0]->zipcode; ?></td>
            </tr>
            <tr>
              <td><b>Gender :</b></td>
              <td><?php echo ($user[0]->gender)?></td>
            </tr>
            <tr>
              <td><b>Birthdate :</b></td>
              <td><?php echo ($user[0]->birthdate)?></td>
            </tr>
            <tr>
              <td><b>Mobile Number :</b></td>
              <td><?php if ($enableNormalView == 1){ echo base64_decode($user[0]->mobile);} else { echo $user[0]->mobile;}?></td>

            </tr>
			<tr>
              <td><b>Landline Number :</b></td>
              <td><?php echo ($user[0]->landline)?></td>
            </tr>
			<tr>
              <td><b>Personal Email :</b></td>
              <td><?php echo $user[0]->email;?></td>
            </tr>
			<tr>
              <td><b>Alternate Email :</b></td>

              <td><?php if ($enableNormalView == 1){ echo base64_decode($user[0]->alternate_email);} else{ echo ($user[0]->alternate_email);}?></td>
            </tr>



			<tr>
              <td><b>Skype :</b></td>
              <td><?php if ($enableNormalView == 1){ echo base64_decode($user[0]->skype);} else{ echo($user[0]->skype);}?></td>
            </tr>
			<tr>
              <td><b>Gtalk :</b></td>
              <td><?php if ($enableNormalView == 1){ echo base64_decode($user[0]->gtalk);} else { echo($user[0]->gtalk);}?></td>
            </tr>
   <!--  If i am owner of record or HR  then decode and show -->
			<tr>
              <td><b>Pancard Number :</b></td>
              <td><?php if ($enableNormalView == 1){ echo base64_decode($user[0]->pancard_no);} else {echo($user[0]->pancard_no);}?></td>
            </tr>
			<tr>
              <td><b>Pf Number :</b></td>
              <td><?php if ($enableNormalView == 1){ echo base64_decode($user[0]->pf_no);} else { echo ($user[0]->pf_no);}?></td>
            </tr>
			<tr>
              <td><b>Religion :</b></td>
              <td><?php echo $user[0]->religion_name;?></td>
            </tr>

						<tr>
			              <td><b>Religion :</b></td>
			              <td><?php echo $user[0]->religion_name;?></td>
			            </tr>

          </table>
        </div>
      </div>
    </div>

  </div>
