<?php

$country = getAllCountry();
$religion = getAllReligion();
$department = getAllDepartment();
$role = getAllRoles();
for($i=0;$i<sizeof($permission);$i++)
{
	$nm = $permission[$i]->module_name;
	$arr[$nm] = $permission[$i];
}

?>
<?php
if(isset($usersdata[0]->user_id)){
$enableNormalView = showEncryptedData($usersdata[0]->user_id);
} else {
	$enableNormalView = 0;
}
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($usersdata[0]->user_id)){ echo "Edit Users";} else{echo "Add Users";}?>
          </header>

		  <?php $a ='';
		        if(isset($usersdata[0]->first_name) != '')
				{
					if($arr['users']->module_edit == 'yes')
					{
						$a='yes';
					}
				}
				elseif($arr['users']->module_add == 'yes')
				{
					$a = 'yes';
				}

		  ?>
		  <?php if($a == 'yes') { ?>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addUsers', 'id' => 'addUsers', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($usersdata[0]->user_id)){
			  	echo form_open(base_url().'users/add/'.$usersdata[0]->user_id,$form_attributes);
			  }else{
				echo form_open(base_url().'users/add',$form_attributes);
			  }
			  ?>

			  <div class="form-group ">
                <label for="role_id" class="control-label col-lg-2">Role <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="role_id" id="role_id" class="form-control m-bot15">
				    <option value="">Select Role</option>
                  	<?php foreach($role as $rr){ ?>
						<option value="<?php echo $rr->role_id; ?>" <?php $value1 = (isset($usersdata[0]->role_id)) ? $usersdata[0]->role_id : 0; echo selectedVal($value1,$rr->role_id)?>> <?php echo $rr->role_name; ?></option>-->
                    <?php } ?>
                  </select>
                </div>
              </div>

			  <div class="form-group ">
                <label for="department_id" class="control-label col-lg-2">Department<span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="department_id" id="department_id" disabled="disabled" class="form-control">
                    <option value="">Select Department</option>
                    <?php foreach($department as $row1){?>
                    <option value="<?php echo $row1->department_id;?>" <?php $value1 = (isset($department_id)) ? $department_id : 0; echo selectedVal($value1,$row1->department_id)?>><?php echo $row1->department_name;?></option>
                    <?php } ?>
                  </select>
				  <input type="hidden" name="department_id" id="department_id" value="<?php echo $department_id; ?>" />
                </div>
              </div>

              <div class="form-group">
                <label for="firstname" class="control-label col-lg-2">First Name <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="first_name" maxlength="25" name="first_name" type="text"  value="<?php if(isset($usersdata[0]->first_name)){ echo $usersdata[0]->first_name;} ?>"/>
                </div>
              </div>

              <div class="form-group">
                <label for="middlename" class="control-label col-lg-2">Middle Name</label>
                <div class="col-lg-10">
                  <input class="form-control" id="middle_name" maxlength="25" name="middle_name" type="text"  value="<?php if(isset($usersdata[0]->middle_name)){ echo $usersdata[0]->middle_name;} ?>"/>
                </div>
              </div>

              <div class="form-group">
                <label for="lastname" class="control-label col-lg-2">Last Name <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="last_name" maxlength="25" name="last_name" type="text"  value="<?php if(isset($usersdata[0]->last_name)){ echo $usersdata[0]->last_name;} ?>"/>
                </div>
              </div>

			  <?php if(!isset($usersdata[0]->user_id)) { ?>
			  <div class="form-group">
                <label for="username" class="control-label col-lg-2">Username <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="username" maxlength="15" name="username" type="text"  value="<?php if(isset($usersdata[0]->username)){ echo $usersdata[0]->username;} ?>"/>
                </div>
              </div>
			  <?php } ?>

			  <div class="form-group">
                <label for="lastname" class="control-label col-lg-2">Password <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="password" maxlength="20" name="password" type="password"  value="<?php if(isset($usersdata[0]->password)){ echo $usersdata[0]->password;} ?>"/>
                </div>
              </div>

              <div class="form-group ">
                <label for="countryid" class="control-label col-lg-2">Country <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="country_id" id="country_id" class="form-control m-bot15">
                  	<?php foreach($country as $row2){ ?>
                    <option value="<?php echo $row2->country_id; ?>" <?php $value1 = (isset($usersdata[0]->country_id)) ? $usersdata[0]->country_id : 0; echo selectedVal($value1,$row2->country_id)?>> <?php echo $row2->short_name; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group ">
                <label for="state_id" class="control-label col-lg-2">State <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="state_id" id="state_id" class="form-control">
                    <option value="">Select State</option>
					<?php if(isset($usersdata[0]->state_id)) { ?>
						<?php foreach($state as $dd){?>
						<option value="<?php echo $dd->state_id;?>" <?php $value1 = (isset($usersdata[0]->state_id)) ? $usersdata[0]->state_id : 0; echo selectedVal($value1,$dd->state_id)?>><?php echo $dd->state_name;?></option>
						<?php } ?>
					<?php } ?>
                  </select>
                </div>
              </div>

               <div class="form-group ">
                <label for="state_id" class="control-label col-lg-2">City <span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="city_id" id="city_id" class="form-control">
                    <option value="">Select City</option>
					<?php if(isset($usersdata[0]->city_id)) { ?>
						<?php foreach($city as $dd1){?>
						<option value="<?php echo $dd1->city_id;?>" <?php $value1 = (isset($usersdata[0]->city_id)) ? $usersdata[0]->city_id : 0; echo selectedVal($value1,$dd1->city_id)?>><?php echo $dd1->city_name;?></option>
						<?php } ?>
					<?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="street" class="control-label col-lg-2">Street <span class="red">*</span></label>
                <div class="col-lg-10">
                  <textarea class="form-control" id="street" name="street" rows="3" cols="40"><?php if(isset($usersdata[0]->street)){ echo $usersdata[0]->street;} ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="zipcode" class="control-label col-lg-2">Zipcode <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="zipcode" maxlength="10" name="zipcode" type="text"  value="<?php if(isset($usersdata[0]->zipcode)){ echo $usersdata[0]->zipcode;} ?>"/>
                </div>
              </div>
              <div class="form-group ">
                <label for="gender" class="control-label col-lg-2">Gender <span class="red">*</span></label>
                <div class="col-lg-6">
                  <select name="gender" id="gender" class="form-control">
                    <option value="">Select Gender</option>
                    <option value="Male" <?php echo isset($usersdata[0]->gender)? selectedVal($usersdata[0]->gender,"Male"): '';?> >Male</option>
                    <option value="Female" <?php echo isset($usersdata[0]->gender)? selectedVal($usersdata[0]->gender,"Female"): '';?>>Female</option>
                  </select>
                </div>

              </div>
              <div class="form-group">
                <label  class="col-lg-2 control-label">Birthday <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="birthdate" name="birthdate" data-date-format="yyyy-mm-dd" placeholder=" "  value="<?php  if(isset($usersdata[0]->birthdate)){ echo $usersdata[0]->birthdate;} ?>">
                </div>
              </div>


              <div class="form-group">
                <label for="mobile" class="control-label col-lg-2">Mobile Number </label>
                <div class="col-lg-10">
                  <input class="form-control" id="mobile" maxlength="10" name="mobile" type="text"  value="<?php if(isset($usersdata[0]->mobile)){ if ($enableNormalView == 1) {echo base64_decode($usersdata[0]->mobile);} else {echo $usersdata[0]->mobile;}} ?>"/>
                </div>
              </div>

              <div class="form-group">
                <label for="landline" class="control-label col-lg-2">Landline Number</label>
                <div class="col-lg-10">
                  <input class="form-control" id="landline" maxlength="10" name="landline" type="text"  value="<?php if(isset($usersdata[0]->landline)){ echo $usersdata[0]->landline;} ?>"/>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="control-label col-lg-2">Personal Email <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="email" name="email" type="text"  value="<?php if(isset($usersdata[0]->email)){ echo $usersdata[0]->email;} ?>"/>
                </div>
              </div>
			  <div class="form-group">
                <label for="alternate_email" class="control-label col-lg-2">Alternate Email </label>
                <div class="col-lg-10">
                  <input class="form-control" id="alternate_email" name="alternate_email" type="text"  value="<?php if(isset($usersdata[0]->alternate_email)){ if ($enableNormalView == 1) {echo base64_decode($usersdata[0]->alternate_email);} else {echo $usersdata[0]->alternate_email;}} ?>"/>
                </div>
              </div>

			  <div class="form-group ">
                <label for="religion_id" class="control-label col-lg-2">Religion<span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="religion_id" id="religion_id" class="form-control">
                    <option value="">Select Religion</option>
                    <?php foreach($religion as $row){?>
                    <option value="<?php echo $row->religion_id;?>" <?php $value1 = (isset($usersdata[0]->religion_id)) ? $usersdata[0]->religion_id : 0; echo selectedVal($value1,$row->religion_id)?>><?php echo $row->religion_name;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

			  <!-----------------------------social media ------------------------>
			  <div class="form-group">
                <label for="facebook_id" class="control-label col-lg-2">Skype Id </label>
                <div class="col-lg-10">
                  <input class="form-control" id="skype" maxlength="20" name="skype" type="text"  value="<?php if(isset($usersdata[0]->skype)){ if ($enableNormalView == 1) { echo base64_decode($usersdata[0]->skype);} else { echo $usersdata[0]->skype;}} ?>"/>

                </div>
              </div>
			  <div class="form-group">
                <label for="facebook_page" class="control-label col-lg-2">Gtalk</label>
                <div class="col-lg-10">
                  <input class="form-control" id="gtalk" maxlength="20" name="gtalk" type="text"  value="<?php if(isset($usersdata[0]->gtalk)){ if ($enableNormalView == 1) {echo base64_decode($usersdata[0]->gtalk);}else{ echo $usersdata[0]->gtalk;} }?>"/>
                </div>
              </div>
			  <div class="form-group">
                <label for="twitter_id" class="control-label col-lg-2">Pancard Number </label>
                <div class="col-lg-10">
                  <input class="form-control" id="pancard_no" maxlength="30" name="pancard_no" type="text"  value="<?php if(isset($usersdata[0]->pancard_no)){ if ($enableNormalView == 1) {echo base64_decode($usersdata[0]->pancard_no);} else { echo $usersdata[0]->pancard_no;} } ?>"/>
                </div>
              </div>
			  <div class="form-group">
                <label for="pf_no" class="control-label col-lg-2">PF Number </label>
                <div class="col-lg-10">
                  <input class="form-control" id="pf_no" maxlength="30" name="pf_no" type="text"  value="<?php  if(isset($usersdata[0]->pf_no)){ if ($enableNormalView == 1) {echo base64_decode($usersdata[0]->pf_no);} else { echo $usersdata[0]->pf_no;}}?>"/>
                </div>
              </div>

							<div class="panel panel-primary">
							<div class="panel-heading" id="focusPass">Bank Account Details</div>
						  </div>

						  <div class="form-group">
			                <label for="pf_no" class="control-label col-lg-2">Bank Name </label>
			                <div class="col-lg-10">
			                  <input class="form-control" id="bank_name" maxlength="20" name="bank_name" type="text"  value="<?php if(isset($usersdata[0]->bank_name)){ if($enableNormalView == 1) {echo base64_decode($usersdata[0]->bank_name);} else { echo $usersdata[0]->bank_name;}} ?>"/>
			                </div>
			              </div>

						  <div class="form-group">
			                <label for="" class="control-label col-lg-2">Account Number </label>
			                <div class="col-lg-10">
			                  <input class="form-control" id="account_number" maxlength="20" name="account_number" type="text"  value="<?php if(isset($usersdata[0]->account_number)){ if($enableNormalView == 1){echo base64_decode($usersdata[0]->account_number);} else { echo $usersdata[0]->account_number;}}?>"/>
			                </div>
			              </div>

						  <div class="form-group">
			                <label for="" class="control-label col-lg-2">IFSC Code</label>
			                <div class="col-lg-10">
			                  <input class="form-control" id="ifsc_code" maxlength="20" name="ifsc_code" type="text"  value="<?php if(isset($usersdata[0]->ifsc_code)){ if($enableNormalView == 1){echo base64_decode($usersdata[0]->ifsc_code);} else { echo $usersdata[0]->ifsc_code;}} ?>"/>
			                </div>
			              </div>

						  <div class="form-group">
			                <label for="" class="control-label col-lg-2">Branch</label>
			                <div class="col-lg-10">
			                  <input class="form-control" id="branch" maxlength="20" name="branch" type="text"  value="<?php if(isset($usersdata[0]->branch)){ if ($enableNormalView == 1){echo base64_decode($usersdata[0]->branch);}  else { echo $usersdata[0]->branch;}}?>"/>
			                </div>
			              </div>
			  <!--------------------------------end of social media-------------------------------->

			  <div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($usersdata[0]->status)? selectedVal($usersdata[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($usersdata[0]->status)? selectedVal($usersdata[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
									<?php if ($enableNormalView == 1){
                  echo '<button class="btn btn-danger" type="submit" name="addEditUsers">';
                  if(isset($usersdata[0]->user_id)){ echo "Update";} else{echo "Submit";}
                  echo '</button>';
								} else {
									if(!isset($usersdata[0]->user_id)){
									echo '<button class="btn btn-danger" type="submit" name="addEditUsers">Add </button>';
									}
								}
									?>

              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
		  <?php } else { ?>
		  <div class="panel-body">Permission Denied</div>
		  <?php } ?>
        </section>
      </div>
    </div>
  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-users.js" ></script>

<script>
$('#country_id').change(function(){

    var country_id = $('#country_id').val();

    $.ajax({
            url: '<?php echo base_url();?>users/getStates',
            type: 'POST',
            data: {
                country_id:country_id,
            },
            success:function(data){
                //alert(data);
				//var obj = JSON.parse(data);
                //alert(obj.yourfield);
				$('#state_id').html(data);
            },
            error:function(){
                alert('Data not Found')
            }


        });

 });
 $('#state_id').change(function(){

    var state_id = $('#state_id').val();

    $.ajax({
            url: '<?php echo base_url();?>users/getCities',
            type: 'POST',
            data: {
                state_id:state_id,
            },
            success:function(data){
                //alert(data);
				//var obj = JSON.parse(data);
                //alert(obj.yourfield);
				$('#city_id').html(data);
            },
            error:function(){
                alert('Data not Found')
            }


        });

 });

</script>
