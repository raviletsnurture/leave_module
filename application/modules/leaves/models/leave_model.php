<?php
class Leave_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}
	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : get department Name
	*/
	function getdepartmentName($department_id){
		$this->db->select('*');
		$this->db->from('crm_department');
		$this->db->where('department_id',$department_id);
		$query=$this->db->get();
		return $query->row();
	}
	//get all meetin details
	function getAllMeetings()
	{
		$this->db->select(" m.*,u.first_name,u.last_name, GROUP_CONCAT( u.first_name ) AS members_name ,(select CONCAT((first_name),(' '),(last_name)) from crm_users where user_id = m.user_id) as user_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("FIND_IN_SET( u.user_id, m.members)");
		$this->db->where("m.schedule_date > now()");
		//$this->db->where("m.user_id = u.u");
		$this->db->group_by("m.meeting_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getLeaveDayMeetings($id)
	{
		$this->db->select(" m.*,u.first_name,u.last_name, GROUP_CONCAT( u.first_name ) AS members_name ,(select CONCAT((first_name),(' '),(last_name)) from crm_users where user_id = m.user_id) as user_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("FIND_IN_SET( u.user_id, m.members)");
		$this->db->where("m.schedule_date > now()");
		$this->db->where("FIND_IN_SET ($id,m.members)");
		//$this->db->where("m.user_id = u.u");
		$this->db->group_by("m.meeting_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}


	function getAllLeave($id)
	{
		$this->db->select('d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->order_by("leave_id","desc");
		$this->db->where('l.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function getLeaveTypeText($leave_id){
		$this->db->select('leave_type');
		$this->db->from('crm_leave_type');
		$this->db->where('leave_type_id',$leave_id);
		$query=$this->db->get();
		return $query->row();
	}
	function checkLeaveDate($date,$user_id,$leave_type){

		$this->db->select('user_id,leave_type');
		$this->db->from('leaves');
		$this->db->where('leave_start_date',$date);
		$this->db->where('user_id',$user_id);
		$this->db->where('is_cancelled = 0');
		$query=$this->db->get();
		$result = $query->result();
		if($result){
			if(count($result) != 1){
				if(($leave_type == 14) && ($result[0]->leave_type == 14 || $result[1]->leave_type == 14)){
					return $result;
				}else if($leave_type == 6 && ($result[0]->leave_type == 14 || $result[1]->leave_type == 14)){

				}else if($leave_type == 14 && ($result[0]->leave_type == 6 || $result[1]->leave_type == 6)){

				}else if($result[0]->leave_type != 14 || $result[1]->leave_type != 14){
					return $result;
				}
			}else{
				if(($leave_type == 14) && $result[0]->leave_type == 14){
					return $result;
				}else if($leave_type == 6 && $result[0]->leave_type == 14){

				}else if($leave_type == 14 && $result[0]->leave_type == 6){

				}else if($result[0]->leave_type != 14 || $result[1]->leave_type != 14){
					return $result;
				}
			}
		}
	}
	function getearlyleavestatus($date,$user_id){
		$this->db->select('user_id,leave_start_date');
		$this->db->from('crm_leaves');
		$this->db->where('leave_type = 6');
		$this->db->where("MONTH( leave_start_date ) = MONTH('".$date."')  AND YEAR( leave_start_date) = YEAR(NOW())");
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		return $query->result();
	}
	function getAllUserLeaves($id,$userid)
	{
		$this->db->select('au.first_name as afirst_name,au.last_name as alast_name,d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('users as au','au.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->order_by("leave_id","desc");
		$this->db->where('l.user_id != '.$userid);
		$this->db->where('u.department_id != '.$id);
		$query=$this->db->get();
		return $query->result();
	}
	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      :
	 * @created		  :	19-07-2017
	 * @Modified by	: Jignasa
	 * @Status		  :
	 * @Comment		  : Get leave list of all users permitted by super admin
	 **/
	function getAllinfo($id)
	{
		$this->db->select('leave_users');
		$this->db->from('users');
		$this->db->where('user_id',$id);
		$this->db->where('leave_users IS NOT NULL', null, false);
		$this->db->where('status','Active');
		$this->db->order_by("first_name","asc");
		$query=$this->db->get();
		$result =  $query->result();
		if(!empty($result)){
			$this->db->select('d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u.email,u.first_name,u.last_name,l.*');
			$this->db->from('leaves as l');
			$this->db->join('users as u','u.user_id = l.user_id', 'left');
			$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
			$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
			$this->db->join('department as d','d.department_id = u.department_id', 'left');
			$this->db->where_in('u.user_id', explode(',',$result[0]->leave_users));
			$this->db->where('u.status','Active');
			$this->db->where('l.is_cancelled','0');
			$this->db->order_by("leave_id","desc");
			$query=$this->db->get();
			return $query->result();
		}else{
			return 0;
		}
		exit;
	}


	function add($data){
	    if($this->db->insert('leaves',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getLeave($catId){
		$this->db->select('*');
		$this->db->from('leaves');
		$this->db->where('leave_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}

	function getCancelLeave($catId){
		$this->db->select('u.email,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->where('l.leave_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}

	function updateLeave($data){
		$this->db->where('leave_id',$data['leave_id']);
		if($this->db->update('leaves',$data)){
			return true;
		}else{
			return false;
		}
	}


 // role id & department id
	function getemails($rid, $did){
		//TL,PM,MGMT,HR,Sr HR
		$roles = '13,15,20,22,49';
		$this->db->select('GROUP_CONCAT(email) as memberEmail');
		$this->db->from('users');
		$this->db->where('role_id in ('.$roles.')');
		$this->db->where("status =  'Active'");
		if($rid != '13' || $rid != '15' || $rid != '20' || $rid != '22' || $rid != '49'){
			$this->db->where("email != 'ketan@letsnurture.com'");
		}
		$query=$this->db->get();
		$arr = $query->row_array();

		$this->db->select('GROUP_CONCAT(sEmail) as memberEmail');
		$this->db->from('super_admin');
		$query=$this->db->get();
		$arr1 = $query->row_array();
		$dd = $arr['memberEmail'].','.$arr1['memberEmail'];
		return $dd;
	}

	function deleteLeave($id)
	{
		$this->db->where('leave_id',$id);
	    $this->db->delete('leaves');
	}

	function get_status($year,$month,$user_id,$id)
	{
		$this->db->select('COUNT(*) as stat_count');
		$this->db->from('leaves');
		if($id == '')
		{
			$this->db->where('YEAR(leave_start_date) = '.$year.' and MONTH(leave_start_date) = '.$month.'  and  user_id = '.$user_id);
		}
		else
		{
			$this->db->where('YEAR(leave_start_date) = '.$year.' and MONTH(leave_start_date) = '.$month.' and leave_id != '.$id.' and status = "Unpaid" and user_id = '.$user_id);
		}
		$query=$this->db->get();
		$arr = $query->result();
		return $arr;
	}

	/*function updateleavecount($data)
	{
		$this->db->set('total_leaves', 'total_leaves+1', FALSE);
		$this->db->where('user_id',$data['user_id']);
		$this->db->update('users');
	}*/

	function getusertotal_leaves($id)
	{
		$this->db->select('total_leaves');
		$this->db->from('users');
		$this->db->where('user_id',$id);
		$query=$this->db->get()->result();
		return $query[0]->total_leaves;
	}


	/* ************
	* EL update function for user
	* @param user_id , action of type Deduct / Putback
	*
	*
	******************** */
	function updateLeaveRecords($user_id, $leaveType, $action, $num_days){
	 // get specific user whose EL details to be updated
	 $this->db->select('total_leaves, earned_leaves, el_balance');
	 $this->db->from('users');
	 $this->db->where('user_id',$user_id);
	 $query=$this->db->get()->result();
   // 3 - EL, 4 -Urgent, 5 - Sick, 8 - Birthday, 9 - Annivarsary
	 //$updated_sick = $query[0]->sick_leaves;
	 $updated_total = $query[0]->total_leaves;
	 $updated_EL = $query[0]->earned_leaves;
   //var_dump($leaveType);
	 switch ($leaveType){
	   case 3:
		 						if($action == "P"){
									$updated_total = $query[0]->total_leaves + $num_days;
									$updated_EL = $query[0]->earned_leaves + $num_days;
									}else{
		 							$updated_total = $query[0]->total_leaves - $num_days;
									$updated_EL = $query[0]->earned_leaves - $num_days;
								}
		 						break;

		 case 10:	// Do nothing for marriage leave
		 						//$updated_total = $query[0]->total_leaves - $num_days;
		 						//$updated_EL = $query[0]->earned_leaves - $num_days;
		 						break;

		 case 12:	// Do nothing for Work from home leave
					 		 	//$updated_total = $query[0]->total_leaves - $num_days;
					 		 	//$updated_EL = $query[0]->earned_leaves - $num_days;
					 		 	break;

		default:
								if($action == "P"){
									$updated_total = $query[0]->total_leaves + $num_days;
									$updated_EL = $query[0]->earned_leaves + $num_days;
								}else{
									$updated_total = $query[0]->total_leaves - $num_days;
									$updated_EL = $query[0]->earned_leaves - $num_days;
								}
								break;

	 }

   $data['total_leaves'] = $updated_total;
	 //$data['el_balance'] = $updated_sick;
	 $data['earned_leaves'] = $updated_EL;

	 $this->db->where('user_id',$user_id);
	 if($this->db->update('users',$data)){
		 return true;
	 }else{
		 return false;
	 }

 die;
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : check When more than 3 team members has applied for leave on same day from same department. Send Alert to TLs , PM & HR.
	*/
	function checkTotalLeaveOnSameDays($leave_start_date,$leave_end_date,$department_id){
		$this->db->select('user_id');
		$this->db->from('leaves');
		$this->db->where('leave_start_date >=',$leave_start_date);
		$this->db->where('leave_end_date <=',$leave_end_date);
		$this->db->where('department_id',$department_id);
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : When More then 1 TL from same department has applied for Leave please send alert to TL , PM & HR ( Notification + Email )
	*/
	function checkTLTotalLeaveOnSameDays($leave_start_date,$leave_end_date,$department_id,$role_id){
		$this->db->select('l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->where('l.leave_start_date >=',$leave_start_date);
		$this->db->where('l.leave_end_date <=',$leave_end_date);
		$this->db->where('l.department_id',$department_id);
		$this->db->where('l.is_cancelled != 1');
		$this->db->where('l.leave_status in (1,2)');
		$this->db->where('u.role_id',$role_id);
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	leave type = EL, Urgent, Birthday Leave, Anniversary Leave, Marriage Leave, Early Leave.
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function checkTotalNumberLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		$this->db->select('SUM( l.leave_days) as totalLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		$this->db->where('l.leave_type in (3,4,6,8,9,10)');
		$this->db->where('l.leave_status = 2');
		$query=$this->db->get();
		return $query->row();
	}


	/*
	* Developed By : Krunal
	* Date : 10/04/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function getActiveEmployee(){
		$this->db->select('COUNT(user_id)');
		$this->db->from('crm_users');
		$this->db->where("status =  'Active'");
		return $this->db->count_all_results();
	}

	function commnetAdd($leavdeid,$comment)
	{
			$data = array(
				'comment'=>$comment
			);
			$this->db->set($data);
			$this->db->where('leave_id',$leavdeid);
			$update = $this->db->update('crm_leaves');

			$this->db->select('*');
			$this->db->from('crm_leaves');
			$this->db->where('leave_id',$leavdeid);
			$query=$this->db->get();
			return $query->row();
	}

	function deleteLeaveFromLeaveMonth($id)
	{
		$this->db->where('leave_id',$id);
	    $this->db->delete('leave_month');
	}
}
?>
