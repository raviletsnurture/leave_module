<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leaves extends MX_Controller {
    var $logSession;
	function __construct(){
		parent::__construct();
      $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('leave_model');
        $this->load->library('email');
        date_default_timezone_set("Asia/Kolkata");

	}

	function index(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
    $data['role_id'] = $userData[0]->role_id;
		$data['user_id'] = $userData[0]->user_id;
		$data['titalTag'] = ' - Leave';
		$data["leave"] = $this->leave_model->getAllLeave($id);

		/*----other department members leave-----*/
		$inf['id'] = $id;
		$inf['department_id'] = $userData[0]->department_id;

		/*--------- end----------*/

		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);



    $data["allleave"] = $this->leave_model->getAllinfo($inf['id']);
		$data['permission'] = $per;
		/*------------end------------*/

    $this->template->load_partial('dashboard_master','leaves/list_leave',$data);
	}

  function checkLeaveMeeting(){
    exit;
    $loginSession = $this->session->userdata("login_session");
    $addData['user_id'] = $loginSession[0]->user_id;
    $addData['leave_start_date'] = $this->input->post('leave_start_date');
    $addData['leave_end_date'] = $this->input->post('leave_end_date');
    //metting compearision
    $getallmeeting = $this->leave_model->getLeaveDayMeetings($loginSession[0]->user_id);

    $total_leave_days = array();
    $email_add = array();
    $you_have = '';
    foreach($getallmeeting as $meeting_details){

      $selected_members = explode(",",$meeting_details['members']);
      $schedule_date = $meeting_details['schedule_date'];
      array_push($selected_members,$meeting_details['user_id']);


      $period = new DatePeriod(new DateTime($addData['leave_start_date']),new DateInterval('P1D'),new DateTime($addData['leave_end_date']));
      foreach($period as $list_dates){
        array_push($total_leave_days,$list_dates->format('Y-m-d'));
      }
      array_push($total_leave_days,$addData['leave_end_date']);

      $total_leave_days = array_unique($total_leave_days);
      foreach($selected_members as $memb){

        $email_data = $this->leave_model->getLoggedInUser($memb);
        array_push($email_add,$email_data[0]->email);

        if(($memb == $addData['user_id'])){
          foreach($total_leave_days as $leave_list){
            if($leave_list == $schedule_date){
              $date_format =date_create($leave_list);
              $you_have .= date_format($date_format,"d-M-Y").',';
            }
          }
        }
      }
      $total_leave_days = array();
    }

    if($you_have != ""){
      $you_have = rtrim($you_have,",");
      $you_have = implode(",",array_unique(explode(",",$you_have)));
      echo $you_have.";";
      $final_email = "";
      foreach ($email_add as $user_email) {
        $final_email .=  $user_email.',';
      }
      $final_email = rtrim($final_email,",");
      //Get logged in user's email id
      $curr_user = $this->leave_model->getLoggedInUser($addData['user_id']);
      //Remove users email id from list
      $final_email = array_diff(array_unique(explode(",",$final_email)),(array)$curr_user[0]->email);
      $final_email = implode(",",$final_email);
      echo $final_email;
      exit;
    }
  }

function add($leaveId = ''){
    $data['titalTag'] = ' - Add Leave';
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
    $user_id = $loginSession[0]->user_id;
		$department_id = $loginSession[0]->department_id;
		$per = getUserpermission($role_id);
    $departmentName = $this->leave_model->getdepartmentName($department_id);
		$data['login_session'] = $loginSession;
		$data['permission'] = $per;

    $addData['user_id'] = $user_id;
    $addData['department_id'] =$department_id;
    $leave_type = $this->input->post('leave_type');
    if(isset($_POST['leave_start_date']) && !empty($_POST['leave_start_date'])){
      $leave_start_date = $_POST['leave_start_date'];

      //We will have one new leave type "Exam Leave (only for college interns)"
      if($role_id != 10 && $leave_type == 18){
        $this->session->set_flashdata('error','You cannot apply this request type.');
        redirect(base_url().'leaves');exit;
      }

      //if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.They will need to discuss with HR Team for Leave. leave type = EL, Urgent, Birthday Leave, Anniversary Leave, Marriage Leave, Early Leave.( HR team can add leave)
     /* if($leave_type == 3 || $leave_type == 4 || $leave_type == 6 || $leave_type == 8 || $leave_type == 9 || $leave_type == 10){
      	$checkTotalNumberLeave = $this->leave_model->checkTotalNumberLeave($leave_start_date);
        $totalThisMonthLeave =  $checkTotalNumberLeave->totalLeave;
        $getActiveEmployee = $this->leave_model->getActiveEmployee();
        if($totalThisMonthLeave > $getActiveEmployee){
          //$data['leavePermission'] = "noPermission";
          $this->session->set_flashdata('error','You will need to discuss with HR Team for this request type.');
          redirect(base_url().'leaves');exit;
          exit;
        }
      } */

      if($leave_type == 7){
        $date = date('Y-m-d');
        $startDate1 = date('Y-m-d',strtotime($leave_start_date));
        if($startDate1 == $date){
          $time = date('H:i:s',strtotime("10 AM"));
          if($time < date('H:i:s')){
             $this->session->set_flashdata('error','You can not applied leave after 10 AM.');
              redirect(base_url().'leaves');
              exit();
          }
        }
      }

      			//Check if any meeting on leave date
            if(isset($_POST['leave_message']) && !empty($_POST['leave_message'])){
              $leave_array = explode(";",$_POST['leave_message']);
              $leave_message = $leave_array[0];
              $leave_email = explode(",",$leave_array[1]);
              $mailData['body'] = '<tr>
                            <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$loginSession[0]->first_name.' '.$loginSession[0]->last_name.'</strong> is on leave for <strong style="color:#03A9F4;">'.$leave_message.'</strong> and has a meeting with you.</p></td>
                            </tr>';

              $mailData['name'] = '';
              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
              $this->email->bcc($leave_email);
              $this->email->subject("Leave taken while meeting schedule");
              $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
              $this->email->message($body);
             // //$this->email->send();
            }


      			if($this->input->post('leave_type') != ''){
      				$addData['leave_type'] = $this->input->post('leave_type');
              //get leave type name by type id
              $text_leave = $this->leave_model->getLeaveTypeText($addData['leave_type']);
              $leaveData['leave_text_msg'] =$text_leave->leave_type;
      			}

      			$date = strtotime(date($leave_start_date));
      			$now = strtotime(date("Y-m-d"));
      			if($date < $now){
      				$addData['leave_status'] = '4';
      			}else{
      				if($this->input->post('leave_status') != ''){
      					$addData['leave_status'] = $this->input->post('leave_status');
      				}else{
      					$addData['leave_status'] = '1';
      				}
      			}



            $addData['leave_start_date'] = $leave_start_date;
      			if($this->input->post('leave_end_date') != ''){
      				$addData['leave_end_date'] = $this->input->post('leave_end_date');
      			}
            $addData['leave_reason']= $this->input->post('leave_reason');
            $addData['leave_critical_task']= $this->input->post('leave_critical_task');
            $addData['leave_res_assignee']= $this->input->post('leave_res_assignee');

      			/*-----To check the total number of leaves of users ----------*/
      			$chk_no_leave_o = getAllLeaveType();
      			$chk_no_leave = $chk_no_leave_o[0]->leave_amount;
      			$user_no_leave = $this->leave_model->getusertotal_leaves($loginSession[0]->user_id);
      			/*-----------*/


      		 /*---------checking the +3 working days of adding leaves---------*/
           $todayDate = date("Y-m-d");
           $work_days = getWorkingDays($todayDate, $leave_start_date);
           if($addData['leave_type'] == 4 || $addData['leave_type'] == 6 || $addData['leave_type'] == 7 || $addData['leave_type'] == 14 || $addData['leave_type'] == 12){
             $a = 'yes';
           }else{
             if($work_days < 4){
               $a = 'false';
             }else{
               $a = 'yes';
             }
           }
          if($a == 'yes'){
            //update leave record
      			if(is_numeric($leaveId) && $leaveId != ''){
      					if($user_no_leave >= $chk_no_leave){
      						$addData['status']= 'Paid';
      					}
      					$addData['leave_id']= $leaveId;
      					$addData['leave_created'] = date('Y-m-d H:i:s');
                $addData['leave_days'] = getDaysCount($addData['leave_start_date'],$addData['leave_end_date']);
      					$data = $this->leave_model->updateLeave($addData);
                $this->session->set_flashdata('success','Leave updated successfully!');
      			}else{
                //check alreay add leave same date
                $checkLeaveDate = $this->leave_model->checkLeaveDate($_POST['leave_start_date'], $addData['user_id'],$addData['leave_type']);
                if($addData['leave_type'] == 6){
                  $early_leave_count = $this->leave_model->getearlyleavestatus($_POST['leave_start_date'],$addData['user_id']);
                }else{
                  $early_leave_count = '';
                }
                //check same day apply
                if(empty($checkLeaveDate)){
                     //Eraly leave status check
                    if(empty($early_leave_count)){
                      if($user_no_leave >= $chk_no_leave){
                        $addData['status']= 'Paid';
                      }
                      $addData['leave_created'] = date('Y-m-d H:i:s');
                      $addData['leave_days'] = getDaysCount($addData['leave_start_date'],$addData['leave_end_date']);
                      $data = $this->leave_model->add($addData);
                      $num_days = getDaysCount($addData['leave_start_date'],$addData['leave_end_date']);
                      $ELupdation = $this->leave_model->updateLeaveRecords($addData['user_id'], $addData['leave_type'], "D", $num_days);


                      //Send Alert to TLs , PM & HR.
                      $managementEmails = getSeniorEmails($addData['user_id'], 'leave_users');

                      $addData['leave_text_msg'] = $leaveData;
                      $this->email->from('hrms@letsnurture.com', "HRMS");
                      $this->email->to('hrms@letsnurture.com');
                      $this->email->bcc($managementEmails,'ketan@letsnurture.com');
                      $this->email->subject("Request for ".$leaveData['leave_text_msg']);
                      $body = $this->load->view("leaves/leave_request_tmp",$addData, true);
                      $this->email->message($body);
                     // //$this->email->send();

                      //Mail will be send to that responsible person who is going to work or look after the work on behalf of the applicant will be filled in By Applicant.
                      $mailData['body'] = '<tr>
                                            <td colspan="2" style="font-size:14px; color:#FFF;
                                             padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.$loginSession[0]->first_name.' '.$loginSession[0]->last_name.'</b> is on leave and assigned you responsibility for ongoing projects work.
                                             </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" style="font-size:14px; color:#FFF;
                                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Leave Duration : '.'</b>From <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($addData['leave_start_date'])).'</b> To <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($addData['leave_end_date'])).'</b>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" style="font-size:14px; color:#FFF;
                                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Reason for leave : '.'</b>'.$addData['leave_reason'].'
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" style="font-size:14px; color:#FFF;
                                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Critical Task : '.'</b>'.$addData['leave_critical_task'].'
                                              </td>
                                            </tr>';
                      $userDetails = $this->leave_model->getLoggedInUser($this->input->post('leave_res_assignee'));
                      $mailData['name'] = '';
                      $this->email->from('hrms@letsnurture.com', "HRMS");
                      $this->email->to('hrms@letsnurture.com');
                      $this->email->bcc($userDetails->email);
                      $this->email->subject($loginSession[0]->first_name.' on leave and assigned you as responsible for work');
                      $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                      $this->email->message($body);
                     // //$this->email->send();

                      //check When more than 3 team members has applied for leave on same day from same department. Send Alert to TLs , PM & HR.
                      $checkTotalLeaveOnSameDays = $this->leave_model->checkTotalLeaveOnSameDays($addData['leave_start_date'],$addData['leave_end_date'],$department_id);
                      if($checkTotalLeaveOnSameDays > 3){
                        $mailData['body'] = '<tr>
                                             <td colspan="2" style="font-size:14px; color:#FFF;
                                               padding:0 40px 20px 40px;">More than 3 <b style="color:#03A9F4;">'.$departmentName->department_name.'</b> team members has applied for leave on same day <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($addData['leave_start_date'])).'</b></td>
                                           </tr>';
                        $mailData['name'] = '';
                        $this->email->from('hrms@letsnurture.com', "HRMS");
                        $this->email->to('hrms@letsnurture.com');
                        $this->email->bcc($managementEmails);
                        $this->email->subject('More than 3 team members has applied for leave');
                        $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                        $this->email->message($body);
                       // //$this->email->send();
                      }

                      //When More then 1 TL from same department has applied for Leave please send alert to TL , PM & HR ( Notification + Email )
                      if($role_id = 13){
                        $checkTLTotalLeaveOnSameDays = $this->leave_model->checkTLTotalLeaveOnSameDays($addData['leave_start_date'],$addData['leave_end_date'],$department_id,$role_id);
                        if($checkTLTotalLeaveOnSameDays > 1){
                            $mailData['body'] = '<tr>
                                                 <td colspan="2" style="font-size:14px; color:#FFF;
                                                   padding:0 40px 20px 40px;">More than 1 <b style="color:#03A9F4;">'.$departmentName->department_name.'</b> TL has applied for leave on same day <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($addData['leave_start_date'])).'</b></td>
                                               </tr>';
                            $mailData['name'] = '';
                            $this->email->from('hrms@letsnurture.com', "HRMS");
                            $this->email->to('hrms@letsnurture.com');
                            $this->email->bcc($managementEmails);
                            $this->email->subject('Alert - TL Leave Congestion in '.$departmentName->department_name);
                            $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
                            $this->email->message($body);
                          //  //$this->email->send();
                        }
                      }

                      /*Send push Notification*/
                      //Send Alert to TLs , PM & HR.
                      $deviceTokens = getSeniorTokens($addData['user_id'], 'leave_users');
                      //$deviceTokens = getAllDeviceToken();//send to all user
                      $Tokenlist = array();
                      foreach($deviceTokens as $value){
                        array_push($Tokenlist,$value->deviceToken);
                      }
                      $notification_message = $loginSession[0]->first_name.' '.$loginSession[0]->last_name.' requested for '.$leaveData['leave_text_msg'].' from '.date('d-M-Y',strtotime($addData['leave_start_date'])).' to '.date('d-M-Y',strtotime($addData['leave_end_date']));
                      SendNotificationFCMWeb('Request for '.$leaveData['leave_text_msg'],$notification_message,$Tokenlist);
                      /*Send push Notification*/

                      $this->session->set_flashdata('success','Request Added successfully!');
                    }else{
                      $this->session->set_flashdata('error','You can apply only one early leave per month.');
                    }
                }else{
        					$this->session->set_flashdata('error','Already applied for request on same day!');
        				}
      			}
          }else{
            $this->session->set_flashdata('error','Please Select the date prior +3 working days!');
          }

			redirect(base_url().'leaves');exit;
		}
    $this->template->load_partial('dashboard_master','leaves/add_edit_leave',$data);
}

function addemployleave($leaveId = ''){
  $data['titalTag'] = ' - Add Request';
  $loginSession = $this->session->userdata("login_session");
  $role_id = $loginSession[0]->role_id;
  $user_id = $loginSession[0]->user_id;
  $department_id = $loginSession[0]->department_id;
  $per = getUserpermission($role_id);
  $departmentName = $this->leave_model->getdepartmentName($department_id);
  $data['login_session'] = $loginSession;
  $data['permission'] = $per;
  $data['user_id'] = $user_id;
  $data['department_id'] =$department_id;
  if($this->input->post("addEditLeave") !== false)
  {
    $addData['user_id'] = $this->input->post('user_id');
    $addData['leave_type'] = $this->input->post('leave_type');
    $addData['leave_status'] = $this->input->post('leave_status');
    $addData['leave_reason'] = $this->input->post('leave_reason');
    $addData['leave_start_date'] = $this->input->post('leave_start_date');
    $addData['leave_end_date'] = $this->input->post('leave_end_date');
    $addData['leave_res_assignee'] = $user_id;
    $addData['approved_by'] = $user_id;
    $addData['leave_approved_by'] = $user_id;
    $addData['leave_days'] = getDaysCount($addData['leave_start_date'],$addData['leave_end_date']);


    //$addData['status'] = $this->input->post('status');
    $userDetails = getUserDetail($addData['user_id']);
    $addData['department_id'] = $userDetails->department_id;
    if(is_numeric($leaveId) && $leaveId != '')
    { /*when update*/
      $addData['leave_id']= $leaveId;
      //$addData['leave_updated'] = date('Y-m-d H:i:s');
      $data = $this->leave_model->updateLeave($addData);
      $this->session->set_flashdata('success','Leave updated successfully!');
    }else{
      //$addData['leave_created'] = date('Y-m-d H:i:s');
      //$addData['leave_updated'] = date('Y-m-d H:i:s');
      $data = $this->leave_model->add($addData);
      $this->session->set_flashdata('success','Leave Added successfully!');
    }
    redirect(base_url().'leaves');exit;
  }
  $this->template->load_partial('dashboard_master','leaves/employ_add_edit_leave',$data);
}


	public function edit($leaveId){
		$data['titalTag'] = ' - Update Leave';
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		$data['login_session'] = $this->session->userdata("login_session");
		$data['permission'] = $per;

		$data['leaveEdit'] = $this->leave_model->getLeave($leaveId);

		if($data['leaveEdit'][0]->user_id == $loginSession[0]->user_id){
			if($data['leaveEdit'][0]->leave_status == 1){
				$this->template->load_partial('dashboard_master','leaves/add_edit_leave',$data);
			}else{
				redirect(base_url().'leaves');exit;
			}
		}else{
			redirect(base_url().'leaves');exit;
		}
	}

	public function useredit($leaveId)
	{
		$data['titalTag'] = ' - Update Leave';
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		$data['login_session'] = $this->session->userdata("login_session");
		$data['permission'] = $per;

		$data['leaveEdit'] = $this->leave_model->getLeave($leaveId);
		$this->template->load_partial('dashboard_master','leaves/add_edit_user_leave',$leaveId);
	}

	public function delete($leaveId)
	{
		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		$data['permission'] = $per;
		/*------------end------------*/


		if(isset($per[3]) && $per[3]->module_delete == 'yes' || $role_id == 20 || $role_id == 22 )
		{
			$this->leave_model->deleteLeave($leaveId);
      //$num_days = getDaysCount($addData['leave_start_date'],$addData['leave_end_date']);
      //$ELupdation = $this->leave_model->updateLeaveRecords($addData['user_id'], $addData['leave_type'], "P", $num_days);
			$this->session->set_flashdata('success','Leave deleted successfully!');
      //auditUserActivity('leaves', 'delete/'.$leaveId, $loginSession[0]->user_id, date('Y-m-d H:i:s'),$leaveId);
			redirect(base_url().'leaves');
			exit;
		}
		else
		{
			$this->session->set_flashdata('error','Permission Denied!');
			redirect(base_url().'leaves');
			exit;
		}
	}

	public function updatestaus(){

    $data['leave_id'] = $this->input->post('leave_id');
    $data['leave_status'] = $this->input->post('status');
		$loginSession = $this->session->userdata("login_session");
		$user_id = $loginSession[0]->user_id;
		$data['approved_by'] = $user_id;
    $data['leave_approved_by'] = $user_id;
    $addData['user_id'] = $this->input->post('userid');
    $addData['leave_start_date'] = $this->input->post('leave_start_date');
    $addData['leave_end_date'] = $this->input->post('leave_end_date');

      //Check meeting status
      if($data['leave_status'] == 2){

        //metting compearision
        $getallmeeting = $this->leave_model->getAllMeetings();

        $you_have = '';
        $metting_description = '';
        foreach($getallmeeting as $meeting_details){

          $selected_members = explode(",",$meeting_details['members']);
          $schedule_date = $meeting_details['schedule_date'];
          array_push($selected_members,$meeting_details['user_id']);

          $total_leave_days = array();

          $start = new DateTime($addData['leave_start_date']);
          $start->format('Y-m-d');
          $end = new DateTime($addData['leave_end_date']);
          $end->format('Y-m-d');
          $period = new DatePeriod($start,new DateInterval('P1D'),$end);

          foreach($period as $list_dates){
            array_push($total_leave_days,$list_dates->format('Y-m-d'));
          }
          array_push($total_leave_days,$end->format('Y-m-d'));

          foreach($selected_members as $memb){
            if(($memb == $addData['user_id'])){
              foreach($total_leave_days as $leave_list){
                if($leave_list == $schedule_date){

                  $date_format =date_create($leave_list);
                  $email_date = date_format($date_format,"d-M-Y");
                  $you_have .= $leave_list.'<br>';
                  $metting_description .= '<tr>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                      <p style="margin:4px 0;"></p>
            					<p><strong style="color:#03A9F4;">Date: </strong></p>
            				</td>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                      <p style="margin:4px 0;"></p>
            					<p>'.$email_date.'</p>
            				</td>
                  </tr>
                  <tr>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                      <p style="margin:4px 0;"></p>
            					<p><strong style="color:#03A9F4;">Time: </strong></p>
            				</td>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                      <p style="margin:4px 0;"></p>
            					<p>'.date('g:i a', strtotime($meeting_details['schedule_time'])).'</p>
            				</td>
                  </tr>
                  <tr>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                      <p style="margin:4px 0;"></p>
            					<p><strong style="color:#03A9F4;">Subject: </strong></p>
            				</td>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                      <p style="margin:4px 0;"></p>
            					<p>'.$meeting_details['subject'].'</p>
            				</td>
                  </tr>
                  <tr>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
                      <p style="margin:4px 0;"></p>
            					<p><strong style="color:#03A9F4;">Agenda: </strong></p>
            				</td>
            				<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                      <p style="margin:4px 0;"></p>
            					<p>'.$meeting_details['description'].'</p>
            				</td>
                  </tr>'
                  ;
                }
              }
            }
          }

        }
        if($you_have != ""){
          $you_have = '<tr>
            <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
              <p style="margin:4px 0;"></p>
              <p>You have meeting schedule as follow. Please manage accordingly</p>
            </td>
          </tr>';
          $metting_description;
          $email_data = $this->leave_model->getLoggedInUser($addData['user_id']);


          $meet['description'] = $you_have;
          $meet['description'] .= $metting_description;

          $this->email->from('hrms@letsnurture.com', "HRMS");
          $this->email->to('hrms@letsnurture.com');
          $this->email->bcc($email_data[0]->email);
          $this->email->subject("Meeting reminder on leave");
          $body = $this->load->view("leaves/meeting_reminder_mail",$meet, true);
          $this->email->message($body);
          //$this->email->send();
          echo 'ok';
        }
      }

    $this->leave_model->updateLeave($data);
    $id = $this->input->post('leave_id');

    $leave['leave'] = $this->leave_model->getCancelLeave($id);
    //Getting leave type
    $leave_type_id = $leave['leave'][0]->leave_type;
    $text_leave = $this->leave_model->getLeaveTypeText($leave_type_id);
    $leaveData['leave_text_msg'] =$text_leave->leave_type;

    $this->db->select('l.leave_start_date,l.leave_end_date,ls.leave_status,u.email,u.first_name,u.last_name,u.user_id,,u.email');
		$this->db->from('users as u');
    $this->db->join('leaves as l','l.user_id = u.user_id', 'left');
    $this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->where('l.leave_id',$this->input->post('leave_id'));
		$query=$this->db->get()->row();

		$leave['first_name'] = $query->first_name;
		$leave['last_name'] = $query->last_name;
		$leave['leave_status'] = $query->leave_status;
		$leave['leave_start_date'] = $query->leave_start_date;
    $leave['leave_end_date'] = $query->leave_end_date;
    $leave['leave_text_msg'] = $leaveData;

    if($leave['leave_status'] ==3){
      // Rejected - EL roll back please
      $ELupdation = $this->leave_model->updateLeaveRecords($query->user_id, $query->leave_type, "P", $query->leave_days);
    }

    $managementEmails = getSeniorEmails($addData['user_id'], 'leave_users');
    $developerEmail = $this->input->post('email');
    $this->email->from('hrms@letsnurture.com', "HRMS");
    $this->email->to($query->email);
    $this->email->bcc($managementEmails);
    $this->email->subject("Request ".$query->leave_status);
    $body = $this->load->view("leaves/request_answer_tmp",$leave, true);
    $this->email->message($body);
    //$this->email->send();
	}

	public function request()
	{
		$id = $this->input->post('leave_id');
	  $loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$department_id = $loginSession[0]->department_id;

		if($id!='')
		{
			$leave['leave'] = $this->leave_model->getCancelLeave($id);

      //Getting leave type
      $leave_type_id = $leave['leave'][0]->leave_type;
      $text_leave = $this->leave_model->getLeaveTypeText($leave_type_id);
      $leaveData['leave_text_msg'] =$text_leave->leave_type;
      $leave['leave_text_msg'] = $leaveData;

      // Cancelled now rollback EL
      $num_days = getDaysCount($leave['leave'][0]->leave_start_date,$leave['leave'][0]->leave_end_date);
      //$ELupdation = $this->leave_model->updateLeaveRecords($leave['leave'][0]->user_id, $leave['leave'][0]->leave_type, "P", $num_days);

			$toEmail = $leave['leave'][0]->email;
			if($toEmail != ''){

                $managementEmails = getSeniorEmails($loginSession[0]->user_id, 'leave_users');
                $this->email->from('hrms@letsnurture.com', "HRMS");
                $this->email->to('hrms@letsnurture.com');
                $this->email->bcc($managementEmails);
                $this->email->subject($text_leave->leave_type." cancelation Request");
                $body = $this->load->view("leaves/cancel_tmp",$leave, true);
                $this->email->message($body);
                //$this->email->send();

				$this->db->set('is_cancelled','1',False);
				$this->db->where('leave_id',$id);
				$this->db->update('leaves');

        //delete leave from table leave_month
        $this->leave_model->deleteLeaveFromLeaveMonth($id);
			}else{
				$this->session->set_flashdata('error','Email Not Found!');
				echo "false";exit;
			}
		}
	}

	public function all()
	{
		$userData = $this->logSession;
		$id = $userData[0]->user_id;

		$data['user_id'] = $userData[0]->user_id;
		$data['titalTag'] = ' - All Users Leave';
		$data["leave"] = $this->leave_model->getAllUserLeaves($userData[0]->department_id,$userData[0]->user_id);

		/*----other department members leave-----*/
		$inf['id'] = $id;
		$inf['department_id'] = $userData[0]->department_id;
		//$data["allleave"] = $this->leave_model->getAllinfo($inf);
		/*--------- end----------*/
		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

    //check is user module exist or not
    $count = count($per);
    for($i=0;$i<$count;$i++){
      if($per[$i]->module_name == 'users'){
        $module_load = 'true';
      }else{
        $module_load = 'false';
      }
    }

		$data['permission'] = $per;
		/*------------end------------*/
    $this->session->set_userdata('referred_from', current_url());
    $referred_from = $this->session->userdata('referred_from');
		$data['role_id'] = $role_id;

    if($module_load == 'true'){
      $this->template->load_partial('dashboard_master','leaves/all_leave',$data);
    }else{
       $this->index();
    }
	}

  function commnetAdd(){
    $leavdeid = $_REQUEST['Leave_ID'];
    $comment = $_REQUEST['comment_Add'];
    $startDate = $_REQUEST['Leave_Start_Date'];
    $endDate = $_REQUEST['Leave_End_Date'];
    $Request_Type = $_REQUEST['Request_Type'];
    $response =  $this->leave_model->commnetAdd($leavdeid,$comment);
    $mailData['body'] = '<tr>
                          <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
                             Your requested '.$Request_Type.' from <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($startDate)).' </b>to <b style="color:#03A9F4;">'.date('d-M-Y',strtotime($endDate)).'</b>
                           </td>
                       </tr>
                       <tr>
                          <td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;"><b>Comment : </b>'.$comment.'</td>
                       </tr>';
    $mailData['name'] = '';

    $userDetails = getUserDetail($response->user_id);
    $this->email->from('hrms@letsnurture.com', "HRMS");
    $this->email->to($userDetails->email);
    $this->email->subject("Comment on your request");
    $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
    $this->email->message($body);
    if($this->email->send()){
      echo "1";
    }else{
      echo "2";
    }

  }
}
?>
