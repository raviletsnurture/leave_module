<?php
$user = getAllUsers();
$department = getAllDepartment();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();

for($i=0;$i<sizeof($permission);$i++)
{
	$nm = $permission[$i]->module_name;
	$arr[$nm] = $permission[$i];
}
// echo "<pre>";
// print_r($arr);
// exit;
$role_id = $arr['users']->role_id;
$role_view = $arr['users']->module_view;
$role_update = $arr['users']->module_edit;
?>

<section id="main-content">
  <section class="wrapper site-min-height">

    <!-- page start-->

    <section class="panel">
      <?php if($role_view == 'yes') { ?>
	  <header class="panel-heading">All Department User Leaves </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">

          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>



          <select id="engines">
            <option value="">Select Users</option>
            <?php foreach($user as $row){?>
            <option value="<?php echo $row->first_name; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
            <?php } ?>
          </select>

		  <select id="engines1">
            <option value="">Select Department</option>
            <?php foreach($department as $row1){?>
            <option value="<?php echo $row1->department_name; ?>"><?php echo $row1->department_name;?></option>
            <?php } ?>
          </select>

		  <select id="engines2">
            <option value="">Request Type</option>
            <?php foreach($leave_type as $bo){?>
            <option value="<?php echo $bo->leave_type; ?>"><?php echo $bo->leave_type; ?></option>
            <?php } ?>
          </select>

		  <select id="engines3">
            <option value="">Request Status</option>
            <?php foreach($leave_status as $bow){?>
            <option value="<?php echo $bow->leave_status; ?>"><?php echo $bow->leave_status;?></option>
            <?php } ?>
          </select>

          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
				        <th width="15%">Full Name</th>
								<th>Department</th>
								<th width="15%">Request Type</th>
								<th width="10%">Start Date</th>
								<th width="10%">End Date</th>
								<th width="12%">Request Status</th>
								<th width="15%">Approved/Rejected By</th>
								<th width="12%">Change Status</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($leave as $row){ ?>
              <tr>
                <td><?php echo $row->first_name.' '.$row->last_name;?></td>
					<td><?php echo $row->department_name;?></td>
					<td><?php echo $row->ltype; ?></td>
					<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_start_date))."</span>". date("d-m-Y", strtotime($row->leave_start_date)) ?></td>
					<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_end_date))."</span>". date("d-m-Y", strtotime($row->leave_end_date)) ?></td>
					<td><?php echo $row->lstatus; ?><?php if($row->is_cancelled == '1') { echo " | Cancelled"; } ?></td>
					<td><?php echo $row->afirst_name.' '.$row->alast_name; ?></td>
					<?php
					$checkRole = getRoleUser($row->leave_approved_by);
					if (isset($checkRole[0])){
						$finalRoleId = $checkRole[0]->role_id;
					} else {
						$finalRoleId = $role_id;
					}
					?>
					<?php if($role_update == 'yes') {?>
					<td>
						<select id="leave_status" name="leave_status" onchange="ff(this)">
							<option value="">--Select--</option>
							<?php foreach($leave_status as $bow){ ?>
							<option value="<?php echo $bow->leave_status_id; ?>,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>" <?php if($bow->leave_status_id == $row->leave_status) { ?>selected="selected"<?php } ?>><?php echo $bow->leave_status;?></option>
							<?php } ?>
						</select>
					</td>
				<?php	} else {
					echo "<td>&nbsp;</td>";}
					 ?>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>



	<?php } else { ?>
	 <div class="panel-body">Permission Denied</div>
	<?php } ?>
    <!--SELECT sum(DATEDIFF(leave_end_date,leave_start_date) +1) as difference FROM `crm_leaves` WHERE YEAR(leave_start_date) = '2015' and MONTH(leave_end_date) = '03' and user_id = 2 -->


	</section>



    <!-- page end-->
  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">

function ff(sel)
{
	var ff = sel.value.split(",");
	if(ff == '')
	{
		alert("Please Select an Option");
		location.reload();
		//return false;
	}
	else
	{
		var r = confirm("Are You Sure for this operation");
		if (r == true)
		{
			// show loader
			$('#loaderAjax').show();
			$.ajax({
			 url: '<?php echo base_url(); ?>leaves/updatestaus/',
			 data: { leave_id: ff[1], status: ff[0], userid: ff[2]},
			 dataType: 'html',
			 type: 'POST',
			 success: function(data){
				 // hide loader
				 $('#loaderAjax').hide();
				 //$('.modal-body').html(data);
				 //$('#myModal').modal('show');
				 //alert("Leave Status Has Been Changed");
				 location.reload();
				 }
			});
		}
		else {

		}
	 }

}

$(document).ready(function() {
	//jQuery.noConflict();
	 var dt =  $('#example').dataTable({
		"aaSorting": [[ 3, "desc" ]],
    "bLengthChange": true,
    "bFilter": false,
    "bSort": true,
    "bInfo": true,
    "bAutoWidth": true,

} );


$('select#engines').change( function() {
	//alert("hi");
dt.fnFilter( $(this).val(), 0 );
} );
$('select#engines1').change( function() {
dt.fnFilter( $(this).val(), 1 );
} );
$('select#engines2').change( function() {
dt.fnFilter( $(this).val(), 2 );
} );
$('select#engines3').change( function() {
dt.fnFilter( $(this).val(), 5 );
} );
$(".dataTables_filter input").addClass('form-control');
} );
</script>
<style>
.editable-table .dataTables_filter{
		width:40
}
</style>
