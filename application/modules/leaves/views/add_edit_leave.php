<?php
	$user = getAllUsers();
	$leave_status = getAllLeaveStatus();
	$leave_type = getAllLeaveType();
	$all_department = getAllDepartment();

	for ($i=0; $i<sizeof($permission); $i++) {
		$nm = $permission[$i]->module_name;
		$arr[$nm] = $permission[$i];
	}
?>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						<?php if(isset($leaveEdit[0]->leave_id)){ echo "Edit Leave";} else{echo "Add Leave";}?>
					</header>
					<?php
						$a = '';
						if (isset($leaveEdit[0]->leave_type) != '') {
							if($arr['leaves']->module_edit == 'yes') {
								$a = 'yes';
							}
						} elseif ($arr['leaves']->module_add == 'yes') {
							$a = 'yes';
						}
					?>
					<?php if ($a == 'yes') { ?>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
								<?php
									$form_attributes = array('name' => 'addLeave', 'id' => 'addLeave', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
									if (isset($leaveEdit[0]->leave_id)) {
										echo form_open('leaves/add/'.$leaveEdit[0]->leave_id,$form_attributes);
									} else {
										echo form_open(base_url().'leaves/add',$form_attributes);
									}
								?>
								<div class="form-group">
									<label for="firstName" class="control-label col-lg-2">User <span class="red">*</span></label>
									<div class="col-lg-6">
										<select name="user_id" id="user_id" disabled="disabled" class="form-control">
											<option value="">Select User</option>
											<?php foreach($user as $dd) { ?>
												<option value="<?php echo $dd->user_id; ?>" <?php $value1 = (isset($login_session[0]->user_id)) ? $login_session[0]->user_id : 0; echo selectedVal($value1,$dd->user_id); ?>>
													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
												</option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="form-group ">
									<label for="project_id" class="control-label col-lg-2">Request Type <span class="red">*</span></label>
									<div class="col-lg-6">
										<select name="leave_type" id="leave_type" class="form-control">
											<option value="">Select Request Type</option>
											<?php foreach($leave_type as $dd1) { ?>
												<option value="<?php echo $dd1->leave_type_id; ?>" <?php $value1 = (isset($leaveEdit[0]->leave_type)) ? $leaveEdit[0]->leave_type : 0; echo selectedVal($value1,$dd1->leave_type_id); ?>><?php echo $dd1->leave_type; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Start Date <span class="red">*</span></label>
									<div class="col-lg-2">
										<input type="text" class="form-control" readonly="readonly" id="leave_start" name="leave_start_date" data-date-format="yyyy-mm-dd" value="<?php if(isset($leaveEdit[0]->leave_start_date)){ echo $leaveEdit[0]->leave_start_date;} ?>">
									</div>

									<label  class="col-lg-2 control-label">End Date <span class="red">*</span></label>
									<div class="col-lg-2">
										<input type="text" class="form-control" readonly="readonly" id="leave_end" name="leave_end_date" data-date-format="yyyy-mm-dd" placeholder=" "  value="<?php if(isset($leaveEdit[0]->leave_end_date)){ echo $leaveEdit[0]->leave_end_date;} ?>">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Critical Task<span class="red display-star">*</span></label>
									<div class="col-lg-6">
										<input type="text" class="form-control" id="critical_task" name="leave_critical_task">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Responsibility<span class="red display-star">*</span></label>
									<div class="col-lg-6">
										<select type="text" class="form-control" id="res_assignee" name="leave_res_assignee">
											<option value="">Select User</option>
											<?php foreach($user as $dd) {
												if(isset($login_session[0]->user_id) && $login_session[0]->user_id != $dd->user_id){?>
												<option value="<?php echo $dd->user_id; ?>">
													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
												</option>
											<?php }} ?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label  class="col-lg-2 control-label">Reason <span class="red">*</span></label>
									<div class="col-lg-6">
										<textarea class="form-control" style="resize:none;" id="leave_reason" name="leave_reason"   rows="3" cols="40" placeholder="reason for leave"><?php if(isset($leaveEdit[0]->leave_reason)){ echo $leaveEdit[0]->leave_reason;} ?></textarea>
									</div>
								</div>
								<?php if($login_session[0]->role_id == '22') { ?>
									<div class="form-group ">
										<label for="project_id" class="control-label col-lg-2">Leave Status<span class="red">*</span></label>
										<div class="col-lg-10">
											<select name="leave_status" id="leave_status" class="form-control">
												<option value=""> Leave Status</option>
												<?php foreach($leave_status as $dd) { ?>
													<option value="<?php echo $dd->leave_status_id; ?>" <?php $value1 = (isset($leaveEdit[0]->leave_status)) ? $leaveEdit[0]->leave_status : 0; echo selectedVal($value1,$dd->leave_status_id); ?>><?php echo $dd->leave_status; ?></option>
												<?php }  ?>
											</select>
										</div>
									</div>


								<?php } ?>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-danger" type="submit" name="addEditLeave" id="addEditLeave">
											<?php if(isset($leaveEdit[0]->leave_id)){ echo "Update";} else{echo "Submit";}?>
										</button>
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
									</div>
								</div>
								<!-- Hidden field for json data -->
								<input type="hidden" name="leave_message" id="leave_message" value="" >
							</form>
						</div>
						<?php } else { ?>
							<span>Permission Denied</span>
						<?php } ?>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-leave.js"></script>

<script type="text/javascript">
	$('#leave_start,#leave_end').datepicker({
    startDate: today,
   	autoclose: true,
	});

</script>

<script type="text/javascript">

	$('#leave_type').change(function() {
		if($(this).val() == 14) {
			$('.display-star').hide();
		} else {
			$('.display-star').show();
		}
	});

	$(document).ready(function(){
		$('#addEditLeave').click(function(event){
			var start_date = $('#leave_start').val();
			var end_date = $('#leave_end').val();

			// Work on holiday only allowed in Sat and Sun
			var week_day_start = new Date(start_date);
			var week_day_end = new Date(end_date);
			var leave_type = $("#leave_type").val();

			var week_start = week_day_start.getDay();
			var week_end = week_day_end.getDay();

			//Date Comparision
			var date_one = new Date(start_date);
			var date_two = new Date(end_date);

			if(start_date ==""){
				alert('Please select start date!');
				return false;
			}
			if(start_date !=="" && end_date == ""){
				alert('Please select end date!');
				return false;
			}

			if(date_two >= date_one){

				//Check if work on holiday or not
				if(week_start == 0 || week_end == 0){
					if(leave_type != 18){
						alert('Please Select Request Type "Work on holiday"!');
						return false;
					}
				}

				event.preventDefault();
				var formData = $("#addLeave").serialize();
					$('#loaderAjax').show();
				$.ajax({
					type: 'POST',
					data: formData,
					url: '<?php echo base_url()?>leaves/checkLeaveMeeting',
					cache: false,
					async:false,
					processData: false,
					success:function(data){
						if(data != ""){
							var res = data.substr(0, data.indexOf(';'));
							var isGood=confirm('Are you sure? You have scheduled meeting on ' + res + '. Please manage your time accordingly.');
					    if (isGood) {
							 $('#leave_message').val(data);
					         $('#addLeave').submit();
					    }
					 }else{
							$('#addLeave').submit();
					 }
					 	$('#loaderAjax').hide();
					 return false;
					}
 				});
		}else {
			alert('End Date must be greater than Start Date');
			event.preventDefault();
		}

		});
	});
</script>
