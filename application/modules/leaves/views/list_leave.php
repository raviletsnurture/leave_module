<?php

//Jignasa
$perUser = getJuniorUsersList($user_id, 'leave_users');

$perDepartment = getJuniorUsersDepartmentList($user_id, 'leave_users');

$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();
$month_type = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');

//$get_diff = getDateDiff($id,$month);

for($i=0;$i<sizeof($permission);$i++)
{
	$nm = $permission[$i]->module_name;
	$arr[$nm] = $permission[$i];
}

if(isset($arr['leaves'])){
$role_id = $arr['leaves']->role_id;
}

?>
<style type="text/css">
.dataTables_filter {
     display: none;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height">
    <!-- My Leaves start-->
		<section class="panel">
		<?php if($arr['leaves']->module_add == 'yes') {
			if($role_id != '1'){?>
      <header class="panel-heading"> My Leaves</header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
					<div class="row">
					    <div class="col-lg-12">
							<div id="editable-sample_length" class="dataTables_length">
								<div class="btn-group">
									<a href="<?php echo base_url()?>leaves/add">
										<button class="btn btn-info" id="editable-sample_new"> Apply Request <i class="fa fa-plus"></i> </button>
									</a>
								</div>
							</div>
						</div>
		      </div>
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
				<th width="18%">Request Type</th>
				<th width="15%">Start Date</th>
				<th width="15%">End Date</th>
				<th width="5%">Leave Days</th>
				<th width="20%">Request Status</th>
				<th>Applied Date</th>
                <th><?php if($arr['leaves']->module_edit == 'yes' || $arr['leaves']->module_delete == 'yes') { ?>Action<?php } ?></th>
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($leave as $row){?>
              <tr>
				<td><?php echo $row->ltype; ?></td>
				<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_start_date))."</span>". date("d-m-Y", strtotime($row->leave_start_date)) ?></td>
				<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_end_date))."</span>". date("d-m-Y", strtotime($row->leave_end_date)) ?></td>
				<td><?php echo $row->leave_days; ?></td>
				<td><?php if($row->leave_status == '0') { echo "Pending"; } else { echo $row->lstatus; } ?><?php if($row->is_cancelled == '1') { echo " | Cancelled"; } ?></td>
				<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_created))."</span>". date("d-m-Y", strtotime($row->leave_created)) ?></td>
                <td>
					<button class="btn btn-info btn-xs tooltips" data-toggle="tooltip" data-original-title="<?php echo $row->leave_reason;?>" title=""><i class="fa fa-info"></i></button>
					<?php if($arr['leaves']->module_edit == 'yes') {
					if($row->leave_status == '1' && $row->is_cancelled == '') {
					?>
					<a href="<?php echo base_url()?>leaves/edit/<?php echo $row->leave_id;?>" >
					<button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Leave" title=""><i class="fa fa-pencil"></i></button>
					</a>
					<?php } } ?>
					<?php if($arr['leaves']->module_delete == 'yes') { ?>
					<a href="<?php echo base_url()?>leaves/delete/<?php echo $row->leave_id;?>" class="deleteRec">
					<button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Leave"><i class="fa fa-trash-o "></i></button>
					</a>
					<?php } ?>

					<?php if($row->is_cancelled != '1' && ($row->leave_status == 1 || $row->leave_status == 2) && strtotime($row->leave_start_date) > strtotime(date("Y-m-d"))) { ?>
					<a href="#" class="" onclick="cancel(<?php echo $row->leave_id; ?>)">
					<button class="btn btn-success btn-xs tooltips" data-toggle="tooltip" data-original-title="Cancel&nbsp;Request"><i class="fa fa-exclamation-triangle"></i></button>
					</a>
					<?php }?>
					<?php if($row->comment != "" ){ ?>
					<button class="btn btn-info btn-xs tooltips" data-toggle="tooltip" data-original-title="<?php echo $row->comment;?>" title=""><i class="fa fa-comment"></i></button>
					<?php } ?>
				</td>
              </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
			<?php } } ?>
			  <!-- My Leaves end-->

  	<?php
		if(!empty($perUser)){?>
		<header class="panel-heading">Department Leaves </header>
		<div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
				<div class="row">
						<div class="col-lg-12">
						<div id="editable-sample_length" class="dataTables_length">
							<div class="btn-group">
								<a href="<?php echo base_url()?>leaves/addemployleave">
									<button class="btn btn-info" id="editable-sample_new"> Apply Department Request <i class="fa fa-plus"></i> </button>
								</a>
							</div>
						</div>
					</div>
				</div>
		</div>
		<div class="panel-body">
			<?php if($arr['leaves']->module_add == 'yes') { ?>
					<select id="engines">
						<option value="">Select Users</option>
						<?php foreach($perUser as $row){?>
						<option value="<?php echo $row->first_name . ' ' . $row->last_name; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
						<?php } ?>
					</select>

			<select id="engines1">
						<option value="">Select Department</option>
						<?php foreach($perDepartment as $row1){?>
						<option value="<?php echo $row1->department_name; ?>"><?php echo $row1->department_name;?></option>
						<?php } ?>
					</select>
			<?php } ?>

			<select id="engines2">
						<option value="">Request Type</option>
						<?php foreach($leave_type as $bo){?>
						<option value="<?php echo $bo->leave_type; ?>"><?php echo $bo->leave_type; ?></option>
						<?php } ?>
					</select>

			<select id="engines3">
						<option value="">Request Status</option>
						<?php foreach($leave_status as $bow){?>
						<option value="<?php echo $bow->leave_status; ?>"><?php echo $bow->leave_status;?></option>
						<?php } ?>
			</select>
			<!-- <select id="engines4">
						<option value="">Select Month</option>
						<?php foreach($month_type as $key=>$months){?>
						<option value="0<?php echo $key; ?>"><?php echo $months;?></option>
						<?php } ?>
			</select> -->

			<input type="text" placeholder="Month" id="month_wise" class="date_picker" data-date-format="mm-yyyy" />
			<button class="btn btn-info btn-xs clearFilter tooltips" data-toggle="tooltip" data-original-title="Clear filter" title=""><i class="fa fa-refresh"></i></button>

			<div class="adv-table editable-table ">
			  <table class="table table-striped table-hover table-bordered" id="examplenew">
				<thead>
				  <tr>
					<th>Full Name</th>
					<th>Department</th>
					<th width="15%">Request Type</th>
					<th width="12%">Start Date</th>
					<th width="12%">End Date</th>
					<th width="13%">Request Status</th>
					<th>Applied Date</th>
          			<th>Change Status</th>
					<!--<th><?php if($arr['leaves']->module_edit == 'yes' || $arr['leaves']->module_delete == 'yes') { ?>Action<?php } ?></th>-->
				  </tr>
				</thead>
				<tbody>
					<?php foreach($allleave as $row){
						$quarterEL = checkEarnedLeaveCount($row->user_id);
						$heighLeave = checkHeighTotalDeptLeaveCount();
						$class = '';
						$msg = '';
						if($row->first_name != '' && $row->ltype != ''){
							if(date('m', strtotime('-1 month')) == date('m', strtotime($row->leave_start_date)) || $quarterEL > 3 || $heighLeave == $row->user_id){
								$class = 'lastMonthClass';
								if( date('m', strtotime('-1 month')) == date('m', strtotime($row->leave_start_date))){
									$msg = ' Already taken leave in last month';
								}
								if( $quarterEL > 3){
										$msg .= ' Already taken quarterly leave balance';
								}
								if( $heighLeave == $row->user_id){
									$msg .= ' Heighest Leave In Quarter';
								}
						}

					?>
				 <tr class="leaveID<?php echo $row->leave_id;?>">
					<td class="email <?php echo $class; ?>" title="<?php echo $row->email; ?>"><?php echo $row->first_name.' '.$row->last_name;?></td>
					<td class="<?php echo $class; ?>"><?php echo $row->department_name;?></td>
					<td class="request_type <?php echo $class; ?>" title="<?php echo $row->leave_reason; ?>"><?php echo $row->ltype; ?></td>
					<td class="<?php echo $class; ?>"><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_start_date)).'</span><span id="leave_start_date">'.date("d-m-Y", strtotime($row->leave_start_date)).'</span>'; ?></td>
					<td class="<?php echo $class; ?>"><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_end_date)).'</span><span id="leave_end_date">'. date("d-m-Y", strtotime($row->leave_end_date)).'</span>'; ?></td>
					<td class="<?php echo $class; ?>" ><?php echo $row->lstatus; ?><?php if($row->is_cancelled == '1') { echo " | Cancelled"; } ?></td>
					<td  <?php echo $class?'style="background-color:#F7A6B1 !important;"':''; ?> ><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_created))."</span>". date("d-m-Y", strtotime($row->leave_created)); ?></td>
					<td class="action <?php echo $class; ?>">
						<?php
							$checkRole = getRoleUser($row->leave_approved_by);
							//var_dump($user_id_logged); exit;
							if(isset($checkRole[0])){
							$allowedRole = $checkRole[0]->role_id;
							} else {
							$allowedRole = 0;
							}
						?>
						<?php if($arr['leaves']->module_approve == 'yes') { ?>
						<select id="leave_status" name="leave_status" onchange="ff(this)">
							<option value="">--Pending--</option>
							<?php foreach($leave_status as $bow){ if($bow->leave_status_id != 1 && $bow->leave_status_id !=4) { ?>
							<option value="<?php echo $bow->leave_status_id; ?>,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>" <?php if($bow->leave_status_id == $row->leave_status) { ?>selected="selected"<?php } ?>><?php echo $bow->leave_status;?></option>
							<?php }  } ?>
						</select>
						<?php }
						if(!empty($class)){ ?>
						<button class="btn btn-info btn-xs tooltips" data-toggle="tooltip" data-original-title="<?php echo $msg; ?>" title=""><i class="fa fa-info"></i></button>
						<?php } ?>
						<button class="btn btn-info btn-xs btncomment"
						 id="<?php echo $row->leave_id; ?>" data-toggle="modal" data-comment="<?php echo $row->comment; ?>" data-target="#myComment" >
							<i class="fa fa-comment"></i>
						</button>
					</td>
				  </tr>
					<?php } }?>
				</tbody>
			  </table>
			</div>
		</div>
    <?php } ?>

		<?php if($role_id != '1'){?>
		<section class="panel">
			<header class="panel-heading">Leaves Taken</header>
			<div class="panel-body">
				<div class="adv-table editable-table ">
					 <table class="table table-striped table-hover table-bordered" id="">
						<thead>
						  <tr>
							<th>Months</th>
							<th>Leaves Taken</th>
							<th>Total Balance (TBD)</th>
						  </tr>
						</thead>
						<tbody>
							<?php for ($m=1; $m<=12; $m++) {
							$year = date('Y');
							$vv = getDateDiff($user_id,$m,$year);
							//var_dump($vv);
							?>
							<tr>
								<td><?php echo $month_type[$m]; ?></td>
								<td><?php if($vv[0]['info'] >= 1) { echo $vv[0]['info']; } ?></td>
								<td><?php echo "0"; ?></td>
							</tr>
							<?php } ?>
						</tbody>
					 </table>
				</div>
			</div>
		</section>
		<?php } ?>

	</section>
    <!-- page end-->
  </section>
</section>

<!-- Comment Modal -->
<div id="myComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <!-- <div class="modal fade" id="myComment" role="dialog"> -->
	<input type="hidden" name="Leave_id" id="leave_id" class="getleave" value="">
	<!-- <span class="alert"></spanv> -->
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Comment</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<textarea name="leave_comment" id="leave_comment" class="form-control leave_comment" ></textarea>
				</div>
				<div class="alert-danger"></div>
			</div>
			<div class="modal-footer">
				<div class="alert alert-success alert-dismissable fade in">
					Comment added successfully!
				</div>
				<button type="button" class="btn btn-success commentadd" id="commentadd">Submit</button>
			</div>
		</div>
	</div>
</div>

<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<style>
.ui-datepicker-calendar{
    display: none;
}
</style>
<script type="text/javascript" charset="utf-8">

function ff(sel){
	var ff = sel.value.split(",");
	if(ff == ''){
		alert("Please Select an Option");
		location.reload();
		//return false;
	}else{
		var r = confirm("Are You Sure for this operation");
		var leave_start = $('#leave_start_date').text();
		var leave_end = $('#leave_end_date').text();
		if (r == true){
			$('#loaderAjax').show();
			$.ajax({
			 url: '<?php echo base_url(); ?>leaves/updatestaus/',
			 data: { leave_id: ff[1], status: ff[0], userid: ff[2],leave_start_date : leave_start,leave_end_date : leave_end},
			 dataType: 'html',
			 type: 'POST',
			 success: function(data){
				 $('#loaderAjax').hide();
				 $('.modal-body').html(data);
				 $('#myModal').modal('show');
				 alert("Leave Status Has Been Changed");
				 location.reload();
				 }
			});
		}else {
		}
	 }
}

function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel Leave!");
	if (r == true){
		$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>leaves/request/',
		 data: { leave_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('.modal-body').html(data);
			 $('#myModal').modal('show');
			 alert("Your Leave Has Been Cancelled");
			 	$('#loaderAjax').hide();
			 location.reload();
			 }
		});
		//location.reload();
	} else {
	}
}

$(document).ready(function() {

	 var dt =  $('#example').dataTable( {
		 "aaSorting": [[ 1, "desc" ]],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : false,

		 "oSearch": { "bSmart": false, "bRegex": true },
		 "aoColumnDefs": [
			{
			'bSortable': true, 'aTargets': [ 5 ]
			}
		 ]
		});

$('select#engines').change( function() {
		dt.fnFilter($(this).val(),0);
});
$('select#engines1').change( function() {
		dt.fnFilter($(this).val(),1);
});
$('select#engines2').change( function() {
		dt.fnFilter($(this).val(),2);
});
$('select#engines3').change( function() {
		dt.fnFilter($(this).val(),5);
});

$(".clearFilter").on("click",function(){
	dt.fnFilter("",0);
	dt.fnFilter("",1);
	dt.fnFilter("",2);
	dt.fnFilter("",3);
	dt.fnFilter("",5);
	$('#month_wise').val("");
	$("#engines").val($("#target option:first").val());
	$("#engines1").val($("#target option:first").val());
	$("#engines2").val($("#target option:first").val());
	$("#engines3").val($("#target option:first").val());
});

var year = (new Date).getFullYear();
$('#month_wise').datepicker({
	autoClose:true,
	changeMonth: true,
	changeYear: true,
	showButtonPanel: true,
	dateFormat: 'mm-yy',
	yearRange: '2011:' + year + '',
	onClose: function(dateText, inst) {
		$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
		//dt.columns(3).search(inst.selectedMonth+1+'-'+inst.selectedYear).draw();
		dt.fnFilter(inst.selectedMonth+1+'-'+inst.selectedYear,3);
	}
});

$(".dataTables_filter input").addClass('form-control');
var dt =  $('#examplenew').DataTable( {
						"aLengthMenu": [[25, 50, 100], [25, 50, 100]],
						"aaSorting": [[ 6, "desc" ]],
						"iDisplayLength": 10,
						"pagingType": "full_numbers",
						"dom": 'Cfrtip',
						"destroy": true,
						//"bFilter": false,
						"bPaginate": true,
						"bInfo" : true,
						"aoColumnDefs": [
									 //{ 'bSortable': false, 'aTargets': [ 3,4,6,7] }
						 ],
						"initComplete": function(settings, json) {
								$("table thead input").on( 'keyup', function () {
										dt.column( $(this).parent().index()+':visible' ).search( this.value ).draw();
								 });
						}
 });
 $('#engines3').val("Pending").change();
});//document.ready

$(document).ready(function() {
		//Add comment on leave request
	$("body").on("click",".btncomment",function(){
		var comment = $(this).attr("data-comment");
		var leave_id = $(this).attr("id");
		$(".leave_comment").val('');
		$(".leave_comment").val(comment);
		$("#leave_id").val(leave_id);
		$(".alert-success").css('display','none');
		$(".commentadd").removeClass('disabled');
		$(".commentadd").html("Submit");
		$('.alert-danger').css('display','none')
	});
	//Add comment on leave request
	$('.commentadd').click(function(){
		var leavdeid = $('#leave_id').val();
		var comment = $('#leave_comment').val();
		var leave_start_date = $(".leaveID"+leavdeid+" #leave_start_date").html();
		var leave_end_date  = $(".leaveID"+leavdeid+" #leave_end_date ").html();
		var request_type = $(".request_type").html();

		if(comment == ""){
			$(".alert-danger").css({'display':'inline-block','padding':'10px','width':'100%'});
			$(".alert-danger").html('Comment required');
		}else{
			$(".leave_comment").css('border', '1px solid #e5e5e5');
			$(".alert-danger").css('display','none');
			$(".commentadd").html("<i class='fa fa-circle-o-notch fa-spin'></i> Loading");
			$(".commentadd").addClass('disabled');
			var dataString = 'Leave_ID='+ leavdeid + '&comment_Add='+ comment + '&Leave_Start_Date='+ leave_start_date + '&Leave_End_Date='+ leave_end_date + '&Request_Type='+ request_type;

			$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>leaves/commnetAdd",
			data: dataString,
			cache: false,
			success: function(result){
				if(result == 1){
					$(".alert-success").css({"display": "inline-block", "float": "left", "margin-bottom": "0px"});
					$(".action #"+leavdeid).attr('data-comment',comment);
					setTimeout(function() {$('#myComment').modal('hide');}, 1000);
				}else{
				}

			}
			});
		}
	});
});

</script>
