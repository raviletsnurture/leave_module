<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
		Author : Deeshit Parikh
		Date   : 18th Feb 2015
		Action : Home
	 */
class Home extends MX_Controller {

    function __construct()
    {
        parent::__construct();
        $this->template->set('controller', $this);
		user_auth(1);
		$this->load->model('home_model');
		$this->load->helper('cookie');
    $this->load->library('email');
    $this->load->library('user_agent');
    }


	function index(){
		$data['username'] = trim($this->input->post('username'));
		$data['password'] = trim($this->input->post('password'));
		if($this->input->post("submitLogin") !== false || ($data['username'] !='' && $data['password'] != ''))
		{
			$data['username'] = $this->input->post('username');
			$data['password'] = $this->input->post('password');
			$data1 = $this->home_model->login($data);
			if($this->input->post('remember'))
			{
				$expire = '999999999';
				$usernameCookie = array(
					'name'   => 'username_cookie',
					'value'  => $data['userId'],
					'expire' => $expire,

				);
				$passwordCookie = array(
					'name'   => 'password_cookie',
					'value'  => base64_encode($data['password']),
					'expire' => $expire,

				);
				$this->input->set_cookie($usernameCookie);
				$this->input->set_cookie($passwordCookie);

			}
			else
			{
				delete_cookie("username_cookie");
				delete_cookie("password_cookie");
			}
			if(count($data1)>0)
			{

				if($data1[0]->status == 'Active')
				{
					$data2 = $this->home_model->userPermission($data1[0]->role_id);
					$arr = array_merge($data1,$data2);
					$this->session->set_userdata(array('login_session' => $data1));
					$this->session->set_userdata(array('permission' => $data2));
					$login_session = $this->session->userdata('login_session');
					$permission = $this->session->userdata('permission');
					redirect(base_url().'dashboard');
					exit;
				}else{
				  $this->session->set_flashdata('error',"Your account is inactivate");
					redirect(base_url());
					exit;
				}
			}else {
          $loginAttampts = $this->session->userdata('login_attempts');
          $loginAttemptSession = $loginAttampts + 1;
          $this->session->set_userdata(array('login_attempts' => $loginAttemptSession));
          if ($this->session->userdata('login_attempts') > $this->config->item('login_attampt')) {
              $browser = $this->agent->browser();
              $ipAddress = $this->input->ip_address();
              $os = $this->agent->platform();
              $mailData['body'] = '<tr><td colspan="2" style="font-size:14px; color:#FFF;
                             padding:0 40px 20px 40px;"> User had tried to login with more then 5 failed login attempts</td>
                         </tr>
                                      <tr>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                <p><strong style="color:#03A9F4;">IP: </strong></p>
              </td>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                <p>'.$ipAddress.'</p>
              </td>
            </tr>
            <tr>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                <p><strong style="color:#03A9F4;">Browser: </strong></p>
              </td>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                <p>'.$browser.'</p>
              </td>
            </tr>
            <tr>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
                <p><strong style="color:#03A9F4;">Operating system: </strong></p>
              </td>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                <p>'.$os.'</p>
              </td>
            </tr>
            <tr>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
                <p><strong style="color:#03A9F4;">Username: </strong></p>
              </td>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                <p>'.$data['username'].'</p>
              </td>
              </tr>
              <tr>
                                  <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
                <p><strong style="color:#03A9F4;">Password: </strong></p>
              </td>
              <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                <p>'.$data['password'].'</p>
              </td>
                                  </tr>
                                  <tr>
                                  <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
                <p><strong style="color:#03A9F4;">Date and Time: </strong></p>
              </td>
                                  <td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
                <p>'. date('d-M-Y H:i:s').'</p>
              </td>
            </tr>';
              $mailData['name'] = 'HR';
              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to("hrd@letsnurture.com");
              $this->email->subject('Failure Login attempt');
              $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
              $this->email->message($body);
              //$this->email->send();
          }
				$this->session->set_flashdata('error',"Invalid Username or Password");
				redirect(base_url());
				exit;
			}
		}
		$data['username_cookie'] = $this->input->cookie('username_cookie', TRUE) ? $this->input->cookie('username_cookie', TRUE) : '';
		$data['password_cookie'] = $this->input->cookie('password_cookie', TRUE) ? $this->input->cookie('password_cookie', TRUE) : '';;
    $this->template->load_partial('login_master','home',$data);
	}

	public function checkUsername()
	{
		$data['username'] = $this->input->post('username');

		$checkUserId = $this->home_model->verify_username($data);
		if($checkUserId > 0){
			echo "true";
		}else{
			echo 'false';
		}
	}
	public function checkUsername_forgot()
	{
		$data['username'] = $_POST['username1'];

		$checkUserId = $this->home_model->verify_username($data);
		if($checkUserId > 0){
			echo "true";
		}else{
			echo 'false';
		}

	}

	public function forgotpassword()
	{
		if($this->input->post('username1') && $this->input->post('username1') != '')
		{
			$data['username'] = $this->input->post('username1');
			$data1 = $this->home_model->getDistEmail($data);

			if(count($data1) > 0)
			{
				$toEmail = $data1[0]->email;
				if($toEmail != ''){
					$siteNameForMail = $this->config->item('siteNameForMail');

					$emailHtml['confirm_url'] = base_url().'home/resetpassword/'.md5($data['username'].$this->config->item('siteNameForMail'));
					$emailHtml['siteNameForMail'] = $siteNameForMail;

					$finalEmailHtml = $this->load->view("shop/login/forgot_tmp",$emailHtml, true);

					$subject="Forgot password - ".$siteNameForMail;
					if(send_mail($toEmail, $subject, $finalEmailHtml)){
						echo "true";exit;
					}
				}else{
					echo "false";exit;
				}
			}
			else{
				echo "false";
				exit;
			}
		}
		$this->template->load_partial('master','forgotpassword');
	}

	public function resetpassword($hashCode){
		$username = $this->input->post('username');
		$password = $this->input->post('password2');
		$repassword = $this->input->post('repassword2');
		$siteNameForMail = $this->config->item('siteNameForMail');

		if($this->input->post('resetSubmit'))
		{
			//$data['password']= md5($password);
			//$vdata['repassword'] = md5($repassword);

			$data['password']= $password;
			$vdata['repassword'] = $repassword;

			if($data['password'] != $vdata['repassword']){
				$this->session->set_flashdata('error',"Password and Confirm Password are not matched !");
				redirect(base_url().'home/resetpassword');
				exit;
			}
			if(md5($username.$siteNameForMail) == $hashCode && $password == $repassword)
			{
				//$this->home_model->updatepassword($data,md5($userId.$siteNameForMail));
				$this->home_model->updatepassword($data,md5($username.$siteNameForMail));
				$this->session->set_flashdata('success',"Password updated successfully!");
				redirect(base_url());
				exit;
			}else{
				$this->session->set_flashdata('error',"Error while update password, Click again on given link in email");
				redirect(base_url().'home/resetpassword'.$hashCode);
				exit;
			}
		}
		$dataView['hash'] = $hashCode;
		$this->template->load_partial('login_master','home/resetpassword',$dataView);
	}
} ?>
