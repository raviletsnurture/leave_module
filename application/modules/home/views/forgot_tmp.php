<!DOCTYPE html>
<html lang="en">


<title>Forgot Password</title>


</head><body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" style="background:#F1F2F7">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td height="30"></td>
    </tr>
    <tr bgcolor="#F1F2F7">
      <td align="center" bgcolor="#F1F2F7" valign="top" width="100%"><!--  top header -->
        
        <table class="container" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr bgcolor="7087A3">
              <td height="15"></td>
            </tr>
            <tr bgcolor="7087A3">
              <td align="center"><table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                  <tbody>
                    <tr>
                      <td><table class="top-header-left" align="left" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td align="center"><table class="date" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                      <td></td>
                                      <td>&nbsp;&nbsp;</td>
                                      <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"><singleline>
                                          <h1><?php echo $this->config->item('siteNameForMail');?></h1>
                                        </singleline></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr bgcolor="7087A3">
              <td height="10"></td>
            </tr>
          </tbody>
        </table>
        <table class="container" align="center"  border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="ffffff">
          <tbody>
            <tr>
              <td><table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                  <tr >
                    <td height="7"></td>
                  </tr>
                  <tr >
                    <td height="20"></td>
                  </tr>
                  <tr >
                    <td><table cellspacing="0" cellpadding="0" width="560" border="0" bgcolor="F1F2F7" align="center">
                        <tbody>
                          <tr>
                            <td><table cellspacing="0" cellpadding="0" width="528" border="0" align="center" class="mainContent1">
                                <tbody>
                                  <tr>
                                    <td height="20"></td>
                                  </tr>
                                  <tr>
                                    <td><table cellspacing="0" cellpadding="0" width="" border="0" align="left">
                                        <tbody>
                                          
                                          <tr>
                                            <td height="15"></td>
                                          </tr>
                                          <tr>
                                            <td style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">Please reset your password by following this link<br>
                                              <br></td>
                                          </tr>                                                                                                                                
                                          <tr>
                                            <td height="15"></td>
                                          </tr>
                                          <tr>
                                            <td><a style="background-color: #7087A3; font-size: 12px; padding: 10px 15px; color: #fff; text-decoration: none;font-family: Helvetica, Arial, sans-serif;" href="<?php echo $confirm_url;?>"> Click here</a></td>
                                          </tr>
                                          
                                          
                                          
                                        </tbody>
                                      </table></td>
                                  </tr>
                                  
                                  <tr>
                                    <td height="20"></td>
                                  </tr>
                                  <tr>
                                  <td style="color: #a4a4a4; font-size: 12px; font-weight: normal; line-height:20px; font-family: Helvetica, Arial, sans-serif;">Regards,</td>
                                  </tr>
                                  <tr>
                                    <td height="20"></td>
                                  </tr>
                                </tbody>
                              </table></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                  <tr >
                    <td height="25"></td>
                  </tr>
                </table></td>
            </tr>
          </tbody>
        </table>
        <table class="container" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr bgcolor="7087A3">
              <td height="15"></td>
            </tr>
            <tr bgcolor="7087A3">
              <td  style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" align="center"> <?php echo $this->config->item('siteNameForMail')?> &copy; Copyright <?php echo date('Y');?> . All Rights Reserved </td>
            </tr>
            <tr>
              <td bgcolor="7087A3" height="14"></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td height="30"></td>
    </tr>
  </tbody>
</table>
</body>
</html>