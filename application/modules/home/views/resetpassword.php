<form class="cmxform form-horizontal tasi-form form-signin commonForm " method="post" action="<?php echo base_url();?>home/resetpassword/<?php echo $hash;?>" id="signupForm">
  <h2 class="form-signin-heading">Change Password</h2>
  <?php if($this->session->flashdata('error')){?>
  <div class="alert alert-block alert-danger fade in">
    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
  <?php } ?>
  <?php if($this->session->flashdata('success')){?>
  <div class="alert alert-success fade in">
    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
  <?php }?>
  <div class="login-wrap">
    <div class="form-group ">
      <div class="col-lg-12">
        <input type="text" class="form-control" placeholder="Username" name="username" id="username" autofocus>
      </div>
    </div>
    <div class="form-group ">
      <div class="col-lg-12">
        <input type="password" class="form-control" placeholder="New Password" name="password2" id="password2">
      </div>
    </div>
    <div class="form-group ">
      <div class="col-lg-12">
        <input type="password" class="form-control" placeholder="Re-type New Password" name="repassword2" id="repassword2">
      </div>
    </div>
    <input  class="btn btn-lg btn-login btn-block" type="submit" name="resetSubmit" value="Submit">
    <div class="registration"> Click here for <a class="" href="<?php echo base_url();?>"> Login </a> </div>
  </div>
</form>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script> 
<script src="<?php echo base_url()?>assets/js/validate/form-validation-reset.js" ></script> 
