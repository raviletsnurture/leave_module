<!-- Modal -->
<form name="forgotForm" class="cmxform form-horizontal tasi-form form-signin" method="post" id="forgotForm">
  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Forgot Password ?</h4>
        </div>
        
		<div class="alert alert-block alert-danger fade in forgot-error " style="display:none">
          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
          <strong>Oh snap!</strong> <span class="error_snap"></span> </div>
        <div class="alert alert-success fade in forgot-success " style="display:none">
          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
          <strong>Success!</strong> <span class="success_snap"></span> </div>
        <div class="modal-body">
          <p>Enter your username below to reset your password.</p>
          <div class="form-group ">
            <div class="col-lg-12">
              <input type="text" placeholder="username" autocomplete="off" name="username1" id="username1" class="form-control placeholder-no-fix">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
          <input value="Submit" class="btn btn-success" type="submit">
        </div>
      </div>
    </div>
  </div>
</form>
<!-- modal --> 
<form class="cmxform form-horizontal tasi-form form-signin" action="" method="post" id="loginForm" name="loginForm">
  
  <h2 class="form-signin-heading">sign in now</h2>
  
  <?php if($this->session->flashdata('error')){?>
  <div class="alert alert-block alert-danger fade in">
    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
  <?php } ?>
  <?php if($this->session->flashdata('success')){?>
  <div class="alert alert-success fade in">
    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
  <?php }?>
  
  <div class="login-wrap">
    <div class="form-group ">
      <div class="col-lg-12">
        <input type="text" class="form-control" placeholder="Username" name="username" id="username" autofocus value="<?php echo $username_cookie;?>">
      </div>
    </div>
    <div class="form-group ">
      <div class="col-lg-12">
        <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="<?php echo base64_decode($password_cookie);?>">
        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password">
      </div>
    </div>
    
	<label class="checkbox">
      <!--<input type="checkbox" name="remember" id="remember" <?php //if($username_cookie && base64_decode($password_cookie))echo "checked";?>>
      Remember me --> <!--<span class="pull-right"> <a data-toggle="modal" href="#myModal">Forgot Password?</a> </span>--> </label>
    
	<input name="submitLogin" class="btn btn-lg btn-login btn-block" type="submit" value="Sign in">
    
	<!--<div class="registration"> Don't have an account yet? <a class="" href="registration"> <button type="button" class="btn btn-warning btn-block">Sign up for free!</button> </a> </div>-->
  </div>
</form>


<script src="<?php echo base_url()?>assets/js/validate/form-validation-login.js" ></script> 

<script type="text/javascript">
$(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
      
    }
});
</script>
