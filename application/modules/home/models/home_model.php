<?php
class Home_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	public function login($data)
	{
		$this->db->select('user_id,department_id,role_id,status,first_name,last_name,sub_role_id');
		$this->db->from('users');
		$this->db->where('username',$data['username']);
		//$this->db->where('u.password',md5($data['password']));
		$this->db->where('password',$data['password']);
		$this->db->limit(1);
		$query=$this->db->get();
		return $query->result();
	}

	public function userPermission($id)
	{
		$this->db->select('p.*,r.*');
		$this->db->from('role as r');
		$this->db->join('role_permission as p','p.role_id = r.role_id','left');
		$this->db->where('r.role_id',$id);
		$query=$this->db->get();
		return $query->result();
	}

	public function getDistEmail($data)
	{
		$this->db->select('email');
		$this->db->from('users');
		$this->db->where('username',$data['username']);
		$query = $this->db->get();
		return $query->result();
	}

	function verify_username($data)
	{
		$this->db->select("username");
		$this->db->from("users");
		$this->db->where("username",$data['username']);
		return $this->db->get()->num_rows();
	}
	public function updatepassword($dataUpdate,$username)
	{
		$msg=$this->db->where("md5(concat(username,'".$this->config->item('siteNameForMail')."'))",$username);
		$this->db->update('users',$dataUpdate);
	//	$this->db->where('md5(userId)',$data1['userId']);
		//$this->db->update('user',$data);
	}


}
?>
