<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout extends MX_Controller {  
    function __construct()
    {
        parent::__construct();		
        $this->template->set('controller', $this);
		
    }
	
	public function index()
	{			
		$this->session->unset_userdata("login_session");
		$this->session->unset_userdata("ref_login_session");
		redirect(base_url());
		exit;		
	}
	public function referrer()
	{			
		$this->session->unset_userdata("ref_login_session");
		redirect(base_url().'referrer/login');
		exit;		
	}
	function superadmin(){
		$this->session->unset_userdata("sLogin_session");
		redirect(base_url().'superadmin');
		exit;		

	}
	function shopuser(){
		$this->session->unset_userdata("user_login_session");
		redirect(base_url().'shop');
		exit;		

	}
}
?>