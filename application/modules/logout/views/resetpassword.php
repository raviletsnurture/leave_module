<div class="container">
  <div class="panel proper-form-bg">
    <?php 			  

	  $form_attributes = array('name' => 'user_reset_password', 'id' => 'user_reset_password', 'autocomplete' => 'off','class'=>'proper-login' );

	  echo form_open('login/reset_password/'.md5($user_data[0]->vEmail),$form_attributes);

	  ?>
    <?php if($this->session->flashdata('error')){?>
    <div class="alert failed">
      <button data-dismiss="alert" class="close" type="button">x</button>
      <strong>Failed!</strong> <?php echo $this->session->flashdata('error'); ?> </div>
    <?php } ?>
    <?php if($this->session->flashdata('success')){?>
    <div class="alert success">
      <button data-dismiss="alert" class="close" type="button">x</button>
      <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
    <?php }?>
    <h3>Reset password</h3>
    <div class="member-login">
      <div class="clearfix"></div>
      <hr/>
    </div>
    <label>Email<span style="color:#FF0000;"> *</span></label>
    
    <!--<input type="text" class="input-block-level"/>--> 
    
    <span><?php echo $user_data[0]->vEmail;?></span>
    <label>Password<span style="color:#FF0000;"> *</span></label>
    <input type="password" value="" class="input-block-level" name="vPassword" id="vPassword">
    <label>Confirm Password<span style="color:#FF0000;"> *</span></label>
    <input type="password" value="" class="input-block-level" name="cPassword" id="cPassword">
    <hr/>
    <div class="clearfix"> 
      
      <!--<input type="submit" value="Login" class="btn btn-proper"/>-->
      
      <input type="submit" class="btn btn-proper" value="Submit" name="submit">
      &nbsp;&nbsp; <a href="<?php echo base_url();?>register">Create an account</a> </div>
    <?php echo form_close();?> </div>
</div>


<script type="text/javascript">

$(document).ready(function() {
$("#vPassword").focus();
	$("#user_reset_password").validate({

		rules: {

				vEmail: {

						required: true,

						email:true 

						},			

				vPassword: {

						required: true, 

						minlength: 6

						},

				cPassword: {						

						required: true,

						equalTo :"#vPassword"

						},		

								

				},

		messages: {				

					

					vEmail:{							

							required: "Email is required",

							email: "Please enter valid email address"

						},

					vPassword:{							

							required: "Password is required",

							minlength : "Password must be at least 6 character"

						},

					cPassword:{							

							required: "Confirm Password is required",

							equalTo: "Confirm Password must be same as above"

						},

					

			}

	});

});

</script>