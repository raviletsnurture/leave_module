
<div class="container">
  <div class="panel proper-form-bg">
    <?php 

      $form_attributes = array('name' => 'user_login', 'id' => 'user_login', 'autocomplete' => 'off','class'=>'proper-login' );

      echo form_open('login',$form_attributes);

      ?>
    <?php if($this->session->flashdata('error')){?>
    <div class="alert failed">
      <button data-dismiss="alert" class="close" type="button">x</button>
      <strong>Failed!</strong> <?php echo $this->session->flashdata('error'); ?> </div>
    <?php } ?>
    <?php if($this->session->flashdata('success')){?>
    <div class="alert success">
      <button data-dismiss="alert" class="close" type="button">x</button>
      <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
    <?php }?>
    <h3>Login</h3>
    <div class="member-login">
      <div class="clearfix"></div>
      <hr/>
    </div>
    <label>Email<span style="color:#FF0000;"> *</span></label>
    <input type="text"  value="" class="input-block-level"  name="vEmail" id="vEmail">
    <label>Password<span style="color:#FF0000;"> *</span></label>
    <input type="password" value="" class="input-block-level" name="vPassword" id="vPassword">
    <hr/>
    <div class="clearfix"> 
      
      <!--<input type="submit" value="Login" class="btn btn-proper"/>-->
      
      <input type="submit" class="btn btn-proper" value="Login" name="submit">
      &nbsp;&nbsp; <a href="<?php echo base_url()?>login/forgot_password">Forgot Password?</a>     
      </div>
      <br />

    <hr>
    <h4>Don't have an account?</h4>
    <a href="<?php echo base_url()?>register">Go to Sign up <i class="icon-arrow-right"></i></a>

    <?php echo form_close();?> </div>
    
</div>
<script type="text/javascript">

$(document).ready(function() {
	$("#vEmail").focus();
	$("#user_login").validate({

		rules: {

				vEmail: {

						required: true,

						email:true 

						},			

				vPassword: {

						required: true, 

						

						},

								

				},

		messages: {				

					

					vEmail:{							

							required: "Email is required",

							email: "Please enter valid email address"

						},

					vPassword:{							

							required: "Password is required",

							

						},

					

			}

	});

});

</script>