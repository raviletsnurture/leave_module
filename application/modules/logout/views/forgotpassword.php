<div class="container">
  <div class="panel proper-form-bg">
    <?php 

		$form_attributes = array('name' => 'user_forgot', 'id' => 'user_forgot', 'autocomplete' => 'off' ,'class'=>'proper-login');

		echo form_open('login/forgot_password',$form_attributes);

	  ?>
    <?php if($this->session->flashdata('error')){?>
    <div class="alert failed">
      <button data-dismiss="alert" class="close" type="button">x</button>
      <strong>Failed!</strong> <?php echo $this->session->flashdata('error'); ?> </div>
    <?php } ?>
    <?php if($this->session->flashdata('success')){?>
    <div class="alert success">
      <button data-dismiss="alert" class="close" type="button">x</button>
      <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
    <?php }?>
    <h3>Forgot password</h3>
    <div class="member-login">
      <div class="clearfix"></div>
      <hr/>
    </div>
    <label>Email Id<span style="color:#FF0000;"> *</span></label>
    
    <!--<input type="text" class="input-block-level"/>-->
    
    <input type="text"  value="" class="input-block-level"  name="vEmail" id="vEmail">
    <hr/>
    <div class="clearfix"> 
      
      <!--<input type="submit" value="Login" class="btn btn-proper"/>-->
      
      <input type="submit" class="btn btn-proper" value="Submit" name="submit">
      &nbsp;&nbsp; <a href="<?php echo base_url();?>register">Create an account</a> </div>
    <?php echo form_close();?> </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
$("#vEmail").focus();
	$("#user_forgot").validate({

		rules: {

			vEmail: {

					required: true,

					email: true

					}

		},

		

		messages: {				

			vEmail:{

				required: "Email is required",

				email: "Please enter valid email"

				}			

		}

	});

});

</script>