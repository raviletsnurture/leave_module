<?php
class Leave_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllLeave()
	{
		$this->db->select('u1.first_name as af,u1.last_name as al,u.user_id,u.email,d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('users as u1','u1.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("u.first_name is not NULL");
		/*$this->db->where("MONTH(l.leave_start_date) = MONTH(NOW())"); 
		$this->db->where("YEAR(l.leave_start_date) = YEAR(NOW())"); */
		$this->db->where("l.leave_start_date >= DATE_SUB(NOW(),INTERVAL 1 YEAR)");
		$this->db->order_by("leave_id","desc");
		$query=$this->db->get();
		return $query->result();
	}


	function getAllFilterLeave($from_date,$to_date)
	{
		$from = date('Y-m-d',strtotime($from_date));
		$to = date('Y-m-d',strtotime($to_date)); 
		$this->db->select('u1.first_name as af,u1.last_name as al,u.user_id,u.email,d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('users as u1','u1.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("u.first_name is not NULL");
		$this->db->where("l.leave_start_date >= '$from'");
		$this->db->where("l.leave_end_date <= '$to'");
		$this->db->order_by("l.leave_start_date","desc");
		$query=$this->db->get();
		return $query->result();
	}

	function leaveListByMonth($month)
	{
		$this->db->select('u1.first_name as af,u1.last_name as al,u.user_id,d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('users as u1','u1.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("u.first_name is not NULL");
		$this->db->where("MONTH(l.leave_start_date) = MONTH('$month')");
		$this->db->where("YEAR(l.leave_start_date) = YEAR('$month')");
		$this->db->order_by("leave_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('leaves',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getLeave($catId){
		$this->db->select('*');
		$this->db->from('leaves');
		$this->db->where('leave_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateLeave($data){
		$this->db->where('leave_id',$data['leave_id']);
		if($this->db->update('leaves',$data)){
			return true;
		}else{
			return false;
		}
	}

	function updateleavecount($data)
	{
		$this->db->set('total_leaves', 'total_leaves+1', FALSE);
		$this->db->where('user_id',$data['user_id']);
		$this->db->update('users');
	}


	function deleteLeave($id)
	{
		$this->db->where('leave_id',$id);
	    $this->db->delete('leaves');
	}
}

?>
