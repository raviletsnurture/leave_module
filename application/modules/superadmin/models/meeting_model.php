<?php
class meeting_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getMeetingCalendar()
	{
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		//$this->db->where("m.user_id",$id);
		$this->db->where("m.user_id = u.user_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}

	function addMeeting($data){
		if($this->db->insert('crm_meeting',$data)){
			return true;
		}else{
			return false;
		}
	}

	function checkMeetingTime($schedule_date,$schedule_time){
		$this->db->select('*');
		$this->db->from('crm_meeting');
		$this->db->where('schedule_date',$schedule_date);
		$this->db->where('schedule_time',$schedule_time);
		$query=$this->db->get();
		return $query->row();
	}

	function getMyMeetingsCalendar($id)
	{
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.user_id",$id);
		$this->db->where("m.user_id = u.user_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getHrMeetingsCalendar()
	{
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.hr_role_id",'22');
		$this->db->where("m.user_id = u.user_id");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getMyMeetings($id,$date)
	{	$startDate = $date.'-01';
		$endDate = $date.'-31';
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.user_id",$id);
		$this->db->where("m.user_id = u.user_id");
		$this->db->where("schedule_date >= '$startDate'");
		$this->db->where("schedule_date <= '$endDate'");
		$this->db->order_by("schedule_time","ASC");
		$milestone = $this->db->get()->result_array();
		return $milestone;
	}

	function getHrMeetings($date)
	{	$startDate = $date.'-01';
		$endDate = $date.'-31';
		$this->db->select(" m.*,u.first_name,u.last_name");
		$this->db->from("crm_meeting m, crm_users u");
		$this->db->where("m.hr_role_id",'22');
		$this->db->where("m.user_id = u.user_id");
		$this->db->where("schedule_date >= '$startDate'");
		$this->db->where("schedule_date <= '$endDate'");
		$this->db->order_by("schedule_time","ASC");
		$milestone = $this->db->get()->result_array();
		// $tr = $this->db->last_query();
		// print_r($tr);
		return $milestone;

	}

	function updateMeetingStatus($data){
		$this->db->where('meeting_id',$data['meeting_id']);
		if($this->db->update('crm_meeting',$data)){
			return true;
		}else{
			return false;
		}
	}

}
?>
