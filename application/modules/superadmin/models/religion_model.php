<?php
class Religion_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	
	function getAllReligion()
	{
		$this->db->select('*');
		$this->db->from('religion');
		$this->db->order_by("religion_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('religion',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getReligion($catId){
		$this->db->select('*');
		$this->db->from('religion');
		$this->db->where('religion_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateReligion($data){
		
		$this->db->where('religion_id',$data['religion_id']);
		if($this->db->update('religion',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function deleteReligion($id)
	{
		$this->db->where('religion_id',$id);
	    $this->db->delete('religion');
	}
}

?>