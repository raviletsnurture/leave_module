<?php
class Notification_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function addAnnouncement($data)
	{
		if($this->db->insert('announcement', $data)){
			return true;
		}else{
			return false;
		}
	}

	function getAllDeviceToken(){
		$this->db->select("deviceToken");
		$this->db->from("device");
		$query = $this->db->get()->result();
		return $query;
	}

	function getAllDeviceTokenbyType($type){
		$this->db->select("deviceToken");
		$this->db->from("device");
		$this->db->where("deviceType", $type);
		$query = $this->db->get()->result();
		return $query;
	}

	function getAllAnnouncements()
	{
		$sql = "SELECT *
				FROM crm_announcement
				ORDER BY created_time DESC";
		$query = $this->db->query($sql);
		return $results = $query->result();
	}

	function deleteAnnouncement($annId)
	{
		$this->db->where('announcement_id', $annId);
	    $this->db->delete('announcement');
	}
}
?>
