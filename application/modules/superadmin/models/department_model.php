<?php
class Department_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	
	function getAllDepartment()
	{
		$this->db->select('*');
		$this->db->from('department');
		$this->db->order_by("department_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('department',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getDepartment($catId){
		$this->db->select('*');
		$this->db->from('department');
		$this->db->where('department_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateDepartment($data){
		
		$this->db->where('department_id',$data['department_id']);
		if($this->db->update('department',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function deleteDepartment($id)
	{
		$this->db->where('department_id',$id);
	    $this->db->delete('department');
	}
}

?>