<?php
class Campus_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	function getAllCampus()
	{
		$this->db->select('*');
		$this->db->from('campus as c');		
		//$this->db->where("c.status = 'Active'");
		$this->db->order_by("c.campus_id","desc");
		$query=$this->db->get();		
		return $query->result();
		
	}
	function getState_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('state');
		$this->db->where('country_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		$dd = "<option value=''>Select State</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->state_id.">".$row->state_name."</option>";
        }
		return $dd;
	}
	function getCity_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('city');
		$this->db->where('state_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		//$dd = "<option value=''>Select City</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->city_id.">".$row->city_name."</option>";
        }
		return $dd;
	}
	function getCampus($catId){
		$this->db->select('*');
		$this->db->from('campus');
		$this->db->where('campus_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}

	function add($data){
	    if($this->db->insert('campus',$data)){
			return true;
		}else{
			return false;
		}
	}

	function updateCampus($data){
		$this->db->where('campus_id',$data['campus_id']);
		if($this->db->update('campus',$data)){
			return true;
		}else{
			return false;
		}
	}
	function geteditstate($id)
	{
		$this->db->select('s.*');
		$this->db->from('state as s');
		$this->db->join('campus as c','s.country_id = c.country_id', 'left');
		$this->db->where('c.campus_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function geteditcity($id)
	{
		$this->db->select('cc.*');
		$this->db->from('city as cc');
		$this->db->join('campus as c','c.state_id = cc.state_id', 'left');
		$this->db->where('c.campus_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function deleteCampus($id)
	{
		$this->db->delete('campus', array('campus_id' => $id)); 
	}
	function getCampusInfo($id)
	{
		$this->db->select('cc.short_name,s.state_name,ci.city_name,c.*');
		$this->db->from('campus as c');
		$this->db->join('country as cc','cc.country_id = c.country_id', 'left');
		$this->db->join('state as s','s.state_id = c.state_id', 'left');
		$this->db->join('city as ci','ci.city_id = c.city_id', 'left');		
		$this->db->where('c.campus_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
}

?>
