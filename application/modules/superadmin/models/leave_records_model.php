<?php
class Leave_records_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getUserLeave()
	{
		$this->db->select('l.id,l.user_id,l.leave_id,u.first_name,u.last_name,u.status,l.leave_type,d.department_name,l.leave_start_date,l.leave_end_date,l.YR_MTH,sum(l.days) as total_leave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('users as u','u.user_id = l.user_id');
		$this->db->join('department as d','d.department_id = u.department_id');		
		$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');		
		$this->db->where("u.status = 'Active' ");
		$this->db->group_by("l.user_id","desc");
		$query=$this->db->get();
		return $query->result();
	}


	function getUserLeaveDetail($user_id)
	{
		$this->db->select('u.first_name,u.last_name,u.user_id,u.email,d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u1.first_name as af,u1.last_name as al,l.days as total_leave,ld.*');
		$this->db->from('leave_month as l');		
		$this->db->join('leaves as ld','ld.leave_id = l.leave_id');
		$this->db->join('users as u','u.user_id = l.user_id');		
		$this->db->join('users as u1','u1.user_id = ld.approved_by','left');
		$this->db->join('leave_status as ls','ls.leave_status_id = ld.leave_status', 'left');
		$this->db->join('department as d','d.department_id = u.department_id');	
		$this->db->join('leave_type as lt','lt.leave_type_id = ld.leave_type', 'left');	
		$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');		
		$this->db->where("u.user_id = '$user_id'");
		$this->db->where("u.status = 'Active' ");
		$this->db->order_by('l.leave_id','desc');		
		$query=$this->db->get();
		return $query->result();
	}

	function pendingleaveDetail($user_id){        				
		$this->db->select('u.first_name,u.last_name,u.user_id,u.email,d.department_name,ls.leave_status as lstatus,lt.leave_type as ltype,u.first_name as af,u.last_name as al,l.leave_days as total_leave,l.*');
		$this->db->from("leaves as l");
		$this->db->join('users as u','u.user_id = l.user_id');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('department as d','d.department_id = u.department_id');		
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');		
		$this->db->where("l.user_id = '$user_id'");
		$this->db->where("l.leave_status = '1'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->order_by('l.leave_id','desc');			
		$query = $this->db->get();
		return $query->result();
	
	}

	function pendingleave($user_id){        				
		$this->db->select("count(*) as pendingLeavetotal");
		$this->db->from("crm_leaves");
		$this->db->where("user_id = '$user_id'");
		$this->db->where("leave_status = '1'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");		
		$result = $this->db->get()->row();
		return $result;
	
	}

	function getUserDetail($user_id){
		$this->db->select('u.first_name,u.last_name,u.user_id');
		$this->db->from('crm_users as u');					
		$this->db->where("u.user_id = '$user_id'");
		$query=$this->db->get();
		return $query->row();
	}

	function SearchedRecords($start_date,$end_date,$department,$month,$year,$leave_type)
	{

		$this->db->select('l.id,l.user_id,l.leave_id,u.first_name,u.last_name,l.leave_type,d.department_name,l.leave_start_date,l.leave_end_date,l.YR_MTH,sum(l.days) as total_leave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('users as u','u.user_id = l.user_id');
		$this->db->join('department as d','d.department_id = u.department_id');

		if(!empty($start_date) && !empty($end_date)){
			/*$this->db->where("MONTH(YR_MTH) >= MONTH('".$start_date."')");
			$this->db->where("MONTH(YR_MTH) <= MONTH('".$end_date."')");
			$this->db->where("YEAR(YR_MTH) >= YEAR('".$start_date."')");
			$this->db->where("YEAR(YR_MTH) <= YEAR('".$end_date."')");*/
			$this->db->where("DATE(leave_start_date) BETWEEN '$start_date' AND '$end_date'");
		}
		if(!empty($department)){
			$this->db->where("d.department_name = '$department'");
		}
		if(!empty($leave_type)){
			$this->db->where("l.leave_type = '$leave_type'");
		}else{
			$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');
		}

		if(!empty($month)){
			$this->db->where("MONTH(YR_MTH) = MONTH('".$month."') AND YEAR(YR_MTH) = YEAR('".$month."')");
		}
		if(!empty($year)){
			$this->db->where("YEAR(YR_MTH) = '".$year."' ");
		}
		$this->db->where("u.status = 'Active' ");
		$this->db->group_by("l.user_id","desc");
		$query=$this->db->get();
		return $query->result();
	}

	function getChartRecords($start_date,$end_date,$department,$month,$year,$leave_type)
	{

		$this->db->select(' MONTHNAME( l.YR_MTH ) as month, YEAR( l.YR_MTH ) AS year, SUM(days) as total_leave, l.leave_type');
		$this->db->from('crm_leave_month as l');
		$this->db->join('users as u','u.user_id = l.user_id');
		$this->db->join('department as d','d.department_id = u.department_id');

		if(!empty($start_date) && !empty($end_date)){
			/*$this->db->where("MONTH(YR_MTH) >= MONTH('".$start_date."')");
			$this->db->where("MONTH(YR_MTH) <= MONTH('".$end_date."')");
			$this->db->where("YEAR(YR_MTH) >= YEAR('".$start_date."')");
			$this->db->where("YEAR(YR_MTH) <= YEAR('".$end_date."')");*/
			$this->db->where("DATE(leave_start_date) BETWEEN '$start_date' AND '$end_date'");
		}
		if(!empty($department)){
			$this->db->where("d.department_name = '$department'");
		}
		if(!empty($leave_type)){
			$this->db->where("l.leave_type = '$leave_type'");
		}else{
			$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');
		}

		if(!empty($month)){
			$this->db->where("MONTH(YR_MTH) = MONTH('".$month."') AND YEAR(YR_MTH) = YEAR('".$month."')");
		}
		if(!empty($year)){
			$this->db->where("YEAR(YR_MTH) = '".$year."' ");
		}
		$this->db->where("u.status = 'Active' ");
		$this->db->group_by("MONTH(l.YR_MTH ) ");

		$query=$this->db->get();
		return $query->result();
	}

	function getChartRecords2($start_date,$end_date,$department,$month,$year,$leave_type)
	{
       

		$this->db->select('MONTHNAME( l.YR_MTH ) as month, YEAR( l.YR_MTH ) AS year, SUM(days) as total_leave, l.leave_type, lt.leave_type');
		$this->db->from('crm_leave_month as l');
		$this->db->join('users as u','u.user_id = l.user_id');
		$this->db->join('department as d','d.department_id = u.department_id');
		$this->db->join('crm_leave_type as lt','lt.leave_type_id = l.leave_type','left');

        if(!empty($leave_type)){
			$this->db->where("l.leave_type = '$leave_type'");
		}else{
			$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');
		}

		if(!empty($department)){
			$this->db->where("d.department_name = '$department'");
		}

		if(!empty($start_date) && !empty($end_date)){			
			//$this->db->where("l.leave_start_date BETWEEN '".$start_date."' AND '".$end_date."'");
			$this->db->where("((l.leave_start_date BETWEEN '".$end_date."' AND '".$start_date."') or (l.leave_end_date BETWEEN '".$end_date."' AND '".$start_date."')) ");
		}
		if(!empty($month)){ 
			$this->db->where("MONTH(YR_MTH) = MONTH('".$month."') AND YEAR(YR_MTH) = YEAR('".$month."')");
		}
		if(!empty($year)){
			$this->db->where("YEAR(YR_MTH) = '".$year."' ");
		}

		if(empty($start_date) && empty($end_date) && empty($month) && empty($year)){
			$end_date2 = date('Y-m-01', strtotime("-7 months")); 
	        $start_date2 = date('Y-m-d');			    
			//$this->db->where("((l.leave_start_date BETWEEN '".$end_date2."' AND '".$start_date2."') or (l.leave_end_date BETWEEN '".$end_date2."' AND '".$start_date2."')) ");
			$this->db->where("((l.YR_MTH BETWEEN '".$end_date2."' AND '".$start_date2."')) ");
		}
		$this->db->where("u.status = 'Active' ");
		$this->db->group_by("MONTH(l.YR_MTH)");
		$this->db->group_by('l.leave_type');
		$this->db->order_by('l.leave_start_date','Desc');

		$query=$this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
}

?>
