<?php
class Country_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	
	function getAllCountry()
	{
		$this->db->select('*');
		$this->db->from('country');
		$this->db->order_by("country_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('country',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function getCountry($catId){
		$this->db->select('*');
		$this->db->from('country');
		$this->db->where('country_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateCountry($data){
		
		$this->db->where('country_id',$data['country_id']);
		if($this->db->update('country',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function deleteCountry($id)
	{
		$this->db->where('country_id',$id);
	    $this->db->delete('country');
	}
}

?>