<?php
class state_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllstate()
	{
		$this->db->select('c.short_name,s.*');
		$this->db->from('state as s');
		$this->db->join('country as c',"c.country_id = s.country_id","left");
		$this->db->order_by("s.state_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('state',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getstate($catId){
		$this->db->select('*');
		$this->db->from('state');
		$this->db->where('state_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updatestate($data){

		$this->db->where('state_id',$data['state_id']);
		if($this->db->update('state',$data)){
			return true;
		}else{
			return false;
		}
	}

	function deletestate($id)
	{
		$this->db->where('state_id',$id);
	    $this->db->delete('state');
	}
}

?>
