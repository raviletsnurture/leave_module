<?php
class AuditLogs_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllAuditLogs($id)
	{
		//echo "id - ".$id;
		$this->db->select('u.first_name,u.last_name,l.*');
		$this->db->from('auditlogs as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		//$this->db->order_by("id","desc");
		$this->db->order_by("l.timestamp","desc");
		/*if($id != 0){
		$this->db->where('l.user_id',$id);
	}*/
		$query=$this->db->get();
		//print_r($query->result());
		return $query->result();
	}

}
?>
