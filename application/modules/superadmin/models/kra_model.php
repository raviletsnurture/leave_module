<?php
class Kra_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllFeedbackFields()
	{
		$this->db->select('*');
		$this->db->from('crm_feedback_fields_master');
		$query=$this->db->get();
		return $query->result();
	}
	function getAllRoles()
	{
		$this->db->select('role_id,role_name');
		$this->db->from('crm_role');
		$query=$this->db->get();
		return $query->result();
	}
	function getAllDepartments(){
		$this->db->select('department_id,department_name');
		$this->db->from('crm_department');
		$query=$this->db->get();
		return $query->result();
	}
	function getAllSubRole(){
		$this->db->select('sub_role_id,role_id,role_name,status');
		$this->db->from('crm_sub_role');
		$query=$this->db->get();
		return $query->result();
	}
	function addNewKra($addData){
		$query = $this->db->insert('crm_feedback_fields_master',$addData);
		if($query){
			return true;
		}else{
			return false;
		}
	}

	function updateKra($addData){
		$this->db->where('id',$addData['id']);
		if($this->db->update('crm_feedback_fields_master',$addData)){
			return true;
		}else{
			return false;
		}
	}

	function deleteKra($deleteData){
		$this->db->where('id',$deleteData['id']);
	  if($this->db->delete('crm_feedback_fields_master')){
			return true;
		}else{
			return false;
		}
	}

}

?>
