<?php
class Recruitment_model extends CI_Model
{
	var $column_order = array('fullName','email','phone','experience','designation','currentDesignation','appointment'); //set column field database for datatable orderable
    var $column_search = array('fullName','email','phone','experience','designation','currentDesignation','appointment'); //set column field database for datatable searchable
    var $order = array('id' => 'asc'); // default order
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	/**
	* Developer Name : parth patwala
	* Date : 22-Jun-2017
	* Description : get data from crm_req_applicants table
	*
	* @return view
	*/
	function getRecruitmentData()
	{
		$this->db->select('*, r.id as rec_id');
		$this->db->from('req_applicants as r');
		$this->db->join('req_designation as d','d.id = r.requiredDesignation', 'left');
		$this->db->order_by('r.id DESC');
		$query=$this->db->get()->result();
		return $query;
	}

	function getRecruitmentFilterData($from_date,$to_date)
	{
		$from = date('Y-m-d',strtotime($from_date));
		$to = date('Y-m-d',strtotime($to_date));
		$this->db->select('*, r.id as rec_id');
		$this->db->from('req_applicants as r');
		$this->db->join('req_designation as d','d.id = r.requiredDesignation', 'left');
		$this->db->where("date(r.createdDate) between '$from' and '$to'");
		$this->db->order_by('r.id DESC');
		$query=$this->db->get()->result();
		return $query;
	}

	/**
	* Developer Name : parth patwala
	* Date : 22-Jun-2017
	* Description : get designation total count
	*
	* @return view
	*/
	function getDesignationCount()
	{
		$this->db->select('d.designation, COUNT(d.designation) designationCount');
		$this->db->from('req_applicants as r');
		$this->db->join('req_designation as d','d.id = r.requiredDesignation', 'left');
		$this->db->group_by('d.designation');
		$query=$this->db->get()->result();
		return $query;
	}

	/**
	* Developer Name : parth patwala
	* Date : 22-Jun-2017
	* Description : get single record from crm_req_applicants table
	*
	* @return view
	*/
	function getRecruitmentDataOfUser($id)
	{
		$this->db->select('*');
		$this->db->from('req_applicants as r');
		$this->db->join('req_designation as d','d.id = r.requiredDesignation', 'left');
		$this->db->join('req_applicants_time_slots as s','s.req_applicants_id = r.id
			', 'left');
		$this->db->where("r.id = $id");
		$this->db->order_by('d.id DESC');
		$query=$this->db->get()->result();
		return $query;
	}



	function updateInterviewSechduleDate($data){
		$this->db->where('id',$data['id']);
		if($this->db->update('req_applicants',$data)){
			return true;
		}else{
			return false;
		}
	}

	function blockUser($id){
		$this->db->where('id',$id);
		$data = array();
		$data['isBlocked'] = 1;
		if($this->db->update('req_applicants',$data)){
			return true;
		}else{
			return false;
		}
	}

	function unblockUser($id){
		$this->db->where('id',$id);
		$data = array();
		$data['isBlocked'] = 0;
		if($this->db->update('req_applicants',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getRecruitmentDetailsOfUser($id, $getRecruitmentDetailsOfUser)
	{
		$data = $this->getRecruitmentDataOfUser($id);
		$data1 = array(
						'candidate_name' => $data[0]->fullName,
						'interviewers' => $getRecruitmentDetailsOfUser,
						'app_receive_date' => $data[0]->createdDate,
						'interview_date' => date("Y-m-d h:i:s",strtotime($data[0]->interview_date)),
						'qualification' => $data[0]->qualification,
						'current_company' => $data[0]->currentEmployer,
						'current_position' => $data[0]->currentDesignation,
						'applied_position' => $data[0]->designation,
						'current_ctc' => $data[0]->salary,
						'total_experience' => $data[0]->experience,
						'current_location' => $data[0]->currentLocation,
						'contact_number' => $data[0]->phone,
						'email' => $data[0]->email,
						'resume' => $data[0]->file,
		);
		$this->db->insert('crm_interview', $data1);
		$insert_id = $this->db->insert_id();
		return $data1;
	}
	function getEmailOfinterviewer($id){
		$this->db->select('email');
		$this->db->from('crm_users');
		$this->db->where_in('user_id', $id);
		$query = $this->db->get()->result_array();
		$arr = array_column($query,"email");
		return $arr;
	}
	function getCandidateDetail($id){
		$this->db->select('*');
		$this->db->from('crm_interview');
		$this->db->where("interview_id = $id");
		$query=$this->db->get();
		return $query->result_array();
	}


	function addTechnology($data){
	    if($this->db->insert('job_opening',$data)){
			return true;
		}else{
			return false;
		}
	}
	function addVacancy($data,$data2){
		$this->db->where('job_id',$data2['technology_name']);
		if($this->db->update('job_opening',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getAllJobOpening(){
		$this->db->select('*');
		$this->db->from('job_opening as j');
		$this->db->where('vacancy != 0');
		$this->db->order_by("j.job_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function getAllJob(){
		$this->db->select('*');
		$this->db->from('job_opening as j');
		$this->db->order_by("j.job_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function jobdelete($id){
        $this->db->delete('job_opening', array('job_id' => $id));
	}

	function getRecruitmentDetails(){
		$response = array();
		$this->db->select('email as EMAIL,fullName as NAME,currentLocation as address,phone as SMS');
		$q = $this->db->get('req_applicants');
		$response = $q->result_array();
		return $response;
	}

	/**
	* Developer Name : dipika patel
	* Date : 10-July-2019
	* Description : View Server Side Progressive Detail
	*
	* @return view
	*/
	function get_datatables()
	{

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
        return $query->result();
	}

	/**
	* Developer Name : dipika patel
	* Date : 10-July-2019
	* Description : View Server Side Progressive Detail
	*
	* @return view
	*/
	private function _get_datatables_query()
    {

        //add custom filter here
        if($this->input->post('appliedStartDate') && $this->input->post('appliedEndDate'))
        {
			$from = date('Y-m-d',strtotime($this->input->post('appliedStartDate')));
		    $to = date('Y-m-d',strtotime($this->input->post('appliedEndDate')));
			$this->db->where("date(crm_req_applicants.createdDate) between '$from' and '$to'");
		}

        if($this->input->post('filexpfrom') || $this->input->post('filexpto'))
        {
		   $from = $this->input->post('filexpfrom');
		   $to = $this->input->post('filexpto');
		   $this->db->where("crm_req_applicants.experience between '$from' and '$to'");
		}

		if($this->input->post('designation'))
        {
		   $this->db->where('crm_req_designation.designation', $this->input->post('designation'));
		}

		if($this->input->post('fullname'))
        {
		   $this->db->like('crm_req_applicants.fullName', $this->input->post('fullname'));
		}

		if($this->input->post('email'))
        {
		   $this->db->like('crm_req_applicants.email', $this->input->post('email'));
		}

		if($this->input->post('phone'))
        {
		   $this->db->like('crm_req_applicants.phone', $this->input->post('phone'));
		}

		if($this->input->post('experience'))
        {
		   $this->db->where('crm_req_applicants.experience', $this->input->post('experience'));
		}

		if($this->input->post('appointment'))
        {
		   $this->db->like('crm_req_applicants.interview_date', $this->input->post('appointment'));
		}

		if($this->input->post('current'))
        {
		   $this->db->like('crm_req_applicants.currentDesignation', $this->input->post('current'));
		}

		if($this->input->post('applied'))
        {
		   $this->db->where('crm_req_applicants.createdDate', $this->input->post('applied'));
		}

        if($this->input->post('applied_designation'))
        {
		   $this->db->like('crm_req_designation.designation', $this->input->post('applied_designation'));
		}

		if($this->input->post('applicantStatusFilter'))
        {
		   $this->db->like('crm_req_applicants.status', $this->input->post('applicantStatusFilter'));
		}

		$this->db->select('*, crm_req_applicants.id as rec_id, crm_req_applicants.status as recStatus');
		$this->db->from('crm_req_applicants');
		$this->db->join('crm_req_designation','crm_req_designation.id = crm_req_applicants.requiredDesignation', 'left');
		// $this->db->order_by("createdDate", "desc");

		$i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

				if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like('crm_req_applicants'.$item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like('crm_req_applicants'.$item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket

			}

            $i++;
		}

        if(isset($_POST['order'])) // here order processing
        {
			$this->db->order_by('crm_req_applicants.'.$this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {

			$order = $this->order;
            //$this->db->order_by('crm_req_applicants.'.key($order), $order[key($order)]);
						$this->db->order_by("createdDate", "desc");
		}
	}


	function count_all()
    {
        $this->db->from('req_applicants');
        return $this->db->count_all_results();
	}

	function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
	}
	function getTotalRecruitmentData()
	{
		$this->db->select('email');
		$this->db->distinct();
		$this->db->from('req_applicants as r');
		$query=$this->db->get()->num_rows();
		return $query;
	}

	function getRecruitmentDataWithPaginate($start=null)
	{
		$sql = "SELECT GROUP_CONCAT(email) as email FROM (SELECT DISTINCT email FROM crm_req_applicants ORDER BY `id` ASC LIMIT ".$start.",500) as email";
		$query = $this->db->query($sql);
		return $query->result();
	}

	// Updated Application status
	function updateApplicantStatus($data)
	{
		$this->db->set('status',$data['option']);
		$this->db->where('id',$data['userID']);
		if($this->db->update('crm_req_applicants')){
			return true;
		}else{
			return false;
		}
	}
}

?>
