<?php
class Cast_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllCast()
	{
		$this->db->select('*');
		$this->db->from('cast');
		$this->db->order_by("cast_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('cast',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getCast($catId){
		$this->db->select('*');
		$this->db->from('cast');
		$this->db->where('cast_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateCast($data){

		$this->db->where('cast_id',$data['cast_id']);
		if($this->db->update('cast',$data)){
			return true;
		}else{
			return false;
		}
	}

	function deleteCast($id)
	{
		$this->db->where('cast_id',$id);
	    $this->db->delete('cast');
	}
}

?>
