<?php
class request_swap_model extends CI_Model
{ 
    function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
    }
    
    function getAllSwappingLeaves(){		
		
		$this->db->select("l.leave_id,l.leave_type,u.first_name,u.middle_name,u.last_name,u.department_id,d.department_name,l.leave_start_date,l.leave_end_date,l.leave_status,l.approved_by,l.leave_days,l.leave_created,u.status,l.is_cancelled,l.leave_reason,l.leave_critical_task,l.comment");		
        $this->db->from('crm_leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->join('crm_department as d','u.department_id = d.department_id');
		$this->db->where("l.leave_type = '19'");
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("l.leave_created","DESC");
		$query = $this->db->get()->result_array();	
		return $query;
    }
}
?>