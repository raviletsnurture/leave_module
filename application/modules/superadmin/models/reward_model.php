<?php
class Reward_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function addReward($data)
	{
		if($this->db->insert('reward_type', $data)){
			return true;
		}else{
			return false;
		}
	}

	function updateReward($data, $id)
	{
		$this->db->where("id", $id);
		if($this->db->update('reward_type', $data)){
			return true;
		}else{
			return false;
		}
	}

	function updateProduct($data, $id)
	{
		$this->db->where("id", $id);
		if($this->db->update('reward_products', $data)){
			return true;
		}else{
			return false;
		}
	}

	function addExReward($data)
	{
		if($this->db->insert('crm_reward_exeptional', $data)){
			return true;
		}else{
			return false;
		}
	}

	function addProduct($data)
	{
		if($this->db->insert('reward_products', $data)){
			return true;
		}else{
			return false;
		}
	}

	function getAllDeviceToken(){
		$this->db->select("deviceToken");
		$this->db->from("device");
		$query = $this->db->get()->result();
		return $query;
	}

	function getAllDeviceTokenbyType($type){
		$this->db->select("deviceToken");
		$this->db->from("device");
		$this->db->where("deviceType", $type);
		$query = $this->db->get()->result();
		return $query;
	}

	function getAllAnnouncements()
	{
		$sql = "SELECT *
				FROM crm_announcement
				ORDER BY created_time DESC";
		$query = $this->db->query($sql);
		return $results = $query->result();
	}

	function deleteAnnouncement($annId)
	{
		$this->db->where('announcement_id', $annId);
	    $this->db->delete('announcement');
	}

	function getAllRewardsHistory(){
		$this->db->select("rh.*, u.reward_points, u.first_name, u.last_name, rh.status as reward_status, rh.id as rh_id, rh.description as description, rt.id as reward_type_id, rt.point as reward_type_point, rt.reward_name as reward_type_name, rt.point as reward_given_point, ua.first_name as afirst_name, ua.last_name as alast_name,rt.id as reward_id");
		$this->db->from("crm_reward_history as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->join('users as ua','ua.user_id = rh.approved_by', 'left');
		$this->db->join('crm_reward_type as rt','rt.id = rh.reward_id', 'left');
		$this->db->where("u.status = 'Active' ");
		//$this->db->where("rh.user_id = $id");
		$this->db->order_by("date","desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getUserRewardsHistory($user_id){
		$this->db->select("rh.*, u.reward_points, u.first_name, u.last_name, rh.status as reward_status, rh.id as rh_id, rh.description as description, rt.id as reward_type_id, rt.point as reward_type_point, rt.reward_name as reward_type_name, rt.point as reward_given_point, ua.first_name as afirst_name, ua.last_name as alast_name,rt.id as reward_id");
		$this->db->from("crm_reward_history as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->join('users as ua','ua.user_id = rh.approved_by', 'left');
		$this->db->join('crm_reward_type as rt','rt.id = rh.reward_id', 'left');
		$this->db->where("u.user_id = $user_id ");
		$this->db->where("u.status = 'Active' ");
		$this->db->order_by("date","desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRedeemHistory(){
	    $this->db->select("rh.*, u.reward_points, u.first_name, u.last_name, p.name,p.point");
		$this->db->from("crm_reward_redeems as rh");
		$this->db->join('users as u','u.user_id = rh.user_id', 'left');
		$this->db->join('crm_reward_products as p','p.id = rh.product_id', 'left');
		$this->db->where("u.status = 'Active' ");
		//$this->db->where("rh.user_id = $id");
		$this->db->order_by("created_at","desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRewardsProducts(){
		$this->db->select("*");
		$this->db->from("crm_reward_products as p");
		$this->db->order_by("created_at","desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllRewardsPoints(){
		$this->db->select("*");
		$this->db->from("crm_reward_type as p");
		$this->db->order_by("id","desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllUserRewardsPoints($month = ''){

		if($month != ''){
            $time=strtotime($month);
            $month=date("m",$time);
            $year=date("Y",$time);
            $this->db->select("*,sum(history.point) as karma_points,user.user_id");
            $this->db->from("crm_reward_history as history");
            $this->db->join("crm_users as user","user.user_id = history.user_id","join");
            $this->db->join('crm_department as d','d.department_id = user.department_id', 'left');
            $this->db->where("history.status = 1");
            $this->db->where("history.reward_type = 1");
            $this->db->where("MONTH(history.date) = $month");
            $this->db->where("YEAR(history.date) = $year");
            $this->db->where("user.status = 'Active' ");
            $this->db->group_by("history.user_id");
            $this->db->order_by("user.karma_points", "desc");
            $result = $this->db->get()->result();
            return $result;
		}else{
			$this->db->select("p.first_name, p.last_name, p.karma_points, p.status, d.department_name,p.user_id");
			$this->db->from("crm_users as p");
			$this->db->join('crm_department as d','d.department_id = p.department_id', 'left');
			$this->db->where("p.status","Active");
			$this->db->where("d.department_id <> 1");
			$this->db->order_by("p.karma_points", "desc");
			$result = $this->db->get()->result();
			return $result;
		}

	}

	function getAllUsers(){
		$this->db->select("p.user_id, p.first_name, p.last_name, d.department_name");
		$this->db->from("crm_users as p");
		$this->db->join('crm_department as d','d.department_id = p.department_id', 'left');
		$this->db->where("p.status","Active");
		$this->db->where("d.department_id <> 1");
		$this->db->order_by("p.first_name", "asc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getAllPMs(){
		$this->db->select("p.user_id, p.first_name, p.last_name");
		$this->db->from("crm_users as p");
		$this->db->where("(`p`.`status` = 'Active' AND `p`.`role_id` = 15)");
		$this->db->or_where("(`p`.`status` = 'Active' AND `p`.`role_id` = 20)");
		$this->db->or_where("(`p`.`status` = 'Active' AND `p`.`role_id` = 51)");
		$this->db->order_by("p.first_name", "asc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getPMsrewardpointhistory(){
	    $this->db->select("*");
		$this->db->from("crm_reward_PM_history as h");
		$this->db->join("crm_users as p","p.user_id = h.user_id","inner");
		$this->db->where("MONTH(created_at) = MONTH(CURRENT_DATE())");
		$this->db->where("p.status = 'Active' ");
		$this->db->order_by("h.created_at", "desc");
		$result = $this->db->get()->result();
		return $result;
	}

	function getRewardUser($id){
		$this->db->select("*");
		$this->db->from("crm_users");
		$this->db->where("user_id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function updateRewardUser($data, $user_id){
		$this->db->where('user_id',$user_id);
		if($this->db->update('crm_users',$data)){
			return true;
		}else{
			return false;
		}
	}

	function addRewardHistory($data){
	    if($this->db->insert('crm_reward_PM_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getRewardDetailsById($id){
		$this->db->select("*");
		$this->db->from("crm_reward_type");
		$this->db->where("id = $id");
		$result = $this->db->get()->result();
		return $result;
	}

	function getReward($id){
		$this->db->select("*");
		$this->db->from("crm_reward_type");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function getProduct($id){
		$this->db->select("*");
		$this->db->from("crm_reward_products");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->result();
		return $result;
	}

	function getRewardHistory($id){
		$this->db->select("*");
		$this->db->from("crm_reward_history");
		$this->db->where("id = ".$id);
		$result = $this->db->get()->row();
		return $result;
	}

	function addRewardtoUser($data){
	    if($this->db->insert('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function addEOM($data){
	    if($this->db->insert('crm_reward_eom',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getEOM(){
		$this->db->select("eom.*, u.first_name, u.last_name, d.department_name");
		$this->db->from("crm_reward_eom as eom");
		$this->db->join('crm_users as u','u.user_id = eom.user_id', 'left');
		$this->db->join('crm_department as d','d.department_id = u.department_id', 'left');
		$this->db->order_by("month", 'desc');
		$result = $this->db->get()->result();
		return $result;
	}

	function updateRewardStatus($data, $reward_id){
		$this->db->where('id',$reward_id);
		if($this->db->update('crm_reward_history',$data)){
			return true;
		}else{
			return false;
		}
	}

	function claimProductUpdate($data,$row_id, $user_id){
        $this->db->where('user_id',$user_id);
        $this->db->where('id',$row_id);
		if($this->db->update('crm_reward_redeems',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getUserRedeemHistory($row_id, $user_id){
	    $this->db->select("rh.status,p.point");
		$this->db->from("crm_reward_redeems as rh");
		$this->db->join('crm_reward_products as p','p.id = rh.product_id');
		$this->db->where('rh.user_id',$user_id);
        $this->db->where('rh.id',$row_id);
		$result = $this->db->get()->result();
		return $result;
	}
}
?>
