<?php
class Dashboard_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	function getUserinfo()
	{
		$this->db->select('*');
		$this->db->from('super_admin');
		$query=$this->db->get();
		return $query->result();
	}
	public function updateUser($addData)
	{
		$this->db->where('sId',$addData['sId']);
		if($this->db->update('super_admin',$addData)){
			return true;
		}else{
			return false;
		}
	}

	//check if any feedback remaing last month then display popup
	function checkRemaningFeedback()
	{	//TL,PM,MGMT,HR,Sr HR
		$roles = '1,52,15,20,22,49';
		$userId = '102,165,10';
		$this->db->select('u.first_name,u.last_name,d.department_name,(SELECT user_id from crm_feedback as f WHERE u.user_id = f.user_id and YEAR(f.created_time) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(f.created_time) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)) as feedback');
		$this->db->from('crm_users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('u.role_id not in ('.$roles.')');
		$this->db->where('u.user_id not in ('.$userId.')');
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("feedback","ASC");
		$query=$this->db->get();
		return $query->result();
	}


	function getupcominguserleaves(){
		$this->db->select('ls.leave_status as leave_status_name,lt.leave_type as leave_name,u.first_name,u.last_name,l.*');
		$this->db->from('leaves as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		//$this->db->join('users as au','au.user_id = l.approved_by', 'left');
		$this->db->join('leave_status as ls','ls.leave_status_id = l.leave_status', 'left');
		$this->db->join('leave_type as lt','lt.leave_type_id = l.leave_type', 'left');
		//$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("l.is_cancelled != 1");
		$this->db->where("l.leave_start_date > now()");
		$this->db->where("(l.leave_status = 1 OR l.leave_status = 2)");
		//$this->db->order_by("leave_id","desc");
		$this->db->order_by("l.leave_start_date","asc");
		$this->db->where("u.status =  'Active'");
		//$this->db->where('u.department_id != '.$id);
		$this->db->limit(10);
		$query=$this->db->get()->result_array();
		//echo $this->db->last_query();
		return $query;
	}

	/**
	* Develoer : parth patwala
	* Date : 22-jun-2017
	*
	* @return array
	*/
	function getProbationEmployeeList()
	{
		$this->db->select('u.user_id, d.department_name, u.employee_type, u.first_name, u.last_name, u.ex_year, u.joining_date,u.trainee_complete_at,u.probation_complete_at');
		$this->db->from('users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where("u.status = 'Active'");
		$this->db->where("u.joining_date != '0000-00-00'");
		$this->db->where("u.employee_type = 'Probation'");
		$this->db->or_where("u.employee_type = 'Trainee'");
		//$this->db->order_by("l.leave_start_date","asc");
		$query=$this->db->get()->result_array();
		return $query;
	}

	/**
	* Develoer : Arpit Mehta
	* Date : 4-jul-2017
	*
	* @return array
	*/
	function getRelievingDateList()
	{
		$this->db->select('u.user_id, d.department_name, u.employee_type, u.first_name, u.last_name, u.ex_year, u.joining_date, u.reliving_date');
		$this->db->from('users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		//$this->db->where("u.employee_type = 'Probation'");
		$this->db->where("u.reliving_date >= CURDATE() ");
		//$this->db->order_by("l.leave_start_date","asc");
		$query=$this->db->get()->result_array();
		return $query;
	}

	/*
	* Developed By : Jignasa
	* Date : 31/07/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	leave type = EL, Urgent, Birthday Leave, Anniversary Leave, Marriage Leave, Early Leave.
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function checkTotalNumberLeave($leave_start_date){

		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		/*$this->db->select('SUM( l.leave_days) as totalLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');
		$this->db->where('l.leave_status = 2');
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();*/

        $this->db->select('SUM(days) as totalLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('l.leave_type in (3,4,8,9,10,13,18,20)');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	/*
	* Developed By : Jignasa
	* Date : 31/07/2017
	* Modified By :
	* Description :  get Pending leave Total
	*/
	function getPendingLeaveTotal(){
		$datetime = new DateTime();
		$datetime->setTimezone(new DateTimeZone('Asia/Kolkata'));
		$datetime->add(new DateInterval("P1D"));
		$startDate = $datetime->format('Y-m-d');
		$this->db->select("sum(leave_days) as pendingLeavetotal");
		$this->db->from("crm_leaves as l");
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where("l.leave_status = '1'");
		$this->db->where("l.is_cancelled != '1'");
		$this->db->where("l.leave_days != '0'");
		$this->db->where("l.leave_start_date <= '$startDate'");
		$this->db->where("u.status =  'Active'");
		$result = $this->db->get()->row();
		return $result;
	}

	/*
	* Developed By : Jignasa
	* Date : 31/07/2017
	* Modified By :
	* Description : Total Number of Active Employee in The company X 1 EL is a threshold for given month this threshold is crossed please do not allow any employee to take leave. They will need to discuss with HR Team for Leave. ( HR team can add leave)
	Ex if we have 100 employee . maximum leave we can allow is 100. Beyond that in same month is not going to get accepted without discussion with HR.
	*/
	function getActiveEmployee(){
		$this->db->select('COUNT(user_id)');
		$this->db->from('crm_users');
		$this->db->where("status =  'Active'");
		return $this->db->count_all_results();
	}

	/*
	* Developed By : Jignasa
	* Date : 31/07/2017
	* Modified By :
	* Description :  get Pending leaves of current month
	*/
	function getMonthPendingLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		$this->db->select('SUM(l.leave_days) as monthPendingLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		$this->db->where("leave_status = '1'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : Dipika
	* Date : 12/02/2018
	* Modified By :
	* Description :  get early leaves of current month
	*/
	function getEarlyPendingLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		/*$this->db->select('SUM( l.leave_days) as monthEarlyLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where('l.leave_type in (6)');
		$this->db->where("leave_status = '2'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");
		$query=$this->db->get();
		return $query->row();*/

		$this->db->select('SUM(days) as monthEarlyLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('l.leave_type in (6)');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();

	}

	/*
	* Developed By : Jignasa
	* Date : 31/07/2017
	* Modified By :
	* Description :  get sum of total birthday and anniversary leaves of current month
	*/
	function getMonthBirthAnniLeave($leave_start_date){
		$year = date('Y',strtotime($leave_start_date));
		$month = date('m',strtotime($leave_start_date));
		$this->db->select('SUM( l.leave_days) as monthBirthAnniLeave');
		$this->db->from('leaves as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id', 'left');
		$this->db->where('YEAR(l.leave_start_date) ='.$year);
		$this->db->where('MONTH(l.leave_start_date)='.$month);
		$this->db->where('l.leave_type in (8,9)');
		$this->db->where("leave_status = '2'");
		$this->db->where("is_cancelled != '1'");
		$this->db->where("leave_days != '0'");
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of recruitments for last month
	*/
	function getNoOfRecruitmentLastMonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select('user_id');
		$this->db->from('crm_users as u');
		$this->db->where('YEAR(u.joining_date)='.$year);
		$this->db->where('MONTH(u.joining_date)='.$month);
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get();
		return $query->num_rows();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of recruitments for last 3 months
	*/
	function getNoOfRecruitmentLast3Month(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
		$this->db->select('*');
		$this->db->from('crm_users as u');
		$this->db->where('u.joining_date BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get();
		return $query->num_rows();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of recruitments for last year
	*/
	function getNoOfRecruitmentLastYear(){
		$year = date('Y', strtotime('-1 year'));
		$this->db->select('user_id');
		$this->db->from('crm_users as u');
		$this->db->where('YEAR(u.joining_date)='.$year);
		$this->db->where("u.status =  'Active'");
		$query=$this->db->get();
		return $query->num_rows();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of applications recieved last month
	*/
	function ApplicationRecievedLastMonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select('*');
		$this->db->from('crm_req_applicants as u');
		$this->db->where('MONTH(u.createdDate)='.$month);
		$this->db->where('YEAR(u.createdDate)='.$year);
		$query=$this->db->get()->num_rows();
		return array('applicationLastMonth' => $query, 'month' => $month, 'year' => $year);
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of applications recieved last 3 months
	*/
	function ApplicationRecievedLast3Month(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
		$this->db->select('*');
		$this->db->from('crm_req_applicants as u');
		$this->db->where('u.createdDate BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$query=$this->db->get()->num_rows();
		// return $query->num_rows();
		return array('applicationLast3Months' => $query, 'startDay' => $start_date, 'endDay' => $end_date);
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of applications recieved last year
	*/
	function ApplicationRecievedLastYear(){
		$year = date('Y', strtotime('-1 year'));
		$this->db->select('*');
		$this->db->from('crm_req_applicants as u');
		$this->db->where('YEAR(u.createdDate)='.$year);
		$query=$this->db->get()->num_rows();
		return array('applicationLastYear' => $query, 'year' => $year);
	}

	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of Interviews scheduled last month
	*/
	function InterviewsScheduleLastMonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select('id');
		$this->db->from('crm_req_applicants as u');
		$this->db->where('MONTH(u.createdDate)='.$month);
		$this->db->where('YEAR(u.createdDate)='.$year);
		$this->db->where('interview_date !=','NULL');
		$this->db->where('interview_date !=','');
		$query=$this->db->get();
		return $query->num_rows();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of Interviews scheduled last 3 months
	*/
	function InterviewsScheduleLast3Month(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
		$this->db->select('id');
		$this->db->from('crm_req_applicants as u');
		$this->db->where('u.createdDate BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$this->db->where('interview_date !=','NULL');
		$this->db->where('interview_date !=','');
		$query=$this->db->get();
		return $query->num_rows();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get no of Interviews scheduled last year
	*/
	function InterviewsScheduleLastYear(){
		$year = date('Y', strtotime('-1 year'));
		$this->db->select('id');
		$this->db->from('crm_req_applicants as u');
		$this->db->where('YEAR(u.createdDate)='.$year);
		$this->db->where('interview_date !=','NULL');
		$this->db->where('interview_date !=','');
		$query=$this->db->get();
		return $query->num_rows();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Total Leaves Last Month
	*/
	function getTotalLeavesLastMonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
        $this->db->select('SUM(days) as totalLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Total Leaves Last 3 Months
	*/
	function getTotalLeavesLast3Months(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
        $this->db->select('SUM(days) as totalLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('l.YR_MTH BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Total Leaves Last Year
	*/
	function getTotalLeavesLastYear(){
		$year = date('Y', strtotime('-1 year'));
        $this->db->select('SUM(days) as totalLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('YEAR(l.YR_MTH)='.$year);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Average Leave Last Month
	*/
	function AverageLeaveLastMonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select('AVG(days) as AverageLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		// $this->db->group_by('u.user_id');
		$this->db->where('YEAR(l.YR_MTH) ='.$year);
		$this->db->where('MONTH(l.YR_MTH)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Average Leave Last 3 Months
	*/
	function AverageLeaveLast3Months(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
		$this->db->select('AVG(days) as AverageLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('l.YR_MTH BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Average Leave Last Year
	*/
	function AverageLeaveLastYear(){
		$year = date('Y', strtotime('-1 year'));
		$this->db->select('AVG(days) as AverageLeave');
		$this->db->from('crm_leave_month as l');
		$this->db->join('crm_users as u','u.user_id = l.user_id');
		$this->db->where('YEAR(l.YR_MTH)='.$year);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Average Working Hours Last month
	*/
	function AverageWorkingHoursLastMonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select('SEC_TO_TIME( AVG( TIME_TO_SEC( `gross_work_hours` ) ) ) AS AverageWorkingHour');
		$this->db->from('crm_employ_log as l');
		$this->db->join('crm_users as u','u.user_id = l.punch_id');
		$this->db->where('YEAR(l.log_date) ='.$year);
		$this->db->where('MONTH(l.log_date)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Average Working Hours Last 3 months
	*/
	function AverageWorkingHoursLast3Months(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
		$this->db->select('SEC_TO_TIME( AVG( TIME_TO_SEC( `gross_work_hours` ) ) ) AS AverageWorkingHour');
		$this->db->from('crm_employ_log as l');
		$this->db->join('crm_users as u','u.user_id = l.punch_id');
		$this->db->where('l.log_date BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 16-04-2019
	* Modified By :
	* Description :  Get Average Working Hours Last Year
	*/
	function AverageWorkingHoursLastYear(){
		$year = date('Y', strtotime('-1 year'));
		$this->db->select('SEC_TO_TIME( AVG( TIME_TO_SEC( `gross_work_hours` ) ) ) AS AverageWorkingHour');
		$this->db->from('crm_employ_log as l');
		$this->db->join('crm_users as u','u.user_id = l.punch_id');
		$this->db->where('YEAR(l.log_date)='.$year);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Get no.of Male and Female Last Month
	*/
	function MaleFemaleRatioLastMonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select('SUM(CASE WHEN gender = "Male" THEN 1 ELSE 0 END) as MaleCount, SUM(CASE WHEN gender = "Female" THEN 1 ELSE 0 END) as FemaleCount');
		$this->db->from('crm_users as l');
		$this->db->where('YEAR(l.joining_date) ='.$year);
		$this->db->where('MONTH(l.joining_date)='.$month);
		$this->db->where("l.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Get no.of Male and Female in Last 3 Months
	*/
	function MaleFemaleRatioLast3Months(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
		$this->db->select('SUM(CASE WHEN gender = "Male" THEN 1 ELSE 0 END) as MaleCount, SUM(CASE WHEN gender = "Female" THEN 1 ELSE 0 END) as FemaleCount');
		$this->db->from('crm_users as l');
		$this->db->where('l.joining_date BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$this->db->where("l.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Get no.of Male and Female in Last 3 Months
	*/
	function MaleFemaleRatioLastYear(){
		$year = date('Y', strtotime('-1 year'));
		$this->db->select('SUM(CASE WHEN gender = "Male" THEN 1 ELSE 0 END) as MaleCount, SUM(CASE WHEN gender = "Female" THEN 1 ELSE 0 END) as FemaleCount');
		$this->db->from('crm_users as l');
		$this->db->where('YEAR(l.joining_date)='.$year);
		$this->db->where("l.status = 'Active'");
		$query=$this->db->get();
		return $query->row();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Get Oldest employees according to working Days
	*/
	function OldestEmployees(){
		// $this->db->select('u.last_name, u.first_name, COUNT(l.punch_id) as TotalWorkingDays');
		// $this->db->from('crm_employ_log as l');
		// $this->db->join('crm_users as u','u.user_id = l.punch_id');
		// $this->db->where("u.status = 'Active'");
		// $this->db->group_by('u.user_id');
		// $this->db->order_by('TotalWorkingDays', 'desc');
		// $this->db->limit(5);
		// $query=$this->db->get();
		$this->db->select('last_name, first_name,user_created');
		$this->db->from('crm_users');
		$this->db->where("status = 'Active'");
		$this->db->where("user_id != '10'");
		$this->db->limit(5);
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Get Newest employees according to working Days
	*/
	function NewestEmployees(){
		// $this->db->select('u.last_name, u.first_name, COUNT(l.punch_id) as TotalWorkingDays');
		// $this->db->from('crm_employ_log as l');
		// $this->db->join('crm_users as u','u.user_id = l.punch_id');
		// $this->db->where("u.status = 'Active'");
		// $this->db->group_by('u.user_id');
		// $this->db->order_by('TotalWorkingDays', 'ASC');
		// $this->db->limit(5);
		// $query=$this->db->get();
		$this->db->select('last_name, first_name,user_created');
		$this->db->from('crm_users');
		$this->db->where("status = 'Active'");
		$this->db->where("user_id != '10'");
		$this->db->order_by('user_id', 'desc');
		$this->db->limit(5);
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Top Rewards Claimed by Employees
	*/
	function TopReward(){
		$this->db->select('distinct(MAX(rr.product_id)) as ProductID, rp.name as ProductName, rp.point as ProductPoint');
		$this->db->from('crm_reward_redeems as rr');
		$this->db->join('crm_users as u','u.user_id = rr.user_id');
		$this->db->join('crm_reward_products as rp','rp.id = rr.product_id');
		$this->db->where("u.status = 'Active'");
		$this->db->where("rp.status = '1'");
		$this->db->group_by('rr.id');
		$this->db->order_by('ProductID', 'DESC');
		$this->db->limit(5);
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Average Experience of team member Last Month
	*/
	function AverageExperienceOfTeamMemberLastmonth(){
		$month = date('m', strtotime('-1 month'));
		$year = date('Y');
		$this->db->select('COUNT(u.user_id) as total_user, SUM(u.ex_year) as ExYear, SUM(u.ex_month) as ExMonth');
		$this->db->from('crm_users as u');
		$this->db->where('YEAR(u.joining_date) ='.$year);
		$this->db->where('MONTH(u.joining_date)='.$month);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Average Experience of team member Last 3 Months
	*/
	function AverageExperienceOfTeamMemberLast3months(){
		$start_date = date('Y-m-d', strtotime('first day of -3 month'));
		$end_date = date('Y-m-d', strtotime('first day of this month'));
		$this->db->select('COUNT(u.user_id) as total_user,SUM(u.ex_year) as ExYear, SUM(u.ex_month) as ExMonth');
		$this->db->from('crm_users as u');
		$this->db->where('u.joining_date BETWEEN "'. $start_date. '" and "'. $end_date.'"');
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->result_array();
	}

	/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description :  Average Experience of team member Last Year
	*/
	function AverageExperienceOfTeamMemberLastYear(){
		$year = date('Y', strtotime('-1 year'));
		$this->db->select('COUNT(u.user_id) as total_user, SUM(u.ex_year) as ExYear, SUM(u.ex_month) as ExMonth');
		$this->db->from('crm_users as u');
		$this->db->where('YEAR(u.joining_date)='.$year);
		$this->db->where("u.status = 'Active'");
		$query=$this->db->get();
		return $query->result_array();
	}
	/*
	* Developed By : MD Mashroor
	* Date : 22-04-2019
	* Modified By :
	* Description :  Top 10 users did HRMS access outside of Local IP.
	*/

	function HrmsAccessdOutside(){
		$this->db->select('u.first_name, u.last_name, u.user_id, COUNT(au.user_id) as noofAttempts, au.ip, au.user_agent');
		$this->db->from('crm_auditlogs as au');
		$this->db->join('crm_users as u','u.user_id = au.user_id');
		$this->db->where('au.timestamp >=(DATE(NOW()) + INTERVAL -7 DAY)');
		$this->db->not_like('au.ip','27.109.17.154');
		$this->db->not_like('au.ip','27.109.17.155');
		$this->db->not_like('au.ip','27.109.17.156');
		$this->db->not_like('au.ip','27.109.17.157');
		$this->db->not_like('au.ip','27.109.17.158');
		$this->db->not_like('au.ip','127.0.0.1');
		$this->db->not_like('au.ip','192.168','after');
		$this->db->order_by('noofAttempts','DESC');
		$this->db->group_by('au.user_id');
		$this->db->limit(10);
		$query=$this->db->get();
		return $query->result_array();
	}
}

?>
