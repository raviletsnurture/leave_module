<?php
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	function getAllUsers($users, $limit='', $start='',$for='')
	{
		$this->db->select('d.department_name,r.role_name,u.*,l.religion_name,c.cast_name,s.state_name, TIMESTAMPDIFF( YEAR, birthdate, CURDATE() ) AS age');
		$this->db->from('users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->join('role as r','u.role_id = r.role_id', 'left');
		$this->db->join('religion as l','u.religion_id = l.religion_id', 'left');
		$this->db->join('cast as c','u.cast_id = c.cast_id', 'left');
		$this->db->join('state as s','s.state_id = u.state_id', 'left');
		//$this->db->where("u.status = 'Active'");
		$this->db->order_by("u.user_id","desc");
		$query=$this->db->get();
		if($limit == NULL && $start == NULL && $for == NULL){
			return $query->num_rows();
		}else{
			return $query->result();
		}
	}

	function getAllUsersName($users, $limit='', $start='',$for='')
	{
		$this->db->select('d.department_name,r.role_name,u.*,l.religion_name,c.cast_name,s.state_name, TIMESTAMPDIFF( YEAR, birthdate, CURDATE() ) AS age');
		$this->db->from('users as u');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->join('role as r','u.role_id = r.role_id', 'left');
		$this->db->join('religion as l','u.religion_id = l.religion_id', 'left');
		$this->db->join('cast as c','u.cast_id = c.cast_id', 'left');
		$this->db->join('state as s','s.state_id = u.state_id', 'left');
		$this->db->where("u.status = 'Active'");
		$this->db->order_by("u.first_name","asc");
		$query=$this->db->get();
		if($limit == NULL && $start == NULL && $for == NULL){
			return $query->num_rows();
		}else{
			return $query->result();
		}
	}
	//Graph Data
	function religionGraph()
	{
		$this->db->select('COUNT( u.user_id ) as total , r.religion_name');
		$this->db->from('users as u');
		$this->db->join('religion as r','r.religion_id = u.religion_id');
		$this->db->group_by("r.religion_name");
		$query=$this->db->get();

		return $query->result();
	}
	function genderGraph()
	{
		$this->db->select('COUNT(user_id ) as total , gender');
		$this->db->from('users');
		$this->db->group_by("gender");
		$query=$this->db->get();

		return $query->result();
	}
	function castGraph()
	{
		$this->db->select('COUNT( u.user_id ) as total , c.cast_name');
		$this->db->from('users as u');
		$this->db->join('cast as c','c.cast_id = u.cast_id');
		$this->db->group_by("c.cast_name");
		$query=$this->db->get();

		return $query->result();
	}
	function stateGraph()
	{
		$this->db->select('COUNT( u.user_id ) as total , s.state_name');
		$this->db->from('users as u');
		$this->db->join('state as s','s.state_id = u.state_id');
		$this->db->group_by("s.state_name");
		$query=$this->db->get();

		return $query->result();
	}
	function bloodgroupGraph()
	{
		$this->db->select('COUNT(user_id ) as total , LOWER(blood_group) as blood_group');
		$this->db->from('users');
		$this->db->group_by("LOWER(blood_group)");
		$this->db->where("blood_group != ''");
		$query=$this->db->get();

		return $query->result();
	}
	function agegroupGraph($age){

		$age_query = array();
		foreach($age as $value){
			if(substr($value,0,1) == '<'){
				$this->db->select("COUNT(user_id ) as total, LOWER('$value') as string");
				$this->db->from('users');
				$this->db->where('DATEDIFF(CURRENT_DATE, birthdate) < ('.substr($value,1).' * 365.25)');
				$query=$this->db->get();

				array_push($age_query,$query->result());

			}else if(substr($value,0,1) == '+'){

				$this->db->select("COUNT(user_id ) as total, LOWER('$value') as string");
				$this->db->from('users');
				$this->db->where('DATEDIFF(CURRENT_DATE, birthdate) > ('.substr($value,1).' * 365.25)');
				$query=$this->db->get();
				array_push($age_query,$query->result());

			}else{
				$values = explode("-",$value);
				$this->db->select("COUNT(user_id ) as total, LOWER('$value') as string");
				$this->db->from('users');
				$this->db->where('DATEDIFF(CURRENT_DATE, birthdate) >= ('.$values[0].' * 365.25) AND DATEDIFF(CURRENT_DATE, birthdate) <= ('.$values[1].' * 365.25)');
				$query=$this->db->get();
				array_push($age_query,$query->result());
			}
		}
		return $age_query;
	}


	//Graph Data

	function verify_username($data)
	{
		$data['username'] = trim($data['username']);
		$this->db->select("username");
		$this->db->from("users");
		$this->db->where("username",$data['username']);
		$checkUserId = $this->db->get()->num_rows();

		if($checkUserId > 0)
		{
				echo 'false';
		}else{
			echo "true";
		}
	}
	function generate_password( $length = 8 ) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}
	function getState_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('state');
		$this->db->where('country_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		$dd = "<option value=''>Select State</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->state_id.">".$row->state_name."</option>";
        }
		return $dd;
	}
	function getCity_drop($catId)
	{
		$this->db->select('*');
		$this->db->from('city');
		$this->db->where('state_id',$catId);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		//$dd = "<option value=''>Select City</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->city_id.">".$row->city_name."</option>";
        }
		return $dd;
	}
	function getUsers($catId){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}

	function add($data){
	    if($this->db->insert('users',$data)){
			return true;
		}else{
			return false;
		}
	}

	function updateUsers($data){
		$this->db->where('user_id',$data['user_id']);
		if($this->db->update('users',$data)){
			return true;
		}else{
			return false;
		}
	}
	function geteditstate($id)
	{
		$this->db->select('s.*');
		$this->db->from('state as s');
		$this->db->join('users as c','s.country_id = c.country_id', 'left');
		$this->db->where('c.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function geteditcity($id)
	{
		$this->db->select('cc.*');
		$this->db->from('city as cc');
		$this->db->join('users as c','c.state_id = cc.state_id', 'left');
		$this->db->where('c.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function deleteUsers($id)
	{
		$data['status']='Inactive';
		$this->db->where('user_id',$id);
 	  $this->db->update('users',$data);
	}
	function getUserInfo($id)
	{
		$this->db->select('d.department_name,ro.role_name,r.religion_name,cc.short_name,s.state_name,ci.city_name,u.*');
		$this->db->from('users as u');
		$this->db->join('country as cc','cc.country_id = u.country_id', 'left');
		$this->db->join('state as s','s.state_id = u.state_id', 'left');
		$this->db->join('city as ci','ci.city_id = u.city_id', 'left');
		$this->db->join('religion as r','r.religion_id = u.religion_id', 'left');
		$this->db->join('role as ro','ro.role_id = u.role_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('u.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}

	function fetchCredentials($id)
	{
		$this->db->select('u.username, u.password, u.email, u.first_name, u.last_name');
		$this->db->from('users as u');
		$this->db->where('u.user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}

	/* ************
	* EL update function for user
	* @param user_id , action of type Deduct / Putback
	*
	*
	******************** */
	function updateELRecords($user_id, $el_balance, $el_counter, $el_reason){
	 // get specific user whose EL details to be updated
	 $this->db->select('total_leaves, earned_leaves, el_balance');
	 $this->db->from('users');
	 $this->db->where('user_id',$user_id);
	 $query=$this->db->get()->result();
   //var_dump($query); die;
	 $running_balance = $query[0]->el_balance;
	 $running_total = $query[0]->total_leaves;
	 $running_EL = $query[0]->earned_leaves;
	 //var_dump($leaveType);
		$updated_balance = $el_balance;
		$updated_EL = $el_counter;

	 $data['el_balance'] = $updated_balance;
	 $data['earned_leaves'] = $updated_EL;
 //var_dump($data); die;
	 $this->db->where('user_id',$user_id);
	 if($this->db->update('users',$data)){
		 // log this
			auditUserActivity('EL', 'EL: from <b>'.$running_EL.'</b> to <b>'.$el_counter.'</b> EL Balance: from <b>'.$running_balance.' </b> to <b> '.$updated_balance.'</b><br/>Specified reason : '.$el_reason, $user_id, date('Y-m-d H:i:s'),0);
		 return true;
	 }else{
		 return false;
	 }

 die;
	}



	function getUserSkills($id)
	{
		$this->db->select('us.*,t.name');
		$this->db->from('crm_users_skill as us');
		$this->db->join('crm_skill as t','t.skill_id= us.skill_id', 'left');
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	function getDocument($id){
		$this->db->select('crm_users.first_name, crm_users.middle_name, crm_users.last_name, document.*');
		$this->db->from('crm_user_document as document');
		$this->db->join('crm_users','crm_users.user_id = document.user_id');
		if($id != '0'){
			$this->db->where('document.user_id',$id);
		}
		$this->db->group_by('document.document_id');
		$query = $this->db->get('crm_user_document');
		return $query->result();
	}
	function addDocument($data){
		$this->db->insert('crm_user_document',$data);
		return $insert_id = $this->db->insert_id();
	}
	function deleteDocument($id){
		$this->db->where('document_id',$id);
		$query = $this->db->delete('crm_user_document');
	}

}

?>
