<?php
class Visitorapp_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getVisitors()
	{
		$this->db->select('*');
		$this->db->from('visitor_app as v');
		$this->db->join('users as u','u.user_id = v.user_id', 'left');
		$this->db->order_by("created_at","desc");
		$query=$this->db->get();
		return $query->result();
	}

}
?>
