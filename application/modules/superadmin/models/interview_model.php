<?php
class Interview_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllInterview()
	{
		$this->db->select('crm_interview.interview_id, crm_interview.candidate_name,
		crm_interview.reference_name,crm_interview.app_receive_date,crm_interview.interview_date,
		crm_interview.interview_taken_by,crm_interview.qualification,crm_interview.current_company,
		crm_interview.current_position,crm_interview.applied_position,crm_interview.current_ctc,
		crm_interview.expected_ctc,crm_interview.total_experience,crm_interview.notice_period,
		crm_interview.current_location,crm_interview.contact_number,crm_interview.email,
		crm_interview.cat_id,crm_interview.resume,crm_interview.feedback,
		crm_interview.status,crm_interview.interview_created,crm_interview.interview_modified,
		crm_interview_category.cat_name from crm_interview');
		$this->db->join('crm_interview_category', 'crm_interview.cat_id = crm_interview_category.cat_id', 'left');
		$this->db->order_by("interview_modified","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function getAllInterviewCategory()
	{
		$this->db->select('* from crm_interview_category');
		$this->db->order_by("cat_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('crm_interview',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getInterview($catId){
		$this->db->select('*');
		$this->db->from('crm_interview');
		$this->db->where('interview_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}

	function getCandidateName($name){
		$this->db->select('*');
		$this->db->from('crm_req_applicants');
		$this->db->join('crm_req_designation', 'crm_req_designation.id = crm_req_applicants.requiredDesignation', 'left');
		$this->db->like('fullName', $name);
		$query=$this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	function update($data){
		$this->db->where('interview_id',$data['interview_id']);
		if($this->db->update('crm_interview',$data)){
			return true;
		}else{
			return false;
		}
	}

	function delete($id)
	{
		$this->db->where('interview_id',$id);
	    $this->db->delete('crm_interview');
	}

	// For register comment
	function getRecruitmentUserforComment($id)
	{
		$this->db->select('*');
		$this->db->from('crm_req_applicants_comment as r');		
		$this->db->join('crm_users as u','u.user_id = r.reviewer_id');			
		$this->db->where("r.req_applicants_id = $id");		
		$this->db->order_by("r.created_at","Desc");	
		$query=$this->db->get()->result();
		return $query;
	}
	function RecruitmentcommentSubmit($comment, $req_applicants_id){    
        $data = array(
        	    'req_applicants_id'=>$req_applicants_id,
				'comment'=>$comment,
				'reviewer_id' => '28'
        	    );        
        $query = $this->db->insert('crm_req_applicants_comment',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}

	/**
	* Developer Name : dipika patel
	* Date : 10-July-2019
	* Description : View Server Side Progressive Detail
	*
	* @return view
	*/
	function get_datatables()
	{

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();	
        return $query->result();
	}

	/**
	* Developer Name : dipika patel
	* Date : 10-July-2019
	* Description : View Server Side Progressive Detail
	*
	* @return view
	*/
	private function _get_datatables_query()
    {   
   
        //add custom filter here
        if($this->input->post('appliedStartDate') && $this->input->post('appliedEndDate'))
        {
			$from = date('Y-m-d',strtotime($this->input->post('appliedStartDate')));
		    $to = date('Y-m-d',strtotime($this->input->post('appliedEndDate'))); 
			$this->db->where("date(crm_interview.interview_date) between '$from' and '$to'");	
		}		
		
        if($this->input->post('filexpfrom') || $this->input->post('filexpto'))
        {  
		   $from = $this->input->post('filexpfrom');
		   $to = $this->input->post('filexpto'); 
		   $this->db->where("crm_interview.total_experience between '$from' and '$to'");	
		}

		if($this->input->post('designation'))
        {  
		   $this->db->where('crm_req_designation.designation', $this->input->post('designation'));	
		}

		if($this->input->post('candidate_name'))
        {  
		   $this->db->like('crm_interview.candidate_name', $this->input->post('candidate_name'));	
		}

		if($this->input->post('status') != '' && ($this->input->post('status') == 0 || $this->input->post('status') > 0))
        {  
		   $this->db->where('crm_interview.status', $this->input->post('status'));	
		}

		if($this->input->post('qualification'))
        {  
		   $this->db->like('crm_interview.qualification', $this->input->post('qualification'));	
		}

		if($this->input->post('applied_position'))
        {  
		   $this->db->like('crm_interview.applied_position', $this->input->post('applied_position'));	
		}

		if($this->input->post('current_ctc'))
        {  
		   $this->db->like('crm_interview.current_ctc', $this->input->post('current_ctc'));	
		}

		if($this->input->post('expected_ctc'))
        {  
		   $this->db->like('crm_interview.expected_ctc', $this->input->post('expected_ctc'));	
		}

		if($this->input->post('experience'))
        {  
		   $this->db->like('crm_interview.total_experience', $this->input->post('experience'));	
		}

		if($this->input->post('interview_date'))
        {  
		   $this->db->where('crm_interview.interview_date', $this->input->post('interview_date'));	
		}

		if($this->input->post('category'))
        {  
		   $this->db->like('crm_interview_category.cat_name', $this->input->post('category'));	
		}  

        if($this->input->post('feedback'))
        {  
		   $this->db->like('crm_interview.feedback', $this->input->post('feedback'));	
		}     

		
		$this->db->select('*');
		$this->db->from('crm_interview');
		$this->db->join('crm_interview_category','crm_interview_category.cat_id = crm_interview.cat_id', 'left');
		
		// $i = 0;
		
        // foreach ($this->column_search as $item) // loop column 
        // {
        //     if($_POST['search']['value']) // if datatable send POST for search
        //     {
             
		// 		if($i===0) // first loop
        //         {  
        //             $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
        //             $this->db->like('crm_interview'.$item, $_POST['search']['value']);
        //         }
        //         else
        //         {  
        //             $this->db->or_like('crm_interview'.$item, $_POST['search']['value']);
        //         }
 
        //         if(count($this->column_search) - 1 == $i) //last loop
		// 			$this->db->group_end(); //close bracket
					
		// 	}
				
        //     $i++;
		// }
         
        if(isset($_POST['order'])) // here order processing
        {
			$this->db->order_by('crm_interview.'.$this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        { 
			
			$order = $this->order;
            $this->db->order_by('crm_interview.'.key($order), $order[key($order)]);
		}
	}
	
	
	function count_all()
    {
        $this->db->from('crm_interview');
        return $this->db->count_all_results();
	}
	
	function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
}

?>
