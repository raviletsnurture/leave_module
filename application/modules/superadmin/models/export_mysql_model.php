<?php
class export_mysql_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllData(){		
		return $this->db->query('SHOW TABLES')->result();		
	}	
}

?>
