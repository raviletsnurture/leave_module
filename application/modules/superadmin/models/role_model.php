<?php
class Role_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllRole()
	{
		$this->db->select('*');
		$this->db->from('role');
		$this->db->order_by("role_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('role',$data))
		{
			$rdata['role_id'] = $this->db->insert_id();
			$rdata['module_add'] = 'no';
			$rdata['module_edit'] = 'no';
			$rdata['module_delete'] = 'no';
			$rdata['module_view'] = 'no';
			$rdata['module_approve'] = 'no';
			$rdata['module_kra'] = 'no';

			$arr = array('leaves','feedback');

			for($i=0;$i<sizeof($arr);$i++)
			{
				$rdata['module_name'] = $arr[$i];
				$this->db->insert('role_permission',$rdata);
				$rdata['module_name'] = '';
			}
			return true;
		}else{
			return false;
		}
	}
	function getRole($catId){
		$this->db->select('*');
		$this->db->from('role');
		$this->db->where('role_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateRole($data){

		$this->db->where('role_id',$data['role_id']);
		if($this->db->update('role',$data)){
			return true;
		}else{
			return false;
		}
	}
	function updatePermission($rdata)
	{
		$this->db->where('role_id',$rdata['role_id']);
		$this->db->where('module_name',$rdata['module_name']);
		if($this->db->update('role_permission',$rdata))
		{
			return true;
		}else{
			return false;
		}
	}
	function getPermission($id)
	{
		$this->db->select('*');
		$this->db->from('role_permission');
		$this->db->where('role_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function deleteRole($id)
	{
		$this->db->where('role_id',$id);
	    $this->db->delete('role');

		$dd = $this->db->where('role_id',$id);
	    $dd1 = $this->db->delete('role_permission');
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	17-07-2017
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : Give permission to the selected user
	 **/
	function givePermission($data,$user_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->update('users',$data);
		return true;
	}
	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	17-07-2017
	 * @Modified by	:
	 * @Status		  :
	 * @Comment		  : List of all departments in form of array
	 **/
	function getAllDepartment()
	{
		$this->db->select('d.*,COUNT(d.department_id) as departmentTotal');
		$this->db->from('department as d');
		$this->db->join('crm_users as u','u.department_id = d.department_id', 'right');
		$this->db->where('d.status','Active');
		$this->db->where('u.status','Active');
		$this->db->group_by("d.department_id");
		$this->db->order_by("departmentTotal", "DESC");
		$query=$this->db->get();
		return $query->result();
	}
	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	18-07-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : List of all department wise users
	 **/
	function getAllDepartmentUsers($deptID,$userID)
	{
		$this->db->select('first_name,last_name,user_id');
		$this->db->from('users');
		$this->db->join('department', 'users.department_id = department.department_id');
		$this->db->where('department.department_id',$deptID);
		$this->db->where('users.user_id != ',$userID);
		$this->db->where('users.status','Active');
		$this->db->order_by("first_name");
		$query=$this->db->get();
		return $query->result();
	}
	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	19-07-2017
	 * @Modified by	  :
	 * @Status		  :
		 * @Comment		  : List of all juniors for leave and feedback
	 **/
	function getUserPermission($id,$field)
	{
		$this->db->select($field);
		$this->db->from('users');
		$this->db->where('user_id',$id);
		$this->db->where($field.' is NOT NULL', NULL, FALSE);
		$this->db->where('users.status','Active');
		$query=$this->db->get();
		return $query->row();
	}

}

?>
