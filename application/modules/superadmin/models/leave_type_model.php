<?php
class Leave_Type_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	
	function getAllLeave_Type()
	{
		$this->db->select('*');
		$this->db->from('leave_type');
		$this->db->order_by("leave_type_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('leave_type',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getLeave_Type($catId){
		$this->db->select('*');
		$this->db->from('leave_type');
		$this->db->where('leave_type_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateLeave_Type($data){
		
		$this->db->where('leave_type_id',$data['leave_type_id']);
		if($this->db->update('leave_type',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function deleteLeave_Type($id)
	{
		$this->db->where('leave_type_id',$id);
	    $this->db->delete('leave_type');
	}
}

?>