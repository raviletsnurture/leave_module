<?php
class Leave_Status_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllLeave_Status()
	{
		$this->db->select('*');
		$this->db->from('leave_status');
		$this->db->order_by("leave_status_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('leave_status',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getLeave_Status($catId){
		$this->db->select('*');
		$this->db->from('leave_status');
		$this->db->where('leave_status_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateLeave_Status($data){
		$this->db->where('leave_status_id',$data['leave_status_id']);
		if($this->db->update('leave_status',$data)){
			return true;
		}else{
			return false;
		}
	}

	function deleteLeave_Status($id)
	{
		$this->db->where('leave_status_id',$id);
	    $this->db->delete('leave_status');
	}
}

?>
