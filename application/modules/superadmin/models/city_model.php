<?php
class City_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	
	function getAllCity()
	{
		$this->db->select('s.state_name,c.*');
		$this->db->from('city as c');
		$this->db->join('state as s','s.state_id = c.state_id', 'left');
		$this->db->order_by("city_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function add($data){
	    if($this->db->insert('city',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getState($catId){
		$this->db->select('s.*');
		$this->db->from('state as s');
		$this->db->join('city as c','s.country_id = c.country_id', 'left');
		
		$query=$this->db->get();
		return $query->result();
	}
	function getCity($catId){
		$this->db->select('*');
		$this->db->from('city');
		$this->db->where('city_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}
	function updateCity($data){
		
		$this->db->where('city_id',$data['city_id']);
		if($this->db->update('city',$data)){
			return true;
		}else{
			return false;
		}
	}
	function getCity_drop($data)
	{
		$this->db->select('*');
		$this->db->from('state');
		$this->db->where('country_id',$data);
		$query=$this->db->get();
		$ff = $query->result();
		$dd = '';
		//$dd = "<option value=''>Select Milestone</option>";
		foreach($ff as $row)
		{
          $dd .= "<option value=".$row->state_id.">".$row->state_name."</option>";
        }
		return $dd;
	}
	function deleteCity($id)
	{
		$this->db->where('city_id',$id);
	    $this->db->delete('city');
	}
}

?>