<?php
class Reimbursement_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
    }
    
    function getAllRequest(){
		$this->db->select('rr.status, u.first_name, u.last_name, rr.reimbursement_id, rr.user_id, rr.dept_id, rr.role_id, rr.datetime, rr.amount, rr.reciept,  rr.description, rr.payment_mode, rr.seniorApproval, rr.approvedBy, rr.created_at, rr.updated_at');
		$this->db->from('crm_reimbursement_request as rr');
		$this->db->join('crm_users as u','rr.user_id = u.user_id', 'inner');
		$this->db->where('u.status = "Active"');
		$this->db->order_by("rr.created_at","desc");
		$query = $this->db->get()->result_array();
		return $query;
	}

	function updateStatus($data){
		$this->db->set('status',$data['status']);
		$this->db->where('reimbursement_id',$data['request_id']);
		$this->db->where('user_id',$data['user_id']);
		if($this->db->update('crm_reimbursement_request'))
		{
				return true;
			}
			else
			{
				return false;
		}
	}
}

?>