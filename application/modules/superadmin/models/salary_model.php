<?php
class Salary_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllSalaryUploads($user_id = NULL)
	{
		$this->db->select('u.first_name, u.last_name, d.department_name, l.*');
		$this->db->from('salary as l');
		$this->db->where('filename != ""');
		$this->db->join('users as u','u.user_id = l.user_id', 'right');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('u.status = "Active"');
		if($user_id != NULL){
			$this->db->where('l.user_id',$user_id);
		}
		$this->db->order_by("l.year","desc");
		$this->db->order_by("l.month","desc");
		$this->db->order_by("l.upload_id","desc");
		$query=$this->db->get();
		return $query->result();
	}

	function add($data){
		// FIXED : SAme user same year same month not to be uploaded but updated
		$paramQuery = array('user_id'=>$data["user_id"], 'year'=>$data["year"], 'month'=>$data["month"]);
		$this->db->select('*');
		$this->db->from('salary');
		$this->db->where($paramQuery);
		$query=$this->db->get();
		$result = $query->row();
		if($result){
			 $this->db->where('upload_id',$result->upload_id);
			 if($this->db->update('salary',$data)){
				 return true;
			 }else{
				 return false;
			 }
		}else{
				if($this->db->insert('salary',$data)){
				return true;
					}else{
				return false;
			}
		}
	}

	function getSalary($catId){
		$this->db->select('*');
		$this->db->from('salary');
		$this->db->where('upload_id',$catId);
		$query=$this->db->get();
		return $query->result();
	}

	function deleteSalary($id){
	  	$this->db->where('upload_id',$id);
	    $this->db->delete('salary');
	}

	//krunal
	function addBulk($data){
			// FIXED : SAme user same year same month not to be uploaded but updated
			$paramQuery = array('user_id'=>$data["user_id"], 'year'=>$data["year"], 'month'=>$data["month"]);
			$this->db->select('*');
			$this->db->from('salary');
			$this->db->where($paramQuery);
			$query=$this->db->get();
			$result = $query->row();
			if($result){
				 $this->db->where('upload_id',$result->upload_id);
				 if($this->db->update('salary',$data)){
					 return true;
				 }else{
					 return false;
				 }
			}else{
			    if($this->db->insert('salary',$data)){
					return true;
						}else{
					return false;
				}
			}
	}

	function getAllUsersWithInactive()
	{
		$CI = & get_instance();
		$query = $CI->db->select('*');
		$CI->db->order_by("first_name","asc");
		$query = $CI->db->get('users');
		return $query->result();
	}

	function getUserSalarySlip($start_date,$end_date,$user_id)
	{
		$paramQuery = array('user_id'=>$user_id);
		$this->db->select('*');
		$this->db->from('salary');
		$this->db->where($paramQuery);
		$this->db->where('uploaded_at BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
		$query=$this->db->get();
		return $query->result();
	}

}

?>
