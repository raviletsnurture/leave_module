<?php
class Superadmin_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}
	
	public function login($data)
	{
		$this->db->select('*');
		$this->db->from('super_admin');
		$this->db->where('sUsername',$data['sUsername']);
		$this->db->where('sPassword',md5($data['sPassword']));
		//$this->db->where('tStatus',"1");
		$this->db->limit(1);
		$query=$this->db->get();
		return $query->result();
	}	
}

?>