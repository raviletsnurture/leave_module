<?php
class Skill_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllSkill(){
		$this->db->select('*');
		$this->db->from('crm_skill');
		$this->db->order_by("skill_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function addSkill($data){
		$this->db->select('*');
		$this->db->from('crm_skill');
		$this->db->where("name like '".$data['name']."'");
		$query=$this->db->get();
		$result = $query->row();
		if(empty($result)){
	    if($this->db->insert('crm_skill',$data)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function deleteSkill($skill_id){
		//delet skill
		$this->db->where('skill_id',$skill_id);
    $this->db->delete('crm_skill');
		
		//delet user skill
		$this->db->where('skill_id',$skill_id);
    $this->db->delete('users_skill');
	}
}

?>
