<?php
class policies_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}


	function getAllPolicies()
	{
		$this->db->select("*");
		$this->db->from("crm_policies");
		$this->db->order_by("policiesId","ASC");
		$result = $this->db->get()->result_array();
		return $result;
	}

	function add($data){
    if($this->db->insert('crm_policies',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getPolicy($policiesId){
		$this->db->select('*');
		$this->db->from('crm_policies');
		$this->db->where('policiesId',$policiesId);
		$query=$this->db->get();
		return $query->result();
	}

	function updatePolicies($data){
		$this->db->where('policiesId',$data['policiesId']);
		if($this->db->update('crm_policies',$data)){
			return true;
		}else{
			return false;
		}
	}

	function deletePolicy($policiesId){
		$this->db->where('policiesId',$policiesId);
    $this->db->delete('crm_policies');
	}

}
?>
