<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class State extends MX_Controller {  
	function __construct(){
		parent::__construct();		
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('state_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - State';
		
		$data["state"] = $this->state_model->getAllState();
		
	   	$this->template->load_partial('superadmin_master','state/list_state',$data);
	}
	function add($catId = ''){
		
		$data['titalTag'] = ' - Add State';
		if($this->input->post("addEditDepart") !== false)
		{
				$addData['state_name'] = $this->input->post('state_name');
				$addData['country_id'] = $this->input->post('country_id');
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['state_id']= $catId;
					$data = $this->state_model->updateState($addData);
					$this->session->set_flashdata('success','State updated successfully!');
				}else{
					
					$data = $this->state_model->add($addData);
					$this->session->set_flashdata('success','State Added successfully!');
				}
				redirect(base_url().'superadmin/state');exit;		
		}
		
		
	   	$this->template->load_partial('superadmin_master','state/add_edit_state',$data);
	}
	public function edit($catId)
	{
		$data['titalTag'] = ' - Edit State';
		$data['stateEdit'] = $this->state_model->getState($catId);
		$this->template->load_partial('superadmin_master','state/add_edit_state',$data);
	}
	public function delete($catId)
	{
		$this->state_model->deleteState($catId);	
		$this->session->set_flashdata('success','State deleted successfully!');
		redirect(base_url().'superadmin/state');
		exit;
	}
}

?>