<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Interview extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->helper('general_helper', 'form', 'url');
		$this->load->model('interview_model');
		$this->load->model('users_model');
		$this->load->library('email');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Interview';

		$data["interview"] = $this->interview_model->getAllInterview();

	   	$this->template->load_partial('superadmin_master','interview/list_interview',$data);
	}
	function add($catId = ''){        	
		$data['titalTag'] = ' - Add Interview';
		if($this->input->post("addEditInterview") !== false)
		{
			$addData['candidate_name'] = $this->input->post('candidate_name');
			$addData['reference_name'] = $this->input->post('reference_name');
			$addData['app_receive_date'] = date("Y-m-d H:i", strtotime($this->input->post('app_receive_date')));
			$addData['interview_date'] = date("Y-m-d H:i", strtotime($this->input->post('interview_date')));
			$addData['interview_taken_by'] = $this->input->post('interview_taken_by');
			$addData['cat_id'] = $this->input->post('cat_id');
			$addData['qualification'] = $this->input->post('qualification');
			$addData['current_company'] = $this->input->post('current_company');
			$addData['current_position'] = $this->input->post('current_position');
			$addData['applied_position'] = $this->input->post('applied_position');
			$addData['current_ctc'] = $this->input->post('current_ctc');
			$addData['expected_ctc'] = $this->input->post('expected_ctc');
			$addData['total_experience'] = $this->input->post('total_experience');
			$addData['notice_period'] = $this->input->post('notice_period');
			$addData['current_location'] = $this->input->post('current_location');
			$addData['contact_number'] = $this->input->post('contact_number');
			$addData['email'] = $this->input->post('email');
			$addData['feedback'] = $this->input->post('feedback', TRUE);
			$addData['status'] = $this->input->post('status');



			if(!empty($_FILES['resume']['name'])) {
				$config = array();
				$config['upload_path']   = FCPATH . 'uploads/resumes';
				$config['allowed_types'] = 'pdf|doc|docx';
				$config['max_size']      = 10000;
				$config['file_name'] = time() .'_'. rand(100,100000) .'_Resume.'. substr(strrchr($_FILES['resume']['name'],'.'),1); //$addData['candidate_name'] . '_Resume.' . substr(strrchr($_FILES['resume']['name'],'.'),1);
				$this->load->library('upload', $config);

				$uploaded = $this->upload->do_upload('resume');
				$upload_data = $this->upload->data();
				$addData['resume'] = $upload_data['file_name'];
			}

			if(is_numeric($catId) && $catId != ''){
				 /*when update*/
				$addData['interview_id']= $catId;
				$addData['interview_modified'] = date('Y-m-d H:i:s');
				$data = $this->interview_model->update($addData);
				$this->session->set_flashdata('success','Interview updated successfully!');
			}else{
				$addData['interview_modified'] = date('Y-m-d H:i:s');
				$addData['interview_created'] = date('Y-m-d H:i:s');
				$data = $this->interview_model->add($addData);
				$this->session->set_flashdata('success','Interview Added successfully!');
			}

			$interviewTakenDetail = $this->users_model->getUsers($this->input->post('interview_taken_by'));
			$mailData['body'] = '<tr>
										<td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;">Interview schedule with you please check resume and timing in hrms of <strong style="color:#03A9F4;">'.$this->input->post('candidate_name').'</strong>  <br/>Applied Position <strong style="color:#03A9F4;">'.$this->input->post('applied_position').'</strong>  <br/>Total Experience <strong style="color:#03A9F4;">'.$this->input->post('total_experience').'Year </strong>  <br/>Interview Date <strong style="color:#03A9F4;">'.$this->input->post('interview_date').'</strong></p></td>
										</tr>';
			$mailData['name'] = $interviewTakenDetail[0]->first_name;
			$this->email->from('hrd@letsnurture.com', "HRMS");
			$this->email->to('hrd@letsnurture.com');
			$this->email->bcc($interviewTakenDetail[0]->email);
			$this->email->subject("Interview schedule on ".$this->input->post('interview_date'));
			$body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
			$this->email->message($body);
			$this->email->send();

			redirect(base_url().'superadmin/list_interview');exit;
		}

	   	$this->template->load_partial('superadmin_master','interview/add_edit_interview',$data);
	}
	public function edit($catId)
	{
		$data['titalTag'] = ' - Interview';
		$data['interviewEdit'] = $this->interview_model->getInterview($catId);
		$this->template->load_partial('superadmin_master','interview/add_edit_interview',$data);
	}
	public function updatestaus()
	{
		$data['leave_id'] = $this->input->post('leave_id');
		$data['status'] = $this->input->post('status');

		$this->leave_model->updateLeave($data);

		if($this->input->post('status') == 'Paid')
		{
			$data['user_id'] = $this->input->post('user_id');
			$up = $this->leave_model->updateleavecount($data);
		}
	}
	public function up_staus()
	{
		$data['leave_id'] = $this->input->post('leave_id');
		$data['leave_status'] = $this->input->post('status');
		$this->leave_model->updateLeave($data);
	}
	public function delete($catId)
	{
		$this->interview_model->delete($catId);

		$this->session->set_flashdata('success','Leave deleted successfully!');
		redirect(base_url().'superadmin/interview');
		exit;
	}

	public function user_list()
	{
		//echo $this->input->get('keyword');
		echo json_encode($this->interview_model->getCandidateName($this->input->get('keyword')));
	}
	public function putComment()
	{
		$userId = $this->input->post('id');
		$data['userId'] = $userId;
		$data['userData'] = $this->interview_model->getRecruitmentUserforComment($userId);
		$this->load->view('superadmin/interview/comment_user', $data);
	}
	function recruitmentComment(){
        $id = $this->input->post('applicant_id');
		$comment = $this->input->post('comment');
 		if($this->interview_model->RecruitmentcommentSubmit($comment,$id))
		{
			echo 1;
		}else{
			echo 0;
		}
	}


	//  For Serverside pagination

    /**
	* Developer Name : dipika patel
	* Date : 19-July-2019
	* Description : View Server Side Progressive Main
	*
	* @return view
	*/
	function interviews()
	{

		$data['titalTag'] = '- Recruitment';
		$opt = array('' => 'All Designation');
		$opt['0'] = 'Selected';
		$opt['1'] = 'Selected but not confirmed';
		$opt['2'] = 'Rejected';
		$opt['3'] = 'Discussion Required';
		$opt['4'] = 'Other';
		$data['designationCountArray'] = form_dropdown('',$opt,'','id="status" class="form-control"');
		$this->template->load_partial('superadmin_master','interview/interview',$data);
	}

	/**
	* Developer Name : dipika patel
	* Date : 19-July-2019
	* Description : View Server Side Progressive Detail
	*
	* @return view
	*/
	public function ajax_list()
    {
		$list = $this->interview_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {
			//resume
			$resume = '<a href="'.base_url().'uploads/resumes/'.$customers->resume.'" target="_blank">Resume</a>';
            //feedback
			$out = strlen($customers->feedback) > 50 ? substr($customers->feedback,0,50)."..." : $customers->feedback;
			$feedback =  '<span title="'.html_escape($customers->feedback).'">'.html_escape($out).'</span>';
			//Action
			$action = '<a href="'.base_url().'superadmin/interview/edit/'.$customers->interview_id.'" >
                  <button class="btn btn-primary btn-xs" title=""><i class="fa fa-pencil"></i></button>
                  </a>
                  <a href="'.base_url().'superadmin/interview/delete/'.$customers->interview_id.'" class="deleteRec">
                  <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                  </a>
                  <a value="'.$customers->interview_id.'"  href="#" onclick="putComment('.$customers->interview_id.')">
                      <button class="btn btn-primary btn-xs tooltips" data-original-title="Comment" title=""><i class="fa fa-comment"></i></button>
                  </a>';

            $no++;
            $row = array();
			$row[] = $customers->candidate_name;
			$row[] = $customers->qualification;
            $row[] = $customers->applied_position;
            $row[] = $customers->current_ctc;
            $row[] = $customers->expected_ctc;
            $row[] = $customers->total_experience;
			$row[] = $resume;
			$row[] = date("Y/m/d",strtotime($customers->interview_date));
			$row[] = $customers->cat_name;
			$row[] = $feedback;
			$row[] = $action;
            $data[] = $row;
        }

        $output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->interview_model->count_all(),
					"recordsFiltered" => $this->interview_model->count_filtered(),
					"data" => $data
                );
        echo json_encode($output);
    }
}

?>
