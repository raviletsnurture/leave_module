<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('department_model');
		sAdmin_auth('0');
	}

	function index(){
		$data['titalTag'] = ' - Department';

		$data["department"] = $this->department_model->getAllDepartment();

	   	$this->template->load_partial('superadmin_master','department/list_department',$data);
	}

	function add($catId = '')
	{
		$data['titalTag'] = ' - Department add';
		if($this->input->post("addEditDepart") !== false)
		{
				$SeniorsArr = $this->input->post('seniors');
				if(!empty($SeniorsArr)){
					$Seniors = implode(',', $SeniorsArr);
				}else{
					$Seniors = '';
				}

				$addData['department_name'] = $this->input->post('department_name');
				$addData['seniors'] = $Seniors;
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['department_id']= $catId;
					$data = $this->department_model->updateDepartment($addData);
					$this->session->set_flashdata('success','Your Department updated successfully!');
				}else{
					$data = $this->department_model->add($addData);
					$this->session->set_flashdata('success','Department Added successfully!');
				}
				redirect(base_url().'superadmin/department');exit;
		}
		if(is_numeric($catId) && $catId != ''){/* when edit*/
		$data['departmentEdit'] = $this->department_model->getDepartment($catId);
			if(count($data['departmentEdit']) == 0){
				redirect(base_url().'superadmin/department');exit;
			}
		$data['titalTag'] = ' - Department Edit';
		$data['actionUrl'] = 'superadmin/department/add/'.$catId;
		}
	   	$this->template->load_partial('superadmin_master','department/add_edit_department',$data);
	}


	public function edit($catId)
	{
		$data['departmentEdit'] = $this->department_model->getDepartment($catId);
		$this->template->load_partial('superadmin_master','department/add_edit_department',$data);
	}

	public function delete($catId)
	{
		$this->department_model->deleteDepartment($catId);

		$this->session->set_flashdata('success','Department deleted successfully!');
		redirect(base_url().'superadmin/department');
		exit;
	}
}

?>
