<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class export_mysql extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('export_mysql_model','export');
		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->dbforge();
		sAdmin_auth('0');
		$this->load->library('upload');
	}
	function index(){
		$data['titalTag'] = 'Export';
		if(isset($_POST['export_submit'])){
			$instance = &get_instance();
			$instance->load->database();
			$hostname = $instance->db->hostname;
			$username = $instance->db->username;
			$password = $instance->db->password;
			$database = $instance->db->database;

		    header("Content-Type: text/plain");		
			include APPPATH.'third_party/MX/exportdb/mysql_dump.php';
			$file = APPPATH.'third_party/MX/exportdb/dump.sql';	 	
		    if(mysql_dump($file, $hostname, $database, $username, $password))
		    {
				if (file_exists($file))
			    {
				    header('Content-Description: File Transfer');
				    header('Content-Type: application/octet-stream');
				    header('Content-Disposition: attachment; filename='.basename($file));
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate');
				    header('Pragma: public');
				    header('Content-Length: ' . filesize($file));
				    ob_clean();
				    flush();
				    readfile($file);
				    exit;
			    }
				
				$this->session->set_flashdata('success','Exported data successfully');	
			}
			else
			{
				
				$this->session->set_flashdata('error','File is not found');
			}
		}
	    $this->template->load_partial('superadmin_master','export/export_db',$data);		
	}	
	
	function import(){
		$data['titalTag'] = 'Import';		
		/*$Tabledata = $this->export->getAllData();	
		foreach ($Tabledata as $key => $value) {						
			foreach ($value as $table) {
					$this->db->query('SET foreign_key_checks = 0');
				    $this->db->query('DROP TABLE '.$table);	
			}
		} */
            if(isset($_POST)){  
				$uploads_dir = './exportDatabase/';	
	            $temp = explode(".", $_FILES["database_import"]["name"]);
				$newfilename = date('Y-m-d').'-'.round(microtime(true)).'.'.end($temp);
				move_uploaded_file($_FILES["database_import"]["tmp_name"], "$uploads_dir/$newfilename");
				$file_path = FCPATH.'exportDatabase/'.$newfilename;
				if(is_file($file_path))
				{  
				    chmod($file_path, 0777); 
				    include APPPATH.'third_party/MX/exportdb/mysql_import.php';			
					if(mysql_import($file_path, 'localhost', 'letsnurt_hrmsdev', 'root', 'ln'))
					{
						$this->session->set_flashdata('success','Imported data successfully');	
						@unlink(FCPATH.'exportDatabase/'.$newfilename);
					}
					else
					{
						
						$this->session->set_flashdata('success','Something went wrong.');	
					}
				}else{
					   
					   $this->session->set_flashdata('error','File is not found');
				}	
			} 
			
			$this->template->load_partial('superadmin_master','export/export_db',$data);	
	    }
}
?>