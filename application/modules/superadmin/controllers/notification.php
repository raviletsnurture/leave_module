<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->template->set('controller', $this);
        $this->load->database();
        $this->logSession = $this->session->userdata("login_session");
        $this->load->helper('download');
        $this->load->helper('general_helper');
	      $this->load->library('email');
        $this->load->model('notification_model');
        sAdmin_auth('0');
    }

    function index()
    {
        $data['titalTag'] = ' - Notification';
        $data["announcements"] = []; // $this->notification_model->getAllAnnouncements();
        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'notification/add_notification', $data);
    }

	function addNotification(){
      if ($this->input->post('description') != '' && $this->input->post('description') != '') {
			$data['description']        = trim($this->input->post('description'));
			$data['web']   = trim($this->input->post('web'));
      $data['android']   = trim($this->input->post('android'));
      $data['ios']   = trim($this->input->post('ios'));
			$data['published_status']   = trim($this->input->post('published_status'));

      //print_r($data);

			$this->session->set_flashdata('success', 'Notification sent successfully!');

      //Push Notification Logic

      if($data['web'] == 1)
      {
        $deviceTokens = $this->notification_model->getAllDeviceTokenbyType(0);
        $Tokenlist = array();
        foreach($deviceTokens as $value){
          array_push($Tokenlist,$value->deviceToken);
        }
        //SendNotificationFCMWeb('HRMS - Notification ', $data['description'],$Tokenlist);
      }
      if($data['android'] == 1)
      {
        $deviceTokens = $this->notification_model->getAllDeviceTokenbyType(1);
        $Tokenlist = array();
        foreach($deviceTokens as $value){
          array_push($Tokenlist,$value->deviceToken);

        }
        //array_push($Tokenlist,'d1Rq8tii9cw:APA91bGu2Iw8mBdz2r5iRN8EvpOSF6XDQPGNQwU4aaRRulQblW5FE0zlT6nK9nBd95NkVgvYDEd1E0YhOksws84tGB4U-SGK60mM1KPm3qb5VWi4p9SSbhN8mP9soGhwIJLbeaaUk9FV');
        //print_r($Tokenlist);
        SendNotificationFCMWeb('HRMS - Notification ', $data['description'],$Tokenlist);
      }
      if($data['ios'] == 1)
      {
        $deviceTokens = $this->notification_model->getAllDeviceTokenbyType(2);
        $Tokenlist = array();
        foreach($deviceTokens as $value){
          array_push($Tokenlist,$value->deviceToken);
        }
        array_push($Tokenlist,'cFPsD7XN2g8:APA91bEGJZ3xjljmILTuMe_LYKNCgJqUCIzfoze7i1MnBOgWSe8LjLcr2JTooi62Txf5q4A_XLj0RkXmyLpthLhZPBVNQM_fPvnD7cyCoBzbGVXy3Qd995sVVzEW2THwQIhxxaLffqJu');
        SendNotificationFCMWeb('HRMS - Notification ', $data['description'],$Tokenlist);
      }

      //Push Notification Logic
			//redirect(base_url() . 'superadmin/notification');
			exit;
        }
    }

    public function delete($annId){
  		$this->notification_model->deleteAnnouncement($annId);
  		$this->session->set_flashdata('success', 'Announcement deleted successfully!');
  		redirect(base_url().'superadmin/notification');
  		exit;
  	}

}
?>
