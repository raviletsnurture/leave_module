<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Skill extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('skill_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Skill';
		$data["skills"] = $this->skill_model->getAllSkill();
   	$this->template->load_partial('superadmin_master','skill/skill',$data);
	}
	function addSkill(){
		if($_POST['skill'] != ''){
			$data['name'] = $_POST['skill'];
			$data = $this->skill_model->addSkill($data);
			if($data == 1){
				echo "1";
			}else {
				echo "2";
			}
		}else {
			echo "0";
		}
	}

	public function deleteSkill(){
		if($_POST['skill_id'] !== false){
			$data = $this->skill_model->deleteSkill($_POST['skill_id']);
		}
	}
}

?>
