<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_Status extends MX_Controller {  
	function __construct(){
		parent::__construct();		
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('leave_status_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Leave Status';
		
		$data["leave_status"] = $this->leave_status_model->getAllLeave_Status();
		
	   	$this->template->load_partial('superadmin_master','leave_status/list_leave_status',$data);
	}
	function add($catId = ''){
		
		$data['titalTag'] = ' - Leave Status';
		if($this->input->post("addEditLeaveStatus") !== false)
		{
				$addData['leave_status'] = $this->input->post('leave_status');
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['leave_status_id']= $catId;
					$data = $this->leave_status_model->updateLeave_Status($addData);
					$this->session->set_flashdata('success','Leave Status updated successfully!');
				}else{
					
					$data = $this->leave_status_model->add($addData);
					$this->session->set_flashdata('success','Leave Status Added successfully!');
				}
				redirect(base_url().'superadmin/leave_status');exit;		
		}
		
	   	$this->template->load_partial('superadmin_master','leave_status/add_edit_leave_status',$data);
	}
	public function edit($catId)
	{
		$data['leave_statusEdit'] = $this->leave_status_model->getLeave_Status($catId);
		$this->template->load_partial('superadmin_master','leave_status/add_edit_leave_status',$data);
	}
	public function statusdefault()
	{
		//echo $_POST['id'];
		$data['status_default'] = '0';
		if($this->db->update('leave_status',$data))
		{
			$data['status_default'] = '1';
			$this->db->where('leave_status_id',$_POST['id']);
			if($this->db->update('leave_status',$data))
			{
			return true;
			}
		}
		else
		{
			return false;
		}
		exit;
		
	}
	public function delete($catId)
	{
		$this->leave_status_model->deleteLeave_Status($catId);	
		$this->session->set_flashdata('success','Leave Status deleted successfully!');
		redirect(base_url().'superadmin/leave_status');
		exit;
	}
}

?>