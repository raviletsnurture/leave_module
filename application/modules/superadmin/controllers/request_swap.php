<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_swap extends MX_Controller{
    function __construct()
    {
        parent::__construct();

        $this->template->set('controller', $this);
        $this->load->database();
        $this->logSession = $this->session->userdata("login_session");
        $this->load->helper('download');
        $this->load->helper('general_helper');
	    $this->load->library('email');
        $this->load->model('request_swap_model');
        sAdmin_auth('0');
    }
    function index(){
        $data['titalTag'] = ' - Swapping Requests';
        $data["getAllSwappingLeaves"] = $this->request_swap_model->getAllSwappingLeaves();         
        $this->template->load_partial('superadmin_master','request_swap/list', $data);
    }
}

?>