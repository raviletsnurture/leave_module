<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends MX_Controller {  
	function __construct(){
		parent::__construct();		
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('country_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Country';
		
		$data["country"] = $this->country_model->getAllCountry();
		
	   	$this->template->load_partial('superadmin_master','country/list_country',$data);
	}
	function add($catId = ''){
		
		$data['titalTag'] = ' - Add Country';
		if($this->input->post("addEditCountry") !== false)
		{
				$addData['short_name'] = $this->input->post('short_name');
				
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['country_id']= $catId;
					$data = $this->country_model->updateCountry($addData);
					$this->session->set_flashdata('success','Country updated successfully!');
				}else{
					
					$data = $this->country_model->add($addData);
					$this->session->set_flashdata('success','Country Added successfully!');
				}
				redirect(base_url().'superadmin/country');exit;		
		}
		
		
	   	$this->template->load_partial('superadmin_master','country/add_edit_country',$data);
	}
	public function edit($catId)
	{
		$data['countryEdit'] = $this->country_model->getCountry($catId);
		$this->template->load_partial('superadmin_master','country/add_edit_country',$data);
	}
	public function delete($catId)
	{
		$this->country_model->deleteCountry($catId);	
		$this->session->set_flashdata('success','Country deleted successfully!');
		redirect(base_url().'superadmin/country');
		exit;
	}
}

?>