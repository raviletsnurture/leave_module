<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timelog extends MX_Controller {
  var $logSession;
	function __construct(){
    parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
    $this->load->helper('download');
		$this->load->helper('general_helper');
		$this->load->model('timelog_model');
    sAdmin_auth('0');
	}

	function index(){
		$data['titalTag'] = ' - Time Log';
    $this->template->load_partial('superadmin_master','timelog/index',$data);
	}

  function getLogByDate(){
		$id = $_POST['selectUser'];
    if($id){
      $firstDate = $_POST['start_date'];
      $lastDate = $_POST['end_date'];
  		$timelogs = $this->timelog_model->getAllTimelog($id,$firstDate,$lastDate);
      if($timelogs){
        $i=1; foreach ($timelogs as $timelog) { ?>
          <?php if($i == 1){ ?>
            <div class="row">
          <?php }?>
         <div class="col-sm-4">
               <section class="panel">
                 <div class="panel-body progress-panel">
                     <div class="task-progress">
                         <h1>Time Log -  <?php echo date("d F, Y",strtotime($timelog['log_date']));?></h1>
                     </div>
                 </div>
                     <table class="table table-striped logTable">
                         <thead>
                         <tr>
                             <th><i class="fa fa-bookmark"></i> Door</th>
                             <th><i class="fa fa-bookmark"></i> IO Type</th>
                             <th><i class=" fa fa-edit"></i> Time</th>
                         </tr>
                         </thead>
                           <tbody>
                               <?php
                               $employ_logs = $timelog['employ_log'];
                               $employ_logs = json_decode($employ_logs);
                               foreach($employ_logs as $employ_log){ ?>
                                   <tr>
                                       <td><?php echo $employ_log->device_name;?></td>
                                       <td><?php if(($employ_log->device_name == 'MAIN DOOR' && $employ_log->EntryExitType == '1') || ($employ_log->device_name == 'RECREATIONAL AREA' && $employ_log->EntryExitType == '1')){echo "OUT";}else{echo "IN";}?></td>
                                       <td><span class="label label-<?php if(($employ_log->device_name == 'MAIN DOOR' && $employ_log->EntryExitType == '1') || ($employ_log->device_name == 'RECREATIONAL AREA' && $employ_log->EntryExitType == '1')){echo "danger";}else{echo "success";}?> label-mini"><?php echo date('H:i', strtotime($employ_log->ETime));?></span></td>
                                   </tr>
                               <?php } ?>
                           </tbody>
                       </table>
                               <div class="panel-body">
                                    <div class="alert alert-success">
                                        Gross Work Hours: <?php echo date('H:i', strtotime($timelog['gross_work_hours'])); ?>
                                    </div>
                                    <div class="alert alert-danger">
                                        Total Out Time: <?php echo date('H:i', strtotime($timelog['total_out_time'])); ?><br/>
                                        <small>(Including Recreational Area)</small>
                                    </div>
                                    <div class="alert alert-info">
                                        N-Punch Work Hours: <?php echo date('H:i', strtotime($timelog['nPunch_work_hours'])); ?>
                                    </div>
                                    <div class="alert alert-warning">
                                        Extra Work Hours: <?php echo date('H:i', strtotime($timelog['extra_work_hours'])); ?>
                                    </div>
                               </div>
                           </section>
                         </div>
          <?php if($i == 3){ ?>
            </div>
          <?php $i=0;}?>
      <?php $i++; }
      }else{
        echo "0";
      }
    }else{
        echo "1";
    }
	}

  function getTimeLogTotal(){
    $id = $_POST['selectUser'];
    if($id){
        $firstDate = $_POST['start_date'];
        $lastDate = $_POST['end_date'];
        $totalHours = $this->timelog_model->getAllTotalTimelog($id,$firstDate,$lastDate);
        if($totalHours){
          $response['totalTime'] = @(str_replace(".",":", (string) round($totalHours->total,2)) * 100 / 185);
          $response['totalTimeHours'] = @(str_replace(".",":", (string) round($totalHours->total,2)));
          echo json_encode($response);
        }else{
          $response['totalTime'] = 0;
          $response['totalTimeHours'] = 0;
          echo json_encode($response);
        }
    }else{
      $response['totalTime'] = 0;
      $response['totalTimeHours'] = 0;
      echo json_encode($response);
    }
  }

  /**
  * Developed By : Parth patwala
  * Date : 29-May-2017
  * Detail : Get leave records between dates
  */
  function getLeaveDetailsOnAjax(){

      $id = $_POST['selectUser'];
      $firstDate = $_POST['start_date'];
      $lastDate = $_POST['end_date'];

      $leaveTaken = $this->timelog_model->getLeaveTakenInTimelog($id,$firstDate,$lastDate);

      $leaveHtml = '';
      if(!empty($leaveTaken)){
          foreach($leaveTaken as $leaveDetails) {

          $start_date = new DateTime($leaveDetails->leave_start_date);
          $end_date = new DateTime($leaveDetails->leave_end_date);

          if((int)$leaveDetails->days > 1){
              $date_diff_text = '('.(int)$leaveDetails->days.' days)';
          }else{
              $date_diff_text = '('.(int)$leaveDetails->days.' day)';
          }

          $leaveHtml .= '<p><span>'.$leaveDetails->leave_name.' '.$date_diff_text.'</span> On
              <em style="font-size:14px; color: #c3c3c3;">'.$start_date->format('dS, M Y').' - '.$end_date->format('dS, M Y').' ('.date_format(date_create($leaveDetails->YR_MTH),'M').' Month)</em></p>';
          }
      }else{
          $leaveHtml = '<p> No leaves Found</p>';
      }
      echo json_encode(array('leaveData' => $leaveHtml), JSON_UNESCAPED_SLASHES);
  }

  /**
  * Developed By : Jignasa
  * Date : 14-07-2017
  * Detail : Get late entry records between dates
  */
  function getLateEntryDetailsOnAjax(){
      $id = $_POST['selectUser'];
      $firstDate = $_POST['start_date'];
      $lastDate = $_POST['end_date'];

      $lateEntryCount = $this->timelog_model->getLateEntryInTimelog($id,$firstDate,$lastDate);
      if(!empty($lateEntryCount) ){
        $count = $lateEntryCount[0]->total;
      }else{
        $count =  0;
      }
    echo json_encode(array('lateEntryData' => $count), JSON_UNESCAPED_SLASHES);
  }

  function addTimeLog(){
     //print_r($this->input->post); exit;
		$data['titalTag'] = ' - Upload Time Log';
    if(!empty($_FILES['timeLogZip'])){
        $date = date('d-m-Y-H-i-s',strtotime("-1 days"));
    		$pdfPath = './uploads/log/'.$date;
    		makeDir($pdfPath, 0755, true);

    		$mainImage = 'timeLogZip';
    		$encodedName = $date;
    		$ext = array("zip");
        //upload file
    		if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != ''){
    			$mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);
    			if($mainImageName !== false){
            //extract zip file
            $zip = new ZipArchive;
            if ($zip->open($pdfPath.'/'.$mainImageName) === TRUE) {
                $zip->extractTo($pdfPath);
                $zip->close();

                //delete zip file
                unlink($pdfPath.'/'.$mainImageName);
                $addData['file_path'] = $pdfPath;
                $this->db->insert('crm_employ_log_file',$addData);
                $this->session->set_flashdata('success','Time log uploaded successfully!');
                redirect(base_url().'superadmin/timelog');
                exit;
            } else {
              $this->session->set_flashdata('error','Please upload again!');
              redirect(base_url().'superadmin/timelog');exit;
            }
    			} else {
    					$this->session->set_flashdata('error','On Snap ! Time log not uploaded, verify file and upload again!');
    					redirect(base_url().'superadmin/timelog');exit;
    			}
    		} else {
    				$this->session->set_flashdata('error','On Snap ! Forgot to upload file? please upload again!');
    				redirect(base_url().'superadmin/timelog');exit;
    		}
    } else{
  		$this->template->load_partial('superadmin_master','timelog/add_timelog',$data);
  	}
	}

  /**
	* Developed By: Kaushal
	* Date : 01-Jan-2019
	*
	*/
	function exportLateEntry(){
		$today = new DateTime();
		$userId = (isset($_POST['selectUser']) && !empty($_POST['selectUser'])) ? $_POST['selectUser'] : "" ;
		if(isset($_POST['start_date']) && !empty($_POST['start_date'])){
			$startDate = date_format(date_create($_POST['start_date']),'Y-m-d');
		} else {
			$startDate = date('Y').'-04-01';
		}
		if(isset($_POST['end_date']) && !empty($_POST['end_date'])){
			$endDate = date_format(date_create($_POST['end_date']),'Y-m-d');
		}else{
			$endDate = date('Y',strtotime('+1 years')).'-04-01';
		}
		if(empty($userId)){
			$userIdList = $this->timelog_model->getAllUsersRecord(); // get All user id
			$fileName = 'All-User-LateEntry.xlsx';
		}else{
			$userIdList[]['user_id'] = $userId;
			$FileuserDetails = getUserDetail($userId);
			$FiledepartmentName = getDepartmentFromUserId($userId);
			$fileName = $FileuserDetails->first_name.' '.$FileuserDetails->last_name.' - '.$FiledepartmentName[0]['department_name'].'-LateEntry.xlsx';
		}

    //error_reporting(0);
    include 'PHPExcel/PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $i = 1;
    foreach ($userIdList as $userId){
      // get LateEntry data
      $userDetails = getUserDetail($userId['user_id']);
      $lateEntryData = $this->timelog_model->getLateEntryOfUser($userId['user_id'],$startDate,$endDate);
      $departmentName = getDepartmentFromUserId($userId['user_id']);
      $userName = $userDetails->first_name.' '.$userDetails->last_name.' - '.$departmentName[0]['department_name'];

        // Check LateEntry Data
        if(!empty($lateEntryData)){
          $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A'.$i++, $userName)
          ->setCellValue('A'.$i, 'Date')
          ->setCellValue('B'.$i, 'In Time')
          ->setCellValue('C'.$i, 'Gross work hours');
          $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
          $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFont()->setBold(true);
          $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);
          $objPHPExcel->getActiveSheet()->getStyle('A'.$i++)->getFont()->setBold(true);
          //$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);
          foreach($lateEntryData as $lateEntryRow){
            $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$i, date("d-m-Y", strtotime($lateEntryRow->log_date)))
              ->setCellValue('B'.$i, $lateEntryRow->in_time)
              ->setCellValue('C'.$i, $lateEntryRow->gross_work_hours);
            $i++;
          }
          // $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);
          // $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFont()->setBold(true);
        $i++; }
     }
    $callEndTime = date("d-m-Y");
    $objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFont()->setBold(true);
    //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
    //$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->setTitle('Time Log ');
    $objPHPExcel->setActiveSheetIndex(0);

    //save csv
    // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    // $objWriter->save('order_csv_'.$callEndTime.'.xlsx');
    // echo 'order_csv_'.$callEndTime.'.xlsx';
    //download csv
    // Redirect output to a client’s web browser (Excel5)
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="Time-Log-'.$callEndTime.'.xls"');
     header('Cache-Control: max-age=0');
     header('Cache-Control: max-age=1');
     header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
     header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
     header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
     header ('Pragma: public'); // HTTP/1.0
     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
     $objWriter->save('php://output');
     exit;





		/*//Excel Code starts here
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment; filename=\"$fileName\"");
		header("Cache-Control: max-age=0");
		// Error reporting
		//error_reporting(E_ALL);
		// Include path
		ini_set('include_path', ini_get('include_path').';../Classes/');
		// PHPExcel
		include 'PHPExcel/PHPExcel.php';
		// PHPExcel_Writer_Excel2007
		include 'PHPExcel/PHPExcel/Writer/Excel2007.php';
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Loop through each users
		if(!empty($userIdList)){
			$loop = 0;
      $objPHPExcel->getProperties()->setCreator("LetsNurture");
      $objPHPExcel->getProperties()->setLastModifiedBy("LetsNurture");
      $objPHPExcel->getProperties()->setTitle("HRMS Late Entry Sheet");
      $objPHPExcel->getProperties()->setSubject("HRMS Late Entry Sheet");
      $objPHPExcel->getProperties()->setDescription("HRMS Late Entry Sheet of Employee");
      $objPHPExcel->createSheet($loop);
      $objPHPExcel->setActiveSheetIndex($loop);
      $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
      $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getStyle("1")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
      $objPHPExcel->getActiveSheet()->setTitle('HRMS Late Entry Sheet');
      $i=1;
      $alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
      $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':C'.$i);
      $objPHPExcel->getActiveSheet()->SetCellValue($alpha[0].$i++, 'HRMS Late Entry Sheet of Employee');
			foreach($userIdList as $userId){
        // get LateEntry data
        $userDetails = getUserDetail($userId['user_id']);
        $lateEntryData = $this->timelog_model->getLateEntryOfUser($userId['user_id'],$startDate,$endDate);
        $departmentName = getDepartmentFromUserId($userId['user_id']);
        $userName = $userDetails->first_name.' '.$userDetails->last_name.' - '.$departmentName[0]['department_name'];
				// Set properties
				// Styling
				// Check LateEntry Data
        if(!empty($lateEntryData)){
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':C'.$i);
          $objPHPExcel->getActiveSheet()->SetCellValue($alpha[0].$i, '');
          $objPHPExcel->getActiveSheet()->SetCellValue($alpha[0].$i, $userName);
          $objPHPExcel->getActiveSheet()->getStyle($i)->getFont()->setBold(true);
          $i++;
          $objPHPExcel->getActiveSheet()->SetCellValue($alpha[0].$i,'Date');
          $objPHPExcel->getActiveSheet()->SetCellValue($alpha[1].$i,'In Time');
          $objPHPExcel->getActiveSheet()->SetCellValue($alpha[2].$i,'Gross work hours');
          $objPHPExcel->getActiveSheet()->getStyle($i)->getFont()->setBold(true);
          $i++;
					foreach($lateEntryData as $lateEntryRow){
						$objPHPExcel->getActiveSheet()->SetCellValue($alpha[0].$i,date("d-m-Y", strtotime($lateEntryRow->log_date)));
            $objPHPExcel->getActiveSheet()->SetCellValue($alpha[1].$i,$lateEntryRow->in_time);
            $objPHPExcel->getActiveSheet()->SetCellValue($alpha[2].$i,$lateEntryRow->gross_work_hours);
            $i++;
					}
				}

				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        // //Save Excel 2007 file
				// //$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
        ob_end_clean();
				$objWriter->save("php://output");
			}
      $loop++;
		}*/
		die;
	}
}
?>
