<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Policies extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->helper('general_helper');
		$this->load->model('policies_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Policies List';
		$data["policies"] = $this->policies_model->getAllPolicies();
	  $this->template->load_partial('superadmin_master','policies/index',$data);
	}

	function add($policiesId = ''){
		$data['titalTag'] = ' - Add Policies';
		if($this->input->post("addEditPolicies") !== false){
				$addData['policies_name'] = $this->input->post('policieName');
				$addData['status'] = $this->input->post('policiestatus');
				if(is_numeric($policiesId) && $policiesId != ''){
					$addData['policiesId']= $policiesId;
					$data = $this->policies_model->updatePolicies($addData);
					$this->session->set_flashdata('success','Policies updated successfully!');
					redirect(base_url().'superadmin/policies');exit;
				}else{
					$pdfPath = './uploads/policy';
					$mainImage = 'policiePdf';
					// avoid _ / = in encodedname as it might result in 404 error for filenames
					$encodedName = rtrim(strtr(base64_encode(time()), '+/', '-_'), '=');
					$ext = array("pdf", "docx");

					if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != ''){
						$mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);
						if($mainImageName !== false){
							$addData['policies_file_name'] = $mainImageName;
						}else{
								$this->session->set_flashdata('error','On Snap ! Policies not uploaded, verify file and upload again!');
								redirect(base_url().'superadmin/policies');exit;
						}
					} else {
							$this->session->set_flashdata('error','On Snap ! Forgot to upload file? please upload again!');
							redirect(base_url().'superadmin/policies');exit;
					}

					$data = $this->policies_model->add($addData);
					$this->session->set_flashdata('success','Policies Added successfully!');
					redirect(base_url().'superadmin/policies');exit;
				}
		}
	  $this->template->load_partial('superadmin_master','policies/add_edit_policies',$data);
	}

	public function edit($policiesId){
		$data['titalTag'] = ' - Edit Policies';
		$data['policiesEdit'] = $this->policies_model->getPolicy($policiesId);
		$this->template->load_partial('superadmin_master','policies/add_edit_policies',$data);
	}

	public function delete($policiesId){
		$data = $this->policies_model->getPolicy($policiesId);

		$upOne = realpath(BASEPATH . '/..');
		$upOne.'/uploads/policy/'.$data[0]->policies_file_name;
		$this->policies_model->deletePolicy($policiesId);
		unlink($fileDeleteInfo);

		$this->session->set_flashdata('success','Policies deleted successfully!');
		redirect(base_url().'superadmin/policies');
		exit;
	}
}

?>
