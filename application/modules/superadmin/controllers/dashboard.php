<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('dashboard_model');
		$this->load->helper('general_helper');
		sAdmin_auth('0');
		$this->logSession = $this->session->userdata("sLogin_session");
	}
	function index(){
		$data['titalTag'] = ' - Superadmin dashboard';
		sAdmin_auth('0');
		$today = date('Y-m-d H:i:s');
		//get upcoming user leaves list
		$data['upcomingleaves'] = $this->dashboard_model->getupcominguserleaves();

		//check if any feedback remaing last month then display popup
		$checkRemaningFeedbacks = $this->dashboard_model->checkRemaningFeedback();
		$data['checkRemaningFeedbacks'] = $checkRemaningFeedbacks;

		$data['probationEmployeeList'] = $this->dashboard_model->getProbationEmployeeList();
		$data['relievingDateList'] = $this->dashboard_model->getRelievingDateList();
		//Total pending leave
		$checkPendingLeavetotal = $this->dashboard_model->getPendingLeaveTotal();
		$data['pendingLeavetotal'] =  $checkPendingLeavetotal->pendingLeavetotal;
		//pending leaves of current month
		$getMonthPendingLeave = $this->dashboard_model->getMonthPendingLeave($today);
		$data['monthPendingLeave'] = $getMonthPendingLeave->monthPendingLeave;
		//Early leaves of current month
		$getEarlyPendingLeave = $this->dashboard_model->getEarlyPendingLeave($today);
		$data['monthEarlyLeave'] = $getEarlyPendingLeave->monthEarlyLeave;
		//Birthday and Anniversary leaves of current month
		$getMonthBirthAnniLeave = $this->dashboard_model->getMonthBirthAnniLeave($today);
		$data['monthBirthAnniLeave'] = $getMonthBirthAnniLeave->monthBirthAnniLeave;
		//Total current month leave
    $checkTotalNumberLeave = $this->dashboard_model->checkTotalNumberLeave($today);
		$data['totalThisMonthLeave'] =  $checkTotalNumberLeave->totalLeave;

		$data['getActiveEmployee'] = $this->dashboard_model->getActiveEmployee();
		// No of Last month recruitment
		$data['getNoOfRecruitmentLastMonth'] = $this->dashboard_model->getNoOfRecruitmentLastMonth();
		// No of Last 3 month recruitment
		$data['getNoOfRecruitmentLast3Month'] = $this->dashboard_model->getNoOfRecruitmentLast3Month();
		// No of Last year recruitment
		$data['getNoOfRecruitmentLastYear'] = $this->dashboard_model->getNoOfRecruitmentLastYear();
		// No of application recieved last month
		$data['ApplicationRecievedLastMonth'] = $this->dashboard_model->ApplicationRecievedLastMonth();
		// No of application recieved last 3 months
		$data['ApplicationRecievedLast3Month'] = $this->dashboard_model->ApplicationRecievedLast3Month();
		// No of application recieved last year
		$data['ApplicationRecievedLastYear'] = $this->dashboard_model->ApplicationRecievedLastYear();
		// No of Interviews Scheduled last month
		$data['InterviewsScheduleLastMonth'] = $this->dashboard_model->InterviewsScheduleLastMonth();
		// No of Interviews Scheduled last 3 months
		$data['InterviewsScheduleLast3Month'] = $this->dashboard_model->InterviewsScheduleLast3Month();
		// No of Interviews Scheduled last Year
		$data['InterviewsScheduleLastYear'] = $this->dashboard_model->InterviewsScheduleLastYear();
		// Total Leaves last month
		$data['getTotalLeavesLastMonth'] = $this->dashboard_model->getTotalLeavesLastMonth();
		// Total Leaves last 3 months
		$data['getTotalLeavesLast3Months'] = $this->dashboard_model->getTotalLeavesLast3Months();
		// Total Leaves last Year
		$data['getTotalLeavesLastYear'] = $this->dashboard_model->getTotalLeavesLastYear();
		// Average Leave Last Month
		$data['AverageLeaveLastMonth'] = number_format((float)$this->dashboard_model->AverageLeaveLastMonth()->AverageLeave, 2, '.', '');
		// Average Leave Last 3 Months
		$data['AverageLeaveLast3Months'] = number_format((float)$this->dashboard_model->AverageLeaveLast3Months()->AverageLeave, 2, '.', '');
		// Average Leave Last Year
		$data['AverageLeaveLastYear'] = number_format((float)$this->dashboard_model->AverageLeaveLastYear()->AverageLeave, 2, '.', '');
		// Get Average Working Hours Last month
		$data['AverageWorkingHoursLastMonth'] = $this->dashboard_model->AverageWorkingHoursLastMonth()->AverageWorkingHour;
		// Get Average Working Hours Last 3 months
		$data['AverageWorkingHoursLast3Months'] = $this->dashboard_model->AverageWorkingHoursLast3Months()->AverageWorkingHour;
		// Get Average Working Hours Last 3 months
		$data['AverageWorkingHoursLastYear'] = $this->dashboard_model->AverageWorkingHoursLastYear()->AverageWorkingHour;
		// Get Male Female Ratio Last Month
		$recieve_data = $this->dashboard_model->MaleFemaleRatioLastMonth();
		if($recieve_data->MaleCount !='' && $recieve_data->FemaleCount !=''){
			$data['MaleFemaleRatioLastMonth'] = CalculateRatio($recieve_data->MaleCount, $recieve_data->FemaleCount);
		}
		else{
			$data['MaleFemaleRatioLastMonth'] = CalculateRatio('0', '0');
		}
		// Get Male Female Ratio Last 3 Months
		$recieve_data = $this->dashboard_model->MaleFemaleRatioLast3Months();
		$data['MaleFemaleRatioLast3Months'] = CalculateRatio($recieve_data->MaleCount, $recieve_data->FemaleCount);
		// Get Male Female Ratio Last Year
		$recieve_data = $this->dashboard_model->MaleFemaleRatioLastYear();
		$data['MaleFemaleRatioLastYear'] = CalculateRatio($recieve_data->MaleCount, $recieve_data->FemaleCount);
		// Average Experience of team member Last Month
		$reciveExperince = $this->dashboard_model->AverageExperienceOfTeamMemberLastmonth();
		$get_exp = AverageExperience($reciveExperince[0]['ExYear'], $reciveExperince[0]['ExMonth'], $reciveExperince[0]['total_user']);
		$data['AverageExperienceOfTeamMemberLastmonth'] = $get_exp;
		// Average Experience of team member Last 3 Months
		$reciveExperince = $this->dashboard_model->AverageExperienceOfTeamMemberLast3months();
		$get_exp = AverageExperience($reciveExperince[0]['ExYear'], $reciveExperince[0]['ExMonth'], $reciveExperince[0]['total_user']);
		$data['AverageExperienceOfTeamMemberLast3months'] = $get_exp;
		// Average Experience of team member Last Years
		$reciveExperince = $this->dashboard_model->AverageExperienceOfTeamMemberLastYear();
		$get_exp = AverageExperience($reciveExperince[0]['ExYear'], $reciveExperince[0]['ExMonth'], $reciveExperince[0]['total_user']);
		$data['AverageExperienceOfTeamMemberLastYear'] = $get_exp;
		// echo $this->db->last_query();
		// Get Oldest employees according to working hours
		$data['OldestEmployees'] = $this->dashboard_model->OldestEmployees();
		// Get Newest employees according to working hours
		$data['NewestEmployees'] = $this->dashboard_model->NewestEmployees();
		// Top Rewards Claimed by Employees
		$data['TopReward'] = $this->dashboard_model->TopReward();
		// Top 10 users did HRMS access outside of Local IP
		$data['HrmsAccessdOutside'] = $this->dashboard_model->HrmsAccessdOutside();

	  $this->template->load_partial('superadmin_master','superadmin_dashboard',$data);
	}

	function profile()
	{
		$data['titalTag'] = ' - Superadmin Profile';
		$userData = $this->logSession;

		$data['userdata'] = $this->dashboard_model->getUserinfo();

		if($this->input->post("updatePassword") !== false)
		{
			$udata2['sId'] = $userData[0]->sId;

			$currPassword = md5($this->input->post('currPassword'));
			$repassword = md5($this->input->post('repassword'));

			$udata2['sPassword'] = md5($this->input->post('password1'));

			if($udata2['sPassword'] != '' && $repassword != '' && $currPassword != '')
			{
				if($data['userdata'][0]->sPassword != $currPassword)
				{
					$this->session->set_flashdata('errorPass','Your current password is incorrect! Try again');
					redirect(base_url()."superadmin/dashboard/profile/#focusPass");exit;
				}
				if($repassword != $udata2['sPassword'])
				{
					$this->session->set_flashdata('errorPass','Password and Confirm Password are not matched!');
					redirect(base_url()."superadmin/dashboard/profile/profile#focusPass");exit;
				}

				$data2 = $this->dashboard_model->updateUser($udata2);
				if($data2 == true)
				{
					$this->session->set_flashdata('successPass','Your password updated successfully!');
					redirect(base_url()."superadmin/dashboard/profile#focusPass");exit;
				}
			}
		}

	   	$this->template->load_partial('superadmin_master','superadmin_profile',$data);
	}


	function sendmail()
	{
		$id = $this->input->post('id');
		print_r($id);

	}
	function send()
	{
		$this->load->model('templates_model');
		$this->load->model('users_model');
		$user = $this->users_model->getUsers($_POST['user_id']);
		$template = $this->templates_model->getTemplates($_POST['email_id']);

		$finalHTML = $_POST['message_info'].'<br />'.$template[0]->email_description;

		$subject = $template[0]->email_name;

		send_mail($user[0]->email, $subject, $finalHTML);
		$this->session->set_flashdata('success','Email Send successfully!');
		redirect (base_url().'superadmin/dashboard');exit;
	}

	/*
     * @Author:  krunal
     * @Created: September 02 2015
     * @Modified By:
     * @Modified at:
     * @Comment: Announcements sidebar with paggination
     */
    function getAnnouncementsSidebar() {
     $CI = & get_instance();
     $sql = "SELECT count(*) as count
             FROM crm_announcement
             WHERE published_status = '1'
             ORDER BY created_time DESC";
     $query = $CI->db->query($sql);
     $totalresults = $query->result();
     $count= $totalresults[0]->count;
     // For pagination
     $page = (int) (!isset($_POST['pageId']) ? 1 :$_POST['pageId']);
     $page = ($page == 0 ? 1 : $page);
     $recordsPerPage = 2;
     $start = ($page-1) * $recordsPerPage;
     $class='meat_page';
     //pass pagination value in pagination function
     $pagination= pagination($count,$page,$recordsPerPage,$class);

     $sql = "SELECT *
             FROM crm_announcement
             WHERE published_status = '1'
             ORDER BY created_time DESC LIMIT $start, $recordsPerPage";
     $query = $CI->db->query($sql);
     $results = $query->result(); ?>
         <ul>
         <?php for($a=0; $a<count($results); $a++): ?>
             <li>
                 <div class="subdiv">
                     <span><?php echo $results[$a]->title; ?></span>
                     <span class="date"><?php echo date("d-m-Y", strtotime($results[$a]->created_time)); ?></span>
                     <div class="clearfix"></div>
                     <p><?php echo $results[$a]->description; ?></p>
                 </div>
             </li>
         <?php endfor;?>
        </ul>
     <?php echo $pagination;
    }

		/*
			 * @Author:  Jignasa
			 * @Created: 31-07-2017
			 * @Modified By:
			 * @Modified at:
			 * @Comment: Function to get the different leaves
			 */
		public function getTotalLeave(){
			$today = date("Y-m-01 H:i:s", strtotime($_POST['month']));
			//Total current month leave
			$checkTotalNumberLeave = $this->dashboard_model->checkTotalNumberLeave($today);
			$getMonthPendingLeave = $this->dashboard_model->getMonthPendingLeave($today);
			$getMonthBirthAnniLeave = $this->dashboard_model->getMonthBirthAnniLeave($today);

			//Early leaves of current month
			$getEarlyPendingLeave = $this->dashboard_model->getEarlyPendingLeave($today);
			$data['monthEarlyLeave'] = $getEarlyPendingLeave->monthEarlyLeave;


			if($checkTotalNumberLeave->totalLeave){
				$data['totalLeave'] = $checkTotalNumberLeave->totalLeave;
			}else{
				$data['totalLeave'] = "0";
			}
			if($getMonthPendingLeave->monthPendingLeave){
				$data['monthPendingLeave'] = $getMonthPendingLeave->monthPendingLeave;
			}else{
				$data['monthPendingLeave'] =  "0";
			}
			if($getMonthBirthAnniLeave->monthBirthAnniLeave){
				$data['monthBirthAnniLeave'] = $getMonthBirthAnniLeave->monthBirthAnniLeave;
			}else{
				$data['monthBirthAnniLeave'] =  "0";
			}
			echo json_encode($data);
		}

}

?>
