<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Meeting extends MX_Controller {
	function __construct()
    {
        parent::__construct();

        $this->template->set('controller', $this);
        $this->load->database();
        $this->logSession = $this->session->userdata("login_session");
        $this->load->helper('download');
        $this->load->helper('general_helper');
		$this->load->library('email');
        $this->load->model('meeting_model');
        sAdmin_auth('0');
    }

	function index(){
		$data['titalTag'] = ' - Meeting Request';
		$data['meetings'] = $this->meeting_model->getMeetingCalendar();
		$this->template->load_partial('superadmin_master','meeting/list_meeting',$data);
	}

	function add(){
		$data['titalTag'] = ' - Add Meeting';
		$this->template->load_partial('superadmin_master','meeting/add_meeting',$data);
	}

	function addMeeting(){
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		if($_POST['huser_id'] != '' && $_POST['created_time'] != ''){
			$checkMeetingTime = $this->meeting_model->checkMeetingTime($_POST['created_time'], $_POST['meeting_time']);
			if(empty($checkMeetingTime)) {
				if(isset($_POST['user_id'])){
					//hr add metting
					$data['user_id'] =$_POST['user_id'];
					$userInfo = $this->meeting_model->getLoggedInUser($data['user_id']);
				}else{
					//employ add metting
					$data['user_id'] =$_POST['huser_id'];
					$userInfo = $this->meeting_model->getLoggedInUser(90);
				}
				$data['hr_role_id'] = 22;
				$data['schedule_date'] =$_POST['created_time'];
				$data['schedule_time'] =$_POST['meeting_time'];
				$data['subject'] =$_POST['Subject'];
				$dataSubmit = $this->meeting_model->addMeeting($data);

				//send mail
				$mailData['description'] = $userInfo[0]->first_name.' '.$userInfo[0]->last_name.' meeting with you';					$activeUserEmails = activeUserEmails(); // Get all active user emails with comma separated
				//$user_emails = array('krunal.letsnurture@gmail.com','kalpesh.letsnurture@gmail.com');
				$this->email->from('hrms@letsnurture.com', "HRMS");
				$this->email->to($userInfo[0]->email);
				$this->email->subject('Meeting Schedule');
				$body = $this->load->view('mail_layouts/meetingshedule/employ_mail.php', $mailData, TRUE);
				$this->email->message($body);
				if ($this->email->send()):
					//echo "Mail sent!"; // Mail sent!
				else:
					//echo "There is error in sending mail!"; // There is error in sending mail!
				endif;
				echo 1;
			}else{
				echo 2;
			}
		} else {
			echo 0;
		}
	}


	function statuslist(){
		$data['titalTag'] = ' - Status Meeting';
		$this->template->load_partial('superadmin_master','meeting/statuslist',$data);
	}
	function getMonthMeeting(){
		if($_POST['date'] != ''){

			$date = $_POST['date'];

				//Hr meeting data
			  $datas = $this->meeting_model->getHrMeetings($date);
				foreach ($datas as $data) {?>
					<div class="col-lg-6">
	                  <section class="panel">
	                      <header class="panel-heading">
	                          <?php echo $data['first_name'].' '.$data['last_name'];?>
	                      </header>
	                      <div class="panel-body">
	                          <form class="form-horizontal frmUpdate" name="frmUpdate<?php echo $data['meeting_id'];?>" id="frmUpdate<?php echo $data['meeting_id'];?>">
								  <script>
					  			$('#frmUpdate<?php echo $data['meeting_id'];?> #date<?php echo $data['meeting_id'];?>').datepicker({
					  			   //format: 'yyyy-mm-dd',
					  			   daysOfWeekDisabled: [0,6],
					  			   autoclose: true,
					  		   });
					  		   </script>
								  <input type="hidden" value="<?php echo $data['meeting_id'];?>" name="meeting_id" id="meeting_id">
								  <input type="hidden" value="<?php echo $data['user_id'];?>" name="user_id" id="user_id">
								  <div class="form-group">
		                            <label class="col-lg-2 col-sm-2 control-label" >Subject</label>
									<div class="col-lg-10">
	                                  <input type="text" class="form-control" id="subject" name="subject" value="<?php echo $data['subject'];?>" required="">
									</div>
	                              </div>
	                              <div class="form-group">
		                             <label class="col-lg-2 col-sm-2 control-label" >Time</label>
									 <div class="col-lg-10">
										<select name="meeting_time" id="meeting_time" class="form-control" required>
											<option value="">Time</option>
											<option value="09:00" <?php if($data['schedule_time'] == '09:00'){ echo "selected";}?>>09:00</option>
											<option value="09:15" <?php if($data['schedule_time'] == '09:15'){ echo "selected";}?>>09:15</option>
											<option value="09:30" <?php if($data['schedule_time'] == '09:30'){ echo "selected";}?>>09:30</option>
											<option value="09:45" <?php if($data['schedule_time'] == '09:45'){ echo "selected";}?>>09:45</option>
											<option value="10:00" <?php if($data['schedule_time'] == '10:00'){ echo "selected";}?>>10:00</option>
											<option value="10:15" <?php if($data['schedule_time'] == '10:15'){ echo "selected";}?>>10:15</option>
											<option value="10:30" <?php if($data['schedule_time'] == '10:30'){ echo "selected";}?>>10:30</option>
											<option value="10:45" <?php if($data['schedule_time'] == '10:45'){ echo "selected";}?>>10:45</option>
											<option value="11:00" <?php if($data['schedule_time'] == '11:00'){ echo "selected";}?>>11:00</option>
											<option value="11:15" <?php if($data['schedule_time'] == '11:15'){ echo "selected";}?>>11:15</option>
											<option value="11:30" <?php if($data['schedule_time'] == '11:30'){ echo "selected";}?>>11:30</option>
											<option value="11:45" <?php if($data['schedule_time'] == '11:45'){ echo "selected";}?>>11:45</option>
											<option value="12:00" <?php if($data['schedule_time'] == '12:00'){ echo "selected";}?>>12:00</option>
											<option value="12:15" <?php if($data['schedule_time'] == '12:15'){ echo "selected";}?>>12:15</option>
											<option value="12:30" <?php if($data['schedule_time'] == '12:30'){ echo "selected";}?>>12:30</option>
											<option value="12:45" <?php if($data['schedule_time'] == '12:45'){ echo "selected";}?>>12:45</option>
											<option value="01:00" <?php if($data['schedule_time'] == '01:00'){ echo "selected";}?>>01:00</option>
											<option value="01:15" <?php if($data['schedule_time'] == '01:15'){ echo "selected";}?>>01:15</option>
											<option value="01:30" <?php if($data['schedule_time'] == '01:30'){ echo "selected";}?>>01:30</option>
											<option value="01:45" <?php if($data['schedule_time'] == '01:45'){ echo "selected";}?>>01:45</option>
											<option value="02:00" <?php if($data['schedule_time'] == '02:00'){ echo "selected";}?>>02:00</option>
											<option value="02:15" <?php if($data['schedule_time'] == '02:15'){ echo "selected";}?>>02:15</option>
											<option value="02:30" <?php if($data['schedule_time'] == '02:30'){ echo "selected";}?>>02:30</option>
											<option value="02:45" <?php if($data['schedule_time'] == '02:45'){ echo "selected";}?>>02:45</option>
											<option value="03:00" <?php if($data['schedule_time'] == '03:00'){ echo "selected";}?>>03:00</option>
											<option value="03:15" <?php if($data['schedule_time'] == '03:15'){ echo "selected";}?>>03:15</option>
											<option value="03:30" <?php if($data['schedule_time'] == '03:30'){ echo "selected";}?>>03:30</option>
											<option value="03:45" <?php if($data['schedule_time'] == '03:45'){ echo "selected";}?>>03:45</option>
											<option value="04:00" <?php if($data['schedule_time'] == '04:00'){ echo "selected";}?>>04:00</option>
											<option value="04:15" <?php if($data['schedule_time'] == '04:15'){ echo "selected";}?>>04:15</option>
											<option value="04:30" <?php if($data['schedule_time'] == '04:30'){ echo "selected";}?>>04:30</option>
											<option value="04:45" <?php if($data['schedule_time'] == '04:45'){ echo "selected";}?>>04:45</option>
											<option value="05:00" <?php if($data['schedule_time'] == '05:00'){ echo "selected";}?>>05:00</option>
											<option value="05:15" <?php if($data['schedule_time'] == '05:15'){ echo "selected";}?>>05:15</option>
											<option value="05:30" <?php if($data['schedule_time'] == '05:30'){ echo "selected";}?>>05:30</option>
											<option value="05:45" <?php if($data['schedule_time'] == '05:45'){ echo "selected";}?>>05:45</option>
											<option value="06:00" <?php if($data['schedule_time'] == '06:00'){ echo "selected";}?>>06:00</option>
											<option value="06:15" <?php if($data['schedule_time'] == '06:15'){ echo "selected";}?>>06:15</option>
											<option value="06:30" <?php if($data['schedule_time'] == '06:30'){ echo "selected";}?>>06:30</option>
											<option value="06:45" <?php if($data['schedule_time'] == '06:45'){ echo "selected";}?>>06:45</option>
											<option value="07:00" <?php if($data['schedule_time'] == '07:00'){ echo "selected";}?>>07:00</option>
											<option value="07:15" <?php if($data['schedule_time'] == '07:15'){ echo "selected";}?>>07:15</option>
											<option value="07:30" <?php if($data['schedule_time'] == '07:30'){ echo "selected";}?>>07:30</option>
											<option value="07:45" <?php if($data['schedule_time'] == '07:45'){ echo "selected";}?>>07:45</option>
										</select>
									 </div>
	                              </div>
	                              <div class="form-group">
	                                  <label class="col-lg-2 col-sm-2 control-label" >Date</label>
									  <div class="col-lg-10">
	                                  	<input type="text" class="form-control dates" id="date<?php echo $data['meeting_id'];?>"  name="date" value="<?php echo date("m/d/Y",strtotime($data['schedule_date']));?>">
									  </div>
	                              </div>
								  <div class="form-group">
	                                  <label class="col-lg-2 col-sm-2 control-label" >Status</label>
									  <div class="col-lg-10">
	                                  	<select name="status" id="status" class="form-control" required>									        				      <option value="0" <?php if($data['status'] == '0'){ echo "selected";}?>>Inactive</option>
											<option value="1" <?php if($data['status'] == '1'){ echo "selected";}?>>Accepted</option>
											<option value="2" <?php if($data['status'] == '2'){ echo "selected";}?>>Reject</option>
										</select>

									  </div>
	                              </div>
	                              <button class="btn btn-info btnUpdate" id="<?php echo $data['meeting_id'];?>" type="submit">Update</button>
								  <div class="loader">
									<img src="<?php echo base_url()?>assets/img/input-spinner.gif" alt="loading...">
								  </div>
	                          </form>
	                      </div>
	                  </section>
	              </div>
				<?php

			}
		}
	}

	function updateMeetingStatus(){

		if($_POST['meeting_id'] != '' && $_POST['date'] != ''){
			$checkMeetingTime = $this->meeting_model->checkMeetingTime($_POST['date'], $_POST['meeting_time']);
			if(empty($checkMeetingTime)) {
				$data['meeting_id'] = $_POST['meeting_id'];
				$data['subject'] = $_POST['subject'];
				$data['schedule_time'] =$_POST['meeting_time'];
				$data['schedule_date'] = date("Y-m-d",strtotime($_POST['date']));
				$data['status'] =$_POST['status'];
				$dataSubmit = $this->meeting_model->updateMeetingStatus($data);

					//send mail
					$userInfo = $this->meeting_model->getLoggedInUser($_POST['user_id']);
					if($data['status'] == 1){
						$status = 'Accepted';
					}else{
						$status = 'Reject';
					}
					$mailData['description'] = 'Your meeting with HR scheduled on '.$data['schedule_date'].' has been '.$status;
	                $activeUserEmails = activeUserEmails(); // Get all active user emails with comma separated
					//$user_emails = array('krunal.letsnurture@gmail.com','kalpesh.letsnurture@gmail.com');
					$this->email->from('hrms@letsnurture.com', "HRMS");
					$this->email->to($userInfo[0]->email);
					$this->email->subject('Meeting Schedule');
					$body = $this->load->view('mail_layouts/meetingshedule/employ_mail.php', $mailData, TRUE);
					$this->email->message($body);
					if ($this->email->send()):
						//echo "Mail sent!"; // Mail sent!
					else:
						//echo "There is error in sending mail!"; // There is error in sending mail!
					endif;
				echo 1;
			}else{
				echo 2;
			}
		} else {
			echo 0;
		}
	}

}
?>
