<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Superadmin extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('superadmin_model');
		$this->load->library('email');
		$this->load->library('user_agent');
		sAdmin_auth('1');
	}
	public function index(){

		$data['sUsername'] = trim($this->input->post('sUsername'));
		$data['sPassword'] = trim($this->input->post('sPassword'));
		if($this->input->post("submitLogin") !== false || ($data['sUsername'] !='' && $data['sPassword'] != ''))
		{
			$data['sUsername'] = $this->input->post('sUsername');
			$data['sPassword'] = $this->input->post('sPassword');

			$data1 = $this->superadmin_model->login($data);
			if($this->input->post('remember')){
				$expire = '999999999';
				$usernameCookie = array(
					'name'   => 'sUsername_cookie',
					'value'  => $data['sUsername'],
					'expire' => $expire,

				);
				$passwordCookie = array(
					'name'   => 'sPassword_cookie',
					'value'  => base64_encode($data['sPassword']),
					'expire' => $expire,

				);
				$this->input->set_cookie($sUsernameCookie);
				$this->input->set_cookie($sPasswordCookie);

			}else{
				delete_cookie("sUsername_cookie");
				delete_cookie("sPassword_cookie");
			}
			if(count($data1)>0){
					$this->session->set_userdata('sLogin_session',$data1);
					$login_session = $this->session->userdata('sLogin_session');
					redirect(base_url().'superadmin/dashboard');
					exit;
			}else {
					$loginAttampts = $this->session->userdata('login_super_admin_attempts');
					$loginAttemptSession = $loginAttampts + 1;
					$this->session->set_userdata(array('login_super_admin_attempts' => $loginAttemptSession));
					if ($this->session->userdata('login_super_admin_attempts') > $this->config->item('login_attampt')) {
							$browser = $this->agent->browser();
							$ipAddress = $this->input->ip_address();
							$os = $this->agent->platform();
							$mailData['body'] = '<tr><td colspan="2" style="font-size:14px; color:#FFF;
														 padding:0 40px 20px 40px;"> User had tried to login with more then 5 failed login attempts</td>
												 </tr>
																			<tr>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
								<p><strong style="color:#03A9F4;">IP: </strong></p>
							</td>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
								<p>' . $ipAddress . '</p>
							</td>
						</tr>
						<tr>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
								<p><strong style="color:#03A9F4;">Browser: </strong></p>
							</td>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
								<p>' . $browser . '</p>
							</td>
						</tr>
						<tr>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;">
								<p><strong style="color:#03A9F4;">Operating system: </strong></p>
							</td>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
								<p>' . $os . '</p>
							</td>
						</tr>
						<tr>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
								<p><strong style="color:#03A9F4;">Username: </strong></p>
							</td>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
								<p>' . $data['sUsername'] . '</p>
							</td>
							</tr>
							<tr>
																	<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
								<p><strong style="color:#03A9F4;">Password: </strong></p>
							</td>
							<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
								<p>' . $data['sPassword'] . '</p>
							</td>
																	</tr>
																	<tr>
																	<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;width:120px;" valign="top">
								<p><strong style="color:#03A9F4;">Date and Time: </strong></p>
							</td>
																	<td style="padding:0 0 0 40px; font-size:13px; color:#FFF; font-weight:normal;">
								<p>' . date('d-M-Y H:i:s') . '</p>
							</td>
						</tr>';
							$mailData['name'] = 'HR';
							$this->email->from('hrms@letsnurture.com', "HRMS");
							$this->email->to("hrd@letsnurture.com");
							$this->email->subject('Failure Login attempt');
							$body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
							$this->email->message($body);
							//$this->email->send();
					}
				$this->session->set_flashdata('error',"Invalid Username or Password");
				redirect(base_url().'superadmin');
				exit;
			}
		}
		$data['sUsername_cookie'] = $this->input->cookie('sUsername_cookie', TRUE) ? $this->input->cookie('sUsername_cookie', TRUE) : '';
		$data['sPassword_cookie'] = $this->input->cookie('sPassword_cookie', TRUE) ? $this->input->cookie('sPassword_cookie', TRUE) : '';;


	   	$this->template->load_partial('login_master','superadmin_login',$data);
	}

}

?>
