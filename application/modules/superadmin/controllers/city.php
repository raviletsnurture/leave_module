<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City extends MX_Controller {  
	function __construct(){
		parent::__construct();		
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('city_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - City';
		
		$data["city"] = $this->city_model->getAllCity();
		
	   	$this->template->load_partial('superadmin_master','city/list_city',$data);
	}
	function add($catId = ''){
		
		$data['titalTag'] = ' - Add City';
		if($this->input->post("addEditCity") !== false)
		{
				$addData['city_name'] = $this->input->post('city_name');
				$addData['country_id'] = $this->input->post('country_id');
				$addData['state_id'] = $this->input->post('state_id');
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['city_id']= $catId;
					$data = $this->city_model->updateCity($addData);
					$this->session->set_flashdata('success','City updated successfully!');
				}else{
					
					$data = $this->city_model->add($addData);
					$this->session->set_flashdata('success','City Added successfully!');
				}
				redirect(base_url().'superadmin/city');exit;		
		}
		
		
	   	$this->template->load_partial('superadmin_master','city/add_edit_city',$data);
	}
	public function edit($catId)
	{
		$data['cityEdit'] = $this->city_model->getCity($catId);
		$data['state'] = $this->city_model->getState($catId);
		
		$this->template->load_partial('superadmin_master','city/add_edit_city',$data);
	}
	public function getStates()
	{
		$country_id = $this->input->post('country_id');
		$city_dropdown = $this->city_model->getCity_drop($country_id);
		
		echo $city_dropdown;
	}
	public function delete($catId)
	{
		$this->city_model->deleteCity($catId);	
		$this->session->set_flashdata('success','City deleted successfully!');
		redirect(base_url().'superadmin/city');
		exit;
	}
}

?>