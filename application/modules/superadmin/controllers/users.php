<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->helper('general_helper');
		$this->load->model('users_model');
		$this->load->library('email');
		sAdmin_auth('0');
	}


	function index(){
		$data['titalTag'] = ' - All Users';
		$customUrl = "";
		$categoryUrl = "";
		$users = "";
		$country = "";

		/*Pagination start */
		if($this->input->get('keyword')){
			$customUrl .= 'keyword='.$this->input->get('keyword');
		}
		if($this->input->get('perpage')){
			$customUrl .= '&perpage='.$this->input->get('perpage');
		}
		$totalRec = $this->users_model->getAllUsers($users, '', '');
		$perPage = $this->input->get('perpage') && $this->input->get('perpage') != '' ? $this->input->get('perpage') : 25;
		$pageQueryString = 'page';
		$pageUrl = "superadmin/users?".$categoryUrl.$customUrl;
		$page = ($this->input->get('page')) ? $this->input->get('page') : '1';
		$page = ($page-1)*$perPage;

		$data["users"] = $this->users_model->getAllUsers($users,$perPage,$page);
		$data["links"] = paginate($totalRec,$perPage,$pageQueryString,$pageUrl);
		//$data['categroyRecCnt'] = $leadRecCnt;
		$data['religion_chart_data'] = $this->users_model->religionGraph();
	    $data['gender_chart_data'] = $this->users_model->genderGraph();
	    $data['cast_chart_data'] = $this->users_model->castGraph();
	    $data['state_chart_data'] = $this->users_model->stateGraph();
	    $data['blood_group_chart_data'] = $this->users_model->bloodgroupGraph();
	    $age_array = array('0-20','20-25','25-30','30-40','+40');
	    $data['age_group_chart_data'] = $this->users_model->agegroupGraph($age_array);

		  $this->template->load_partial('superadmin_master','users/list_users',$data);
	}

	function add($catId = '')
	{

		$data['titalTag'] = ' - Users add';
		if($this->input->post("addEditUsers") !== false)
		{
			$addData['role_id'] = $this->input->post('role_id');
			$addData['salaryslip_id'] = $this->input->post('salaryslip_id');
			$addData['department_id'] = $this->input->post('department_id');
			$addData['first_name'] = $this->input->post('first_name');
			$addData['middle_name'] = $this->input->post('middle_name');
			$addData['last_name'] = $this->input->post('last_name');
			$addData['ex_year'] = $this->input->post('ex_year');
			$addData['ex_month'] = $this->input->post('ex_month');
			if($this->input->post('joining_date') != ''){
				$addData['joining_date'] = $this->input->post('joining_date');
			}
			if($this->input->post('reliving_date') != ''){
				$addData['reliving_date'] = $this->input->post('reliving_date');
			}
			if($this->input->post('trainee_complete_at') != ''){
				$addData['trainee_complete_at'] = $this->input->post('trainee_complete_at');
			}
			if($this->input->post('probation_complete_at') != ''){
				$addData['probation_complete_at'] = $this->input->post('probation_complete_at');
			}
			if($this->input->post('username') != '')
			{
				$addData['username'] = $this->input->post('username');
				$len = 8;
				$password_new = $this->users_model->generate_password($len);
				$addData['password'] = $password_new;
			}

			$addData['country_id'] = $this->input->post('country_id');
			$addData['state_id'] = $this->input->post('state_id');
			$addData['city_id'] = $this->input->post('city_id');
			$addData['street'] = $this->input->post('street');
			$addData['zipcode'] = $this->input->post('zipcode');
			$addData['gender'] = $this->input->post('gender');
			$addData['birthdate'] = $this->input->post('birthdate');
			$addData['mobile'] = base64_encode($this->input->post('mobile'));
			$addData['landline'] = $this->input->post('landline');
			$addData['emergency_name1'] = $this->input->post('emergency_name1');
			$addData['emergency_name2'] = $this->input->post('emergency_name2');
			$addData['landline2'] = $this->input->post('landline2');
			$addData['total_leaves'] = 0;


			$addData['skype'] = base64_encode($this->input->post('skype'));
			$addData['gtalk'] = base64_encode($this->input->post('gtalk'));
			$addData['pancard_no'] = base64_encode($this->input->post('pancard_no'));
			$addData['pf_no'] = base64_encode($this->input->post('pf_no'));
			$addData['employee_insurance_number'] = base64_encode($this->input->post('employee_insurance_number'));
			$addData['aadhar_no'] = base64_encode($this->input->post('aadhar_no'));
			$addData['email'] = $this->input->post('email');
			$addData['gmail_id'] = $this->input->post('gmail_id');
			$addData['alternate_email'] = $this->input->post('alternate_email');
			$addData['religion_id'] = $this->input->post('religion_id');
			$addData['cast_id'] = $this->input->post('cast_id');

			$addData['employee_type'] = $this->input->post('employee_type');
			$addData['anniversary'] = $this->input->post('anniversary');
			$addData['blood_group'] = $this->input->post('blood_group');
			$addData['license'] = base64_encode($this->input->post('license'));
			$addData['ESIC_number'] = base64_encode($this->input->post('ESIC_number'));

			$addData['bank_name'] = base64_encode($this->input->post('bank_name'));
			$addData['account_number'] = base64_encode($this->input->post('account_number'));
			$addData['ifsc_code'] = base64_encode($this->input->post('ifsc_code'));
			$addData['branch'] = base64_encode($this->input->post('branch'));


			/*-----------------upload image--------------*/

			$imgPath = './uploads/user_images';
			$mainImage = 'mainImage';
			$ext = array("gif", "jpeg", "jpg", "png","GIF","JPEG", "JPG", "PNG");
			if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != '')
			{
				$mainImageName = saveImage($mainImage,400,$imgPath,$ext);
				if($mainImageName !== false){
					$addData['user_pic'] = $mainImageName;
				}
			}
			$addData['status'] = $this->input->post('status');
			//var_dump($addData['user_pic']);
			if(is_numeric($catId) && $catId != '')
			{ /*when update*/
				$addData['user_id'] = $catId;
				$addData['user_updated'] = date('Y-m-d H:i:s');
				$data = $this->users_model->updateUsers($addData);
				$notifyMail = 0;
				if($this->input->post('notify_mail') != '')
				$notifyMail = 1;
				//echo $notifyMail;
				// does this contains EL edits??
				//var_dump($this->input->post()); die;
				if(($this->input->post('el_balance') != $this->input->post('el_balance_hidden')) || ($this->input->post('earned_leaves') != $this->input->post('el_earned_leaves_hidden'))){
			       if($this->input->post('el_balance_reason') != ''){
							 $elReason = $this->input->post('el_balance_reason');
						 } else {
							 $elReason = get_el_remarks($this->input->post('el_remarks'));
						 }
						 $addData['total_leaves'] = $this->input->post('total_leaves');
						 $addData['earned_leaves'] = $this->input->post('earned_leaves');
						 $addData['el_balance'] = $this->input->post('el_balance');
						 $addData['elReason'] = $elReason;
						 $checkData = $this->users_model->updateELRecords($addData['user_id'], $this->input->post('el_balance'),$this->input->post('earned_leaves'), $elReason);
						 if($checkData && $notifyMail){
							 //TODO : Send Email for the reason behind the EL updates
							 $finalEmailHtml = $this->load->view("superadmin/users/EL_notify_tmp",$addData, true);
			 				$subject="EL records updated ";
			 				$toEmail = $this->input->post('email');
			 				send_mail($toEmail, $subject, $finalEmailHtml);
						 }
					}
				$this->session->set_flashdata('success','User details updated successfully!');
			}
			else
			{
				/*-------------------sending email to the new users-------------*/
				$finalEmailHtml = $this->load->view("superadmin/users/newuser_tmp",$addData, true);
				$subject="Welcome User";
				$toEmail = $this->input->post('email');
				if(send_mail($toEmail, $subject, $finalEmailHtml));
				{
					$addData['user_created'] = date('Y-m-d H:i:s');
					$addData['user_updated'] = date('Y-m-d H:i:s');
					$data = $this->users_model->add($addData);
					$role = getAllRoleName($addData['role_id']);
					//Send new Joining detail to Managentment
					$mailData['body'] = '<tr>
																 <td colspan="2" style="font-size:14px; color:#FFF;
																	 padding:0 40px 20px 40px;">We have following new member joined as a part of our Lets Nurture team.</td>
															 </tr>
															 <tr>
																	 <td colspan="2" style="font-size:14px; color:#FFF;
																		 padding:0 40px 20px 40px;">Name: <b
																			 style="color:#03A9F4;">'.$addData['first_name'].' '.$addData['last_name'].'</b></td>
															 </tr>
															 <tr>
																 <td colspan="2" style="font-size:14px; color:#FFF;
																	 padding:0 40px 20px 40px;">Joining Date: <b
																		 style="color:#03A9F4;">'.$addData['joining_date'].'</b></td>
															 </tr>
															 <tr>
																	 <td colspan="2" style="font-size:14px; color:#FFF;
																		 padding:0 40px 20px 40px;">Role: <b
																			 style="color:#03A9F4;">'.$role->role_name.'</b></td>
															 </tr>
															 <tr>
																	 <td colspan="2" style="font-size:14px; color:#FFF;
																		 padding:0 40px 20px 40px;">Email: <b
																			 style="color:#03A9F4;">'.$addData['email'].'</b></td>
																 </tr>';
					$mailData['name'] = 'Team';
					$managementEmails = getmanagementEmails();
					$this->email->from('hrms@letsnurture.com', "HRMS");
					$this->email->to('hrms@letsnurture.com');
          $this->email->bcc($managementEmails);
					$this->email->subject('Welcome to New Employee');
					$body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
					$this->email->message($body);
					//$this->email->send(FALSE);

					/*Send push Notification*/
					//Send Alert to TLs , PM & HR.
					$deviceTokens = getmanagementTokens();
					$Tokenlist = array();
					foreach($deviceTokens as $value){
						array_push($Tokenlist,$value->deviceToken);
					}
					$notification_message = 'New member '.$addData['first_name'].' '.$addData['last_name'].' joining on '.$addData['joining_date'];
					SendNotificationFCMWeb('Welcome to New Employee',$notification_message,$Tokenlist);
					/*Send push Notification*/

					$this->session->set_flashdata('success','User inserted successfully!');
				}
			}

			redirect(base_url().'superadmin/users');exit;
		}

	   	$this->template->load_partial('superadmin_master','users/add_edit_users',$data);
	}

	function ff($dd)
	{
		echo "<pre>";
		print_r($_POST);
		exit;
	}

	function edit($distId = '')
	{
		$state = '';
		$city = '';
		$data['titalTag'] = ' - Edit Users';
		$data["state"] = $this->users_model->geteditstate($distId);
		$data["city"] = $this->users_model->geteditcity($distId);
		$data['usersdata'] = $this->users_model->getUsers($distId);
		$this->template->load_partial('superadmin_master','users/add_edit_users',$data);

	}
	public function checkUsername()
	{
		$data['username'] = $_POST['username'];
		$check = $this->users_model->verify_username($data);
		echo $check;
		exit;
	}

	public function getStates()
	{
		$country_id = $this->input->post('country_id');
		$state_dropdown = $this->users_model->getState_drop($country_id);

		echo $state_dropdown;
	}
	public function getCities()
	{
		$state_id = $this->input->post('state_id');
		$city_dropdown = $this->users_model->getCity_drop($state_id);

		echo $city_dropdown;
	}
	public function delete($catId)
	{
		$addData['status'] = "Inactive";
		$addData['user_id'] = $catId;
		$data = $this->users_model->updateUsers($addData);
		$this->users_model->deleteUsers($catId);

		$this->session->set_flashdata('success','User deleted successfully!');
		redirect(base_url().'superadmin/users');
		exit;
	}
	public function view()
	{
		$id = $this->input->post('id');
		$data["user"] = $this->users_model->getUserInfo($id);
		$data["skills"] = $this->users_model->getUserSkills($id);
		$this->load->view('superadmin/users/view_user',$data);
	}
	public function leavestatus()
	{
		$id = $this->input->post('id');
		$data["userid"] = $id;
		$this->load->view('superadmin/users/leave_status',$data);
	}
	public function sendLoginInfo()
	{
		$user_id = $this->input->post('id');
		$userDetails = $this->users_model->fetchCredentials($user_id);
		$mailData['body'] = '<tr>
													<td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
														 Please find your credentials below
													 </td>
											 </tr>
											 <tr>
													<td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">UserName: <b style="color:#03A9F4;">'.$userDetails[0]->username.'</b></td>
											 </tr>
											 <tr>
													<td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">Password: <b style="color:#03A9F4;">'.$userDetails[0]->password.'</b></td>
											 </tr>';
		$mailData['name'] = $userDetails[0]->first_name.' '.$userDetails[0]->last_name;

		$this->email->from('hrms@letsnurture.com', "HRMS");
		$this->email->to($userDetails[0]->email);
		$this->email->subject("Hrms credentials");
		$body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
		$this->email->message($body);
		// if($this->email->send()){
		// 	echo "1";
		// }else {
		// 	echo "0";
		// }
		echo "1";
	}

	public function loadtable(){
		print_r($_POST);
		exit;
	}

	function document($distId = ''){
		$data['titalTag'] = ' - Document Users';
		$data["documentDatas"] = $this->users_model->getDocument($distId);
		$this->template->load_partial('superadmin_master','users/document',$data);
	}

	function deleteDocument(){
		if($_POST['document_id'] !== false){
			$data = $this->users_model->deleteDocument($_POST['document_id']);
		}
	}

	/**
	* Developed By: Kaushal
	* Date : 25-Aug-2020
	*
	*/
	function exportUsersData(){
		$userIdList = $this->users_model->getAllUsers("",10000,0); // get All user id
		$fileName = 'All-Employee-Data.xlsx';
    include 'PHPExcel/PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $i = 1;
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'Full Name')
		->setCellValue('B'.$i, 'Personal Email')
		->setCellValue('C'.$i, 'Offic Gmail Id')
		->setCellValue('D'.$i, 'Alternate Email')
		->setCellValue('E'.$i, 'Mobile Number')
		->setCellValue('F'.$i, 'Landline Number')
		->setCellValue('G'.$i, 'Gender')
		->setCellValue('H'.$i, 'Date Of Birth')
		->setCellValue('I'.$i, 'Anniversary')
		->setCellValue('J'.$i, 'Address')
		->setCellValue('K'.$i, 'Department')
		->setCellValue('L'.$i, 'Role')
		->setCellValue('M'.$i, 'Blood Group')
		->setCellValue('N'.$i, 'Pancard No')
		->setCellValue('O'.$i, 'Aadharcard No')
		->setCellValue('P'.$i, 'Pf. No')
		->setCellValue('Q'.$i, 'Driving License Number')
		->setCellValue('R'.$i, 'Employee Insurance Number')
		->setCellValue('S'.$i, 'ESIC Number')
		->setCellValue('T'.$i, 'Bank Name')
		->setCellValue('U'.$i, 'Account Number')
		->setCellValue('V'.$i, 'IFSC Code')
		->setCellValue('W'.$i, 'Branch Name')
		->setCellValue('X'.$i, 'Religion')
		->setCellValue('Y'.$i, 'Cast')
		->setCellValue('Z'.$i, 'User Created Date');
		$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFont()->setBold(true);
    foreach ($userIdList as $key => $value){
			$i++;
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i,$value->first_name.' '.$value->middle_name.' '.$value->last_name)
			->setCellValue('B'.$i, $value->email)
			->setCellValue('C'.$i, $value->gmail_id)
			->setCellValue('D'.$i, $value->alternate_email)
			->setCellValue('E'.$i, base64_decode($value->mobile))
			->setCellValue('F'.$i, $value->landline)
			->setCellValue('G'.$i, $value->gender)
			->setCellValue('H'.$i, date('d-m-Y',strtotime($value->birthdate)))
			->setCellValue('I'.$i, date('d-m-Y',strtotime($value->anniversary)))
			->setCellValue('J'.$i, $value->street.' '.$value->zipcode)
			->setCellValue('K'.$i, $value->department_name)
			->setCellValue('L'.$i, $value->role_name)
			->setCellValue('M'.$i, $value->blood_group)
			->setCellValue('N'.$i, base64_decode($value->pancard_no))
			->setCellValue('O'.$i, base64_decode($value->aadhar_no))
			->setCellValue('P'.$i, base64_decode($value->pf_no))
			->setCellValue('Q'.$i, base64_decode($value->license))
			->setCellValue('R'.$i, base64_decode($value->employee_insurance_number))
			->setCellValue('S'.$i, base64_decode($value->ESIC_number))
			->setCellValue('T'.$i, base64_decode($value->bank_name))
			->setCellValue('U'.$i, base64_decode($value->account_number))
			->setCellValue('V'.$i, base64_decode($value->ifsc_code))
			->setCellValue('W'.$i, base64_decode($value->branch))
			->setCellValue('X'.$i, $value->religion_name)
			->setCellValue('Y'.$i, $value->cast_name)
			->setCellValue('Z'.$i, date('d-m-Y',strtotime($value->user_created)));
    }
    $time = date("d-m-Y");
    $objPHPExcel->getActiveSheet()->setTitle('Employee Data');
    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="All-Employee-Data-'.$time.'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    die;
	}

}

?>
