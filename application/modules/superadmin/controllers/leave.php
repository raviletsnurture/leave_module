<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Leave extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->helper('general_helper');
		$this->load->model('leave_model');
		sAdmin_auth('0');
	}
	function index(){				
		$data['titalTag'] = ' - Request';
		$data["leave"] = $this->leave_model->getAllLeave();
		$this->template->load_partial('superadmin_master','leave/list_leave',$data);
	}
	function leaveListByMonth(){

		$leave_status = getAllLeaveStatus();
		$leave = $this->leave_model->leaveListByMonth($_POST['month']);		
		foreach($leave as $row){ ?>
		<tr>
			<td><?php echo $row->first_name.' '.$row->last_name;?></td>
			<td><?php echo $row->department_name;?></td>
			<td title="<?php echo $row->leave_reason; ?>"><?php echo $row->ltype; ?></td>
			<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_start_date))."</span>". date("d-m-Y", strtotime($row->leave_start_date)) ?></td>
			<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_end_date))."</span>". date("d-m-Y", strtotime($row->leave_end_date)) ?></td>
			<td><?php echo "<span style='display:none;'>".date("Y/m/d", strtotime($row->leave_created))."</span>". date("d-m-Y", strtotime($row->leave_created)) ?></td>
			<td>
			<?php if($row->is_cancelled == 1) { echo "Cancelled - self"; } else {?>
			<select id="leave_status" name="leave_status" onchange="stat(this)">
				<option value="">--Select--</option>
				<?php foreach($leave_status as $bow){ ?>
				<option value="<?php echo $bow->leave_status_id; ?>,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>" <?php if($bow->leave_status_id == $row->leave_status) { ?>selected="selected"<?php } ?>><?php echo $bow->leave_status;?></option>
				<?php } ?>
			</select>
			<?php } //echo $row->lstatus; ?>
		</td>
		<td><?php echo $row->af.' '.$row->al; ?></td>
		<td><?php if($row->status == '' && $row->lstatus != 'Pending') { ?>
			<select name="status" id="status" onchange="ff(this)">
				<option value="">Select Status</option>
				<option value="Paid,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>">Paid</option>
				<option value="Unpaid,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>">Unpaid</option>
			</select>
			<?php } else { echo $row->status; } ?></td>
			<td>
				<a href="<?php echo base_url()?>superadmin/leave/edit/<?php echo $row->leave_id;?>" >
				<button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Leave" title=""><i class="fa fa-pencil"></i></button>
				</a>
				<a href="<?php echo base_url()?>superadmin/leave/delete/<?php echo $row->leave_id;?>" class="deleteRec">
				<button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Leave"><i class="fa fa-trash-o "></i></button>
				</a>
				</td>
		</tr>
		<?php }
	}
	function add($leaveId = ''){
		$data['titalTag'] = ' - Add Request';
		if($this->input->post("addEditLeave") !== false)
		{  
			$addData['user_id'] = $this->input->post('user_id');
			$addData['leave_type'] = $this->input->post('leave_type');
			$addData['leave_status'] = $this->input->post('leave_status');
			$addData['leave_reason'] = $this->input->post('leave_reason');
			$addData['leave_start_date'] = $this->input->post('leave_start_date');
			$addData['leave_end_date'] = $this->input->post('leave_end_date');
			$addData['leave_days'] = getDaysCount($addData['leave_start_date'],$addData['leave_end_date']);
            
			
			//$addData['status'] = $this->input->post('status');
			$userDetails =  getUserDetail($addData['user_id']);
			$addData['department_id'] = $userDetails->department_id;
			if(is_numeric($leaveId) && $leaveId != '')
			{ /*when update*/
				$addData['leave_id']= $leaveId;
				//$addData['leave_updated'] = date('Y-m-d H:i:s');
				$data = $this->leave_model->updateLeave($addData);
				$this->session->set_flashdata('success','Leave updated successfully!');
			}else{
				//$addData['leave_created'] = date('Y-m-d H:i:s');
				//$addData['leave_updated'] = date('Y-m-d H:i:s');
				$data = $this->leave_model->add($addData);
				$this->session->set_flashdata('success','Leave Added successfully!');
			}
            
            redirect(base_url().'superadmin/leave');exit;	
			
		}

	   	$this->template->load_partial('superadmin_master','leave/add_edit_leave',$data);
	}
	public function edit($leaveId)
	{
		$data['titalTag'] = ' - Request';
		$data['leaveEdit'] = $this->leave_model->getLeave($leaveId);
		$this->template->load_partial('superadmin_master','leave/add_edit_leave',$data);
	}
	public function updatestaus()
	{
		$data['leave_id'] = $this->input->post('leave_id');
		$data['status'] = $this->input->post('status');

		$this->leave_model->updateLeave($data);

		if($this->input->post('status') == 'Paid')
		{
			$data['user_id'] = $this->input->post('user_id');
			$up = $this->leave_model->updateleavecount($data);
		}
	}
	public function up_staus()
	{
		$data['leave_id'] = $this->input->post('leave_id');
		$data['leave_status'] = $this->input->post('status');
		$this->leave_model->updateLeave($data);
	}
	public function delete($leaveId)
	{
		$this->leave_model->deleteLeave($leaveId);

		$this->session->set_flashdata('success','Leave deleted successfully!');
		redirect(base_url().'superadmin/leave');
		exit;
	}
     
    public function leave_filter(){    	
        $from_date = $this->input->post('appliedStartDate');
		$to_date = $this->input->post('appliedEndDate');	
		if(!empty($from_date) && !empty($to_date)){
		   $data["leave"] = $this->leave_model->getAllFilterLeave($from_date,$to_date);	
		}else{
		   $data["leave"] = $this->leave_model->getAllLeave();	
		}	
		$html = $this->load->view('leave/filter_list_leave',$data, TRUE);
		echo json_encode(array('table' => $html));		
    } 
	
}
?>
