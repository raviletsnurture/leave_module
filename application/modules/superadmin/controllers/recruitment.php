<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recruitment extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->helper('general_helper');
		$this->load->model('recruitment_model');
		$this->load->library('email');
		sAdmin_auth('0');
	}

	/**
	* Developer Name : parth patwala
	* Date : 22-Jun-2017
	* Description : Index page
	*
	* @return view
	*/
	function index()
	{
		// getting the date from the url

		// $url = parse_url($_SERVER['REQUEST_URI']);
		$data['titalTag'] = '- Recruitment';
		$designationCountArray = $this->recruitment_model->getDesignationCount();

		foreach ($designationCountArray as $key=>$value) {
		    if(is_null($value->designation) || $value->designation == '')
		        unset($designationCountArray[$key]);
		}
		$data['designationCountArray'] =$designationCountArray;
		$data['recruitmentData'] = $this->recruitment_model->getRecruitmentData();
		$data['AllActiveUsers'] = getAllUsers();
		// if($url['query'] != NULL && defined($url['query']) && !isEmpty($url['query'])){
		// 	$data['dateUrl'] = $url['query'];
		// }

		$this->template->load_partial('superadmin_master', 'recruitment/index', $data);
	}

    function recruitment_filter()
	{
		$data['titalTag'] = '- Recruitment';
		$designationCountArray = $this->recruitment_model->getDesignationCount();

		foreach ($designationCountArray as $key=>$value) {
		    if(is_null($value->designation) || $value->designation == '')
		        unset($designationCountArray[$key]);
		}
		$data['designationCountArray'] =$designationCountArray;

		$from_date = $this->input->post('appliedStartDate');
		$to_date = $this->input->post('appliedEndDate');
		if(!empty($from_date) && !empty($to_date)){
		   $data['recruitmentData'] = $this->recruitment_model->getRecruitmentFilterData($from_date,$to_date);
		}else{
		   $data['recruitmentData'] = $this->recruitment_model->getRecruitmentData();
		}

		$html = $this->load->view('recruitment/filter_list',$data, TRUE);
		echo json_encode(array('table' => $html));
	}

	/**
	* Developer Name : parth patwala
	* Date : 22-Jun-2017
	* Description : View Record Detail
	*
	* @return view
	*/
	function view()
	{
		$userId = $this->input->post('id');
		$data['userData'] = $this->recruitment_model->getRecruitmentDataOfUser($userId);
		$this->load->view('superadmin/recruitment/view_user', $data);
	}

	/**
	* Developer Name : Jignasa Sutariya
	* Date : 08-11-2017
	* Description : View User Comment Detail
	*
	* @return view
	*/
	function blockUser()
	{
		$userId = $this->input->post('id');

		if($this->recruitment_model->blockUser($userId))
		{
			echo 1;
		}else{
			echo 0;
		}

	}

	function unblockUser()
	{
		$userId = $this->input->post('id');

		if($this->recruitment_model->unblockUser($userId))
		{
			echo 2;
		}else{
			echo 0;
		}

	}

	/**
	* Developer Name : parth patwala
	* Date : 22-Jun-2017
	* Description : Send confirmation Email
	*
	* @return view
	*/
	function sendConfirmationEmail(){
		$userId = $this->input->post('id');
		$confirmDate = $this->input->post('date');
		$interview_mode = $this->input->post('interview_mode');
		$date = new DateTime($confirmDate);
		$data['id']=$userId;
		$data['interview_date']=$date->format('Y-m-d H:i:s');
		$arrayInterviewersID = $this->input->post('interviewers');
		$interviewers = implode(', ', $arrayInterviewersID);
		$userData = $this->recruitment_model->getRecruitmentDataOfUser($userId);

		$interviewSechdule = $this->recruitment_model->updateInterviewSechduleDate($data);
		if($interviewSechdule){
			$body = '
				<!DOCTYPE html>
				<html>
					<p> Hello '.$userData[0]->fullName.', </p>
					<p>Hope you are doing great.</p>
					<p>We have received your application, We are glad to notify you that you are selected for our recruitment process.</p>
					<p>Your interview is scheduled as per below: </p>
		    		<body>
				        <table cellpadding=5 cellspacing=0 style="width:100%" border:1px solid #000;>
				          <tr>
				            <th style="padding:5px; text-align:left; background:#4f81bd; ">Date</th>
				            <th style="padding:5px; text-align:left; background:#4f81bd; ">'.$date->format('d-M-Y').' ('.$date->format('D').')</th>
				          </tr>
				          <tr>
				            <td style="padding:5px; text-align:left; border:1px solid #000;">Time</td>
				            <td class="new" style="width:70%; border:1px solid #000;">'.$date->format('g:i a').'</td>
				          </tr>
				          <tr>
				            <td style="padding:5px; text-align:left; border:1px solid #000;">Venue</td>
				            <td class="new" style="width:70%; border:1px solid #000;">A- 103/C, Ganesh Meridian, Opp. Gujarat High Court, S.G. Highway, Ahmedabad- 380060
				            (O): 079 40095953</td>
				          </tr>
				          <tr>
				            <td style="padding:5px; text-align:left; border:1px solid #000;">Interview Mode</td>
				            <td class="new" style="width:70%; border:1px solid #000;">'.$interview_mode.'</td>
				          </tr>
				          <tr>
				            <td style="padding:5px; text-align:left; border:1px solid #000;">Contact Person</td>
				            <td class="new" style="width:70%; border:1px solid #000;">Komal Thakkar</td>
				          </tr>
				        </table>
						<p>You are requested to kindly confirm your availability as per above mentioned schedule for the interview through revert email.</p>
					    <p>Looking forward to hear from your side. </p>
					    <p>Regards</p>
					    <p>HR</p>
		    		</body>
	    		</html>
	        ';

			$this->email->from('hrms@letsnurture.com', "HRMS");
			$this->email->to($userData[0]->email);
			$this->email->reply_to('career@letsnurture.com');
			$this->email->bcc('hrms@letsnurture.com');
			$this->email->subject("Recruitment Confirmation Email");
			$this->email->message($body);
			$this->email->send();
			$userData1 = $this->recruitment_model->getRecruitmentDetailsOfUser($userId, $interviewers);
			$insert_id = $this->db->insert_id();
			$getEmailOfinterviewer = $this->recruitment_model->getEmailOfinterviewer($arrayInterviewersID);
			$impGetEmailOfinterviewer = implode(', ',$getEmailOfinterviewer);
			$getCandidateDetail = $this->recruitment_model->getCandidateDetail($insert_id);
			$mailData['name'] = '';
			$mailData['body'] ='
						<tr>
							<td>
							<table style="border-collapse: collapse">
								<tr>
									<td colspan="2"><p style="color:white">An interview has been scheduled with the following candidate</p></td>
								</tr>
								<tr>
									<th style="text-align:left; padding:5px; color:white; border:1px solid white">Candidate name</th>
									<td style="padding:5px; color:white; border:1px solid white">'.$getCandidateDetail[0]["candidate_name"].'</td>
								</tr>
								<tr>
									<th style="text-align:left; padding:5px; color:white; border:1px solid white">Qualification</th>
									<td style="padding:5px;color:white; border:1px solid white">'.$getCandidateDetail[0]["qualification"].'</td>
								</tr>
								<tr>
									<th style="text-align:left; padding:5px; color:white; border:1px solid white">Applied Position</th>
									<td style="padding:5px;color:white; border:1px solid white">'.$getCandidateDetail[0]["applied_position"].'</td>
								</tr>
								<tr>
									<th style="text-align:left; padding:5px; color:white; border:1px solid white">Total Experience(in year)</th>
									<td style="padding:5px;color:white; border:1px solid white">'.$getCandidateDetail[0]["total_experience"].'</td>
								</tr>
								<tr>
									<th style="text-align:left; padding:5px; color:white; border:1px solid white">Interview date</th>
									<td style="padding:5px;color:white; border:1px solid white">'.$getCandidateDetail[0]["interview_date"].'</td>
								</tr>
								<tr>
									<th style="text-align:left; padding:5px; color:white; border:1px solid white">Resume</th>
									<td style="padding:5px; color:white; white; border:1px solid white"><a href="http://recruitment.letsnurture.com/CV/'.$getCandidateDetail[0]["resume"].'"  target="_blank"><button class="btn btn-danger btn-xs tooltips" data-original-title="Download&nbsp;PDF" title="Download&nbsp;PDF">view resume</button></a>
									</td>
								</tr>
							</table>

							</td>

						</tr>
						';
			$this->email->from('hrms@letsnurture.com', "HRMS");
			$this->email->to('hrms@letsnurture.com');
			$this->email->bcc($impGetEmailOfinterviewer);
			$this->email->subject("New Interview Schedule");
			$body2 = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
			$this->email->message($body2);
			if($this->email->send()){
				echo "1";
			}else{
				echo "0";
			}
		}else {
			echo "0";
		}
	}
	/**
	* Developer Name : parth patwala
	* Date : 22-Jun-2017
	* Description : Download csv of Recruitment table
	*
	* @return view
	*/
	function exportCsv()
	{

		$fileName = 'Users.xls';
		//** Excel Code starts here
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

		header("Content-Disposition: attachment; filename=\"$fileName\"");
		header("Cache-Control: max-age=0");
		// Error reporting
		error_reporting(E_ALL);
		// Include path
		ini_set('include_path', ini_get('include_path').';../Classes/');
		// PHPExcel
		include 'PHPExcel/PHPExcel.php';
		// PHPExcel_Writer_Excel2007
		include 'PHPExcel/PHPExcel/Writer/Excel2007.php';
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("LetsNurture");
		$objPHPExcel->getProperties()->setLastModifiedBy("LetsNurture");
		$objPHPExcel->getProperties()->setTitle("HRMS Feedback Sheet");
		$objPHPExcel->getProperties()->setSubject("HRMS Feedback Sheet");
		$objPHPExcel->getProperties()->setDescription("HRMS Feedback Sheet of Employee");
		$objPHPExcel->createSheet(0);
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('User Details');

		// Styling

		// $objPHPExcel->getActiveSheet()->getStyle("1")->getFont()->setBold(true);
		// $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);

		$alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$fieldsArray = array('ID','Full Name','Email','Phone','Current Location', 'Preferred Location','Key skills', 'Experience', 'Current Designation','Current Employer',
			'Required Designation', 'Current Salary', 'Qualification', 'Institution', 'Resume', 'Created Date', 'IP Address', 'Browser Login');

		$totalFields = count($fieldsArray);

		for ($i=0; $i <$totalFields ; $i++) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($alpha[$i])->setWidth(20);
			$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].'1', $fieldsArray[$i]);
		}

		$recruitmentData = $this->recruitment_model->getRecruitmentData();

		if(!empty($recruitmentData)) {
			$fieldsCount = 2;
			foreach ($recruitmentData as $data) {
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fieldsCount, $data->rec_id);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fieldsCount, $data->fullName);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$fieldsCount, $data->email);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$fieldsCount, $data->phone);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$fieldsCount, $data->currentLocation);
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$fieldsCount, $data->preferredLocation);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$fieldsCount, $data->keySkills);
				$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fieldsCount, $data->experience);
				$objPHPExcel->getActiveSheet()->SetCellValue('I'.$fieldsCount, $data->currentDesignation);
				$objPHPExcel->getActiveSheet()->SetCellValue('J'.$fieldsCount, $data->currentEmployer);
				$objPHPExcel->getActiveSheet()->SetCellValue('K'.$fieldsCount, $data->requiredDesignation);
				$objPHPExcel->getActiveSheet()->SetCellValue('L'.$fieldsCount, $data->salary);
				$objPHPExcel->getActiveSheet()->SetCellValue('M'.$fieldsCount, $data->qualification);
				$objPHPExcel->getActiveSheet()->SetCellValue('N'.$fieldsCount, $data->institution);
				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$fieldsCount, $data->file);
				$objPHPExcel->getActiveSheet()->SetCellValue('P'.$fieldsCount, $data->createdDate);
				$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$fieldsCount, $data->ipAddress);
				$objPHPExcel->getActiveSheet()->SetCellValue('R'.$fieldsCount, $data->browserlogin);
				$fieldsCount++;
			}
		}

		//Save Excel 2007 file
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
		$objWriter->save("php://output");
		die;
	}

    function exportdata()
	{

	   // file name
	   $filename = 'users_'.date('Ymd').'.csv';
	   header("Content-Description: File Transfer");
	   header("Content-Disposition: attachment; filename=$filename");
	   header("Content-Type: application/csv; ");

	   // get data
	   $usersData = $this->recruitment_model->getRecruitmentDetails();
	   // file creation
	   $file = fopen('php://output', 'w');
	   $header = array("EMAIL","NAME","ADDRESS","PHONE");
	   fputcsv($file, $header);
	   foreach ($usersData as $key=>$line){
	     fputcsv($file,$line);
	   }
	   fclose($file);
	   exit;

	}


	/* current opening */
	function jobopening(){
		$data['titalTag'] = ' - Job';
		if($this->input->post("addEdittechnology") !== false)
		{
			$addData['technology'] = $this->input->post('technology');
			$data = $this->recruitment_model->addTechnology($addData);
			$this->session->set_flashdata('success','Technology inserted successfully!');
			redirect(base_url().'superadmin/recruitment/jobopening');exit;
		}
		if($this->input->post("addEditvacancy") !== false)
		{
			$addData2['technology_name'] = $this->input->post('technology_name');
			$addData['vacancy'] = $this->input->post('vacancy');
			$data = $this->recruitment_model->addVacancy($addData,$addData2);
			$this->session->set_flashdata('success','Vacancy inserted successfully!');
			redirect(base_url().'superadmin/recruitment/jobopening');exit;
		}
		$data["total_job"] = $this->recruitment_model->getAllJob();
		$data["jobOpening"] = $this->recruitment_model->getAllJobOpening();
		$this->template->load_partial('superadmin_master','recruitment/add_edit_job_opening',$data);
	}

	function jobdelete($Id)
	{
		$this->recruitment_model->jobdelete($Id);
		$this->session->set_flashdata('success','Job deleted successfully!');
		redirect(base_url().'superadmin/recruitment/jobopening');
		exit;
	}

    //  For Serverside pagination

    /**
	* Developer Name : dipika patel
	* Date : 10-July-2019
	* Description : View Server Side Progressive Main
	*
	* @return view
	*/
	function recruitments()
	{

		$data['titalTag'] = '- Recruitment';
		$designationCountArray = $this->recruitment_model->getDesignationCount();
		$opt = array('' => 'All Designation');
		foreach ($designationCountArray as $key=>$value) {
		    if(is_null($value->designation) || $value->designation == ''){
              unset($designationCountArray[$key]);
			}else{
				 $opt[$value->designation] = $value->designation.'('.$value->designationCount.')';
			}
		}
		$data['designationCountArray'] = form_dropdown('',$opt,'','id="designation" class="form-control"');
		$data['AllActiveUsers'] = getAllUsers();

		$this->template->load_partial('superadmin_master','recruitment/recruitment_list',$data);
	}

	/**
	* Developer Name : dipika patel
	* Date : 10-July-2019
	* Description : View Server Side Progressive Detail
	*
	* @return view
	*/
	public function ajax_list()
    {

		$list = $this->recruitment_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {

			if($customers->isBlocked==1) { $userblock = 'Unblock'; } else {  $userblock = 'Block'; }
			$interview_date = '';
			if(empty($customers->interview_date)){
				$interview_date = '<a value="'.$customers->rec_id.'" onclick="reply_click(this.id)" href="javascript:;" id="'.$customers->rec_id.'" >
							<button class="btn btn-success btn-xs tooltips" data-original-title="Schedule an interview and send the mail to the candidate" title="Schedule an interview and send the mail to the candidate"><i class="fa fa-share"></i></button>
						</a>';
			}
			$no++;
			$new="";$notResponding="";$notReachable="";$selected="";$nextRound="";$rejected="";$onHold="";
			if($customers->recStatus == 1){
				$new = "selected";
			}elseif($customers->recStatus == 2){
				$notResponding = "selected";
			}elseif($customers->recStatus == 3){
				$notReachable = "selected";
			}elseif($customers->recStatus == 4){
				$selected = "selected";
			}elseif($customers->recStatus == 5){
				$nextRound = "selected";
			}elseif($customers->recStatus == 6){
				$rejected = "selected";
			}else{
				$onHold = "selected";
			}
            $row = array();
            $row[] = $customers->fullName;
            $row[] = $customers->email;
            $row[] = $customers->phone;
            $row[] = $customers->experience;
            $row[] = $customers->designation;
			$row[] = $customers->currentDesignation;
			$row[] = (!empty($customers->interview_date)) ? $customers->interview_date : 'Not Scheduled';
			$row[] = date("Y/m/d",strtotime($customers->createdDate));

			$row[] = '<select name="applicantStatus" onchange=changeApplicantStatus(this,'.$customers->rec_id.') >
				<option '.$new.' value="1">New</option>
				<option '.$notResponding.' value="2">Not responding</option>
				<option '.$notReachable.' value="3">Not reachable</option>
				<option '.$selected.' value="4">Selected</option>
				<option '.$nextRound.' value="5">Next round</option>
				<option '.$rejected.' value="6">Rejected </option>
				<option '.$onHold.' value="7">On hold</option>
			</select>';
			$row[] = '<a value='.$customers->rec_id.'" onclick="getchek('.$customers->rec_id.')" href="#" >
						<button class="btn btn-primary btn-xs tooltips" data-original-title="View&nbsp;User" title="View&nbsp;User"><i class="fa fa-eye"></i></button>
					</a>
					<a href="http://recruitment.letsnurture.com/CV/'.$customers->file.'"  target="_blank">
						<button class="btn btn-danger btn-xs tooltips" data-original-title="Download&nbsp;PDF" title="Download&nbsp;PDF"><i class="fa fa-download"></i></button>
					</a>
					'.$interview_date.'
					<a value="'.$customers->rec_id.'" onclick="block_user(this); return false;" href="javascript:;" id="'.$customers->rec_id.'" blockStatus="'.$customers->isBlocked.'" >
						<button class="btn btn-danger btn-xs tooltips" data-original-title="'.$userblock.'" title="'.$userblock.'"><i class="fa fa-close"></i></button>
					</a>';
            $data[] = $row;
        }

        $output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->recruitment_model->count_all(),
					"recordsFiltered" => $this->recruitment_model->count_filtered(),
					"data" => $data
                );
        echo json_encode($output);
	}
	function sendRecruitmentMailToAll(){
		$mails = array();
		$subject = $this->input->post('subject');
		$message = $this->input->post('data');

		$start = 0;
		$count = $this->recruitment_model->getTotalRecruitmentData();
		$loop = (int)($count/500);

		$mailData['body']='<tr><td style="color:white;padding:0 40px;"><p>'.$message.'</p></td></tr>';
		$this->email->from('hrms@letsnurture.com', "HRMS");
		$this->email->to('hrms@letsnurture.com');
		$this->email->subject($subject);
		$body = $this->load->view('mail_layouts/recruitmentMail.php', $mailData, TRUE);

		for($i=0; $i<=$loop; $i++){
			$result = $this->recruitment_model->getRecruitmentDataWithPaginate($start);
			$this->email->bcc($result[0]->email);
			$this->email->message($body);
			if($this->email->send()){
				$response = 1;
			}
			else {
				$response = 2;
			}
			$start=$start+500;
		}
		echo $response;
	}

	function changeApplicantStatus(){
		$data = $this->recruitment_model->updateApplicantStatus($_POST);
		if($data){
			echo 1;
		}else{
			echo 2;
		}
    }
}
?>
