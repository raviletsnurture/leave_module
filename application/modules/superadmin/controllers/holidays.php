<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Holidays extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('holidays_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Holidays List';
		$data["fixedHolidays"] = $this->holidays_model->getAllFixedHolidays();
		$data["flexibleHolidays"] = $this->holidays_model->getAllFlexibleHolidays();
		$data["swappedHolidays"] = $this->holidays_model->getAllSwappedHolidays();
	  $this->template->load_partial('superadmin_master','holidays/index',$data);
	}

	function add($holidaysId = ''){
		$data['titalTag'] = ' - Add Holidays';
		if($this->input->post("addEditHoliday") !== false){
				$addData['festival_date'] = date("Y-m-d H:i:s",strtotime($this->input->post('holidayDate')));
				$addData['festival_name'] = $this->input->post('holidayName');
				$addData['status'] = $this->input->post('holidaystatus');
				if(is_numeric($holidaysId) && $holidaysId != ''){
				  $addData['id']= $holidaysId;
					$data = $this->holidays_model->updateHoliday($addData);
					$this->session->set_flashdata('success','Holidays updated successfully!');
				}else{
					$data = $this->holidays_model->add($addData);
					$this->session->set_flashdata('success','Holidays Added successfully!');
				}
				redirect(base_url().'superadmin/holidays');exit;
		}
	  $this->template->load_partial('superadmin_master','holidays/add_edit_holidays',$data);
	}

	public function edit($holidaysId){
		$data['titalTag'] = ' - Edit Holidays';
		$data['holidaysEdit'] = $this->holidays_model->getHoliday($holidaysId);
		$this->template->load_partial('superadmin_master','holidays/add_edit_holidays',$data);
	}

	public function delete($holidaysId){
		$this->holidays_model->deleteHoliday($holidaysId);
		$this->session->set_flashdata('success','Holidays deleted successfully!');
		redirect(base_url().'superadmin/holidays');
		exit;
	}
}

?>
