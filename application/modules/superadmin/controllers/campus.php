<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campus extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->helper('general_helper');
		$this->load->model('campus_model');
		$this->load->library('email');
		sAdmin_auth('0');
	}


	function index(){
		$data['titalTag'] = ' - All Users';
		$data["campus"] = $this->campus_model->getAllCampus();		
		$this->template->load_partial('superadmin_master','campus/list_campus',$data);
	}

	function add($catId = '')
	{

		$data['titalTag'] = ' - Campus add';
		if($this->input->post("addEditCampus") !== false)
		{
			$addData['college_name'] = $this->input->post('college_name');
			$addData['university_name'] = $this->input->post('university_name');
			$addData['email'] = $this->input->post('email');
			$addData['alternate_email'] = $this->input->post('alternate_email');
			$addData['mobile'] = $this->input->post('mobile');
			$addData['emergency_number'] = $this->input->post('emergency_number');
			$addData['landline_phone1'] = $this->input->post('landline_phone1');
			$addData['landline_phone2'] = $this->input->post('landline_phone2');			
			$addData['contact_person_name_1'] = $this->input->post('contact_person_name_1');
			$addData['contact_person_name_2'] = $this->input->post('contact_person_name_2');
			$addData['fax'] = $this->input->post('fax');
			$addData['country_id'] = $this->input->post('country_id');
			$addData['state_id'] = $this->input->post('state_id');
			$addData['city_id'] = $this->input->post('city_id');
			$addData['street'] = $this->input->post('street');
			$addData['zipcode'] = $this->input->post('zipcode');
			$addData['gender'] = $this->input->post('gender');			
			$addData['description'] = $this->input->post('description');
			$addData['recruitment_status'] = $this->input->post('recruitment_status');
			$addData['tier'] = $this->input->post('tier');
			$addData['special_reason'] = $this->input->post('special_reason');
			$addData['other_information'] = $this->input->post('other_information');
			//$addData['status'] = $this->input->post('status');
			//var_dump($addData['user_pic']);
			if(is_numeric($catId) && $catId != '')
			{   /*when update*/
				$addData['campus_id'] = $catId;
				$addData['campus_updated'] = date('Y-m-d H:i:s');
				$data = $this->campus_model->updateCampus($addData);
				$notifyMail = 0;
				$this->session->set_flashdata('success','Campus details updated successfully!');
			}
			else
			{
				$addData['campus_created'] = date('Y-m-d H:i:s');
				$addData['campus_updated'] = date('Y-m-d H:i:s');
				$data = $this->campus_model->add($addData);
				$this->session->set_flashdata('success','Campus inserted successfully!');
				
			}
			redirect(base_url().'superadmin/campus');exit;
		}
	   	$this->template->load_partial('superadmin_master','campus/add_edit_campus',$data);
	}

	
	function edit($distId = '')
	{   
		$state = '';
		$city = '';
		$data['titalTag'] = ' - Edit Users';
		$data["state"] = $this->campus_model->geteditstate($distId);
		$data["city"] = $this->campus_model->geteditcity($distId);
		$data['campusdata'] = $this->campus_model->getCampus($distId);
		$this->template->load_partial('superadmin_master','campus/add_edit_campus',$data);

	}
	

	public function getStates()
	{
		$country_id = $this->input->post('country_id');
		$state_dropdown = $this->campus_model->getState_drop($country_id);

		echo $state_dropdown;
	}
	public function getCities()
	{
		$state_id = $this->input->post('state_id');
		$city_dropdown = $this->campus_model->getCity_drop($state_id);

		echo $city_dropdown;
	}
	public function delete($catId)
	{
		$this->campus_model->deleteCampus($catId);

		$this->session->set_flashdata('success','Campus deleted successfully!');
		redirect(base_url().'superadmin/campus');
		exit;
	}
	public function view()
	{
		$id = $this->input->post('id');
		$data["campus"] = $this->campus_model->getCampusInfo($id);		
		$this->load->view('superadmin/campus/view_campus',$data);
	}
	public function loadtable(){
		print_r($_POST);
		exit;
	}

}

?>
