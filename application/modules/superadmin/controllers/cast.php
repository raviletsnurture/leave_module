<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cast extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('cast_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Cast';

		$data["cast"] = $this->cast_model->getAllCast();

	   	$this->template->load_partial('superadmin_master','cast/list_cast',$data);
	}
	function add($catId = ''){

		$data['titalTag'] = ' - cast add';
		if($this->input->post("addEditDepart") !== false)
		{
				$addData['cast_name'] = $this->input->post('cast_name');
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['cast_id']= $catId;
					$data = $this->cast_model->updateCast($addData);
					$this->session->set_flashdata('success','cast updated successfully!');
				}else{

					$data = $this->cast_model->add($addData);
					$this->session->set_flashdata('success','cast Added successfully!');
				}
				redirect(base_url().'superadmin/cast');exit;
		}
		if(is_numeric($catId) && $catId != ''){/* when edit*/
		$data['castEdit'] = $this->cast_model->getCast($catId);
			if(count($data['castEdit']) == 0){
				redirect(base_url().'superadmin/cast');exit;
			}
		$data['titalTag'] = ' - cast Edit';
		$data['actionUrl'] = 'superadmin/cast/add/'.$catId;
		}

	   	$this->template->load_partial('superadmin_master','cast/add_edit_cast',$data);
	}
	public function edit($catId)
	{
		$data['castEdit'] = $this->cast_model->getCast($catId);
		$this->template->load_partial('superadmin_master','cast/add_edit_cast',$data);
	}
	public function delete($catId)
	{
		$this->cast_model->deleteCast($catId);
		$this->session->set_flashdata('success','cast deleted successfully!');
		redirect(base_url().'superadmin/cast');
		exit;
	}
}

?>
