<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documents extends MX_Controller {
	//var $loginUser;
  function __construct()
  {
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('users_model');
		sAdmin_auth('0');
		$this->logSession = $this->session->userdata("sLogin_session");
  }
	function index(){
    $data['titalTag'] = ' - Documents';
		$data["users"] = $this->users_model->getAllUsersName($users='', $limit='1', $start='1',$for='1');
    $documents = $this->users_model->getDocument($id=0);
    $data['documentDatas'] = $documents;
		$this->template->load_partial('superadmin_master','documents/uploaddocument',$data);
	}
	function logout()
	{
		$this->session->unset_userdata("login_session");
		redirect(base_url());
		exit;
	}

	function uploaddocument(){
		$data['userdata'] = $this->users_model->getUserinfo($_POST['user_id']);
		$data['titalTag'] = ' - Upload Document';
		if($this->input->post("document_name") !== false){
					$addData['document_name'] = $this->input->post('document_name');
					$addData['user_id'] = $_POST['user_id'];
					$pdfPath = './uploads/document';
					$mainImage = 'documentFile';
					if($_FILES[$mainImage]['size'] > 2097152){
						$this->session->set_flashdata('error','File too large. File must be less than 2Mb.');
  						redirect(base_url().'superadmin/documents');exit;
					}
					$encodedName = rtrim(strtr(base64_encode(time()), '+/', '-_'), '=');
					$ext = array("pdf","docx","doc","jpg","jpeg","png","odt","ods");
					if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != ''){
						$mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);
						if($mainImageName !== false){
							$addData['file_name'] = $mainImageName;
						}else{
								$this->session->set_flashdata('error','On Snap ! Document not uploaded, verify file and upload again!');
								redirect(base_url().'superadmin/documents');exit;
						}
					} else {
							$this->session->set_flashdata('error','On Snap ! Forgot to upload file? please upload again!');
							redirect(base_url().'superadmin/documents');exit;
					}

					$data = $this->users_model->addDocument($addData);
					$this->session->set_flashdata('success','Document added successfully!');
					redirect(base_url().'superadmin/documents');exit;

		}
	  $this->template->load_partial('superadmin_master','documents/uploaddocument',$data);
	}

	function deleteDocument(){
		if($_POST['document_id'] !== false){
			$data = $this->users_model->deleteDocument($_POST['document_id']);
		}
	}
}
?>
