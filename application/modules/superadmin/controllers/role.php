<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('role_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Role';

		$data["role"] = $this->role_model->getAllRole();

	  $this->template->load_partial('superadmin_master','role/list_role',$data);
	}
	function add($catId = ''){

		$data['titalTag'] = ' - Role add';
		if($this->input->post("addEditRole") !== false)
		{
				$department = implode(",", $_POST['department']);
				$role = implode(",", $_POST['role']);
				$addData['role_name'] = $this->input->post('role_name');
				$addData['status'] = $this->input->post('status');

				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				  $addData['role_id']= $catId;

				  $addData['role_updated'] = date('Y-m-d H:i:s');
					$data = $this->role_model->updateRole($addData);
					$this->session->set_flashdata('success','Role updated successfully!');
				}else{
					$addData['role_created'] = date('Y-m-d H:i:s');
				  $addData['role_updated'] = date('Y-m-d H:i:s');
					$data = $this->role_model->add($addData);
					$this->session->set_flashdata('success','Role Added successfully!');
				}
				redirect(base_url().'superadmin/role');exit;
		}


	   	$this->template->load_partial('superadmin_master','role/add_edit_role',$data);
	}

	function addPermission($catId = ''){


		$data['titalTag'] = ' - Role add';
		$arr = array('leaves','feedback','users');
		$rdata['role_id'] = $catId;

		//$boxes = array('add','edit','delete','view','approve','kra');
		$boxes = array('add','edit','delete','view','approve');

		for($i=0;$i<sizeof($arr);$i++)
		{
			//echo '"'.$arr[$i].'"';
			$mailing = array();


			//$p = !empty($this->input->post($arr[$i])) ? $this->input->post($arr[$i]) : array();

			if($this->input->post($arr[$i]) == '')
			{
				$p = array();
			}
			else
			{
				$p = $this->input->post($arr[$i]);

			}


			foreach($boxes as $v)
			{
				if(array_key_exists($v,$p)){
					$mailing[$v] = trim(stripslashes($p[$v]));
				}else{
					$mailing[$v] = 'no';
				}
			}
			//echo "<pre>";
			//print_r($mailing);

			$rdata['module_name'] = $arr[$i];
			$rdata['module_add'] = $mailing['add'];
			$rdata['module_edit'] = $mailing['edit'];
			$rdata['module_delete'] = $mailing['delete'];
			$rdata['module_view'] = $mailing['view'];
			$rdata['module_approve'] = $mailing['approve'];
			//$rdata['module_kra'] = $mailing['kra'];
			$data = $this->role_model->updatePermission($rdata);
		}

	redirect(base_url().'superadmin/role');exit;

	}
	public function edit($catId)
	{	$data['titalTag'] = ' - Edit Permission';
		$data['roleEdit'] = $this->role_model->getRole($catId);
		$this->template->load_partial('superadmin_master','role/add_edit_role',$data);
	}
	public function delete($catId)
	{
		$this->role_model->deleteRole($catId);
		$this->session->set_flashdata('success','Role deleted successfully!');
		redirect(base_url().'superadmin/role');
		exit;
	}
	public function permission($id)
	{
		$data['titalTag'] = ' - Add Permission';
		$data["role_id"] = $id;
		$data['permission'] = $this->role_model->getPermission($id);
		$this->template->load_partial('superadmin_master','role/permission_role',$data);
	}

	/**
	 * @Method		  :	POST
	 * @Params		  :
	 * @author      : Jignasa
	 * @created		  :	18-07-2017
	 * @Modified by	  :
	 * @Status		  :
	 * @Comment		  : Super Admin can give permission to all users
	 **/
	 function givePermission(){

	  $data['titalTag'] = ' - Give Permission';
	  if($this->input->post("givePermissionBtn") !== false){
			$permission_type = $this->input->post('permission_type');
			if(!empty($_POST['users'])){
				$users = implode(",", $_POST['users']);
			}else{
				$users =NULL;
			}

			if($permission_type === 'leave'){
				$addData['leave_users'] = $users;
			}
			if($permission_type === 'feedback'){
				$addData['feedback_users'] = $users;
			}
			if($permission_type === 'reimbursement'){
				$addData['reimbursement_users'] = $users;
			}

			$user_id = $this->input->post('user_id');
			$data = $this->role_model->givePermission($addData,$user_id);
			if($data){
				echo "1";
			}
	  }else{
			$data["departments"] = $this->role_model->getAllDepartment();
			$this->template->load_partial('superadmin_master','role/give_permission',$data);
	  }
	 }

/**
 * @Method		  :	POST
 * @Params		  :
 * @author      : Jignasa
 * @created		  :	18-07-2017
 * @Modified by	:
 * @Status		  :
 * @Comment		  : Get all users permitted by super admin
 **/
	public function userPermissions()
	{
		$id = $_POST['user_id'];
		$permission_type = $_POST['permission_type'];
		if($id != ''){
				if($permission_type === 'leave'){
					$permission = $this->role_model->getUserPermission($id, 'leave_users');
				}
				if($permission_type === 'feedback')
				{
					$permission = $this->role_model->getUserPermission($id, 'feedback_users');
				}
				if($permission_type === 'reimbursement')
				{
					$permission = $this->role_model->getUserPermission($id, 'reimbursement_users');
				}
			$departments = $this->role_model->getAllDepartment();
		
			if(!empty($permission->leave_users) || !empty($permission->feedback_users) || !empty($permission->reimbursement_users))
			{
					if($permission_type === 'leave'){
						$permissions = $permission->leave_users;
					}
					if($permission_type === 'feedback'){
						$permissions = $permission->feedback_users;
					} 
					if($permission_type === 'reimbursement'){
						$permissions = $permission->reimbursement_users;
					}
					?>
						<label for="users_list" class="control-label col-lg-2">Users List</label>
						<div class="col-lg-10" style="text-align:center;">
							<span class="checkall_label">
								<input type="checkbox" name="check_all" id="check_all" onchange="checkAll()"/> Check All
							</span>
						</div>
						<div class="col-lg-10">
							<?php
							foreach ($departments as $dept) {
									$departmentUsers = $this->role_model->getAllDepartmentUsers($dept->department_id, $id); ?>
								<div class="col-lg-4">
									<input type="checkbox" id="department_id_<?= $dept->department_id ?>" name="department[<?= $dept->department_id ?>]" class="department_id allAction" value="<?= $dept->department_id ?>"  onChange="checkAllDeptUsers(<?= $dept->department_id ?>)">
									<b><?= $dept->department_name ?></b>
									<div class="col-lg-12" style="min-height:200px;">
									<?php foreach ($departmentUsers as $du) {?>
												<input type="checkbox" id="<?= $du->user_id; ?>" name="users[]" class="users_<?= $dept->department_id ?> users col-lg-2" onchange="checkUser(<?= $dept->department_id ?>)" value="<?= $du->user_id; ?>" style="margin: 0px;" <?php if (in_array($du->user_id, explode(",",$permissions))){echo "checked";}?>>

											<span class="action_label col-lg-10">
													<?php echo $du->first_name.' '.$du->last_name; ?>
											</span>
									<?php } ?>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php
				}
				else{?>
					<label for="users_list" class="control-label col-lg-2">Users List</label>
					<div class="col-lg-10" style="text-align:center;">
						<span class="checkall_label">
							<input type="checkbox" name="check_all" id="check_all" onchange="checkAll()"/> Check All
						</span>
					</div>
					<div class="col-lg-10">
						<?php foreach ($departments as $dept) {
							$departmentUsers = $this->role_model->getAllDepartmentUsers($dept->department_id, $id);
							?>
							<div class="col-lg-4">
								<input type="checkbox" id="department_id_<?= $dept->department_id ?>" name="department[<?= $dept->department_id ?>]" class="department_id allAction" value="<?= $dept->department_id ?>"  onChange="checkAllDeptUsers(<?= $dept->department_id ?>)">
								<b><?= $dept->department_name ?></b>
								<div class="col-lg-12" style="min-height:200px;">
								<?php foreach ($departmentUsers as $du) {?>
										<input type="checkbox" id="<?= $du->user_id; ?>" name="users[]" class="users_<?= $dept->department_id ?> users col-lg-2" onchange="checkUser(<?= $dept->department_id ?>)" value="<?= $du->user_id; ?>" style="margin: 0px;">
										<span class="action_label col-lg-10">
											<?php echo $du->first_name.' '.$du->last_name; ?>
										</span>

								<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
			<?php }
	}else{
		echo "0";
	}
}
}
?>
