<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Religion extends MX_Controller {  
	function __construct(){
		parent::__construct();		
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('religion_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Religion';
		
		$data["religion"] = $this->religion_model->getAllReligion();
		
	   	$this->template->load_partial('superadmin_master','religion/list_religion',$data);
	}
	function add($catId = ''){
		
		$data['titalTag'] = ' - Religion add';
		if($this->input->post("addEditDepart") !== false)
		{
				$addData['religion_name'] = $this->input->post('religion_name');
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['religion_id']= $catId;
					$data = $this->religion_model->updateReligion($addData);
					$this->session->set_flashdata('success','Religion updated successfully!');
				}else{
					
					$data = $this->religion_model->add($addData);
					$this->session->set_flashdata('success','Religion Added successfully!');
				}
				redirect(base_url().'superadmin/religion');exit;		
		}
		if(is_numeric($catId) && $catId != ''){/* when edit*/
		$data['religionEdit'] = $this->religion_model->getReligion($catId);
			if(count($data['religionEdit']) == 0){
				redirect(base_url().'superadmin/religion');exit;
			}
		$data['titalTag'] = ' - Religion Edit';
		$data['actionUrl'] = 'superadmin/religion/add/'.$catId;
		}
		
	   	$this->template->load_partial('superadmin_master','religion/add_edit_religion',$data);
	}
	public function edit($catId)
	{
		$data['religionEdit'] = $this->religion_model->getReligion($catId);
		$this->template->load_partial('superadmin_master','religion/add_edit_religion',$data);
	}
	public function delete($catId)
	{
		$this->religion_model->deleteReligion($catId);	
		$this->session->set_flashdata('success','Religion deleted successfully!');
		redirect(base_url().'superadmin/religion');
		exit;
	}
}

?>