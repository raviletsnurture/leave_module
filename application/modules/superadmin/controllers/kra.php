<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kra extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('kra_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - KRA';

		$data["kra"] = $this->kra_model->getAllFeedbackFields();
		$data["role"] = $this->kra_model->getAllRoles();
		$data["sub_role"] = $this->kra_model->getAllSubRole();
		$data["departments"] = $this->kra_model->getAllDepartments();

	   	$this->template->load_partial('superadmin_master','kra/list_kra',$data);
	}
	function add(){

		if(isset($_POST['feedback_key']) && isset($_POST['helptext'])){
			if(empty($_POST['feedback_key']) || $_POST['feedback_key']==""){
					$this->session->set_flashdata('error','Feedback key is required fiel');
					redirect(base_url().'superadmin/kra');
			}
		$addData['feedback_key'] = $_POST['feedback_key'];
		$addData['role_id'] = implode(",",$_POST['role_members']);
		$addData['dept_id'] = implode(",",$_POST['department_members']);
		if(isset($_POST['sub_role'])){
			$addData['sub_role_id'] = implode(",",$_POST['sub_role']);
		}
		$addData['helptext']  = $_POST['helptext'];
		$data = $this->kra_model->addNewKra($addData);
		if($data == 1){
			$this->session->set_flashdata('success','KRA added successfully!');
		}else{
			$this->session->set_flashdata('error','Some error occured!');
		}
		redirect(base_url().'superadmin/kra');
		}

	}

	function update(){

		$addData['id'] = $_POST['id'];
		$addData['feedback_key'] = $_POST['feedback_key'];
		$addData['role_id'] = implode(",",$_POST['role_id']);
		$addData['dept_id'] = implode(",",$_POST['department_id']);
		if(isset($_POST['sub_role']) && !empty($_POST['sub_role'])){
			$addData['sub_role_id'] = implode(",",$_POST['sub_role']);
		}
		$addData['helptext']  = $_POST['helptext'];

		$data = $this->kra_model->updateKra($addData);
		echo $data;
	}

	function delete(){
		$deleteData['id'] = $_POST['id'];

		$data = $this->kra_model->deleteKra($deleteData);

		echo $data;

	}
}

?>
