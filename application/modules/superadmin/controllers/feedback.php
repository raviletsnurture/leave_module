<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Feedback extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('download');
		$this->load->helper('general_helper');
		$this->load->model('feedback_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Feedbacks';
		$this->template->load_partial('superadmin_master','feedback/select_feedbacks',$data);
	}

	function logaudit(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$userClicked = $this->input->post('user_id');
		$userUpload = $this->input->post('upload_id');

		if($userClicked != $id) {
			auditUserActivity('Feedback', 'ALERT: download not allowed', $id, date('Y-m-d H:i:s'),'#');
			$data ="fake";
			}else{
			$data = $this->feedback_model->updateDownloadedAt($userClicked, $userUpload);
			$data ="OK";
		  }
    echo $data;
		//$data[""] = $this->feedback_model->getAllFeedbackUploads($id);
		//var_dump($data); die;

	}

	function add()
    {
			//echo "ok";
			//var_dump($this->input->post('addFeedback'));
			$data['titalTag'] = ' - Add Feedback';
			//$loginSession = $this->session->userdata("login_session");
			//$role_id = $loginSession[0]->role_id;
			//$department_id = $loginSession[0]->department_id;
			//$per = getUserpermission($role_id);
			//$data['login_session'] = $this->session->userdata("login_session");
			//$data['permission'] = $per;
			$data['feedback_keys'] = $this->feedback_model->getFeedbackKeys();
			//var_dump($data);
			//$data = $this->leave_model->updateLeave($addData);
			//$this->session->set_flashdata('success','Feedback added successfully!');
			$this->template->load_partial('superadmin_master','feedback/add_feedback',$data);
		}

	function edit($feedbackId=""){
		if($feedbackId != ""){
			$data['titalTag'] = ' - Edit Feedback';
			$data['feedback'] = $this->feedback_model->getFeedback($feedbackId);
			$data['feedback_keys'] = $this->feedback_model->getFeedbackKeys();


			$this->template->load_partial('superadmin_master','feedback/edit_feedback',$data);
		}

	}

	function addFeedback(){
		if($_POST['user_id'] != '' && $_POST['created_time'] != ''){
			$time = $_POST['created_time'];
			$month = date("m", strtotime($time));
			$year = date("Y", strtotime($time));
			$user_id = $_POST['user_id'];
			$checkUserFeedback = checkUserFeedback($month,$year, $user_id); // Check user feedback is exists or not for specific month
			if(empty($checkUserFeedback)) {
				$data['user_id'] =$_POST['user_id'];
				$data['reviewer_id'] =$_POST['reviewer_id'];
				$data['comment'] =$_POST['feedbackcomment'];
				$data['published_status'] =$_POST['publish_status_hidden'];
				$data['overall_score'] =serialize($_POST['feedback_keys']);
				$data['projects_and_ratings'] =serialize(array($_POST['project_name'],$_POST['project_feedback'],$_POST['project_comment']));
				$data['event_remark'] =$_POST['event_remark'];
				$data['created_time'] =$_POST['created_time'];

				$dataSubmit = $this->feedback_model->addFeedbackOfTL($data);
				echo 1;
			} else {
				echo 2;
			}
		} else {
			echo 0;
		}
	}

	function editFeedback(){

		if($this->input->post('user_id')!= '' && $this->input->post('feedback_id')!= ''){
			$data['feedback_id'] =$this->input->post('feedback_id');
			if($this->input->post('feedbackcomment') != ''){
				$user_id = $this->input->post('user_id');
				//$checkUserFeedback = checkUserFeedback($month,$year, $user_id); // Check user feedback is exists or not for specific month
				//print_r($this->input->post('user_id'));
				$data['user_id'] =$this->input->post('user_id');
				//$data['reviewer_id'] =$this->input->post('reviewer_id');
				$data['comment'] =$this->input->post('feedbackcomment');
				$data['published_status'] =$this->input->post('publish_status_hidden');
				$data['overall_score'] =serialize($this->input->post('feedback_keys'));
				$data['projects_and_ratings'] =serialize(array($this->input->post('project_name'),$this->input->post('project_feedback'),$this->input->post('project_comment')));
				$data['event_remark'] =$this->input->post('event_remark');
				$data['improvements'] =$this->input->post('improvements');

			}
			if(!empty($this->input->post('ba_remarks')) && !empty($this->input->post('hidden_ba_id')) ){
				$data['ba_remark'] = serialize(array($this->input->post('ba_remarks'),$this->input->post('hidden_ba_id')));
			}
			if(!empty($this->input->post('qa_remarks'))  && !empty($this->input->post('hidden_qa_id')) ){
				$data['qa_remark'] = serialize(array($this->input->post('qa_remarks'),$this->input->post('hidden_qa_id')));
			}
			if(!empty($this->input->post('hr_remarks'))  && !empty($this->input->post('hidden_hr_id')) ){
				$data['hr_remark'] = serialize(array($this->input->post('hr_remarks'),$this->input->post('hidden_hr_id')));
			}

			// print_r($data);
			// exit;

			$dataSubmit = $this->feedback_model->editFeedbackOfTL($data);
			$this->session->set_flashdata('success','Feedback edited successfully!');
			redirect(base_url().'superadmin/feedback');exit;

		} else {
			$this->session->set_flashdata('error',' Your comments & Team member selection is mandatory!!!');
			redirect(base_url().'superadmin/feedback');exit;

		}
	}

	function viewFeedback(){
		$data['titalTag'] = ' - Feedbacks';

		if($this->input->post('selectUser') != '' && $this->input->post('selectYear') != ''){
			$data["feedback"] = $this->feedback_model->getAllFeedbacks($this->input->post('selectUser'),$this->input->post('selectYear'));
			if($data["feedback"]){
				$data['feedbackMonth'] = $this->feedback_model->getMyfeedMonth($data["feedback"][0]->user_id);
				$data['feedbackMonthRanking'] = $this->feedback_model->getMyfeedMonthRanking($data["feedback"][0]->user_id);
			}else{
				$this->session->set_flashdata('error',' Team member has no review for selected year!');
				redirect(base_url().'superadmin/feedback');exit;
			}
		}else{
			$this->session->set_flashdata('error',' Year & Team member selection is mandatory!!!');
			redirect(base_url().'superadmin/feedback');exit;
		}
		$this->template->load_partial('superadmin_master','feedback/list_feedbacks',$data);
	}

	function getFeedbackFields(){

		if($_POST['user_id'] != ''){
			$html = '';
			$user_id = $_POST['user_id'];
			$role_id = $this->feedback_model->getRoleId($user_id);
			$feedback_keys = $this->feedback_model->getFeedbackKeys();

			$options = "";
			for($i=1; $i<=5; $i++) {
				$options .= '<option value="'.$i.'" >'.$i.'</option>';
			}


			for($fk=0; $fk< count($feedback_keys); $fk++) {
				$field_name = strtolower(str_replace(' ', '_', $feedback_keys[$fk]->feedback_key));
				$info = "";
				if($feedback_keys[$fk]->helptext != ""){
					$info = '<i class="fa fa-info" data-toggle="tooltip" data-placement="top" title="'.ucfirst($feedback_keys[$fk]->helptext).'"></i>';
				}

				$selected_user_sub_role = $role_id[0]->sub_role_id;
				$feedback_sub_role = $feedback_keys[$fk]->sub_role_id;

				if((preg_match('/\b' . $role_id[0]->role_id . '\b/', $feedback_keys[$fk]->role_id) && preg_match('/\b' . $role_id[0]->department_id . '\b/', $feedback_keys[$fk]->dept_id) && ($selected_user_sub_role=='')) || $feedback_keys[$fk]->role_id == '0' ){
				$html .= '<div class="col-lg-6">';
				$html .='<div class="form-group">';
				$html .= '<label  class="col-lg-4 control-label">'.ucfirst($feedback_keys[$fk]->feedback_key).'<span class="red">* </span>'.$info.'</label>';
				$html .= '<div class="col-lg-8">';
				$html .= '<select name="feedback_keys['.$field_name.']" id="'.$field_name.'"  class="form-control" required>';
				$html .= '<option value="">Select Point</option>'.$options.'</select></div></div></div>';
				}

				if(($selected_user_sub_role!='' && $role_id[0]->role_id==20) && (preg_match('/\b' . $selected_user_sub_role . '\b/', $feedback_sub_role)) || ($feedback_sub_role=='0' && $selected_user_sub_role!='' && $role_id[0]->role_id==20)){
					$html .= '<div class="col-lg-6">';
					$html .='<div class="form-group">';
					$html .= '<label  class="col-lg-4 control-label">'.ucfirst($feedback_keys[$fk]->feedback_key).'<span class="red">* </span>'.$info.'</label>';
					$html .= '<div class="col-lg-8">';
					$html .= '<select name="feedback_keys['.$field_name.']" id="'.$field_name.'"  class="form-control" required>';
					$html .= '<option value="">Select Point</option>'.$options.'</select></div></div></div>';
				}
			}
			echo $html;
		} else {
			echo "User not exist with proper role.";
		}
	}

	/**
	* Developed By: Parth Patwala
	* Date : 19-May-2017
	*
	*/
	function exportFeedbackData(){

		$today = new DateTime();

		$userId = (isset($_POST['selectUser']) && !empty($_POST['selectUser'])) ? $_POST['selectUser'] : "" ;
		$year = (isset($_POST['selectYear']) && !empty($_POST['selectYear'])) ? $_POST['selectYear'] : "" ;

		if(isset($_POST['start_date']) && !empty($_POST['start_date'])){
			$startDate = date_format(date_create($_POST['start_date']),'Y-m-d');
		} else {
			$startDate = date('Y').'-04-01';
		}
		if(isset($_POST['end_date']) && !empty($_POST['end_date'])){
			$endDate = date_format(date_create($_POST['end_date']),'Y-m-d');
		}else{
			$endDate = date('Y',strtotime('+1 years')).'-04-01';
		}

		if(empty($userId)){
			$userIdList = $this->feedback_model->getAllUsersRecord(); // get All user id
			$fileName = 'All-User-Feedback.xlsx';
		}else{
			$userIdList[]['user_id'] = $userId;
			$FileuserDetails = getUserDetail($userId);
			$FiledepartmentName = getDepartmentFromUserId($userId);
			$fileName = $FileuserDetails->first_name.' '.$FileuserDetails->last_name.' - '.$FiledepartmentName[0]['department_name'].'.xlsx';
		}

		//** Excel Code starts here
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

		header("Content-Disposition: attachment; filename=\"$fileName\"");
		header("Cache-Control: max-age=0");
		// Error reporting
		error_reporting(E_ALL);
		// Include path
		ini_set('include_path', ini_get('include_path').';../Classes/');
		// PHPExcel
		include 'PHPExcel/PHPExcel.php';
		// PHPExcel_Writer_Excel2007
		include 'PHPExcel/PHPExcel/Writer/Excel2007.php';
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Loop through each users
		if(!empty($userIdList)){
			$loop = 0;
			foreach($userIdList as $userId){
				$feedbackData = $this->feedback_model->getFeedbackOfUser($userId['user_id'],$startDate,$endDate); // get feedback data

				$userDetails = getUserDetail($userId['user_id']);
				$departmentName = getDepartmentFromUserId($userId['user_id']);
				$userName = $userDetails->first_name.' '.$userDetails->last_name.' - '.$departmentName[0]['department_name'];

				// Set properties
				$objPHPExcel->getProperties()->setCreator("LetsNurture");
				$objPHPExcel->getProperties()->setLastModifiedBy("LetsNurture");
				$objPHPExcel->getProperties()->setTitle("HRMS Feedback Sheet");
				$objPHPExcel->getProperties()->setSubject("HRMS Feedback Sheet");
				$objPHPExcel->getProperties()->setDescription("HRMS Feedback Sheet of Employee");
				$objPHPExcel->createSheet($loop);
				$objPHPExcel->setActiveSheetIndex($loop);

				// Styling
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
				$objPHPExcel->getActiveSheet()->getStyle("1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
				/*$objPHPExcel->getActiveSheet()->getStyle('1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);

				$styleArray = array(
			    'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => 'FFFFFF'),
			        'size'  => 11,
			        'name'  => 'Verdana'
			    ));
				$objPHPExcel->getActiveSheet()->getStyle('1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('1')->getFill()->getStartColor()->setARGB('29bb04');
				$objPHPExcel->getActiveSheet()->getStyle('1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);*/
				// Styling

				// ** Code to auto size column
				// foreach(range('B','R') as $columnID) {
				//     $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				// }

				$objPHPExcel->getActiveSheet()->setTitle($userName);
				// Check Feedback Data
				if(!empty($feedbackData)){
					$alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
					$i=1;
					foreach($feedbackData as $feedbackRow){

						$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Comments');
						$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].'1', date_format(date_create($feedbackRow->created_time),'M Y'));

						$scoreValue = unserialize($feedbackRow->overall_score);
						if(!empty($scoreValue)){
							$sum = 0;
							$ratingsCount = 2;
							$totalRatings = 1;
							// Get all score values
							foreach($scoreValue as $key=>$ratings){
								$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, ucfirst($key));
								$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, $ratings);
								$ratingsCount++;
								$totalRatings++;
								$sum += $ratings;
							}

							$comments = (!empty($feedbackRow->comment)) ? $feedbackRow->comment : "";
							$remarks = (!empty($feedbackRow->event_remark)) ? $feedbackRow->event_remark : "";
							$hrRemarks = (!empty($feedbackRow->hr_remark)) ? unserialize( $feedbackRow->hr_remark)[0] : "";
							$qaRemarks = (!empty($feedbackRow->qa_remark)) ? unserialize( $feedbackRow->qa_remark)[0] : "";
							$baRemarks = (!empty($feedbackRow->ba_remark)) ? unserialize( $feedbackRow->ba_remark)[0] : "";

							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, 'Average');
							$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, round($sum/$totalRatings,2));
							$ratingsCount++;
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, 'Comments/Remarks/Action Items');
							$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, $comments);
							$ratingsCount++;
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, 'Event Remarks');
							$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, $remarks);
							$ratingsCount++;
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, 'HR Remarks');
							$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, $hrRemarks);
							$ratingsCount++;
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, 'QA Remarks');
							$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, $qaRemarks);
							$ratingsCount++;
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, 'BA Remarks ');
							$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, $baRemarks);
							$ratingsCount++;

							if(!empty($feedbackRow->projects_and_ratings)){
								$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ratingsCount, 'Projects');
								$ratingsCount++;
								$projectsAndRatings = unserialize($feedbackRow->projects_and_ratings);
								//print_r($projectsAndRatings);
								$totalProjectRatings = count($projectsAndRatings[0]);
								for($x=0; $x<$totalProjectRatings;$x++){ // Add topics
									if(!empty($projectsAndRatings[0][$x])){
										$projectComments = (!empty($projectsAndRatings[2][$x])) ? '('.$projectsAndRatings[2][$x].')' : "";
									 	$objPHPExcel->getActiveSheet()->SetCellValue($alpha[$i].$ratingsCount, $projectsAndRatings[0][$x].' : '.$projectsAndRatings[1][$x].' '.$projectComments);
										$ratingsCount++;
									}
								}
							}
						}
						$i++;
					}
				}
				// //Save Excel 2007 file
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
				// //$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
				$objWriter->save("php://output");
				$loop++;
			}
		}
		die;
	}
}

?>
