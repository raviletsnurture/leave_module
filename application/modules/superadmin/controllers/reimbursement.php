<?php
if (!defined('BASEPATH')) 
    exit('No direct script access allowed');

class Reimbursement extends MX_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->template->set('controller', $this);
        $this->load->database();
        $this->logSession = $this->session->userdata("login_session");
        $this->load->helper('download');
        $this->load->helper('general_helper');
        $this->load->helper('general2_helper');
	    $this->load->library('email');
        $this->load->model('reimbursement_model');
        sAdmin_auth('0');
    }
    function index()
    {
        $data['titalTag'] = ' - Reimbursement';
        $data['request_list'] = $this->reimbursement_model->getAllRequest();
        $this->template->load_partial('superadmin_master', 'reimbursement/list_reimbursement', $data);
    }
    function changeStatus(){
        $data = $this->reimbursement_model->updateStatus($_POST);
        if($data == true){
            if($_POST['status']==0){
                echo 0;
            }
            elseif($_POST['status']==1){
                echo 1;
            }
            else{
                echo 2;
            }
        }
        else{
            echo 3;
        }
    }
}
?>