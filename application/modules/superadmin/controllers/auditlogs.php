<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AuditLogs extends MX_Controller {
    var $logSession;
	function __construct(){
		parent::__construct();
        $this->template->set('controller', $this);
        //var_dump("hello"); exit;
		/*$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache"); */
		//user_auth(0);
		$this->load->database();
    sAdmin_auth('0');
    $this->logSession = $this->session->userdata("sLogin_session");
		$this->load->helper('general_helper');
		$this->load->model('auditlogs_model');
    //var_dump($this->logSession);
	}

	function index()
	{
    //echo "hi"; exit;
    //var_dump("hello"); exit;
		$userData = $this->logSession;
		$id = $userData[0]->sId;

		$data['user_id'] = $userData[0]->sId;
		$data['titalTag'] = ' - Logs';
		//$data["leave"] = $this->leave_model->getAllLeave($id);

		/*----other department members leave-----*/
		//$inf['id'] = $id;
		//$inf['department_id'] = $userData[0]->department_id;

		/*--------- end----------*/

		/*-----role permission--------*/
		/*$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		//$per = getUserpermission($role_id);

    if($role_id ==20 || $role_id ==21 || $role_id ==22){
      $data["all_logs"] = $this->auditlogs_model->getAllAuditLogs(0);
    } else {*/
      $data["all_logs"] = $this->auditlogs_model->getAllAuditLogs(0);
    //}

		//$data['permission'] = $per;
		/*------------end------------*/
    //var_dump($data); exit;
    $this->template->load_partial('superadmin_master','auditlogs/all_logs',$data);
	}



	public function all()
	{
		$userData = $this->logSession;
		$id = $userData[0]->user_id;

		$data['user_id'] = $userData[0]->user_id;
		$data['titalTag'] = ' - All Users activity';
		$data["all_logs"] = $this->auditlogs_model->getAllAuditLogs($userData[0]->user_id);

		/*--------- end----------*/

		/*-----role permission--------*/
		$loginSession = $this->session->userdata("login_session");
		$role_id = $loginSession[0]->role_id;
		$per = getUserpermission($role_id);

		//$data['permission'] = $per;
		/*------------end------------*/

		$data['role_id'] = $role_id;
    //var_dump($data); exit;
        $this->template->load_partial('superadmin_master','auditlogs/all_logs',$data);
	}
}

?>
