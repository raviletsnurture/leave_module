<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Visitorapp extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('visitorapp_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Visitors App';
		$data["visitors"] = $this->visitorapp_model->getVisitors();
	  $this->template->load_partial('superadmin_master','visitorapp/index',$data);
	}
}
?>
