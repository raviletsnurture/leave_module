<?php
apache_setenv('no-gzip', 1);
ini_set('zlib.output_compression', 0);
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Form16 extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->library('zip');
		$this->load->helper('general_helper');
		$this->load->model('form16_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Form16 Upload';

		$data["form16"] = $this->form16_model->getAllForm16Uploads();
		//var_dump($data); die;
	  $this->template->load_partial('superadmin_master','form16/list_form16uploads',$data);
	}
	function add($catId = ''){
		$data['titalTag'] = ' - Upload Form16 Slip';
		if($this->input->post('user_id') != NULL){
		$addData['user_id'] = $this->input->post('user_id');
		$addData['year'] = $this->input->post('year');
		//$addData['form16Pdf'] = $this->input->post('form16Pdf');
		$pdfPath = './uploads/form16_slips/'.$addData['year'];
		makeDir($pdfPath);
		// avoid _ / = in encodedname as it might result in 404 error for filenames
		$encodedName = rtrim(strtr(base64_encode($addData['user_id'].time()), '+/', '-_'), '=');
		$ext = array("pdf", "docx");

		if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != '')
		{
			$mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);
			if($mainImageName !== false){
				$addData['filename'] = $mainImageName;
			} else {
					$this->session->set_flashdata('error','On Snap ! Form16 slip not uploaded, verify file and upload again!');
					redirect(base_url().'superadmin/form16');exit;
			}
		} else {
				$this->session->set_flashdata('error','On Snap ! Forgot to upload file? please upload again!');
				redirect(base_url().'superadmin/form16');exit;
		}
		//print_r($addData); exit;
		$addData['uploaded_at'] = date('Y-m-d H:i:s');
		//$addData['leave_updated'] = date('Y-m-d H:i:s');
		$data = $this->form16_model->add($addData);
		$this->session->set_flashdata('success','Form16 slip uploaded successfully!');
		//redirect(base_url().'superadmin/form16');exit;
		//print_r($addData); exit;
		redirect(base_url().'superadmin/form16');exit;
	} else{
		$this->template->load_partial('superadmin_master','form16/add_form16',$data);
	}

	}

	public function delete($catId){
		$data = $this->form16_model->getForm16($catId);
		// parse data and delete file pathinfo
		// back to 1 level for path mapping
		$upOne = realpath(BASEPATH . '/..');
		foreach($data as $fileData){
		 $fileDeleteInfo =$upOne.'/uploads/form16_slips/'.$fileData->year.'/'.sprintf("%02d",$fileData->month).'/'.$fileData->filename;
		}
		//var_dump($fileDeleteInfo); exit;
		$this->form16_model->deleteForm16($catId);
		unlink($fileDeleteInfo);
		$this->session->set_flashdata('success','Form16 Slip deleted successfully!');
		redirect(base_url().'superadmin/form16');
		exit;
	}


  /**
	* Developed By: Kaushal
	* Date : 24-Aug-2020
	*/
	function addAll(){
		$data['titalTag'] = ' - Upload Form16 Slip';

		if($this->input->post('year') != NULL){
			$date = date('Y-m-d-H:i:s');
			$addData['year'] = $this->input->post('year');
			// $addData['month'] = $this->input->post('month');
			$addData['uploaded_at'] = date('Y-m-d H:i:s');
			$pdfPath = './uploads/form16_slips/'.$addData['year'];
			makeDir($pdfPath);
			// $pdfPath = $pdfPath.'/'.$addData['month'];
			// makeDir($pdfPath);
			$mainImage = 'form16Pdf';
			$encodedName = rtrim(strtr(base64_encode($date), '+/', '-_'), '=');
			$ext = array("zip");

			if($_FILES[$mainImage]['name'] && $_FILES[$mainImage]['name'] != ''){
				$mainImageName = savePDF($mainImage,$pdfPath,$ext,$encodedName);

				if($mainImageName !== false){
					$addData['filename'] = $mainImageName;

						//extract zip file
					  	$zip = new ZipArchive;
						$zip->open($pdfPath.'/'.$mainImageName);
						$zip->extractTo($pdfPath);
						$zip->close();
						//delete zip file
						unlink($pdfPath.'/'.$mainImageName);
						if (is_dir($pdfPath)){
							$logFileNames = array_diff(scandir($pdfPath), array('..', '.'));

          			foreach ($logFileNames as $logFileName) {
								$fileUlr = $pdfPath.'/'.$logFileName;
								$path_parts = pathinfo($fileUlr);
								$extension = $path_parts['extension'];
								$filename_UserId = $path_parts['filename'];
								$pan_number = substr($filename_UserId, 0, 10);
                $Filelastchar = substr($filename_UserId , -2);
								//check old file
								if($Filelastchar != 'ln'){
									if($extension == 'pdf' || $extension == 'PDF'){
										//get user id from form16 slip id
										$this->db->select("*");
						        $this->db->from('crm_users');
						        $this->db->where("pancard_no = ('".base64_encode($pan_number)."')");
						        $getUserId = $this->db->get()->row();
										if($getUserId){
											//$newFileName = md5($date).$filename_UserId.'ln.'.$extension;
											$newFileName = md5($date).$getUserId->user_id.'ln.'.$extension;
											rename($fileUlr, $pdfPath.'/'.$newFileName);
											$addData['user_id'] = $getUserId->user_id;
											$addData['filename'] = $newFileName;
											$data = $this->form16_model->addBulk($addData);
										}
									}
								}
							}

							if($data == 1){
								$this->session->set_flashdata('success','Form16 slip uploaded successfully!');
								redirect(base_url().'superadmin/form16');exit;
							}else {
									$this->session->set_flashdata('error','Please upload again!');
									redirect(base_url().'superadmin/form16');exit;
							}
						}else {
								$this->session->set_flashdata('error','Please upload again!');
								redirect(base_url().'superadmin/form16');exit;
						}
				} else {
						$this->session->set_flashdata('error','On Snap ! Form16 slip not uploaded, verify file and upload again!');
						redirect(base_url().'superadmin/form16');exit;
				}
			} else {
					$this->session->set_flashdata('error','On Snap ! Forgot to upload file? please upload again!');
					redirect(base_url().'superadmin/form16');exit;
			}
	} else{
		$this->template->load_partial('superadmin_master','form16/add_form16',$data);
	}

	}

	/**
	* Developed By: Kaushal
	* Date : 24-Aug-2020
	*/
	public function getForm16Slip(){
		$today = new DateTime();
		$userId = $_POST['selectUser'];
		if(isset($_POST['start_date']) && !empty($_POST['start_date'])){
			$startDate = date_format(date_create($_POST['start_date']),'Y-m-d');
		} else {
			$startDate = date('Y').'-04-01';
		}
		if(isset($_POST['end_date']) && !empty($_POST['end_date'])){
			$endDate = date_format(date_create($_POST['end_date']),'Y-m-d');
		}else{
			$endDate = date('Y',strtotime('+1 years')).'-04-01';
		}
		$FileuserDetails = getUserDetail($userId);
		$FiledepartmentName = getDepartmentFromUserId($userId);
		$archive_file_name = $FileuserDetails->first_name.'-'.$FileuserDetails->last_name.'-'.$FiledepartmentName[0]['department_name'].'-Form16-Slip.zip';
		$user_records = $this->form16_model->getUserForm16Slip($startDate,$endDate,$userId);
		$file_names = array();
		foreach ($user_records as $key => $value) {
			$file_names[$key] = $value->filename;
		}
		$file_path_root=$_SERVER['DOCUMENT_ROOT'].'/ln-projects/live/hrms/uploads/form16_slips/';
    foreach($file_names as $key => $files)
    {
				$file_path = $file_path_root.$user_records[$key]->year.'/'.(strlen($user_records[$key]->month) <= 1 ? '0'.$user_records[$key]->month : $user_records[$key]->month).'/';
        $this->zip->read_file($file_path.$files);
    }
    $this->zip->download($archive_file_name);
	}

}
?>
