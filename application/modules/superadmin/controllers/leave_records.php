<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Leave_records extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->helper('general_helper');
		$this->load->model('leave_records_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Leave';
		$data["leave"] = $this->leave_records_model->getUserLeave();		
	   	$this->template->load_partial('superadmin_master','leave_records/list_records',$data);
	}

	function loadtable(){

		if(isset($_POST['start_date']) && !empty($_POST['start_date'])){
			$start_date = $_POST['start_date'];
		}else{
			$start_date = '';
		}

		if(isset($_POST['start_date']) && !empty($_POST['start_date'])){
			$end_date = $_POST['end_date'];
		}else{
			$end_date = '';
		}

		$department = $_POST['department'];
		$month = $_POST['month'];
		$year = $_POST['year'];
		$leave_type = $_POST['leave_type'];

		//Date Range Setting
		if(!empty($start_date) && !empty($end_date)){
			$date = date_create($start_date);
			$start_date = date_format($date,"Y-m-d");
			$date = date_create($end_date);
			$end_date = date_format($date,"Y-m-d");
		}else{
			$start_date = '';
			$end_date = '';
		}

		//Get Month
		if(!empty($month)){
			$date = $date = date_create($month);
			$month = date_format($date,"Y-m-d");
		}else{
			$month = '';
		}

		//Get year
		if(!empty($year)){
			$date = date_create($year);
			$year = date_format($date,"Y-m-d");
		}else{
			$year = '';
		}

		$data = $this->leave_records_model->SearchedRecords($start_date,$end_date,$department,$month,$year,$leave_type);

		if(!isset($leave_type))
		{
			$chartData = $this->leave_records_model->getChartRecords2($start_date,$end_date,$department,$month,$year,$leave_type);
		}else{
			$chartData = $this->leave_records_model->getChartRecords2($start_date,$end_date,$department,$month,$year,$leave_type);
		}

		$addData = '';
		foreach($data as $row){
			$P_leave = $this->leave_records_model->pendingleave($row->user_id); 
		    $addData .= '<tr id="'.$row->user_id.'">
							<td>'.$row->first_name.' '.$row->last_name.'</td>
							<td>'.$row->department_name.'</td>
							<td>'.$row->total_leave.'</td>
							<td>'.$P_leave->pendingLeavetotal.'</td>
					</tr>';
		}
		$data['basic_records'] = $chartData;

        /* month data */ 
		$serialized = array();
	    $serialized_year = array();
	    $monthData = array();
	    $month_array = array();         	    
	    for ($i=0; $i < sizeof($chartData); $i++) { 
	      $test = in_array($chartData[$i]->month, $serialized);
	        if ($test == false) {
	          $serialized_year[] =$chartData[$i]->year;
	          $serialized[] = $chartData[$i]->month;
	        }
	    }
	    $c=array_combine($serialized,$serialized_year);   

	    foreach ($c as $key => $value) {
	      $monthData[] = $key." ".$value;
	      $month_array[]= $key;            
	    } 	  

	    $data['monthData'] = $monthData;
	    $data['month_array'] = $month_array;

        /* leave Data  */
	    $month_grouped = $this->array_group_by( $chartData, "month" );
	    $leave_type_grouped = $this->array_group_by( $chartData, "leave_type" );
	    $chartDetail = array();
	    $html = '[';
	    foreach($leave_type_grouped as $row1){ 
    	    // echo $row1[0]->leave_type; 
           $chartViewData = '';
           $chart = array();
           for($i=0;$i<count($row1);$i++){
             $chart[$row1[$i]->month] = $row1[$i]->total_leave;
           }
           for($i=0;$i<count($month_array);$i++){
             $month_chart[$month_array[$i]] = 0;
           }
           foreach ($month_chart as $key => $value) {
             foreach ($chart as $key1 => $value1) {
                if($key == $key1){
                  $month_chart[$key] = $value1;
                }
             }
           } 
           foreach ($month_chart as $key => $value) {
            $chartViewData .= "['". $key . "'," . $value . "],";
           }

	        $chartViewData = rtrim($chartViewData,",");	 
	        $html .= "{
	        	     name: '".$row1[0]->leave_type."',
	        	     data:  [".$chartViewData ."],
	        	    },"; 
	    }	    
	    $html .= "]";
	    $data['chartDetail'] = $html;      

		echo json_encode(array('table' => $addData, 'monthData' => $monthData, 'month_array' => $month_array, 'chartDetail' => $html ), JSON_UNESCAPED_SLASHES);
	}

	function userLeave($user_id){
		$data['titalTag'] = 'user - Request';
		$data["userdetail"] = $this->leave_records_model->getUserDetail($user_id);
		$data["leave"] = $this->leave_records_model->getUserLeaveDetail($user_id);			
		$this->template->load_partial('superadmin_master','leave_records/user_leaverecords',$data);
	}

	function userPendingLeave($user_id){
		$data['titalTag'] = 'user - Request';
		$data["userdetail"] = $this->leave_records_model->getUserDetail($user_id);
		$data["leave"] = $this->leave_records_model->pendingleaveDetail($user_id);			
		$this->template->load_partial('superadmin_master','leave_records/user_leaverecords',$data);
	}


	function array_group_by(array $array, $key)
  {
    if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
      trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
      return null;
    }

    $func = (!is_string($key) && is_callable($key) ? $key : null);
    $_key = $key;

    // Load the new array, splitting by the target key
    $grouped = [];
    foreach ($array as $value) {
      $key = null;

      if (is_callable($func)) {
        $key = call_user_func($func, $value);
      } elseif (is_object($value) && isset($value->{$_key})) {
        $key = $value->{$_key};
      } elseif (isset($value[$_key])) {
        $key = $value[$_key];
      }

      if ($key === null) {
        continue;
      }

      $grouped[$key][] = $value;
    }
    // Recursively build a nested grouping if more parameters are supplied
    // Each grouped array value is grouped according to the next sequential key
    if (func_num_args() > 2) {
      $args = func_get_args();

      foreach ($grouped as $key => $value) {
        $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
        $grouped[$key] = call_user_func_array('array_group_by', $params);
      }
    }
    return $grouped;
  }
}
?>
