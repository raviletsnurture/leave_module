<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_Type extends MX_Controller {  
	function __construct(){
		parent::__construct();		
		$this->template->set('controller', $this);
		$this->load->library('session');
		$this->load->model('leave_type_model');
		sAdmin_auth('0');
	}
	function index(){
		$data['titalTag'] = ' - Leave_Type';
		
		$data["leave_type"] = $this->leave_type_model->getAllLeave_Type();
		
	   	$this->template->load_partial('superadmin_master','leave_type/list_leave_type',$data);
	}
	function add($catId = ''){
		
		$data['titalTag'] = ' - Leave Type';
		if($this->input->post("addEditLeaveType") !== false)
		{
				$addData['leave_type'] = $this->input->post('leave_type');
				$addData['leave_amount'] = $this->input->post('leave_amount');
				$addData['status'] = $this->input->post('status');
				if(is_numeric($catId) && $catId != '')
				{ /*when update*/
				    $addData['leave_type_id']= $catId;
					$data = $this->leave_type_model->updateLeave_Type($addData);
					$this->session->set_flashdata('success','Leave Type updated successfully!');
				}else{
					
					$data = $this->leave_type_model->add($addData);
					$this->session->set_flashdata('success','Leave Type Added successfully!');
				}
				redirect(base_url().'superadmin/leave_type');exit;		
		}
		
		
	   	$this->template->load_partial('superadmin_master','leave_type/add_edit_leave_type',$data);
	}
	public function edit($catId)
	{
		$data['leave_typeEdit'] = $this->leave_type_model->getLeave_Type($catId);
		$this->template->load_partial('superadmin_master','leave_type/add_edit_leave_type',$data);
	}
	public function delete($catId)
	{
		$this->leave_type_model->deleteLeave_Type($catId);	
		$this->session->set_flashdata('success','Leave Type deleted successfully!');
		redirect(base_url().'superadmin/leave_type');
		exit;
	}
}

?>