<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rewards extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->template->set('controller', $this);
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('download');
        $this->load->helper('general_helper');
	      $this->load->library('email');
        $this->load->model('reward_model');
        sAdmin_auth('0');

      $config['upload_path']   = './uploads/products/';
       $config['allowed_types'] = 'gif|jpg|png';
       $config['max_size']      = 10000000;
       $config['max_width']     = 20000;
       $config['max_height']    = 20000;
       $this->load->library('upload', $config);

    }

    function index()
    {
        $data['titalTag'] = ' - Rewards';
        $data["announcements"] = []; // $this->notification_model->getAllAnnouncements();
        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'rewards/index', $data);
    }

    function add()
    {
        $data['titalTag'] = ' - Rewards';
        $data["announcements"] = []; // $this->notification_model->getAllAnnouncements();
        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'rewards/add', $data);
    }

    function edit($id)
    {
        $data['titalTag'] = ' - Rewards';
        $data["rewardEdit"] = $this->reward_model->getRewardDetailsById($id);
        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'rewards/add', $data);
    }

    function exreward()
    {
        $data['titalTag'] = ' - Exceptional Rewards';
        $data["users"] = $this->reward_model->getAllUsers();
        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'rewards/exreward', $data);
    }

    function pmpoints()
    {
        $data['titalTag'] = ' - PM Rewards Points';
        $data["users"] = $this->reward_model->getAllPMs();
        $data["history"] = $this->reward_model->getPMsrewardpointhistory();

        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'rewards/pmpoints', $data);
    }

    public function addpmpoints(){

        if($this->input->post('user_id'))
        {
          $data = array();
    			$user_id        = trim($this->input->post('user_id'));
          $data['point']   = trim($this->input->post('reward_point'));

          $data2 = array();
          $user_past_Reward = $this->reward_model->getRewardUser($user_id);
          $data2['pm_reward_points'] = $user_past_Reward[0]->pm_reward_points + $data['point'];

          $data3 = array();
          $data3['reward_point'] = $data['point'];
          $data3['user_id'] = $user_id;
          $res1 = $this->reward_model->addRewardHistory($data3);
          $res = $this->reward_model->updateRewardUser($data2, $user_id);
    			$this->session->set_flashdata('success', 'PM Reward Added Sucessfully');
        }else{
          $this->session->set_flashdata('error', 'Please provide data');
        }

        //Push Notification Logic
  			redirect(base_url() . 'superadmin/rewards/pmpoints');
  			exit;
      }

    function negative()
    {
        $data['titalTag'] = ' - Nagative Rewards';
        $data["users"] = $this->reward_model->getAllUsers();
        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'rewards/nagative', $data);
    }

    function points()
    {
        $data['titalTag'] = ' - Reward Points';
        $data["points"] = $this->reward_model->getAllRewardsPoints();

        $this->template->load_partial('superadmin_master', 'rewards/points', $data);
    }

    function eom()
    {
        $data['titalTag'] = ' - EOM';
        $data["points"] = $this->reward_model->getAllRewardsPoints();
        $data["eom"] = $this->reward_model->getEOM();

        $this->template->load_partial('superadmin_master', 'rewards/eom', $data);
    }

    function addeom()
    {
        $data['titalTag'] = ' - Add EOM';
        $data["users"] = $this->reward_model->getAllUsers();

        $this->template->load_partial('superadmin_master', 'rewards/addeom', $data);
    }

    function addeom2()
    {
        $data = array();
        $data['user_id']        = trim($this->input->post('user_id'));
        $data['month']   = date('y-m-d H:i:s', strtotime(trim($this->input->post('eom_month'))));
        $data['description']   = trim($this->input->post('description'));
        $data['created_at']   = date('y-m-d H:i:s');
        $data['point']   = trim($this->input->post('reward_point'));

        $reward_user = array();
        $reward_user['user_id'] = $data['user_id'];
  			$reward_user['reward_name']        = 'Employee of the Month - ' . date('F Y', strtotime(trim($this->input->post('eom_month'))));
  			$reward_user['description']   = trim($this->input->post('description'));
        $reward_user['status']   = 1;
        $reward_user['date']   = date('y-m-d H:i:s');
        $reward_user['point']   = trim($this->input->post('reward_point'));
        $reward_user['approved_by'] = 69;
        $reward_user['reward_type'] = 0;
        $this->reward_model->addRewardtoUser($reward_user);

        $reward_user = $this->reward_model->getRewardUser($data['user_id']);
        $data2 = array();

        $data2['reward_points'] = $reward_user[0]->reward_points + $data['point'];
        $res = $this->reward_model->updateRewardUser($data2, $data['user_id']);

        if($this->reward_model->addEOM($data))
        {
          $this->session->set_flashdata('success', 'Employee of the Month added Sucessfully');
          redirect(base_url() . 'superadmin/rewards/eom');
        }else{
          $this->session->set_flashdata('success', 'Error Adding Employee Of the Month');
          redirect(base_url() . 'superadmin/rewards/addeom');
        }

    }

    function userrewardpoints()
    {
        $data['titalTag'] = ' - Reward Points';
        if(isset($_POST['eom_month']) && $_POST['eom_month'] != ''){
           $month = $_POST['eom_month'];
           $data['month'] = $month;
           $data["points"] = $this->reward_model->getAllUserRewardsPoints($month);
        }else{
           $data["points"] = $this->reward_model->getAllUserRewardsPoints();
        }
        $this->template->load_partial('superadmin_master', 'rewards/userrewardpoints', $data);
    }

    function userReward($user_id){
        $data['titalTag'] = 'User - Rewards Request';
        $data["rewards"] = $this->reward_model->getUserRewardsHistory($user_id);
        $this->template->load_partial('superadmin_master','rewards/history',$data);
    }

    function rewardproduct()
    {
        $data['titalTag'] = ' - Reward Product';
        $data["products"] = $this->reward_model->getAllRewardsProducts();
        $this->template->load_partial('superadmin_master', 'rewards/rewardproduct', $data);
    }

    function addproduct()
    {
        $data['titalTag'] = ' - Reward Products';
        $data["announcements"] = []; // $this->notification_model->getAllAnnouncements();
        //$this->template->load_partial('superadmin_master', 'announcement/select_announcement', $data);

        $this->template->load_partial('superadmin_master', 'rewards/addproduct', $data);
    }

    function editproduct($id = '')
    {
        $data['titalTag'] = ' - Reward Products';
        $data["product"] = $this->reward_model->getProduct($id);
        $this->template->load_partial('superadmin_master', 'rewards/addproduct', $data);
    }

	  public function addReward($id = ''){

      if($this->input->post('reward_name'))
      {
        $data = array();
  			$data['reward_name']        = trim($this->input->post('reward_name'));
  			$data['description']   = trim($this->input->post('description'));
        $data['status']   = trim($this->input->post('reward_status'));
        $data['point']   = trim($this->input->post('reward_point'));

        if(is_numeric($id) && $id != '' && $id>0)
        {
          $this->reward_model->updateReward($data, $id);
  			  $this->session->set_flashdata('success', 'Reward Type Updated Sucessfully');
        }else{
          $this->reward_model->addReward($data);
  			  $this->session->set_flashdata('success', 'Reward Type Added Sucessfully');
        }
      }else{
        $this->session->set_flashdata('error', 'Please provide data');
      }

      //Push Notification Logic
			redirect(base_url() . 'superadmin/rewards/points');
			exit;

    }

    public function exaddReward(){

        if($this->input->post('user_id'))
        {
          $data = array();
    			$data['user_id']        = trim($this->input->post('user_id'));
    			$data['description']   = trim($this->input->post('description'));
          $data['point']   = trim($this->input->post('reward_point'));
          $data['created_at'] = date('Y-m-d H:i:s');
          $data['date'] = $data['created_at'];
          $data['status'] = 1;
          $data['reward_name'] = 'Exeptional Reward';
          $data['approved_by'] = 69;
          $data['reward_type'] = 0;

          $reward_user = $this->reward_model->getRewardUser($data['user_id']);
          $data2 = array();

          $data2['reward_points'] = $reward_user[0]->reward_points + $data['point'];
          $res = $this->reward_model->updateRewardUser($data2, $data['user_id']);

          $this->reward_model->addRewardtoUser($data);

    			$this->session->set_flashdata('success', 'Reward Type Added Sucessfully');
        }else{
          $this->session->set_flashdata('error', 'Please provide data');
        }

        //Push Notification Logic
  			redirect(base_url() . 'superadmin/rewards/exreward');
  			exit;

      }


      public function addNegativeReward(){

          if($this->input->post('user_id'))
          {
            $data = array();
      			$data['user_id']        = trim($this->input->post('user_id'));
      			$data['description']   = trim($this->input->post('description'));
            $data['point']   = trim($this->input->post('reward_point')) * -1;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['date'] = $data['created_at'];
            $data['status'] = 1;
            $data['reward_name'] = 'Negative Reward';
            $data['approved_by'] = 69;
            $data['reward_type'] = 1;

            $reward_user = $this->reward_model->getRewardUser($data['user_id']);
            $data2 = array();

            $data2['reward_points'] = $reward_user[0]->reward_points + $data['point'];
            $data2['karma_points'] = $reward_user[0]->karma_points + $data['point'];
            $res = $this->reward_model->updateRewardUser($data2, $data['user_id']);

            $this->reward_model->addRewardtoUser($data);

      			$this->session->set_flashdata('success', 'Negative Reward Added Sucessfully');
          }else{
            $this->session->set_flashdata('error', 'Please provide data');
          }

          //Push Notification Logic
    			redirect(base_url() . 'superadmin/rewards/negative');
    			exit;

        }

    public function addProductdata($id = ''){

        if($this->input->post('product_name'))
        {
          $data = array();
    			$data['name']        = trim($this->input->post('product_name'));
    			$data['description'] = trim($this->input->post('description'));
          $data['status']   = trim($this->input->post('status'));
          $data['point']   = trim($this->input->post('point'));
          if(is_numeric($id) && $id != '' && $id>0)
          {
            if(isset($_FILES['prod_image']['tmp_name']) && $_FILES['prod_image']['tmp_name'] != ''){

              if ( ! $this->upload->do_upload('prod_image')) {
                  $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect(base_url() . 'superadmin/rewards/addproduct');
              			exit;
               }else {
                 $udata = $this->upload->data();
                 $data['image'] = $udata['orig_name'];
                 $this->reward_model->updateProduct($data, $id);
                 $this->session->set_flashdata('success', 'Reward Product Updated Sucessfully');
               }
            }else{
                 $udata = $this->upload->data();
                 $this->reward_model->updateProduct($data, $id);
                 $this->session->set_flashdata('success', 'Reward Product Updated Sucessfully');
            }
             redirect(base_url() . 'superadmin/rewards/rewardproduct');
          }else{
            if ( ! $this->upload->do_upload('prod_image')) {
                $this->session->set_flashdata('error', $this->upload->display_errors());
                  redirect(base_url() . 'superadmin/rewards/addproduct');
            			exit;
             }else {
               $udata = $this->upload->data();
               $data['image'] = $udata['orig_name'];
               $this->reward_model->addProduct($data);
               $this->session->set_flashdata('success', 'Reward Product Added Sucessfully');
               redirect(base_url() . 'superadmin/rewards/rewardproduct');
             }
          }

        }else{
          $this->session->set_flashdata('error', 'Please provide data');
          redirect(base_url() . 'superadmin/rewards/rewardproduct');
        }

        //Push Notification Logic
  			//redirect(base_url() . 'superadmin/rewards/rewardproduct');
  			exit;

      }

    function request(){
         $data['titalTag'] = ' - Rewards Request';
         $data["rewards"] = $this->reward_model->getAllRewardsHistory();
         $this->template->load_partial('superadmin_master','rewards/history',$data);
    }

    public function delete($annId){
  		$this->notification_model->deleteAnnouncement($annId);
  		$this->session->set_flashdata('success', 'Announcement deleted successfully!');
  		redirect(base_url().'superadmin/notification');
  		exit;
  	}

    function redeemhistory(){
        $data['titalTag'] = ' - Redeem History';
        $data["rewards"] = $this->reward_model->getAllRedeemHistory();
        $this->template->load_partial('superadmin_master','rewards/redeemhistory',$data);
    }


   /* function updateRewardStatus(){
        $reward_id = $this->input->post('reward_id');
        $status = $this->input->post('status');
        $user_id = $this->input->post('user_id');
        $reason_desc = $this->input->post('reason_desc');
         $rh_id = $this->input->post('rh_id');


    		$user_id2 = 69; //Ketan sir's id, superadmin
        $data['status'] = $status;
        if($reason_desc)
        {
          $data['reply_description'] = $reason_desc;
        }

        $reward_data = $this->reward_model->getReward($reward_id);
        $reward_user = $this->reward_model->getRewardUser($user_id);
        $reward_status = $this->reward_model->getRewardHistory($rh_id);
        $data2 = array();
        if($status == 1)
        {

          if($reward_status->status != 1){
          $data2['reward_points'] = $reward_user[0]->reward_points + $reward_data[0]->point;
          $res = $this->reward_model->updateRewardUser($data2, $user_id);

         if($res){
              $rewardDetails = $this->reward_model->getRewardDetailsById($reward_id);
              $rewardName = $rewardDetails[0]->reward_name;
              //Send Alert to TLs , PM & HR.
              $managementEmails = getSeniorEmails($addData['user_id'], 'feedback_users');
              //$managementEmails = 'hitendra@letsnurture.com';

              $mailData['body'] = '<tr>
                            <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$reward_user[0]->first_name.' '.$reward_user[0]->last_name.'</strong> your request for <strong style="color:#03A9F4;">'.$rewardName.'</strong> reward has beed approved<br />Reward Point: '.$reward_data[0]->point.'<br />Your Reward Balance: '.$data2['reward_points'].'</p></td>
                            </tr>
                            <tr>
                              <td colspan="2" style="font-size:14px; color:#FFF;
                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'.$description.'
                              </td>
                            </tr>';
              $mailData['name'] = '';

              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
              //$this->email->to('rajendra.letsnurture@gmail.com');
              $this->email->cc($managementEmails);
              $this->email->subject("Reward Approved");
              $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
              $this->email->message($body);
              //$this->email->send();
              }
            }
        }else if($status == 2){

              if($reward_status->status == 1){
                  $data3['reward_points'] =  $reward_user[0]->reward_points - $reward_data[0]->point;
                  $res = $this->reward_model->updateRewardUser($data3, $user_id);
              }
            }
        if($status == 1 || $status == 0){
          $data['approved_by'] = $user_id2;
        }else{
          $data['approved_by'] = $user_id2;
        }

        $updateRewardStatusSucess = $this->reward_model->updateRewardStatus($data, $reward_id);

    } */
    function updateRewardStatus(){
        $reward_id = $this->input->post('reward_id');
        $status = $this->input->post('status');
        $user_id = $this->input->post('user_id');
        $rh_id = $this->input->post('rh_id');

        $user_id2 = 69; //Ketan sir's id, superadmin
        $data['status'] = $status;
        $data['reward_type'] = 1;
        if($this->input->post('reason_desc'))
        {
          $data['reply_description'] = $this->input->post('reason_desc');
        }

        $reward_data = $this->reward_model->getReward($reward_id);
        $reward_user = $this->reward_model->getRewardUser($user_id);
        $reward_status = $this->reward_model->getRewardHistory($rh_id);
        $data2 = array();
        if($status == 1)
        {

          if($reward_status->status != 1){
          $data2['karma_points'] =  $reward_user[0]->reward_points +  $reward_data[0]->point;
          $data2['reward_points'] = $reward_user[0]->reward_points + $reward_data[0]->point;
          $res = $this->reward_model->updateRewardUser($data2, $user_id);
          if($res){
              $rewardDetails = $this->reward_model->getRewardDetailsById($reward_id);
              $rewardName = $rewardDetails[0]->reward_name;
              //Send Alert to TLs , PM & HR.
              $managementEmails = getSeniorEmails($addData['user_id'], 'feedback_users');
              //$managementEmails = 'hitendra@letsnurture.com';

              $mailData['body'] = '<tr>
                            <td colspan="2" style="padding:0 40px;"><p style="font-size:14px; line-height:20px; color:#FFF; font-weight:normal;"><strong style="color:#03A9F4;">'.$reward_user[0]->first_name.' '.$reward_user[0]->last_name.'</strong> your request for <strong style="color:#03A9F4;">'.$rewardName.'</strong> reward has beed approved<br />Reward Point: '.$reward_data[0]->point.'<br />Your Reward Balance: '.$data2['reward_points'].'</p></td>
                            </tr>
                            <tr>
                              <td colspan="2" style="font-size:14px; color:#FFF;
                              padding:0 40px 20px 40px;"><b style="color:#03A9F4;">'.'Description : '.'</b>'. ' Reward Approved'.'
                              </td>
                            </tr>';
              $mailData['name'] = '';

              $this->email->from('hrms@letsnurture.com', "HRMS");
              $this->email->to('hrms@letsnurture.com');
              //$this->email->to('rajendra.letsnurture@gmail.com');
              $this->email->cc($managementEmails);
              $this->email->subject("Reward Approved");
              $body = $this->load->view('mail_layouts/comman_mail.php', $mailData, TRUE);
              $this->email->message($body);
              //$this->email->send();
          }
          }
        }else if($status == 2){
          if($reward_status->status == 1){
              $data3['reward_points'] =  $reward_user[0]->reward_points - $reward_data[0]->point;
              $data3['karma_points'] =  $reward_user[0]->reward_points;
              $res = $this->reward_model->updateRewardUser($data3, $user_id);
          }
        }
        if($status == 1 || $status == 0){
          $data['approved_by'] = $user_id2;
        }else{
          $data['approved_by'] = $user_id2;
        }

        $updateRewardStatusSucess = $this->reward_model->updateRewardStatus($data, $rh_id);

    }

    /* user product claim */
    function claim_product(){

      $status = $this->input->post('reward_id');
      $data['status'] = $status;
      $data['updated_at'] = date('Y-m-d h:i:s');
      $row_id = $this->input->post('rh_id');
      $user_id = $this->input->post('user_id');

      $reward_user = $this->reward_model->getRewardUser($user_id);
      $reward_data = $this->reward_model->getUserRedeemHistory($row_id, $user_id);

      /* if previous status is rejected again approved */
      if($status == 1){
        $pre_status = $reward_data[0]->status;
        if($pre_status == 2){
           $data2['reward_points'] =  $reward_user[0]->reward_points - $reward_data[0]->point;
           $data2['karma_points'] =  $reward_user[0]->karma_points - $reward_data[0]->point;
           $res = $this->reward_model->updateRewardUser($data2, $user_id);
        }
      }

      /* if reject change user reward point */
      if($status == 2){
          $data2['reward_points'] =  $reward_user[0]->reward_points + $reward_data[0]->point;
          $data2['karma_points'] =  $reward_user[0]->karma_points + $reward_data[0]->point;
          $res = $this->reward_model->updateRewardUser($data2, $user_id);
      }

      $claimProductUpdate = $this->reward_model->claimProductUpdate($data, $row_id, $user_id);
    }
}
?>
