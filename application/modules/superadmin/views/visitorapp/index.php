<section id="main-content">
  <section class="wrapper site-min-height">


    <section class="panel">
      <header class="panel-heading"> Visitors </header>

      <div class="panel-body">
        <div class="adv-table">
          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th>Staff Name</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Purpose</th>
                <th>Appointment</th>
                <th>Visting Card</th>
                <th>Datetime</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($visitors as $row){ ?>
              <tr>
                <td><?php echo $row->first_name.' '.$row->last_name;?></td>
                <td><?php echo $row->vName;?></td>
                <td><?php echo $row->vPhone; ?></td>
                <td><?php echo $row->vEmail; ?></td>
                <td><?php echo $row->vPurpose; ?></td>
                <td><?php echo $row->appoinment; ?></td>
                <td><a href="<?php echo base_url()."uploads/visitorCard/".$row->cardImage; ?>" target="_blank"><?php echo $row->cardImage; ?></a></td>
                <td><?php echo $row->created_at; ?></td>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#example').dataTable({
		"aaSorting": [],
		"bFilter": true,
		"bInfo": true,
		"bPaginate": true,
	});
 $(".dataTables_filter input").addClass('form-control');
});
</script>
