<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading"> All Reimbursement Requests </header>
        <div class="panel-body">
        <table class="table table-striped table-hover table-bordered" id="example">
        <thead>
        <tr>
              <th >Amount</th>
              <th width="10%">Name</th>
              <th>Description</th>
              <th width="10%">Payment mode</th>
              <th width="15%">Reimbursement date</th>
              <th>Created at</th>
              <th>Your Approval</th>
              <th>Senior Approval Status</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
         <?php
         foreach( $request_list as $request){?>
         <?php
          if($request['seniorApproval'] != '1'){
            $seniorApprovalDetail = getUserDetail($request['approvedBy']);
            $seniorApprovalName = $seniorApprovalDetail->first_name.' '.$seniorApprovalDetail->last_name;
        }else{
          $seniorApprovalName = "-";
        }
         ?>
             <tr>
                <td><?php echo $request['amount']?></td>
                <td><?php echo $request['first_name'].' '.$request['last_name'] ?></td>
                <td><?php echo $request['description']?></td>
                <td><?php echo $request['payment_mode']?></td>
                <td><?php echo date("d-M-Y h:i:s a", strtotime($request['datetime']))?></td>
                <td><?php echo date("d-M-Y", strtotime($request['created_at']))?></td>
                <td><?php
                    if($request['status']!=''&&$request['status']!=Null){?>
                        <select name="status" class="status" onchange="ff(this);">
                            <option <?php if($request['status']==0){echo "selected='selected'";}?> value="0, <?php echo $request['reimbursement_id']; ?>, <?php echo $request['user_id']; ?>">Reject</option>
                            <option <?php if($request['status']==1){echo "selected='selected'";}?> value="1, <?php echo $request['reimbursement_id']; ?>, <?php echo $request['user_id']; ?>">Pending</option>
                            <option <?php if($request['status']==2){echo "selected='selected'";}?> value="2, <?php echo $request['reimbursement_id']; ?>, <?php echo $request['user_id']; ?>">Approve</option>
                        </select>
                    <?php }?>
                </td>
                <td>
                <?php
                    if($request['role_id'] != 15 || $request['role_id'] == 20){
                            if($request['seniorApproval']=='0'){

                                echo "<span style='color:red'>Rejected</span>".' by '.$seniorApprovalName;
                            }
                            elseif($request['seniorApproval']=='1'){
                                echo "<span>Pending</span>";
                            }
                            else{
                                echo "<span style='color:green'>Approved</span>".' by '.$seniorApprovalName;
                            }

                    }else{
                        echo "<i>not required</i>";
                    }
                    ?>
                </td>
                <td>
                <a href="<?php echo base_url()."/uploads/cash_reimbursement/".$request['reciept']?>" target="_blank"><button class="btn btn-info btn-xs tooltips" data-toggle="tooltip" data-original-title="view&nbsp;reciept"><i class="fa fa-eye "></i></button></a>
                </td>

             </tr>
         <?php
        }
         ?>
         </tbody>
        </table>
        </div>
        </section>
    </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script>
$(document).ready(function(){
  var dt =  $('#example').dataTable( {
    "aaSorting": [],
		 "pagingType": "full_numbers",
		 "bPaginate": true,
		 "bInfo" : false,
         aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1, 6 ]
            }
        ]
    });
});
function ff(sel){
    var ff = sel.value.split(",");
    var r = confirm("Are you sure you want to change the status of the request?");
    if (r == true){
        $.ajax({
            url: '<?php echo base_url(); ?>superadmin/reimbursement/changeStatus',
            data: { request_id: ff[1], status: ff[0], user_id: ff[2]},
            type: 'POST',
            success: function(data){
                if(data == 0){
                    alert("Request Rejected successfully!");
                }
                else if(data == 1){
                    alert("Request Pending successfully!");
                }
                else if(data == 2){
                    alert("Request approved successfully!");
                }
                else{
                    alert("Oops! Error occured.");
                }
            }
        });
    }
}
</script>
