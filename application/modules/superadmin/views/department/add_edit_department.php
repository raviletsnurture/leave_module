
<?php
    $User = getAllUsers();
    //echo "<pre>"; print_r($departmentEdit); echo "</pre>";

    $SeniorsIdsArr = array();
    if(isset($departmentEdit[0]->seniors) && !empty($departmentEdit[0]->seniors)){
        $SeniorsIds = $departmentEdit[0]->seniors;
        $SeniorsIdsArr = explode(',' ,$SeniorsIds);
    }else{
        $SeniorsIds = "";
        $SeniorsIdsArr = array();
    }
?>

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?php if(isset($departmentEdit[0]->department_id)){ echo "Edit Department";} else{echo "Add Department";}?>
                    </header>
                    <?php if($this->session->flashdata('error')){?>
                        <div class="alert alert-block alert-danger fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
                        </div>
                    <?php } ?>
                    <?php if($this->session->flashdata('success')){?>
                        <div class="alert alert-success fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                        </div>
                    <?php }?>
                    <div class="panel-body">
                        <div class="form">
                            <?php
                            $form_attributes = array('name' => 'addDepartment', 'id' => 'addDepartment', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
                                if(isset($departmentEdit[0]->department_id)){
                                    echo form_open('superadmin/department/add/'.$departmentEdit[0]->department_id,$form_attributes);
                                }else{
                                    echo form_open(base_url().'superadmin/department/add',$form_attributes);
                                }
                                ?>
                                <div class="form-group ">
                                    <label for="firstName" class="control-label col-lg-2">Department Name <span class="red">*</span></label>
                                    <div class="col-lg-10">
                                        <input class="form-control" id="department_name" name="department_name" type="text"  value="<?php if(isset($departmentEdit[0]->department_name)){ echo $departmentEdit[0]->department_name;} ?>"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="seniors" class="control-label col-lg-2">Seniors <span class="red">*</span></label>
                                    <div class="col-lg-10">
                                        <select name="seniors[]" id="seniors" multiple="" class="form-control">
                                            <?php for($u=0; $u<count($User); $u++): ?>
                                                <?php if(in_array($User[$u]->user_id, $SeniorsIdsArr)): ?>
                                                    <option selected="selected" value="<?php echo $User[$u]->user_id; ?>"><?php echo ucwords($User[$u]->first_name). ' ' . ucwords($User[$u]->last_name); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo $User[$u]->user_id; ?>"><?php echo ucwords($User[$u]->first_name). ' ' . ucwords($User[$u]->last_name); ?></option>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                          </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                                    <div class="col-lg-10">
                                        <select name="status" id="status" class="form-control m-bot15">
                                            <option value="Active" <?php echo isset($departmentEdit[0]->status)? selectedVal($departmentEdit[0]->status,"Active"): '';?>>Active</option>
                                            <option value="Inactive" <?php echo isset($departmentEdit[0]->status)? selectedVal($departmentEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-danger" type="submit" name="addEditDepart">
                                            <?php if(isset($departmentEdit[0]->department_id)){ echo "Update";} else{echo "Submit";}?>
                                        </button>
                                        <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-department.js" ></script>
