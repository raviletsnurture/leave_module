<?php
// $user = getAllUsers();
// $loginSession = $this->session->userdata("login_session");
// $role_id = $loginSession[0]->role_id;
?>

<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">
      <header class="panel-heading">Swapping Leaves List</header>
        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered" id="example">
                <thead>
                    <tr>
                        <th width="7%">Leave Id</th>
                        <th width="15%">Employee Name</th>
                        <!-- <th>Department</th>                         -->
                        <th width="10%">Leave Date</th>
                        <th>Reason</th>                
                        <!-- <th>Request Status</th> -->
                        <!-- <th>Approved By</th> -->
                        <!-- <th>Applied Date</th> -->
                        <!-- <th>Action</th>                         -->
                    </tr>
                </thead>
                <tbody>
                <?php foreach($getAllSwappingLeaves as $row){?>
                  <tr>
                        <td><?php print_r($row['leave_id'])?></td>
                        <td><?php print_r($row['first_name']." ".$row['last_name'])?></td>
                        <!-- <td><?php print_r($row['department_name'])?></td>                         -->
                        <td><?php print_r($row['leave_start_date'])?></td> 
                        <td><?php print_r($row['leave_reason'])?></td>                
                        <!-- <td><?php 
                        if($row['is_cancelled'] == "0"){
                          if($row['leave_status'] == "1"){echo "Pending";}elseif($row['leave_status'] == "2"){echo "Approved";}elseif($row['leave_status'] == "3"){echo "Rejected";}else{echo "Unapproved";}                          
                        }else{
                          echo "Self-Cancelled";
                        }
                        
                        ?></td>
                        <td><?php if($row['approved_by']!=null && $row['approved_by']!= ''){
                          $user = getUserDetail($row['approved_by']);
                          print_r($user->first_name." ".$user->last_name);                          
                        }else{
                          echo "---";
                        }?></td>                                      
                        <td><?php echo( date("Y-m-d", strtotime($row['leave_created'])) )?></td>
                        <td></td> -->
                  </tr>
                  
                <?php }?>
                </tbody>
            </table>
        </div>
    </section>
  </section>
</section>

<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script>
 $(document).ready(function() {
    $('#example').dataTable({
      "aaSorting": [[ 0, "desc" ]],
        "iDisplayLength": 10,    
        "bFilter": true,
        "bInfo": true,
        "bPaginate": true,
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }
        ]    
    });
 });
</script>

