<?php
$user = getAllUsers();
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php echo "Salary upload";?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
        			  $form_attributes = array('name' => 'uploadSalary', 'id' => 'uploadSalary', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form");
        			  if(isset($salaryEdit[0]->upload_id)){
        			  	echo form_open_multipart('superadmin/salary/addAll/'.$salaryEdit[0]->upload_id,$form_attributes);
        			  }else{
        				echo form_open_multipart(base_url().'superadmin/salary/addAll',$form_attributes);
                }
        			  ?>
                <!-- <div class="form-group">
                  <label for="firstName" class="control-label col-lg-2">User <span class="red">*</span></label>
                  <div class="col-lg-10">
          					<select name="user_id" id="user_id" class="form-control">
          						<option value="">Select user</option>
          						<?php foreach($user as $dd) { ?>
          						<option value="<?php echo $dd->user_id; ?>" <?php $value1 = (isset($salaryEdit[0]->user_id)) ? $salaryEdit[0]->user_id : 0; echo selectedVal($value1,$dd->user_id); ?>><?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?></option>
          						<?php } ?>
          					</select>
                  </div>
                </div> -->
    			       <div class="form-group">
                    <label  class="col-lg-2 control-label">Year <span class="red">*</span></label>
                    <div class="col-lg-6">
                      <select name="year" id="year" class="form-control" >
                        <?php
                        $currYear = date("Y");
                        $loopYear = $currYear - 2;
      
                        for($i = $loopYear; $i <= $currYear+1; $i++){
                          if($i == $currYear){
                            echo '<option value="'.$i.'" selected ="selected">'.$i.'</option>';
                          } else{
                            echo '<option value="'.$i.'">'.$i.'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-lg-2 control-label">Month <span class="red">*</span></label>
                    <div class="col-lg-6">
                      <select name="month" id="month" class="form-control">
                        <?php
                        $currMonth = date("m");
                        $loopMonth = $currMonth;
                        for($i = 1; $i <= 12; $i++){
                          if($i == $currMonth){
                            echo '<option value="'.sprintf("%02d", $i).'" selected ="selected">'.date("M", mktime(null, null, null, $i, 1)).'</option>';
                          } else{
                            echo '<option value="'.sprintf("%02d", $i).'">'.date("M", mktime(null, null, null, $i, 1)).'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-2 control-label">Upload zip file</label>
                    <div class="col-lg-6">
                      <input type="file" class="file-pos" name="salaryPdf" id="salaryPdf" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button class="btn btn-danger" type="submit" name="addEditSalarySlip">
                      <?php if(isset($salaryEdit[0]->upload_id)){ echo "Update";} else{echo "Submit";}?>
                      </button>
                  	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
