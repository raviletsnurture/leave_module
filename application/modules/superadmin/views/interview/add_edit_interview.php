<?php
$user = getAllUsers();
$categories = getAllInterviewCategory();

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($interviewEdit[0]->interview_id)){ echo "Edit Interview";} else{echo "Add Interview";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addInterview', 'id' => 'addInterview', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($interviewEdit[0]->interview_id)){
			  	echo form_open_multipart('superadmin/interview/add/'.$interviewEdit[0]->interview_id,$form_attributes);
			  }else{
				echo form_open_multipart(base_url().'superadmin/interview/add',$form_attributes);
			  }
			  ?>
              <div class="form-group ">
                <label for="firstName" class="control-label col-lg-2">Candidate Name <span class="red">*</span></label>
                <div class="col-lg-6">
					          <input type="text" maxlength="100" class="form-control" id="candidate_name" name="candidate_name" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->candidate_name)){ echo $interviewEdit[0]->candidate_name;} ?>">
                </div>
              </div>

			  <div class="form-group ">
                <label for="project_id" class="control-label col-lg-2">Reference Name <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="100" class="form-control" id="reference_name" name="reference_name" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->reference_name)){ echo $interviewEdit[0]->reference_name;} ?>">
                </div>
              </div>

      <!-- <div class="form-group ">
              <label for="project_id" class="control-label col-lg-2">App Receive Date <span class="red">*</span></label>
              <div class="col-lg-2">
                <input type="text" readonly="true" class="form-control date" id="app_receive_date" name="app_receive_date"   value="<?php //if(isset($interviewEdit[0]->app_receive_date)){ echo $interviewEdit[0]->app_receive_date;} ?>">
              </div>
              <div class="col-lg-1">
                <i class="fa fa-calendar" style="padding-top:4px;font-size:24px;" OnClick="$(app_receive_date).focus();"></i>
              </div>

            </div> -->
            
            <div class="form-group">
               <label for="project_id" class="control-label col-lg-2">App Receive Date <span class="red">*</span></label>
               <div class="col-lg-6">
                  <div class='input-group date' id='app_receive_date2'>
                      <input type='text' readonly="true" name="app_receive_date" class="form-control" id="app_receive_date2" value="<?php if(isset($interviewEdit[0]->app_receive_date)){ echo $interviewEdit[0]->app_receive_date;} ?>" />
                      <span class="input-group-addon">
                          <span class="fa fa-calendar"></span>
                      </span>
                  </div>
               </div>
            </div>
            
            
            
            <div class="form-group">
                <label for="project_id" class="control-label col-lg-2">Next Action Date </label>
                <div class="col-lg-6">
                  <div class='input-group date' id='interview_date2'>
                      <input readonly="true" value="<?php if(isset($interviewEdit[0]->interview_date)){ echo $interviewEdit[0]->interview_date;} ?>" type='text' name="interview_date" class="form-control" id="interviewDate" />
                      <span class="input-group-addon">
                          <span class="fa fa-calendar"></span>
                      </span>
                  </div>                
                </div>                
            </div>
            
            
          <!-- <div class="form-group ">
            <label for="project_id" class="control-label col-lg-2">Next Action Date </label>
            <div class="col-lg-2">
              <input type="text" maxlength="10" class="form-control" id="interview_date" name="interview_date" data-date-format="yyyy-mm-dd" placeholder=" "  value="<?php  //if(isset($interviewEdit[0]->interview_date)){ echo $interviewEdit[0]->interview_date;} ?>">
            </div>
            <div class="col-lg-1">
              <i class="fa fa-calendar" style="padding-top:4px;font-size:24px;" OnClick="$(interview_date).focus();"></i>
            </div>
          </div> -->

          <div class="form-group ">
            <label for="project_id" class="control-label col-lg-2">Interview Taken By <span class="red">*</span></label>
            <div class="col-lg-6">

              <select name="interview_taken_by" id="interview_taken_by" class="form-control">
                <option value="">Select User</option>
                <?php
                  foreach($user as $u)
                  {
                    echo '<option value="' .$u->user_id. '"' .(($u->user_id==$interviewEdit[0]->interview_taken_by)? ' selected' : '') . '>'.$u->first_name . ' ' . $u->last_name.'</option>';
                  }
                ?>
              </select>
            </div>

          </div>
			  <div class="form-group">
                <label  class="col-lg-2 control-label">Qualification <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="50" class="form-control" id="qualification" name="qualification" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->qualification)){ echo $interviewEdit[0]->qualification;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Current Company <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="100" class="form-control" id="current_company" name="current_company" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->current_company)){ echo $interviewEdit[0]->current_company;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Current Position <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="200" class="form-control" id="current_position" name="current_position" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->current_position)){ echo $interviewEdit[0]->current_position;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Applied Position <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="30" class="form-control" id="applied_position" name="applied_position" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->applied_position)){ echo $interviewEdit[0]->applied_position;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Current CTC (Per Annum) <span class="red">*</span></label>
                <div class="col-lg-2">
                  <input type="text" maxlength="25" class="form-control" id="current_ctc" name="current_ctc" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->current_ctc)){ echo $interviewEdit[0]->current_ctc;} ?>">
                </div>
                <label  class="col-lg-2 control-label">Expected CTC (Per Annum) <span class="red">*</span></label>
                <div class="col-lg-2">
                  <input type="text" maxlength="25" class="form-control" id="expected_ctc" name="expected_ctc" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->expected_ctc)){ echo $interviewEdit[0]->expected_ctc;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Total Experience (in years) <span class="red">*</span></label>
                <div class="col-lg-2">
                  <input type="text" maxlength="25" class="form-control" id="total_experience" name="total_experience" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->total_experience)){ echo (float)$interviewEdit[0]->total_experience+0;} ?>">
                </div>
                <label  class="col-lg-2 control-label">Notice Period (in days)<span class="red">*</span></label>
                <div class="col-lg-2">
                  <input type="text" maxlength="25" class="form-control" id="notice_period" name="notice_period" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->notice_period)){ echo $interviewEdit[0]->notice_period;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Current Location<span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="50" class="form-control" id="current_location" name="current_location" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->current_location)){ echo $interviewEdit[0]->current_location;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Contact No<span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="20" class="form-control" id="contact_number" name="contact_number" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->contact_number)){ echo $interviewEdit[0]->contact_number;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Email ID <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" maxlength="50" class="form-control" id="email" name="email" placeholder=" "  value="<?php  if(isset($interviewEdit[0]->email)){ echo $interviewEdit[0]->email;} ?>">
                </div>
        </div>

        <div class="form-group ">
                <label for="project_id" class="control-label col-lg-2">Designation <span class="red">*</span></label>
                <div class="col-lg-6">
                  <select name="cat_id" id="cat_id" class="form-control">
                    <option value="">Select Category</option>
                  <?php
                  foreach($categories as $cat)
                  {
                    echo '<option value="' .$cat->id. '"' . (($interviewEdit[0]->cat_id==$cat->id) ?  'selected'  : '') . '>' . $cat->designation . '</option>';
                  }
                  ?>

				  </select>
                </div>
              </div>

        <div class="form-group">
          <label  class="col-lg-2 control-label">Feedback <span class="red">*</span></label>
          <div class="col-lg-6">
            <textarea class="form-control" id="feedback" name="feedback"   rows="3" cols="40" placeholder=""><?php if(isset($interviewEdit[0]->feedback)){ echo $interviewEdit[0]->feedback;} ?></textarea>
          </div>
        </div>

        <div class="form-group">
          <label  class="col-lg-2 control-label">Resume </label>
          <div class="col-lg-6">
            <input type="file" class="form-control" id="resume" name="resume" style="padding-bottom:40px;" />
            <?php if(isset($interviewEdit[0]->resume)){ echo '<a target="_blank" href="' . base_url() .'uploads/resumes/'. $interviewEdit[0]->resume.'">' .$interviewEdit[0]->resume. '</a>';} ?>
          </div>
        </div>

			  <div class="form-group ">
                <label for="project_id" class="control-label col-lg-2">Status<span class="red">*</span></label>
                <div class="col-lg-6">
                  <select name="status" id="status" class="form-control">
                    <option value="">Select Status</option>
						<option value="0" <?php if(@$interviewEdit[0]->status==0) echo 'selected'; ?>>Selected</option>
            <option value="1" <?php if(@$interviewEdit[0]->status==1) echo 'selected'; ?>>Selected but not confirmed</option>
            <option value="2" <?php if(@$interviewEdit[0]->status==2) echo 'selected'; ?>>Rejected</option>
            <option value="3" <?php if(@$interviewEdit[0]->status==3) echo 'selected'; ?>>Discussion Required</option>
            <option value="4" <?php if(@$interviewEdit[0]->status==4) echo 'selected'; ?>>Other</option>
				  </select>
                </div>
              </div>


               <!--<div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($interviewEdit[0]->status)? selectedVal($interviewEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($interviewEdit[0]->status)? selectedVal($interviewEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>-->
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditInterview">
                  <?php if(isset($interviewEdit[0]->interview_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<!-- <link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" /> -->
<link href="<?php echo base_url()?>assets/css/jquery.flexdatalist.css" rel="stylesheet" />
<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script> -->
<script src="<?php echo base_url()?>assets/js/jquery.flexdatalist.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-interview.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script>
    $(function() {

var dateToday = new Date();
        //dateToday.setTime(dateToday.getTime() + (2*60*60*1000));
        dateToday.setTime(Date.now() + (2*60*60*1000));
        $('#app_receive_date2').datetimepicker({
            format: 'YYYY-MM-DD hh:mm a',
            // minDate: dateToday,
            stepping: 15,
            sideBySide: true,
            ignoreReadonly: true
        });
        
        
        
var dateToday = new Date();
        //dateToday.setTime(dateToday.getTime() + (2*60*60*1000));
        dateToday.setTime(Date.now() + (2*60*60*1000));
        $('#interview_date2').datetimepicker({
            format: 'YYYY-MM-DD hh:mm a',
            // minDate: dateToday,
            stepping: 15,
            sideBySide: true,
            ignoreReadonly: true
        });        


      //$('.my-input').flexdatalist();
      $('#candidate_name').flexdatalist({
           //limitOfValues: 2,
           //minLength: 1,
           //valueProperty: '*',
           //selectionRequired: true,
           visibleProperties: ["fullName"],
           searchIn: 'fullName',
           url: '<?php echo base_url(); ?>superadmin/interview/user_list'
      }).on('select:flexdatalist', function(event, set) {
          //alert(JSON.stringify(set));
          console.log(JSON.stringify(set));
          $('#qualification').val(set.qualification);
          $('#current_company').val(set.currentEmployer);
          $('#current_position').val(set.currentDesignation);
          $('#applied_position').val(set.designation);
          $('#current_ctc').val(set.salary);
          $('#expected_ctc').val(set.exp_salary);
          $('#total_experience').val(set.experience);
          $('#current_location').val(set.currentLocation);
          $('#contact_number').val(set.phone);
          $('#email').val(set.email);          
        //  console.log(set.text);
        //Show_Exam_Result_Detail(set.uid);
      });

    });

    </script>
