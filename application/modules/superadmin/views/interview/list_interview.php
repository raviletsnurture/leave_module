<style type="text/css">
.dataTables_filter {
     display: none;
}
.coloredBtn{
    float: right;
    padding: 15px;
    text-align: right;
    padding-right: 30px;
}
.addNew{
    padding: 15px;
}
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
tr.highlight {
    background-color: blue !important;
}
#example_interview{
  width: 100% !important;
}
tfoot {
    display: table-header-group;
}
</style>

<section id="main-content">
  <section class="wrapper site-min-height">
     <!--<div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row mbot30">
          <div class="col-lg-2">
              <div class="btn-group">
                <span class="my-span-head">Category</span>
              </div>
          </div>
          <div class="col-lg-2 pull-right">
              <div class="btn-group pull-right">
                <a href="<?php echo base_url()?>superadmin/category/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
          </div>
        </div>
      </div>-->
    <!-- page start-->
    <?php //$this->load->view('list_header'); ?>
    <section class="panel">
      <header class="panel-heading"> All Interviews </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
          <div class="row">
             <div class="col-lg-4">
                <div class="btn-group addNew">
                  <a href="<?php echo base_url()?>superadmin/interview/add">
                  <button class="btn btn-info" id="editable-sample_new"> Add New Interview <i class="fa fa-plus"></i> </button>
                  </a>
                </div>
              </div>
              <div class="col-lg-8 coloredBtn">
                  <a id="selected" href="#"><span style="background-color:#A2F5A7;width:32;height:32;padding: 5px;">Selected</span></a>
                  <a id="not_confirmed" href="#"><span style="background-color:#FCE9BB;width:32;height:32;padding: 5px;">Selected but not confirmed</span></a>
                  <a id="rejected" href="#"><span style="background-color:#F7A6B1;width:32;height:32;padding: 5px;">Rejected</span></a>
                  <a id="dis_rejected" href="#"><span style="background-color:#B5E9F7;width:32;height:32;padding: 5px;">Discussion Required</span></a>
                  <a id="other" href="#"><span style="background-color:#d3d3d3;width:32;height:32;padding: 5px;">Other</span></a>
              </div>
            </div>
        </div>

        <!-- experience Filter -->
        <div class="row" style="padding:15px;">
            <div class="col-md-2">
                <label for="">Filter Experience</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="filexpfrom" name="filexpfrom" class="form-control" placeholder="From">
            </div>
            <div class="col-md-3">
                <input type="text" id="filexpto" name="filexpto" class="form-control" placeholder="To">
            </div>
       </div>
       <!-- Applied data filter -->
       <div class="row" style="padding:15px;">
           <div class="col-md-2">
               <label for="">Filter Interview Date</label>
           </div>
           <div class="col-md-3">
               <input type="text" id="appliedStartDate" name="appliedStartDate" class="form-control date-range-filter" placeholder="From" data-date-format="dd-mm-yyyy">
           </div>
           <div class="col-md-3">
               <input type="text" id="appliedEndDate" name="appliedEndDate" class="form-control date-range-filter" placeholder="To" data-date-format="dd-mm-yyyy">
           </div>
      </div>
      <p style="padding-left:15px;"><a href="#" id="clearFilter">Clear filter</a></p>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

          <table class="table table-striped table-hover table-bordered dataTable" id="example_interview">

            <thead>
                <!-- <tr>
                  <th><input type="text" placeholder="Candidate Name" style="width: 100px;" /></th>
                  <th>Qualification</th>
                  <th><input type="text" placeholder="Applied Position" style="width: 100px;" /></th>
                  <th><small>(Per Annum)</small><input type="text" placeholder="Current CTC" style="width: 100px;" /></th>
                  <th><small>(Per Annum)</small><input type="text" placeholder="Expected CTC" style="width: 100px;" /></th>
                  <th>Total Experience</th>
                  <th>Resume</th>
                  <th><input type="text" placeholder="Interview Date" id="search_date" class="date_picker" style="width: 100px;" data-date-format="yyyy-mm-dd" /></th>
                  <th><input type="text" placeholder="Category" style="width: 80px;" /></th>
                  <th>Feedback</th>
                  <th style="display: none;">Modified Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr> -->

                 <tr>
                  <th>Candidate Name</th>
                  <th>Qualification</th>
                  <th>Applied Position</th>
                  <th><small>(Per Annum)</small>Current CTC</th>
                  <th><small>(Per Annum)</small>Expected CTC</th>
                  <th>Total Experience</th>
                  <th>Resume</th>
                  <th>Interview Date</th>
                  <th>Category</th>
                  <th>Feedback</th>
                  <th style="display: none;">Modified Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>

            </thead>
            <tfoot>
                <tr>
                  <th>Candidate Name</th>
                  <th>Qualification</th>
                  <th>Applied Position</th>
                  <th>Current CTC</th>
                  <th><small>(Per Annum)</small>Expected CTC</th>
                  <th>Total Experience</th>
                  <th>Resume</th>
                  <th>Interview Date</th>
                  <th>Category</th>
                  <th>Feedback</th>
                  <th style="display: none;">Modified Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
              <?php foreach($interview as $row){ ?>
                <?php
                  $rowColor = '';
                  $intStatus = '';
                  if($row->status==0)
                  {
                    $rowColor = '#A2F5A7';
                    $intStatus = 'Selected';
                  }else if($row->status==1)
                  {
                    $rowColor = '#FCE9BB';
                    $intStatus = 'Selected but not confirmed';
                  }else if($row->status==2)
                  {
                    $rowColor = '#F7A6B1';
                    $intStatus = 'Rejected';
                  }else if($row->status==3)
                  {
                    $rowColor = '#B5E9F7';
                    $intStatus = 'Discussion Required';
                  }else if($row->status==4)
                  {
                    $rowColor = '#d3d3d3';
                    $intStatus = 'Other';
                  }
                ?>
              <tr <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>><?php echo $row->candidate_name;?></td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>><?php echo $row->qualification;?></td>
				<td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>><?php echo $row->applied_position;?></td>
				<td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>><?php echo $row->current_ctc ; ?></td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>><?php echo $row->expected_ctc ; ?></td>
				<td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>><?php echo $row->total_experience; ?></td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>>
                    <a href="<?php echo base_url(). 'uploads/resumes/' . $row->resume; ?>" target="_blank"><?php echo $row->resume; ?></a>
                </td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>>
                    <?php echo $row->interview_date; ?>
                </td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>>
                    <?php echo $row->cat_name; ?>
                </td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>>
                    <span title="<?php echo html_escape($row->feedback); ?>">
                        <?php
                        $out = strlen($row->feedback) > 50 ? substr($row->feedback,0,50)."..." : $row->feedback;
                        echo  html_escape($out);
                        ?>
                    </span>
                </td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>><?php echo $row->interview_modified; ?></td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>>
                    <?php echo $row->status; ?>
                </td>
                <td <?php echo 'style="background-color:' .$rowColor. ' !important;"'; ?>>
                  <a href="<?php echo base_url()?>superadmin/interview/edit/<?php echo $row->interview_id;?>" >
                  <button class="btn btn-primary btn-xs" title=""><i class="fa fa-pencil"></i></button>
                  </a>
                  <a href="<?php echo base_url()?>superadmin/interview/delete/<?php echo $row->interview_id;?>" class="deleteRec">
                  <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                  </a>

                  <a value="<?php echo $row->interview_id;?>"  href="#" onclick="putComment(<?php echo $row->interview_id;?>)">
                      <button class="btn btn-primary btn-xs tooltips" data-original-title="Comment" title=""><i class="fa fa-comment"></i></button>
                  </a>
                  </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
          <div class="modal fade" id="putCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="putCommentModalLabel">Comment</h4>
                            </div>
                            <div class="modal-body">
                            </div>
                        </div>
                    </div>
                </div>

          <?php if(isset($links)){echo $links;} ?>

        </div>
      </div>
    </section>
    <!-- page end-->

  </section>
</section>
<!-- <link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/> -->
<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>


<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css" />
<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script> -->
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<style>
table{
  margin: 0 auto;
  width: 100%;
  clear: both;
  border-collapse: collapse;
  table-layout: fixed;
  word-wrap:break-word;
}
</style>
<script type="text/javascript" charset="utf-8">

function putComment(id){
        $.ajax({
            url: '<?php echo base_url(); ?>superadmin/interview/putComment',
            data: {id: id},
            dataType: 'html',
            type: 'POST',
            success: function(data){
                $('#putCommentModal .modal-body').html(data);
                $('#putCommentModal').modal('show');
            }
        });
    }
          function ff(sel)
          {
          	var ff = sel.value.split(",");
          	if(ff == '')
          	{
          		alert("Please Select an Option");
          		location.reload();
          		//return false;
          	}
          	else
          	{
          		var r = confirm("Are You Sure for this operation");
          		if (r == true)
          		{
          			$.ajax({
          			 url: '<?php echo base_url(); ?>superadmin/interview/updatestaus/',
          			 data: { interview_id: ff[1], status: ff[0]},
          			 dataType: 'html',
          			 type: 'POST',
          			 success: function(data){
          				 //$('.modal-body').html(data);
          				 //$('#myModal').modal('show');

          				 alert("Leave Status Has Been Changed");
          				 //location.reload();
          				 }
          			});
          		}
          	 }
          }


          function stat(sel)
          {
          	var ff = sel.value.split(",");
          	if(ff == '')
          	{
          		alert("Please Select an Option");
          		location.reload();
          		//return false;
          	}
          	else
          	{
          		var r = confirm("Are You Sure for this operation");
          		if (r == true)
          		{
          			$.ajax({
          			 url: '<?php echo base_url(); ?>superadmin/interview/up_staus/',
          			 data: { leave_id: ff[1], status: ff[0], userid: ff[2]},
          			 dataType: 'html',
          			 type: 'POST',
          			 success: function(data){
          				 //$('.modal-body').html(data);
          				 //$('#myModal').modal('show');
          				 alert("Interview Status Has Been Changed");
          				 location.reload();
          				 }
          			});
          		}
          	 }
          }
          $(document).ready(function(){
            /* var table =  $('#example_interview').DataTable( {
                  "aaSorting": [[0, false], [ 10, "desc" ]],
      				   "iDisplayLength": 10,
      				   "pagingType": "full_numbers",
      				   //"dom": 'Cfrtip',
      				   "destroy": false,
                       "lengthChange":true,
                       "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      				   //"bFilter": false,
      				   "bPaginate": true,
                       "scrollY": true,
      				   "bInfo" : true,
                       "scrollX": true,
                       "scrollCollapse": true,
                       "aoColumnDefs": [
                           { 'bSortable': false, 'aTargets': [ 0,2,3,4,6,7,8,9,11] } // 0, 1, 12, 13
                       ],
                 "initComplete": function(settings, json) {
                   $("table thead input").on( 'keyup', function () {
                            table
                               .column( $(this).parent().index()+':visible' )
                               .search( this.value )
                               .draw();
                    });
                    var checkin = $(".date_picker").fdatepicker({
                        autoclose: true,
                        //daysOfWeekDisabled: [0, 6],
                        onRender: function(date) {
                            //return date.valueOf() < now.valueOf() ? 'disabled' : '';

                        }
                    }).on('changeDate', function(ev) {
                        //if (ev.date.valueOf() > checkout.date.valueOf())
                        //{
                        var newDate = new Date(ev.date)
                        newDate.setDate(newDate.getDate());
                        //checkout.update(newDate);
                        //}
                        checkin.hide();
                        console.log(this.value);
                        //$('#leave_end_date')[0].focus();
                        //$('#leave_start_date').datepicker('hide');
                        table
                           .column( $(this).parent().index()+':visible' )
                           .search( this.value )
                           .draw();
                    }).data('datepicker');
                 },
                 "columnDefs": [
                    { "width": "10%", "targets": 0 },
                    { "width": "10%", "targets": 0 },
                    { "width": "20%", "targets": 0 },
                  ]
              } );
              //table.fnSetColumnVis( 8, false );
              table.column(10).visible(false);
              table.column(11).visible(false);*/


                $(function() {
                 $('#interview_datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
                });              
                // Setup - add a text input to each footer cell
                $('#example_interview tfoot th').each( function () {
                    var title = $(this).text();
                    if (title === "Interview Date") {
                      $(this).html( '<input type="text" id="interview_datepicker" placeholder="'+title+'" />' );
                    }else {
                      $(this).html( '<input type="text" placeholder="'+title+'" />' );
                    }
                });

                // DataTable
                var table = $('#example_interview').DataTable({                     
                    "aaSorting": [[0, false], [ 10, "desc" ]],
                    "iDisplayLength": 10,
                    "pagingType": "full_numbers",                    
                    "destroy": false,
                    "lengthChange":true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],                   
                    "bPaginate": true,                    
                    "bInfo" : true,
                    "scrollX": false,
                    "scrollCollapse": false,
                    "aoColumnDefs": [
                      { 'bSortable': false, 'aTargets': [ 0,2,3,4,6,7,8,9,11] } // 0, 1, 12, 13
                    ]
                });

                $('#example_interview tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
                });


                $('#example_interview tbody')
                    .on( 'mouseenter', 'td', function () {
                        var colIdx = table.cell(this).index().column;
                        $( table.cells().nodes() ).removeClass( 'highlight' );
                        $( table.column( colIdx ).nodes() ).addClass( 'highlight' );
                });
                $('#button').click( function () {
                    alert( table.rows('.selected').data().length +' row(s) selected' );
                });
                // Apply the search
                table.columns().every( function () {
                    var that = this;
                    $( 'input', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
                table.column(10).visible(false);
                table.column(11).visible(false);


              // Filter experience Functionality
              $.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
                      var min = parseFloat( $('#filexpfrom').val(), 10 );
                      var max = parseFloat( $('#filexpto').val(), 10 );
                      var age = parseFloat( data[5] ) || 0;
                      if ( ( isNaN( min ) && isNaN( max ) ) || ( isNaN( min ) && age <= max ) || ( min <= age   && isNaN( max ) ) || ( min <= age   && age <= max ) ) {
                         return true;
                      }
                          return false;
              });

              $('#filexpfrom, #filexpto').keyup( function() {
                  table.draw();
              });
              $.fn.dataTable.ext.search.push(
                  function (settings, data, dataIndex) {
                     var min = $('#appliedStartDate').datepicker("getDate");
                     var max = $('#appliedEndDate').datepicker("getDate");
                     var startDate = new Date(data[7]);
                     if (min == null && max == null) { return true; }
                     if (min == null && startDate <= max) { return true;}
                     if(max == null && startDate >= min) {return true;}
                     if (startDate <= max && startDate >= min) { return true; }
                     return false;
                  }
              );

              $("#appliedStartDate").datepicker({ onSelect: function() { table.draw(); }, changeMonth: true, changeYear: true });
              $("#appliedEndDate").datepicker({ onSelect: function() { table.draw(); }, changeMonth: true, changeYear: true });

              $('#selected').click(function() {
                  table.columns(11).search(0).draw();
              });
              $('#not_confirmed').click(function() {
                  table.columns(11).search(1).draw();
              });
              $('#rejected').click(function() {
                  table.columns(11).search(2).draw();
              });
              $('#dis_rejected').click(function() {
                  table.columns(11).search(3).draw();
              });
              $('#other').click(function() {
                  table.columns(11).search(4).draw();
              });
              $('#clearFilter').click(function() {
                  $('#selected, #not_confirmed ,#rejected, #dis_rejected, #other, #filexpfrom, #filexpto, #appliedStartDate, #appliedEndDate').val('');
                  table.search('').draw();
                  table.columns().search("").draw();
              });
    	  });
      </script>
