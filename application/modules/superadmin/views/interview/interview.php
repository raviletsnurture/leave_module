<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<style>#example td p { min-height:51px;}</style>
<div class="ajaxLoader">
    <img src="<?php echo base_url()?>assets/img/loader.gif" alt="loading..." >
</div>
<section id="main-content">
    <section class="wrapper site-min-height" style="margin-top:0px !important;">
        <section class="panel">
            <header class="panel-heading">Recruitment Details</header>
            <div class="panel-body">
            <div class="col-lg-2 pull-right">
            
                <div class="btn-group addNew">
                  <a href="<?php echo base_url()?>superadmin/interview/add">
                  <button class="btn btn-info" id="editable-sample_new"> Add New Interview <i class="fa fa-plus"></i> </button>
                  </a>
                </div>

          </div>
                <form id="form-filter" class="form-horizontal">
                        
                    <div class="row">
                        <div class="col-md-2">
                            <label for="">Designation</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo $designationCountArray; ?>
                            </div>
                        </div>
                    </div>

                    <!-- experience Filter -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="">Filter Experience</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="filexpfrom" name="filexpfrom" class="form-control" placeholder="From">
                            </div>   
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="filexpto" name="filexpto" class="form-control" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <!-- Applied data filter -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="">Filter Applied Date</label>
                        </div>
                        <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" id="appliedStartDate" name="appliedStartDate" class="form-control date-range-filter" placeholder="From" data-date-format="dd-mm-yyyy">
                                </div>    
                        </div>
                        <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" id="appliedEndDate" name="appliedEndDate" class="form-control date-range-filter" placeholder="To" data-date-format="dd-mm-yyyy">
                                </div>
                        </div>
                        <div class="col-md-3">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>   
                </form>
                
                <table id="example" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                      <td><p>Candidate Name</p> <input type="text" id="candidate_name" name="candidatename" class="form-control" placeholder="Name"></td>
                      <td><p>Qualification </p><input type="text" id="qualification" name="qualification" class="form-control" placeholder="Qualification"></td>
                      <td><p>Applied Position</p> <input type="text" id="applied_position" name="applied_position" class="form-control" placeholder="Applied Position"></td>
                      <td><p><small>(Per Year)</small><br/>Current CTC </p><input type="text" id="current_ctc" name="current_ctc" class="form-control" placeholder="Current CTC"></td>
                      <td><p><small>(Per Year)</small><br/>Expected CTC </p><input type="text" id="expected_ctc" name="expected_ctc" class="form-control" placeholder="Expected CTC"></td>
                      <td><p>Total Experience </p><input type="text" id="experience" name="experience" class="form-control" placeholder="Experience"></td>
                      <td><p>Resume </p></td>
                      <td><p>Interview Date </p><input type="text" id="interview_date" name="interview_date" class="form-control" placeholder="Interview Date"></td>
                      <td><p>Category </p><input type="text" id="category" name="category" class="form-control" placeholder="Category"></td>
                      <td><p>Feedback </p><input type="text" id="feedback" name="feedback" class="form-control" placeholder="Feedback"></td>
                      <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>        
                    <tfoot>
                        <tr>
                            <th>Candidate Name</th>
                            <th>Qualification</th>
                            <th>Applied Position</th>
                            <th><small>(Per Year)</small><br/>Current CTC</th>
                            <th><small>(Per Year)</small><br/>Expected CTC</th>
                            <th>Total Experience</th>
                            <th>Resume</th>
                            <th>Interview Date</th>
                            <th>Category</th>
                            <th>Feedback</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>    
            </div>
        </section>   
    </section>
</section>
  <div class="modal fade" id="putCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="putCommentModalLabel">Comment</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
  </div>
         
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
 
<script type="text/javascript">

    $("#example").on("click",".deleteRec", function(){
      var del = confirm("Are you sure delete this records?");
      if(del == false){
        return false;
      }
    });

    function putComment(id){
      $.ajax({
          url: '<?php echo base_url(); ?>superadmin/interview/putComment',
          data: {id: id},
          dataType: 'html',
          type: 'POST',
          success: function(data){
              $('#putCommentModal .modal-body').html(data);
              $('#putCommentModal').modal('show');
          }
      });
    }

    var table; 
    $(document).ready(function() {
        // Send mail model
        var dateToday = new Date();
        //dateToday.setTime(dateToday.getTime() + (2*60*60*1000));
        dateToday.setTime(Date.now() + (2*60*60*1000));
        $('#confirmationDate').datetimepicker({
            format: 'YYYY-MM-DD hh:mm a',
            minDate: dateToday,
            stepping: 15,
            sideBySide: true,
        });

        $("#appliedStartDate").datepicker({  
            dateFormat: 'yy-mm-dd',
            changeMonth: true, 
            changeYear: true 
        });
        $("#appliedEndDate").datepicker({  
            dateFormat: 'yy-mm-dd',
            changeMonth: true, 
            changeYear: true 
        });
    
        //datatables
        table = $('#example').DataTable({  
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,
            searching: false,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('superadmin/interview/ajax_list')?>",
                "type": "POST",
                "data": function ( data ) {
                    data.candidate_name = $('#candidate_name').val();
                    data.qualification = $('#qualification').val();
                    data.applied_position = $('#applied_position').val();
                    data.current_ctc = $('#current_ctc').val();
                    data.expected_ctc = $('#expected_ctc').val();
                    data.experience = $('#experience').val();
                    data.interview_date = $('#interview_date').val();
                    data.category = $('#category').val();
                    data.feedback = $('#feedback').val();
                    data.filexpfrom = $('#filexpfrom').val();
                    data.filexpto = $('#filexpto').val();
                    data.status = $('#status').val();
                    data.appliedStartDate = $('#appliedStartDate').val();
                    data.appliedEndDate = $('#appliedEndDate').val();
                }
            }
        });
    
        $('#btn-filter').click(function(){ //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function(){ //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });
        
        $("#candidate_name").on('keyup', function (e) {  if (e.keyCode == 13) { table.ajax.reload(); } });

        $("#qualification").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#applied_position").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#current_ctc").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload(); } });

        $("#expected_ctc").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#experience").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#interview_date").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#category").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#feedback").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });
    
    });
 
</script>
