<!-- Modal -->
<form name="forgotForm" class="cmxform form-horizontal tasi-form form-signin" method="post" id="forgotForm">
  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Forgot Password ?</h4>
        </div>
        <div class="alert alert-block alert-danger fade in forgot-error " style="display:none">
          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
          <strong>Oh snap!</strong> <span class="error_snap"></span> </div>
        <div class="alert alert-success fade in forgot-success " style="display:none">
          <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
          <strong>Success!</strong> <span class="success_snap"></span> </div>
        <div class="modal-body">
          <p>Enter your username below to reset your password.</p>
          <div class="form-group ">
            <div class="col-lg-12">
              <input type="text" placeholder="Username" autocomplete="off" name="userName" id="userName" class="form-control placeholder-no-fix">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
          <input value="Submit" class="btn btn-success" type="submit">
        </div>
      </div>
    </div>
  </div>
</form>
<!-- modal -->
<form class="cmxform form-horizontal tasi-form form-signin" action="" method="post" id="loginForm" name="loginForm">
  <h2 class="form-signin-heading">Admin sign in</h2>
  <?php if($this->session->flashdata('error')){?>
  <div class="alert alert-block alert-danger fade in">
    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
  <?php } ?>
  <?php if($this->session->flashdata('success')){?>
  <div class="alert alert-success fade in">
    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
    <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
  <?php }?>
  <div class="login-wrap">
    <div class="form-group ">
      <div class="col-lg-12">
        <input type="text" class="form-control" placeholder="Username" name="sUsername" id="sUsername" autofocus value="<?php echo $sUsername_cookie;?>">
      </div>
    </div>
    <div class="form-group ">
      <div class="col-lg-12">
        <input type="password" class="form-control" placeholder="Password" name="sPassword" id="sPassword" value="<?php echo base64_decode($sPassword_cookie);?>">
        <span toggle="#sPassword" class="fa fa-fw fa-eye field-icon toggle-password">
      </div>
    </div>
    
    <!--<label class="checkbox">
      <input type="checkbox" name="remember" id="remember" <?php //if($sUsername_cookie && base64_decode($sPassword_cookie))echo "checked";?>>
      Remember me <span class="pull-right"> <!--<a data-toggle="modal" href="#myModal">Forgot Password?</a> </span> 
    </label>-->
    <input name="submitLogin" class="btn btn-lg btn-login btn-block" type="submit" value="Login">
    
  </div>
</form>
 

<script src="<?php echo base_url()?>assets/js/validate/form-validation-superadmin-login.js" ></script> 
<script type="text/javascript">
$(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
      
    } else {
      input.attr("type", "password");
    }
});
</script>