  <?php

 // echo "<pre>";
 // print_r($leave);
 // exit;

 $department = getAllDepartment();
 $leave_type = getAllLeaveType();


 if (!function_exists('array_group_by')) {
  /**
   * Groups an array by a given key.
   *
   * Groups an array into arrays by a given key, or set of keys, shared between all array members.
   *
   * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
   * This variant allows $key to be closures.
   *
   * @param array $array   The array to have grouping performed on.
   * @param mixed $key,... The key to group or split by. Can be a _string_,
   *                       an _integer_, a _float_, or a _callable_.
   *
   *                       If the key is a callback, it must return
   *                       a valid key from the array.
   *
   *                       If the key is _NULL_, the iterated element is skipped.
   *
   *                       ```
   *                       string|int callback ( mixed $item )
   *                       ```
   *
   * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
   */
  function array_group_by(array $array, $key)
  {
    if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
      trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
      return null;
    }

    $func = (!is_string($key) && is_callable($key) ? $key : null);
    $_key = $key;

    // Load the new array, splitting by the target key
    $grouped = [];
    foreach ($array as $value) {
      $key = null;

      if (is_callable($func)) {
        $key = call_user_func($func, $value);
      } elseif (is_object($value) && isset($value->{$_key})) {
        $key = $value->{$_key};
      } elseif (isset($value[$_key])) {
        $key = $value[$_key];
      }

      if ($key === null) {
        continue;
      }

      $grouped[$key][] = $value;
    }
    // Recursively build a nested grouping if more parameters are supplied
    // Each grouped array value is grouped according to the next sequential key
    if (func_num_args() > 2) {
      $args = func_get_args();

      foreach ($grouped as $key => $value) {
        $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
        $grouped[$key] = call_user_func_array('array_group_by', $params);
      }
    }
    return $grouped;
  }
 }
?>
<!-- <link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/> -->
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet"/>
<style type="text/css">
label{
  padding-top:5px;
}
.row{
  margin:10px 0 0 0;
}
.dataTables_filter {
     display: none;
}
.graph-section{
  width: 100%;
  height:440px;
  margin:0 0 10px 0 ;
  /*display: none;*/
}
#graph_detail{
  margin:10px 0;
}
#graph_section{
  width: 100%;
  height: 400px;
  margin-top:20px;
  border: 1px solid silver;
  border-radius: 3px;
}
.select_month .ui-datepicker-calendar,.select_year .ui-datepicker-calendar,.select_year .ui-datepicker-month{
    display: none;
}
.dataTables_paginate, .dataTables_info{
    margin-top: 15px;
}
.ajaxLoader{
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.2);
    z-index: 1;
    display: none;
}
.ajaxLoader img{
    left:50%;
    top:50%;
    position: fixed;
    width: 30px;
}
</style>
<div class="ajaxLoader">
    <img src="<?php echo base_url()?>assets/img/loader.gif" alt="loading..." >
</div>
<section id="main-content">
  <section class="wrapper site-min-height">
    <!-- page start-->
    <?php //$this->load->view('list_header'); ?>
    <section class="panel">
      <header class="panel-heading"> Leave Records </header>

      <div class="panel-body">
        <div class="row">

          <div class="col-md-2">
            <label for="department">Department</label>
          </div>
          <div class="col-md-3">
            <select id="department_name" name="department_name" class="form-control">
              <option value="">Select Department</option>
              <?php foreach($department as $row1){?>
              <option value="<?php echo $row1->department_name; ?>"><?php echo $row1->department_name;?></option>
              <?php } ?>
            </select>
          </div>

          <div class="col-md-2">
            <label for="department">Type</label>
          </div>
          <div class="col-md-3">
            <select id="date_type" name="date_type" class="form-control">
              <option value="">Select Date Type</option>
              <option value="date_range">Date Range</option>
              <option value="month">Monthly</option>
              <option value="year">Yearly</option>
            </select>
          </div>

        </div>
        <div class="row">

          <div class="col-md-2">
            <label for="department">Select Month</label>
          </div>
          <div class="col-md-3">
            <input type="text" name="start_date" id="select_month" class="form-control" data-date-format="yyyy-mm-dd" disabled>
          </div>

          <div class="col-md-2">
            <label for="department">Select Year</label>
          </div>
          <div class="col-md-3">
            <input type="text" name="end_date" id="select_year" class="form-control" data-date-format="yyyy-mm-dd" disabled>
          </div>

        </div>
        <div class="row">

          <div class="col-md-2">
            <label for="department">Start Date</label>
          </div>
          <div class="col-md-3">
            <input type="text" name="start_date" id="start_date" class="form-control" data-date-format="yyyy-mm-dd" disabled>
          </div>

          <div class="col-md-2">
            <label for="department">End Date</label>
          </div>
          <div class="col-md-3">
            <input type="text" name="end_date" id="end_date" class="form-control" data-date-format="yyyy-mm-dd" disabled>
          </div>

        </div>
        <div class="row">
          <div class="col-md-2">
            <label for="department">Leave Type</label>
          </div>
          <div class="col-md-3">
            <select id="leave_type" name="leave_type" class="form-control">
              <option value="">Leave Type</option>
              <?php foreach($leave_type as $row1){?>
              <option value="<?php echo $row1->leave_type_id; ?>"><?php echo $row1->leave_type;?></option>
              <?php } ?>
            </select>
          </div>

          <div class="col-md-2 ">
            <button type="button" name="reset" id="reset" class="form-control btn btn-info">Reset</button>
          </div>


        </div>
<!--
        <div class="row">
          <div class="col-md-2">
            <label for="department">Leave Type</label>
          </div>
            <div class="col-md-6 ">
              <input type="radio" name="chartType" id="chartType" value="1" > Department Wise
              <input type="radio" name="chartType" value="2" checked> Leave Type Wise
            </div>
            </div>

        <div class="row">

          <div class="col-md-2 col-md-offset-2">
            <button type="button" name="reset" id="reset" class="form-control btn btn-info">Reset</button>
          </div>
        </div>

-->


        <div class="row" id="graph_0">
          <div class="col-md-12">
          <!--  <button class="btn btn-success" id="graph_detail"> View Leave Report <i class="fa fa-angle-down"></i> </button> -->
              <div class="graph-section" id="graph_portion">
                <!-- show graph -->
                <div id="graph_section"></div>
                <!-- show graph -->
              </div>
          </div>
        </div>

        <!-- <div class="row" id="graph_1">
          <div class="col-md-12">

              <div class="graph-section" id="graph_portion2">

                <div id="graph_section2"></div>

              </div>
          </div>
        </div> -->


        <div class="row" id="loadtabledata">
          <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Department</th>
                <th>Total Leaves</th>
                <th>Pending Leave</th>
            </tr>
        </thead>
        <!-- <tfoot>
            <tr>
              <th>Name</th>
              <th>Department</th>
              <th>Total Leaves</th>
            </tr>
        </tfoot> -->
        <tbody id="table_body">
          <?php
            foreach($leave as $row){ ?>
              <tr id="<?php echo $row->user_id; ?>" <?php if($row->status=='Inactive') echo 'style="color:red;"'; ?> >
                  <td>
                  <a href="<?php echo base_url(); ?>superadmin/leave_records/userLeave/<?php echo $row->user_id; ?>" <?php if($row->status=='Inactive') { echo 'style="color:red;text-decoration:underline"'; }else{ 
                    echo 'style="text-decoration:underline"'; } ?>><?php echo $row->first_name.' '.$row->last_name; ?></a>
                  </td>
                  <td><?php echo $row->department_name; ?></td>
                  <td><?php echo $row->total_leave; ?></td>
                  <td><?php $P_leave = $this->leave_records_model->pendingleave($row->user_id); ?>
                  <a href="<?php echo base_url(); ?>superadmin/leave_records/userPendingLeave/<?php echo $row->user_id; ?>"><?php echo $P_leave->pendingLeavetotal; ?></a>
                  
                  </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
        </div>
      </div>
    </section>  
  </section>
</section>

<!-- <script src="//code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script type="text/javascript">
 /* Highcharts.setOptions({
    colors: ['#82CEFC', '#706F69', '#95ff9c', '#e57272', '#c788e2', '#0f0e4f', '#8e6c23', '#FFF263', '#6AF9C4']
  });
  chart = new Highcharts.chart('graph_section', {    
    chart: {
        type: 'column'
    },
    title: {
        text: 'Leave Records'
    },
    xAxis: {    
        categories: [<?php echo $monthData; ?>],
        showEmpty: false
    },
    exporting: {
        enabled: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Leaves'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: 'gray'
            }
        }
    },
    legend: {
        enabled: true
    },credits :{
        enabled:false
    },
    plotOptions: {
      column: {
            stacking: 'normal',
            dataLabels: {
               enabled: true,
               color: 'white'
           }
        },
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                formatter: function() {
                    if (this.y != 0) {
                      return this.y;
                    } else {
                      return null;
                    }
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.x} {series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
    },
    series: [
    <?php foreach($chartDetail as $key => $value): ?>
      {
      name: '<?php echo $key; ?>',
      data: [<?php  echo $value; ?>],
      },
    <?php endforeach; ?>
    ],
      drilldown: {
          series: [{
              name: 'Chrome',
              id: 'Chrome',
              data: [
                  ['v40.0',5],['v42.0',3.68],['v39.0',2.96],
                  ['v36.0',2.53],['v43.0',1.45],['v31.0',1.24],
                  ['v35.0',0.85],['v38.0',0.6],['v32.0',0.55],
                  ['v37.0',0.38],['v33.0',0.19],['v34.0',0.14],
                  ['v30.0',0.14]
              ]
          }, {
              name: 'Firefox',
              id: 'Firefox',
              data: [
                  ['v35',2.76],['v36',2.32],['v37',2.31],
                  ['v34',1.27],['v38',1.02],['v31',0.33],
                  ['v33',0.22],['v32',0.15]
              ]
          }]
      }
  });*/
</script>
<script type="text/javascript">
  //$('#graph_portion').slideUp();
  $('#graph_detail').click(function(){
    //$('#graph_portion').slideToggle( "normal");
  });
  // Create the chart
</script>

<script type="text/javascript">
  $(document).ready(function(){
    chartDisplay(); 
    //Reset Button
    $('#reset').click(function(){
      window.location.reload();
      /*
      $('#start_date,#end_date,#select_month,#select_year').prop('disabled',true);
      $('#start_date,#end_date,#select_month,#select_year').val('');
      $('#date_type').val('');
      $('#department_name,#leave_type').val('');
      UpdateTable();
      */
    });

    $('input[type=radio][name=chartType]').change(function() {
        if (this.value == '1') {
          $('#graph_0').hide();
          $('#graph_1').show();
        }else{
          $('#graph_0').show();
          $('#graph_1').hide();
        }

    });


    //Date Type logic
    $('#date_type').change(function(){
      var date_type = $(this).val();

      if(date_type == 'date_range'){

        $('#start_date').prop('disabled',false);
        $('#end_date').prop('disabled',false);
        $('#select_month,#select_year').prop('disabled',true);
        $('#select_month,#select_year').val('');

      }else if(date_type == 'month'){

        $('#select_month').prop('disabled',false);
        $('#start_date,#end_date,#select_year').prop('disabled',true);
        $('#start_date,#end_date,#select_year').val('');

      }else if(date_type == 'year'){

        $('#select_year').prop('disabled',false);
        $('#start_date,#end_date,#select_month').prop('disabled',true);
        $('#start_date,#end_date,#select_month').val('');

      }else if(date_type == 'quarter'){
        $('#start_date,#end_date,#select_month,#select_year').prop('disabled',true);
        $('#start_date,#end_date,#select_month,#select_year').val('');
      }else{
        $('#start_date,#end_date,#select_month,#select_year').prop('disabled',true);
        $('#start_date,#end_date,#select_month,#select_year').val('');
        UpdateTable();
      }
    });

    //Datatable Filter
      dt =  $('#example').DataTable({
       "aaSorting": [[ 0, "asc" ]],
       "sDom": '<"top"f>rt<"bottom"ilp><"clear">',
      });

    //Date Picker
    $('#select_month').datepicker({
      autoClose:true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      yearRange: '2016:' + new Date().getFullYear(),
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').addClass(this.id);
        $('#ui-datepicker-div').removeClass('select_year');
      },
      onClose: function(dateText, inst) {
        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        UpdateTable();
      }
    });
    $('#select_year').datepicker({
      autoClose:true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      yearRange: '2016:' + new Date().getFullYear(),
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').addClass(this.id);
        $('#ui-datepicker-div').removeClass('select_month');
      },
      onClose: function(dateText, inst) {
        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        UpdateTable();
      }
    });


    /*$('#start_date,#end_date').datepicker({
      autoClose:true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      yearRange: '2016:' + new Date().getFullYear(),
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').removeClass('select_month select_year');
      },
    }).on("change", function() {
      if($('#start_date').val() != '' && $('#end_date').val() != '')
      {
       UpdateTable();
      }
    });*/

    $("#start_date").datepicker({       
      onClose: function (selectedDate) {
        var dt = new Date(selectedDate);
        var dt1 = new Date(selectedDate);
        dt.setMonth(dt.getMonth() + 10);
        dt1.setMonth(dt1.getMonth());
        $("#end_date").datepicker("option", "maxDate", dt); 
        $("#end_date").datepicker("option", "minDate", dt1); 
      },
      changeMonth: true, 
      changeYear: true,
      autoClose:true, 
      showButtonPanel: true,
      yearRange: '2016:' + new Date().getFullYear(),
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').removeClass('select_month select_year');
      },
    });
    $("#end_date").datepicker({  
      /*onClose: function (selectedDate) {
        var dt = new Date(selectedDate);
        var dt1 = new Date(selectedDate);
        dt.setMonth(dt.getMonth() + 2);
        dt1.setMonth(dt1.getMonth() - 2);
        $("#start_date").datepicker("option", "maxDate", dt); 
        $("#start_date").datepicker("option", "minDate", dt1); 
      },*/
      changeMonth: true, 
      changeYear: true,
      autoClose:true, 
      showButtonPanel: true,
      yearRange: '2016:' + new Date().getFullYear(),
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').removeClass('select_month select_year');
      }, 
    }).on("change", function() {
      if($('#end_date').val() != '')
      {
       UpdateTable();
      }
    });


    $('#department_name').change(function(){
      UpdateTable();
    });

    $('#leave_type').change(function(){
      if($('#leave_type').val() == '')
      {
        location.reload();
      }else{
        UpdateTable();
      }
    });

    function chartDisplay(){
      var department = $('#department_name').val();
      var start_date = $('#start_date').val();
      var end_date = $('#end_date').val();
      var month = $('#select_month').val();
      var year = $('#select_year').val();
      var leave_type = $('#leave_type').val();       
      //Get Fil tered Data          
          $.ajax({
            type: 'POST',
            data: {'start_date' : start_date, 'end_date' : end_date,'department' : department, 'month' : month, 'year' : year , 'leave_type' : leave_type},
            url: '<?php echo base_url()?>superadmin/leave_records/loadtable',
            cache: false,
            dataType: "html",
            beforeSend: function(){
               $('.ajaxLoader').show();
            },
            success:function(chart_data){              
              $('.ajaxLoader').hide();
              var json = $.parseJSON(chart_data);   
              monthData = json.monthData;   
              var chartDetail = json.chartDetail;
              leave_chart(monthData,chartDetail);
            }
         });
    }

    function UpdateTable(){
      var department = $('#department_name').val();
      var start_date = $('#start_date').val();
      var end_date = $('#end_date').val();
      var month = $('#select_month').val();
      var year = $('#select_year').val();
      var leave_type = $('#leave_type').val();       
      //Get Fil tered Data
          $('#example').DataTable().destroy();
          $.ajax({
            type: 'POST',
            data: {'start_date' : start_date, 'end_date' : end_date,'department' : department, 'month' : month, 'year' : year , 'leave_type' : leave_type},
            url: '<?php echo base_url()?>superadmin/leave_records/loadtable',
            cache: false,
            dataType: "html",
            beforeSend: function(){
               $('.ajaxLoader').show();
            },
            success:function(chart_data){              
              $('.ajaxLoader').hide();
              var json = $.parseJSON(chart_data);              
              $('#table_body').html(json['table']);
              $('#example').DataTable({
                  "aaSorting": [[ 0, "asc" ]],
                  "sDom": '<"top"f>rt<"bottom"ilp><"clear">',
              });
              monthData = json.monthData;   
              var chartDetail = json.chartDetail;
              leave_chart(monthData,chartDetail);
            }
         });
    }


    function leave_chart(monthData,chartDetail){
        Highcharts.setOptions({
                colors: ['#82CEFC', '#706F69', '#95ff9c', '#e57272', '#c788e2', '#0f0e4f', '#8e6c23', '#FFF263', '#6AF9C4']
              });
              chart = new Highcharts.chart('graph_section', {    
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Leave Records'
                },
                xAxis: {    
                    categories: monthData,
                    showEmpty: false
                },
                exporting: {
                    enabled: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Leaves'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: 'gray'
                        }
                    }
                },
                legend: {
                    enabled: true
                },credits :{
                    enabled:false
                },
                plotOptions: {
                  column: {
                        stacking: 'normal',
                        dataLabels: {
                           enabled: true,
                           color: 'white'
                       }
                    },
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {
                                if (this.y != 0) {
                                  return this.y;
                                } else {
                                  return null;
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.x} {series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
                },
                series: eval(chartDetail),
                  drilldown: {
                      series: [{
                          name: 'Chrome',
                          id: 'Chrome',
                          data: [
                              ['v40.0',5],['v42.0',3.68],['v39.0',2.96],
                              ['v36.0',2.53],['v43.0',1.45],['v31.0',1.24],
                              ['v35.0',0.85],['v38.0',0.6],['v32.0',0.55],
                              ['v37.0',0.38],['v33.0',0.19],['v34.0',0.14],
                              ['v30.0',0.14]
                          ]
                      }, {
                          name: 'Firefox',
                          id: 'Firefox',
                          data: [
                              ['v35',2.76],['v36',2.32],['v37',2.31],
                              ['v34',1.27],['v38',1.02],['v31',0.33],
                              ['v33',0.22],['v32',0.15]
                          ]
                      }]
                  }
              });
    }
  });
</script>