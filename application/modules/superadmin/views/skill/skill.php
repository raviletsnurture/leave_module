<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">
      <header class="panel-heading"> Skill </header>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <div class="alert alert-block alert-danger fade in" style="display:none">
             <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
             <p></p>
          </div>
          <div class="alert alert-success fade in" style="display:none">
             <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
             <p></p>
          </div>
        </div>
         <div class="form">
            <?php
            $form_attributes = array('name' => 'addEditPolicies', 'id' => 'addEditPolicies', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
                 echo form_open_multipart(base_url().'superadmin/skill',$form_attributes);
               ?>
               <div class="form-group">
                 <div class="col-lg-4">
                   <input maxlength="100" class="form-control" id="skill" name="skill" placeholder="Skill" type="text" required="required">
                 </div>
                  <div class="col-lg-2">
                     <button class="btn btn-danger form-control addSkillBtn" type="button" name="addSkillBtn">Add Skill</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
    </section>
    <section class="panel">
      <div class="panel-body bio-graph-info">
        <div class="row">
            <div class="col-lg-12">
              <table class="table table-striped table-hover table-bordered" id="example">
                <thead>
                <tr>
                    <th><i class="fa fa-bullhorn"></i> Skill</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($skills as $skill) { ?>
                    <tr>
                      <td><?php echo $skill->name;?></td>
                      <td width="30%">
                        <button type="button" onclick="delete_skill(<?php echo $skill->skill_id;?>)" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </button>
                      </td>
                    </tr>
                  <?php }?>
                </tbody>
            </table>
            </div>
        </div>
      </div>
    </section>
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
    $('#example').dataTable({
        "iDisplayLength": 25,    
        "bFilter": true,
        "bInfo": false,
        "bPaginate": true,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
    });

        $(".dataTables_filter input").addClass('form-control');
      } );
      </script>
        
<script>
$(".addSkillBtn").on("click",function(){
  var skill = $('#skill').val();
  $.ajax({
     url: '<?php echo base_url(); ?>superadmin/skill/addSkill',
     data: 'skill='+skill,
     dataType: 'html',
     type: 'POST',
     success: function(data){
      if(data == 1){
        $(".alert-success").show();
        $(".alert-danger").hide();
        $(".alert-success p").html("Skill added successfully.");
        location.reload();
      }else if(data == 2){
        $(".alert-success").hide();
        $(".alert-danger").show();
        $(".alert-danger p").html("Skill already added.");
      }else{
        $(".alert-success").hide();
        $(".alert-danger").show();
        $(".alert-danger p").html("Something went wrong. Please try again");
      }
    }
  });
});

function delete_skill(skill_id){
	var skill_id = skill_id;
  var r = confirm("Are you sure delete this skill. Because some user add skill experience");
	if (r == true){
  	$.ajax({
  	 url: '<?php echo base_url(); ?>superadmin/skill/deleteSkill',
  	 data: { skill_id: skill_id},
  	 dataType: 'html',
  	 type: 'POST',
  	 success: function(data){
       $(".alert-success").show();
       $(".alert-danger").hide();
       $(".alert-success p").html("Skill deleted successfully!");
       location.reload();
  	 }
  	});
  }
}
</script>
