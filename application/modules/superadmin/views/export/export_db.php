<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php echo 'Export - Import'; ?>
          </header>
           
           <!-- export database -->
          <div class="export">
            <?php if($this->session->flashdata('error')){?>
            <div class="alert alert-block alert-danger fade in">
              <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
              <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
            <?php } ?>
            <?php if($this->session->flashdata('success')){?>
            <div class="alert alert-success fade in">
              <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
              <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
            <?php }?>
            <div class="panel-body">
              <div class="form">
                <form method="post" action="<?php echo base_url(); ?>superadmin/export_mysql" >
                      <div class="form-group ">
                      <label for="country_id" class="control-label col-lg-2">Database Export</label>
                      <div class="col-lg-10">
                          <div>
                              <input type="submit" name="export_submit" value="Export"/>
                          </div>
                      </div>
                      </div>
                </form>
              </div>
            </div>
          </div>

           <!-- import database -->
          <div class="import">
              <?php if($this->session->flashdata('error')){?>
              <div class="alert alert-block alert-danger fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('success')){?>
              <div class="alert alert-success fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
              <?php }?>
              <div class="panel-body">
                <div class="form">
                  <form method="post" class="upload" id="upload" action="<?php echo base_url(); ?>superadmin/export_mysql/import" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="country_id" class="control-label col-lg-2">Database Import*</label>
                    <div class="col-lg-10">
                        <div>
                          <input type="file" name="database_import" id="database_import" > <br/>
                          <input type="submit" name="submit"  value="Submit" />
                        </div>
                    </div>
                  </div>
                  </form>
                </div>
              </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<script language="javascript">
  $(function() {
       $("#upload").validate({
           errorClass: "has-error",
           errorElement: 'span',
           rules: {
               database_import: {required: true, accept: "sql"}
           },
           messages: {
               database_import: "Please select a file."
           },
           submitHandler: function(form) {   
            $('#loaderAjax').show();         
            $(':input[type="submit"]').prop('disabled', true);
            form.submit();
          }
       });
   });   

 
</script>