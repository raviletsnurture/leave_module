<?php
$user = getAllUsers();
$module_logs = getAllModules();
?>

<section id="main-content">
  <section class="wrapper site-min-height">

    <!-- page start-->

    <section class="panel">
	  <header class="panel-heading">All User Logs </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">

          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>



          <select id="engines">
            <option value="">Select Users</option>
            <?php foreach($user as $row){?>
            <option value="<?php echo $row->first_name; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
            <?php } ?>
          </select>

		  <select id="engines2">
            <option value="">Module Name</option>
            <?php foreach($module_logs as $mo){?>
            <option value="<?php echo $mo->module_name; ?>"><?php echo $mo->module_name; ?></option>
            <?php } ?>
            <?php
            // TODO: Adding below option which is not a Module way insert but log listing reasons
            ?>
            <option value="EL">EL</option>
          </select>




          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th>Full Name</th>
				<th>IP</th>
				<th>Module</th>
				<th>Uger Agent</th>
				<th>Action Performed</th>
        <th>Reference Link</th>
				<th>Timestamp</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($all_logs as $row){ ?>
              <tr>
                <td><?php echo $row->first_name.' '.$row->last_name;?></td>
					<td><?php echo $row->ip; ?></td>
					<td><?php echo $row->module_name; ?></td>
					<td><?php echo $row->user_agent; ?></td>
					<td><?php echo $row->user_action; ?></td>
          <td><?php if($row->item_id != 0) {echo '<a href="leave/edit/'.$row->item_id.'">Link</a>';} else { echo "";} ?></td>
					<td><?php echo $row->timestamp; ?></td>
              </tr>
              <?php }?>
            </tbody>
          </table>


          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
    <!--SELECT sum(DATEDIFF(leave_end_date,leave_start_date) +1) as difference FROM `crm_leaves` WHERE YEAR(leave_start_date) = '2015' and MONTH(leave_end_date) = '03' and user_id = 2 -->


	</section>



    <!-- page end-->
  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">


$(document).ready(function() {
	var dt =  $('#example').dataTable( {
		  "aaSorting": [[ 6, "desc" ]],
		   "iDisplayLength": 10,
		   "pagingType": "full_numbers",
		   "dom": 'Cfrtip',
		   "destroy": true,
		   "bFilter": true,
		   "bPaginate": true,
		   "bInfo" : true,

		   "oSearch": { "bSmart": false, "bRegex": true },
		   "aoColumnDefs": [
			  {
				'bSortable': true, 'aTargets': [ 5 ]
			  }
		   ]
		} );


		$('select#engines').change( function() {
			dt.fnFilter( $(this).val(), 0 );
		} );
		$('select#engines2').change( function() {
			dt.fnFilter( $(this).val(), 2 );
		} );


	  $(".dataTables_filter input").addClass('form-control');
  } );
</script>
