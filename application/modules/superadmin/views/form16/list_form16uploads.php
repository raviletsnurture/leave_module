<?php
$user = getAllUsersWithInactive();
$department = getAllDepartment();
?>
<style type="text/css">
.dataTables_filter {
     display: none;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height">

    <section class="panel">
      <header class="panel-heading"> All Form 16 </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
          <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/form16/add">
                  <button class="btn btn-info" id="editable-sample_new"> Upload Form 16 <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
          <div class="col-sm-8">
              <?php
                $form_attributes = array('name' => 'getForm16Slip', 'id' => 'getForm16Slip', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
                echo form_open(base_url().'superadmin/form16/getForm16Slip',$form_attributes);
              ?>
              <div class="form-group">
                <label class="col-lg-4 control-label">Select Users</label>
                <div class="col-md-8">
                    <select id="selectUser" name="selectUser" class="form-control selectpicker js-example-basic-single" required="">
                      <option value="">Select Users</option>
                      <?php foreach($user as $row){?>
                      <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
                      <?php } ?>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Start Date</label>
                <div class="col-lg-2">
                  <input type="text" class="form-control" readonly="readonly" id="start_date" name="start_date" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-01'); ?>">
                </div>
                <label  class="col-lg-2 control-label">End Date</label>
                <div class="col-lg-2">
                  <input type="text" class="form-control" readonly="readonly" id="end_date" name="end_date" data-date-format="yyyy-mm-dd" placeholder="" value="<?php echo date('Y-m-d'); ?>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-4 puul-right">
                  <button class="btn btn-success" type="submit" name="exportLateEntry" id="exportLateEntry">
                    Exprort Form 16
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>




          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th><select id="engines">
                  <option value="">Select Users</option>
                  <?php foreach($user as $row){?>
                  <option value="<?php echo $row->first_name; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
                  <?php } ?>
                </select>
              </th>
      				<th><select id="engines1">
                    <option value="">Select Department</option>
                    <?php foreach($department as $row1){?>
                    <option value="<?php echo $row1->department_name; ?>"><?php echo $row1->department_name;?></option>
                    <?php } ?>
                  </select>
              </th>
      				<th>Year</th>
      				<th>FileName</th>
      				<th>Uploaded At</th>
              <th>Last Downloaded</th>
              <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($form16 as $row){ ?>
              <tr>
                <td><?php echo $row->first_name.' '.$row->last_name;?></td>
				<td><?php echo $row->department_name;?></td>
				<td><?php echo $row->year; ?></td>
        <td><?php echo $row->filename;?></td>
				<td><?php echo $row->uploaded_at;?></td>
        <td><?php echo $row->last_accessed;?></td>
				<td>
          <!-- a href="<?php //echo base_url()?>superadmin/form16/edit/<?php //echo //$row->upload_id;?>" >
          <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;" title=""><i class="fa fa-pencil"></i></button>
        </a -->

          <a href="<?php echo base_url()?>superadmin/form16/delete/<?php echo $row->upload_id;?>" class="deleteRec">
          <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;"><i class="fa fa-trash-o "></i></button>
          </a>
          <a target="_blank" value="<?php echo $row->upload_id;?>" href="<?php echo base_url()?>uploads/form16_slips/<?php echo $row->year.'/'.$row->filename;?>" >
          <button class="btn btn-primary btn-xs tooltips" data-original-title="Download&nbsp;" title=""><i class="fa fa-eye"></i></button>
          </a>
        </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
    </section>
    <!-- page end-->

  </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript">
$('#start_date,#end_date').datepicker({
  endDate: "1d",
  autoclose: true,
});
</script>
<script type="text/javascript" charset="utf-8">

          $(document).ready(function() {
             var dt =  $('#example').dataTable( {
            "aaSorting": [[ 5, "desc" ]],
				   "iDisplayLength": 20,
				   "pagingType": "full_numbers",
				   "dom": 'Cfrtip',
				   "destroy": true,
				   //"bFilter": false,
				   "bPaginate": true,
				   "bInfo" : false,

				   "oSearch": { "bSmart": false, "bRegex": true },
				   "aoColumnDefs": [
					  {
						'bSortable': true, 'aTargets': [ 5 ]
					  }
				   ]
              } );

			    $('select#engines').change( function() {
					dt.fnFilter( $(this).val(), 0 );
				} );
				$('select#engines1').change( function() {
					dt.fnFilter( $(this).val(), 1 );
				} );

			  $(".dataTables_filter input").addClass('form-control');
		  } );
</script>
