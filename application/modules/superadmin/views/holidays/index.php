<section id="main-content">
    <section class="wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/holidays/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
           <div class="col-lg-12">
              <?php
              if($this->session->flashdata('error')){?>
              <div class="alert alert-block alert-danger fade in">
                 <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                 <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('success')){?>
              <div class="alert alert-success fade in">
                 <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                 <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
              </div>
              <?php }?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                      Fixed Holidays
                    </header>
                    <table class="table">
                        <thead>
                        <tr>
														<th>No</th>
                            <th>Name of the Festival</th>
                            <th>Date</th>
                            <th>Day</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
													<?php $i=1; foreach ($fixedHolidays as $fixedHoliday) { ?>
														<tr>
															<td><?php echo $i;?></td>
                              <td><?php echo $fixedHoliday['festival_name'];?></td>
                             	<td><?php echo date("dS, F Y",strtotime($fixedHoliday['festival_date']));?></td>
															<td><?php echo date("l",strtotime($fixedHoliday['festival_date']));?></td>
                              <td>
                                <a href="<?php echo base_url()?>superadmin/holidays/edit/<?php echo $fixedHoliday['id'];?>" >
                                <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Holiday" title=""><i class="fa fa-pencil"></i></button>
                                </a>
                                <a href="<?php echo base_url()?>superadmin/holidays/delete/<?php echo $fixedHoliday['id'];?>" class="deleteRec">
                                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Holiday"><i class="fa fa-trash-o "></i></button>
                                </a>
                              </td>
                            </tr>
													<?php $i++; }?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
				<div class="row">
					<div class="col-sm-12 col-lg-6">
						<section class="panel">
							<header class="panel-heading">
								Flexible festivals
							</header>
							<table class="table table-striped">
								<thead>
									<tr>
											<th>No</th>
											<th>Name of the Festival</th>
											<th>Date</th>
											<th>Day</th>
                      <th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php $i=1;foreach ($flexibleHolidays as $flexibleHoliday) { ?>
											<tr>
												<td><?php echo $i;?></td>
												<td><?php echo $flexibleHoliday['festival_name'];?></td>
												<td><?php echo date("dS, F Y",strtotime($flexibleHoliday['festival_date']));?></td>
												<td><?php echo date("l",strtotime($flexibleHoliday['festival_date']));?></td>
                        <td>
                          <a href="<?php echo base_url()?>superadmin/holidays/edit/<?php echo $flexibleHoliday['id'];?>" >
                          <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Holiday" title=""><i class="fa fa-pencil"></i></button>
                          </a>
                          <a href="<?php echo base_url()?>superadmin/holidays/delete/<?php echo $flexibleHoliday['id'];?>" class="deleteRec">
                          <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Holiday"><i class="fa fa-trash-o "></i></button>
                          </a>
                        </td>
											</tr>
										<?php $i++; }?>
									</tr>
								</tbody>
							</table>
						</section>
					</div>
					<div class="col-sm-12 col-lg-6">
						<section class="panel">
							<header class="panel-heading">
								Can be swapped with
							</header>
							<table class="table table-striped">
								<thead>
									<tr>
											<th>No</th>
											<th>Name of the Festival</th>
											<th>Date</th>
											<th>Day</th>
                      <th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php $i=1; foreach ($swappedHolidays as $swappedHoliday) { ?>
											<tr>
												<td><?php echo $i;?></td>
												<td><?php echo $swappedHoliday['festival_name'];?></td>
												<td><?php echo date("dS, F Y",strtotime($swappedHoliday['festival_date']));?></td>
												<td><?php echo date("l",strtotime($swappedHoliday['festival_date']));?></td>
                        <td>
                          <a href="<?php echo base_url()?>superadmin/holidays/edit/<?php echo $swappedHoliday['id'];?>" >
                          <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Holiday" title=""><i class="fa fa-pencil"></i></button>
                          </a>
                          <a href="<?php echo base_url()?>superadmin/holidays/delete/<?php echo $swappedHoliday['id'];?>" class="deleteRec">
                          <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Holiday"><i class="fa fa-trash-o "></i></button>
                          </a>
                        </td>
											</tr>
										<?php $i++; }?>
									</tr>
								</tbody>
							</table>
						</section>
					</div>
				</div>
    </section>
</section>
