<section id="main-content">
   <section class="wrapper">
      <div class="row">
         <div class="col-lg-12">
            <section class="panel">
               <header class="panel-heading">
                  <?php if(isset($holidaysEdit[0]->id)){ echo "Edit Holidays";} else{echo "Add Holidays";}?>
               </header>
               <?php
               if($this->session->flashdata('error')){?>
               <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
               </div>
               <?php } ?>
               <?php if($this->session->flashdata('success')){?>
               <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
               </div>
               <?php }?>
               <div class="panel-body">
                  <div class="form">
                     <?php
                        $form_attributes = array('name' => 'addEditHoliday', 'id' => 'addEditHoliday', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
                        if(isset($holidaysEdit[0]->id)){
                        	echo form_open('superadmin/holidays/add/'.$holidaysEdit[0]->id,$form_attributes);
                        }else{
                        echo form_open(base_url().'superadmin/holidays/add',$form_attributes);
                        }
                        ?>
                        <div class="form-group">
        									<label for="date" class="control-label col-lg-2">Date<span class="red">*</span></label>
        									<div class="col-lg-10">
        										<div class="input-group date form_datetime-component">
                              <input type="text" name="holidayDate" id="holidayDate" class="form-control" value="<?php if(isset($holidaysEdit[0]->festival_date)){ echo date("Y-m-d",strtotime($holidaysEdit[0]->festival_date));} ?>" required>
        										</div>
        									</div>
        								</div>
                     <div class="form-group ">
                        <label for="firstName" class="control-label col-lg-2">Holidays Name <span class="red">*</span></label>
                        <div class="col-lg-10">
                           <input class="form-control" id="holidayName" name="holidayName" type="text" maxlength="50"   value="<?php if(isset($holidaysEdit[0]->festival_name)){ echo $holidaysEdit[0]->festival_name;} ?>"/>
                        </div>
                     </div>
                     <div class="form-group ">
                        <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                        <div class="col-lg-10">
                           <select name="holidaystatus" id="holidaystatus" class="form-control m-bot15">
                              <option value="0" <?php echo isset($holidaysEdit[0]->status)? selectedVal($holidaysEdit[0]->status,"0"): '';?>>Fixed</option>
                              <option value="1" <?php echo isset($holidaysEdit[0]->status)? selectedVal($holidaysEdit[0]->status,"1"): '';?> >Flexible</option>
                              <option value="2" <?php echo isset($holidaysEdit[0]->status)? selectedVal($holidaysEdit[0]->status,"2"): '';?> >Swapped</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                           <button class="btn btn-danger" type="submit" name="addEditHoliday">
                           <?php if(isset($holidaysEdit[0]->id)){ echo "Update";} else{echo "Submit";}?>
                           </button>
                           <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                        </div>
                     </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>

<script language="javascript" type="text/javascript">
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('#holidayDate').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});
</script>
