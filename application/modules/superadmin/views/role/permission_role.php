<style>
.checkbox{float:left !important; margin-top:0px !important;}
input[type="checkbox"]:not(:checked),
input[type="checkbox"]:checked {
  position: absolute;
  left: -9999px;
}
input[type="checkbox"]:not(:checked) + label,
input[type="checkbox"]:checked + label {
  position: relative;
  padding-left: 25px;
  cursor: pointer;
}

/* checkbox aspect */
input[type="checkbox"]:not(:checked) + label:before,
input[type="checkbox"]:checked + label:before {
  content: '';
  position: absolute;
  left:0; top: 2px;
  width: 17px; height: 17px;
  border: 1px solid #aaa;
  background: #f8f8f8;
  border-radius: 3px;
  box-shadow: inset 0 1px 3px rgba(0,0,0,.3)
}
/* checked mark aspect */
input[type="checkbox"]:not(:checked) + label:after,
input[type="checkbox"]:checked + label:after {
  content: '✔';
  position: absolute;
  top: 0; left: 4px;
  font-size: 14px;
  color: #4cc0c1;
  transition: all .2s;
  -webkit-transition: all .2s;
  -moz-transition: all .2s;
  -ms-transition: all .2s;
  -o-transition: all .2s;
}
/* checked mark aspect changes */
input[type="checkbox"]:not(:checked) + label:after {
  opacity: 0;
  transform: scale(0);
}
input[type="checkbox"]:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
/* disabled checkbox */
input[type="checkbox"]:disabled:not(:checked) + label:before,
input[type="checkbox"]:disabled:checked + label:before {
  box-shadow: none;
  border-color: #999999;
  background-color: #ddd;
}
input[type="checkbox"]:disabled:checked + label:after {
  color: #999;
}
input[type="checkbox"]:disabled + label {
  color: #aaa;
}
/* accessibility */
input[type="checkbox"]:checked:focus + label:before,
input[type="checkbox"]:not(:checked):focus + label:before {
  border: inherit;
}
</style>

<section id="main-content">
  <section class="wrapper site-min-height">
     <!--<div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row mbot30">
          <div class="col-lg-2">
              <div class="btn-group">
                <span class="my-span-head">Category</span>
              </div>
          </div>
          <div class="col-lg-2 pull-right">
              <div class="btn-group pull-right">
                <a href="<?php echo base_url()?>superadmin/category/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
          </div>
        </div>
      </div>-->
    <!-- page start-->
    <?php //$this->load->view('list_header'); ?>
    <section class="panel">
      <header class="panel-heading"> User Role Permission </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <!--<a href="<?php echo base_url()?>superadmin/role/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>-->
              </div>
            </div>
          </div>
        </div>
        <!--<div class="row">
          <div class="col-lg-5">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="">
                <div class="form-group ">
                  <div>
                    <select name="perpage" class="form-control perPage" onchange="changePerPage(this.value);">
                       <option value="25" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '25'){echo "selected";} ?>>25 per page</option>
                      <option value="50" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '50'){echo "selected";} ?>>50 per page</option>
                      <option value="100" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '100'){echo "selected";} ?>>100 per page</option>
                    </select>

                  </div>
              </div>
              </div>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="dataTables_filter col-lg-7" id="editable-sample_filter">
              <form method="get" class="pull-right">
                  <div class="form-group">
                      <label for="exampleInputPassword2" class="sr-only">Password</label>
                      <input type="text" placeholder="Search" name="keyword" class="form-control">
                  </div>
                  <input type="submit" value="Search" class="btn-cust btn btn-success" >
              </form>
            </div>
          </div>
        </div>-->
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">

          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
			<?php //echo $role_id; ?>

		  <?php
			  $form_attributes = array('name' => 'addPermission', 'id' => 'addPermission', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );

			  echo form_open('superadmin/role/addPermission/'.$role_id,$form_attributes);

			?>
          <table class="table table-striped table-hover table-bordered">
            <thead>
              <tr>
                <th>Module Names</th>
                <th>Permissions</th>
              </tr>
            </thead>
            <tbody>

			  <?php for($i=0;$i<sizeof($permission);$i++) {?>
          <tr>
                <td><?php echo ucfirst($permission[$i]->module_name); ?></td>
                <td>
					<div class="checkbox ">
					  <input type="checkbox"  <?php echo isset($permission[$i]->module_add)? selectedChk($permission[$i]->module_add,'yes'): '';?> name="<?php echo $permission[$i]->module_name; ?>[add]" id="<?php echo $permission[$i]->module_name; ?>_module_add" value="yes" /><label for="<?php echo $permission[$i]->module_name; ?>_module_add">Add</label>
					</div>
					<div class="checkbox ">
					  <input type="checkbox" <?php echo isset($permission[$i]->module_edit)? selectedChk($permission[$i]->module_edit,'yes'): '';?> name="<?php echo $permission[$i]->module_name; ?>[edit]" id="<?php echo $permission[$i]->module_name; ?>_module_edit" value="yes"  /><label for="<?php echo $permission[$i]->module_name; ?>_module_edit">Edit/Update</label>
					</div>
					<div class="checkbox ">
					  <input type="checkbox" <?php echo isset($permission[$i]->module_delete)? selectedChk($permission[$i]->module_delete,'yes'): '';?> name="<?php echo $permission[$i]->module_name; ?>[delete]" id="<?php echo $permission[$i]->module_name; ?>_module_delete" value="yes" /><label for="<?php echo $permission[$i]->module_name; ?>_module_delete">Delete</label>
					</div>
					<div class="checkbox">
					  <input type="checkbox" <?php echo isset($permission[$i]->module_view)? selectedChk($permission[$i]->module_view,'yes'): '';?> name="<?php echo $permission[$i]->module_name; ?>[view]" id="<?php echo $permission[$i]->module_name; ?>_module_view" value="yes" /><label for="<?php echo $permission[$i]->module_name; ?>_module_view">View</label>
					</div>
          <?php if($permission[$i]->module_name == "leaves"){ ?>
          <div class="checkbox">
					  <input type="checkbox" <?php echo isset($permission[$i]->module_approve)? selectedChk($permission[$i]->module_approve,'yes'): '';?> name="<?php echo $permission[$i]->module_name; ?>[approve]" id="<?php echo $permission[$i]->module_name; ?>_module_approve" value="yes" /><label for="<?php echo $permission[$i]->module_name; ?>_module_approve">Approve</label>
					</div>
          <?php } ?>

          <?php //if($permission[$i]->module_name == "feedback"){ ?>
          <!-- <div class="checkbox">
            <input type="checkbox" <?php //echo isset($permission[$i]->module_kra)? selectedChk($permission[$i]->module_kra,'yes'): '';?> name="<?php echo $permission[$i]->module_name; ?>[kra]" id="<?php echo $permission[$i]->module_name; ?>_module_kra" value="yes" /><label for="<?php echo $permission[$i]->module_name; ?>_module_kra">KRA</label>
          </div> -->
          <?php //} ?>

                </td>
          </tr>
			  <?php } ?>
			  <!--<tr>
                <td>Users</td>
                <td>
					<div class="checkbox ">
					  <input type="checkbox" name="users[add]" id="users_module_add" value="yes" /><label for="users_module_add">Add</label>
					</div>
					<div class="checkbox ">
					  <input type="checkbox" name="users[edit]" id="users_module_edit" value="yes" /><label for="users_module_edit">Edit/Update</label>
					</div>
					<div class="checkbox">
					  <input type="checkbox" name="users[delete]" id="users_module_delete" value="yes" /><label for="users_module_delete">Delete</label>
					</div>
					<div class="checkbox">
					  <input type="checkbox" name="users[view]" id="users_module_view" value="yes" /><label for="users_module_view">View</label>
					</div>



                </td>
              </tr>-->

            </tbody>
          </table>
		  <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditPermission">
                  <?php if(isset($roleEdit[0]->role_id)){ echo "Submit";} else{echo "Update";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
          </form>
          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
    </section>
    <!-- page end-->
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">

      </script>
