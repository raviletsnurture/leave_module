<?php
	$user = getAllUsers();

	$userId = $this->session->userdata('sLogin_session');
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Give Permission
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
					<form class="commonForm  cmxform form-horizontal tasi-form" id="givePermission" name="givePermission"  method="post" action="">
					<div class="col-lg-12">
								<div class="form-group">
									<label for="firstName" class="control-label col-lg-2">Team Member <span class="red">*</span></label>
									<div class="col-lg-10">
										<select name="user_id" id="user_id"  class="form-control selectpicker js-example-basic-single" required>
											<option value="">Select Member</option>
											<?php foreach($user as $dd) { ?>
												<option value="<?php echo $dd->user_id; ?>" <?php $value1 = (isset($userId[0]->sId)) ? $userId[0]->sId: 0; echo selectedVal($value1,$dd->user_id); ?>>
													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
												</option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="permission_type" class="control-label col-lg-2">Permission Type<span class="red">*</span></label>
									<div class="col-lg-10" >
										<select name="permission_type" id="permission_type"  class="form-control selectpicker" required>
											<option value="feedback">Feedback</option>
											<option value="leave">Leave</option>
											<option value="reimbursement">Reimbursement / User Reward</option>
										</select>
									</div>
								</div>

								<div class="form-group allcheckboxList">

								</div>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-danger" type="submit" name="givePermissionBtn" id="givePermissionBtn">Submit Permission</button>
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
									</div>
									<div class="col-lg-offset-5 col-lg-7">
										<div class="loader">
											<img src="<?php echo base_url()?>assets/img/input-spinner.gif" alt="loading...">
										</div>
									</div>
								</div>
								<div class="form-group">
		   						   <div class="col-lg-offset-2 col-lg-10">
		   							   <span class="frmmessage"></span>
		   						   </div>
		   						</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>

<script type="text/javascript">
$(document).ready(function(){
	$("#givePermission").validate({
	   ignore: [],
	   rules: {
	   },
	   messages: {
	   },
	  errorElement: "div",
	  wrapper: "div",  // a wrapper around the error message
	  errorPlacement: function(error, element) {
		  offset = element.offset();
		  error.insertAfter(element)
		  error.addClass('errormessage');  // add a class to the wrapper
		  error.css('position', 'relative');
		  //error.css('left', offset.left + element.outerWidth());
		  //error.css('top', offset.top);
	  },
	   submitHandler: function(form) {
		   var formData = $("#givePermission").serialize();
		   $.ajax({
			   type: 'POST',
			   url: '<?php echo base_url()?>superadmin/role/givePermission',
			   data: formData,
			   async:false,
			   processData: false,
			   beforeSend: function(){
				   $('.loader').show();
				   $('#givePermissionBtn').hide();
			   },
			   success:function(data){
				  $('.loader').hide();
				  //$('#givePermissionBtn').show();
				   if(data == 1){
						$('#givePermission')[0].reset();
						$(".frmmessage").html("Permission Given successfully!");
						$(".frmmessage").css("color","#29B6F6");
						setTimeout(function(){
						 location.reload();
					 },1000);
				  }else {
						$('#givePermission')[0].reset();
 					  $(".frmmessage").html("Permission Updated successfully!");
 					  $(".frmmessage").css("color","#29B6F6");
				  }
					return false;
			   }
		});
	}
  });
});//document ready
function checkAllDeptUsers(id){
		if($('#department_id_'+id+':checked').length > 0){
				$('#department_id_'+id).prop('checked', true);
				$('.users_'+id).prop('checked', true);
		} else {
			  $('#department_id_'+id).prop('checked', false);
			  $('.users_'+id).prop('checked', false);
				$('#check_all').prop('checked', false);
		}
		if($('.department_id:checkbox:not(:checked)').length == 0){
				$('#check_all').prop('checked', true);
		} else {
				$('#check_all').prop('checked', false);
		}
}

  function checkUser(id){
       if($('.users_'+id+':checkbox:not(:checked)').length == 0){
           $('#department_id_'+id).prop('checked', true);
					 $('#check_all').prop('checked', false);
       } else {
           $('#department_id_'+id).prop('checked', false);
           $('#check_all').prop('checked', false);
       }
			 if($('.department_id:checkbox:not(:checked)').length == 0){
	 				$('#check_all').prop('checked', true);
	 		} else {
	 				$('#check_all').prop('checked', false);
	 		}
   }
//$("input:checkbox:not(:checked)")
   function checkAll(){
       if($('#check_all:checked').length > 0){
           $('.users').prop('checked', true);
           $('.department_id').prop('checked', true);
           $('.allAction').prop('checked', true);
       } else {
           $('.users').prop('checked', false);
           $('.department_id').prop('checked', false);
           $('.allAction').prop('checked', false);
       }
   }

	 $('#user_id, #permission_type').on('change', function() {
		 	var id= $('#user_id').val();			 
			var permission_type = $('#permission_type').val();	
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>superadmin/role/userPermissions',
				data: ({ user_id :id, permission_type :permission_type}),
				success:function(data){
				 if(data != 0){
						$(".allcheckboxList").html(data)
				 }else{
					 $(".allcheckboxList").html('')
				 }
				 return false;
				}
	 });
	});

</script>
