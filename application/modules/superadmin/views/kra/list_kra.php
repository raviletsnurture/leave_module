<link href="<?php  echo base_url();?>assets/css/bootstrap-select.css" rel="stylesheet" >
<script src="<?php  echo base_url();?>assets/js/bootstrap-select.js"></script>

<style>
button.btn.dropdown-toggle.bs-placeholder.btn-default,
button.btn.dropdown-toggle.btn-default {
    background: #fff !important;
    color: #999 !important;
    width: 100%;
}
.add_new_field .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){
  width:100%;
}
</style>

<section id="main-content">
  <section class="wrapper site-min-height">

    <!-- page start-->

    <section class="panel">
      <header class="panel-heading"> All Roles </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <button class="btn btn-info" id="editable-sample_new"> Add New KRA <i class="fa fa-plus"></i> </button>
              </div>
            </div>
          </div>
        </div>

        <script>
          function enable_sub_role(){
            var selected_roles = $.map($('#add_role_members').find("option:selected"), function(o) { return o["value"]; });
            var selected_department = $.map($('#add_dep_member').find("option:selected"), function(o) { return o["value"]; });

            if((jQuery.inArray('20', selected_roles) !== -1) && (jQuery.inArray('11', selected_department) !== -1)){
                $('#add_sub_role').removeAttr('disabled');
                $('#add_sub_role').selectpicker('refresh');
            }else {
              $('#add_sub_role').selectpicker('val', '');
              $('#add_sub_role').attr('disabled',true);
              $('#add_sub_role').selectpicker('refresh');
            }

          }
        </script>

        <!-- Toggle add new field -->
        <form method="POST" action="<?php echo base_url(); ?>superadmin/kra/add" >

        <div class="add_new_field" style="padding:15px;display:none;">
        <div class="" style="border:1px solid #CCC;border-radius:3px;">

        <div class="row" style="padding:10px 0px;">
          <div class="col-lg-12">
            <div class="col-lg-2">
              <lable style="vertical-align:middle;line-height:35px;font-weight:bold;">Feedback key :</label>
            </div>
            <div class="col-lg-6">
              <input type="text" name="feedback_key" value="" class="form-control">
            </div>
        </div>
        </div>

        <div class="row" style="padding:10px 0px;">
          <div class="col-lg-12">
            <div class="col-lg-2">
              <lable style="vertical-align:middle;line-height:35px;font-weight:bold;">Role :</label>
            </div>
            <div class="col-lg-6">
              <select name="role_members[]" required class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" id="add_role_members" onchange="enable_sub_role()">
                <option value="0" selected="selected">ALL</option>
                <?php foreach($role as $role_data){?>
                <option value="<?php echo $role_data->role_id; ?>" >
                <?php echo $role_data->role_name; ?>
                </option>
                <?php } ?>
              </select>
            </div>
        </div>
        </div>

        <div class="row" style="padding:10px 0px;">
          <div class="col-lg-12">
            <div class="col-lg-2">
              <lable style="vertical-align:middle;line-height:35px;font-weight:bold;">Department :</label>
            </div>
            <div class="col-lg-6">
              <select name="department_members[]" required class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" id="add_dep_member" onchange="enable_sub_role()">
                <option value="0" selected="selected">ALL</option>
                <?php foreach($departments as $department_data){?>
                <option value="<?php echo $department_data->department_id; ?>" >
                <?php echo $department_data->department_name; ?>
                </option>
                <?php } ?>
              </select>
            </div>
        </div>
        </div>

        <div class="row" style="padding:10px 0px;">
          <div class="col-lg-12">
            <div class="col-lg-2">
              <lable style="vertical-align:middle;line-height:35px;font-weight:bold;">Sub Role :</label>
            </div>
            <div class="col-lg-6">
              <select name="sub_role[]" required class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" disabled="disabled" id="add_sub_role">
                <option value="0">ALL</option>
                <?php foreach($sub_role as $sub_role_data){?>
                <option value="<?php echo $sub_role_data->sub_role_id; ?>" >
                <?php echo $sub_role_data->role_name; ?>
                </option>
                <?php } ?>
              </select>
            </div>
        </div>
        </div>

        <div class="row" style="padding:10px 0px;">
          <div class="col-lg-12">
            <div class="col-lg-2">
              <lable style="vertical-align:middle;line-height:35px;font-weight:bold;">Helptext :</label>
            </div>
            <div class="col-lg-6">
              <input type="text" name="helptext" value="" class="form-control">
            </div>
        </div>
        </div>

        <div class="row" style="padding:10px 0px;">
          <div class="col-lg-12">
            <div class="col-lg-2">

            </div>
            <div class="col-lg-6">
              <input type="submit" value="Add" class="btn btn-primary" />
              <input type="submit" value="Clear" class="btn btn-danger" />
            </div>

        </div>
        </div>


        </div>
      </div>
    </form>
        <!-- Toogle end-->

      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">

          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

          <table class="table table-striped table-hover table-bordered table-responsive" id="example">
            <thead>
              <tr>
                <th style="width:5%;">No</th>
                <th style="width:25%;">Feedback Key</th>
                <th style="width:10%">Role</th>
                <th style="width:10%;">Department</th>
                <th style="width:10%;">Sub role</th>
                <th style="width:25%;">Helptext</th>
                <th style="width:15%;">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $count =1;?>
              <?php foreach($kra as $row){ ?>
              <tr id="<?php echo $row->id; ?>">
                <td><?php echo $count; $count++; ?></td>
                <td><input type="text" name="feedback_key" class="form-control" value="<?php echo $row->feedback_key; ?>" id="feedback_key"></td>
                <td>
                  <?php $roles = explode(",",$row->role_id); ?>

                  <select name="role_members[]" required class="selectpicker role_select" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" id="role_id">
                    <option value="0" <?php if($roles[0] == 0){ echo "selected"; } ?> >ALL</option>
                    <?php foreach($role as $role_data){?>
                    <option value="<?php echo $role_data->role_id; ?>"
                      <?php foreach($roles as $role_id){
                        if($role_id == $role_data->role_id){
                          echo "selected";
                        }
                      }?>
                    ><?php echo $role_data->role_name; ?></option>
                    <?php } ?>
                  </select>

                </td>
                <td><?php $department = explode(",",$row->dept_id); ?>
                  <select name="department_members[]" required class="selectpicker deg_select" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" id="department_id">
                    <option value="0" <?php if($department[0] == 0){ echo "selected"; } ?> >ALL</option>
                    <?php foreach($departments as $department_data){?>
                    <option value="<?php echo $department_data->department_id; ?>"
                      <?php foreach($department as $department_id){
                        if($department_id == $department_data->department_id){
                          echo "selected";
                        }
                      }?>
                    ><?php echo $department_data->department_name; ?></option>
                    <?php } ?>
                  </select>

                </td>
                <td>
                  <?php $selected_sub_roles = explode(",",$row->sub_role_id); ?>
                  <select name="sub_role[]" required class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" id="sub_role">
                    <option value="0" <?php if($selected_sub_roles[0] == 0){ echo "selected"; } ?> >ALL</option>
                    <?php foreach($sub_role as $sub_role_data){?>
                    <option value="<?php echo $sub_role_data->sub_role_id; ?>" <?php if (in_array($sub_role_data->sub_role_id,$selected_sub_roles)){ ?> selected="selected" <?php } ?>>
                      <?php echo $sub_role_data->role_name; ?>
                    </option>
                    <?php } ?>
                  </select>
                </td>
                <td><input type="text" name="helptext" class="form-control" value="<?php echo $row->helptext; ?>" id="helptext"></td>
                <td>
                  <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit" title="" id="enable_elements"><i class="fa fa-pencil"></i></button>

                  <button class="btn btn-info btn-xs tooltips btn_update" data-toggle="tooltip" data-original-title="Save" id="<?php echo $row->id; ?>"><i class="fa fa-check-circle"></i></button>
                  <button class="btn btn-danger btn-xs tooltips btn_delete" data-toggle="tooltip" data-original-title="Delete" id="<?php echo $row->id; ?>"><i class="fa fa-trash-o "></i></button>
                  </a>
                </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
    </section>
    <!-- page end-->
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">

    $(document).ready(function() {

    $('#example #feedback_key').attr('disabled','disabled');
    $('#example #role_id').attr('disabled','disabled');
    $('#example #department_id').attr('disabled','disabled');
    $('#example #helptext').attr('disabled','disabled');
    $('#example #sub_role').attr('disabled','disabled');

    $('.selectpicker').on('change', function(){
      //var row_id = $(this).closest('tr').attr('id');
      var row = $(this).closest('tr');

      var first_select = $(row).find('.role_select');
      var second_select = $(row).find('.deg_select');

      var selected_roles = $.map($(first_select).find("option:selected"), function(o) { return o["value"]; });
      var selected_department = $.map($(second_select).find("option:selected"), function(o) { return o["value"]; });
      //alert(selected_roles);
      //alert(selected_department);
          if((jQuery.inArray('20', selected_roles) !== -1) && (jQuery.inArray('11', selected_department) !== -1)){
              $(row).find('#sub_role').removeAttr('disabled');
              $(row).find('#sub_role').selectpicker('refresh');
          }else {
            $(row).find('#sub_role').selectpicker('val', '');
            $(row).find('#sub_role').attr('disabled',true);
            $(row).find('#sub_role').selectpicker('refresh');
          }

     });


    //Update code
    $('#example .btn_update').hide();


    $(document).on('click','#enable_elements',function(){

      var row = $(this).closest('tr');

      $(row).find('.btn_update').show();
      $(row).find('#enable_elements').hide();

      //$('#role_id').selectpicker('refresh');
      $(row).find('.role_select').removeAttr('disabled');
      $(row).find('#department_id').removeAttr('disabled');
      $(row).find('.role_select').selectpicker('refresh');
      $(row).find('#department_id').selectpicker('refresh');
      $(row).find('#feedback_key,#department_id,#helptext').prop("disabled",false);

      var first_select = $(row).find('.role_select');
      var second_select = $(row).find('.deg_select');
      var selected_roles = $.map($(first_select).find("option:selected"), function(o) { return o["value"]; });
      var selected_department = $.map($(second_select).find("option:selected"), function(o) { return o["value"]; });

          if((jQuery.inArray('20', selected_roles) !== -1) && (jQuery.inArray('11', selected_department) !== -1)){
              $(row).find('#sub_role').removeAttr('disabled');
              $(row).find('#sub_role').selectpicker('refresh');
          }else {
            $(row).find('#sub_role').attr('disabled',true);
            $(row).find('#sub_role').selectpicker('refresh');
          }
    });

    $('.btn_update').click(function(){

      var row = $(this).closest('tr');
      var id = this.id;
      var feedback_key = $(row).find('#feedback_key').val();

      var role_id = [];
      $(row).find('.role_select :selected').each(function(i, selected){
      role_id[i] = $(selected).val();
      });

      var department_id = [];
      $(row).find('#department_id :selected').each(function(i, selected){
      department_id[i] = $(selected).val();
      });

      var sub_role = [];
      $(row).find('#sub_role :selected').each(function(i, selected){
        sub_role[i] = $(selected).val();
      });

      //var role_id = $(row).find('.role_select option:selected').val();
      var helptext = $(row).find('#helptext').val();


      $.ajax({
			 url: '<?php echo base_url(); ?>superadmin/kra/update',
			 data: { id:id,feedback_key : feedback_key, role_id : role_id, department_id: department_id, sub_role:sub_role, helptext:helptext},
			 type: 'POST',
			 success: function(data){
         console.log(data);
          if(data == 1){
            alert("KRA updated successfully");
          }else{
            alert("Oops! Error occured.");
          }

          //location.reload(1);
			 }
			});
    });

    //Delete KRA
    $('.btn_delete').click(function(){
      var id = this.id;

      $.ajax({
			 url: '<?php echo base_url(); ?>superadmin/kra/delete',
			 data: {id:id},
			 type: 'POST',
			 success: function(data){
         console.log(data);
          if(data == 1){
            alert("KRA deleted successfully");
          }else{
            alert("Oops! Error occured.");
          }
        location.reload(1);
			 }
			});
    });


    // $(document).on("click",".editIcon",function(){
    //       var editID = $(this).attr("id");;
    //       var saveID = $(this).attr("id");
    //       var editfinalValue = editID.split("_");
    //       var savefinalValue = saveID.split("_");
    //       if(editfinalValue[1] == savefinalValue[1]){
    //             $("#editIcon_"+editfinalValue[1]).css("display","none");
    //             $("#saveIcon_"+savefinalValue[1]).css("display","block");
    //       }
    // });
    // $(document).on("click",".saveIcon",function(){
    //       var editID = $(this).attr("id");;
    //       var saveID = $(this).attr("id");
    //       var editfinalValue = editID.split("_");
    //       var savefinalValue = saveID.split("_");
    //       if(editfinalValue[1] == savefinalValue[1]){
    //             $("#editIcon_"+editfinalValue[1]).css("display","block");
    //             $("#saveIcon_"+savefinalValue[1]).css("display","none");
    //       }
    // });
    $('#example').dataTable({
        "iDisplayLength": 25,    
        
        "bInfo": false,
        "bPaginate": true,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [1,2,3,4,5] }
        ],
    });

		$(".dataTables_filter input").addClass('form-control');

    $("#editable-sample_new").click(function(){
      $(".add_new_field").fadeToggle(300);
    });
  });


</script>
<style type="text/css">
.dataTables_filter {
  width: 30%;    
}
.span6{
  width: 50%;
  float: left;
}

</style>
