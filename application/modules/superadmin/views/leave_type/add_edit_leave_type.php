<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($leave_typeEdit[0]->leave_type_id)){ echo "Edit Leave Type";} else{echo "Add Leave Type";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addLeave_Type', 'id' => 'addLeave_Type', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($leave_typeEdit[0]->leave_type_id)){
			  	echo form_open('superadmin/leave_type/add/'.$leave_typeEdit[0]->leave_type_id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/leave_type/add',$form_attributes);
			  }
			  ?>
              <div class="form-group ">
                <label for="firstName" class="control-label col-lg-2">Request Type<span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="leave_type" name="leave_type" type="text"  value="<?php if(isset($leave_typeEdit[0]->leave_type)){ echo $leave_typeEdit[0]->leave_type;} ?>"/>
                </div>
              </div>
			  <div class="form-group ">
                <label for="firstName" class="control-label col-lg-2">Numbmer Of Request</label>
                <div class="col-lg-10">
                  <input class="form-control" id="leave_amount" name="leave_amount" type="text"  value="<?php if(isset($leave_typeEdit[0]->leave_amount)){ echo $leave_typeEdit[0]->leave_amount;} ?>"/>
                </div>
              </div>
               <div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($leave_typeEdit[0]->status)? selectedVal($leave_typeEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($leave_typeEdit[0]->status)? selectedVal($leave_typeEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditLeaveType">
                  <?php if(isset($leave_typeEdit[0]->leave_type_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-state.js" ></script>
