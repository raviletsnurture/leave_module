<section id="main-content">
  <section class="wrapper site-min-height">
     <!--<div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row mbot30">
          <div class="col-lg-2">
              <div class="btn-group">
                <span class="my-span-head">Category</span>
              </div>
          </div>
          <div class="col-lg-2 pull-right">
              <div class="btn-group pull-right">
                <a href="<?php echo base_url()?>superadmin/category/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
          </div>
        </div>
      </div>-->
    <!-- page start-->
    <?php //$this->load->view('list_header'); ?>
    <section class="panel">
      <header class="panel-heading"> Request Status </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/leave_status/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="row">
          <div class="col-lg-5">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="">
                <div class="form-group ">
                  <div>
                    <select name="perpage" class="form-control perPage" onchange="changePerPage(this.value);">
                       <option value="25" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '25'){echo "selected";} ?>>25 per page</option>
                      <option value="50" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '50'){echo "selected";} ?>>50 per page</option>
                      <option value="100" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '100'){echo "selected";} ?>>100 per page</option>
                    </select>

                  </div>
              </div>
              </div>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="dataTables_filter col-lg-7" id="editable-sample_filter">
              <form method="get" class="pull-right">
                  <div class="form-group">
                      <label for="exampleInputPassword2" class="sr-only">Password</label>
                      <input type="text" placeholder="Search" name="keyword" class="form-control">
                  </div>
                  <input type="submit" value="Search" class="btn-cust btn btn-success" >
              </form>
            </div>
          </div>
        </div>-->
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">

          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>


          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th style="width:15px;">Default Status</th>
				<th>Request Status Name</th>
                <th>Status</th>

                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($leave_status as $row){ ?>
              <tr>
				<td><input type="radio" name="status_default" <?php if($row->status_default == '1') { ?>checked="checked"<?php } ?> value="<?php echo $row->leave_status_id; ?>">
                <td><?php echo $row->leave_status;?></td>
                <td><?php echo $row->status; ?></td>
                <td>
                  <a href="<?php echo base_url()?>superadmin/leave_status/edit/<?php echo $row->leave_status_id;?>" >
                  <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Leave Status" title=""><i class="fa fa-pencil"></i></button>
                  </a>
                  <a href="<?php echo base_url()?>superadmin/leave_status/delete/<?php echo $row->leave_status_id;?>" class="deleteRec">
                  <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Leave Status"><i class="fa fa-trash-o "></i></button>
                  </a>
                  </td>
              </tr>
              <?php }?>
            </tbody>
          </table>

          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
    </section>
    <!-- page end-->
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
             /* $('#example').dataTable( {
                  //"aaSorting": [[ 3, "desc" ]],
				  //        "iDisplayLength": 4,
				   "pagingType": "full_numbers",
				   "bFilter": false,
				   "bPaginate": false,
				   "bInfo" : false,
				   "aoColumnDefs": [
					  { 'bSortable': false, 'aTargets': [ 3 ] }
				   ]
              } );
			  $(".dataTables_filter input").addClass('form-control');*/

			   $("input[name='status_default']").change(function() {
					var ff = $("input[name='status_default']:checked").val();
					$.ajax({
					 url: '<?php echo base_url(); ?>superadmin/leave_status/statusdefault/',
					 data: { id: ff},
					 dataType: 'html',
					 type: 'POST',
					 success: function(data){
						 //$('.modal-body').html(data);
						 //$('#myModal').modal('show');
						 alert("hello");
						 }
					});
			   });

		  } );
      </script>
