<?php
$country = getAllCountry();

?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($cityEdit[0]->city_id)){ echo "Edit City";} else{echo "Add City";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php 
			  $form_attributes = array('name' => 'addCity', 'id' => 'addCity', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($cityEdit[0]->city_id)){
			  	echo form_open('superadmin/city/add/'.$cityEdit[0]->city_id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/city/add',$form_attributes);
			  }
			  ?>
			  
			  <div class="form-group ">
                <label for="country_id" class="control-label col-lg-2">Country<span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="country_id" id="country_id" class="form-control">
                    <option value="">Select Country</option>
                    <?php foreach($country as $row2){?>
                    <option value="<?php echo $row2->country_id;?>" <?php $value1 = (isset($cityEdit[0]->country_id)) ? $cityEdit[0]->country_id : 0; echo selectedVal($value1,$row2->country_id)?>><?php echo $row2->short_name;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  
			  <div class="form-group ">
                <label for="state_id" class="control-label col-lg-2">State</label>
                <div class="col-lg-10">
                  <select name="state_id" id="state_id" class="form-control">
                    <option value="">Select State</option>
					<?php if(isset($cityEdit[0]->state_id)) { ?>
						<?php foreach($state as $dd){?>
						<option value="<?php echo $dd->state_id;?>" <?php $value1 = (isset($cityEdit[0]->state_id)) ? $cityEdit[0]->state_id : 0; echo selectedVal($value1,$dd->state_id)?>><?php echo $dd->state_name;?></option>
						<?php } ?>
					<?php } ?>
                  </select>
                </div>
              </div>
             
			  
              <div class="form-group ">
                <label for="firstName" class="control-label col-lg-2">City Name <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="city_name" name="city_name" type="text"  value="<?php if(isset($cityEdit[0]->city_name)){ echo $cityEdit[0]->city_name;} ?>"/>
                </div>
              </div>
               <div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($cityEdit[0]->status)? selectedVal($cityEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($cityEdit[0]->status)? selectedVal($cityEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditCity">
                  <?php if(isset($cityEdit[0]->city_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-state.js" ></script> 
<script>
$('#country_id').change(function(){

    var country_id = $('#country_id').val();

    $.ajax({
            url: '<?php echo base_url();?>superadmin/city/getStates',
            type: 'POST',
            data: {
                country_id:country_id,
            },
            success:function(data){
                //alert(data);
				//var obj = JSON.parse(data);
                //alert(obj.yourfield);
				$('#state_id').html(data);
            },
            error:function(){
                alert('Data not Found')
            }


        });

 });
</script>