<section id="main-content">
   <section class="wrapper">
      <div class="row">
         <div class="col-lg-12">
            <section class="panel">
               <header class="panel-heading">
                  <?php if(isset($policiesEdit[0]->id)){ echo "Edit Policies";} else{echo "Add Policies";}?>
               </header>
               <?php
               if($this->session->flashdata('error')){?>
               <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
               </div>
               <?php } ?>
               <?php if($this->session->flashdata('success')){?>
               <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
               </div>
               <?php }?>
               <div class="panel-body">
                  <div class="form">
                     <?php
                        $form_attributes = array('name' => 'addEditPolicies', 'id' => 'addEditPolicies', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
                        if(isset($policiesEdit[0]->policiesId)){
                        	echo form_open_multipart('superadmin/policies/add/'.$policiesEdit[0]->policiesId,$form_attributes);
                        }else{
                        echo form_open_multipart(base_url().'superadmin/policies/add',$form_attributes);
                        }
                        ?>
                     <div class="form-group ">
                        <label for="firstName" class="control-label col-lg-2">Policies Name <span class="red">*</span></label>
                        <div class="col-lg-10">
                           <input class="form-control" id="policieName" name="policieName" type="text" maxlength="50" required value="<?php if(isset($policiesEdit[0]->policies_name)){ echo $policiesEdit[0]->policies_name;} ?>"/>
                        </div>
                     </div>
                     <?php if(!isset($policiesEdit[0]->policiesId)){?>
                       <div class="form-group">
                         <label class="col-lg-2 control-label">Upload file <span class="red">*</span></label>
                         <div class="col-lg-6">
                           <input type="file" class="file-pos" name="policiePdf" id="policiePdf" required>
                         </div>
                       </div>
                     <?php }?>
                     <div class="form-group ">
                        <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                        <div class="col-lg-3">
                           <select name="policiestatus" id="policiestatus" class="form-control m-bot15">
                              <option value="0" <?php echo isset($policiesEdit[0]->status)? selectedVal($policiesEdit[0]->status,"0"): '';?>>Active</option>
                              <option value="1" <?php echo isset($policiesEdit[0]->status)? selectedVal($policiesEdit[0]->status,"1"): '';?> >Deactive</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                           <button class="btn btn-danger" type="submit" name="addEditPolicies">
                           <?php if(isset($policiesEdit[0]->id)){ echo "Update";} else{echo "Submit";}?>
                           </button>
                           <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                        </div>
                     </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>

<script language="javascript" type="text/javascript">
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('#holidayDate').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});
</script>
