<section id="main-content">
    <section class="wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/policies/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
           <div class="col-lg-12">
              <?php
              if($this->session->flashdata('error')){?>
              <div class="alert alert-block alert-danger fade in">
                 <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                 <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('success')){?>
              <div class="alert alert-success fade in">
                 <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                 <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
              </div>
              <?php }?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                      Policies
                    </header>
                    <table class="table">
                        <thead>
                        <tr>
														<th>No</th>
                            <th>Policies Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
													<?php $i=1; foreach ($policies as $policie) { ?>
														<tr>
															<td><?php echo $i;?></td>
                              <td><?php echo $policie['policies_name'];?></td>
                              <td><?php if($policie['status'] == 0){echo "Active";}else{echo "Deactive";}?></td>
                              <td>
                                <a href="<?php echo base_url()?>uploads/policy/<?php echo $policie['policies_file_name'];?>" class="viewRec" target="_blank">
                                  <button class="btn btn-success btn-xs tooltips" data-toggle="tooltip" data-original-title="View&nbsp;Policies"><i class="fa fa-eye"></i></button>
                                </a>
                                <a href="<?php echo base_url()?>superadmin/policies/edit/<?php echo $policie['policiesId'];?>" >
                                <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Policies" title=""><i class="fa fa-pencil"></i></button>
                                </a>
                                <a href="<?php echo base_url()?>superadmin/policies/delete/<?php echo $policie['policiesId'];?>" class="deleteRec">
                                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Policies"><i class="fa fa-trash-o "></i></button>
                                </a>
                              </td>
                            </tr>
													<?php $i++; }?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </section>
</section>
