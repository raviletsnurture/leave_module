<?php
    $getBirthday = getBirthday();
    $leaves = getUserLeaves();
    $allbirthday = getAllUserBirthday();
    $preleave = getAllPredefineLeave();
    $getAnnouncements = getAnnouncements();
    $gettop5rewardWinner = getTop5rewardWinner();
    $getToprewardWinnerMonthly = getToprewardWinnerMonthly();
    //print_r($relievingDateList); die;
?>
<style>
.fc-sat,
.fc-sun {
	background: #F1F790;
}
.orange {
	background: #8271FF;
}
.fc-event-hori {
	border: none !important;
}
.fc-state-default {
	padding: 8px 12px 8px 12px;
	border-left-style: solid;
	border-right-style: solid;
	border-color: #ddd;
	border-width: 0 1px;
}
.scroll-height {
    height: auto !important;
}
.probation-list h4, .probation-list p{
    float: left;
    width: 100% !important;
}
.toppnel_custom{
  height: 264px;
  background: #fff;
  margin-bottom: 15px;
}
.profile_activity_custom{
  overflow-y: auto;
  height: 207px;
}
.top5 .panel-heading{
    height:60px;
    /* text-align:center; */
}
.top5 .panel-body{
    padding:0 15px;
}
</style>
<section id="main-content">
    <section class="wrapper">
        <?php if(!empty($getAnnouncements)): ?>

        <div class="row">
              <div class="col-lg-12">
                <section class="panel ">
                     <div class="col-lg-3 col-md-6"  >
                         <div class="toppnel">
                            <header class="panel-heading">
                               <span><i class="fa fa-bullhorn"></i></span> Announcements
                            </header>
                            <div class="panel-body profile-activity">
                              <?php for($a=0; $a<count($getAnnouncements); $a++): ?>
                                  <div class="activity blue">
                                      <div class="activity-desk">
                                          <div class="panel">
                                              <div class="panel-body">
                                                  <i class=" fa fa-clock-o"></i>
                                                  <h5><?php echo date("d-m-Y", strtotime($getAnnouncements[$a]->created_time)); ?></h5>
                                                  <h4><?php echo $getAnnouncements[$a]->title; ?></h4>
                                                  <p><?php echo $getAnnouncements[$a]->description; ?></p>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                <?php endfor; ?>
                            </div>
                         </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="toppnel">
                        <header class="panel-heading">
                            <i class="fa fa-calendar custom_color" aria-hidden="true"></i>
                            Upcoming leaves
                        </header>
                        <div class="panel-body profile-activity ">
                        <?php
                        $curr_date = new DateTime();
                        $curr_date->format('Y-m-d');
                        $date_count = 0;
                        foreach ($upcomingleaves as $upcomingleave) {
                          $user_date = new DateTime($upcomingleave['leave_start_date']);
                          if($user_date > $curr_date){
                            $start_date = new DateTime($upcomingleave['leave_start_date']);
                            $end_date = new DateTime($upcomingleave['leave_end_date']);
                            $date_diff = $start_date->diff($end_date);
                            if((int)$date_diff->format('%a ') > 0){
                                $date_diff_text = ''.((int)$date_diff->format('%a ') + 1).' days';
                            }else{
                                $date_diff_text = ''.((int)$date_diff->format('%a ') + 1).' day';
                            }
                       ?>
                            <div class="activity blue">
                                <div class="activity-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <h4><?php echo $upcomingleave['first_name'].' '.$upcomingleave['last_name']; echo ' '.$date_diff_text;?></h4><h4> <?php echo $upcomingleave['leave_name'];?> (<?php $sum = getLeaveCountByUser($upcomingleave['user_id'], $upcomingleave['leave_type']); echo $sum[0]['leave_sum']; ?>/<?php echo $sum[0]['leave_amount']; ?>)</h4>
                                            <em style="font-size:14px; color: #c3c3c3;"><?php echo $start_date->format('dS, M Y').' - '.$end_date->format('dS, M Y');?></em>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          <?php $date_count++;
                        }else{
                        continue;
                        }
                      } ?>
                      </div>
                       </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                          <div class="toppnel">
                            <header class="panel-heading">
                                <i class="fa fa-users" aria-hidden="true"></i> Employee for Probation
                            </header>
                            <div class="panel-body profile-activity feedback-lists">
                                <?php
                                    $today = new DateTime();
                                    foreach ($probationEmployeeList as $emp) {
                                        $totalExp = $emp['ex_year'];
                                        $joinDate = new DateTime($emp['joining_date']);
                                        $trainee_complete_at = new DateTime($emp['trainee_complete_at']);
                                        $probation_complete_at = new DateTime($emp['probation_complete_at']);
                                        $interval = $joinDate->diff($today);
                                        $dateDiff =  $interval->format('%a');
                                        if($totalExp >= 3){
                                            if($dateDiff >= 75) {
                                                $departmentName = $emp['department_name']; ?>
                                                <div class="activity blue">
                                                    <div class="activity-desk">
                                                        <div class="panel">
                                                            <div class="panel-body probation-list">
                                                                <h4><?php echo $emp['first_name'].' '.$emp['last_name']; ?></h4>
                                                                <p><?php echo $emp['department_name'].' - '.$emp['employee_type']; ?></p>
                                                                <p><?php echo 'Joining: '.$joinDate->format('d-M-Y'); ?></p>
                                                                <?php if($emp['employee_type'] == 'Trainee'){?>
                                                                  <p style="color: #e40c0c;"><?php echo 'Trainee Complete At: '.$trainee_complete_at->format('d-M-Y'); ?></p>
                                                                <?php }else{?>
                                                                  <p style="color: #e40c0c;"><?php echo 'Probation Complete At: '.$probation_complete_at->format('d-M-Y'); ?></p>
                                                                <?php }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php }
                                        } else if($totalExp < 3) {
                                            if($dateDiff >= 165) {?>
                                                <div class="activity blue">
                                                    <div class="activity-desk">
                                                        <div class="panel">
                                                            <div class="panel-body probation-list">
                                                                <h4><?php echo $emp['first_name'].' '.$emp['last_name']; ?></h4>
                                                                <p><?php echo $emp['department_name'].' - '.$emp['employee_type']; ?></p>
                                                                <p><?php echo 'Joining: '.$joinDate->format('d-M-Y'); ?></p>
                                                                <?php if($emp['employee_type'] == 'Trainee'){?>
                                                                  <p style="color: #e40c0c;"><?php echo 'Trainee Complete At: '.$trainee_complete_at->format('d-M-Y'); ?></p>
                                                                <?php }else{?>
                                                                  <p style="color: #e40c0c;"><?php echo 'Probation Complete At: '.$probation_complete_at->format('d-M-Y'); ?></p>
                                                                <?php }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php
                                            }
                                        }
                                    }
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6" >
                        <?php /*<div class="toppnel">
                            <header class="panel-heading">
                                <i class="fa fa-comments-o" aria-hidden="true"></i>
                                Remaining Feedbacks
                            </header>
                            <div class="panel-body profile-activity feedback-lists">
                              <?php foreach ($checkRemaningFeedbacks as $checkRemaningFeedback) {
                              if($checkRemaningFeedback->feedback != ''){
                                break;
                              }?>
                                <div class="activity blue">
                                    <div class="activity-desk">
                                        <div class="panel">
                                            <div class="panel-body">
                                                <h4><?php echo $checkRemaningFeedback->first_name.' '.$checkRemaningFeedback->last_name; ?></h4>
                                                <p><?php echo $checkRemaningFeedback->department_name; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              <?php
                            } ?>
                            </div>
                          </div> */?>
                        <div class="toppnel_custom">
                            <header class="panel-heading">
                                <i class="fa fa-users" aria-hidden="true"></i> Relieving Employee
                            </header>
                            <div class="panel-body feedback-lists">
                                <?php
                                if(isset($relievingDateList) && !empty($relievingDateList)){
                                    foreach ($relievingDateList as $emp) {
                                                $relevingDate = $emp['reliving_date'];
                                                $newDate = date("d-M-Y", strtotime($relevingDate));
                                                $empName =  $emp['first_name'].' '.$emp['last_name'];
                                                $departmentName = $emp['department_name']; ?>

                                                <div class="activity blue">
                                                    <div class="activity-desk">
                                                        <div class="panel">
                                                            <div class="panel-body probation-list">
                                                                <h4><?php echo $empName; ?></h4>
                                                                <p><?php echo $departmentName; ?></p>
                                                                <p><?php echo 'Relieving: '.$newDate; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php
                                    }
                                }
                                else {
                                    echo "No Records Found<br>";
                                }
                                ?>
                            </div>
                        </div>
                        <?php /*?>
                        <div class="toppnel_custom">
                          <header class="panel-heading">
                              <i class="fa fa-users" aria-hidden="true"></i> Reward Winners
                          </header>
                              <section class="profile_activity_custom">
                                  <header class="panel-heading ">
                                      Top 3 reward winners
                                  </header>
                                      <?php if (!empty($gettop5rewardWinner) && $gettop5rewardWinner != ''){ ?>
                                        <div class="panel-body">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Reward Points</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $x=1;
                                            for($a=0; $a<count($gettop5rewardWinner); $a++): ?>
                                                <tr>
                                                    <td style="font-size:14px; color: #7B787B;"><?php echo $x.'.&nbsp;'; echo $gettop5rewardWinner[$a]["first_name"];echo " "; echo $gettop5rewardWinner[$a]["last_name"];?></td>
                                                    <td style="font-size:14px; color: #7B787B;"><?php echo $gettop5rewardWinner[$a]["reward_points"];?></td>
                                                </tr>
                                                <?php $x++;
                                            endfor; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                      <?php }else{
                                        echo "<h6 class='text-center'>No winners yet!<h6>";
                                      } ?>

                              <header class="panel-heading">
                              Top 3 reward winners this month
                              </header>
                                <?php if (!empty($getToprewardWinnerMonthly) && $getToprewardWinnerMonthly != ''){  ?>
                                  <div class="panel-body">
                                  <table class="table table-striped">
                                  <thead>
                                  <tr>
                                  <th>Name</th>
                                  <th>Reward Points</th>
                                  </tr>
                                  </thead>
                                  <tbody>

                                  <?php $x=1;
                                  for($a=0; $a<count($getToprewardWinnerMonthly); $a++): ?>
                                  <tr>
                                  <td style="font-size:14px; color: #7B787B;"><?php echo $x.'.&nbsp';echo $getToprewardWinnerMonthly[$a]->first_name;echo " ";echo $getToprewardWinnerMonthly[$a]->last_name;?></td>
                                  <td style="font-size:14px; color: #7B787B;"><?php echo $getToprewardWinnerMonthly[$a]->totalpoints;?></td>
                                  </tr>
                                  <?php $x++;
                                  endfor; ?>
                                  </tbody>
                                  </table>
                                  </div>
                                <?php }else{
                                  echo "<h5 class='text-center'>No winners this month</h5>";
                                } ?>

                              </section>
                        </div>
                        <?php */?>
                    </div>
                </section>
              </div>
        </div>

        <?php endif; ?>
        <section class="panel">
            <?php if($this->session->flashdata('error')){?>
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('success')){?>
                <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                </div>
            <?php }?>


            <!-- Top 5 module -->
            <div class="row top5">
                <section style="margin-top:20px;" class="panel">
                    <div class="col-sm-4 col-xs-12">
                        <div class="toppnel">
                            <header class="panel-heading">
                                <span><i class="fa fa-bullhorn"></i></span> Top 5 Oldest Employee of the Companies
                            </header>
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Joined On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $x=1;
                                        for($a=0; $a<count($OldestEmployees); $a++): ?>
                                            <tr>
                                                <td style="font-size:14px; color: #7B787B;"><?php echo $x.'.&nbsp;'; echo $OldestEmployees[$a]["first_name"];echo " "; echo $OldestEmployees[$a]["last_name"];?></td>
                                                <td style="font-size:14px; color: #7B787B;"><?php echo $OldestEmployees [$a]["user_created"];?></td>
                                            </tr>
                                            <?php $x++;
                                        endfor; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="toppnel">
                            <header class="panel-heading">
                                <span><i class="fa fa-bullhorn"></i></span> Top 5 Newest Employees of the company
                            </header>
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Joined On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $x=1;
                                        for($a=0; $a<count($NewestEmployees); $a++): ?>
                                            <tr>
                                                <td style="font-size:14px; color: #7B787B;"><?php echo $x.'.&nbsp;'; echo $NewestEmployees[$a]["first_name"];echo " "; echo $NewestEmployees[$a]["last_name"];?></td>
                                                <td style="font-size:14px; color: #7B787B;"><?php echo $NewestEmployees[$a]["user_created"];?></td>
                                            </tr>
                                            <?php $x++;
                                        endfor; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="toppnel">
                            <header class="panel-heading">
                                <span><i class="fa fa-bullhorn"></i></span> Top 5 Rewards Claimed by Employees
                            </header>
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Reward Name</th>
                                            <th>Reward Points</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $x=1;
                                        for($a=0; $a<count($TopReward); $a++): ?>
                                            <tr>
                                                <td style="font-size:14px; color: #7B787B;"><?php echo $x.'.&nbsp;'; echo $TopReward[$a]["ProductName"];?></td>
                                                <td style="font-size:14px; color: #7B787B;"><?php echo $TopReward[$a]["ProductPoint"];?></td>
                                            </tr>
                                            <?php $x++;
                                        endfor; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- End -->
            <!-- Overall Status of the Company -->
            <div class="panel-body tile_count">
                <div class="row center">
                    <div class="col-md-6 col-sm-12 border_for_col">
                        <div class="inner_div">
                            <h3 class="text-center"><u>Number of applications received</u></h3>
                            <div class="row">
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                    <div class="count"><a href="<?php echo base_url();?>superadmin/recruitment?date1=<?php echo $ApplicationRecievedLastMonth['year'].'/'.$ApplicationRecievedLastMonth['month'];?>"><?php echo $ApplicationRecievedLastMonth['applicationLastMonth'];?></a></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                    <div class="count"><a href="<?php echo base_url();?>superadmin/recruitment?sd=<?php echo $ApplicationRecievedLast3Month['startDay'].'&ed='.$ApplicationRecievedLast3Month['endDay'];?>"><?php echo $ApplicationRecievedLast3Month['applicationLast3Months']?></a></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                    <div class="count"><a href="<?php echo base_url();?>superadmin/recruitment?date1=<?php echo $ApplicationRecievedLastYear['year']?>"><?php echo $ApplicationRecievedLastYear['applicationLastYear'];?></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 ">
                        <div class="inner_div">
                            <h3 class="text-center"><u>Number of new team members joined</u></h3>
                            <div class="row">
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                    <div class="count"><?php echo $getNoOfRecruitmentLastMonth;?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                    <div class="count"><?php echo $getNoOfRecruitmentLast3Month;?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                    <div class="count"><?php echo $getNoOfRecruitmentLastYear;?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row center">
                    <div class="col-md-6 col-sm-12 border_for_col">
                            <div class="inner_div">
                                <h3 class="text-center"><u>Number of interviews scheduled</u></h3>
                                    <div class="row">
                                        <div class="col-sm-4 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                            <div class="count"><?php echo $InterviewsScheduleLastMonth?></div>
                                        </div>
                                        <div class="col-sm-4 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                            <div class="count"><?php echo $InterviewsScheduleLast3Month;?></div>
                                        </div>
                                        <div class="col-sm-4 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                            <div class="count"><?php echo $InterviewsScheduleLastYear;?></div>
                                        </div>
                                    </div>

                            </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="inner_div">
                            <h3 class="text-center"><u>Number of leaves</u></h3>
                            <div class="row">
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                    <div class="count"><?php if($getTotalLeavesLastMonth->totalLeave !=''){echo($getTotalLeavesLastMonth->totalLeave);}else{
                                        echo('0');
                                    } ?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                    <div class="count"><?php if($getTotalLeavesLast3Months->totalLeave != ''){echo $getTotalLeavesLast3Months->totalLeave;}else{echo 0;}?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                    <div class="count"><?php  echo($getTotalLeavesLastYear->totalLeave)?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row center">
                    <div class="col-md-6 col-sm-12 border_for_col">
                        <div class="inner_div">
                            <h3 class="text-center"><u>Average leave per person</u></h3>
                            <div class="row">
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                    <div class="count"><?php echo $AverageLeaveLastMonth;?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                    <div class="count"><?php echo $AverageLeaveLast3Months?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                    <div class="count"><?php echo $AverageLeaveLastYear?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="inner_div">
                            <h3 class="text-center"><u>Average working hours</u></h3>
                            <div class="row">
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                    <div class="count"><?php echo date('g:i', strtotime($AverageWorkingHoursLastMonth));?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                    <div class="count"><?php echo date('g:i',strtotime($AverageWorkingHoursLast3Months));?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                    <div class="count"><?php echo date('g:i',strtotime($AverageWorkingHoursLastYear));?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row center">
                    <div class="col-md-6 col-sm-12 border_for_col">
                        <div class="inner_div">
                            <h3 class="text-center"><u>Male vs Female Ratio</u></h3>
                            <div class="row">
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                    <div class="count"><?php echo $MaleFemaleRatioLastMonth['var1'].":".$MaleFemaleRatioLastMonth['var2'];?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                    <div class="count"><?php if($MaleFemaleRatioLast3Months['var1'] !='' && $MaleFemaleRatioLast3Months['var2'] != ''){echo $MaleFemaleRatioLast3Months['var1'].":".$MaleFemaleRatioLast3Months['var2'];}else{echo "0:0";} ?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                    <div class="count"><?php echo $MaleFemaleRatioLastYear['var1'].":".$MaleFemaleRatioLastYear['var2']?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="inner_div">
                            <h3 class="text-center"><u>Average Experience of Team member(in years)</u></h3>
                            <div class="row">
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last month</span>
                                    <div class="count"><?php echo number_format((float)$AverageExperienceOfTeamMemberLastmonth, 2, '.', '');?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last 3 months</span>
                                    <div class="count"><?php echo number_format((float)$AverageExperienceOfTeamMemberLast3months, 2, '.', '');?></div>
                                </div>
                                <div class="col-sm-4 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Last year</span>
                                    <div class="count"><?php echo  number_format((float)$AverageExperienceOfTeamMemberLastYear, 2, '.', '');?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End -->
                <hr>
            <!-- HRMS access outside of Local IP. -->
            <hr>
            <div class="row">
               <div class="col-sm-12">
                   <div class="toppel">
                        <header class="panel-heading">
                            <span><i class="fa fa-bullhorn"></i></span> Top 10 users did HRMS access outside of Local IP(according to no of attempts in last 7 days)
                        </header>
                        <div class="panel-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Sl no</th>
                                        <th>Name</th>
                                        <th>Ips</th>
                                        <th>Device</th>
                                        <th>Number of Attempts</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1;
                                    for($a=0; $a<count($HrmsAccessdOutside); $a++):
                                    ?>
                                    <tr>
                                        <td><?php echo $x?></td>
                                        <td><?php echo $HrmsAccessdOutside[$a]['first_name']." ".$HrmsAccessdOutside[$a]['last_name']?></td>
                                        <td><?php echo $HrmsAccessdOutside[$a]['ip']?></td>
                                        <td><?php echo $HrmsAccessdOutside[$a]['user_agent']?></td>
                                        <td><?php echo $HrmsAccessdOutside[$a]['noofAttempts']?></td>
                                    </tr>
                                    <?php $x++;
                                        endfor;?>
                                </tbody>
                            </table>
                        </div>
                   </div>
               </div>
            </div>


            <!-- End -->
            <div class="row state-overview">
		        <div class="col-lg-12 col-sm-6">
                    <section class="panel tasks-widget">
                        <header class="panel-heading"> Upcoming Birthday(s) </header>
                        <div class="panel-body">
                            <div class="task-content">
                                <ul id="sortable" class="task-list sortable">
                                    <?php if(count($getBirthday) == 0){?>
                                    <li class="list-warning"> <i class=" fa fa-ellipsis-v"></i>
                                    <div class="task-title"> <span class="task-title-sp"> No record found! </span></div>
                                    </li>
                                    <?php }?>
                                    <?php
                                    $in = array('danger','warning','success','info','primary','inverse');
                                    foreach($getBirthday as $birthList){  ?>
                                    <li class="list-<?php echo $in[rand(0,count($in)-1)];?>"> <i class=" fa fa-ellipsis-v"></i>
                                    <div class="task-title"> <span class="task-title-sp"> <?php echo $birthList['full_name'];?> turns <span class="badge badge-sm label-<?php echo $in[rand(0,count($in)-1)];?>"><?php echo getAge($birthList['birthdate'])?></span> on <?php echo toBirthDate(toBirthYear($birthList['birthdate']));?> </span>
                                        <div class="pull-right hidden-phone">
                                            <a href="mailto:<?php echo $birthList['email']; ?>">
                                                <button class="btn btn-danger btn-xs fa fa-envelope tooltips"  data-toggle="tooltip" data-original-title="Send Email"></button>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <?php }?>
                                </ul>
                            </div>
                        </div>
			        </section>
                </div>
             </div>

		    <aside class="">
                <header class="panel-heading"><h4 style="float: left; margin-right:5px;"><b>Employee Leaves</b></h4>
                <h4 class="" style="float: left;">
                Total : <span class="totalMonth"> <?php echo $totalThisMonthLeave;?></span> Leaves,
                        <span class="totalEarlyMonth"><?php echo $monthEarlyLeave;?></span> Early Leave,
                        <span class="totalPendingMonth"><?php echo $monthPendingLeave;?></span> Pending ,<span class="monthBirthAnniLeave"><?php echo $monthBirthAnniLeave;?></span> Birthday & Anniversary Leaves,  <?php echo $pendingLeavetotal;?> Pending(all), <?php echo $getActiveEmployee;?> Employees</h4>
                </header>
                <section class="panel">
                    <div class="panel-body" style="padding: 15px 0px 15px 0px;">
                        <div id="leavecalendar" class="has-toolbar"></div>
                    </div>
                </section>
		    </aside>

		    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Feedbacks remaining inform to TL/Management</h4>
                        </div>
                    </div>
                </div>
		    </div>

            <div id="eventContent" title="Event Details">
                    <div id="eventInfo"></div>
                    <p><strong><a id="eventLink" target="_blank"></a></strong></p>
            </div>
        </section>
  </section>
</section>
<script src="<?php echo base_url();?>assets/js/fullcalendar/fullcalendar/moment.js"></script>
<script type="text/javascript">
function getClick(id)
{
	$.ajax({
		 url: '<?php echo base_url(); ?>superadmin/dashboard/sendmail/',
		 data: { id: id},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('.modal-body').html(data);
			 $('#myModal').modal('show');

		 }
	});
}
function getEventDate(event) { var dateobj = event.start;
    var date = dateobj.getFullYear()+'-'+dateobj.getMonth()+1+'-'+dateobj.getDate();
    return date;
}
$( document ).ready(function() {
    $('#scroll-announcements').niceScroll({
        styler: "fb",
        cursorcolor: "#e8403f",
        cursorwidth: '6',
        cursorborderradius: '10px',
        background: '#404040',
        spacebarenabled: false,
        cursorborder: '',
        zindex: '1000'
    });

	/* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#leavecalendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: false,
        droppable: false,

        events: [
			<?php foreach($leaves as $leave) { ?>
            {
				title: '<?php echo $leave['first_name'].' '.$leave['last_name']; ?>',
                start: '<?php echo $leave['leave_start_date']; ?> 00:00:00',
                end: '<?php echo $leave['leave_end_date']; ?> 00:00:00',
				description: '<div>Department: <?php echo $leave['department_name']; ?><br />Leave Type: <?php echo $leave['leave_name']; ?><br />Leave Status: <?php if($leave['is_cancelled'] == 1) { echo "cancelled";} else { echo $leave['leave_status_name']; } ?></div>',
				backgroundColor: '<?php if($leave['is_cancelled'] == 1) { echo '#ED5757'; } else { if($leave['leave_status_name'] == 'Pending') { echo '#6883a3'; } elseif($leave['leave_status_name'] == 'Approved') { echo '#378006'; } else { echo '#FA5578'; } }?>',
				textColor: '#fff',
			},
			<?php } ?>
			<?php foreach($allbirthday as $birth) { if($birth['month'] < 10) { $d = '0'; } else { $d = ''; }  if($birth['day'] < 10) { $d1 = '0'; } else { $d1 = ''; } ?>
            {
				title: '<?php echo $birth['fullname']; ?>',
                start: '<?php echo Date("Y"); ?>-<?php echo $d.$birth['month']; ?>-<?php echo $d1.$birth['day']; ?>',
                imageurl: 'img',
				description: '',
				backgroundColor: '#41cac0',
				textColor: '#fff',
            },
			<?php } ?>
			<?php foreach($preleave as $prel) { ?>
            {
        title: '<?php echo $prel['festival_name']; ?>',
        start: '<?php echo $prel['festival_date']; ?>',
				end: '<?php echo $prel['festival_date']; ?>',
        image: 'img',
				description: '',
				backgroundColor: '#8271FF',
				textColor: '#fff',
            },
			<?php } ?>
        ],

		eventRender: function(event, eventElement, monthView) {
			if (event.imageurl)
			{
				eventElement.find("div.fc-event-inner").prepend("<i class='fa fa-gift' style='font-size:16px;'></i>&nbsp;");
			}
			if (event.image)
			{
				eventElement.find("div.fc-event-inner").prepend("<i class='fa fa-smile-o' style='font-size:16px;'></i>&nbsp;");
				var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
				$('.fc-day[data-date="'+ dateString + '"]').addClass('orange');
			}

		},
		eventClick:  function(event, jsEvent, view) {

		if(!event.image && !event.imageurl) {
        $("#eventInfo").html(event.description);
        $("#eventLink").attr('href', event.url);
        $("#eventContent").dialog({ modal: true, title: event.title });
		}
    },
		/*eventMouseover: function(event, jsEvent, view) {
			$('.fc-event-inner', this).append('<div id=\"'+event.id+'\" class=\"hover-end\">'+event.description+'</div>');
		},
		eventMouseout: function(event, jsEvent, view) {
			$('#'+event.id).remove();
		}*/
    });
    $("body").on('click',".fc-button-next,.fc-button-prev",function(){
      var month = $(".fc-header-title h2").html();
    		$.ajax({
    			type: 'POST',
    			url: '<?php echo base_url()?>superadmin/dashboard/getTotalLeave',
    			data: 'month='+ month,
    			beforeSend: function(){
    			},
    			success:function(data){
            data = JSON.parse( data );
            $(".panel-heading h4 .totalMonth").html(data.totalLeave);
            $(".panel-heading h4 .totalPendingMonth").html(data.monthPendingLeave);
            $(".panel-heading h4 .monthBirthAnniLeave").html(data.monthBirthAnniLeave);
            $(".panel-heading h4 .totalEarlyMonth").html(data.monthEarlyLeave);

    			}
     		});
  	});


});

</script>
<?php //display popup if any feedback remaning
if($checkRemaningFeedbacks[0]->feedback === null){?>
<script>
$(document).ready(function() {
  //$('#myModal').modal('show');
});
</script>
<?php } ?>
