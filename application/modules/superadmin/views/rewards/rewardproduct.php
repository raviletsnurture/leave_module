<style type="text/css">
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading">Rewards Product</header>
        <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
			<div class="row">
			    <div class="col-lg-12">
					<div id="editable-sample_length" class="dataTables_length">
            <div class="btn-group">
							<a href="<?php echo base_url()?>superadmin/rewards/addproduct">
								<button class="btn btn-info" id="editable-sample_new"> Add Reward Product <i class="fa fa-plus"></i> </button>
							</a>
						</div>
					</div>
				</div>
            </div>
      </div>
      <div class="panel-body">
        <div class="">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
        <th width="40%">Product Name</th>
        <th width="5%">Points</th>
        <th width="40%">Description</th>

				<th width="15%">Status</th>
                <th width="20%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($products as $row){

                ?>
              <tr>
        <td><?php echo $row->name; ?></td>
        <td><?php echo $row->point; ?></td>
				<td><?php echo $row->description; ?></td>

				<td><?php if($row->status == '0') {
                            echo "Deactive";
                        } else if($row->status == '1') {
                            echo "Active";
                        } ?></td>

				<td><?php if($row->status == '0'){ ?>
                <a href="#" class="" onclick="cancel(<?php echo $row->id; ?>)">
                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Cancel&nbsp;Request"><i class="fa fa-exclamation-triangle"></i></button>
                </a>
                <?php }?>
                <a href="<?php echo base_url() . 'superadmin/rewards/editproduct/' . $row->id; ?>">
                <button class="btn btn-primary btn-xs" title=""><i class="fa fa-pencil"></i></button>
                </a>
        </td>
              </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>
			  <!-- My Leaves end-->
    <!-- page end-->
  </section>
</section>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script>
$(document).ready(function() {

	 var dt =  $('#example').dataTable( {
		 "aaSorting": [[ 3, "asc" ]],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : false,

		 "oSearch": { "bSmart": false, "bRegex": true },
		 "aoColumnDefs": [
			{
			'bSortable': true, 'aTargets': [ 3 ]
			}
		 ]
		});
});

function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel Reward!");
	if (r == true){
			$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/cancel',
		 data: { reward_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 location.reload();
	     },
    error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
		});
		//location.reload();
	} else {
	}
}
</script>
