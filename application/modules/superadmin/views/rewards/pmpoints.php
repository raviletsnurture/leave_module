<?php
$user = getAllUsers();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($leaveEdit[0]->leave_id)){ echo "Edit Exceptional Reward";} else{echo "Add Exceptional Reward";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addReward', 'id' => 'addReward', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($leaveEdit[0]->leave_id)){
			  	echo form_open('superadmin/rewards/addpmpoints/'.$leaveEdit[0]->leave_id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/rewards/addpmpoints',$form_attributes);
			  }
			  ?>

			  <div class="form-group">
                <label  class="col-lg-2 control-label">PM Name <span class="red">*</span></label>
                <div class="col-lg-6">
                  
                  <select class="form-control" id="user_id" name="user_id" placeholder="User Name ">
                    <option value="">Select User</option>
                    <?php  foreach($users as $u): ?>
                    <option value="<?php echo $u->user_id; ?>"><?php echo $u->first_name . ' ' . $u->last_name; ?> </option>
                  <?php endforeach; ?>
                  </select>
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Reward Point <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="reward_point" name="reward_point" placeholder="Reward Point "  value="<?php  if(isset($leaveEdit[0]->reward_name)){ echo $leaveEdit[0]->reward_name;} ?>">
                </div>
        </div>

               <!--<div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>-->
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditLeave">
                  <?php if(isset($leaveEdit[0]->leave_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
          <section class="panel">
                 <div class="panel-body">
                  <div class=""> 
                    <table class="table table-striped table-hover table-bordered" id="example2">
                      <thead>
                        <tr>
                            <th>User</th>                  
                            <th>Last Reward Point</th>
                            <th>Total Reward Point</th>                  
                            <th>Applied Date</th>        
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($history as $row){ ?>
                        <tr>
                            <td><?php echo $row->first_name . ' ' . $row->last_name; ?></td>
                            <td><?php echo $row->reward_point; ?></td> 
                            <td><?php echo $row->pm_reward_points; ?></td>                                    
                            <td><?php echo date("d-m-Y", strtotime($row->created_at)) ?></td>                 
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
          </section>
      </div>
    </div>
  </section>  
</section>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-negativereward.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

   var dt =  $('#example2').dataTable( {
     "aaSorting": [[ 3, "asc" ]],
     "iDisplayLength": 20,
     "pagingType": "full_numbers",
     "dom": 'Cfrtip',
     "destroy": true,
    //  "bFilter": false,
     "bPaginate": true,
     "bInfo" : false,

     "oSearch": { "bSmart": false, "bRegex": true },
     "aoColumnDefs": [
      {
      'bSortable': true, 'aTargets': [ 3 ]
      }
     ]
    });
});
</script>
