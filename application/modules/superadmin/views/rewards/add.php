<?php
$user = getAllUsers();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($rewardEdit[0]->leave_id)){ echo "Edit Reward";} else{echo "Add Reward";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addReward', 'id' => 'addReward', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($rewardEdit[0]->id)){
			  	echo form_open('superadmin/rewards/addReward/'.$rewardEdit[0]->id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/rewards/addReward',$form_attributes);
			  }
			  ?>

			  <div class="form-group">
                <label  class="col-lg-2 control-label">Reward Name <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="reward_name" name="reward_name" placeholder="Reward Name "  value="<?php  if(isset($rewardEdit[0]->reward_name)){ echo $rewardEdit[0]->reward_name;} ?>">
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Reward Point <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="reward_point" name="reward_point" placeholder="Reward Point "  value="<?php  if(isset($rewardEdit[0]->point)){ echo $rewardEdit[0]->point;} ?>">
                </div>
        </div>



              <div class="form-group">
                <label  class="col-lg-2 control-label">Description <span class="red">*</span></label>
                <div class="col-lg-6">
                  <textarea class="form-control" id="description" name="description"   rows="3" cols="40" placeholder="Reward Description"><?php if(isset($rewardEdit[0]->description)){ echo $rewardEdit[0]->description;} ?></textarea>
                </div>
              </div>

              <div class="form-group ">
                      <label for="project_id" class="control-label col-lg-2">Status<span class="red">*</span></label>
                      <div class="col-lg-6">
                        <select name="reward_status" id="reward_status" class="form-control">
                          <option value="1">Activate</option>
                          <option value="0">Deactivate</option>
      				  </select>
                      </div>
                    </div>

               <!--<div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($rewardEdit[0]->status)? selectedVal($rewardEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($rewardEdit[0]->status)? selectedVal($rewardEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>-->
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditLeave">
                  <?php if(isset($rewardEdit[0]->id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-addreward.js" ></script>
