<style type="text/css">
.dataTables_filter {
     /* display: none; */
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height"  style="min-height: 594px;">
    <!-- My Leaves start-->
	<section class="panel">
      <header class="panel-heading">Rewards History</header>
      <div class="panel-body">
        <div class="">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                  <th width="40%">User</th>
                  <th width="40%">Product</th>
                  <th width="40%">Point</th>                  
          				<th width="5%">status</th>				
          				<th width="15%">Applied Date</th>        
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($rewards as $row){
                ?>
              <tr>
                  <td><?php echo $row->first_name . ' ' . $row->last_name; ?></td>
                  <td><?php echo $row->name; ?></td>
                  <td><?php echo $row->point; ?></td>          				
          				<td>
                   <?php if($row->status == 3){
                        echo "Cancelled";
                   }else{ ?>  
                   <select id="reward_status" name="reward_status" onchange="updateReward(this)"  user_id="<?php echo $row->user_id;?>" row_id="<?php echo $row->id;?>">   
                      <?php if($row->status == 0){ ?>
                      <option value="0" <?php if($row->status == 0 ) { ?>selected="selected"<?php } ?>>Pending</option>
                      <?php }else{ ?>
                      <option value="" >--Select--</option>
                      <?php  } ?>
                      <option value="1" <?php if($row->status == 1 ) { ?>selected="selected"<?php } ?>>Accepted</option>
                      <option value="2" <?php if($row->status == 2 ) { ?>selected="selected"<?php } ?>>Rejected</option>                      
                  </select>              
                  <?php } ?>
                  </td>
          				<td><?php echo date("d-m-Y", strtotime($row->created_at)) ?></td>          				
              </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>			  
    <!-- page end-->
  </section>
</section>
</section>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script>
$(document).ready(function() {

	 var dt =  $('#example').dataTable( {
		 "aaSorting": [[ 3, "asc" ]],
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",
		 "dom": 'Cfrtip',
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : true,

		 "oSearch": { "bSmart": false, "bRegex": true },
		 "aoColumnDefs": [
			{
			'bSortable': true, 'aTargets': [ 3 ]
			}
		 ]
		});
});

function updateReward(sel)
{
  var ff = sel.value.split(",");
  if(ff == '')
  {
    alert("Please Select an Option");
    location.reload();
    //return false;
  }
  else
  {
    var r = false;
    if(ff[1] == 2)
    {
      $('#h_id').val($(sel).attr('row_id'));
      $('#u_id').val($(sel).attr('user_id'));
      $('#reward_id').val(ff[0]);
      $('#myModal').modal();
    }else{
        r = confirm("Are You Sure for this operation");
    }
    if (r == true)
    {
      // show loader
      $('#loaderAjax').show();
      $.ajax({
       url: '<?php echo base_url(); ?>superadmin/rewards/claim_product',
       data: { reward_id: ff[0], rh_id: $(sel).attr('row_id'), status: ff[1], user_id: $(sel).attr('user_id')},
       dataType: 'html',
       type: 'POST',
       success: function(data){
         // hide loader
         $('#loaderAjax').hide();         
         //location.reload();
         }
      });
    }
    else {

    }
   }

}
</script>
