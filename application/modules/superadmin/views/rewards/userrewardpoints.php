<style type="text/css">
/* .dataTables_filter {
  width: 30%;    
} */
.span6{
  width: 50%;
  float: left;
}
.lastMonthClass{
	background-color:#F7A6B1 !important;
}

.start_date .ui-datepicker-calendar,.eom_month .ui-datepicker-calendar{
    display: none;
}
</style>
<section id="main-content">
<section class="wrapper">
<div class="row">
<div class="col-lg-12">
    <section class="panel">
          <header class="panel-heading">
            Search User Reward  point
          </header>
          <div class="panel-body">
            <div class="form">
              <form action="<?php echo base_url(); ?>superadmin/rewards/userrewardpoints" method="post" accept-charset="utf-8" name="addReward" id="addReward" autocomplete="off" class="commonForm  cmxform form-horizontal tasi-form" novalidate="novalidate">
              <div class="form-group">
                <label class="col-lg-2 control-label">Select Month <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" name="eom_month" id="eom_month" value="<?php if(isset($month) && $month != ''){ echo $month; } ?>" class="form-control" data-date-format="yyyy-MM-dd">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditLeave">
                  Submit</button>
                  <button class="btn btn-default" type="submit" onclick="search_submit()" name="reset" value="Reset">Reset</button>
                </div>
              </div>
              </form>
            </div>
          </div>
    </section>  

    <!-- My Leaves start-->
	<section class="panel">
        <header class="panel-heading">User's Rewards Points</header>
       <!--  <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
    			<div class="row">
    			    <div class="col-lg-12">
    					<div id="editable-sample_length" class="dataTables_length"> 
                  
    					</div>
    				</div>
          </div>
        </div> -->
      <div class="panel-body">
        <div class="">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          
		      <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
        <th width="30%">Full Name</th>
        <th width="15%">Department</th>
        <th width="5%">Karma Points</th>

			<!--	<th width="15%">Status</th>
                <th width="20%">Action</th> -->
              </tr>
            </thead>
            <tbody>
              <?php $cnt=1; foreach($points as $row){
                ?>
              <tr>
        <td>
        <a href="<?php echo base_url(); ?>superadmin/rewards/userReward/<?php echo $row->user_id; ?>">
        <?php echo $row->first_name . ' ' . $row->last_name; ?>          
        </td>
        <td><?php echo $row->department_name; ?></td>
				<td><?php echo $row->karma_points; ?></td>
        <!--
				<td><?php if($row->status == 'Deactive') {
                            echo "Deactive";
                        } else if($row->status == 'Active') {
                            echo "Active";
                        } ?></td>

				<td><?php if($row->status == 'Deactive'){ ?>
                <a href="#" class="" onclick="cancel(<?php echo $row->id; ?>)">
                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Cancel&nbsp;Request"><i class="fa fa-exclamation-triangle"></i></button>
                </a>
              <?php }?></td> -->
              </tr>
              <?php $cnt++; }?>
            </tbody>
          </table>
        </div>
      </div>
			  <!-- My Leaves end-->
    <!-- page end-->
    </div>
    </div>
  </section>
</section>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script>
function search_submit(e){
  $("#eom_month").val('');
  document.getElementById("addReward").reset();   
}

$(document).ready(function() {

	 var dt =  $('#example').dataTable( {		 
		 "iDisplayLength": 20,
		 "pagingType": "full_numbers",		
		 "destroy": true,
		 //"bFilter": false,
		 "bPaginate": true,
		 "bInfo" : false,
     "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
		});
});

function cancel(sel){
	var r = confirm("Are You Sure You Want to Cancel Reward!");
	if (r == true){
			$('#loaderAjax').show();
		$.ajax({
		 url: '<?php echo base_url(); ?>rewards/cancel',
		 data: { reward_id: sel },
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('#loaderAjax').hide();
			 location.reload();
	     },
    error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
		});
		//location.reload();
	} else {
	}
}
</script>


<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-addeom.js" ></script>
<script>
$(document).ready(function(){
  $('#eom_month').datepicker({
      autoClose:true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat: 'MM-yy',
      minDate: '-1y',
      maxDate: 'now',
      viewMode: "months",
      minViewMode: "months",
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').addClass(this.id);
        $('#ui-datepicker-div').removeClass('select_year');
      },
      onClose: function(dateText, inst) {
        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
      }
    });
});
</script>
