<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($product[0]->id)){ echo "Edit Product";} else{echo "Add Product";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addLeave', 'id' => 'addLeave', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form", "enctype"=>"multipart/form-data" );
			  if(isset($product[0]->id)){
			  	echo form_open('superadmin/rewards/addProductdata/'.$product[0]->id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/rewards/addProductdata',$form_attributes);
			  }
			  ?>
			  <div class="form-group">
          <label  class="col-lg-2 control-label">Product Name <span class="red">*</span></label>
          <div class="col-lg-6">
            <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name "  value="<?php  if(isset($product[0]->name)){ echo $product[0]->name;} ?>">
          </div>
        </div>
        <div class="form-group">
                <label  class="col-lg-2 control-label">Product Point <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="point" name="point" placeholder="Product Point "  value="<?php  if(isset($product[0]->point)){ echo $product[0]->point;} ?>">
                </div>
        </div>
          <div class="form-group">
                <label  class="col-lg-2 control-label">Description <span class="red">*</span></label>
                <div class="col-lg-6">
                  <textarea class="form-control" id="description" name="description"   rows="3" cols="40" placeholder="Product Description"><?php if(isset($product[0]->description)){ echo $product[0]->description;} ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label  class="col-lg-2 control-label">Product Image <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="file" class="form-control" id="prod_image" name="prod_image" accept="image/*" > 
                  <?php if(isset($product[0]->image)){ ?>
                  <img width="60%" height="40%" src="<?php echo base_url(); ?>/uploads/products/<?php echo $product[0]->image; ?>" />
                  <?php } ?>                   
                </div>
              </div>
              <div class="form-group ">
                      <label for="project_id" class="control-label col-lg-2">Status<span class="red">*</span></label>
                      <div class="col-lg-6">
                        <select name="status" id="status" class="form-control">
                          <option value="1">Active</option>
                          <option value="0">Deactive</option>
      				          </select>
                      </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditLeave">
                  <?php if(isset($product[0]->id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-product.js" ></script>
