<style type="text/css">
.start_date .ui-datepicker-calendar,.eom_month .ui-datepicker-calendar{
    display: none;
}
</style>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($rewardEdit[0]->leave_id)){ echo "Edit Employee Of the Month";} else{echo "Add Employee Of the Month";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addReward', 'id' => 'addReward', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($rewardEdit[0]->id)){
			  	echo form_open('superadmin/rewards/addeom2/'.$rewardEdit[0]->id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/rewards/addeom2',$form_attributes);
			  }
			  ?>

        <div class="form-group">
                <label  class="col-lg-2 control-label">User Name <span class="red">*</span></label>
                <div class="col-lg-6">

                  <select class="form-control" id="user_id" name="user_id" placeholder="User Name ">
                    <option value="">Select User</option>
                    <?php  foreach($users as $u): ?>
                    <option value="<?php echo $u->user_id; ?>"><?php echo $u->first_name . ' ' . $u->last_name; ?> (<?php echo $u->department_name; ?>)</option>
                  <?php endforeach; ?>
                  </select>
                </div>
        </div>

        <div class="form-group">
                <label  class="col-lg-2 control-label">Reward Point <span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="reward_point" name="reward_point" placeholder="Reward Point "  value="<?php  if(isset($rewardEdit[0]->point)){ echo $rewardEdit[0]->point;} ?>">
                </div>
        </div>

        <div class="form-group ">
                <label for="project_id" class="control-label col-lg-2">Month<span class="red">*</span></label>
                <div class="col-lg-6">
                  <input type="text" name="eom_month" id="eom_month" class="form-control" data-date-format="yyyy-MM-dd">
                </div>
              </div>

              <div class="form-group">
                <label  class="col-lg-2 control-label">Description <span class="red">*</span></label>
                <div class="col-lg-6">
                  <textarea class="form-control" id="description" name="description"   rows="3" cols="40" placeholder="Reward Description"><?php if(isset($rewardEdit[0]->description)){ echo $rewardEdit[0]->description;} ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditLeave">
                  <?php if(isset($rewardEdit[0]->id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-addeom.js" ></script>
<script>
$(document).ready(function(){
  $('#eom_month').datepicker({
      autoClose:true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat: 'MM-yy',
      minDate: '-1y',
      maxDate: 'now',
      viewMode: "months",
      minViewMode: "months",
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').addClass(this.id);
        $('#ui-datepicker-div').removeClass('select_year');
      },
      onClose: function(dateText, inst) {
        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
      }
    });
});
</script>
