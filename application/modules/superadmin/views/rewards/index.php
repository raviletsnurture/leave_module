<?php
	$user = getAllUsers();
	$userId = $this->session->userdata('sLogin_session');
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Send Notification
					</header>

					<?php if($this->session->flashdata('error')){?>
						<div class="alert alert-block alert-danger fade in">
							<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
							<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
						</div>
					<?php } ?>
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success fade in">
							<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
							<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
						</div>
					<?php } ?>

					<div class="panel-body">
						<div class="form">
							<?php
								$form_attributes = array('name' => 'addAnnouncement', 'id' => 'addNotification', 'autocomplete' => 'off', "class"=>"commonForm cmxform form-horizontal tasi-form");
								echo form_open(base_url().'superadmin/notification/addNotification', $form_attributes);
							?>
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-lg-2 control-label" for="description"> Notification Message<span class="red">*</span></label>
										<div class="col-lg-8">
											<textarea class="form-control" id="description" name="description" rows="5" cols="40" maxlength="800" required></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label col-lg-2">Send Notification To:</label>
										<div class="col-lg-10">
											<label class="checkbox-inline">
												<input name="web" id="web" value="1" type="checkbox"> Web
											</label>
											<label class="checkbox-inline">
												<input name="android" id="android" value="1" type="checkbox"> Android
											</label>
											<label class="checkbox-inline">
												<input name="ios" id="ios" value="1" type="checkbox"> iOS
											</label>
										</div>
									</div>

									<div class="form-group">
										<div class="col-lg-offset-2 col-lg-10">
											<button class="btn btn-danger" type="submit" name="addFeedback">Submit Notification</button>
											<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
