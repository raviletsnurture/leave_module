<?php
$country = getAllCountry();
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($stateEdit[0]->state_id)){ echo "Edit State";} else{echo "Add State";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php 
			  $form_attributes = array('name' => 'addState', 'id' => 'addState', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($stateEdit[0]->state_id)){
			  	echo form_open('superadmin/state/add/'.$stateEdit[0]->state_id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/state/add',$form_attributes);
			  }
			  ?>
			  
			  <div class="form-group ">
                <label for="country_id" class="control-label col-lg-2">Choose Project<span class="red">*</span></label>
                <div class="col-lg-10">
                  <select name="country_id" id="country_id" class="form-control">
                    <option value="">Select Country</option>
                    <?php foreach($country as $row){?>
                    <option value="<?php echo $row->country_id;?>" <?php $value1 = (isset($stateEdit[0]->country_id)) ? $stateEdit[0]->country_id : 0; echo selectedVal($value1,$row->country_id)?>><?php echo $row->short_name;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  
              <div class="form-group ">
                <label for="firstName" class="control-label col-lg-2">State Name <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="state_name" name="state_name" type="text"  value="<?php if(isset($stateEdit[0]->state_name)){ echo $stateEdit[0]->state_name;} ?>"/>
                </div>
              </div>
               <div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($stateEdit[0]->status)? selectedVal($stateEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($stateEdit[0]->status)? selectedVal($stateEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditDepart">
                  <?php if(isset($stateEdit[0]->state_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-state.js" ></script> 
