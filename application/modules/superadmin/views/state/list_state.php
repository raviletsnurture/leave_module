<section id="main-content">
  <section class="wrapper site-min-height">
     <!--<div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row mbot30">
          <div class="col-lg-2">
              <div class="btn-group">
                <span class="my-span-head">Category</span>	
              </div>
          </div>
          <div class="col-lg-2 pull-right">
              <div class="btn-group pull-right">
                <a href="<?php echo base_url()?>superadmin/category/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
          </div>          
        </div>
      </div>-->
    <!-- page start-->
    <?php //$this->load->view('list_header'); ?>
    <section class="panel">
      <header class="panel-heading"> All states </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/state/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="row">
          <div class="col-lg-5">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="">
                <div class="form-group ">                
                  <div>
                    <select name="perpage" class="form-control perPage" onchange="changePerPage(this.value);"> 
                       <option value="25" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '25'){echo "selected";} ?>>25 per page</option>
                      <option value="50" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '50'){echo "selected";} ?>>50 per page</option>
                      <option value="100" <?php if($this->input->get('perpage') && $this->input->get('perpage') == '100'){echo "selected";} ?>>100 per page</option>
                    </select> 
                    
                  </div>
              </div>
              </div>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="dataTables_filter col-lg-7" id="editable-sample_filter">
              <form method="get" class="pull-right">
                  <div class="form-group">
                      <label for="exampleInputPassword2" class="sr-only">Password</label>
                      <input type="text" placeholder="Search" name="keyword" class="form-control">
                  </div>
                  <input type="submit" value="Search" class="btn-cust btn btn-success" >                 
              </form>
            </div>
          </div>
        </div>-->
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">         
          
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

          
          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
				<th>Country Name</th>
                <th>State Name</th>
                <th>Status</th>      
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($state as $row){ ?>
              <tr>
				<td><?php echo $row->short_name; ?></td>
                <td><?php echo $row->state_name;?></td>
                <td><?php echo $row->status; ?></td>
                <td>
                  <a href="<?php echo base_url()?>superadmin/state/edit/<?php echo $row->state_id;?>" >
                  <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;state" title=""><i class="fa fa-pencil"></i></button>
                  </a> 
                  <a href="<?php echo base_url()?>superadmin/state/delete/<?php echo $row->state_id;?>" class="deleteRec">
                  <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;state"><i class="fa fa-trash-o "></i></button>
                  </a>
                  </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
        
          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
    </section>
    <!-- page end--> 
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
              
    $('#example').dataTable({
      "iDisplayLength": 25,        
        "bFilter": true,
        "bInfo": false,
        "bPaginate": true,
         "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],         
    });

			  $(".dataTables_filter input").addClass('form-control');
		  } );		  
      </script>
       <style type="text/css">
.dataTables_filter {
  width: 30%;    
}
.span6{
  width: 50%;
  float: left;
}

</style>