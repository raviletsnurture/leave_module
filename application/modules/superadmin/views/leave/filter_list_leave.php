<?php 
$user = getAllUsers();
$department = getAllDepartment();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();
$i=1; 
if(!empty($leave)){
 foreach($leave as $row){ ?>
    <tr class="leaveID<?php echo $row->leave_id; ?>">
      <td><?php echo $row->leave_id; ?></td>
      <td class="email" title="<?php echo $row->email; ?>" ><?php echo $row->first_name.' '.$row->last_name;?></td>
    	<td><?php echo $row->department_name;?></td>
    	<td class="request_type" title="<?php echo $row->leave_reason; ?>"><?php echo $row->ltype; ?></td>
      <td class="leave_start_date">
      <?php echo date("d-m-Y", strtotime($row->leave_start_date));?></td>
    	<td class="leave_end_date"><?php echo date("d-m-Y", strtotime($row->leave_end_date)); ?></td>
    	<td><?php echo date("d-m-Y", strtotime($row->leave_created)); ?></td>
    	<td>
    	<?php if($row->is_cancelled == 1) { echo "Cancelled - self"; } else {?>
    	<select id="leave_status" name="leave_status" onchange="stat(this)">
    		<option value="">--Select--</option>
    		<?php foreach($leave_status as $bow){ ?>
    		<option value="<?php echo $bow->leave_status_id; ?>,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>" <?php if($bow->leave_status_id == $row->leave_status) { ?>selected="selected"<?php } ?>><?php echo $bow->leave_status;?></option>
    		<?php } ?>
    	</select>
    	<?php } //echo $row->lstatus; ?>
    </td>
    <td><?php echo $row->af.' '.$row->al; ?></td>
    <td><?php if($row->status == '' && $row->lstatus != 'Pending') { ?>
    	<select name="status" id="status" onchange="ff(this)">
    		<option value="">Select Status</option>
    		<option value="Paid,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>">Paid</option>
    		<option value="Unpaid,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>">Unpaid</option>
    	</select>
    	<?php } else { echo $row->status; } ?></td>
      <td class="action">
        <a href="<?php echo base_url()?>superadmin/leave/edit/<?php echo $row->leave_id;?>" >
        <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Leave" title=""><i class="fa fa-pencil"></i></button>
        </a>
        <a href="<?php echo base_url()?>superadmin/leave/delete/<?php echo $row->leave_id;?>" class="deleteRec">
        <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Leave"><i class="fa fa-trash-o "></i></button>
        </a>
        <button class="btn btn-<?php if($row->comment !=="" ){echo "danger";}else{echo "info";} ?> btn-xs btncomment"
    		 id="<?php echo $row->leave_id; ?>" data-toggle="modal" data-comment="<?php echo $row->comment; ?>" data-target="#myComment">
    			<i class="fa fa-comment"></i></button>
        </td>
    </tr>
<?php $i++; }}else{
        echo "No Data Found.";
    }?>