<?php
$user = getAllUsers();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();
?>

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($leaveEdit[0]->leave_id)){ echo "Edit Leave";} else{echo "Add Leave";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php
			  $form_attributes = array('name' => 'addLeave', 'id' => 'addLeave', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($leaveEdit[0]->leave_id)){
			  	echo form_open('superadmin/leave/add/'.$leaveEdit[0]->leave_id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/leave/add',$form_attributes);
			  }
			  ?>
              <div class="form-group ">
                <label for="firstName" class="control-label col-lg-2">User <span class="red">*</span></label>
                <div class="col-lg-6">
					<select name="user_id" id="user_id" class="form-control">
						<option value="">Select user</option>
						<?php foreach($user as $dd) { ?>
						<option value="<?php echo $dd->user_id; ?>" <?php $value1 = (isset($leaveEdit[0]->user_id)) ? $leaveEdit[0]->user_id : 0; echo selectedVal($value1,$dd->user_id); ?>><?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?></option>
						<?php } ?>
					</select>

                </div>
              </div>

			  <div class="form-group ">
                <label for="project_id" class="control-label col-lg-2">Leave Type<span class="red">*</span></label>
                <div class="col-lg-6  ">
                  <select name="leave_type" id="leave_type" class="form-control">
                    <option value="">Select Request type</option>

						<?php foreach($leave_type as $dd1) { ?>
						<option value="<?php echo $dd1->leave_type_id; ?>" <?php $value1 = (isset($leaveEdit[0]->leave_type)) ? $leaveEdit[0]->leave_type : 0; echo selectedVal($value1,$dd1->leave_type_id); ?>><?php echo $dd1->leave_type; ?></option>
						<?php } ?>

				  </select>
                </div>
              </div>

			  <div class="form-group">
                <label  class="col-lg-2 control-label">Start Date <span class="red">*</span></label>
                <div class="col-lg-2">
                  <input type="text" class="form-control" id="leave_start_date" name="leave_start_date" data-date-format="yyyy-mm-dd" placeholder=" "  value="<?php  if(isset($leaveEdit[0]->leave_start_date)){ echo $leaveEdit[0]->leave_start_date;} ?>">
                </div>

                <label  class="col-lg-2 control-label">End Date <span class="red">*</span></label>
                <div class="col-lg-2">
                  <input type="text" class="form-control"  id="leave_end_date" name="leave_end_date" data-date-format="yyyy-mm-dd" placeholder=" "  value="<?php  if(isset($leaveEdit[0]->leave_end_date)){ echo $leaveEdit[0]->leave_end_date;} ?>">
                </div>
        </div>

			  <div class="form-group ">
                <label for="project_id" class="control-label col-lg-2">Request Status<span class="red">*</span></label>
                <div class="col-lg-6">
                  <select name="leave_status" id="leave_status" class="form-control">
                    <option value="">Select Request Status</option>

						<?php foreach($leave_status as $dd) { ?>
						<option value="<?php echo $dd->leave_status_id; ?>" <?php $value1 = (isset($leaveEdit[0]->leave_status)) ? $leaveEdit[0]->leave_status : 0; echo selectedVal($value1,$dd->leave_status_id); ?>><?php echo $dd->leave_status; ?></option>
						<?php } ?>

				  </select>
                </div>
              </div>

              <div class="form-group">
                <label  class="col-lg-2 control-label">Reason <span class="red">*</span></label>
                <div class="col-lg-6">
                  <textarea class="form-control" id="leave_reason" name="leave_reason"   rows="3" cols="40" placeholder="reason for leave"><?php if(isset($leaveEdit[0]->leave_reason)){ echo $leaveEdit[0]->leave_reason;} ?></textarea>
                </div>
              </div>
               <!--<div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($leaveEdit[0]->status)? selectedVal($leaveEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>-->
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditLeave">
                  <?php if(isset($leaveEdit[0]->leave_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<style media="screen">
.table-condensed > tbody > tr > td:first-of-type{ color:red;}
.table-condensed > tbody > tr > td:last-of-type{ color:red;}
</style>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-leave.js" ></script>
