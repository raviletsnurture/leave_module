<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<?php
$user = getAllUsers();
$department = getAllDepartment();
$leave_status = getAllLeaveStatus();
$leave_type = getAllLeaveType();

?>
<style type="text/css">
.dataTables_filter {
     display: none;
}
.dataTables_wrapper .dataTables_info{
  float: none !important;
}
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
tfoot {
    display: table-header-group;
}
</style>

<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">
      <header class="panel-heading"> All Request </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/leave/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New Request <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
          <!-- <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
            <div class="input-group date form_datetime-component">
                <input type="text" name="startMonth" id="startMonth" class="form-control" size="36" required placeholder="Month Of Leave">
            </div>
            </div>
          </div> -->
        </div>
        <!-- Add Graph for Leave Report -->
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
        
          <!-- <select id="engines">
            <option value="">Select Users</option>
            <?php foreach($user as $row){?>
            <option value="<?php echo $row->first_name . ' ' . $row->last_name; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
            <?php } ?>
          </select>
          <select id="engines3">
              <option value="">Request Status</option>
              <?php foreach($leave_status as $bow){?>
              <option value="<?php echo $bow->leave_status; ?>"><?php echo $bow->leave_status;?></option>
              <?php } ?>
          </select> -->


          <!-- Applied data filter -->
          <div class="row">
               <div class="col-md-2">
                   <label for="">Filter Applied Date</label>
               </div>
               <div class="col-md-3">
                   <input type="text" id="appliedStartDate" name="appliedStartDate" class="form-control date-range-filter" placeholder="From" data-date-format="dd-mm-yyyy">
               </div>
               <div class="col-md-3">
                   <input type="text" id="appliedEndDate" name="appliedEndDate" class="form-control date-range-filter" placeholder="To" data-date-format="dd-mm-yyyy">
               </div>
               <div class="col-md-3">
               <a class="btn btn-info" href="javascript:;" id="Filter_leave">Submit</a>
               <a class="btn btn-info" href="javascript:;" id="clearFilter">Clear filter</a>
               </div>
          </div>
          
          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th>No.</th>
                <th>Full Name</th>
		            <th>Department</th>
        				<th>Request Type</th>
                <th style="width:10%">Start Date</th>
        				<th style="width:10%">End Date</th>
                <th style="width:10%">Applied Date</th>
        				<th>Request Status</th>
        				<th>Approved/Rejected By</th>
        				<th>Paid/Unpaid</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                  <th>No.</th>
                  <th>
                     <input type="text" placeholder="Full Name" style="width: 105px; height: 35px;" />
                  </th>
                  <th >
                    <select id="engines1"  style="width:100%;" data-live-search="true">
                      <option value="">Department</option>
                      <?php foreach($department as $row1){?>
                      <option value="<?php echo $row1->department_name; ?>"><?php echo $row1->department_name;?></option>
                      <?php } ?>
                    </select>
                  </th>
                  <th style="width:10%;">
                    <select id="engines2" style="width:100%;">
                        <option value="">Request Type</option>
                        <?php foreach($leave_type as $bo){?>
                        <option value="<?php echo $bo->leave_type; ?>"><?php echo $bo->leave_type; ?></option>
                        <?php } ?>
                    </select>
                  </th>
                  <th style="width:10%">Start Date</th>
                  <th style="width:10%">End Date</th>
                  <th style="width:10%">Applied Date</th>
                  <th>Request Status</th>
                  <th><input type="text" placeholder="Approved/Rejected By" style="width: 105px; height: 35px;" /></th>
                  <th>Paid/Unpaid</th>
                  <th>Action</th>
                </tr>
            </tfoot>
            <tbody id="table_body">
              <?php $i=1; foreach($leave as $row){ ?>
              <tr class="leaveID<?php echo $row->leave_id; ?>">
                <td><?php echo $row->leave_id; ?></td>
                <td class="email" title="<?php echo $row->email; ?>" ><?php echo $row->first_name.' '.$row->last_name;?></td>
        				<td><?php echo $row->department_name;?></td>
        				<td class="request_type" title="<?php echo $row->leave_reason; ?>"><?php echo $row->ltype; ?></td>
                <td class="leave_start_date">
                <?php echo date("d-m-Y", strtotime($row->leave_start_date));?></td>
        				<td class="leave_end_date"><?php echo date("d-m-Y", strtotime($row->leave_end_date)); ?></td>
        				<td><?php echo date("d-m-Y", strtotime($row->leave_created)); ?></td>
        				<td>
      					<?php if($row->is_cancelled == 1) { echo "Cancelled - self"; } else {?>
      					<select id="leave_status" name="leave_status" onchange="stat(this)">
      						<option value="">--Select--</option>
      						<?php foreach($leave_status as $bow){ ?>
      						<option value="<?php echo $bow->leave_status_id; ?>,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>" <?php if($bow->leave_status_id == $row->leave_status) { ?>selected="selected"<?php } ?>><?php echo $bow->leave_status;?></option>
      						<?php } ?>
      					</select>
      					<?php } //echo $row->lstatus; ?>
      				</td>
      				<td><?php echo $row->af.' '.$row->al; ?></td>
      				<td><?php if($row->status == '' && $row->lstatus != 'Pending') { ?>
      					<select name="status" id="status" onchange="ff(this)">
      						<option value="">Select Status</option>
      						<option value="Paid,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>">Paid</option>
      						<option value="Unpaid,<?php echo $row->leave_id; ?>,<?php echo $row->user_id; ?>">Unpaid</option>
      					</select>
      					<?php } else { echo $row->status; } ?></td>
                <td class="action">
                  <a href="<?php echo base_url()?>superadmin/leave/edit/<?php echo $row->leave_id;?>" >
                  <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Leave" title=""><i class="fa fa-pencil"></i></button>
                  </a>
                  <a href="<?php echo base_url()?>superadmin/leave/delete/<?php echo $row->leave_id;?>" class="deleteRec">
                  <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Leave"><i class="fa fa-trash-o "></i></button>
                  </a>
                  <button class="btn btn-<?php if($row->comment !=="" ){echo "danger";}else{echo "info";} ?> btn-xs btncomment"
      						 id="<?php echo $row->leave_id; ?>" data-toggle="modal" data-comment="<?php echo $row->comment; ?>" data-target="#myComment">
      							<i class="fa fa-comment"></i></button>
                  </td>
              </tr>
              <?php $i++; }?>
            </tbody>
          </table>
          <?php if(isset($links)){echo $links;} ?>
        </div>
      </div>
    </section>
    <!-- page end-->
  </section>
</section>

<!-- Comment Modal -->
<div id="myComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <!-- <div class="modal fade" id="myComment" role="dialog"> -->
	<input type="hidden" name="Leave_id" id="leave_id" class="getleave" value="">
	<!-- <span class="alert"></spanv> -->
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Comment</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<textarea name="leave_comment" id="leave_comment" class="form-control leave_comment" ></textarea>
				</div>
				<span class="alert-danger"></span>
			</div>
			<div class="modal-footer">
				<div class="alert alert-success alert-dismissable fade in">
					Comment added successfully!
				</div>
				<button type="button" class="btn btn-success" id="commentadd">Submit</button>
			</div>
		</div>
	</div>
</div>

<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<!-- <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$('#leave_start').datepicker({
   	autoclose: true,
	});
</script>



<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />

<link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.dataTables.css" />
<script src="<?php echo base_url()?>assets/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" charset="utf-8">


$('#startMonth,#endMonth').datepicker({
    //endDate: '-1m',
		//startDate: '-1m',
    autoclose: true,
		format: "yyyy-mm-dd",
    startView: "months",
    minViewMode: "months",
});

function ff(sel){
	var ff = sel.value.split(",");
	if(ff == ''){
		alert("Please Select an Option");
		location.reload();
		//return false;
	}else{
		var r = confirm("Are You Sure for this operation");
		if (r == true){
			$.ajax({
			 url: '<?php echo base_url(); ?>superadmin/leave/updatestaus/',
			 data: { leave_id: ff[1], status: ff[0], user_id: ff[2]},
			 dataType: 'html',
			 type: 'POST',
			 success: function(data){
				 //$('.modal-body').html(data);
				 //$('#myModal').modal('show');
				 alert("Leave Status Has Been Changed");
				 //location.reload();
				 }
			});
		}
	 }
}


function stat(sel){
	var ff = sel.value.split(",");
	if(ff == ''){
		alert("Please Select an Option");
		location.reload();
	}else{
		var r = confirm("Are You Sure for this operation");
		if (r == true){
			$.ajax({
			 url: '<?php echo base_url(); ?>superadmin/leave/up_staus/',
			 data: { leave_id: ff[1], status: ff[0], userid: ff[2]},
			 dataType: 'html',
			 type: 'POST',
			 success: function(data){
				 //$('.modal-body').html(data);
				 //$('#myModal').modal('show');
				 alert("Leave Status Has Been Changed");
				 location.reload();
				 }
			});
		}
	 }
}


$(document).ready(function() {
  $(function() {
    $('#start_datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    $('#applied_datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    $('#end_datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();    
  });

  var dt =  $('#example').DataTable({
    "iDisplayLength": 25,
    "pagingType": "full_numbers",
    "bPaginate": true,
    "bInfo" : true,
    "destroy": true,   
    "oSearch": { "bSmart": false, "bRegex": true },
    /*"aoColumnDefs": [{
        'bSortable': false, 'aTargets': [4]
    }],    */
    "ordering": false,
    "lengthMenu": [[10, 25, 50, -1],[10, 25, 50, "All"]],
    "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
    "initComplete": function(settings, json) {        
        $("table tfoot select").on('change', function(){
             dt
              .column( $(this).parent().index()+':visible' )
              .search( this.value )
              .draw();
         });
     }
   });

   // Apply the search
    dt.columns().every( function () {      
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        });
    }); 

    $("#appliedStartDate").datepicker({ dateFormat: 'dd-mm-yy',   changeMonth: true, changeYear: true });
    $("#appliedEndDate").datepicker({  dateFormat: 'dd-mm-yy',  changeMonth: true, changeYear: true });

    /*$.fn.dataTable.ext.search.push(
      function (settings, data, dataIndex) {
         var min1 = $('#appliedStartDate').datepicker("getDate");
         var max1 = $('#appliedEndDate').datepicker("getDate");
         
         var min = new Date(min1).getTime();
         var max = new Date(max1).getTime();

         var startDate = new Date(data[4]).getTime();
         var endDate = new Date(data[5]).getTime();
         
         if (min == null && max == null) { return true; }
         if (min == null && endDate <= max) { return true;}
         if (max == null && startDate >= min) { return true;}
         if (endDate <= max && startDate >= min) { return true; }
         return false;
      }
    );*/

    

  // $('select#engines').change( function() {
	// 	dt.fnFilter( $(this).val(), 0 );
	// });
	// $('select#engines1').change( function() {
	// 	dt.fnFilter( $(this).val(), 1 );
	// });
	// $('select#engines2').change( function() {
	// 	dt.fnFilter( $(this).val(), 2 );
	// });
	// $('select#engines3').change( function() {
	// 	dt.fnFilter( $(this).val(), 5 );
	// });
  //$(".dataTables_filter input").addClass('form-control');


  /*$("#startMonth").on("change",function(){
    //dt.destroy();
		var month = $(this).val();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>superadmin/leave/leaveListByMonth',
			data : { 'month' : month},
			beforeSend: function(){
			},
			success:function(data){
        $("#example tbody").html(data);
        $('#example').DataTable().ajax.reload();
			}
 		});
	});*/

  $('#clearFilter').click(function() {
    $('#example').DataTable().destroy();
    $('#appliedStartDate, #appliedEndDate').val('');
    var appliedStartDate = $("#appliedStartDate").val();
    var appliedEndDate = $("#appliedEndDate").val();
    reload_datatable_ajax(appliedStartDate,appliedEndDate);
  });
  $( "#Filter_leave" ).click(function() {   
    var appliedStartDate = $("#appliedStartDate").val();
    var appliedEndDate = $("#appliedEndDate").val(); 
    if((appliedStartDate != '') && (appliedEndDate != ''))
    {
      $('#example').DataTable().destroy();    
      reload_datatable_ajax(appliedStartDate,appliedEndDate);      
    }else{
      alert('Please select date');
      return false;
    }
    
    
  });
});

function reload_datatable_ajax(appliedStartDate,appliedEndDate) {
  $.ajax({
      type: 'POST',
      url: '<?php echo base_url()?>superadmin/leave/leave_filter',
      data : { 'appliedStartDate' : appliedStartDate,'appliedEndDate':appliedEndDate},
      dataType: "html",
      beforeSend: function(){
        $('.ajaxLoader').show();
      },
      success:function(data){
        $('.ajaxLoader').hide();
        var json = $.parseJSON(data);        
        if(json['table'] == 'No Data Found.'){
                  alert("No Data Found.");
                  return false;
        }else{      
        $('#table_body').html(json['table']);
        dt1 = $('#example').DataTable({
            "iDisplayLength": 25,
            "pagingType": "full_numbers",
            "bPaginate": true,
            "bInfo" : true,
            "destroy": true,   
            "oSearch": { "bSmart": false, "bRegex": true },
            "ordering": false,
            "lengthMenu": [[10, 25, 50, -1],[10, 25, 50, "All"]],
            "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
            "initComplete": function(settings, json) {        
                $("table tfoot select").on('change', function(){
                     dt
                      .column( $(this).parent().index()+':visible' )
                      .search( this.value )
                      .draw();
                 });
             }
        });
        dt1.columns().every( function () {      
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            });
        }); 
      }
      }
    });
}



// Setup - add a text input to each footer cell
  $('#example tfoot th').each( function () {
      var title = $(this).text();
      if (title === "Start Date") {
        $(this).html( '<input type="text" id="start_datepicker" placeholder="'+title+'" />' );
      }else if(title === "End Date"){
        $(this).html( '<input type="text" id="end_datepicker" placeholder="'+title+'" />' );  
      }else if(title === "Applied Date"){
        $(this).html( '<input type="text" id="applied_datepicker" placeholder="'+title+'" />' );
      }
  });

$("body").on("click",".btncomment",function(){
	var comment = $(this).attr("data-comment");
  var leave_id = $(this).attr("id");
	$(".leave_comment").val('');
	$(".leave_comment").val(comment);
	$("#leave_id").val(leave_id);
	$(".alert-success").css('display','none');
  $("#commentadd").removeClass('disabled');
	$("#commentadd").html("Submit");
	$('.alert-danger').css('display','none')
});

$(document).ready(function() {
	$('#commentadd').click(function(){

		var leavdeid = $('#leave_id').val();
		var comment = $('#leave_comment').val();
    var leave_start_date = $(".leave_start_date span").html();
		var leave_end_date  = $(".leave_end_date span").html();
		var request_type = $(".request_type").html();
		var email = $('.email').attr('title');

		if(comment == ""){
			$(".alert-danger").css({'display':'inline-block','padding':'10px','width':'100%'});
			$(".alert-danger").html('Comment required');
    }else{
			$(".alert-danger").css('display','none');
      $("#commentadd").html("<i class='fa fa-circle-o-notch fa-spin'></i> Loading");
			$("#commentadd").addClass('disabled');

		  var dataString = 'Leave_ID='+ leavdeid + '&comment_Add='+ comment + '&Leave_Start_Date='+ leave_start_date + '&Leave_End_Date='+ leave_end_date + '&Request_Type='+ request_type + '&Email='+ email;
      $.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>leaves/commnetAdd",
			data: dataString,
			cache: false,
			success: function(result){
				if(result == 1){
          $(".alert-success").css({"display": "inline-block", "float": "left", "margin-bottom": "0px"});
					$(".action #"+leavdeid).attr('data-comment',comment);
					setTimeout(function() {$('#myComment').modal('hide');}, 500);
				}else{
				}
			}
			});
		}
	});
});
</script>
