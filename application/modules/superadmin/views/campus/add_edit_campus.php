<?php $country = getAllSelectedCountry(); ?>
<section id="main-content">
   <section class="wrapper">
      <div class="row">
         <div class="col-lg-12">
            <section class="panel">
               <header class="panel-heading">
                  <?php if(isset($campusdata[0]->campus_id)){ echo "Edit Campus";} else{echo "Add Campus";}?>
               </header>
               <?php if($this->session->flashdata('error')){?>
               <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
               </div>
               <?php } ?>
               <?php if($this->session->flashdata('success')){?>
               <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
               </div>
               <?php }?>
               <div class="panel-body">
                  <div class="form">
                     <?php
                        $form_attributes = array('name' => 'addCampus', 'id' => 'addCampus', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form","enctype"=>"multipart/form-data" );
                        if(isset($campusdata[0]->campus_id)){
                        	echo form_open(base_url().'superadmin/campus/add/'.$campusdata[0]->campus_id,$form_attributes);
                        }else{
                          echo form_open(base_url().'superadmin/campus/add',$form_attributes);
                        }
                        ?>

                        <div class="form-group">
                          <div class="col-lg-6">
                            <label for="college_name">College Name <span class="red">*</span></label>
                           <input class="form-control" id="college_name" maxlength="25" name="college_name" type="text"  value="<?php if(isset($campusdata[0]->college_name)){ echo $campusdata[0]->college_name;} ?>" placeholder="College Name"/>
                          </div>

                          <div class="col-lg-6">
                            <label for="university_name">University Name <span class="red">*</span></label>
                           <input class="form-control" id="university_name" maxlength="25" name="university_name" type="text"  value="<?php if(isset($campusdata[0]->university_name)){ echo $campusdata[0]->university_name;} ?>" placeholder="University Name"/>
                          </div>
                        </div>
                         <div class="form-group">
                            <div class="col-lg-6">
                              <label for="email">Official Email <span class="red">*</span></label>
                               <input class="form-control" id="email" name="email" type="text"  value="<?php if(isset($campusdata[0]->email)){ echo $campusdata[0]->email;} ?>" placeholder="Official Email"/>
                            </div>

                            <div class="col-lg-6">
                              <label for="alternate_email">Alternate Email </label>
                               <input class="form-control" id="alternate_email" name="alternate_email" type="text"  value="<?php if(isset($campusdata[0]->alternate_email)){ echo $campusdata[0]->alternate_email;} ?>" placeholder="Alternate Email"/>
                            </div>
                         </div>
                         <div class="form-group">
                            <div class="col-lg-6">
                              <label for="mobile">Mobile Number <span class="red">*</span></label>
                               <input class="form-control" id="mobile" maxlength="10" name="mobile" type="text"  value="<?php if(isset($campusdata[0]->mobile)){ echo $campusdata[0]->mobile;} ?>" placeholder="Mobile Number"/>
                            </div>

                            <div class="col-lg-6">
                              <label for="landline">Emergency Number</label>
                               <input class="form-control" id="emergency_number" maxlength="10" name="emergency_number" type="text"  value="<?php if(isset($campusdata[0]->emergency_number)){ echo $campusdata[0]->emergency_number;} ?>" placeholder="Emergency Number"/>
                            </div>
                         </div>
                         <div class="form-group">
                            <div class="col-lg-6">
                              <label for="landline_phone1">Landline phone1s</label>
                               <input class="form-control" id="landline_phone1" maxlength="10" name="landline_phone1" type="text"  value="<?php if(isset($campusdata[0]->landline_phone1)){ echo $campusdata[0]->landline_phone1;} ?>" placeholder="Landline Phone1"/>
                            </div>

                            <div class="col-lg-6">
                              <label for="landline_phone2">Landline Phone2</label>
                               <input class="form-control" id="landline_phone2" maxlength="10" name="landline_phone2" type="text"  value="<?php if(isset($campusdata[0]->landline_phone2)){ echo $campusdata[0]->landline_phone2;} ?>" placeholder="Landline Phone2"/>
                            </div>
                         </div>
                         <div class="form-group">
                            <div class="col-lg-6">
                              <label for="contact_person_name_1">Contact Person Name 1<span class="red">*</span></label>
                               <input class="form-control" id="contact_person_name_1" maxlength="25" name="contact_person_name_1" type="text"  value="<?php if(isset($campusdata[0]->contact_person_name_1)){ echo $campusdata[0]->contact_person_name_1;} ?>" placeholder=" Contact Person Name 1"/>
                            </div>

                            <div class="col-lg-6">
                              <label for="contact_person_name_2">Contact Person Name 2</label>
                               <input class="form-control" id="contact_person_name_2" maxlength="25" name="contact_person_name_2" type="text"  value="<?php if(isset($campusdata[0]->contact_person_name_2)){ echo $campusdata[0]->contact_person_name_2;} ?>" placeholder="Contact Person Name 2"/>
                            </div>
                         </div>
                     
                         
                         <div class="form-group">
                            <div class="col-lg-12">
                              <label for="fax">Fax </label>
                               <input class="form-control" id="fax" maxlength="15" name="fax" type="text"  value="<?php if(isset($campusdata[0]->fax)){ echo $campusdata[0]->fax;} ?>" placeholder="Fax"/>
                            </div>
                         </div>
                         
                         <div class="form-group">
                            <div class="col-lg-12">
                              <label for="street" >street<span class="red">*</span></label>
                               <textarea class="form-control" id="street" name="street" rows="3" cols="40" placeholder="Street"><?php if(isset($campusdata[0]->street)){ echo $campusdata[0]->street;} ?></textarea>
                            </div>
                         </div>                     
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="countryid">Country <span class="red">*</span></label>
                           <select name="country_id" id="country_id" class="form-control m-bot15">
                              <?php foreach($country as $row2){ ?>
                              <option value="<?php echo $row2->country_id; ?>" <?php $value1 = (isset($campusdata[0]->country_id)) ? $campusdata[0]->country_id : 0; echo selectedVal($value1,$row2->country_id)?>> <?php echo $row2->short_name; ?></option>
                              <?php } ?>
                           </select>
                        </div>

                        <div class="col-lg-6">
                          <label for="state_id">State <span class="red">*</span></label>
                           <select name="state_id" id="state_id" class="form-control">
                              <option value="">Select State</option>
                              <?php if(isset($campusdata[0]->state_id)) { ?>
                              <?php foreach($state as $dd){?>
                              <option value="<?php echo $dd->state_id;?>" <?php $value1 = (isset($campusdata[0]->state_id)) ? $campusdata[0]->state_id : 0; echo selectedVal($value1,$dd->state_id)?>><?php echo $dd->state_name;?></option>
                              <?php } ?>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="state_id">City <span class="red">*</span></label>
                           <select name="city_id" id="city_id" class="form-control">
                              <option value="">Select City</option>
                              <?php if(isset($campusdata[0]->city_id)) { ?>
                              <?php foreach($city as $dd1){?>
                              <option value="<?php echo $dd1->city_id;?>" <?php $value1 = (isset($campusdata[0]->city_id)) ? $campusdata[0]->city_id : 0; echo selectedVal($value1,$dd1->city_id)?>><?php echo $dd1->city_name;?></option>
                              <?php } ?>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-lg-6">
                          <label for="zipcode">Zipcode <span class="red">*</span></label>
                           <input class="form-control" id="zipcode" maxlength="10" name="zipcode" type="text"  value="<?php if(isset($campusdata[0]->zipcode)){ echo $campusdata[0]->zipcode;} ?>" placeholder="Zipcode"/>
                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="gender">Gender <span class="red">*</span></label>
                           <select name="gender" id="gender" class="form-control">
                              <option value="">Select Gender</option>
                              <option value="Male" <?php echo isset($campusdata[0]->gender)? selectedVal($campusdata[0]->gender,"Male"): '';?> >Male</option>
                              <option value="Female" <?php echo isset($campusdata[0]->gender)? selectedVal($campusdata[0]->gender,"Female"): '';?>>Female</option>
                           </select>
                        </div>
                        <div class="col-lg-6">
                          <label for="description" >Description</label>
                           <textarea class="form-control" id="description" name="description" rows="3" cols="40" placeholder="Description"><?php if(isset($campusdata[0]->description)){ echo $campusdata[0]->description;} ?></textarea>
                        </div>    
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="recruitment_status">Have We Done Recruitment in this college in Past? <span class="red">*</span></label>
                           <input type="radio" name="recruitment_status" value="yes" <?php echo isset($campusdata[0]->recruitment_status)? selectedChk($campusdata[0]->recruitment_status,"yes"): '';?>>Yes
                           <input type="radio" name="recruitment_status" value="no" <?php echo isset($campusdata[0]->recruitment_status)? selectedChk($campusdata[0]->recruitment_status,"no"): '';?> >No
                        </div>
                        <div class="col-lg-6">
                          <label for="tier" >Tier<span class="red">*</span></label>
                           <select name="tier" id="tier" class="form-control">
                              <option value="">Select Tier</option>
                              <option value="Tier1" <?php echo isset($campusdata[0]->tier)? selectedVal($campusdata[0]->tier,"Tier1"): '';?>>Tier1</option>
                              <option value="Tier2" <?php echo isset($campusdata[0]->tier)? selectedVal($campusdata[0]->tier,"Tier2"): '';?>>Tier2</option>
                              <option value="Tier3" <?php echo isset($campusdata[0]->tier)? selectedVal($campusdata[0]->tier,"Tier3"): '';?>>Tier3</option>
                           </select>
                        </div>    
                     </div>
                     <div class="form-group">                        
                        <div class="col-lg-6">
                          <label for="special_reason">Any Special Condition For Recruitment?</label>
                           <input class="form-control" id="special_reason" name="special_reason" type="text"  value="<?php if(isset($campusdata[0]->special_reason)){ echo $campusdata[0]->special_reason;} ?>" placeholder="Specify Reason"/>
                        </div>
                        <div class="col-lg-6">
                          <label for="other_information">Other Information</label>
                           <input class="form-control" id="other_information" name="other_information" type="text"  value="<?php if(isset($campusdata[0]->other_information)){ echo $campusdata[0]->other_information;} ?>" placeholder="Other Information"/>
                        </div>                        
                     </div>
                      <!-- <div class="form-group">   
                        <div class="col-lg-6">
                          <label for="catStatusFlag">Status</label>
                           <select name="status" id="status" class="form-control m-bot15">
                              <option value="Active" <?php //echo isset($campusdata[0]->status)? selectedVal($campusdata[0]->status,"Active"): '';?>>Active</option>
                              <option value="Inactive" <?php //echo isset($campusdata[0]->status)? selectedVal($campusdata[0]->status,"Inactive"): '';?> >Inactive</option>
                           </select>
                        </div>
                      </div>  --> 
                     <div class="form-group">
                        <div class="col-lg-2">
                             <button class="btn btn-danger" type="submit" name="addEditCampus">
                             <?php if(isset($campusdata[0]->campus_id)){ echo "Update";} else{echo "Submit";}?>
                             </button>
                             <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                        </div>
                     </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-campus.js" ></script>
<script>
 <?php if(isset($campusdata[0]->campus_id)){} else{?>
   var country_id = $('#country_id').val();
       $.ajax({
               url: '<?php echo base_url();?>superadmin/users/getStates',
               type: 'POST',
               data: {
                   country_id:country_id,
               },
               success:function(data){
                 $('#state_id').html(data);
               },
               error:function(){
                   alert('Data not Found')
               }
           }); 
    <?php } ?>   
   $('#country_id').change(function(){
       var country_id = $('#country_id').val();
       $.ajax({
               url: '<?php echo base_url();?>superadmin/users/getStates',
               type: 'POST',
               data: {
                   country_id:country_id,
               },
               success:function(data){
   				       $('#state_id').html(data);
               },
               error:function(){
                   alert('Data not Found')
               }
           });
    });
    $('#state_id').change(function(){
       var state_id = $('#state_id').val();
       $.ajax({
               url: '<?php echo base_url();?>superadmin/users/getCities',
               type: 'POST',
               data: {
                   state_id:state_id,
               },
               success:function(data){
   				        $('#city_id').html(data);
               },
               error:function(){
                   alert('Data not Found')
               }
           });
    });
    //edit profile image
    function readURL(input) {
        var old_profile_image = $('#old_profile_image').val();
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.old_profile_imageSub')
                    .attr('src', e.target.result)
                    .width(125)
                    .height(125);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
