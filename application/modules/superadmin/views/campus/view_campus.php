  <div class="row-fluid">
    <div class="span9">
      <div class="new-list">
        <div class="item panel" id="myprofile">
          <table class="table myprofile_tbl">              
            <tr>
              <td width="250"><b>College Name :</b></td>
              <td><?php echo $campus[0]->college_name;?></td>
            </tr>
              <td><b>University Name :</b></td>
              <td><?php echo ($campus[0]->university_name)?></td>
            </tr>
            <tr>
              <td><b>Email :</b></td>
              <td><?php echo ($campus[0]->email)?></td>
            </tr>
            <tr>
              <td><b>Alternate Email :</b></td>
              <td><?php echo ($campus[0]->alternate_email)?></td>
            </tr>
            <tr>
              <td><b>Mobile :</b></td>
              <td><?php echo ($campus[0]->mobile)?></td>
            </tr>
            <tr>
              <td><b>Emergency Number :</b></td>
              <td><?php echo ($campus[0]->emergency_number)?></td>
            </tr>
            <tr>
              <td><b>Contact Person Name 1 :</b></td>
              <td><?php echo ($campus[0]->contact_person_name_1)?></td>
            </tr>
            <tr>
              <td><b>Contact Person Name 2 :</b></td>
              <td><?php echo ($campus[0]->contact_person_name_2)?></td>
            </tr>
            
      			<tr>
              <td><b>Fax :</b></td>
              <td><?php echo ($campus[0]->fax)?></td>
            </tr>
      			<tr>
              <td><b>Street :</b></td>
              <td><?php echo $campus[0]->street;?></td>
            </tr>
            <tr>
              <td><b>Country :</b></td>
              <td><?php echo ($campus[0]->short_name)?></td>
            </tr>
            <tr>
              <td><b>State :</b></td>
              <td><?php echo ($campus[0]->state_name)?></td>
            </tr>
            <tr>
              <td><b>City :</b></td>
              <td><?php echo ($campus[0]->city_name)?></td>
            </tr>
            <tr>
              <td><b>zipcode :</b></td>
              <td><?php echo $campus[0]->zipcode;?></td>
            </tr>
      			<tr>
              <td><b>Gender :</b></td>
              <td><?php echo $campus[0]->gender;?></td>
            </tr>
      			<tr>
              <td><b>Description :</b></td>
              <td><?php echo $campus[0]->description;?></td>
            </tr>
      			<tr>
              <td><b>Any Special Condition For Recruitment? :</b></td>
              <td><?php echo $campus[0]->special_reason;?></td>
            </tr>
      			<tr>
              <td><b>Other Information :</b></td>
              <td><?php echo $campus[0]->other_information;?></td>
            </tr>            
          </table>
        </div>
      </div>
    </div>
  </div>
