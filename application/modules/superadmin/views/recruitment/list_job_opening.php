<?php $state = getAllState(); ?>
<style media="screen">
  table{
    width: 100% !important;
  }
  .editable-table .dataTables_filter{width: 23%;}
  tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
      }
  tfoot {
      display: table-header-group;
  }
  .dataTables_length{
    float: left;
  }  
</style>
<section id="main-content">
  <section class="wrapper site-min-height">    
    <section class="panel">
      <header class="panel-heading"> Campus List </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/campus/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <!-- Age inputs -->
          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th>College Name</th>
                <th>University Name</th>               
                <th>Email</th>
                <th>Mobile</th>
                <th>Contact Person</th>                
                <th>Gender</th>                
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>College Name</th>
                <th>University Name</th>               
                <th>Email</th>
                <th>Mobile</th>
                <th>Contact Person</th>                
                <th>Gender</th>                
                <th>Action</th>
              </tr>
            </tfoot>
            <tbody>
              <?php foreach($campus as $row){ ?>
              <tr>
                <td><?php echo $row->college_name;?></td>
                <td><?php echo $row->university_name; ?></td>
                <td><?php echo $row->email; ?></td>
                <td><?php echo $row->mobile; ?></td>
                        <td><?php echo $row->contact_person_name_1; ?></td>
                <td><?php echo $row->gender; ?></td>                
                <td>
                  <a href="<?php echo base_url()?>superadmin/campus/edit/<?php echo $row->campus_id;?>" >
                  <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Campus" title=""><i class="fa fa-pencil"></i></button>
                  </a>
                  <a href="<?php echo base_url()?>superadmin/campus/delete/<?php echo $row->campus_id;?>" class="deleteRec">
                  <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Campus"><i class="fa fa-trash-o "></i></button>
                  </a>
                  <a value="<?php echo $row->campus_id;?>" onclick="getchek(<?php echo $row->campus_id;?>)" href="javascript:;" >
                            <button class="btn btn-primary btn-xs tooltips" data-original-title="View&nbsp;Campus" title=""><i class="fa fa-eye"></i></button>
                  </a>
                      </td>
              </tr>
              <?php }?>
            </tbody>
          </table>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Campus Information</h4>
                </div>
                <div class="modal-body">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {    
    $("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
      $(".dataTables_filter input").addClass('form-control');
    $('#example tfoot th').each( function (){
      var title = $(this).text();
      if (title != "Action") {
        $(this).html( '<input type="text" placeholder="Search" />' );
      }         
    }); 
    var dt =  $('#example').DataTable({
        /*"aaSorting": [[ 0, "desc" ]],
        //"scrollX": true,
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [0] }
        ],   */        
    });
    $('#example tbody').on( 'click', 'tr', function () {
       $(this).toggleClass('selected');
    });
    $('#example tbody').on( 'mouseenter', 'td', function () {
      var colIdx = dt.cell(this).index().column;
      $( dt.cells().nodes() ).removeClass( 'highlight' );
      $( dt.column( colIdx ).nodes() ).addClass( 'highlight' );
    });        
    // Apply the search
    dt.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        });
    });
  });
  function getchek(id)
  {
    $.ajax({
       url: '<?php echo base_url(); ?>superadmin/campus/view/',
       data: { id: id},
       dataType: 'html',
       type: 'POST',
       success: function(data){
         $('.modal-body').html(data);
         $('#myModalLabel').show();         
         $('#myModal').modal('show');
       }
    });
  }
</script>



