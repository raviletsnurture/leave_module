<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<div class="ajaxLoader">
    <img src="<?php echo base_url()?>assets/img/loader.gif" alt="loading..." >
</div>
<section id="main-content">
    <section class="wrapper site-min-height" style="margin-top:0px !important;">
        <section class="panel">
            <header class="panel-heading">Recruitment Details
                <span class="pull-right" style="margin-left: 10px;"><a href="<?php echo base_url()?>superadmin/recruitment/exportdata" class="btn btn-success">ExportData</a></span>
                <span class="pull-right"><a href="<?php echo base_url()?>superadmin/recruitment/exportCsv" class="btn btn-success">Download CSV</a></span>
                <button id="mailSendbutton" style="margin-right: 10px;" type="button" class="btn btn-success pull-right">Send mail to all</button>
            </header>

            <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                        <div class="alert alert-block alert-danger fade in margin-top hidden" id="alertBox">
                        <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                        <p id="appendError"></p>
                        </div>

                </div>
            </div>
                <form id="form-filter" class="form-horizontal">

                    <div class="row">
                        <div class="col-md-2">
                            <label for="">Designation</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo $designationCountArray; ?>
                            </div>
                        </div>
                    </div>

                    <!-- experience Filter -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="">Filter Experience</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="filexpfrom" name="filexpfrom" class="form-control" placeholder="From">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" id="filexpto" name="filexpto" class="form-control" placeholder="To">
                            </div>
                        </div>
                    </div>

                    <!-- Applied data filter -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="">Filter Applied Date</label>
                        </div>
                        <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" id="appliedStartDate" name="appliedStartDate" class="form-control date-range-filter" placeholder="From" data-date-format="dd-mm-yyyy">
                                </div>
                        </div>
                        <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" id="appliedEndDate" name="appliedEndDate" class="form-control date-range-filter" placeholder="To" data-date-format="dd-mm-yyyy">
                                </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </form>
                <!-- Modal For sending mail to all-->
                        <div class="modal fade" id="mailModal" role="dialog">
                        <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Write your email here</h4>
                            </div>
                            <div class="modal-body">
                                <form id="mailForm" method="post">
                                <div class="form-group">
                                    <label for="subject">Subject:</label>
                                    <input required type="text" class="form-control subject" name="subject" id="subject">
                                </div>
                                <div class="form-group">
                                    <label for="editor1">Message:</label>
                                    <textarea required name="editor1" class="mailBody" id="editor1" cols="30" rows="10"></textarea>
                                    <p id="errorAppend"></p>
                                </div>
                                <button type="submit" id="submitMail" name="submitMail" class="btn btn-success submitMail">Send mail to all</button>
                                </form>
                            </div>

                            </div>
                        </div>
                        </div>
                <!-- End Modal -->

                <table id="example" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                            <td>Name <input type="text" id="fullname" name="fullname" class="form-control" placeholder="FullName"></td>
                            <td>Email <input type="text" id="email" name="email" class="form-control" placeholder="Email"> </td>
                            <td>Phone <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone"></td>
                            <td>Experience <input type="text" id="experience" name="experience" class="form-control" placeholder="Experience"></td>
                            <td>Applied Post <input type="text" id="applied_designation" name="applied_designation" class="form-control" placeholder="Applied"></td>
                            <td>Current Post <input type="text" id="current" name="current" class="form-control" placeholder="Current"></td>
                            <td>Appoint Type <input type="text" id="appointment" name="appointment" class="form-control" placeholder="Appointment"></td>
                            <td>Applied At <input type="text" id="applied" name="applied" class="form-control" placeholder="Applied"></td>
                            <td>Status 
                            <select name="applicantStatusFilter" id="applicantStatusFilter" class="form-control" >
                <option value="">All</option>
                <option value="1">New</option>
				<option value="2">Not responding</option>
				<option value="3">Not reachable</option>
				<option value="4">Selected</option>
				<option value="5">Next round</option>
				<option value="6">Rejected </option>
				<option value="7">On hold</option>
			</select>
                            </td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Experience</th>
                            <th>Applied Designation</th>
                            <th>Current Designation</th>
                            <th>Appointment Type</th>
                            <th>Applied At</th>
                            <td>Status</td>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>


            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">User Information</h4>
                            </div>
                            <div class="modal-body">
                            </div>
                        </div>
                    </div>
            </div>
           <!-- Modal to schedule interview -->
            <div class="modal fade" id="sendMail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Next Appointment</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="idData">
                            <div class="form-group">
                                <label>Date</label>
                                <div class='input-group date' id='confirmationDate'>
                                    <input type='text' class="form-control" id="interviewDate" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="">Choose Interviewers</label>
                                <select class="interviewer" name="interviewers[]" multiple="multiple">
                                    <?php foreach($AllActiveUsers as $user){?>
                                        <option value="<?php echo $user->user_id?>"><?php echo $user->first_name.' '.$user->last_name;?></option>
                                    <?php }?>
                                </select>
                                </div>
                            <div class="form-group">
                                <label>Interview Mode</label>
                                <div class='input-group'>
                                    <select name="interview_mode" style="width:359%"  id="interview_mode" class="form-control">
                                        <option value="Skype">Skype</option>
                                        <option value="Telephonic">Telephonic</option>
                                        <option value="FaceToFace">FaceToFace</option>
                                        <option value="TechnicalRound">TechnicalRound</option>
                                        <option value="HRRound">HRRound</option>
                                        <option value="FinalRound">FinalRound</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal" id="sendEmail">Send Email</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.js"></script>


<script type="text/javascript">

    function block_user(thisx){
        var clicked_id = $(thisx).attr('id');
        var blockStatus = $(thisx).attr('blockStatus');
        //alert(blockStatus);
            if(blockStatus==0)
            {
                if (confirm('Are you sure you want to block this user?')) {
                $.ajax({
                    url: '<?php echo base_url(); ?>superadmin/recruitment/blockUser',
                    data: {id: clicked_id},
                    //dataType: 'html',
                    type: 'POST',
                    beforeSend: function(){
                        $('.ajaxLoader').show();
                    },
                    success: function(data){
                        $('.ajaxLoader').hide();
                        if(data == 1) {
                            alert('User blocked sucessfully');
                            $(thisx).attr('blockStatus', 1);
                        } else if(data == 2) {
                            $(thisx).attr('blockStatus', 0);
                            alert('User unblocked sucessfully');
                        }else{

                        }
                    }
                });
                }else{
                    return false;
                }
                }else{
                        if (confirm('Are you sure you want to unblock this user?')) {
                        $.ajax({
                            url: '<?php echo base_url(); ?>superadmin/recruitment/unblockUser',
                            data: {id: clicked_id},
                            //dataType: 'html',
                            type: 'POST',
                            beforeSend: function(){
                                $('.ajaxLoader').show();
                            },
                            success: function(data){
                                $('.ajaxLoader').hide();
                                if(data == 1) {
                                    $(thisx).attr('blockStatus', 0);
                                    alert('User unblocked sucessfully');
                                } else if(data == 2) {
                                    $(thisx).attr('blockStatus', 1);
                                    alert('User unblocked sucessfully');
                                }else{

                                }
                            }
                        });
                    }else{
                        return false;
                    }
                }
        }

    
    function reply_click(clicked_id){
        $('#idData').val(clicked_id);
        $('#sendMail').modal('show');
    }
    function getchek(id){
        $.ajax({
            url: '<?php echo base_url(); ?>superadmin/recruitment/view',
            data: {id: id},
            dataType: 'html',
            type: 'POST',
            success: function(data){
                $('#myModal .modal-body').html(data);
                $('#myModal').modal('show');
            }
        });
    }

    $('#sendEmail').click(function(){
        var id = $('#idData').val();
        var date = $('#interviewDate').val();
        var interview_mode = $('#interview_mode').val();
        var interviewers =  $('.interviewer').val();
        if (confirm('Are you sure you want to schedule this?')) {
            $.ajax({
                url: '<?php echo base_url(); ?>superadmin/recruitment/sendConfirmationEmail',
                data: {id: id, date: date,interview_mode:interview_mode, interviewers:interviewers},
                type: 'POST',
                beforeSend: function(){
                    $('.ajaxLoader').show();
                },
                success: function(data){
                    $('.ajaxLoader').hide();
                    if(data == 1) {
                        alert('Mail Sent!');
                    } else {
                        alert('error occured');
                    }
                }
            });
        }else{
        $('#sendMail').modal('hide');
        return false;
        }
    });

    var table;
    $(document).ready(function() {
        // Send mail model
        var dateToday = new Date();
        //dateToday.setTime(dateToday.getTime() + (2*60*60*1000));
        dateToday.setTime(Date.now() + (2*60*60*1000));
        $('#confirmationDate').datetimepicker({
            format: 'YYYY-MM-DD hh:mm a',
            minDate: dateToday,
            stepping: 15,
            sideBySide: true,
        });

        $("#appliedStartDate").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $("#appliedEndDate").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        //datatables
        table = $('#example').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,
            searching: false,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('superadmin/recruitment/ajax_list')?>",
                "type": "POST",
                "data": function ( data ) {
                    data.fullname = $('#fullname').val();
                    data.email = $('#email').val();
                    data.phone = $('#phone').val();
                    data.experience = $('#experience').val();
                    data.applied_designation = $('#applied_designation').val();
                    data.current = $('#current').val();
                    data.appointment = $('#appointment').val();
                    data.applied = $('#applied').val();
                    data.designation = $('#designation').val();
                    data.filexpfrom = $('#filexpfrom').val();
                    data.filexpto = $('#filexpto').val();
                    data.appliedStartDate = $('#appliedStartDate').val();
                    data.appliedEndDate = $('#appliedEndDate').val();
                    data.applicantStatusFilter = $('#applicantStatusFilter').val();
                }
            }
        });

        $('#btn-filter').click(function(){ //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function(){ //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });

        $('.interviewer').select2();

        $("#fullname").on('keyup', function (e) {  if (e.keyCode == 13) { table.ajax.reload(); } });

        $("#email").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#phone").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#experience").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload(); } });

        $("#applied_designation").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#current").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#appointment").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#applied").on('keyup', function (e) { if (e.keyCode == 13) { table.ajax.reload();  } });

        $("#applicantStatusFilter").on('change', function (e) { table.ajax.reload(); });

    });
 $('#mailSendbutton').click(function(){
    $('#mailModal').modal('show');
 });
CKEDITOR.replace( 'editor1' );

$(document).ready(function () {
    $('.submitMail').click(function () {
      $('#mailForm').validate({
        errorClass: "error",
        errorElement: "span",
        ignore: [],
        rules: {
            subject:{
                required : true,
                lettersonly: true
            },
            editor1: {
                required: function()
                {
                    CKEDITOR.instances.editor1.updateElement();
                }
            }

        },
        messages: {
            editor1: {
                required: "Mail cannot be blank"
          },
            subject:{
                required:"Mail Subject is required",
                lettersonly :"Subject can contain only letters"
          }
        },
        submitHandler: function (form, event) {
          event.preventDefault();
          var desc = CKEDITOR.instances.editor1.getData();
          var subject = $('.subject').val();
          $("#submitMail").html('<i class="fa fa-refresh fa-spin">');
          $.ajax({
            url: '<?php echo base_url(); ?>superadmin/recruitment/sendRecruitmentMailToAll',
            data: {
              data: desc, subject:subject
            },
            type: 'POST',
            success: function (res) {
                $("#submitMail").html('Send mail to all');
              if(res == 1){
                $('#mailModal').modal('hide');
                  $('#alertBox').removeClass('hidden alert-danger');
                  $('#alertBox').addClass('alert-success');
                  $('#appendError').html("<strong>Success! </strong>Mail sent successfully!");
              }else{
                $('#alertBox').removeClass('hidden alert-danger');
                $('#alertBox').addClass('alert-success');
                $('#appendError').html("<strong>Success! </strong>Mail sent successfully!");
                  // $('#mailModal').modal('hide');
                  // $('#alertBox').removeClass('hidden alert-success');
                  // $('#alertBox').addClass('alert-danger');
                  // $('#appendError').html("<strong>Error! </strong>there was a problem sending mail!");
              }

            }
          });
        }
      });
    });
  });
  CKEDITOR.instances.editor1.on('change', function() {
    if(CKEDITOR.instances.editor1.getData().length >  0) {
      $('span[for="editor1"]').hide();
    }
});

function changeApplicantStatus(sel, userID){
    var option = sel.value; 
    var r = confirm("Are you sure you want to change the status of the applicant?");
    if (r == true){
        $.ajax({
            url: '<?php echo base_url(); ?>superadmin/recruitment/changeApplicantStatus',
            data: { userID, option},
            type: 'POST',
            success: function(data){
                console.log(data);
                if(data == 1){
                    alert("Status updated!");
                }
                else{
                    alert("Oops! Error occured.");
                }
            }
        });
    }
}
</script>
