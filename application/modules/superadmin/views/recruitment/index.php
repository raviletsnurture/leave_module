<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<style media="screen">
table{width: 100% !important; }
#myModal .modal-body{
  overflow-y: scroll;
  height: 500px;
}
#myModal .modal-body::-webkit-scrollbar {
    width: 10px;
}

#myModal .modal-body::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(232, 64, 63,0.3);
    border-radius: 10px;
}

#myModal .modal-body::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(232, 64, 63,0.5);
    background:rgb(232, 64, 63);
}
.col-md-3{
    margin-bottom: 10px;
}
.left-badge{
    min-width: 40px;
    float:left;
}
#example_wrapper .row-fluid .span6:first-of-type{
  float: left;
  width:20%;
}
#example_wrapper .row-fluid .span6:last-of-type{
  float: left;
  width:80%;
}
.ajaxLoader{
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.2);
    z-index: 1;
    display: none;
}
.ajaxLoader img{
    left:50%;
    top:50%;
    position: fixed;
    width: 30px;
}
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
tfoot {
    display: table-header-group;
}
.collapsable:after {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\f054";
    transform: rotate(90deg);
    transition: all linear 0.25s;
    margin-left:5px;
  }   
/* .collapsable.collapsed:after {
  transform: rotate(90deg) ;
} */

.margin-bottom{
    margin-bottom:1em;
}
</style>
<div class="ajaxLoader">
    <img src="<?php echo base_url()?>assets/img/loader.gif" alt="loading..." >
</div>
<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading">Recruitment Details                
                <span class="pull-right" style="margin-left: 10px;"><a href="<?php echo base_url()?>superadmin/recruitment/exportdata" class="btn btn-success">ExportData</a></span>                
                <span class="pull-right"><a href="<?php echo base_url()?>superadmin/recruitment/exportCsv" class="btn btn-success">Download CSV</a></span>                
            </header>
            <div class="panel-body">
       
                <button type="button" id="toggle_field" class="btn btn-info margin-bottom collapsable" data-toggle="collapse" data-target="#demo">Field Count</button>
                <div id="demo" class="collapse">
                    <div class="row" id="designations">
                        <?php
                            foreach ($designationCountArray as $row) { ?>
                                <div class="col-md-3">
                                    <a href="javascript:;">
                                        <div class="left-badge"><span class="badge"><?php echo $row->designationCount; ?></span></div>
                                        <span id="des"><?php echo $row->designation; ?></span>
                                    </a>
                                </div>
                            <?php
                            }
                        ?>
                    </div>
                </div>

                <!-- experience Filter -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="">Filter Experience</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="filexpfrom" name="filexpfrom" class="form-control" placeholder="From">
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="filexpto" name="filexpto" class="form-control" placeholder="To">
                    </div>
               </div>

               <!-- Applied data filter -->
               <div class="row">
                   <div class="col-md-2">
                       <label for="">Filter Applied Date</label>
                   </div>
                   <div class="col-md-3">
                       <input type="text" id="appliedStartDate" value="<?php if(isset($_GET['sd'])){echo $_GET['sd'];} ?>" name="appliedStartDate" class="form-control date-range-filter" placeholder="From" data-date-format="dd-mm-yyyy">
                   </div>
                   <div class="col-md-3">
                       <input type="text" id="appliedEndDate" value="<?php if(isset($_GET['ed'])){echo $_GET['ed'];} ?>" name="appliedEndDate" class="form-control date-range-filter" placeholder="To" data-date-format="dd-mm-yyyy">
                   </div>
                   <div class="col-md-3">
                   <a class="btn btn-info" href="javascript:;" id="Filter_Recruitment">Submit</a>
                   <a class="btn btn-info" href="javascript:;" id="clearFilter">Clear filter</a>
                   </div>
              </div>   
             
                                    
                <table class="table table-striped table-hover table-bordered" id="example">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Phone</td>
                            <td style="width:100px;">Experience</td>
                            <td style="width:160px;">Applied Designation</td>
                            <td style="width:200px;">Current Designation</td>
                            <td style="width:200px;">Appointment Type</td>
                            <td style="width:100px;">Applied At</td>
                            <td style="width:100px;">Action</td>
                        </tr>
                    </thead>
                    <tfoot>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th style="width:100px;">Experience</th>
                            <th style="width:160px;">Applied Designation</th>
                            <th style="width:200px;">Current Designation</th>
                            <th style="width:200px;">Appointment Type</th>
                            <th style="width:100px;">Applied At</th>
                            <th style="width:100px;">Action</th>
                          </tr>
                      </tfoot>
                    <tbody id="table_body">
                        <?php
                            foreach ($recruitmentData as $data) { ?>
                                <tr>
                                    <td><?php echo $data->fullName; ?></td>
                                    <td><?php echo $data->email; ?></td>
                                    <td><?php echo $data->phone; ?></td>
                                    <td><?php echo $data->experience; ?></td>
                                    <td><?php echo $data->designation; ?></td>
                                    <td><?php echo $data->currentDesignation; ?></td>
                                    <td><?php echo (!empty($data->interview_date)) ? $data->interview_date : 'Not Scheduled'; ?></td>
                                    <td><?php echo date("Y/m/d",strtotime($data->createdDate)); ?></td>
                                    <td style="text-align:center;">
                                        <a value="<?php echo $data->rec_id;?>" onclick="getchek(<?php echo $data->rec_id;?>)" href="#" >
                                            <button class="btn btn-primary btn-xs tooltips" data-original-title="View&nbsp;User" title=""><i class="fa fa-eye"></i></button>
                                        </a>
                                        <a href="http://recruitment.letsnurture.com/CV/<?php echo $data->file; ?>"  target="_blank">
                                            <button class="btn btn-danger btn-xs tooltips" data-original-title="Download&nbsp;PDF" title=""><i class="fa fa-download"></i></button>
                                        </a>
                                        <?php if(empty($data->interview_date)){?>
                                            <a value="<?php echo $data->rec_id;?>" onclick="reply_click(this.id)" href="#" id="<?php echo $data->rec_id;?>" >
                                                <button class="btn btn-success btn-xs tooltips" data-original-title="Schedule an interview and send the mail to the candidate" title=""><i class="fa fa-share"></i></button>
                                            </a>
                                       <?php }?>
                                        <a value="<?php echo $data->rec_id;?>" onclick="block_user(this); return false;" href="#" id="<?php echo $data->rec_id;?>" blockStatus="<?php echo $data->isBlocked;?>" >
                                            <button class="btn btn-danger btn-xs tooltips" data-original-title="<?php if($data->isBlocked==1) echo 'Unblock'; else echo 'Block'; ?>" title=""><i class="fa fa-close"></i></button>
                                        </a>
                                
                                    </td>
                                </tr>
                            <?php
                            }
                        ?>
                    </tbody>
                </table>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">User Information</h4>
                            </div>
                            <div class="modal-body">
                            </div>
                        </div>
                    </div>
                </div>
 
                <div class="modal fade" id="sendMail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Next Appointment</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="idData">
                                <div class="form-group">
                                    <label>Date</label> 
                                    <div class='input-group date' id='confirmationDate'>
                                        <input type='text' class="form-control" id="interviewDate" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
    
                                <div class="form-group">
                                    <label class="">Choose Interviewers</label>
                                    <select class="interviewer" name="interviewers[]" multiple="multiple">
                                        <?php foreach($AllActiveUsers as $user){?>
                                            <option value="<?php echo $user->user_id?>"><?php echo $user->first_name.' '.$user->last_name;?></option>
                                        <?php }?>                                                                    
                                    </select>
                                  </div>
                                <div class="form-group">
                                    <label>Interview Mode</label>
                                    <div class='input-group'>
                                        <select name="interview_mode" style="width:359%"  id="interview_mode" class="form-control">
                                          <option value="Skype">Skype</option>
                                          <option value="Telephonic">Telephonic</option>
                                          <option value="FaceToFace">FaceToFace</option>
                                          <option value="TechnicalRound">TechnicalRound</option>
                                          <option value="HRRound">HRRound</option>
                                          <option value="FinalRound">FinalRound</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">                         
                                <button type="button" class="btn btn-success" data-dismiss="modal" id="sendEmail">Send Email</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <!-- page end-->

    </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    // Check if url has start date and end date parameters
    var urlParams = new URLSearchParams(window.location.search);
    if(urlParams.has('sd')==true && urlParams.has('ed')==true){
        setTimeout(function(){
            $("#Filter_Recruitment").click();
                },2000);
    }


    $('.interviewer').select2();    
});
    function getchek(id){
        $.ajax({
            url: '<?php echo base_url(); ?>superadmin/recruitment/view',
            data: {id: id},
            dataType: 'html',
            type: 'POST',
            success: function(data){
                $('#myModal .modal-body').html(data);
                $('#myModal').modal('show');
            }
        });
    }

    function reply_click(clicked_id){
        $('#idData').val(clicked_id);
        $('#sendMail').modal('show');
    }

    function block_user(thisx){
      var clicked_id = $(thisx).attr('id');
      var blockStatus = $(thisx).attr('blockStatus');
      //alert(blockStatus);
      if(blockStatus==0)
      {
        if (confirm('Are you sure you want to block this user?')) {
          $.ajax({
              url: '<?php echo base_url(); ?>superadmin/recruitment/blockUser',
              data: {id: clicked_id},
              //dataType: 'html',
              type: 'POST',
              beforeSend: function(){
                  $('.ajaxLoader').show();
              },
              success: function(data){
                  $('.ajaxLoader').hide();
                  if(data == 1) {
                      alert('User blocked sucessfully');
                      $(thisx).attr('blockStatus', 1);
                  } else if(data == 2) {
                      $(thisx).attr('blockStatus', 0);
                      alert('User unblocked sucessfully');
                  }else{

                  }
              }
          });
      }else{
        return false;
      }
    }else{
            if (confirm('Are you sure you want to unblock this user?')) {
              $.ajax({
                  url: '<?php echo base_url(); ?>superadmin/recruitment/unblockUser',
                  data: {id: clicked_id},
                  //dataType: 'html',
                  type: 'POST',
                  beforeSend: function(){
                      $('.ajaxLoader').show();
                  },
                  success: function(data){
                      $('.ajaxLoader').hide();
                      if(data == 1) {
                          $(thisx).attr('blockStatus', 0);
                          alert('User unblocked sucessfully');
                      } else if(data == 2) {
                          $(thisx).attr('blockStatus', 1);
                          alert('User unblocked sucessfully');
                      }else{

                      }
                  }
              });
          }else{
            return false;
          }
      }
    }

    $('#sendEmail').click(function(){
        var id = $('#idData').val();
        var date = $('#interviewDate').val();
        var interview_mode = $('#interview_mode').val(); 
        var interviewers =  $('.interviewer').val();              
       if (confirm('Are you sure you want to schedule this?')) {
          $.ajax({
              url: '<?php echo base_url(); ?>superadmin/recruitment/sendConfirmationEmail',
              data: {id: id, date: date,interview_mode:interview_mode, interviewers:interviewers},
              type: 'POST',
              beforeSend: function(){
                  $('.ajaxLoader').show();
              },
              success: function(data){
                  $('.ajaxLoader').hide();
                  if(data == 1) {
                      alert('Mail Sent!');
                  } else {
                      alert('error occured');
                  }
              }
          });
      }else{
        $('#sendMail').modal('hide');
        return false;
      }
    });

    

    $(document).ready(function() {

         $(function() {
            $('#applied_datepicker').datepicker({ dateFormat: 'yy/mm/dd' }).val();
        });  
        // Setup - add a text input to each footer cell
        $('#example tfoot th').each( function () {            
            var title = $(this).text();
            if (title === "Applied At") {
                $.urlParam = function(name){
                        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                        if(results == null){
                            return '';
                        }
                        else{
                            return results[1] || 0;
                        }
                    }  
                    if(decodeURIComponent($.urlParam('date1')) !='' ){
                       var dateUrl = decodeURIComponent($.urlParam('date1'));
                       $(this).html( '<input type="text" value="'+dateUrl+'" id="applied_datepicker" placeholder="Search" />' );
                    }   
                    else{
                        $(this).html( '<input type="text" id="applied_datepicker" placeholder="Search" />' );
                    }
                    
                    setTimeout(function(){
                    $( "#applied_datepicker" ).trigger( "keyup" );
                    }, 2000);
                }
            else {
                    $(this).html( '<input type="text" placeholder="Search" />' );
                }
        }); 

        var dt =  $('#example').DataTable({
            "ordering": false,
            "bPaginate": true,
		    "bInfo" : true
     
            /*"aaSorting": [[ 7, "desc" ]],
            "scrollX": true,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [7] }
            ],*/          
        });
        $('#example tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
                });

        $('#example tbody').on( 'mouseenter', 'td', function () {
                var colIdx = dt.cell(this).index().column;
                $( dt.cells().nodes() ).removeClass( 'highlight' );
                $( dt.column( colIdx ).nodes() ).addClass( 'highlight' );
        });
        $('#button').click( function () {
                alert( dt.rows('.selected').data().length +' row(s) selected' );
        });
        // Apply the search
        dt.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            });
        });

        $(".dataTables_filter input").addClass('form-control');
        $('#designations #des').click(function() {
            var txt = $(this).text();
            dt.columns(4).search(txt).draw();
        });

        $('#clearFilter').click(function() {            
            $('#example').DataTable().destroy();
            $('#filexpfrom, #filexpto, #appliedStartDate, #appliedEndDate').val('');
            var appliedStartDate = $("#appliedStartDate").val();
            var appliedEndDate = $("#appliedEndDate").val();
            reload_datatable_ajax(appliedStartDate,appliedEndDate);
        });
        // Filter experience Functionality
        $.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
                var min = parseFloat( $('#filexpfrom').val(), 10 );
                var max = parseFloat( $('#filexpto').val(), 10 );
                var age = parseFloat( data[3] ) || 0;

                if ( ( isNaN( min ) && isNaN( max ) ) || ( isNaN( min ) && age <= max ) || ( min <= age   && isNaN( max ) ) || ( min <= age   && age <= max ) ) {
                    return true;
                }
                    return false;
        });

        $('#filexpfrom, #filexpto').keyup( function() {
            dt.draw();
        } );

        // Send mail model
        var dateToday = new Date();
        //dateToday.setTime(dateToday.getTime() + (2*60*60*1000));
        dateToday.setTime(Date.now() + (2*60*60*1000));
        $('#confirmationDate').datetimepicker({
            format: 'YYYY-MM-DD hh:mm a',
            minDate: dateToday,
            stepping: 15,
            sideBySide: true,
        });
        

        $("#appliedStartDate").datepicker({  
          dateFormat: 'yy-mm-dd',
          changeMonth: true, 
          changeYear: true 
        });
        $("#appliedEndDate").datepicker({  
          dateFormat: 'yy-mm-dd',
          changeMonth: true, 
          changeYear: true 
        });

        $( "#Filter_Recruitment" ).click(function() {   
          var appliedStartDate = $("#appliedStartDate").val();
          var appliedEndDate = $("#appliedEndDate").val(); 
          if((appliedStartDate != '') && (appliedEndDate != ''))
          {
            $('#example').DataTable().destroy();    
            reload_datatable_ajax(appliedStartDate,appliedEndDate);      
          }else{
            alert('Please select date');
            return false;
          }
          
          
        });

        function reload_datatable_ajax(appliedStartDate,appliedEndDate){

          $.ajax({
            type: 'POST',
            url: '<?php echo base_url()?>superadmin/recruitment/recruitment_filter',
            data : { 'appliedStartDate' : appliedStartDate,'appliedEndDate':appliedEndDate},
            dataType: "html",
            beforeSend: function(){
              $('.ajaxLoader').show();
            },
            success:function(data){
              $('.ajaxLoader').hide();
              var json = $.parseJSON(data); 
              if(json['table'] == 'No Data Found.'){
                  alert("No Data Found.");
                  return false;
              }else{           
              $('#table_body').html(json['table']);
              $('#example tfoot th').each( function () {            
                  var title = $(this).text();
                        if (title === "Applied At") {
                            $.urlParam = function(name){
                                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                                if(results == null){
                                    return '';
                                }
                                else{
                                return results[1] || 0;

                                }
                            }
   
                            if(decodeURIComponent($.urlParam('date1')) !='' ){
                                var dateUrl = decodeURIComponent($.urlParam('date1'));
                            }   
                            else{
                                var dateUrl = '';
                            }
                        $(this).html( '<input type="text" value="'+dateUrl+'" id="applied_datepicker" placeholder="Search" />' );
                        setTimeout(function(){
                        $( "#applied_datepicker" ).trigger( "keyup" );
                        }, 2000);
                        
                      }
                      else {
                          $(this).html( '<input type="text" placeholder="Search" />' );
                      }
              }); 

              var dt1 =  $('#example').DataTable({                     
                  "ordering": false
            
              });
              $('#example tbody').on( 'click', 'tr', function () {
                      $(this).toggleClass('selected');
              });
              $('#example tbody').on( 'mouseenter', 'td', function () {
                      var colIdx = dt1.cell(this).index().column;
                      $( dt1.cells().nodes() ).removeClass( 'highlight' );
                      $( dt1.column( colIdx ).nodes() ).addClass( 'highlight' );
              });
              $('#button').click( function () {
                      alert( dt1.rows('.selected').data().length +' row(s) selected' );
              });
              // Apply the search
              dt1.columns().every( function () {
                  var that = this;
                  $( 'input', this.footer() ).on( 'keyup change', function () {
                      if ( that.search() !== this.value ) {
                          that
                              .search( this.value )
                              .draw();
                      }
                  });
              });

              $(".dataTables_filter input").addClass('form-control');
              $('#designations #des').click(function() {
                  var txt = $(this).text();
                  dt1.columns(4).search(txt).draw();
              });
              // Filter experience Functionality
              $.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
                      var min = parseFloat( $('#filexpfrom').val(), 10 );
                      var max = parseFloat( $('#filexpto').val(), 10 );
                      var age = parseFloat( data[3] ) || 0;

                      if ( ( isNaN( min ) && isNaN( max ) ) || ( isNaN( min ) && age <= max ) || ( min <= age   && isNaN( max ) ) || ( min <= age   && age <= max ) ) {
                          return true;
                      }
                          return false;
              });

              $('#filexpfrom, #filexpto').keyup( function() {
                  dt.draw();
              } );

              // Send mail model
              var dateToday = new Date();
              //dateToday.setTime(dateToday.getTime() + (2*60*60*1000));
              dateToday.setTime(Date.now() + (2*60*60*1000));
              $('#confirmationDate').datetimepicker({
                  format: 'YYYY-MM-DD hh:mm a',
                  minDate: dateToday,
                  stepping: 15,
                  sideBySide: true,
              });
            }
            }
          });
        }

    });   
</script>
