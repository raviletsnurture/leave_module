<?php
if(!empty($recruitmentData)){
    foreach ($recruitmentData as $data) { ?>
        <tr>
            <td><?php echo $data->fullName; ?></td>
            <td><?php echo $data->email; ?></td>
            <td><?php echo $data->phone; ?></td>
            <td><?php echo $data->experience; ?></td>
            <td><?php echo $data->designation; ?></td>
            <td><?php echo $data->currentDesignation; ?></td>
            <td><?php echo (!empty($data->interview_date)) ? $data->interview_date : 'Not Sechduled'; ?></td>
            <td><?php echo date("Y/m/d",strtotime($data->createdDate)); ?></td>
            <td>
                <a value="<?php echo $data->rec_id;?>" onclick="getchek(<?php echo $data->rec_id;?>)" href="#" >
                    <button class="btn btn-primary btn-xs tooltips" data-original-title="View&nbsp;User" title=""><i class="fa fa-eye"></i></button>
                </a>
                <a href="http://recruitment.letsnurture.com/CV/<?php echo $data->file; ?>"  target="_blank">
                    <button class="btn btn-danger btn-xs tooltips" data-original-title="Download&nbsp;PDF" title=""><i class="fa fa-download"></i></button>
                </a>
                <a value="<?php echo $data->rec_id;?>" onclick="reply_click(this.id)" href="#" id="<?php echo $data->rec_id;?>" >
                    <button class="btn btn-success btn-xs tooltips" data-original-title="Send&nbsp;Email" title=""><i class="fa fa-share"></i></button>
                </a>
                <a value="<?php echo $data->rec_id;?>" onclick="block_user(this); return false;" href="#" id="<?php echo $data->rec_id;?>" blockStatus="<?php echo $data->isBlocked;?>" >
                    <button class="btn btn-danger btn-xs tooltips" data-original-title="<?php if($data->isBlocked==1) echo 'Unblock'; else echo 'Block'; ?>" title=""><i class="fa fa-close"></i></button>
                </a>
                <a value="<?php echo $data->rec_id;?>" onclick="putComment(<?php echo $data->rec_id;?>)" href="#" >
                    <button class="btn btn-primary btn-xs tooltips" data-original-title="Comment" title=""><i class="fa fa-comment"></i></button>
                </a>
                <!-- Uravisha Prabhatsinh Parmar_201704151138.pdf -->
            </td>
        </tr>
    <?php
    } }else{
        echo "No Data Found.";
    }
?>