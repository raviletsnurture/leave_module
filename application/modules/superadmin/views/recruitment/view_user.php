<?php
    $data = $userData[0];

?>
<div class="row-fluid">
    <div class="span9">
        <div class="new-list">
            <div class="item panel" id="myprofile">
                <form>
                    <table class="table myprofile_tbl">
                    <tr>
                        <th>Name</th>
                        <td><?php echo $data->fullName; ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><?php echo $data->email; ?></td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td><?php echo $data->phone; ?></td>
                    </tr>
                    <tr>
                        <th>Skype</th>
                        <td><?php echo $data->skype_email; ?></td>
                    </tr>
                    <tr>
                        <th>Dob</th>
                        <td><?php $dob = $data->dob;
                            if($dob){$date = date('d-m-Y', strtotime($dob));
                            echo $date;}
                        ?></td>
                    </tr>
                    <tr>
                        <th>Current Location</th>
                        <td><?php echo $data->currentLocation; ?></td>
                    </tr>
                    <tr>
                        <th>Preferred Location</th>
                        <td><?php echo $data->preferredLocation; ?></td>
                    </tr>
                    <tr>
                        <th>KeySkills</th>
                        <td><?php echo $data->keySkills; ?></td>
                    </tr>
                    <tr>
                        <th>Experience</th>
                        <td><?php echo $data->experience; ?></td>
                    </tr>
                    <tr>
                        <th>Current Designation</th>
                        <td><?php echo $data->currentDesignation; ?></td>
                    </tr>
                    <tr>
                        <th>Current Employer</th>
                        <td><?php echo $data->currentEmployer; ?></td>
                    </tr>
                    <tr>
                        <th>Required Designation</th>
                        <td><?php echo $data->designation; ?></td>
                    </tr>
                    <tr>
                        <th>Appointment Type</th>
                        <td><?php echo $data->appointment; ?></td>
                    </tr>
                    <tr>
                        <th>Current Salary</th>
                        <td><?php echo $data->salary; ?></td>
                    </tr>
                    <tr>
                        <th>Qualification</th>
                        <td><?php echo $data->qualification; ?></td>
                    </tr>
                    <tr>
                        <th>Institution</th>
                        <td><?php echo $data->institution; ?></td>
                    </tr>
                    <tr>
                        <th>Created Date</th>
                        <td><?php echo $data->createdDate; ?></td>
                    </tr>
                    <tr>
                        <th>IP Address</th>
                        <td><?php echo $data->ipAddress; ?></td>
                    </tr>
                    <tr>
                        <th>Login Browser</th>
                        <td><?php echo $data->browserlogin; ?></td>
                    </tr>
                    <tr>
                        <th>Video Profile</th>
                        <td><?php echo $data->video_profile; ?></td>
                    </tr>
                    <tr>
                        <th>Github</th>
                        <td><?php echo $data->github; ?></td>
                    </tr>
                    <tr>
                        <th>StackOverflow</th>
                        <td><?php echo $data->stackoverflow; ?></td>
                    </tr>
                    <tr>
                        <th>Twitter</th>
                        <td><?php echo $data->twitter; ?></td>
                    </tr>
                    <tr>
                        <th>LinkedIn</th>
                        <td><?php echo $data->linkedin; ?></td>
                    </tr>
                    <tr>
                        <th>Facebook</th>
                        <td><?php echo $data->facebook; ?></td>
                    </tr>
                    <tr>
                        <th>Blog</th>
                        <td><?php echo $data->blog; ?></td>
                    </tr>
                    <tr>
                        <th>Certification</th>
                        <td><?php echo $data->certfication; ?></td>
                    </tr>
                    <tr>
                        <th>First Available TimeSlot</th>
                        <td><?php echo $data->slot_first; ?></td>
                    </tr>
                    <tr>
                        <th>Second Available TimeSlot</th>
                        <td><?php echo $data->slot_second; ?></td>
                    </tr>
                    <tr>
                        <th>Third Available TimeSlot</th>
                        <td><?php echo $data->slot_third; ?></td>
                    </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
