<style media="screen">
  table{
    width: 100% !important;
  }
  .editable-table .dataTables_filter{width: 23%;}
  tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
      }
  tfoot {
      display: table-header-group;
  }
  .dataTables_length{
    float: left;
  }  
</style>
<section id="main-content">
   <section class="wrapper">
      <div class="row">
         <div class="col-lg-12">
            <section class="panel">
               <header class="panel-heading">
                  Add Technology
               </header>
               <div class="col-lg-12">
               <?php if($this->session->flashdata('error')){?>
               <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
               </div>
               <?php } ?>
               <?php if($this->session->flashdata('success')){?>
               <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
               </div>
               <?php }?>
               </div>
               <div class="panel-body">
                  <div class="row">
                      <div class="col-lg-12">
                        <div class="form">
                            <?php
                            $form_attributes = array('name' => 'addEdittechnology', 'id' => 'addEdittechnology', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form");                        
                              echo form_open(base_url().'superadmin/recruitment/jobopening',$form_attributes);                        
                            ?>
                            <div class="form-group">
                              <div class="col-lg-4">
                                <label for="technology">Technology<span class="red">*</span></label>
                               <input class="form-control" id="technology" maxlength="25" name="technology" type="text" placeholder="Technology"/>
                              </div>
                              <div class="col-lg-4" style="top:23px;">
                                    <button class="btn btn-danger" type="submit" name="addEdittechnology">Submit                                 
                                    </button>                                
                              </div>
                            </div>                                                     
                           </form>
                        </div>
                      </div>
                  </div>    
                  <div class="row">
                  <br/><br/>
                  <header class="panel-heading">
                      Add Vacancy
                  </header>
                  <br/><br/>
                      <div class="col-lg-12">
                          <div class="form">
                            <?php
                            $form_attributes = array('name' => 'addEditvacancy', 'id' => 'addEditvacancy', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form");                        
                              echo form_open(base_url().'superadmin/recruitment/jobopening',$form_attributes);                        
                            ?>
                            <div class="form-group">
                              <div class="col-lg-4">
                                <label for="vacancy">Technology<span class="red">*</span></label>
                                   <select name="technology_name" id="technology_name" class="form-control m-bot15">
                                   <option value="">select technology</option>
                                    <?php foreach($total_job as $row2){ ?>
                                    <option value="<?php echo $row2->job_id; ?>"> <?php echo $row2->technology; ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                              <div class="col-lg-4">
                                <label for="vacancy">Vacancy<span class="red">*</span></label>
                               <input class="form-control" id="vacancy" maxlength="25" name="vacancy" type="text" placeholder="Vacancy"/>
                              </div>
                              <div class="col-lg-2" style="top:23px;">
                                    <button class="btn btn-danger" type="submit" name="addEditvacancy">Submit                                 
                                    </button>                                
                                </div>
                            </div>                                                   
                           </form>
                          </div>
                      </div>  
                  <br/><br/>    
                  </div>
                       <?php if(!empty($jobOpening)){ ?>
                       <table class="table table-striped table-hover table-bordered" id="example">
                          <thead>
                            <tr>
                              <th>Technology</th>
                              <th>Vacancy</th> 
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach($jobOpening as $row){ ?>
                            <tr>
                              <td><?php echo $row->technology;?></td>
                              <td><?php echo $row->vacancy; ?></td>
                              <td>
                                <!-- <a href="<?php// echo base_url()?>superadmin/recruitment/edit/<?php //echo $row->job_id;?>" >
                                <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Campus" title=""><i class="fa fa-pencil"></i></button>
                                </a> -->
                                <a href="<?php echo base_url()?>superadmin/recruitment/jobdelete/<?php echo $row->job_id;?>" class="deleteRec">
                                <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Campus"><i class="fa fa-trash-o "></i></button>
                                </a>
                              </td>
                            </tr>
                            <?php }?>
                          </tbody>
                        </table>
                        <?php } ?>
                   
               </div>
            </section>
         </div>
      </div>
   </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/validate/form-validation-job.js"></script>
<script type="text/javascript">
  var dt =  $('#example').DataTable({
        "aaSorting": [[ 0, "desc" ]],
        "scrollX": true,
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [0] }
        ],          
    });
</script>
