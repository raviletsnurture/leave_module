
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <?php if(isset($countryEdit[0]->country_id)){ echo "Edit Country";} else{echo "Add Country";}?>
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
              <?php 
			  $form_attributes = array('name' => 'addCountry', 'id' => 'addCountry', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
			  if(isset($countryEdit[0]->country_id)){
			  	echo form_open('superadmin/country/add/'.$countryEdit[0]->country_id,$form_attributes);
			  }else{
				echo form_open(base_url().'superadmin/country/add',$form_attributes);
			  }
			  ?>
			  
			  
			  
			              
			  
              <div class="form-group ">
                <label for="firstName" class="control-label col-lg-2">Country Name <span class="red">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="short_name" name="short_name" type="text"  value="<?php if(isset($countryEdit[0]->short_name)){ echo $countryEdit[0]->short_name;} ?>"/>
                </div>
              </div>
               <div class="form-group ">
                <label for="catStatusFlag" class="control-label col-lg-2">Status</label>
                <div class="col-lg-10">
                  <select name="status" id="status" class="form-control m-bot15">
                    <option value="Active" <?php echo isset($countryEdit[0]->status)? selectedVal($countryEdit[0]->status,"Active"): '';?>>Active</option>
                    <option value="Inactive" <?php echo isset($countryEdit[0]->status)? selectedVal($countryEdit[0]->status,"Inactive"): '';?> >Inactive</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-danger" type="submit" name="addEditCountry">
                  <?php if(isset($countryEdit[0]->country_id)){ echo "Update";} else{echo "Submit";}?>
                  </button>
              	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-state.js" ></script> 
<script>
$('#country_id').change(function(){

    var country_id = $('#country_id').val();

    $.ajax({
            url: '<?php echo base_url();?>superadmin/country/getStates',
            type: 'POST',
            data: {
                country_id:country_id,
            },
            success:function(data){
                //alert(data);
				//var obj = JSON.parse(data);
                //alert(obj.yourfield);
				$('#state_id').html(data);
            },
            error:function(){
                alert('Data not Found')
            }


        });

 });
</script>