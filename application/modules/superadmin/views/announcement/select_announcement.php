<style type="text/css">
.dataTables_filter {
     display: none;
}
</style>

<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading"> All Announcements </header>
            <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
                <div class="row">
                    <div class="col-lg-4">
                        <div id="editable-sample_length" class="dataTables_length">
                            <div class="btn-group">
                                <a href="<?php echo base_url()?>superadmin/announcement/add">
                                    <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <?php if($this->session->flashdata('error')){?>
                        <div class="alert alert-block alert-danger fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
                        </div>
                    <?php } ?>

                    <?php if($this->session->flashdata('success')){?>
                        <div class="alert alert-success fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                        </div>
                    <?php }?>

                    <select id="engines">
                        <option value="">ID</option>
                        <?php foreach($announcements as $row){?>
                            <option value="<?php echo $row->announcement_id; ?>"><?php echo $row->announcement_id; ?></option>
                        <?php } ?>
                    </select>

                    <select id="engines3">
                        <option value="">Status</option>
                        <option value="Inactive">Inactive</option>
                        <option value="Active">Active</option>
                    </select>

                    <table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
                            <tr>
                                <th width="4%">ID</th>
                                <th width="20%">Title</th>
                                <th width="64%">Description</th>
                                <th width="10%">Announcement Date</th>
                                <th width="2%">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach($announcements as $row){ ?>
                            <tr>
                              <td><?php echo $row->announcement_id; ?></td>
                      				<td><?php echo $row->title;?></td>
                      				<td><?php echo $row->description;?></td>
                      				<td><?php echo date("Y-m-d", strtotime($row->created_time)); ?></td>
                              <td>
                                  <a href="<?php echo base_url()?>superadmin/announcement/delete/<?php echo $row->announcement_id;?>" class="deleteRec">
                                      <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Announcement"><i class="fa fa-trash-o "></i></button>
                                  </a>
                              </td>
                            </tr>
                          <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </section>
</section>

<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type = "text/javascript" charset = "utf-8" >
$(document).ready(function () {
	var dt = $('#example').dataTable({
		"aaSorting": [
			[2, "desc"]
		],
		"iDisplayLength": 10,
		"pagingType": "full_numbers",
		"dom": 'Cfrtip',
		"destroy": true,
		//"bFilter": false,
		"bPaginate": true,
		"bInfo": false,
		"oSearch": {
			"bSmart": false,
			"bRegex": true
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [2, 4]
		}]
	});
	$('select#engines').change(function () {
		dt.fnFilter($(this).val(), 0);
	});
	$(".dataTables_filter input").addClass('form-control');
});
</script>
