<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    
    <div class="row">
      <?php //echo $this->load->view("left_panel");?>
      <aside class="profile-info col-lg-9">
      
		
		
        <section>
          <div class="panel panel-primary">
            <div class="panel-heading" id="focusPass">Set New Password</div>
            
            <div class="panel-body">
			  
			  <?php if($this->session->flashdata('errorPass')){?>
              <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('errorPass');?> </div>
              <?php } ?>
              <?php if($this->session->flashdata('successPass')){?>
              <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('successPass');?> </div>
              <?php }?>
              
			  <form class="commonForm form-horizontal cmxform" role="form" enctype="multipart/form-data" method="post" name="updatePass" id="updatePass">
                <div class="form-group">
                  <label  class="col-lg-2 control-label">Current Password <span class="red">*</span></label>
                  <div class="col-lg-6">
                    <input type="password" class="form-control" id="currPassword" name="currPassword" placeholder=" ">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-lg-2 control-label">New Password <span class="red">*</span></label>
                  <div class="col-lg-6">
                    <input type="password" class="form-control" id="password1" name="password1" placeholder=" ">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-lg-2 control-label">Re-type New Password <span class="red">*</span></label>
                  <div class="col-lg-6">
                    <input type="password" class="form-control" id="repassword" name="repassword" placeholder=" ">
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                  	<input type="submit" name="updatePassword" class="btn btn-info" value="Update">
                    <button type="button" class="btn btn-default" onclick="window.location='<?php echo base_url();?>superadmin/dashboard/'">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </aside>
    </div>
    <!-- page end--> 
  </section>
</section>

<script src="<?php echo base_url()?>assets/js/validate/form-validation-state.js" ></script> 
