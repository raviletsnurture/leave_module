<?php
	$user = getAllUsers();

	$userId = $this->session->userdata('sLogin_session');
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Add Feedback
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
								<?php
					//$form_attributes = array('name' => 'addFeedback', 'id' => 'addFeedback', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
					//echo form_open(base_url().'superadmin/feedback/addFeedback',$form_attributes);

					?>
					<form class="commonForm  cmxform form-horizontal tasi-form" id="addFeedback" name="addFeedback"  method="post" action="">
					<div class="col-lg-6">
								<div class="form-group">
									<label for="firstName" class="control-label col-lg-4">Team Member <span class="red">*</span></label>
									<div class="col-lg-8">
										<select name="user_id" id="user_id"  class="form-control selectpicker js-example-basic-single" required>
											<option value="">Select Member</option>
											<?php foreach($user as $dd) { ?>
												<?php  if($dd->user_id != $loginSession[0]->user_id){?>
												<option value="<?php echo $dd->user_id; ?>" <?php $value1 = (isset($userId[0]->sId)) ? $userId[0]->sId: 0; echo selectedVal($value1,$dd->user_id); ?>>
													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
												</option>
												<?php } ?>
											<?php } ?>
										</select>
									</div>
								</div>
								</div>

								<div class="col-lg-6">
								<div class="form-group">
									<label for="date" class="control-label col-lg-4">Feedback Month<span class="red">*</span></label>
									<div class="col-lg-8">
										<div class="input-group date form_datetime-component">
											<input type="text" name="created_time" id="created_time" class="form-control" size="36" required>
										</div>
									</div>
								</div>
								</div>

								<div id="masterFeedbackFields"></div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label class="col-lg-2 control-label"> Event Remarks<span class="red">*</span></label>
									<div class="col-lg-8">
										<textarea class="form-control" id="event_remark" name="event_remark"   rows="5" cols="40" placeholder="Specify if any event attended or not" required></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label"> Comments/Remarks/Action Items (+ve/-iv)<span class="red">*</span></label>
									<div class="col-lg-8">
										<textarea class="form-control" id="feedbackcomment" name="feedbackcomment"   rows="5" cols="40" placeholder="Other concerns and comments, remarks" required></textarea>
									</div>
									<input class="form-control" id="publish_status_hidden" name="publish_status_hidden" type="hidden"  value="1"/>

									<input class="form-control" id="reviewer_id" name="reviewer_id" type="hidden"  value="<?php echo $userId[0]->sId;?>"/>

								</div>
								<div class="col-md-12 rem-pad-left" id="add-row">
									<div class="form-group">
										<div class="col-lg-2">
											<label  class="control-label">Project</label>
										</div>
										<div class="col-lg-2">
											<input class="form-control" id="project_name" name="project_name[]" type="text" placeholder="Project Name" />
										</div>
										<div class="col-lg-2">
											<select name="project_feedback[]" id="project_feedback"  class="form-control">
												<option value="" >---</option>
												<option value="1" >1</option>
												<option value="2" >2</option>
												<option value="3" >3</option>
												<option value="4" >4</option>
												<option value="5" >5</option>
											</select>
										</div>
										<div class="col-lg-2">
											<input class="form-control" id="project_comment" name="project_comment[]" type="text" placeholder="Extra Comment?" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="add_field_button btn green margintopbottom"><i class="fa fa-plus"></i> Add Project</button>
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
										<button class="btn btn-danger" type="submit" id="addFeedbackBtn">Submit Feedback</button>
									</div>
									<div class="col-lg-offset-5 col-lg-7">
										<div class="loader">
											<img src="<?php echo base_url()?>assets/img/input-spinner.gif" alt="loading...">
										</div>
									</div>
								</div>
								<div class="form-group">
		   						   <div class="col-lg-offset-2 col-lg-10">
		   							   <span class="frmmessage"></span>
		   						   </div>
		   						</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script language="javascript" type="text/javascript">
$('#created_time').datepicker({
    format: 'yyyy-mm-dd',
    endDate: '+0d',
    autoclose: true
});
$(document).ready(function() {
    var max_fields      =10; //maximum input boxes allowed
    var wrapper         = $("#add-row"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group"><label  class="col-lg-2 control-label">Project</label><div class="col-lg-2"><input class="form-control" id="project_name" name="project_name[]" type="text" placeholder="Project Name" /></div><div class="col-lg-2"><select name="project_feedback[]" id="project_feedback" class="form-control"><option value="">---</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div><div class="col-lg-2"><input class="form-control" id="project_comment" name="project_comment[]" type="text" placeholder="Extra Comment?" /></div><a href="#" class="remove_field btn red"><i class="fa fa-times"></i></a></div>');
		}});

	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$.validator.addMethod("PhoneNumberRegex", function(value, element) {
				return this.optional(element) || /^[0-9-()+ ]{8,20}$/i.test(value);
			}, "Please enter only numeric characters for your Phone number.");
	$("#addFeedback").validate({
	   ignore: [],
	   rules: {
	   },
	   messages: {
	   },
	  errorElement: "div",
	  wrapper: "div",  // a wrapper around the error message
	  errorPlacement: function(error, element) {
		  offset = element.offset();
		  error.insertAfter(element)
		  error.addClass('errormessage');  // add a class to the wrapper
		  error.css('position', 'relative');
		  //error.css('left', offset.left + element.outerWidth());
		  //error.css('top', offset.top);
	  },
	   submitHandler: function(form) {
		   var formData = $("#addFeedback").serialize();
		   $.ajax({
			   type: 'POST',
			   url: '<?php echo base_url()?>superadmin/feedback/addFeedback',
			   data: formData,
			   cache: false,
			   async:false,
			   processData: false,
			   beforeSend: function(){
				   $('.loader').show();
				   $('#addFeedbackBtn').hide();
			   },
			   success:function(data){
				  $('.loader').hide();
				  $('#addFeedbackBtn').show();
				   if(data == 1){
						$('#addFeedback')[0].reset();
						$(".frmmessage").html("Feedback added successfully!");
						$(".frmmessage").css("color","#29B6F6");
						setTimeout(function(){
						 window.location.href= '<?php echo base_url()?>superadmin/feedback';
					  },2000);
				  }else if(data == 2){
					  $(".frmmessage").html("Feedback is already given by you for this month.");
					  $(".frmmessage").css("color","#d32f2f");
				  }else{
					   $(".frmmessage").html("Your comments & Team member selection is mandatory!");
					   $(".frmmessage").css("color","#d32f2f");
				   }
					return false;
			   }
		});
	}
  });

	$("#user_id").on("change",function(){
		var formData = $("#user_id").serialize();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>superadmin/feedback/getFeedbackFields',
			data: formData,
			beforeSend: function(){
				$('.loader').show();
				$('#addFeedbackBtn').hide();
			},
			success:function(data){
			 $('.loader').hide();
			 $('#addFeedbackBtn').show();
			 $('#masterFeedbackFields').html(data);
			 $('[data-toggle="tooltip"]').tooltip();
			}
 		});
	});
});//document ready
</script>
