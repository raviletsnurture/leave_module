<?php
	$user = getAllUsers();
	$userId = $this->session->userdata('sLogin_session');

?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Edit Feedback
					</header>
						<?php if($this->session->flashdata('error')){?>
							<div class="alert alert-block alert-danger fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
							</div>
						<?php } ?>
						<?php if ($this->session->flashdata('success')) { ?>
							<div class="alert alert-success fade in">
								<button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
								<strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
							</div>
						<?php } ?>
						<div class="panel-body">
							<div class="form">
								<?php
					$form_attributes = array('name' => 'addFeedback', 'id' => 'addFeedback', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
					echo form_open(base_url().'superadmin/feedback/editFeedback',$form_attributes);

					?>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="firstName" class="control-label col-lg-4">Team Member <span class="red">*</span></label>
										<div class="col-lg-8">
											<select name="user_id" id="user_id"  class="form-control">
												<option value="">Select Member</option>
												<?php foreach($user as $dd) { ?>
													<option value="<?php echo $dd->user_id; ?>" <?php $value1 = (isset($feedback[0]->user_id)) ? $feedback[0]->user_id: 0; echo selectedVal($value1,$dd->user_id); ?>>
														<?php echo $dd->first_name.'&nbsp;'.$dd->last_name; ?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>


								<?php


								for($fk=0; $fk< count($feedback_keys); $fk++) {
									$field_name = strtolower(str_replace(' ', '_', $feedback_keys[$fk]->feedback_key));
									foreach ($feedback as $feedbackData) {
										$feedbackValues = (unserialize($feedbackData->overall_score));
										$projectValues = (unserialize($feedbackData->projects_and_ratings));
									}
									if(!empty($feedbackData->overall_score)){
									?>

									<?php if(array_key_exists($field_name,$feedbackValues)) { ?>
									<div class="col-lg-6">
										<div class="form-group">
						                	<label  class="col-lg-4 control-label"><?php echo ucfirst($feedback_keys[$fk]->feedback_key);


											?></label>
						                	<div class="col-lg-8">
												<select name="feedback_keys[<?php echo $field_name; ?>]" id="<?php echo $field_name; ?>"  class="form-control">
													<?php for($i=1; $i<=5; $i++) { ?>
														<?php if(array_key_exists($field_name,$feedbackValues) && $i == $feedbackValues[$field_name]) { ?>
																<option value="<?php echo $i;?>" selected ><?php echo $i;?></option>
														<?php }else{ ?>
																<option value="<?php echo $i;?>"><?php echo $i;?></option>
														<?php }?>

													<?php } ?>
												</select>
										  	</div>
						        		</div>
									</div>
									<?php }?>
								<?php }
								}
								 ?>

								<div class="clearfix"></div>
									<?php if(!empty($feedbackData->overall_score)){ ?>
								<div class="form-group">
									<label class="col-lg-2 control-label"> Event Remarks<span class="red">*</span></label>
									<div class="col-lg-8">
										<textarea class="form-control" id="event_remark" name="event_remark"   rows="5" cols="40" placeholder="Specify if any event attended or not"><?php echo $feedback[0]->event_remark; ?></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label"> Comments/Remarks/Action Items (+ve/-iv)<span class="red">*</span></label>
									<div class="col-lg-8">
										<textarea class="form-control" id="feedbackcomment" name="feedbackcomment"   rows="5" cols="40" placeholder="Other concerns and comments, remarks"><?php echo $feedback[0]->comment; ?></textarea>
									</div>
									<input class="form-control" id="publish_status_hidden" name="publish_status_hidden" type="hidden"  value="1"/>

									<input class="form-control" id="reviewer_id" name="reviewer_id" type="hidden"  value="<?php echo $userId[0]->sId;?>"/>


								</div>

								<div class="col-md-12 rem-pad-left" id="add-row">
									<?php for ($project=0; $project < count($projectValues[0]); $project++) { ?>
										<div class="form-group">
											<div class="col-lg-2">
												<label  class="control-label">Project</label>
											</div>
											<div class="col-lg-2">
												<input class="form-control" id="project_name" name="project_name[]" type="text" placeholder="Project Name" value="<?php echo $projectValues[0][$project]; ?>" />
											</div>
											<div class="col-lg-2">
												<select name="project_feedback[]" id="project_feedback"  class="form-control">
													<?php for($i=1; $i<=5; $i++) {
															if($projectValues[1][$project] == $i){
																	echo '<option value="'.$i.'" selected>'.$i.'</option>';
															}else{
																echo '<option value="'.$i.'" >'.$i.'</option>';
															}

													} ?>
												</select>
											</div>
											<div class="col-lg-2">
												<input class="form-control" id="project_comment" name="project_comment[]" type="text" placeholder="Extra Comment?" value="<?php echo $projectValues[2][$project]; ?>" />
											</div>
											<?php if($project!=0) {?>
												<a href="#" class="remove_field btn red"><i class="fa fa-times"></i></a>
											<?php } ?>
										</div>

									<?php }?>
								</div>
								<div class="row" style="margin-bottom:10px;">
									<div class="col-md-4 col-lg-offset-2">
										<button class="add_field_button btn green margintopbottom"><i class="fa fa-plus"></i> Add Project</button>
									</div>
								</div>

								<?php } ?>

								<!-- Ba Qa and HR remarks -->
								<div class="row">
									<div class="col-md-12 rem-pad-left"><div class="form-group">
										<label class="col-lg-2 control-label"> BA remarks<span class="red">*</span></label>
										<div class="col-lg-8">
											<textarea class="form-control" id="ba_remarks" maxlength="500" name="ba_remarks" style="resize:none;"  rows="5" cols="40" placeholder="BA team remarks"><?php $ba_remarks =  unserialize($feedback[0]->ba_remark); echo $ba_remarks[0]; ?></textarea>
											<input type="hidden" name="hidden_ba_id" value="<?php $ba_remarks =  unserialize($feedback[0]->ba_remark); echo $ba_remarks[1]; ?>">
										</div>

									</div></div>

									<div class="col-md-12 rem-pad-left"><div class="form-group">
										<label class="col-lg-2 control-label"> QA remarks<span class="red">*</span></label>
										<div class="col-lg-8">
											<textarea class="form-control" maxlength="500" id="qa_remarks" name="qa_remarks" style="resize:none;" rows="5" cols="40" placeholder="QA team remarks"><?php $qa_remarks =  unserialize($feedback[0]->qa_remark); echo $qa_remarks[0]; ?></textarea>
											<input type="hidden" name="hidden_qa_id" value="<?php $qa_remarks =  unserialize($feedback[0]->qa_remark); echo $qa_remarks[1]; ?>">
										</div>

									</div></div>

									<div class="col-md-12 rem-pad-left"><div class="form-group">
										<label class="col-lg-2 control-label"> HR remarks<span class="red">*</span></label>
										<div class="col-lg-8">
											<textarea class="form-control" id="hr_remarks" maxlength="500" name="hr_remarks" style="resize:none;" rows="5" cols="40" placeholder="HR team remarks"><?php $hr_remarks =  unserialize($feedback[0]->hr_remark); echo $hr_remarks[0]; ?></textarea>
											<input type="hidden" name="hidden_hr_id" value="<?php $hr_remarks =  unserialize($feedback[0]->hr_remark); echo $hr_remarks[1]; ?>">
										</div>

									</div></div>

								</div>
								<!-- ba qa and hr remarks -->

								<!-- Feedback id -->
								<input class="form-control" id="feedback_id" name="feedback_id" type="hidden"  value="<?php echo $feedback[0]->feedback_id;?>"/>
								<!-- feedback id -->
								<div class="form-group"></div>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">

										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
										<button class="btn btn-danger" type="submit" name="addFeedback">Update Feedback</button>

									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-leave.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function() {
    var max_fields      =10; //maximum input boxes allowed
    var wrapper         = $("#add-row"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group"><label  class="col-lg-2 control-label">Project</label><div class="col-lg-2"><input class="form-control" id="project_name" name="project_name[]" type="text" placeholder="Project Name" /></div><div class="col-lg-2"><select name="project_feedback[]" id="project_feedback" class="form-control"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div><div class="col-lg-2"><input class="form-control" id="project_comment" name="project_comment[]" type="text" placeholder="Extra Comment?" /></div><a href="#" class="remove_field btn red"><i class="fa fa-times"></i></a></div>');
		}});

	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })
});
</script>
