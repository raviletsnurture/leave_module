<?php
$user = getAllUsers();
$years = getAllFeedbackYear();

// for($i=0;$i<sizeof($permission);$i++)
// {
// 	$nm = $permission[$i]->module_name;
// 	$arr[$nm] = $permission[$i];
// }
//var_dump($arr);

if(isset($arr['users'])){
$role_id = $arr['users']->role_id;
}
?>
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.dataTables_filter {
     display: none;
}
.start_date .ui-datepicker-calendar,.end_date .ui-datepicker-calendar{
    display: none;
}
.feedbackRequired{
    color: red;
    display: none;
}
</style>
<section id="main-content">
  <section class="wrapper site-min-height">
    <section class="panel">



      <header class="panel-heading">Select User to View Feedbacks </header>
      <div class="panel-body">
        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

        <form method="post" action="<?php echo base_url()?>superadmin/feedback/viewFeedback" class="feedbackForm" id="feedbackForm">
            <div class="row">
                <div class="col-md-6">
                    <select id="selectUser" name="selectUser" class="form-control selectpicker js-example-basic-single">
                        <option value="">Select User</option>
                        <?php foreach($user as $row){?>
                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
                        <?php } ?>
                    </select>
                    <span class="feedbackRequired userField">Please select user</span>
                </div>
                <div class="col-md-6">
                    <select id="selectYear" name="selectYear" class="form-control">
                        <option value="">Select Year</option>
                        <?php foreach($years as $row1){?>
                        <option value="<?php echo $row1->year; ?>"><?php echo $row1->year;?>-<?php echo ($row1->year + 1)?></option>
                        <?php } ?>
                    </select>
                    <span class="feedbackRequired YearField">Please select Year</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <input type="checkbox" name="export_feedback" value="" id="export_feedback"> Export Feedback
                </div>
            </div>
                <div class="row">
                    <div class="col-md-6">
                    <label for="start_date">Start Month</label>
                    <input type="text" name="start_date" id="start_date" class="form-control" data-date-format="yyyy-mm-dd" disabled>
                </div>
                <div class="col-md-6">
                    <label for="end_date">End Month</label>
                    <input type="text" name="end_date" id="end_date" class="form-control" data-date-format="yyyy-mm-dd" disabled>
                </div>
            </div>

            <div class="col-lg-4">
                <div id="editable-sample_length" class="dataTables_length">
                    <button class="btn btn-info" id="editable-sample_new" type="submit"> View </button>
                    <button class="btn btn-success " id="export_button" type="submit" disabled="disabled"> Export</button>
                </div>
            </div>
        </form>

        </div>
      </div>
    </section>
    <!-- page end-->

  </section>
</section>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
//Date Picker
$(document).ready(function(){

    $('#start_date,#end_date').datepicker({
      autoClose:true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      beforeShow: function(input, inst) {
        $('#ui-datepicker-div').addClass(this.id);
        $('#ui-datepicker-div').removeClass('select_year');
      },
      onClose: function(dateText, inst) {
        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
      }
    });

    $('#export_feedback').click(function(){
        if($('#export_feedback').is(":checked")){
            $('#start_date,#end_date').prop('disabled',false);
            $('.feedbackForm #editable-sample_new').prop('disabled',true);
            $('.feedbackForm #export_button').prop('disabled',false);
        }else{
            $('#start_date,#end_date').prop('disabled',true);
            $('.feedbackForm #editable-sample_new').prop('disabled',false);
            $('.feedbackForm #export_button').prop('disabled',true);
        }
    });

    $('.feedbackForm #editable-sample_new').click(function(){

        var selectUser = $("#selectUser").val();
        var selectYear = $("#selectYear").val();
        if(selectUser == ""){
            $(".userField").show();
        }else{
            $(".userField").hide();
        }
        if(selectYear == ""){
            $(".YearField").show();
        }else{
            $(".userField").hide();
        }
        if(selectUser != "" && selectYear != ""){
            $('#feedbackForm').attr('action', "<?php echo base_url()?>superadmin/feedback/viewFeedback").submit();
        }else{
            return false;
        }
    });

    // Export File
    $('#export_button').click(function(e){
        e.preventDefault();

        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();

        var dateOne = new Date(start_date);
        var dateTwo = new Date(end_date);

        if(start_date != "" && end_date != ""){
            if(dateTwo < dateOne){
                alert('End date can\'t be lower than start date.');
                return false;
            }
        }
        $('#feedbackForm').attr('action', "<?php echo base_url()?>superadmin/feedback/exportFeedbackData").submit();
    })
});


</script>
