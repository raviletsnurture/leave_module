<section class="panel">
	<header class="panel-heading"> Paid/Unpaid Leaves </header>
	<div class="panel-body">
		<div class="adv-table editable-table ">
			 <table class="table table-striped table-hover table-bordered" id="examplenew">
				<thead>
				  <tr>
					<th>Months</th>
					<th>Paid Leaves</th>
					<th>Unpaid Leaves</th>
				  </tr>
				</thead>
				<tbody>
				<?php for ($m=1; $m<=12; $m++) {
				$year = date('Y');
				$vv = getDateDiff($userid,$m,$year);
				?>
				<tr>
					<td><?php echo date('M', mktime(0,0,0,$m)); ?></td>
					<td><?php if($vv[0]['info'] >= 1) { echo "1"; } else { echo "0"; } ?></td>
					<td><?php if($vv[0]['info'] > 1) { echo $vv[0]['info'] - 1; } else { echo "0"; } ?></td>
				</tr>
				<?php } ?>
				</tbody>
			 </table>
		</div>
	</div>
</section>
