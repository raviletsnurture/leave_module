<?php
$department = getAllDepartment();
$role = getAllRoles();
$religion = getAllReligion();
$cast = getAllCast();
$state = getAllState();

// echo "<pre>";
// print_r($users);
// exit;
?>

<style media="screen">
  #engines,#engines1,#engines2,#engines3,#engines4,#engines5,#engines6,#engines7,#engines8,#engines9{
    width:15%;
    margin: 10px 0;
  }
  .graph-section{
    width: 100%;
    height:auto;
    margin:0 0 10px 0 ;
    /*display: none;*/
  }
  #graph_detail{
    margin:10px 0;
  }
  #graph_section{
    width: 80%;
    height: 400px;
    margin-top:20px;
    border: 1px solid silver;
    border-radius: 3px;
  }
  #example_wrapper .row-fluid .span6:first-of-type{
    float: left;
    width:20%;
  }
  #example_wrapper .row-fluid .span6:last-of-type{
    float: left;
    width:80%;
  }
  #myModal .modal-body{
    overflow-y: scroll;
    height: 500px;
  }
  #myModal .modal-body::-webkit-scrollbar {
      width: 10px;
  }

  #myModal .modal-body::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(232, 64, 63,0.3);
      border-radius: 10px;
  }

  #myModal .modal-body::-webkit-scrollbar-thumb {
      border-radius: 10px;
      -webkit-box-shadow: inset 0 0 6px rgba(232, 64, 63,0.5);
  	background:rgb(232, 64, 63);
  }
  #religion_chart,#gender_chart,#cast_chart,#state_chart,#blood_group_chart,#age_chart{
    width: 100%;
    height: 300px;
    margin: 0 auto;
    border:1px solid silver;
    border-radius: 3px;
    margin: 10px 0;
  }
  table{
    width: 100% !important;
  }

  .editable-table .dataTables_filter{width: 23%;}
</style>

<section id="main-content">
  <section class="wrapper site-min-height">

    <!-- page start-->
    <?php //$this->load->view('list_header'); ?>
    <section class="panel">
      <header class="panel-heading"> All Users </header>
      <div role="grid" class="dataTables_wrapper form-inline" id="editable-sample_wrapper">
        <div class="row">
           <div class="col-lg-4">
            <div id="editable-sample_length" class="dataTables_length">
              <div class="btn-group">
                <a href="<?php echo base_url()?>superadmin/users/add">
                <button class="btn btn-info" id="editable-sample_new"> Add New <i class="fa fa-plus"></i> </button>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
           <div id="editable-sample_length" class="dataTables_length">
             <div class="btn-group">
               <a href="<?php echo base_url()?>superadmin/users/exportUsersData">
               <button class="btn btn-warning" id="editable-sample_new"> Export All Employee Data <i class="fa fa-download"></i> </button>
               </a>
             </div>
           </div>
         </div>
        </div>
      </div>
      <div class="panel-body">

      <button class="btn btn-success" id="graph_detail"> View Employee Analysis Report <i class="fa fa-angle-down"></i> </button>
        <div class="row graph-section" id="graph_portion">

          <div class="row">
            <div class="col-md-4">
              <div id="religion_chart"></div>
            </div>
            <div class="col-md-4">
              <div id="gender_chart"></div>
            </div>
            <div class="col-md-4">
              <div id="cast_chart"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div id="state_chart"></div>
            </div>
            <div class="col-md-4">
              <div id="age_chart"></div>
            </div>
            <div class="col-md-4">
              <div id="blood_group_chart"></div>
            </div>
          </div>

          <!-- show graph -->
        </div>

        <div class="adv-table editable-table ">
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>

		      <select id="engines">
            <option value="">Select Department</option>
            <?php foreach($department as $row){?>
            <option value="<?php echo $row->department_name;?>"><?php echo $row->department_name;?></option>
            <?php } ?>
          </select>

		      <select id="engines1">
            <option value="">Select Role</option>
            <?php foreach($role as $row1){?>
            <option value="<?php echo $row1->role_name;?>"><?php echo $row1->role_name; ?></option>
            <?php } ?>
          </select>

		       <select id="engines2">
            <option value="">Employee Type</option>
            <option value="Trainee">Trainee</option>
            <option value="Probation">Probation</option>
			      <option value="Permanent">Permanent</option>
          </select>
          <select id="engines3">
                <option value="">Select Religion</option>
                <?php foreach($religion as $row2){?>
                <option value="<?php echo $row2->religion_name;?>"><?php echo $row2->religion_name; ?></option>
                <?php } ?>
          </select>
          <select id="engines4">
                <option value="">Select Gender</option>
                <option value="(Male)">Male</option>
                <option value="(Female)">Female</option>
          </select>
          <select id="engines5">
                <option value="">Select Cast</option>
                <?php foreach($cast as $row3){?>
                <option value="<?php echo $row3->cast_name;?>"><?php echo $row3->cast_name; ?></option>
                <?php } ?>
          </select>
          <select id="engines6">
                <option value="">Select State</option>
                <?php foreach($state as $row4){?>
                <option value="<?php echo $row4->state_name;?>"><?php echo $row4->state_name; ?></option>
                <?php } ?>
          </select>
          <select id="engines7">
                <option value="" data-min="" data-max="">Select Age</option>
                <option value="0-20" data-min="0" data-max="20"> Less than 20 </option>
                <option value="20-25" data-min="20" data-max="25"> 20-25 </option>
                <option value="25-30" data-min="25" data-max="30"> 25-30 </option>
                <option value="30-40" data-min="30" data-max="40"> 30-40 </option>
                <option value="+40" data-min="40" data-max="100"> greater than 40 </option>
          </select>

          <select id="engines8">
                <option value="">Select Blood Group</option>
                <option value="A+">A+</option>
                <option value="A-">A- </option>
                <option value="B+">B+</option>
                <option value="B-">B-</option>
                <option value="AB+">AB+</option>
                <option value="AB-">AB-</option>
                <option value="O+">O+</option>
                <option value="O-">O-</option>
          </select>
          <select id="engines9">
                <option value="">Select Status</option>
                <option value="Active">Active</option>
                <option value="Inactive">Inactive</option>
          </select>
          <!-- Age inputs -->
          <table class="table table-striped table-hover table-bordered" id="example">
            <thead>
              <tr>
                <th>Full Name</th>
                <th>Department </th>
                <th>Role</th>
                <th>Email</th>
				        <th>Employee Type</th>
                <th>Phone</th>
                <th style="display:none;">Religion</th>
                <th style="display:none;">Gender</th>
                <th style="display:none;">Cast</th>
                <th style="display:none;">State</th>
                <th style="display:none;">Age</th>
                <th style="display:none;">BG</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($users as $row){ ?>
              <tr>
                <td onclick="getchek(<?php echo $row->user_id;?>)"><?php echo $row->first_name.' '.$row->last_name;?></td>
                <td onclick="getchek(<?php echo $row->user_id;?>)"><?php echo $row->department_name; ?></td>
                <td onclick="getchek(<?php echo $row->user_id;?>)"><?php echo $row->role_name; ?></td>
                <td onclick="getchek(<?php echo $row->user_id;?>)"><?php echo $row->email; ?></td>
		            <td onclick="getchek(<?php echo $row->user_id;?>)"><?php echo $row->employee_type; ?></td>
                <td onclick="getchek(<?php echo $row->user_id;?>)"><?php echo base64_decode($row->mobile); ?></td>
                <td style="display:none;"><?php echo $row->religion_name; ?></td>
                <td style="display:none;"><span>(<?php echo $row->gender; ?>)</span><?php echo $row->gender; ?></td>
                <td style="display:none;"><?php echo $row->cast_name; ?></td>
                <td style="display:none;"><?php echo $row->state_name; ?></td>
                <td style="display:none;"><?php echo $row->age; ?></td>
                <td style="display:none;"><?php echo $row->blood_group; ?></td>
                <td ><?php echo $row->status; ?></td>
                <td>
                  <a href="<?php echo base_url()?>superadmin/users/edit/<?php echo $row->user_id;?>" >
                  <button class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-original-title="Edit&nbsp;Users" title=""><i class="fa fa-pencil"></i></button>
                  </a>
                  <a href="<?php echo base_url()?>superadmin/users/delete/<?php echo $row->user_id;?>" class="deleteRec">
                  <button class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" data-original-title="Delete&nbsp;Users"><i class="fa fa-trash-o "></i></button>
                  </a>
        				  <a value="<?php echo $row->user_id;?>" onclick="getchek(<?php echo $row->user_id;?>)" href="#" >
        					<button class="btn btn-primary btn-xs tooltips" data-original-title="View&nbsp;User" title=""><i class="fa fa-eye"></i></button>
                  </a>

        				  <a value="<?php echo $row->user_id; ?>" onclick="getchek1(<?php echo $row->user_id; ?>)" href="#">
        					<button class="btn btn-success btn-xs tooltips" data-original-title="Leave&nbsp;Status"><i class="fa fa-check"></i></button>
                  </a>
                  <a value="<?php echo $row->user_id; ?>" onclick="sendLoginDetails(<?php echo $row->user_id; ?>)" href="#">
                  <button class="btn btn-success btn-xs tooltips" data-original-title="Send&nbsp;login"><i class="fa fa-lock"></i></button>
                  </a>
                  <a value="<?php echo $row->user_id; ?>" href="<?php echo base_url()?>superadmin/users/document/<?php echo $row->user_id; ?>">
                  <button class="btn btn-success btn-xs tooltips" data-original-title="Document"><i class="fa fa-file"></i></button>
                  </a>
      				  </td>
              </tr>
              <?php }?>
            </tbody>
          </table>

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">User Information</h4>
								<h4 class="modal-title" id="myModalLabel1">User Leave Status</h4>
						</div>
						<div class="modal-body">
						</div>
					</div>
				</div>
			</div>


        </div>
      </div>
    </section>

    <!-- page end-->

  </section>
</section>
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.css" />
<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/jquery.dataTables.js"></script> -->
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
function getchek(id)
{
	$.ajax({
		 url: '<?php echo base_url(); ?>superadmin/users/view/',
		 data: { id: id},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('.modal-body').html(data);
			 $('#myModalLabel').show();
			 $('#myModalLabel1').hide();
			 $('#myModal').modal('show');
		 }
	});
}
function getchek1(id)
{
	$.ajax({
		 url: '<?php echo base_url(); ?>superadmin/users/leavestatus/',
		 data: { id: id},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
			 $('.modal-body').html(data);
			 $('#myModalLabel1').show();
			 $('#myModalLabel').hide();
			 $('#myModal').modal('show');
		 }
	});
}

function sendLoginDetails(id)
{
	$.ajax({
		 url: '<?php echo base_url(); ?>superadmin/users/sendLoginInfo/',
		 data: { id: id},
		 dataType: 'html',
		 type: 'POST',
		 success: function(data){
       if(data == 1){
  			 $('.modal-body').html("Sent Successfully");
  			 $('#myModalLabel1').show();
  			 $('#myModalLabel').hide();
  			 $('#myModal').modal('show');
       }else{
         $('.modal-body').html("Please try again");
  			 $('#myModalLabel1').show();
  			 $('#myModalLabel').hide();
  			 $('#myModal').modal('show');
       }
		 }
	});
}

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {

        var min = parseInt( $('select#engines7 option:selected').attr('data-min'), 10 );
        var max = parseInt( $('select#engines7 option:selected').attr('data-max'), 10 );

        var age = parseFloat( data[10] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && age <= max ) ||
             ( min <= age   && isNaN( max ) ) ||
             ( min <= age   && age <= max ) )
        {
            return true;
        }
        return false;
    }
);

  $(document).ready(function() {

	var dt =  $('#example').DataTable( {
		  "aaSorting": [[ 12, "asc" ]],
       "scrollX": true
	  } );

  $("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
	$('select#engines').change( function() {
    dt.columns( 1).search($(this).val()).draw();
	} );
	$('select#engines1').change( function() {
    dt.columns(2).search($(this).val()).draw();
	} );
	$('select#engines2').change( function() {
    dt.columns(4).search($(this).val()).draw();
	} );
  $('select#engines3').change( function() {
    dt.columns(6).search($(this).val()).draw();
  } );
  $('select#engines4').change( function() {
    dt.columns(7).search($(this).val()).draw();
  } );
  $('select#engines5').change( function() {
    dt.columns(8).search($(this).val()).draw();
  } );
  $('select#engines6').change( function() {
    dt.columns(9).search($(this).val()).draw();
  } );
  $('select#engines7').change( function() {
    dt.draw();
  } );
  $('select#engines8').change( function() {
    dt.columns(11).search($(this).val()).draw();
  } );
  $('select#engines9').change( function() {
    console.log($(this).val());
    dt.columns(12).search($(this).val()).draw();
  } );

	  $(".dataTables_filter input").addClass('form-control');
  } );
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script type="text/javascript">
$('#graph_portion').slideUp();
$('#graph_detail').click(function(){
  $('#graph_portion').slideToggle( "normal");
});
// Create the chart
</script>
<script type="text/javascript">

//Religion Chart
Highcharts.chart('religion_chart', {
  chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
  },
  title: {
      text: 'Religion'
  },credits :{
      enabled:false
  },
  tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
          }
      }
  },
  series: [{
      name: 'Total',
      colorByPoint: true,
      data: [
              <?php
              $religion_data = '';
              foreach($religion_chart_data as $value){
                $religion_data .=  "{name: '$value->religion_name', y: $value->total},";
              }
              echo rtrim($religion_data,",");
              ?>
            ]
  }]
});

//Gender Chart
Highcharts.chart('gender_chart', {
  chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
  },
  title: {
      text: 'Gender'
  },credits :{
      enabled:false
  },
  tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
          }
      }
  },
  series: [{
      name: 'Total',
      colorByPoint: true,
      data: [
            <?php
            $gender_data = '';
            foreach($gender_chart_data as $value){
              $gender_data .=  "{name: '$value->gender', y: $value->total},";
            }
            echo rtrim($gender_data,",");
            ?>
        ]
  }]
});

//Cast Chart
Highcharts.chart('cast_chart', {
  chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
  },
  title: {
      text: 'Cast'
  },credits :{
      enabled:false
  },
  tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
          }
      }
  },
  series: [{
      name: 'Total',
      colorByPoint: true,
      data: [
            <?php
            $cast_data = '';
            foreach($cast_chart_data as $value){
              $cast_data .=  "{name: '$value->cast_name', y: $value->total},";
            }
            echo rtrim($cast_data,",");
            ?>
        ]
  }]
});

//State Chart
Highcharts.chart('state_chart', {
  chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
  },
  title: {
      text: 'State'
  },credits :{
      enabled:false
  },
  tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
          }
      }
  },
  series: [{
      name: 'Total',
      colorByPoint: true,
      data: [
            <?php
            $state_data = '';
            foreach($state_chart_data as $value){
              $state_data .=  "{name: '$value->state_name', y: $value->total},";
            }
            echo rtrim($state_data,",");
            ?>
        ]
  }]
});

//Age Chart
Highcharts.chart('age_chart', {
  chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
  },
  title: {
      text: 'Age'
  },credits :{
      enabled:false
  },
  tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
          }
      }
  },
  series: [{
      name: 'Total',
      colorByPoint: true,
      data: [
            <?php
            $age_data = '';
            foreach($age_group_chart_data as $value){
              foreach($value as $final_data){
                $age_data .=  "{name: '$final_data->string', y: $final_data->total},";
              }
            }
            echo rtrim($age_data,",");
            ?>
        ]
  }]
});

//Blood Group Chart
Highcharts.chart('blood_group_chart', {
  chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
  },
  title: {
      text: 'Blood Group'
  },credits :{
      enabled:false
  },
  tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
          }
      }
  },
  series: [{
      name: 'Total',
      colorByPoint: true,
      data: [
            <?php
            $blood_group_data = '';
            foreach($blood_group_chart_data as $value){
              $blood_group_data .=  "{name: '$value->blood_group', y: $value->total},";
            }
            echo rtrim($blood_group_data,",");
            ?>
        ]
  }]
});

//$blood_group_chart_data

</script>
