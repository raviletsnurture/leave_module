<?php
   $country = getAllCountry();
   $religion = getAllReligion();
   $department = getAllDepartment();
   $role = getAllRoles();
   $cast = getAllCast();
   ?>
<style>
   .datepicker{width:200px;}
</style>
<section id="main-content">
   <section class="wrapper">
      <div class="row">
         <div class="col-lg-12">
            <section class="panel">
               <header class="panel-heading">
                  <?php if(isset($usersdata[0]->user_id)){ echo "Edit Users";} else{echo "Add Users";}?>
               </header>
               <?php if($this->session->flashdata('error')){?>
               <div class="alert alert-block alert-danger fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?>
               </div>
               <?php } ?>
               <?php if($this->session->flashdata('success')){?>
               <div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
               </div>
               <?php }?>
               <div class="panel-body">
                  <div class="form">
                     <?php
                        $form_attributes = array('name' => 'addUsers', 'id' => 'addUsers', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form","enctype"=>"multipart/form-data" );
                        if(isset($usersdata[0]->user_id)){
                        	echo form_open(base_url().'superadmin/users/add/'.$usersdata[0]->user_id,$form_attributes);
                        }else{
                          echo form_open(base_url().'superadmin/users/add',$form_attributes);
                        }
                        ?>

                       <div class="form-group">

                         <div class="col-lg-6">
                           <label for="salaryslip_id" >Salary Slip Id</label>
                            <input class="form-control" id="salaryslip_id" maxlength="25" name="salaryslip_id" type="text"  placeholder="Salary Slip id" value="<?php if(isset($usersdata[0]->salaryslip_id)){ echo $usersdata[0]->salaryslip_id;} ?>" placeholder="First Name"/>
                         </div>

                          <div class="col-lg-6">
                            <label for="role_id" >Role <span class="red">*</span></label>
                             <select name="role_id" id="role_id" class="form-control m-bot15">
                                <option value="">Select Role</option>
                                <?php foreach($role as $rr){ ?>
                                <option value="<?php echo $rr->role_id; ?>" <?php $value1 = (isset($usersdata[0]->role_id)) ? $usersdata[0]->role_id : 0; echo selectedVal($value1,$rr->role_id)?>> <?php echo $rr->role_name; ?></option>
                                <?php } ?>
                             </select>
                          </div>

                          <div class="col-lg-6">
                            <label for="role_id">Employee Type <span class="red">*</span></label>
                             <select name="employee_type" id="employee_type" class="form-control m-bot15">
                               <option value="Trainee" <?php $value1 = (isset($usersdata[0]->employee_type)) ? $usersdata[0]->employee_type : 0; echo selectedVal($value1,'Trainee')?>>Trainee</option>
                                <option value="Probation" <?php $value1 = (isset($usersdata[0]->employee_type)) ? $usersdata[0]->employee_type : 0; echo selectedVal($value1,'Probation')?>>Probation</option>
                                <option value="Permanent" <?php $value1 = (isset($usersdata[0]->employee_type)) ? $usersdata[0]->employee_type : 0; echo selectedVal($value1,'Permanent')?>>Permanent</option>
                             </select>
                          </div>
                       </div>

                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="department_id">Department<span class="red">*</span></label>
                           <select name="department_id" id="department_id" class="form-control">
                              <option value="">Select Department</option>
                              <?php foreach($department as $row1){?>
                              <option value="<?php echo $row1->department_id;?>" <?php $value1 = (isset($usersdata[0]->department_id)) ? $usersdata[0]->department_id : 0; echo selectedVal($value1,$row1->department_id)?>><?php echo $row1->department_name;?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-lg-6">
                          <label for="firstname">First Name <span class="red">*</span></label>
                           <input class="form-control" id="first_name" maxlength="25" name="first_name" type="text"  value="<?php if(isset($usersdata[0]->first_name)){ echo $usersdata[0]->first_name;} ?>" placeholder="First Name"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="middlename">Middle Name</label>
                           <input class="form-control" id="middle_name" maxlength="25" name="middle_name" type="text"  value="<?php if(isset($usersdata[0]->middle_name)){ echo $usersdata[0]->middle_name;} ?>" placeholder="Middle Name"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="lastname">Last Name <span class="red">*</span></label>
                           <input class="form-control" id="last_name" maxlength="25" name="last_name" type="text"  value="<?php if(isset($usersdata[0]->last_name)){ echo $usersdata[0]->last_name;} ?>" placeholder="Last Name"/>
                        </div>
                     </div>
                     <?php if(!isset($usersdata[0]->user_id)) { ?>
                     <div class="form-group">
                        <div class="col-lg-12">
                          <label for="username">Username <span class="red">*</span></label>
                           <input class="form-control" id="username" maxlength="15" name="username" type="text"  value="<?php if(isset($usersdata[0]->username)){ echo $usersdata[0]->username;} ?>" placeholder="Username"/>
                        </div>
                     </div>
                     <?php } ?>
                     <div class="form-group">
                        <div class="col-lg-12">
                          <label for="lastname">Password <span class="red">*</span></label>
                           <input class="form-control" id="password" maxlength="20" name="password" type="password"  value="<?php if(isset($usersdata[0]->password)){ echo $usersdata[0]->password;} ?>" placeholder="Password"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-12">
                          <label for="street" >Street <span class="red">*</span></label>
                           <textarea class="form-control" id="street" name="street" rows="3" cols="40" placeholder="Street"><?php if(isset($usersdata[0]->street)){ echo $usersdata[0]->street;} ?></textarea>
                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="countryid">Country <span class="red">*</span></label>
                           <select name="country_id" id="country_id" class="form-control m-bot15">
                              <?php foreach($country as $row2){ ?>
                              <option value="<?php echo $row2->country_id; ?>" <?php $value1 = (isset($usersdata[0]->country_id)) ? $usersdata[0]->country_id : 0; echo selectedVal($value1,$row2->country_id)?>> <?php echo $row2->short_name; ?></option>
                              <?php } ?>
                           </select>
                        </div>

                        <div class="col-lg-6">
                          <label for="state_id">State <span class="red">*</span></label>
                           <select name="state_id" id="state_id" class="form-control">
                              <option value="">Select State</option>
                              <?php if(isset($usersdata[0]->state_id)) { ?>
                              <?php foreach($state as $dd){?>
                              <option value="<?php echo $dd->state_id;?>" <?php $value1 = (isset($usersdata[0]->state_id)) ? $usersdata[0]->state_id : 0; echo selectedVal($value1,$dd->state_id)?>><?php echo $dd->state_name;?></option>
                              <?php } ?>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="state_id">City <span class="red">*</span></label>
                           <select name="city_id" id="city_id" class="form-control">
                              <option value="">Select City</option>
                              <?php if(isset($usersdata[0]->city_id)) { ?>
                              <?php foreach($city as $dd1){?>
                              <option value="<?php echo $dd1->city_id;?>" <?php $value1 = (isset($usersdata[0]->city_id)) ? $usersdata[0]->city_id : 0; echo selectedVal($value1,$dd1->city_id)?>><?php echo $dd1->city_name;?></option>
                              <?php } ?>
                              <?php } ?>
                           </select>
                        </div>

                        <div class="col-lg-6">
                          <label for="zipcode">Zipcode <span class="red">*</span></label>
                           <input class="form-control" id="zipcode" maxlength="10" name="zipcode" type="text"  value="<?php if(isset($usersdata[0]->zipcode)){ echo $usersdata[0]->zipcode;} ?>" placeholder="Zipcode"/>
                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="gender">Gender <span class="red">*</span></label>
                           <select name="gender" id="gender" class="form-control">
                              <option value="">Select Gender</option>
                              <option value="Male" <?php echo isset($usersdata[0]->gender)? selectedVal($usersdata[0]->gender,"Male"): '';?> >Male</option>
                              <option value="Female" <?php echo isset($usersdata[0]->gender)? selectedVal($usersdata[0]->gender,"Female"): '';?>>Female</option>
                           </select>
                        </div>

                        <div class="col-lg-6">
                          <label>Birthday <span class="red">*</span></label>
                           <input type="text" class="form-control" readonly="readonly" id="birthdate" name="birthdate" data-date-format="yyyy-mm-dd" placeholder=" "  value="<?php  if(isset($usersdata[0]->birthdate)){ echo $usersdata[0]->birthdate;} ?>" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label>Upload Photo <span class="maroon-clr">Image Size: 400px X 400px</span></label>
                           <input type="file" class="file-pos" name="mainImage" id="mainImage" onchange="readURL(this);">
                           <?php  if(!empty($usersdata[0]->user_pic) && $usersdata[0]->user_pic != ''){ ?>
                               <input type="hidden" id="old_profile_image" value="<?php echo $usersdata[0]->user_pic; ?>">
                               <img class="old_profile_imageSub" src="<?php echo base_url().'/uploads/user_images/resized/'.$usersdata[0]->user_pic; ?>" height="100" alt="">
                           <?php } ?>
                        </div>

                        <div class="col-lg-6">
                          <label for="mobile">Mobile Number </label>
                           <input class="form-control" id="mobile" maxlength="10" name="mobile" type="text"  value="<?php if(isset($usersdata[0]->mobile)){ echo base64_decode($usersdata[0]->mobile);} ?>" placeholder="Mobile Number"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="email">Official Email <span class="red">*</span></label>
                           <input class="form-control" id="email" name="email" type="text"  value="<?php if(isset($usersdata[0]->email)){ echo $usersdata[0]->email;} ?>" placeholder="Official Email"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="gmail_id">Offical Gmail Id </label>
                           <input class="form-control" id="gmail_id" name="gmail_id" type="text"  value="<?php if(isset($usersdata[0]->gmail_id)){ echo $usersdata[0]->gmail_id;} ?>" placeholder="Offical Gmail Id"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="alternate_email">Alternate Email </label>
                           <input class="form-control" id="alternate_email" name="alternate_email" type="text"  value="<?php if(isset($usersdata[0]->alternate_email)){ echo $usersdata[0]->alternate_email;} ?>" placeholder="Alternate Email"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="blood_group">Blood Group <span class="red">*</span></label>

                           <select name="blood_group" id="blood_group" class="form-control" required>
                              <option value="">Select Blood Group</option>
                              <?php if(isset($usersdata[0]->blood_group) && $usersdata[0]->blood_group != ''){ ?>
                                <option value="A+" <?php echo selectedVal('A+',$usersdata[0]->blood_group)?>>A+</option>
                                <option value="A-" <?php echo selectedVal('A-',$usersdata[0]->blood_group)?>>A- </option>
                                <option value="B+" <?php echo selectedVal('B+',$usersdata[0]->blood_group)?>>B+</option>
                                <option value="B-" <?php echo selectedVal('B-',$usersdata[0]->blood_group)?>>B-</option>
                                <option value="AB+" <?php echo selectedVal('AB+',$usersdata[0]->blood_group)?>>AB+</option>
                                <option value="AB-" <?php echo selectedVal('AB-',$usersdata[0]->blood_group)?>>AB-</option>
                                <option value="O+" <?php echo selectedVal('O+',$usersdata[0]->blood_group)?>>O+</option>
                                <option value="O-" <?php echo selectedVal('O-',$usersdata[0]->blood_group)?>>O-</option>
                              <?php }else{?>
                                <option value="A+">A+</option>
                                <option value="A-">A- </option>
                                <option value="B+">B+</option>
                                <option value="B-">B-</option>
                                <option value="AB+">AB+</option>
                                <option value="AB-">AB-</option>
                                <option value="O+">O+</option>
                                <option value="O-">O-</option>
                              <?php }?>
                           </select>

                        </div>
                     </div>
                     <div class="form-group ">
                        <div class="col-lg-6">
                          <label for="religion_id">Religion <span class="red">*</span></label>
                           <select name="religion_id" id="religion_id" class="form-control" required>
                              <option value="">Select Religion</option>
                              <?php foreach($religion as $row){?>
                              <option value="<?php echo $row->religion_id;?>" <?php $value1 = (isset($usersdata[0]->religion_id)) ? $usersdata[0]->religion_id : 0; echo selectedVal($value1,$row->religion_id)?>><?php echo $row->religion_name;?></option>
                              <?php } ?>
                           </select>
                        </div>

                        <div class="col-lg-6">
                          <label for="cast_id">Cast <span class="red">*</span></label>
                           <select name="cast_id" id="cast_id" class="form-control" required>
                              <option value="">Select Cast</option>
                              <?php foreach($cast as $row){?>
                              <option value="<?php echo $row->cast_id;?>" <?php $value1 = (isset($usersdata[0]->cast_id)) ? $usersdata[0]->cast_id : 0; echo selectedVal($value1,$row->cast_id)?>><?php echo $row->cast_name;?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>

                     <div class="form-group">
                        <div class="col-lg-6">
                          <label>Anniversary</label>
                           <input type="text" class="form-control" readonly="readonly" id="anniversary" name="anniversary" data-date-format="yyyy-mm-dd" placeholder="Anniversary"  value="<?php  if(isset($usersdata[0]->anniversary)){ echo $usersdata[0]->anniversary;} ?>">
                        </div>

                        <div class="col-lg-6">
                          <label for="blood_group">License Number </label>
                           <input class="form-control" id="license" name="license" type="text"  value="<?php if(isset($usersdata[0]->license)){ echo base64_decode($usersdata[0]->license);} ?>" placeholder="License Number"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="ESIC_number">ESIC Number </label>
                           <input class="form-control" id="ESIC_number" name="ESIC_number" type="text"  value="<?php if(isset($usersdata[0]->ESIC_number)){ echo base64_decode($usersdata[0]->ESIC_number);} ?>" placeholder="ESIC Number"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="facebook_id">UAN No. </label>
                           <input class="form-control" id="skype" maxlength="20" name="skype" type="text"  value="<?php if(isset($usersdata[0]->skype)){ echo base64_decode($usersdata[0]->skype);} ?>" placeholder="UAN No."/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6" style="display:none;">
                          <label for="facebook_page">UDID</label>
                           <input class="form-control" id="gtalk" maxlength="20" name="gtalk" type="text"  value="<?php if(isset($usersdata[0]->gtalk)){ echo base64_decode($usersdata[0]->gtalk);} ?>" placeholder="UDID"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="pancard_no">Pancard Number</label>
                           <input class="form-control" id="pancard_no" maxlength="30" name="pancard_no" type="text"  value="<?php if(isset($usersdata[0]->pancard_no)){ echo base64_decode($usersdata[0]->pancard_no);} ?>" placeholder="Pancard Number"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="pf_no">PF Number</label>
                           <input class="form-control" id="pf_no" maxlength="30" name="pf_no" type="text"  value="<?php if(isset($usersdata[0]->pf_no)){ echo base64_decode($usersdata[0]->pf_no);} ?>" placeholder="PF Number"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                          <label for="employee_insurance_number">Employee Insurance Number </label>
                           <input class="form-control" id="employee_insurance_number" name="employee_insurance_number" type="text"  value="<?php if(isset($usersdata[0]->employee_insurance_number)){ echo base64_decode($usersdata[0]->employee_insurance_number);} ?>" placeholder="Employee Insurance Number"/>
                        </div>
                     <div class="form-group">
                         <div class="col-lg-6">
                           <label for="aadhar_no">Aadhar Number </label>
                            <input class="form-control" id="aadhar_no" name="aadhar_no" type="text"  value="<?php if(isset($usersdata[0]->aadhar_no)){ echo base64_decode($usersdata[0]->aadhar_no);} ?>" placeholder="Aadhar No"/>
                         </div>
                      </div>
                    </div>
                    <div class="panel panel-primary">
                       <div class="panel-heading" id="focusPass">Emergency Contact</div>
                    </div>
                      <div class="form-group">
                        <div class="col-lg-6">
                          <label for="landline">Emergency Name One</label>
                           <input class="form-control" id="emergency_name1" maxlength="10" name="emergency_name1" type="text"  value="<?php if(isset($usersdata[0]->emergency_name1)){ echo $usersdata[0]->emergency_name1;} ?>" placeholder="Emergency Number One"/>
                        </div>
                        <div class="col-lg-6">
                          <label for="landline">Emergency Number One</label>
                           <input class="form-control" id="landline" maxlength="10" name="landline" type="text"  value="<?php if(isset($usersdata[0]->landline)){ echo $usersdata[0]->landline;} ?>" placeholder="Emergency Number One"/>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-6">
                          <label for="landline">Emergency Name Two</label>
                           <input class="form-control" id="emergency_name2" maxlength="10" name="emergency_name2" type="text"  value="<?php if(isset($usersdata[0]->emergency_name2)){ echo $usersdata[0]->emergency_name2;} ?>" placeholder="Emergency Number Two"/>
                        </div>
                        <div class="col-lg-6">
                          <label for="landline">Emergency Number Two</label>
                           <input class="form-control" id="landline2" maxlength="10" name="landline2" type="text"  value="<?php if(isset($usersdata[0]->landline2)){ echo $usersdata[0]->landline2;} ?>" placeholder="Emergency Number Two"/>
                        </div>
                     </div>

                     <div class="panel panel-primary">
                        <div class="panel-heading" id="focusPass">Experience</div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                         <label for="ex_year">Year<span class="red">*</span></label>
                          <select name="ex_year" id="ex_year" class="form-control" required>
                           <?php for ($i=0; $i < 30; $i++) { ?>
                             <option value="<?php echo $i;?>" <?php if(isset($usersdata[0]->ex_year) && $i == $usersdata[0]->ex_year){echo 'selected="selected"';}?>><?php echo $i;?> Year</option>
                           <?php }?>
                          </select>
                        </div>
                        <div class="col-lg-6">
                         <label for="ex_month">Month<span class="red">*</span></label>
                          <select name="ex_month" id="ex_month" class="form-control" required>
                            <?php for ($i=0; $i <= 11; $i++) { ?>
                               <option value="<?php echo $i;?>" <?php if(isset($usersdata[0]->ex_month) && $i == $usersdata[0]->ex_month){echo 'selected="selected"';}?>><?php echo $i;?> Month</option>
                            <?php }?>
                          </select>
                        </div>
                     </div>

                     <div class="form-group">
                        <div class="col-lg-6">
                         <label for="joining_date">Joining<span class="red">*</span></label>
                          <input type="text" class="form-control" readonly="readonly" id="joining_date" name="joining_date" data-date-format="yyyy-mm-dd" placeholder="Joining Date"  value="<?php if(isset($usersdata[0]->joining_date) && $usersdata[0]->joining_date != '0000-00-00' && $usersdata[0]->joining_date != NULL){ echo $usersdata[0]->joining_date;} ?>" required>
                        </div>
                        <div class="col-lg-6">
                         <label for="reliving_date">Reliving</label>
                        <input type="text" class="form-control" readonly="readonly" id="reliving_date" name="reliving_date" data-date-format="yyyy-mm-dd" placeholder="Reliving Date"  value="<?php if(isset($usersdata[0]->reliving_date) && $usersdata[0]->reliving_date != '0000-00-00' && $usersdata[0]->reliving_date != NULL){ echo $usersdata[0]->reliving_date;} ?>">
                        </div>
                     </div>

                     <div class="form-group">
                        <div class="col-lg-6">
                         <label for="trainee_complete_at">Trainee Complete At</label>
                          <input type="text" class="form-control" readonly="readonly" id="trainee_complete_at" name="trainee_complete_at" data-date-format="yyyy-mm-dd" placeholder="Trainee Complete At"  value="<?php if(isset($usersdata[0]->trainee_complete_at) && $usersdata[0]->trainee_complete_at != '0000-00-00' && $usersdata[0]->trainee_complete_at != NULL){ echo $usersdata[0]->trainee_complete_at;} ?>">
                        </div>
                        <div class="col-lg-6">
                         <label for="probation_complete_at">Probation Complete At</label>
                        <input type="text" class="form-control" readonly="readonly" id="probation_complete_at" name="probation_complete_at" data-date-format="yyyy-mm-dd" placeholder="Probation Complete At"  value="<?php if(isset($usersdata[0]->probation_complete_at) && $usersdata[0]->probation_complete_at != '0000-00-00' && $usersdata[0]->probation_complete_at != NULL){ echo $usersdata[0]->probation_complete_at;} ?>">
                        </div>
                     </div>

                     <div class="panel panel-primary">
                        <div class="panel-heading" id="focusPass">Bank Account Details</div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="pf_no">Bank Name </label>
                           <input class="form-control" id="bank_name" maxlength="20" name="bank_name" type="text"  value="<?php if(isset($usersdata[0]->bank_name)){ echo base64_decode($usersdata[0]->bank_name);} ?>" placeholder="Bank Name"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="">Account Number </label>
                           <input class="form-control" id="account_number" maxlength="20" name="account_number" type="text"  value="<?php if(isset($usersdata[0]->account_number)){ echo base64_decode($usersdata[0]->account_number);} ?>" placeholder="Account Number"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-lg-6">
                          <label for="">IFSC Code</label>
                           <input class="form-control" id="ifsc_code" maxlength="20" name="ifsc_code" type="text"  value="<?php if(isset($usersdata[0]->ifsc_code)){ echo base64_decode($usersdata[0]->ifsc_code);} ?>" placeholder="IFSC Code"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="">Branch</label>
                           <input class="form-control" id="branch" maxlength="20" name="branch" type="text"  value="<?php if(isset($usersdata[0]->branch)){ echo base64_decode($usersdata[0]->branch);} ?>" placeholder="Branch"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                          <label for="">Earned Leaves</label>
                           <input class="form-control" id="earned_leaves" maxlength="3" name="earned_leaves" type="text"  value="<?php if(isset($usersdata[0]->earned_leaves)){ echo $usersdata[0]->earned_leaves;} ?>" placeholder="Earned Leaves"/>
                           <input class="form-control" id="el_earned_leaves_hidden"  name="el_earned_leaves_hidden" type="hidden"  value="<?php if(isset($usersdata[0]->earned_leaves)){ echo $usersdata[0]->earned_leaves;} ?>"/>
                        </div>

                        <div class="col-lg-6">
                          <label for="">EL Balance</label>
                           <input class="form-control" id="el_balance" maxlength="2" name="el_balance" type="text"  value="<?php if(isset($usersdata[0]->el_balance)){ echo $usersdata[0]->el_balance;} ?>" placeholder="EL Balance"/>
                           <input class="form-control" id="el_balance_hidden"  name="el_balance_hidden" type="hidden"  value="<?php if(isset($usersdata[0]->el_balance)){ echo $usersdata[0]->el_balance;} ?>"/>
                        </div>
                     </div>
                     <?php /*?>
                     <div class="form-group">
                        <label for="" class="control-label col-lg-2">EL Remarks</label>
                        <div class="col-lg-10">
                           <select name="el_remarks" id="el_remarks" class="form-control" required>
                              <option>Select a value</option>
                              <?php $remarksArray= get_el_remarks();
                                 for($idx=0; $idx<count(get_el_remarks()); $idx++){ ?>
                              <option value="<?php echo $idx;?>" ><?php echo $remarksArray[$idx];?></option>
                              <?php  } ?>
                           </select>
                        </div>
                     </div>
                     <?php */?>
                     <div class="form-group" id="el_balance_reason" style="display:none;">
                        <label for="" class="control-label col-lg-2">Specify Reason</label>
                        <div class="col-lg-10">
                           <input class="form-control" id="el_balance_reason" name="el_balance_reason" type="text"  value=""/>
                        </div>
                     </div>
                     <div class="form-group">

                        <div class="col-lg-6">
                          <label for="catStatusFlag">Status</label>
                           <select name="status" id="status" class="form-control m-bot15">
                              <option value="Active" <?php echo isset($usersdata[0]->status)? selectedVal($usersdata[0]->status,"Active"): '';?>>Active</option>
                              <option value="Inactive" <?php echo isset($usersdata[0]->status)? selectedVal($usersdata[0]->status,"Inactive"): '';?> >Inactive</option>
                           </select>
                        </div>

                        <div class="col-lg-2">
                          <label for="">
                            <input class="form-control" id="notify_mail" name="notify_mail" type="checkbox"  value="0"/>Notify via email</label>
                        </div>

                        <div class="col-lg-2">
                           <button class="btn btn-danger" type="submit" name="addEditUsers">
                           <?php if(isset($usersdata[0]->user_id)){ echo "Update";} else{echo "Submit";}?>
                           </button>
                           <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                        </div>
                     </div>
                     </form>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" />
<!--<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js" ></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url()?>assets/js/validate/form-validation-users.js" ></script>
<script>
   $('#country_id').change(function(){
       var country_id = $('#country_id').val();
       $.ajax({
               url: '<?php echo base_url();?>superadmin/users/getStates',
               type: 'POST',
               data: {
                   country_id:country_id,
               },
               success:function(data){
                   //alert(data);
   				//var obj = JSON.parse(data);
                   //alert(obj.yourfield);
   				$('#state_id').html(data);
               },
               error:function(){
                   alert('Data not Found')
               }
           });
    });
    $('#state_id').change(function(){
       var state_id = $('#state_id').val();
       $.ajax({
               url: '<?php echo base_url();?>superadmin/users/getCities',
               type: 'POST',
               data: {
                   state_id:state_id,
               },
               success:function(data){
                   //alert(data);
   				//var obj = JSON.parse(data);
                   //alert(obj.yourfield);
   				$('#city_id').html(data);
               },
               error:function(){
                   alert('Data not Found')
               }
           });

    });
    $('#el_remarks').change(function(){
      $('#el_balance_reason').hide();
       var remarks_id = $('#el_remarks').val();
       if(remarks_id ==4){
         $('#el_balance_reason').show();
       }

    });
    //edit profile image
    function readURL(input) {
        var old_profile_image = $('#old_profile_image').val();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.old_profile_imageSub')
                    .attr('src', e.target.result)
                    .width(125)
                    .height(125);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
