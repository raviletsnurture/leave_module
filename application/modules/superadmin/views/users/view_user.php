<?php $vv = gettotalleaves($user[0]->user_id);  ?>
  <div class="row-fluid">
    <div class="span9">
      <div class="new-list">
        <div class="item panel" id="myprofile">
          <table class="table myprofile_tbl">
              <tr>
                <?php if(!empty($user[0]->user_pic) || $user[0]->user_pic != ''){?>
                <img  class="userImage" src="<?php echo base_url().'/uploads/user_images/resized/'.$user[0]->user_pic; ?>" height="100" alt=""></td>              </tr>
                <?php }else{ ?>
                    <img  class="userImage" src="<?php echo base_url().'assets/img/avatar.jpg' ?>" height="100" alt=""></td>              </tr>
                <?php }?>
            </tr>
            <tr>
              <td width="250"><b>User Full Name :</b></td>
              <td><?php echo $user[0]->first_name.'&nbsp;'.$user[0]->middle_name.'&nbsp;'.$user[0]->last_name;?></td>
            </tr>
			<tr>
              <td><b>Role :</b></td>
              <td><?php echo ($user[0]->role_name)?></td>
            </tr>
			<tr>
              <td><b>Department :</b></td>
              <td><?php echo ($user[0]->department_name)?></td>
            </tr>
			<tr>
              <td><b>Username :</b></td>
              <td><?php echo ($user[0]->username)?></td>
            </tr>
            <tr>
										<td><b>Password :</b></td>
										<td><?php echo ($user[0]->password)?></td>
									</tr>
            <tr>

            <tr>
              <td><b>Country :</b></td>
              <td><?php echo ($user[0]->short_name)?></td>
            </tr>
            <tr>
              <td><b>State :</b></td>
              <td><?php echo ($user[0]->state_name)?></td>
            </tr>
            <tr>
              <td><b>City :</b></td>
              <td><?php echo ($user[0]->city_name)?></td>
            </tr>
            <tr>
              <td><b>Address :</b></td>
              <td><?php echo ($user[0]->street)?><br /><?php echo $user[0]->zipcode; ?></td>
            </tr>
            <tr>
              <td><b>Gender :</b></td>
              <td><?php echo ($user[0]->gender)?></td>
            </tr>
            <tr>
              <td><b>Birthdate :</b></td>
              <td><?php echo ($user[0]->birthdate)?></td>
            </tr>
            <tr>
              <td><b>Mobile Number :</b></td>
              <td><?php echo base64_decode($user[0]->mobile)?></td>

            </tr>
			<tr>
              <td><b>Landline Number :</b></td>
              <td><?php echo ($user[0]->landline)?></td>
            </tr>
			<tr>
              <td><b>Personal Email :</b></td>
              <td><?php echo $user[0]->email;?></td>
            </tr>
      <tr>
              <td><b>Offical Gmail Id :</b></td>
              <td><?php echo $user[0]->gmail_id;?></td>
            </tr>
			<tr>
              <td><b>Alternate Email :</b></td>
              <td><?php echo $user[0]->alternate_email;?></td>
            </tr>
			<tr>
              <td><b>Blood Group :</b></td>
              <td><?php echo $user[0]->blood_group;?></td>
            </tr>
			<tr>
              <td><b>Anniversary :</b></td>
              <td><?php echo $user[0]->anniversary;?></td>
            </tr>
			<tr>
              <td><b>License Number :</b></td>
              <td><?php echo base64_decode($user[0]->license);?></td>
            </tr>
            <tr>
              <td><b>Employee Insurance Number :</b></td>
              <td><?php echo (isset($user[0]->employee_insurance_number) && $user[0]->employee_insurance_number != '')?base64_decode($user[0]->employee_insurance_number):'';?></td>
            </tr>
            <tr>
              <td><b>Aadhar no :</b></td>
              <td><?php echo (isset($user[0]->aadhar_no) && $user[0]->aadhar_no != '')?base64_decode($user[0]->aadhar_no):'';?></td>
            </tr>

			<tr>
              <td><b>Skype :</b></td>
              <td><?php echo base64_decode($user[0]->skype);?></td>
            </tr>
			<tr>
              <td><b>Gtalk :</b></td>
              <td><?php echo base64_decode($user[0]->gtalk);?></td>
            </tr>

			<tr>
              <td><b>Pancard Number :</b></td>
              <td><?php echo base64_decode($user[0]->pancard_no);?></td>
            </tr>
			<tr>
              <td><b>Pf Number :</b></td>
              <td><?php echo base64_decode($user[0]->pf_no);?></td>
            </tr>
			<tr>
              <td><b>ESIC Number :</b></td>
              <td><?php echo base64_decode($user[0]->ESIC_number);?></td>

            </tr>
			<tr>
              <td><b>Religion :</b></td>
              <td><?php echo $user[0]->religion_name;?></td>
            </tr>
			<tr>
			  <td><b>Total Leaves :</b></td>
			  <td><?php echo $user[0]->total_leaves; ?></td>
			</tr>
			<tr>
			  <td><b>Paid Leaves :</b></td>
			  <td><?php if($vv[0]['info'] >= 1) { echo "1"; } else { echo "0"; } ?></td>
			</tr>
			<tr>
			  <td><b>Unpaid Leaves :</b></td>
			  <td><?php if($vv[0]['info'] > 1) { echo $vv[0]['info'] - 1; } else { echo "0"; } ?></td>
			</tr>
      <tr>
			  <td colspan="2"><div class="panel panel-primary">
			  <div class="panel-heading" id="focusPass">Experience</div></div></td>
			</tr>
      <tr>
			  <td><b>Joining :</b></td>
			  <td><?php echo $user[0]->joining_date; ?></td>
			</tr>
      <tr>
			  <td><b>Reliving :</b></td>
			  <td><?php echo $user[0]->reliving_date; ?></td>
			</tr>
      <tr>
			  <td><b>Year :</b></td>
			  <td><?php echo $user[0]->ex_year; ?></td>
			</tr>
      <tr>
			  <td><b>Month :</b></td>
			  <td><?php echo $user[0]->ex_month; ?></td>
			</tr>
			<tr>
			  <td colspan="2"><div class="panel panel-primary">
			  <div class="panel-heading" id="focusPass">Bank Account Details</div></div></td>
			</tr>
			<tr>
              <td><b>Bank Name :</b></td>
              <td><?php echo base64_decode($user[0]->bank_name);?></td>
            </tr>

			<tr>
              <td><b>Account Number :</b></td>
              <td><?php echo base64_decode($user[0]->account_number);?></td>
            </tr>
			<tr>
              <td><b>Branch :</b></td>
              <td><?php echo base64_decode($user[0]->branch);?></td>
            </tr>
			<tr>
              <td><b>IFSC Code :</b></td>
              <td><?php echo base64_decode($user[0]->ifsc_code);?></td>
            </tr>
            <tr>
      			  <td colspan="2"><div class="panel panel-primary">
      			  <div class="panel-heading" id="focusPass">Skill Experience</div></div></td>
      			</tr>
            <?php foreach ($skills as $skilldata) { ?>
              <tr>
                <td><b><?php echo $skilldata->name;?> :</b></td>
                <td><?php echo $skilldata->ex_year;?> Year <?php echo $skilldata->ex_month;?> Month</td>
              </tr>
            <?php }?>
          </table>
        </div>
      </div>
    </div>
  </div>
