<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <aside class="profile-info col-lg-12">
        <section class="panel">
          <div class="panel-body bio-graph-info">
            <h1>Document Information</h1>
          </div>
        </section>
        <section class="panel">
          <div class="panel-body bio-graph-info">
            <div class="row">
                <div class="col-lg-12">
                  <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="fa fa-bullhorn"></i> Document</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($documentDatas as $documentData) { ?>
                        <tr>
                          <td><?php echo $documentData->document_name;?></td>
                          <td width="15%">
                            <a href="<?php echo base_url(); ?>uploads/document/<?php echo $documentData->file_name;?>" target="_blank" class="btn btn-success"><i class="fa fa-eye"></i> View </a>
                            <button type="button" onclick="delete_document(<?php echo $documentData->document_id; ?>)" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </button>
                          </td>
                        </tr>
                      <?php }?>
                    </tbody>
                </table>
                </div>
            </div>
          </div>
        </section>
      </aside>
    </div>
  </section>
</section>
<script>
function delete_document(document_id){
	var document_id = document_id;
  var r = confirm("Are you sure delete this document?");
	if (r == true){
  	$.ajax({
  	 url: '<?php echo base_url(); ?>superadmin/users/deleteDocument',
  	 data: { document_id: document_id},
  	 dataType: 'html',
  	 type: 'POST',
  	 success: function(data){
  		 alert("Document delete successfully!");
  		 location.reload();
  		 }
  	});
  }
}
</script>
