<?php
$user = getAllUsers();
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Time Log upload
          </header>
          <?php if($this->session->flashdata('error')){?>
          <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
          <?php } ?>
          <?php if($this->session->flashdata('success')){?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
          <?php }?>
          <div class="panel-body">
            <div class="form">
        <?php
			  $form_attributes = array('name' => 'uploadTimeLog', 'id' => 'uploadTimeLog', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form");
	      echo form_open_multipart(base_url().'superadmin/timelog/addTimeLog',$form_attributes);
        ?>
                  <div class="form-group">
                    <label class="col-lg-2 control-label">Upload zip file</label>
                    <div class="col-lg-6">
                      <input type="file" class="file-pos" name="timeLogZip" id="timeLogZip">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button class="btn btn-danger" type="submit" name="addEditSalarySlip">
                      <?php if(isset($salaryEdit[0]->upload_id)){ echo "Update";} else{echo "Submit";}?>
                      </button>
                  	  <button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
