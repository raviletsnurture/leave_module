<?php
$user = getAllUsers();

?>
<section id="main-content">
          <section class="wrapper">
            <div class="row">

        			<div class="col-lg-12">
        				<section class="panel">
        					<header class="panel-heading">Time Log
                    <div class="btn-group" style="float: right;">
                      <a href="<?php echo base_url().'superadmin/timelog/addTimeLog';?>">
                      <button class="btn btn-info" id="editable-sample_new"> Upload Time Log <i class="fa fa-plus"></i> </button>
                      </a>
                    </div>
                  </header>
                  <div class="panel-body">
                  <?php if($this->session->flashdata('error')){?>
                  <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Oh snap!</strong> <?php echo $this->session->flashdata('error');?> </div>
                  <?php } ?>
                  <?php if($this->session->flashdata('success')){?>
                  <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button"> <i class="fa fa-times"></i> </button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');?> </div>
                  <?php }?>

        							<div class="alert alert-block alert-danger fade in" style="display:none;">
        								<strong>Sorry</strong> no time log on this day.
        							</div>
        							<div class="col-lg-8">
          								<?php
          									$form_attributes = array('name' => 'getTimeLog', 'id' => 'getTimeLog', 'autocomplete' => 'off',"class"=>"commonForm  cmxform form-horizontal tasi-form" );
          									echo form_open(base_url().'timelog/add',$form_attributes);
          								?>
                          <div class="form-group">
          									<label class="col-lg-2 control-label">Select Users</label>
                            <div class="col-md-6">
                                <select id="selectUser" name="selectUser" class="form-control selectpicker js-example-basic-single" required="">
                                  <option value="">Select Users</option>
                                  <?php foreach($user as $row){?>
                                  <option value="<?php echo $row->user_id; ?>"><?php echo $row->first_name.'&nbsp;'.$row->last_name;?></option>
                                  <?php } ?>
                                </select>
                            </div>
                          </div>
          								<div class="form-group">
          									<label class="col-lg-2 control-label">Start Date</label>
          									<div class="col-lg-2">
          										<input type="text" class="form-control" readonly="readonly" id="start_date" name="start_date" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-01'); ?>">
          									</div>
          									<label  class="col-lg-2 control-label">End Date</label>
          									<div class="col-lg-2">
          										<input type="text" class="form-control" readonly="readonly" id="end_date" name="end_date" data-date-format="yyyy-mm-dd" placeholder="" value="<?php echo date('Y-m-d'); ?>">
          									</div>
          								</div>
          								<div class="form-group">
          									<div class="col-lg-offset-2 col-lg-4">
          										<button class="btn btn-danger" type="submit" name="checkLog" id="checkLog">
          											Submit
          										</button>
                            </div>
                            <div class="col-lg-4 puul-right">
                              <button class="btn btn-success" type="button" name="exportLateEntry" id="exportLateEntry">
                                Exprort Late Entry
          										</button>
                            </div>
          								</div>
          							</form>
                      </div>
                      <div class="col-lg-2 col-sm-12 text-center">
                      </div>
                      <div class="col-lg-2 col-sm-12 text-center">
                            <div class="easy-pie-charts">
                              <h4>Working Hours</h4>
                                <div class="percentage easyPieChart" data-percent="0" style="width: 135px; height: 135px; line-height: 135px; margin-left: 50px;"><span>0</span> Hours<canvas width="135" height="135"></canvas></div>
                            </div>
                      </div>
        					</div>
        				</section>
        			</div>
        		</div>

                <div class="row">
                  <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">Leave Detail</header>
                            <div class="panel-body" id="leaveDetails">
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">Late Entry<em style="font-size:14px; color: #c3c3c3;"> (After 10 AM)</em></header>
                            <div class="panel-body" id="lateEntryDetails">
                            </div>
                        </section>
                    </div>
                </div>

            <div id="timeLogData"></div>
      </section>
  </section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar/foundation-datepicker.js"></script>
<script type="text/javascript">
	$('#start_date,#end_date').datepicker({
    endDate: "1d",
   	autoclose: true,
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#checkLog').click(function(event){
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();

			// Work on holiday only allowed in Sat and Sun
			var week_day_start = new Date(start_date);
			var week_day_end = new Date(end_date);

			var week_start = week_day_start.getDay();
			var week_end = week_day_end.getDay();

			//Date Comparision
			var date_one = new Date(start_date);
			var date_two = new Date(end_date);

			if(date_two >= date_one){

				event.preventDefault();
				var formData = $("#getTimeLog").serialize();
				$.ajax({
					type: 'POST',
					data: formData,
					url: '<?php echo base_url()?>superadmin/timelog/getLogByDate',
					cache: false,
					async:false,
					processData: false,
					success:function(data){
            if(data == '1'){
              $("#timeLogData").html("");
              $(".alert-block").show();
              $(".alert-block").html("Please select user.");
            }else if(data == '0'){
              $("#timeLogData").html("");
              $(".alert-block").show();
              $(".alert-block").html("<strong>Sorry</strong> no time log on this day.");
            }else{
              $("#timeLogData").html(data);
              $(".alert-block").hide();
                $.ajax({
                  type: 'POST',
                  data: formData,
                  url: '<?php echo base_url()?>superadmin/timelog/getTimeLogTotal',
                  cache: false,
                  async:false,
                  processData: false,
                  success:function(data){
                    var json = $.parseJSON(data);
                    $('.easyPieChart').data('easyPieChart').update(json.totalTime);
                    $('.easyPieChart span').html(json.totalTimeHours);
                   return false;
                  }
                });
            }
					 return false;
					}
 				});

            // Get leave Details
            $.ajax({
                type: 'POST',
                data: formData,
                url: '<?php echo base_url()?>superadmin/timelog/getLeaveDetailsOnAjax',
                cache: false,
                async:false,
                processData: false,
                success:function(data){
                    var json = $.parseJSON(data);
                    var leaveData = json['leaveData'];
                    $('#leaveDetails').html(leaveData);
                }
            });


            // Get late entry Details
            $.ajax({
                type: 'POST',
                data: formData,
                url: '<?php echo base_url()?>superadmin/timelog/getLateEntryDetailsOnAjax',
                cache: false,
                async:false,
                processData: false,
                success:function(data){
                    var json = $.parseJSON(data);
                    var lateEntryData = json['lateEntryData'];
                    if(lateEntryData != 0){
                      $('#lateEntryDetails').html('<p> Total : '+lateEntryData+'</p>');
                    }else{
                      $('#lateEntryDetails').html('<p> No late entry Found</p>');
                  }
                }
            });
		}else {
			alert('End Date must be greater then Start Date');
			event.preventDefault();
		}
  });//onclick

  // exportLateEntry
  // Kaushal Adhiya
  // 01-01-2019
  // Export File
  $('#exportLateEntry').click(function(e){
      e.preventDefault();

      var start_date = $('#start_date').val();
      var end_date = $('#end_date').val();

      var dateOne = new Date(start_date);
      var dateTwo = new Date(end_date);

      if(start_date != "" && end_date != ""){
          if(dateTwo < dateOne){
              alert('End date can\'t be lower than start date.');
              return false;
          }
      }
      $('#getTimeLog').attr('action', "<?php echo base_url()?>superadmin/timelog/exportLateEntry").submit();
  });
});//document.ready
</script>
