<section id="main-content">
    <section class="wrapper">
            <div class="row">
                <aside class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">Meeting Request Status
                            <a class="btn btn-success pull-right" href="<?php echo base_url()?>superadmin/meeting/add"><i class="glyphicon glyphicon-plus"></i> Add New</a>
                            <a class="btn btn-success pull-right" href="<?php echo base_url()?>superadmin/meeting/statuslist" style=" margin-right: 12px;"><i class="glyphicon glyphicon-plus"></i> Meeting list</a>
                        </header>
                        <div class="panel-body">
                            <form class="commonForm cmxform form-horizontal">
                                <div class="col-lg-12">
                                  <div class="form-group">
                                      <label  class="col-lg-2 control-label">Month <span class="red">*</span></label>
                                      <div class="col-lg-6">
                                        <select name="month" id="month" class="form-control">
                                          <?php
                                          $currMonth = date("m - Y");
                                          $loopMonth = $currMonth;
                                          echo '<option value=" ">Select month</option>';
                                          for($i = 1; $i <= 12; $i++){
                                            echo '<option value="'.date("Y-m", mktime(null, null, null, $i, 1)).'">'.date("M - Y", mktime(null, null, null, $i, 1)).'</option>';
                                          }
                                          ?>
                                        </select>
                                      </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </aside>
            </div>
            <div class="row meetingHtml"></div>
    </section>
</section>

<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#month").on("change",function(){
        var date = $(this).val();
		   $.ajax({
			   type: 'POST',
			   url: '<?php echo base_url()?>superadmin/meeting/getMonthMeeting',
			   data: 'date='+date,
			   cache: false,
			   async:false,
			   processData: false,
			   beforeSend: function(){
				   $('.loader').show();
			   },
			   success:function(data){
				  $('.loader').hide();
				   if(data){
						$(".meetingHtml").html(data);
                   }else{
					   $(".meetingHtml").html('<div class="col-lg-12"><section class="panel"><header class="panel-heading text-center">No meeting this month</header></section></div>');
			      }
					return false;
			   }
		});
    });
});//document ready
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(document).on("click",".btnUpdate", function(){

       var formid = $(this).closest(".frmUpdate").attr("id");
       $("#"+formid).validate({
       	   ignore: [],
       	   rules: {
       	   },
       	   messages: {
       	   },
       	  errorElement: "div",
       	  wrapper: "div",  // a wrapper around the error message
       	  errorPlacement: function(error, element) {
       		  offset = element.offset();
       		  error.insertAfter(element)
       		  error.addClass('errormessage');  // add a class to the wrapper
       		  error.css('position', 'relative');
       		  //error.css('left', offset.left + element.outerWidth());
       		  //error.css('top', offset.top);
       	  },
       	   submitHandler: function(form) {

               var formData =$("#"+formid).serialize();
        	   $.ajax({
        		   type: 'POST',
        		   url: '<?php echo base_url()?>superadmin/meeting/updateMeetingStatus',
        		   data: formData,
        		   cache: false,
        		   async:false,
        		   processData: false,
        		   beforeSend: function(){
        			   $('.loader').show();
        		   },
        		   success:function(data){
        			  $('.loader').hide();
        			   if(data == 1){
                           alert("Meeting update successfully!");
        					$(".frmmessage").html("Meeting set successfully!");
        					$(".frmmessage").css("color","#29B6F6");
        					setTimeout(function(){
        					  //window.location.href= '<?php echo base_url()?>meeting';
			                },2000);
        			  }else if(data == 2){
                          alert("This person had already meeting.");
        				   $(".frmmessage").html("This person had already meeting.");
        				   $(".frmmessage").css("color","#d32f2f");
        		     }else{
                         alert("Please try again!");
        				   $(".frmmessage").html("Please try again!");
        				   $(".frmmessage").css("color","#d32f2f");
        		      }
        				return false;
        		   }
        	});
         }
     });
    });
});//document ready
</script>
