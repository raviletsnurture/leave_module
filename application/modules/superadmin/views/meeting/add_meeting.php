<?php
$loginSession = $this->session->userdata("sLogin_session");
$user = getAllUsers();

?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Add Meeting
					</header>

						<div class="panel-body">
							<div class="form">
					<form class="commonForm  cmxform form-horizontal tasi-form" id="addMeeting" name="addMeeting"  method="post" action="">
                        <input type="hidden" value="<?php echo $loginSession[0]->sId?>" name="huser_id" id="huser_id">

			                    <div class="col-lg-12">
    								<div class="form-group">
    									<label for="firstName" class="control-label col-lg-2">Team Member <span class="red">*</span></label>
    									<div class="col-lg-6">
    										<select name="user_id" id="user_id" class="form-control selectpicker js-example-basic-single" required>
    											<option value="">Select Member</option>
    											<?php foreach($user as $dd) { ?>
                                                    <?php  if($dd->user_id != $loginSession[0]->user_id){?>
    												<option value="<?php echo $dd->user_id; ?>">
    													<?php echo $dd->first_name.'&nbsp;'.$dd->last_name;?>
    												</option>
                                                    <?php } ?>
    											<?php } ?>
    										</select>
    									</div>
    								</div>
								</div>

                                <div class="col-lg-6">
    								<div class="form-group">
    									<label for="date" class="control-label col-lg-4">Date<span class="red">*</span></label>
    									<div class="col-lg-8">
    										<div class="input-group date form_datetime-component">
                                            	<input type="text" name="created_time" id="created_time" class="form-control" size="36" required>
    										</div>
    									</div>
    								</div>
								</div>
                                <div class="col-lg-6">
    								<div class="form-group">
    									<label for="date" class="control-label col-lg-4">Time<span class="red">*</span></label>
    									<div class="col-lg-8">
                                            <select name="meeting_time" id="meeting_time" class="form-control" required>
    											<option value="">Time</option>
												<option value="09:00">09:00</option>
                                                <option value="09:15">09:15</option>
                                                <option value="09:30">09:30</option>
                                                <option value="09:45">09:45</option>
                                                <option value="10:00">10:00</option>
                                                <option value="10:15">10:15</option>
                                                <option value="10:30">10:30</option>
                                                <option value="10:45">10:45</option>
                                                <option value="11:00">11:00</option>
                                                <option value="11:15">11:15</option>
                                                <option value="11:30">11:30</option>
                                                <option value="11:45">11:45</option>
                                                <option value="12:00">12:00</option>
                                                <option value="12:15">12:15</option>
                                                <option value="12:30">12:30</option>
                                                <option value="12:45">12:45</option>
												<option value="01:00">01:00</option>
                                                <option value="01:15">01:15</option>
                                                <option value="01:30">01:30</option>
                                                <option value="01:45">01:45</option>
												<option value="02:00">02:00</option>
                                                <option value="02:15">02:15</option>
                                                <option value="02:30">02:30</option>
                                                <option value="02:45">02:45</option>
												<option value="03:00">03:00</option>
                                                <option value="03:15">03:15</option>
                                                <option value="03:30">03:30</option>
                                                <option value="03:45">03:45</option>
												<option value="04:00">04:00</option>
                                                <option value="04:15">04:15</option>
                                                <option value="04:30">04:30</option>
                                                <option value="04:45">04:45</option>
												<option value="05:00">05:00</option>
                                                <option value="05:15">05:15</option>
                                                <option value="05:30">05:30</option>
                                                <option value="05:45">05:45</option>
												<option value="06:00">06:00</option>
                                                <option value="06:15">06:15</option>
                                                <option value="06:30">06:30</option>
                                                <option value="06:45">06:45</option>
												<option value="07:00">07:00</option>
                                                <option value="07:15">07:15</option>
                                                <option value="07:30">07:30</option>
                                                <option value="07:45">07:45</option>
    										</select>
    									</div>
    								</div>
								</div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12">
                                  <div class="form-group">
                                      <label class="control-label col-lg-2">Subject<span class="red">*</span></label>
                                      <div class="col-lg-10">
                                          <input type="text" placeholder="Subject" id="Subject" class="form-control" required name="Subject">
                                      </div>
                                  </div>
                                 </div>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-default" onclick="goBack('1')" type="button">Cancel</button>
										<button class="btn btn-danger" type="submit" id="addMeetingBtn">Submit Meeing</button>
									</div>
									<div class="col-lg-offset-5 col-lg-7">
										<div class="loader">
											<img src="<?php echo base_url()?>assets/img/input-spinner.gif" alt="loading...">
										</div>
									</div>
								</div>
								<div class="form-group">
		   						   <div class="col-lg-offset-2 col-lg-10">
		   							   <span class="frmmessage"></span>
		   						   </div>
		   						</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.js"></script>

<script language="javascript" type="text/javascript">
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('#created_time').datepicker({
    format: 'yyyy-mm-dd',
    daysOfWeekDisabled: [0,6],
    startDate: today,
    autoclose: true
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$.validator.addMethod("PhoneNumberRegex", function(value, element) {
				return this.optional(element) || /^[0-9-()+ ]{8,20}$/i.test(value);
			}, "Please enter only numeric characters for your Phone number.");
	$("#addMeeting").validate({
	   ignore: [],
	   rules: {
	   },
	   messages: {
	   },
	  errorElement: "div",
	  wrapper: "div",  // a wrapper around the error message
	  errorPlacement: function(error, element) {
		  offset = element.offset();
		  error.insertAfter(element)
		  error.addClass('errormessage');  // add a class to the wrapper
		  error.css('position', 'relative');
		  //error.css('left', offset.left + element.outerWidth());
		  //error.css('top', offset.top);
	  },
	   submitHandler: function(form) {
		   var formData = $("#addMeeting").serialize();
		   $.ajax({
			   type: 'POST',
			   url: '<?php echo base_url()?>superadmin/meeting/addMeeting',
			   data: formData,
			   cache: false,
			   async:false,
			   processData: false,
			   beforeSend: function(){
				   $('.loader').show();
				   $('#addMeetingBtn').hide();
			   },
			   success:function(data){
				  $('.loader').hide();
				  $('#addMeetingBtn').show();
				   if(data == 1){
						$('#addMeeting')[0].reset();
						$(".frmmessage").html("Meeting set successfully!");
						$(".frmmessage").css("color","#29B6F6");
						setTimeout(function(){
						 window.location.href= '<?php echo base_url()?>superadmin/meeting';
					  },2000);
				  }else if(data == 2){
					   $(".frmmessage").html("This person had already meeting.");
					   $(".frmmessage").css("color","#d32f2f");
			     }else{
					   $(".frmmessage").html("Please try again!");
					   $(".frmmessage").css("color","#d32f2f");
			      }
					return false;
			   }
		});
	}
  });
});//document ready
</script>
