<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <aside class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">Meeting Request
                        <a class="btn btn-success pull-right" href="<?php echo base_url()?>superadmin/meeting/add"><i class="glyphicon glyphicon-plus"></i> Add New</a>
                        <a class="btn btn-success pull-right" href="<?php echo base_url()?>superadmin/meeting/statuslist" style=" margin-right: 12px;"><i class="glyphicon glyphicon-plus"></i>Meeting Status </a>
                    </header>
                    <div class="panel-body">
                        <div id="leavecalendar" class="has-toolbar"></div>
                    </div>
                </section>
            </aside>
        </div>

        <div id="eventContent" title="Event Details">
            <div id="eventInfo"></div>
            <p><strong><a id="eventLink" target="_blank"></a></strong></p>
        </div>
    </section>
</section>

<script>
$(document).ready(function() {
    $('#scroll-announcements').niceScroll({
        styler: "fb",
        cursorcolor: "#e8403f",
        cursorwidth: '6',
        cursorborderradius: '10px',
        background: '#404040',
        spacebarenabled: false,
        cursorborder: '',
        zindex: '1000'
    });

	var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#leavecalendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar !!!

        events: [
			<?php for($i=0;$i<sizeof($meetings);$i++) { ?>
            {
				title: '<?php echo $meetings[$i]['first_name'].' '.$meetings[$i]['last_name']; ?>',
                start: '<?php echo $meetings[$i]['schedule_date']; ?> 00:00:00',
                end: '<?php echo $meetings[$i]['schedule_date']; ?> 00:00:00',
				description: '<div>Subject: <?php echo $meetings[$i]['subject']; ?><br />Time: <?php echo $meetings[$i]['schedule_time']; ?><br  />Status: <?php if($meetings[$i]['status'] == 1){echo 'Accepted';}elseif ($meetings[$i]['status'] == 2){echo 'Reject';}else{echo 'Inactive';}?></div>',
				backgroundColor: '<?php if($meetings[$i]['status'] == 1){echo '#6DBB4A';}elseif ($meetings[$i]['status'] == 2){echo '#FF6C60';}else{echo '#53BEE6';}?>',
				/*imageurl: 'img',*/
				textColor: '#fff',
			},
			<?php } ?>

        ],
		eventRender: function(event, eventElement) {
			if (event.imageurl)
			{
				eventElement.find("div.fc-event-inner").prepend("<i class='fa fa-comment' style='font-size:16px;'></i>&nbsp;");
			}
			if (event.image)
			{
				eventElement.find("div.fc-event-inner").prepend("<i class='fa fa-smile-o' style='font-size:16px;'></i>&nbsp;");
				var dateString = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
				$('.fc-day[data-date="'+ dateString + '"]').addClass('orange');
			}

		},
		eventClick:  function(event, jsEvent, view) {
			if(!event.image && !event.imageurl) {
			$("#eventInfo").html(event.description);
			$("#eventLink").attr('href', event.url);
			$("#eventContent").dialog({ modal: true, title: event.title });
			}
		},
		/*eventMouseover: function(event, jsEvent, view) {
			$('.fc-event-inner', this).append('<div id=\"'+event.id+'\" class=\"hover-end\">'+event.description+'</div>');
		},
		eventMouseout: function(event, jsEvent, view) {
			$('#'+event.id).remove();
		}*/
    });
});
</script>
