<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Form16 extends MX_Controller {
	function __construct(){
		parent::__construct();

		$this->template->set('controller', $this);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('download');
		$this->load->helper('general_helper');
		$this->load->model('form16_model');
		user_auth(0);
	}
	function index(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$data['titalTag'] = ' - Form16 Slips';

		$data["form16"] = $this->form16_model->getAllForm16Uploads($id);

		auditUserActivity('Form16', 'Index', $id, date('Y-m-d H:i:s'),$id);

		//var_dump($data); die;
	  $this->template->load_partial('dashboard_master','form16/list_form16uploads',$data);
	}

	function logaudit(){
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		$userClicked = $this->input->post('user_id');
		$userUpload = $this->input->post('upload_id');

		if($userClicked != $id) {
			auditUserActivity('Form16', 'ALERT: download not allowed', $id, date('Y-m-d H:i:s'),'#');
			$data ="fake";
			}else{
			$data = $this->form16_model->updateDownloadedAt($userClicked, $userUpload);
			auditUserActivity('Form16', 'download', $userClicked, date('Y-m-d H:i:s'),$id);
			$data ="OK";
		  }
    echo $data;
		//$data["form16"] = $this->form16_model->getAllForm16Uploads($id);
		//var_dump($data); die;

	}

	function forceForm16Download(){

		header('Content-Disposition: attachment; filename="file.pdf"');
		$userClicked = $this->input->get('user_id');
		$userUpload = $this->input->get('upload_id');
		$userData = $this->logSession;
		$id = $userData[0]->user_id;
		if($id != $userClicked){
			redirect('/form16', 'refresh');
			exit;
		}
		$myForm16 = $this->form16_model->getForm16Slip($userUpload, $userClicked);
		//print_r($myForm16);
		foreach($myForm16 as $row){
			$fileDownload = base_url().'uploads/form16_slips/'.$row->year.'/'.$row->month.'/'.$row->filename;
		}
		$pathDownload = file_get_contents($fileDownload);
		$nameFile = 'Form16Slip.pdf';
		force_download($nameFile,$pathDownload);
		//print_r($fileDownload); exit;

	}


}
?>
