<?php
class Form16_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getAllForm16Uploads($user_id)
	{
		$this->db->select('u.first_name, u.last_name, d.department_name, l.*');
		$this->db->from('form_16 as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->join('department as d','d.department_id = u.department_id', 'left');
		$this->db->where('l.user_id',$user_id);
		$this->db->order_by("l.year","desc");
		$this->db->order_by("l.upload_id","desc");
		$query=$this->db->get();
		return $query->result();
	}

	function updateDownloadedAt($user_id, $upload_id)
	{
		$whereCondition = array('user_id'=>$user_id,'upload_id'=>$upload_id);
		$data =array('last_accessed'=>date('Y-m-d H:i:s'));
		$this->db->where($whereCondition);
		if($this->db->update('form_16',$data)){
			return true;
		}else{
			return false;
		}
	}

	function getForm16Slip($upload_id, $user_id)
	{
		$this->db->select('l.year, l.month, l.filename, u.user_id');
		$this->db->from('form_16 as l');
		$this->db->join('users as u','u.user_id = l.user_id', 'left');
		$this->db->where('l.upload_id',$upload_id);
		$this->db->order_by("l.year","desc");
		$this->db->order_by("l.upload_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
}
?>
