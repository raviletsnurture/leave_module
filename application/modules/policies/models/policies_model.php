<?php
class policies_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->template->set('controller', $this);
		$this->load->database();
	}

	function getLoggedInUser($id){
		$ci = & get_instance();
		$ci->db->where('user_id', $id);
		$query = $ci->db->get('users');
		return $query->result();
	}


	function getAllPolicies()
	{
		$this->db->select("*");
		$this->db->from("crm_policies");
		$this->db->where("status = '0'");
		$result = $this->db->get()->result_array();
		return $result;
	}

}
?>
