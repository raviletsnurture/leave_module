
<section id="main-content">
          <section class="wrapper">
              <div class="row">
                <div class="col-sm-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Policies
                          </header>
                          <div class="panel-body">
                            <?php  foreach ($policies as $policie) { ?>
                              <a style="margin-bottom: 10px; margin-right: 10px;" class="btn btn-success" href="<?php echo base_url();?>uploads/policy/<?php echo $policie['policies_file_name'];?>" target="_blank"><i class="fa fa-eye"></i>  <?php echo $policie['policies_name'];?></a>
                            <?php }?>
                          </div>
                      </section>
                  </div>
              </div>
          </section>
      </section>
