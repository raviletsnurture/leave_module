<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Policies extends MX_Controller {
  var $logSession;
	function __construct(){
		parent::__construct();
    $this->template->set('controller', $this);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		user_auth(0);
		$this->load->database();
		$this->logSession = $this->session->userdata("login_session");
		$this->load->helper('general_helper');
		$this->load->model('policies_model');
	}

	function index(){
		$data['titalTag'] = ' - Policie List';
		$data["policies"] = $this->policies_model->getAllPolicies();

    $userData = $this->logSession;
		$id = $userData[0]->user_id;
    auditUserActivity('Policy', 'Index', $id, date('Y-m-d H:i:s'),$id);

    $this->template->load_partial('dashboard_master','policies/index',$data);


	}

}
?>
