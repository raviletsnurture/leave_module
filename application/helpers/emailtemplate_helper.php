<?php
function birthdayTemplates($homeurl,$name){

	$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			   <head>
			      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			      <title>Happy Birthday ;)</title>
			      <style type="text/css">
			         body {
			         padding-top: 0 !important;
			         padding-bottom: 0 !important;
			         padding-top: 0 !important;
			         padding-bottom: 0 !important;
			         margin:0 !important;
			         width: 100% !important;
			         -webkit-text-size-adjust: 100% !important;
			         -ms-text-size-adjust: 100% !important;
			         -webkit-font-smoothing: antialiased !important;
			         }
			         .tableContent img {
			         border: 0 !important;
			         display: block !important;
			         outline: none !important;
			         }
			         .tableContent .contentEditable{text-align: center;}
			         .tableContent .contentEditable img{
			         display: contents !important;
			         }
			         a{
			         color:#382F2E;
			         }
			         p, h1,ul,ol,li,div{
			         margin:0;
			         padding:0;
			         }
			         td,table{
			         vertical-align: top;
			         }
			         td.middle{
			         vertical-align: middle;
			         }
			         a.link1{
			         color:#ffffff;
			         font-size:14px;
			         text-decoration: none;
			         }
			         a.link2{
			         font-size:13px;
			         color:#999999;
			         text-decoration:none;
			         line-height:19px;
			         }
			         .bigger{
			         font-size: 24px;
			         }
			         .bgBody{
			         background: #dddddd;
			         }
			         .bgItem{
			         background: #ffffff;
			         }
			         h2{
			         font-family:Georgia;
			         font-size:36px;
			         text-align:center;
			         color:#B57801;
			         font-weight: normal;
			         }
			         p{
			         color:#ffffff;
			         }
			         @media only screen and (max-width:480px)
			         {
			         table[class="MainContainer"], td[class="cell"]
			         {
			         width: 100% !important;
			         height:auto !important;
			         }
			         td[class="specbundle"]
			         {
			         width: 100% !important;
			         float:left !important;
			         font-size:13px !important;
			         line-height:17px !important;
			         display:block !important;
			         padding-bottom:15px !important;
			         }
			         td[class="specbundle2"]
			         {
			         width:90% !important;
			         float:left !important;
			         font-size:14px !important;
			         line-height:18px !important;
			         display:block !important;
			         padding-bottom:10px !important;
			         padding-left:5% !important;
			         padding-right:5% !important;
			         }
			         td[class="spechide"]
			         {
			         display:none !important;
			         }
			         img[class="banner"]
			         {
			         width: 100% !important;
			         height: auto !important;
			         }
			         td[class="left_pad"]
			         {
			         padding-left:15px !important;
			         padding-right:15px !important;
			         }
			         }
			         @media only screen and (max-width:540px)
			         {
			         table[class="MainContainer"], td[class="cell"]
			         {
			         width: 100% !important;
			         height:auto !important;
			         }
			         td[class="specbundle"]
			         {
			         width: 100% !important;
			         float:left !important;
			         font-size:13px !important;
			         line-height:17px !important;
			         display:block !important;
			         padding-bottom:15px !important;
			         }
			         td[class="specbundle2"]
			         {
			         width:90% !important;
			         float:left !important;
			         font-size:14px !important;
			         line-height:18px !important;
			         display:block !important;
			         padding-bottom:10px !important;
			         padding-left:5% !important;
			         padding-right:5% !important;
			         }
			         td[class="spechide"]
			         {
			         display:none !important;
			         }
			         img[class="banner"]
			         {
			         width: 100% !important;
			         height: auto !important;
			         }
			         td[class="left_pad"]
			         {
			         padding-left:15px !important;
			         padding-right:15px !important;
			         }
			         }
			      </style>
			   </head>
			   <body paddingwidth="0" paddingheight="0" class="bgBody"  style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
			      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style="font-family:helvetica, sans-serif;">
			         <tbody>
			            <tr>
			               <td>
			                  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer">
			                     <tbody>
			                        <tr>
			                           <td class="movableContentContainer">
			                              <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
			                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                    <tbody>
			                                       <tr>
			                                          <td height="22" bgcolor="#272727"></td>
			                                       </tr>
			                                       <tr>
			                                          <td bgcolor="#272727">
			                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                                <tbody>
			                                                   <tr>
			                                                      <td valign="top" width="20" class="spechide">&nbsp;</td>
			                                                      <td>
			                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                                            <tbody>
			                                                               <tr>
			                                                                  <td valign="top" width="340" class="specbundle2">
			                                                                     <div class="contentEditableContainer contentImageEditable">
			                                                                        <div class="contentEditable" >
			                                                                           <img src="http://www.letsnurture.com/wp-content/themes/letsnurture/images/logo.png" data-max-width="340" alt="letsnurture">
			                                                                        </div>
			                                                                     </div>
			                                                                  </td>
			                                                               </tr>
			                                                            </tbody>
			                                                         </table>
			                                                      </td>
			                                                      <td valign="top" width="20" class="spechide">&nbsp;</td>
			                                                   </tr>
			                                                </tbody>
			                                             </table>
			                                          </td>
			                                       </tr>
			                                       <tr>
			                                          <td height="22" bgcolor="#272727"></td>
			                                       </tr>
			                                    </tbody>
			                                 </table>
			                              </div>
			                              <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
			                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                    <tr>
			                                       <td height="22" bgcolor="#272727"></td>
			                                    </tr>
			                                    <tr>
			                                       <td bgcolor="#B57801">
			                                          <div class="contentEditableContainer contentImageEditable">
			                                             <div class="contentEditable" >
			                                                <img class="banner" src="'.$homeurl.'uploads/images/bigImg.jpg" data-default="placeholder" data-max-width="600" width="600" height="400" alt="Happy Birthday!" border="0">
			                                             </div>
			                                          </div>
			                                       </td>
			                                    </tr>
			                                 </table>
			                              </div>
			                              <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
			                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#B57801">
			                                    <tr>
			                                       <td height="55" colspan="3"></td>
			                                    </tr>
			                                    <tr>
			                                       <td width="125"></td>
			                                       <td>
			                                          <table width="350" border="0" cellspacing="0" cellpadding="0">
			                                             <tr>
			                                                <td>
			                                                   <div class="contentEditableContainer contentTextEditable">
			                                                      <div style="font-family:Georgia;text-align:center;" class="contentEditable" >
			                                                         <p style="font-size:36px;color:#ffffff;">Happy Birthday! '.$name.'</p>
			                                                      </div>
			                                                   </div>
			                                                </td>
			                                             </tr>
			                                             <tr>
			                                                <td height="25"></td>
			                                             </tr>
			                                             <tr>
			                                                <td>
			                                                   <div class="contentEditableContainer contentTextEditable">
			                                                      <div style="font-family:Georgia;font-size:15px;line-height:17px;text-align:center;" class="contentEditable" >
			                                                         <p>It seems such a great day to say we feel so lucky that you came our way! Happy Birthday to you! Make it grand!.</p>
			                                                      </div>
			                                                   </div>
			                                                </td>
			                                             </tr>
			                                          </table>
			                                       </td>
			                                       <td width="125"></td>
			                                    </tr>
			                                    <tr>
			                                       <td height="55" colspan="3"></td>
			                                    </tr>
			                                 </table>
			                              </div>
			                           </td>
			                        </tr>
			                     </tbody>
			                  </table>
			               </td>
			            </tr>
			         </tbody>
			      </table>
			   </body>
			</html>';
			return $body;
  }


function AnniversaryTemplates($homeurl,$name){

	$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			   <head>
			      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			      <title>Happy Birthday ;)</title>
			      <style type="text/css">
			         body {
			         padding-top: 0 !important;
			         padding-bottom: 0 !important;
			         padding-top: 0 !important;
			         padding-bottom: 0 !important;
			         margin:0 !important;
			         width: 100% !important;
			         -webkit-text-size-adjust: 100% !important;
			         -ms-text-size-adjust: 100% !important;
			         -webkit-font-smoothing: antialiased !important;
			         }
			         .tableContent img {
			         border: 0 !important;
			         display: block !important;
			         outline: none !important;
			         }
			         .tableContent .contentEditable{text-align: center;}
			         .tableContent .contentEditable img{
			         display: contents !important;
			         }
			         a{
			         color:#382F2E;
			         }
			         p, h1,ul,ol,li,div{
			         margin:0;
			         padding:0;
			         }
			         td,table{
			         vertical-align: top;
			         }
			         td.middle{
			         vertical-align: middle;
			         }
			         a.link1{
			         color:#ffffff;
			         font-size:14px;
			         text-decoration: none;
			         }
			         a.link2{
			         font-size:13px;
			         color:#999999;
			         text-decoration:none;
			         line-height:19px;
			         }
			         .bigger{
			         font-size: 24px;
			         }
			         .bgBody{
			         background: #dddddd;
			         }
			         .bgItem{
			         background: #ffffff;
			         }
			         h2{
			         font-family:Georgia;
			         font-size:36px;
			         text-align:center;
			         color:#B57801;
			         font-weight: normal;
			         }
			         p{
			         color:#ffffff;
			         }
			         @media only screen and (max-width:480px)
			         {
			         table[class="MainContainer"], td[class="cell"]
			         {
			         width: 100% !important;
			         height:auto !important;
			         }
			         td[class="specbundle"]
			         {
			         width: 100% !important;
			         float:left !important;
			         font-size:13px !important;
			         line-height:17px !important;
			         display:block !important;
			         padding-bottom:15px !important;
			         }
			         td[class="specbundle2"]
			         {
			         width:90% !important;
			         float:left !important;
			         font-size:14px !important;
			         line-height:18px !important;
			         display:block !important;
			         padding-bottom:10px !important;
			         padding-left:5% !important;
			         padding-right:5% !important;
			         }
			         td[class="spechide"]
			         {
			         display:none !important;
			         }
			         img[class="banner"]
			         {
			         width: 100% !important;
			         height: auto !important;
			         }
			         td[class="left_pad"]
			         {
			         padding-left:15px !important;
			         padding-right:15px !important;
			         }
			         }
			         @media only screen and (max-width:540px)
			         {
			         table[class="MainContainer"], td[class="cell"]
			         {
			         width: 100% !important;
			         height:auto !important;
			         }
			         td[class="specbundle"]
			         {
			         width: 100% !important;
			         float:left !important;
			         font-size:13px !important;
			         line-height:17px !important;
			         display:block !important;
			         padding-bottom:15px !important;
			         }
			         td[class="specbundle2"]
			         {
			         width:90% !important;
			         float:left !important;
			         font-size:14px !important;
			         line-height:18px !important;
			         display:block !important;
			         padding-bottom:10px !important;
			         padding-left:5% !important;
			         padding-right:5% !important;
			         }
			         td[class="spechide"]
			         {
			         display:none !important;
			         }
			         img[class="banner"]
			         {
			         width: 100% !important;
			         height: auto !important;
			         }
			         td[class="left_pad"]
			         {
			         padding-left:15px !important;
			         padding-right:15px !important;
			         }
			         }
			      </style>
			   </head>
			   <body paddingwidth="0" paddingheight="0" class="bgBody"  style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
			      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style="font-family:helvetica, sans-serif;">
			         <tbody>
			            <tr>
			               <td>
			                  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer">
			                     <tbody>
			                        <tr>
			                           <td class="movableContentContainer">
			                              <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
			                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                    <tbody>
			                                       <tr>
			                                          <td height="22" bgcolor="#272727"></td>
			                                       </tr>
			                                       <tr>
			                                          <td bgcolor="#272727">
			                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                                <tbody>
			                                                   <tr>
			                                                      <td valign="top" width="20" class="spechide">&nbsp;</td>
			                                                      <td>
			                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                                            <tbody>
			                                                               <tr>
			                                                                  <td valign="top" width="340" class="specbundle2">
			                                                                     <div class="contentEditableContainer contentImageEditable">
			                   <div class="contentEditable" ></div>
			                                                                     </div>
			                                                                  </td>
			                                                               </tr>
			                                                            </tbody>
			                                                         </table>
			                                                      </td>
			                                                      <td valign="top" width="20" class="spechide">&nbsp;</td>
			                                                   </tr>
			                                                </tbody>
			                                             </table>
			                                          </td>
			                                       </tr>
			                                       <tr>
			                                          <td height="22" bgcolor="#272727"></td>
			                                       </tr>
			                                    </tbody>
			                                 </table>
			                              </div>
			                              <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
			                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                    <tr>
			                                       <td height="22" bgcolor="#272727"></td>
			                                    </tr>
			                                    <tr>
			                                       <td bgcolor="#B57801">
			                                          <div class="contentEditableContainer contentImageEditable">
			                                             <div class="contentEditable" >
			                                                <img class="banner" src="'.$homeurl.'uploads/images/anniversary.png" data-default="placeholder" data-max-width="600" width="600" height="400" alt="Happy Birthday!" border="0">
			                                             </div>
			                                          </div>
			                                       </td>
			                                    </tr>
			                                 </table>
			                              </div>
			                              <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
			                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#B57801">
			                                    <tr>
			                                       <td height="55" colspan="3"></td>
			                                    </tr>
			                                    <tr>
			                                       <td width="125"></td>
			                                       <td>
			                                          <table width="350" border="0" cellspacing="0" cellpadding="0">
			                                             <tr>
			                                                <td>
			                                                   <div class="contentEditableContainer contentTextEditable">
			                                                      <div style="font-family:Georgia;text-align:center;" class="contentEditable" >
			                                                         <p style="font-size:36px;color:#ffffff;">Happy Anniversary! '.$name.'</p>
			                                                      </div>
			                                                   </div>
			                                                </td>
			                                             </tr>
			                                             <tr>
			                                                <td height="25"></td>
			                                             </tr>
			                                             <tr>
			                                                <td>
			                                                   <div class="contentEditableContainer contentTextEditable">
			                                                      <div style="font-family:Georgia;font-size:15px;line-height:17px;text-align:center;" class="contentEditable" >
			                                                         <p>just wanted to wish you a happy Anniversary and it seems today is full of blessings and happiness for you!.</p>
			                                                      </div>
			                                                   </div>
			                                                </td>
			                                             </tr>
			                                          </table>
			                                       </td>
			                                       <td width="125"></td>
			                                    </tr>
			                                    <tr>
			                                       <td height="55" colspan="3"></td>
			                                    </tr>
			                                 </table>
			                              </div>
			                           </td>
			                        </tr>
			                     </tbody>
			                  </table>
			               </td>
			            </tr>
			         </tbody>
			      </table>
			   </body>
			</html>';
			return $body;
  }
