<?php
// Function for sorting listing by diatance start
function aasort (&$array, $key,$limit)
{
	$sorter=array();
	$ret=array();
	reset($array);

	foreach ($array as $ii => $va){
		$sorter[$ii]=$va[$key];
	}

	asort($sorter);
	$ll = 0;
	foreach ($sorter as $ii => $va){
		$ret[$ii]=$array[$ii];

		if($limit == $ll+1 && $limit != ''){break;}
		$ll++;
	}
	return $array=$ret;
}


function getLastMonths($num) {
	$months = array();
	for($m = 0 ; $m < $num ; $m ++){
		$months[] = date("Y-m",strtotime("-".$m." Months"));
	}
	return $months;
}

function graphMonth($date) {
	$date = explode('-',$date);
	$year = substr($date[0], -2);
	$monthNum = $date[1];
	$monthName = substr(date('F', mktime(0, 0, 0, $monthNum, 10)),0,3);
	return $monthName.$year;
}

if ( ! function_exists('remove_files')){
  function remove_files($path){
	$files = glob($path); // get all file names
	foreach($files as $file){ // iterate files
	  if(is_file($file)){
		  unlink($file); // delete file
	  }
	}
  }
}
function objToArray($post_id){
	$result = array();
	$i = 0;
	foreach ($post_id as $key => $value) {
		$result[] = (array) $value;
	$i++;
	}
	return $result;
}
function max_with_key($array, $key) {
//	$array = (array) $array;
	$array = objToArray($array);
	//pr($array);
	if (!is_array($array) || count($array) == 0) return false;
	$max = $array[0][$key];
	foreach($array as $a) {
		if($a[$key] > $max) {
			$max = $a[$key];
		}
	}
	return $max;
}
function setLastUrl(){
//	$CI->session->set_userdata('user_login_session',$data1);
	$CI = & get_instance();
	$url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$CI->session->set_userdata('lastUrl',$url);
}
function getLastUrl_(){
	$CI = & get_instance();
	return $CI->session->userdata('lastUrl');
}


function checkout_auth(){
	$CI = & get_instance();
	if(!$CI->session->userdata("user_login_session") && !$CI->session->userdata('cartSession')){
		redirect(base_url().'shop');exit;
	}
}

// Ravi adding this for upload if filepath don't exist please create it
function makeDir($path)
{	
     return is_dir($path) || mkdir($path, 0777, TRUE);
}

function generate_password( $length = 8 ) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%";
	$password = substr( str_shuffle( $chars ), 0, $length );
	return $password;
}

function savePDF($fieldName,$path,$allowExt,$encodedName){

	$temp = explode(".", $_FILES[$fieldName]["name"]);
	$extension = end($temp);
	$fileName = $encodedName.'.'.$extension;
	$fileTmpName = $_FILES[$fieldName]["tmp_name"];
	$fileError = $_FILES[$fieldName]["error"];

	if (in_array($extension, $allowExt))
	{		
	  if ($fileError > 0)
	  {
			return false;
		  }
		else{
			$tmpPath = $path .'/'. $fileName;
			$moved = move_uploaded_file($fileTmpName, $tmpPath);			
			return $fileName;
	  }
	} 
	else {
	  return false;
	}
}

?>
