<?php

// Name : Md Mashroor
// Description : For calculating top 3 reward winners forever
function getTop5rewardWinner()
{
	$CI = & get_instance();
	$CI->db->select("first_name,last_name,reward_points");
	$CI->db->from("crm_users");
	$CI->db->where("status != 'Inactive'");
	$CI->db->order_by("reward_points","desc");
	$CI->db->limit(3);
	$dd = $CI->db->get()->result_array();
	return $dd;
}
// Name : Md Mashroor
// Description : For calculating top 3 reward winners monthly
function getToprewardWinnerMonthly(){
	$month = date('m');
	$year = date('Y');
	$CI = & get_instance();
	$CI->db->select("u.first_name, u.last_name, SUM(rr.point) as totalpoints");
	$CI->db->from('crm_reward_history as rr');
	$CI->db->join('crm_users as u','rr.user_id = u.user_id', 'left');
	$CI->db->where("month(`date`) = $month");
	$CI->db->where("year(`date`) = $year");
	$CI->db->where("rr.status = '1'");
	$CI->db->group_by('rr.user_id');
	$CI->db->order_by('totalpoints', 'desc');
	$CI->db->limit(3);
	$result = $CI->db->get()->result();
	return $result;
}
function getUserByid($id){
	$CI = & get_instance();
	$CI->db->select("*");
	$CI->db->from("crm_users");
	$CI->db->where("status != 'Inactive'");
	$CI->db->where("user_id =",$id);
	$dd = $CI->db->get()->result_array();
	return $dd;
}
function validateUsername($POST){
	$CI = & get_instance();
	$CI->load->model('registration/registrationmodel');
	$data['userId'] = trim($POST['userId']);
	$dataold['usernameOld'] = '';
	if(isset($_POST['usernameOld']) && $_POST['usernameOld'] != ''){
		$dataold['usernameOld'] = trim($POST['usernameOld']);
	}
	$checkUserId = $CI->registrationmodel->verify_userid($data);

	if($checkUserId > 0){
		if($dataold['usernameOld'] == $data['userId']){
			echo "true";
		}else{
			echo 'false';
		}
	}else{
		echo "true";
	}
}
function validateShopUsername($POST){
	$CI = & get_instance();
	$CI->load->model('registration/registrationmodel');
	$data['uUserId'] = trim($POST['uUserId']);
	$dataold['usernameOld'] = '';
	if(isset($_POST['usernameOld']) && $_POST['usernameOld'] != ''){
		$dataold['usernameOld'] = trim($POST['usernameOld']);
	}
	$checkUserId = $CI->registrationmodel->verify_userid($data);

	if($checkUserId > 0){
		if($dataold['usernameOld'] == $data['uUserId']){
			echo "true";
		}else{
			echo 'false';
		}
	}else{
		echo "true";
	}
}
function refValidateUsername($POST){
	$CI = & get_instance();
	$CI->load->model('referrer/referrermodel');
	$data['rUserName'] = trim($POST['rUserName']);
	$dataold['rUserNameOld'] = '';
	if(isset($_POST['rUserNameOld']) && $_POST['rUserNameOld'] != ''){
		$dataold['rUserNameOld'] = trim($POST['rUserNameOld']);
	}
	$checkUserId = $CI->referrermodel->verify_userid($data);

	if($checkUserId > 0){
		if($dataold['rUserNameOld'] == $data['rUserName']){
			echo "true";
		}else{
			echo 'false';
		}
	}else{
		echo "true";
	}
}
function getLeadType()
{
	$CI = & get_instance();
	$CI->db->order_by('leadType', 'ASC');
	$query = $CI->db->get('lead_type');
	return $query->result();
}
function getImage($path,$imgName,$params){
	$distImage =  $path.$imgName;
    if(file_exists($distImage) && $imgName != ''){
	    echo '<img src="'.base_url().$distImage.'" alt="" '.$params.'> ';
    }else{
		echo '<img src="'.base_url().'assets/img/avatar.jpg" alt="" '.$params.'>';
	}
}
function getProductImageThumb($path,$imgName,$params){
	$productImage =  $path.$imgName;
    if(file_exists($productImage) && $imgName != ''){
	    echo '<img src="'.base_url().$productImage.'" alt="" '.$params.'> ';
    }
}

function getProductImage($path,$imgName,$params){
	$productImage =  $path.$imgName;
    if(file_exists($productImage) && $imgName != ''){
	    echo '<img src="'.base_url().$productImage.'" alt="" '.$params.'> ';
    }else{
		echo '<img src="'.base_url().'assets/img/noimage.png" alt="" '.$params.'>';
	}
}
function distDataAuth($logDistId,$editDistId,$url){
	if($logDistId != $editDistId){
		redirect(base_url().$url);
	}
}
function activeSidebar($uri,$matchStr){
	if($uri == $matchStr){
		return "active";
	}else{
		return '';
	}
}
function getAge($then) {
    $then_ts = strtotime($then);
    $then_year = date('Y', $then_ts);
    $age = date('Y') - $then_year;
    if(strtotime('+' . $age . ' years', $then_ts) > time()) $age--;
    return $age+1;
}
function getDays($end) {
  $start = date('Y-m-d');
  $start_ts = strtotime($start);
  $end_ts = strtotime($end);
  $diff = $end_ts - $start_ts;
  return round($diff / (60*60*24));
}
function getAvatar(){
	echo base_url().'assets/img/avatar.jpg';
}
function getLoggedInUser($id){
	$ci = & get_instance();
	$ci->db->where('user_id', $id);
	$query = $ci->db->get('users');
	return $query->result();
}
function getRefLoggedInUser($id){
	$ci = & get_instance();
	$ci->db->where('referrId', $id);
	$query = $ci->db->get('referrers');
	return $query->result();
}
function getShopLoggedInUser($id){
	$ci = & get_instance();
	$ci->db->where('uId', $id);
	$query = $ci->db->get('user');
	return $query->result();
}
function getSaLoggedInUser(){
	$ci = & get_instance();
	$query = $ci->db->get('super_admin');
	return $query->result();
}
function getLeadStatus()
{
	$ci = & get_instance();
	$ci->db->order_by('leadStatus', 'ASC');
	$query = $ci->db->get('lead_status');
	return $query->result();
}
function getOpportunityType()
{
	$CI = & get_instance();
	$CI->db->order_by('opportunityType', 'ASC');
	$query = $CI->db->get('opportunity_type');
	return $query->result();
}
function getOpportunityStatus()
{
	$ci = & get_instance();
	$ci->db->order_by('opportunityStatus', 'ASC');
	$query = $ci->db->get('opportunity_status');
	return $query->result();
}
function getActionType()
{
	$CI = & get_instance();
	$CI->db->order_by('actionType', 'ASC');
	$query = $CI->db->get('action_type');
	return $query->result();
}
function getActionStatus()
{
	$ci = & get_instance();
	$ci->db->order_by('actionStatus', 'ASC');
	$query = $ci->db->get('action_status');
	return $query->result();
}
function getCurrentUrl()
{
	return "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}
// disabled cache
function noocache()
{
	// set expire time to past date/time remove from cache and not store in cache
	header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache, no-store, must-revalidate, max-age=0');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
}
// enabled cache
function yescache()
{
	// seconds, minutes, hours, days
	$expires = 60 * 60 * 24 * 14;
	header("Pragma: public");
	header("Cache-Control: maxage=" . $expires);
	header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
}

function timeAmPm($time){
	return date('h:i:s a', strtotime($time));
}

function toBirthYear($date){
	//echo $date .' ';
	$years = getAge($date);
	return date("Y-m-d", strtotime('+'.$years.' years', strtotime($date)));
	//return false;
}
function toBirthDate($date){
	if($date != '')
		return date("jS, M Y", strtotime($date));
	else
		return '';
}


function getCountryName($cid){
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where('country_id',$cid);
	$query = $CI->db->get('country');
	$cName = $query->result();

	if($cid != 0 && $cid != NULL){
		echo $cName[0]->short_name;
	}else{
		echo "";
	}
}




/*------------------------------------------DEESHIT-------------------------*/
function getAllCountry()
{
	$CI = & get_instance();
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('country');
	return $query->result();
}

function getAllSelectedCountry()
{
	$CI = & get_instance();
	$query = $CI->db->where('status','Active');
	$query = $CI->db->order_by('short_name','desc');
	$query = $CI->db->get('country');
	return $query->result();
}


function getAllState()
{
	$CI = & get_instance();
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('state');
	return $query->result();
}

function getAllDepartment()
{
	$CI = & get_instance();
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('department');
	return $query->result();
}
function getAllCustomer()
{
	$CI = & get_instance();
	$query = $CI->db->get('customers');
	return $query->result();
}
function getAllProject()
{
	$CI = & get_instance();
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('projects');
	return $query->result();
}
function getAllReligion()
{
	$CI = & get_instance();
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('religion');
	return $query->result();
}
function getAllCast()
{
	$CI = & get_instance();
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('cast');
	return $query->result();
}

function getAllUsers()
{
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	return $query->result();
}

function getAllUsersWithInactive()
{
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	return $query->result();
}

function getAllInterviewCategory()
{
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','1');
	$query = $CI->db->get('req_designation');
	return $query->result();
}

function getAllUsersExceptTl($department_id)
{
	 $CI = & get_instance();

	// $query = $CI->db->select('*');
	// $query = $CI->db->where('status','Active');
	// $CI->db->order_by("first_name","asc");
	// $query = $CI->db->get('users');

	$prefix = $CI->db->dbprefix;
	$sql = "SELECT * from ".$prefix."users WHERE status='Active'
					AND user_id NOT IN (SELECT user_id FROM ".$prefix."users WHERE department_id=".$department_id." AND role_id = 13) ";
	$query = $CI->db->query($sql);
	return $query->result();
}

function getRoleUser($user_id)
{
	$CI = & get_instance();
	$query = $CI->db->select('role_id');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->where('user_id',$user_id);
	$query = $CI->db->get('users');
	return $query->result();
}



function getAllLeaveStatus()
{
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('leave_status');
	return $query->result();
}
function getAllLeaveType()
{
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('leave_type');
	return $query->result();
}
function getAllRoles()
{
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('role');
	return $query->result();
}
function getAllCustomerStatus()
{
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->get('customer_status');
	return $query->result();
}
function getUserpermission($id)
{
	$CI = & get_instance();
	$query = $CI->db->select('p.*,r.*');
	$query = $CI->db->from("role as r");
	$query = $CI->db->join('role_permission as p','p.role_id = r.role_id','left');
	$query = $CI->db->where('r.role_id',$id);
	$query = $CI->db->get();
	return $query->result();
}
function getBirthday($lim = '10'){
	$CI = & get_instance();
	//$logSession = $CI->session->userdata("login_session");
  	//$logDistId = $logSession[0]->distId;
	$CI->db->select("*,CONCAT(first_name ,' ',last_name) as full_name,DATEDIFF( birthdate , NOW()) as BirthDayDiff , DATE_FORMAT(birthdate, '%d' ) as bday ", false);
	$CI->db->from("users");
	$CI->db->where("DATE_ADD(birthdate, INTERVAL YEAR(CURDATE()) - YEAR(birthdate) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)");
	$CI->db->where("MONTH(birthdate) = MONTH(NOW())");
	$CI->db->where('status','Active');
	$CI->db->order_by("birthdate","asc");
	$leadsBirthDay = $CI->db->get()->result_array();
	return $leadsBirthDay;
}

function getLeaveCountByUser($userid, $leavetype){
	$CI = & get_instance();
	//$logSession = $CI->session->userdata("login_session");
  	//$logDistId = $logSession[0]->distId;
	$CI->db->select("SUM(l.leave_days) as leave_sum, lt.leave_amount as leave_amount", false);
	$CI->db->from("crm_leaves as l");
	$CI->db->join('crm_leave_type as lt','lt.leave_type_id = l.leave_type');
	$CI->db->where('l.user_id',$userid);
	$CI->db->where('l.leave_type',$leavetype);
	$CI->db->where("YEAR(leave_start_date) = YEAR(NOW())");
	$leave_sum = $CI->db->get()->result_array();
	return $leave_sum;
}


function getDateDiff($id,$month,$year)
{
	$CI = & get_instance();
	$query = $CI->db->select("sum(DATEDIFF(`leave_end_date`,`leave_start_date`) +1) as info",false);
	$query = $CI->db->from("leaves");
	$query = $CI->db->where('YEAR(leave_start_date) = '.$year.' and MONTH(leave_end_date) = '.$month.' and leave_status = 2  and is_cancelled != 1 and (leave_type =3 or leave_type =4) and user_id ='.$id);
	$dd = $CI->db->get()->result_array();
	return $dd;
}

function getAllUserBirthday()
{
	$CI = & get_instance();
	$query = $CI->db->select("CONCAT(first_name ,' ',last_name) as fullname, MONTH(birthdate) as month, DAY(birthdate) as day",false);
	$query = $CI->db->from("users");
	$query = $CI->db->where('status','Active');
	$dd = $CI->db->get()->result_array();
	return $dd;
}

function getTodayUserBirthday()
{
	$CI = & get_instance();
	$query = $CI->db->select("*");
	$query = $CI->db->from("users");
	$query = $CI->db->where('MONTH(birthdate) = MONTH(NOW()) AND DAY(birthdate) = DAY(NOW())');
	$query = $CI->db->where('status','Active');
	$dd = $CI->db->get()->result_array();
	return $dd;
}

function getTodayUserAnniversary()
{
	$CI = & get_instance();
	$query = $CI->db->select("*");
	$query = $CI->db->from("users");
	$query = $CI->db->where('MONTH(anniversary) = MONTH(NOW()) AND DAY(anniversary) = DAY(NOW())');
	$query = $CI->db->where('status','Active');
	$dd = $CI->db->get()->result_array();
	return $dd;
}



function getAllPredefineLeave()
{
	$CI = & get_instance();
	$query = $CI->db->select("*");
	$query = $CI->db->from("holidays_lists");
	$query = $CI->db->where('status','0');
	$dd = $CI->db->get()->result_array();
	return $dd;
}
function gettotalleaves($id)
{
	$CI = & get_instance();
	$query = $CI->db->select("sum(DATEDIFF(`leave_end_date`,`leave_start_date`) +1) as info",false);
	$query = $CI->db->from("leaves");
	$query = $CI->db->where('status IN ("Paid","Unpaid") and user_id ='.$id);
	$dd = $CI->db->get()->result_array();
	return $dd;
}
function getProjectMilestones()
{
	$start_date = firstOfMonth();
	$end_date = lastOfMonth();
	$CI = & get_instance();
	$CI->db->select("p.project_name,m.*");
	$CI->db->from("project_milestone as m");
	$CI->db->join("projects as p","p.project_id = m.project_id","left");
	$CI->db->where("m.start_date >= '".$start_date."' AND m.end_date < '".$end_date."'");
	$milestone = $CI->db->get()->result_array();
	return $milestone;
}
function getUserLeaves()
{
	$start_date = firstOfMonth();
	$end_date = lastOfMonth();
	$CI = & get_instance();
	$CI->db->select("ls.leave_status as leave_status_name,d.department_name,lt.leave_type as leave_name,u.first_name,u.last_name,l.*");
	$CI->db->from("leaves as l");
	$CI->db->join("users as u","u.user_id = l.user_id","left");
	$CI->db->join("leave_type as lt","lt.leave_type_id = l.leave_type","left");
	$CI->db->join("leave_status as ls","ls.leave_status_id = l.leave_status","left");
	$CI->db->join("department as d","d.department_id = u.department_id","left");
	//$CI->db->where("l.leave_start_date >= '".$start_date."' AND l.leave_end_date < '".$end_date."'");
	$milestone = $CI->db->get()->result_array();
	return $milestone;
}
function getUserLeavesByDepartment($did)
{
	$start_date = date("Y-m-d");
	$end_date = lastOfMonth();
	$CI = & get_instance();
	$CI->db->select("ls.leave_status as leave_status_name,lt.leave_type as leave_name,u.first_name,u.last_name,l.*");
	$CI->db->from("leaves as l");
	$CI->db->join("users as u","u.user_id = l.user_id","left");
	$CI->db->join("leave_type as lt","lt.leave_type_id = l.leave_type","left");
	$CI->db->join("leave_status as ls","ls.leave_status_id = l.leave_status","left");
	//$CI->db->join("department as d","d.department_id = u.department_id","left");
	$CI->db->where("l.leave_start_date >= '".$start_date."'");
	$CI->db->where("u.department_id = ".$did);
	$CI->db->limit(5,0);
	$ld = $CI->db->get()->result_array();
	return $ld;
}
function firstOfMonth() {
return date("Y-m-d", strtotime(date('m').'/01/'.date('Y').' 00:00:00'));
}

function lastOfMonth() {
return date("Y-m-d", strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));
}


/*---------------------------------------------END------------------------------------------*/







function preventJsInQueStr($getArr)
{
	$newArr = array();
	foreach($getArr as $k => $v) {
		$newArr[$k] = htmlentities($v);
	}
	return $newArr;
}
/*remove duplicate variavle from URL*/
function removeQueStrVar($url, $key)
{
	$url = preg_replace('/(.*)(?|&)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&');
	$url = substr($url, 0, -1);
	return $url;
}
function removeVarUrl($url){
	$vars = array_filter(explode('&', $url));

	$final = array();
	if(!empty($vars)) {
		foreach($vars as $var) {
			$parts = explode('=', $var);

			$key = $parts[0];
			$val = $parts[1];
			if(!array_key_exists($key, $final) && !empty($val))
				$final[$key] = $val;
		}
	}
}

function flyerNumberAuth()
{
	$CI = & get_instance();
	$flyer_number_session = $CI->session->userdata('flyer_number_session');
	if (!$flyer_number_session) {
		redirect(base_url().'shop');
		exit;
	}
}

function sAdmin_auth($is_login)
{
	$CI = & get_instance();
	$sLogin_session = $CI->session->userdata('sLogin_session');
	if (!$sLogin_session && $is_login == 0) {
		redirect(base_url().'superadmin');exit;
	}
	if ($sLogin_session && $is_login == 1) {
		redirect(base_url() . 'superadmin/dashboard');exit;
	}
}

function user_auth($is_login)
{
	$CI = & get_instance();
	$login_session = $CI->session->userdata('login_session');
	if (!$login_session && $is_login == 0) {
		redirect(base_url());
	}
	if ($login_session && $is_login == 1) {
		redirect(base_url() . 'dashboard');
	}
}
function ref_auth($is_login)
{
	$CI = & get_instance();
	$ref_login_session = $CI->session->userdata('ref_login_session');
	if (!$ref_login_session && $is_login == 0) {
		redirect(base_url().'home/referrer/login');
	}
	if ($ref_login_session && $is_login == 1) {
		redirect(base_url() . 'referrer/dashboard');
	}
}
function shop_user_auth($is_login)
{
	$CI = & get_instance();
	$user_login_session = $CI->session->userdata('user_login_session');
	if (!$user_login_session && $is_login == 0) {
		redirect(base_url().'shop/login');
	}
	if ($user_login_session && $is_login == 1) {
		redirect(base_url() . 'shop');
	}
}
function truncate($string, $s, $e, $url = '')
{
	$string = strip_tags($string);
	if (strlen($string) > $e) {
		$readMore = '';
		if ($url != '') {
			$readMore = '<a href="' . $url . '"><b>Read more</b></a>';
		}
		$str = substr($string, $s, $e) . '...' . $readMore;
	}
	else {
		$str = $string;
	}
	return $str;
}
function pr($arr)
{
	echo "<pre>";
	$new_arr = print_r($arr);
	echo "</pre>";
	return $new_arr;
}
function getSMSRemainCnt($total, $used){
	return ($total - $used);
}
function send_mail($email, $subject, $msg)
{
	$ci = & get_instance();
	/*$headers = "MIME-Version: 1.0" . "\r\n";
	$headers.= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers.= 'From: <no-reply@' . $ci->config->item('siteNameForMail') . '>' . "\r\n";*/
	//var_dump($headers);
	//return @mail($email, $subject, $msg, $headers);

	/**********
	* Send mail via SMTP as mail function has known issues
	* Author : Ravi Tiwari
	***********/

	$ci->load->library('email');
	$ci->email->from('hrms@letsnurture.com', "HRMS");
  $ci->email->to($email);
  $ci->email->subject($subject);
  $ci->email->message($msg);
  if ($ci->email->send()):
      return 1; // Mail sent!
  else:
      return 0; // There is error in sending mail!
  endif;

}

function sendSMS($mno, $msg){
	if($_SERVER['HTTP_HOST'] != 'localhost'){
		include_once ('HTTP/Request.php');
		// $req = &new HTTP_Request('http://89.104.214.102:8181/httpmt');
		$req = new HTTP_Request('http://89.104.214.102:8181/httpmt');//	echo "ads2131";
		$req->setMethod(HTTP_REQUEST_METHOD_GET);
		$req->addQueryString('operation', 'login');
		$req->addQueryString('account_id', '319.xpe');
		$req->addQueryString('password', 'T6yh9DfV');
		$req->sendRequest();
		$response = $req->getResponseBody();
		$get_cookie = $req->getResponseCookies();
		$cookie = $get_cookie[0]['value'];
		// $req2 = &new HTTP_Request('http://89.104.214.102:8181/httpmt');
		$req2 = new HTTP_Request('http://89.104.214.102:8181/httpmt');
		$req2->setMethod(HTTP_REQUEST_METHOD_GET);
		$req2->addCookie('JSESSIONID', $cookie);
		$req2->addQueryString('destination_addr', $mno);
		$req2->addQueryString('dest_addr_ton', '1');
		$req2->addQueryString('dest_addr_npi', '1');
		$req2->addQueryString('source_addr', '3204');
		$req2->addQueryString('source_addr_ton', '5');
		$req2->addQueryString('source_addr_npi', '0');
		$req2->addQueryString('sendviaroute', 'AUTO');
		$req2->addQueryString('msgbody', $msg);
		// $req2->addQueryString('msgbody', 'pqrsrsrs');
		$req2->addQueryString('status_report_request', '1');
		$req2->addQueryString('operation', 'submit');
		$req2->sendRequest();
		$response = $req2->getResponseBody();

		return $response;
	}
}


function paginate($totalRec,$perPage,$pageQueryString,$pageUrl){
	$ci = & get_instance();
	$ci->load->library("pagination");
	$config = array();
	$config["base_url"] = base_url().$pageUrl;
	$config["total_rows"] = $totalRec;
	$config["per_page"] = $perPage;
	$config["uri_segment"] = 4;
	$config['page_query_string'] = TRUE;
	$config['use_page_numbers'] = TRUE;
	$config['query_string_segment'] = $pageQueryString;
	$config['full_tag_open'] = '<section class="panel"><div class="text-center"><ul class="pagination pagination-sm pro-page-list">';
	$config['full_tag_close'] = '</ul></div></section>';
	$config['first_link'] = '&laquo; First';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';

	$config['last_link'] = 'Last &raquo;';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';

	$config['next_link'] = '»';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';

	$config['prev_link'] = '«';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';

	$config['cur_tag_open'] = '<li><a href="javascript:void(0);" class="active">';
	$config['cur_tag_close'] = '</a></li>';

	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';

	$ci->pagination->initialize($config);
	return	$ci->pagination->create_links();
}

function _pagination($count_data, $per_page = 10, $page = 1, $url = '?')
{
	/*
	Created Date: 25-7-2013
	Created By : Vasant
	*/
	$total = $count_data;
	$adjacents = "1";
	$page = ($page == 0 ? 1 : $page);
	$start = ($page - 1) * $per_page;
	$firstPage = 1;
	$prev = ($page == 1) ? 1 : $page - 1;
	$prev = $page - 1;
	$next = $page + 1;
	$lastpage = ceil($total / $per_page);
	$lpm1 = $lastpage - 1;
	$pagination = "";
	if ($lastpage > 1) {
		$pagination.= "<ul class='pagination'>";
		//  $pagination .= "<li class='details'>Page $page of $lastpage</li>";
		if ($page == 1) {
			// $pagination.= "<li><a class='current'>First</a></li>";
			$pagination.= "<li><a class='current'><i class='icon-angle-left'></i></a></li>"; // pre
		}
		else {
			// $pagination.= "<li><a href='{$url}page=$firstPage'>First</a></li>";
			$pagination.= "<li><a href='{$url}page=$prev'><i class='icon-angle-left'></i></a></li>"; //pre
		}
		if ($lastpage < 7 + ($adjacents * 2)) {
			for ($counter = 1; $counter <= $lastpage; $counter++) {
				if ($counter == $page) $pagination.= "<li class='active'><a class='current'>$counter</a></li>";
				else $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
			}
		}
		elseif ($lastpage > 5 + ($adjacents * 2)) {
			if ($page < 1 + ($adjacents * 2)) {
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
					if ($counter == $page) $pagination.= "<li class='active'><a class='current'>$counter</a></li>";
					else $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
				}
				$pagination.= "<li class='dot'><a href='javascript:void(0)'>...</a></li>";
				$pagination.= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
				$pagination.= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";
			}
			elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
				$pagination.= "<li><a href='{$url}page=1'>1</a></li>";
				$pagination.= "<li><a href='{$url}page=2'>2</a></li>";
				$pagination.= "<li class='dot'><a href='javascript:void(0)'>...</a></li>";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
					if ($counter == $page) $pagination.= "<li class='active'><a class='current'>$counter</a></li>";
					else $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
				}
				$pagination.= "<li class='dot'><a href='javascript:void(0)'>..</a></li>";
				$pagination.= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
				$pagination.= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";
			}
			else {
				$pagination.= "<li><a href='{$url}page=1'>1</a></li>";
				$pagination.= "<li><a href='{$url}page=2'>2</a></li>";
				$pagination.= "<li class='dot'><a href='javascript:void(0)'>..</a></li>";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
					if ($counter == $page) $pagination.= "<li class='active'><a class='current'>$counter</a></li>";
					else $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";
				}
			}
		}
		if ($page < $counter - 1) {
			$pagination.= "<li><a href='{$url}page=$next'><i class='icon-angle-right'></i></a></li>"; // next
			// $pagination.= "<li><a href='{$url}page=$lastpage'>Last</a></li>";
		}
		else {
			$pagination.= "<li><a class='current'><i class='icon-angle-right'></i></a></li>"; // next
			// $pagination.= "<li><a class='current'>Last</a></li>";
		}
		$pagination.= "</ul>\n";
	}
	return $pagination;
}
function file_exist($removePath)
{
	if (!file_exists($removePath)) {
		unlink($removePath);
	}
}
/* remove multiple file*/
if (!function_exists('remove_files')) {
	function remove_files($path)
	{
		$files = glob($path); // get all file names
		foreach($files as $file) { // iterate files
			if (is_file($file)) {
				unlink($file); // delete file
			}
		}
	}
}
function selectedValStr($value1, $value2)
{
	if ($value1 == $value2 && $value1 != 0) {
		///echo $value1.'-----------'.$value2;exit;
		return 'selected="selected"';
	}
	else {
		return "";
	}
}

function selectedVal($value1, $value2)
{
	if ($value1 === $value2 && $value1 !== 0) {
		return 'selected="selected"';
	}
	else {
		return "";
	}
}
function selectedChk($value1, $value2)
{
	if ($value1 === $value2 && $value1 !== 0) {
		return 'checked="checked"';
	}
	else {
		return "";
	}
}

function saveImage($fieldName,$w,$path,$allowExt){

	$inputFile = $fieldName;
	$temp = explode(".", $_FILES[$fieldName]["name"]);
	$extension = end($temp);
	$timestmp = time().'_';
	$fileName = $fieldName.'_'.$timestmp.''.$_FILES[$fieldName]["name"];
	$fileTmpName = $_FILES[$fieldName]["tmp_name"];
	$fileError = $_FILES[$fieldName]["error"];
	if (in_array($extension, $allowExt))
	{

	  if ($fileError > 0)
	  {
		return false;
	  }else{

		$tmpPath = $path .'/'. $fileName;

		move_uploaded_file($fileTmpName, $tmpPath);

		$newImagePath = $path.'/resized/'.$fileName;
		if(resizeImage($tmpPath,$newImagePath,$w,$extension))
		{
			unlink($path . '/' . $fileName);
			return $fileName;
		}

	  }
	} else {
	  return false;
	}
}

/*Image Upload*/
if (!function_exists('upload_image')) {
	function upload_image($fname, $file, $path, $dir_name, $proimage_type = '', $resize = '', $w = '100', $h = '100', $ext)
	{
		$ci = & get_instance();
		$name_array = array();
		$value = array();
		foreach($file as $key => $value) {
		}
		$value = $file;
		$count = count($file['name']);
		for ($s = 0; $s < $count; $s++) {
			$timestmp = time() . '_' . $proimage_type;
			$_FILES['property_file']['name'] = $dir_name . '_' . $timestmp . strtolower($value['name']);
			$_FILES['property_file']['type'] = $value['type'];
			$_FILES['property_file']['tmp_name'] = $value['tmp_name'];
			$_FILES['property_file']['size'] = $value['size'];
			$config['upload_path'] = $path.'tmp';
			$config['allowed_types'] = $ext;
			$ci->load->library('upload', $config);
			$ci->upload->initialize($config);
			/********resize**********/
			//$data = '';
			if ($ci->upload->do_upload('property_file')) {
				$data = $ci->upload->data();
				if ($resize == 'resize' && $resize != '') {
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path . 'tmp/' . $data['file_name'];
					$config['new_image'] = $path . '/' . $data['file_name'];
					$config['create_thumb'] = false;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = $w;
					$config['height'] = $h;
					$config['quality'] = '100';
					$ci->load->library('image_lib', $config);
					$ci->image_lib->resize();
					$ci->image_lib->initialize($config);
					$ci->image_lib->clear();
					unlink($path . '/tmp/' . $data['file_name']);
				}
			}
			/********resize**********/
			$image_array = $data;
		}
		return $image_array;
	}
}
/*End Image Upload*/
/*Find aspectRatio*/
function aspectRatio($imageFile, $w)
{
	/* Set the width fixed at 200px; */
	$width = $w;
	/* Get the image info */
	$info = getimagesize($imageFile);
	/* Calculate aspect ratio by dividing height by width */
	$aspectRatio = $info[1] / $info[0];
	/* Keep the width fix at 100px,
	but change the height according to the aspect ratio */
	$newHeight = (int)($aspectRatio * $width) . "px";
	/* Use the 'newHeight' in the CSS height property below. */
	return array(
		'w' => $width,
		'h' => $newHeight
	);
	// $width .= "px";
}
/* resize the image and store to server */
function resizeImage($imgSrcPath, $destPath, $w, $ext)
{
	$ext = strtolower($ext);
	$filename = $imgSrcPath;
	// /$percent = 0.3;
	// Content type
	//header('Content-Type: image/jpeg');
	$aspectRat = aspectRatio($filename, $w);
	// Get new sizes
	list($width, $height) = getimagesize($filename);
	$newwidth = $aspectRat['w'];
	$newheight = $aspectRat['h'];
	// exit;
	// Load
	@$thumb = imagecreatetruecolor($newwidth, $newheight);
	if($ext == 'jpg' || $ext == 'jpeg'){
		@$source = imagecreatefromjpeg($filename);
	}
	if($ext == 'png'){
		@$source = imagecreatefrompng($filename);
	}
	if($ext == 'gif'){
		@$source = imagecreatefromgif($filename);
	}

	// Resize
	@imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	// Output
	if($ext == 'jpg' || $ext == 'jpeg'){
		@$success = imagejpeg($thumb, $destPath);
	}
	if($ext == 'png'){
		@$success = imagepng($thumb, $destPath);
	}
	if($ext == 'gif'){
		@$success = imagegif($thumb, $destPath);
	}
	if ($success) {
		imagedestroy($thumb);
		return true;
	}
	else {
		return false;
	}
}
// Video uploading
function _uploadVideo($file, $path, $file_type, $fieldName)
{
	$ci = & get_instance();
	// file name
	$name = time() . "_video_" . $file[$fieldName]['name'];
	$type = $file[$fieldName]['type'];
	$size = $file[$fieldName]['size'];
	$cname = str_replace(" ", "_", $name);
	$tmp_name = $file[$fieldName]['tmp_name'];
	$target_path = $path;
	// final target path
	$target_path = $target_path . basename($cname);
	if (in_array($type, $file_type)) {
		if (move_uploaded_file($file[$fieldName]['tmp_name'], $target_path)) {
			return $name;
		}
		else {
			return false;
		}
	}
	else {
		$ci->session->set_flashdata('error', "Error while uploading video!");
	}
}
// return last query sting url
function getLastUrl($qs)
{
	$nqs = '';
	if ($qs != '') {
		$nqs = "?" . $qs;
	}
	return $nqs;
}


function removeSpace($str) {
 return preg_replace('!\s+!', '',trim($str)); //preg_replace('/\s+/'," ",$str);
}

function getSortDate($date) {
 $date = explode('-',$date);
 $year = substr($date[0], -2);
 $monthNum = $date[1];
 $monthName = substr(date('F', mktime(0, 0, 0, $monthNum, 10)),0,3);
 $dayNum = $date[2];
 return $dayNum." ".$monthName." ".$year;
}

function getLast14Days() {
 $day = array();
 for($d = 14 ; $d >= 0 ; $d--){
 	$days[] = date("Y-m-d",strtotime("-".$d." Days"));
 }

  return ($days);
}




function getPer($rec,$totalRec){
	if($totalRec > 0){
		return $per = number_format(($rec * 100) / $totalRec,2);
	}else{
		return 0;
	}
}

function findCommision($totalPrice,$percentage){
	return (float)($totalPrice * $percentage)/100;
}
function checkBlank($str){
	return $str != '' ? $str : 'N/A';
}

	/**
	 * @Action:	GetDepartmentSeniors
	 * @Author: LN
	 * @Created: December 08 2015
	 * @Modified By:
	 * @Comment: Get seniors details by department wise
	 * @Todo:
	 */
	if(!function_exists('GetDepartmentSeniors')){
		function GetDepartmentSeniors($DepartmentId){
			$CI = & get_instance();
			if(ctype_digit($DepartmentId)){
				// Find user id from department
				$sql = "SELECT *
						FROM crm_department
						WHERE department_id ='$DepartmentId'";

				$query = $CI->db->query($sql);
				$results = $query->result();

				if(!empty($results) && isset($results[0]->seniors) && !empty($results[0]->seniors)){
					$ids = $results[0]->seniors;
					// Find user details
					$sql = "SELECT *
							FROM crm_users
							WHERE user_id IN ($ids)";
					$query = $CI->db->query($sql);
					return $results = $query->result();
				}
			}
		}
	}

/****
* @Action:	showEncryptedData
* @Author:  Ravi
* @Created: December 28 2015
* @Modified By:
* @Comment: Get Logged in User if HR or same profile view to allow decryption
* @Todo:
*****/

function showEncryptedData($userIdToCheck){
	$CI = & get_instance();
  $logSession = $CI->session->userdata("login_session");
	//var_dump($logSession);
	$logUserId = $logSession[0]->user_id;
	// Allow to HR or same user to edit profile
	if($logUserId == $userIdToCheck || $logSession[0]->role_id ==22 ) {
		return 1;
	} else {
		return 0;
	}
}

/****
* @Action:	getLoggedInUserRole
* @Author:  Ravi
* @Created: Jan 03 2016
* @Modified By:
* @Comment: Get Logged in User Role as we have no seperations on department for MGMT group
* @Todo:
*****/

// Not working anymore
function getLoggedInUserRole(){
	$CI = & get_instance();
	$logSession = $CI->session->userdata("lgetFeedbackKeysogin_session");
	// var_dump($logSession);
	// $logUserId = $logSession->user_id;
	// echo "<pre>";
	// echo "Log Session-->";
	// print_r($logSession);
	// echo "</pre>";
	// die();
	// Allow to HR or same user to edit profile
	return $logSession->role_id;
}



/*
* Log the activity
* This function logs the activity as viewed this page with time
* @param module_name, action, userId, logTime
* @return success
* Author :Ravi Tiwari
* Date : 06 JAN 2016
*/

function auditUserActivity($module_name, $action, $userId, $logTime, $itemId){
	$CI = &get_instance();
	//$CI->load->library('user_agent');
	if ($CI->agent->is_browser()){
	    $agent = $CI->agent->browser().' '.$CI->agent->version();
	} elseif ($CI->agent->is_robot()) {
	    $agent = $CI->agent->robot();
	} elseif ($CI->agent->is_mobile()){
	    $agent = $CI->agent->mobile();
	} else {
	    $agent = 'Unidentified User Agent';
	}
	//echo $agent;
	//echo $CI->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)
	// insert in auditTable
	$data['user_id'] = $userId;
	$data['module_name'] = $module_name;
	$ip = $CI->input->ip_address();
	//var_dump($_SERVER);
	$data['ip'] = $ip;
	$data['user_agent'] = $agent. '/'.$CI->agent->platform();
	$data['user_action'] = $action;
	$data['timestamp'] = $logTime;
	$data['item_id'] = isset($itemId)?$itemId:0;
	$CI->db->insert('auditlogs',$data);

}


function getAllModules()
{
	$CI = & get_instance();
	$query = $CI->db->distinct();
	$query = $CI->db->select('module_name');
	$query = $CI->db->get('role_permission');
	return $query->result();
}

function getLeaveStatus($id){
	$CI = & get_instance();
	$CI->db->select('leave_status');
	$CI->db->from('leave_status');
	$CI->db->where('leave_status_id',$id);
	$query=$CI->db->get();
	return $query->result();
}

function getDaysCount($startDate, $endDate){
	// Return the number of days between the two dates: +1 for date inclusive
	  return round(abs(strtotime($startDate)-strtotime($endDate))/86400)+1;
}

function get_el_remarks($id){
		$el_remarksArray =
		array("Due to less working hours",
		"Due to extra EL taken",
		"Due to EL utilized for Maternity leave",
		"Due to EL utilized for Marriage leave",
		"Other remarks");
		if($id == "Select a value"){
			//echo "Routine EL updates by HR";
			return "Routine EL updates by HR";
		} elseif($id != null){
			return $el_remarksArray[$id];
		} else {
			return $el_remarksArray;
		}
}

function getAllUsersFromDepartment($deptt){
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->where('department_id',$deptt);
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	return $query->result();
}

function getAllUsersOFDepartment($deptt){
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->where('department_id',$deptt);
	$query = $CI->db->where('role_id !=','13');
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	return $query->result();
}

function getAllUsersForManagement($deptt){
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->where('status','Active');
	$query = $CI->db->where('department_id !=',$deptt);
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	return $query->result();
}


function getAllFeedbackYear(){
	$CI = & get_instance();
	$query = $CI->db->select('YEAR(created_time) as year');
	$query = $CI->db->where('published_status','1');
	$CI->db->order_by("year","desc");
	$CI->db->group_by("year");
	$query = $CI->db->get('feedback');
	return $query->result();
}

if(!function_exists('checkUserFeedback')){
	// making this parametric so that if needed can be checked for user only not with user + reviewer combo
	function checkUserFeedback($month,$year,$user_id, $reviewer_id =null) {
		// $reviewercondition ="";
		$CI = & get_instance();
		// if($reviewer_id){
		// 	$reviewercondition = 'AND reviewer_id = "'.$reviewer_id.'" OR reviewer_id = "0"';
		// }
		$sql = "SELECT *
				FROM crm_feedback
				WHERE MONTH( created_time ) =  $month
				and YEAR( created_time ) =  $year
				AND user_id = $user_id";

		$query = $CI->db->query($sql);
		return $results = $query->result();
	}
}

if(!function_exists('activeUserEmails')){
	function activeUserEmails() {
		$CI = & get_instance();

		$sql = "SELECT email
				FROM crm_users
				WHERE status = 'Active'";

		$query = $CI->db->query($sql);
		return $results = $query->result_array();
	}
}

if(!function_exists('getAnnouncements')){
	function getAnnouncements() {
		$CI = & get_instance();
		$sql = "SELECT *
				FROM crm_announcement
				WHERE published_status = '1'
				ORDER BY created_time DESC LIMIT 5";
		$query = $CI->db->query($sql);
		return $results = $query->result();
	}
}

/*
 * @Author:  krunal
 * @Created: September 02 2015
 * @Modified By:
 * @Modified at:
 * @Comment: pagination function
 */
function pagination($count,$page,$recordsPerPage,$class){
	$start = ($page-1) * $recordsPerPage;
	$adjacents = "2";
	$prev = $page - 1;
	$next = $page + 1;
	$lastpage = ceil($count/$recordsPerPage);
	$lpm1 = $lastpage - 1;
	$pagination = "";
	$startCounter = 1;
	$lastCounter = $lastpage;
	$counter = 0;
	if($lastpage > 1){
			$pagination .= "<div class='paginationdiv inner-nav'>";
			if ($page > 1)
			{
					$pagination.= "<a href='#' id='$prev' class='$class'> < </a>";
					if ($page > 3)
					{
						//$pagination.= "<span class='current'>...}</span>";
						$startCounter = $page-1;
					}
					if($page < $lastpage)
						$lastCounter = $page+2;
			}else{
					$pagination.= "<span class='disabled'> < </span>";
					if($page < $lastpage)
						$lastCounter = $page+2;
			}
			if ($lastpage < 3 + ($adjacents * 2)){
				for ($counter = $startCounter; $counter <= $lastCounter; $counter++){
					if ($counter == $page)
							$pagination.= "<span class='current'>$counter</span>";
					else
							$pagination.= "<a href='#' id='$counter' class='$class'>$counter</a>";

				}
				if($page < $lastpage-1)
				{
					$pagination.= "<span class='current'>..</span>";
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
					if($page < 1 + ($adjacents * 2)){
							for($counter = $startCounter; $counter < $lastCounter; $counter++){
									if($counter == $page)
											$pagination.= "<span class='current'>$counter</span>";
									else
											$pagination.= "<a href='#' id='$counter' class='$class'>$counter</a>";
							}
							$pagination.= "..";
							$pagination.= "<a href='#' id='$lpm1' class='$class'>$lpm1</a>";
							$pagination.= "<a href='#' id='$lastpage' class='$class'>$lastpage</a>";

				}elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
						$pagination.= "<a  id='1' class='$class'>1</a>";
						$pagination.= "<a  id='2' class='$class'>2</a>";
						$pagination.= "..";
						for($counter = $startCounter; $counter <= $lastCounter; $counter++)
						{
								if($counter == $page)
										$pagination.= "<span class='current'>$counter</span>";
								else
										$pagination.= "<a href='#' id='$counter' class='$class'>$counter</a>";
						}
						$pagination.= "..";
						$pagination.= "<a   id='$lpm1' class='$class'>$lpm1</a>";
						$pagination.= "<a  id='$lastpage' class='$class'>$lastpage</a>";
				}else{
						$pagination.= "<a  id='1' class='$class'>1</a>";
						$pagination.= "<a  id='2' class='$class'>2</a>";
						$pagination.= "..";
						for($counter = $startCounter; $counter <= $lastCounter; $counter++)
						{
								if($counter == $page)
											$pagination.= "<span class='current'>$counter</span>";
								else
											$pagination.= "<a href='#' id='$counter' class='$class'>$counter</a>";
						}
						if($page < $lastpage-1)
						{
							$pagination.= "<span class='current'>...</span>";
						}
				}
			}
			if($page < $counter - 1)
					$pagination.= "<a  id='$next' class='$class'> > </a>";
			else
					$pagination.= "<span class='disabled'> > </span>";

		return	$pagination.= "</div>";
	}
}


function getDepartmentFromUserId($userId){
	$CI = & get_instance();

	$CI->db->select("d.department_id,d.department_name,d.seniors");
	$CI->db->from("department as d");
	$CI->db->join("users as u","u.department_id = d.department_id","left");
	$CI->db->where("u.user_id",$userId);
	$results = $CI->db->get()->result_array();
	return $results;
}
//get user all token
function getUserDeviceToken($membersId, $deviceType = 0){
	$CI = & get_instance();
	$CI->db->select("deviceToken");
	$CI->db->from("device");
	$CI->db->where("user_id IN ($membersId)");
	$CI->db->where("deviceType", $deviceType);
	$query = $CI->db->get()->result();
	return $query;
}
//get all user token
function getAllDeviceToken($deviceType = 0){
	$CI = & get_instance();
	$CI->db->select("deviceToken");
	$CI->db->from("device");
	//$CI->db->where("deviceType", $deviceType);
	$query = $CI->db->get()->result();
	return $query;
}

//get user token
function getDeviceTokenByUser($user_id){
	$CI = & get_instance();
	$CI->db->select("deviceToken");
	$CI->db->from("device");
	$CI->db->where("user_id = $user_id");
	$query = $CI->db->get()->result();
	return $query;
}

function SendNotificationFCMWeb($title, $message, $target){
	//FCM api URL
	$url = 'https://fcm.googleapis.com/fcm/send';
	//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
	//$server_key = 'AAAAW4SfPFw:APA91bGBq7DVX7VKqDphi9dMPb0YHu6tgSWG3kdsmHty38H3DDDakUp5XOUlrwXSKv5n9Ql91AN2ZQvJV17Qqgf1twSuivsqbWG9s3L8f90CqVxw1dFA-iZML1xheKVMKDl13mCta-tY';
	$server_key = 'AAAAAXvok1k:APA91bF9OOl9SQKRDRzScrmE2OtJB3w41EV1-AyyNmwURI3kgGRQjBFlEeVcgL07mSlJVN0-og0Z6r963JelhSpJVGM2b2twK77mi8l54FLz3rAOnjqUQQRy2ImFN1Ji-SR6xxUWPbyt';

	$fields = array();
	$fields['notification'] = array();
	$fields['notification']['body'] = $message;
	$fields['notification']['title'] = $title;
	$fields['notification']['click_action'] = base_url('dashboard');
	$fields['notification']['icon'] = base_url().'uploads/images/notification-icon.png';
	$fields['content_available'] = true;
	//$fields['notification']['icon'] = "ic_notification_big";

	if(is_array($target)){
		$fields['registration_ids'] = $target;
	}else{
		$fields['to'] = $target;
	}

	$headers = array(
		'Content-Type:application/json',
		'Authorization:key='.$server_key
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('FCM Send Error: ' . curl_error($ch));
	}
	curl_close($ch);
	//print_r($result);
	return $result;
}

/*function SendNotificationFCMAndroid($title, $message, $target){
	//FCM api URL
	$url = 'https://fcm.googleapis.com/fcm/send';
	//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
	//$server_key = 'AAAAeivpA3A:APA91bGvdRvmeTB99dB-qtYAKbBKPt8VnA9NcaL0re5HIA6Z8xUD0geUhTYVMbRxYZhYNbaG1KGv6K2te7l34NATfK9Wf_uKIjmktnWeDg41MFjzoR2qLkMrL-srWIziwGQULpxS2RR6'; //for web

	$server_key = 'AAAAMy3f71o:APA91bEgxIjaCSxbKyEF16wyLnHP8ZRL97Jv6213fen0T_oHOwSBILxMOSbx1DhgWVI1iVQQOyZqdAUEmrNlPpByqytsXfzDoYAprKAyHvrekbTUGkT6w3HlbI96jVbj80WDZPkraTu7'; //for android

	$fields = array();
	$fields['notification'] = array();
	$fields['notification']['body'] = $message;
	$fields['notification']['title'] = $title;
	//$fields['notification']['click_action'] = base_url('dashboard');
	$fields['notification']['icon'] = 'ic_stat_notification';

	//$fields['notification']['icon'] = "ic_notification_big";

	if(is_array($target)){
		$fields['registration_ids'] = $target;
	}else{
		$fields['to'] = $target;
	}

	$headers = array(
		'Content-Type:application/json',
		'Authorization:key='.$server_key
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	//print_r($result);
	if ($result === FALSE) {
		die('FCM Send Error: ' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}


function SendNotificationFCMiOS($title, $message, $target){
	//FCM api URL
	$url = 'https://fcm.googleapis.com/fcm/send';
	//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
	//$server_key = 'AAAAeivpA3A:APA91bGvdRvmeTB99dB-qtYAKbBKPt8VnA9NcaL0re5HIA6Z8xUD0geUhTYVMbRxYZhYNbaG1KGv6K2te7l34NATfK9Wf_uKIjmktnWeDg41MFjzoR2qLkMrL-srWIziwGQULpxS2RR6'; //for web

	$server_key = 'AAAAMy3f71o:APA91bEgxIjaCSxbKyEF16wyLnHP8ZRL97Jv6213fen0T_oHOwSBILxMOSbx1DhgWVI1iVQQOyZqdAUEmrNlPpByqytsXfzDoYAprKAyHvrekbTUGkT6w3HlbI96jVbj80WDZPkraTu7'; //for ios

	$fields = array();
	$fields['notification'] = array();
	$fields['notification']['body'] = $message;
	$fields['notification']['title'] = $title;
	//$fields['notification']['click_action'] = base_url('dashboard');
	$fields['notification']['icon'] = 'ic_stat_notification';

	//$fields['notification']['icon'] = "ic_notification_big";

	if(is_array($target)){
		$fields['registration_ids'] = $target;
	}else{
		$fields['to'] = $target;
	}

	$headers = array(
		'Content-Type:application/json',
		'Authorization:key='.$server_key
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	//print_r($result);
	if ($result === FALSE) {
		die('FCM Send Error: ' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}*/
//get user details
function getUserDetail($userId){
	$CI = & get_instance();
	$CI->db->select("*");
	$CI->db->from("users");
	$CI->db->where('user_id',$userId);
	//$CI->db->where('status = "Active"');
	$query = $CI->db->get()->row();
	return $query;
}

function getUserEmail($userId){
	$CI = & get_instance();
 	$query = $CI->db->select('GROUP_CONCAT(email) as usersEmail');
	$CI->db->from("users");
	$query = $CI->db->where("user_id in ($userId)");
 	$query = $CI->db->where('status','Active');
	$result = $CI->db->get()->row();
	return $result;

}


//getWorkingDays
function getWorkingDays($startDate, $endDate){
  $begin = strtotime($startDate);
  $end   = strtotime($endDate);
  if ($begin > $end) {
      echo "startdate is in the future! <br />";

      return 0;
  } else {
      $no_days  = 0;
      $weekends = 0;
      while ($begin <= $end) {
          $no_days++; // no of days in the given interval
          $what_day = date("N", $begin);
          if ($what_day > 5) { // 6 and 7 are weekend days
              $weekends++;
          };
          $begin += 86400; // +1 day
      };
      $working_days = $no_days - $weekends;

      return $working_days;
  }
}
/**
 * @Method		  :	POST
 * @Params		  :
 * @author      : Jignasa
 * @created		  :	18-07-2017
 * @Modified by	:
 * @Status		  :
 * @Comment		  : Get all Junior users list by Seniors
 * 								$field contains either leave_users or feedback_users
 **/
function getJuniorUsersList($user_id , $field)
{
	$CI = & get_instance();
	$query = $CI->db->select($field);
	$query = $CI->db->where('user_id',$user_id);
	$query = $CI->db->where($field.' IS NOT NULL', null, false);
	$query = $CI->db->where('status','Active');
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	$result =  $query->result();

	if(!empty($result)){
		$CI = & get_instance();
		$query = $CI->db->select('*');
		$query = $CI->db->where_in('user_id', explode(',',$result[0]->$field));
		$query = $CI->db->where('status','Active');
		$CI->db->order_by("first_name","asc");
		$query = $CI->db->get('users');
		return  $query->result();
	}else{
	 return 0;
  }
}

/**
 * @Method		  :	POST
 * @Params		  :
 * @author      : Jignasa
 * @created		  :	19-07-2017
 * @Modified by	:
 * @Status		  :
 * @Comment		  : Get all Junior Users departments list by Seniors
 * 								$field contains either leave_users or feedback_users
 **/
function getJuniorUsersDepartmentList($user_id, $field)
{
	$CI = & get_instance();
	$query = $CI->db->select($field);
	$query = $CI->db->where('user_id',$user_id);
	$query = $CI->db->where($field.' IS NOT NULL', null, false);
	$query = $CI->db->where('status','Active');
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	$result =  $query->result();

	if(!empty($result)){
		$CI = & get_instance();
		$query = $CI->db->select('d.*');
		$query = $CI->db->from("users as u");
		$query = $CI->db->join("department as d","d.department_id = u.department_id","left");
		$query = $CI->db->where_in('u.user_id', explode(',',$result[0]->$field));
		$query = $CI->db->where('u.status','Active');
		$query = $CI->db->where('d.status','Active');
		$CI->db->group_by("department_name");
		$CI->db->order_by("department_name","asc");
		$query = $CI->db->get('users');
		return  $query->result();
	}else{
	 return 0;
	}
}

/**
 * @Method		  :	POST
 * @Params		  :
 * @author      : Jignasa
 * @created		  :	19-07-2017
 * @Modified by	:
 * @Status		  :
 * @Comment		  : Get email list of all seniors of the given user
 * 								$field contains either leave_users , feedback_users or reimbursement
 **/
 function getSeniorEmails($user_id, $field){

	$CI = & get_instance();
 	$query = $CI->db->select('GROUP_CONCAT(user_id) as memberUsers');
	$query = $CI->db->where("FIND_IN_SET('$user_id',$field) !=", '');
 	$query = $CI->db->where('status','Active');
 	$CI->db->order_by("first_name","asc");
 	$query = $CI->db->get('users');
 	$result =  $query->result();

	if(!empty($result)){
	 $users = $result[0]->memberUsers;

	 $query = $CI->db->select('GROUP_CONCAT(email) as memberEmail');
	 $query = $CI->db->from('users');
	 $query = $CI->db->where_in('user_id', explode(',',$users));
	 $query = $CI->db->where("status =  'Active'");
	 $query=$query = $CI->db->get();
	 $arr = $query->row_array();

	 $query = $CI->db->select('GROUP_CONCAT(sEmail) as memberEmail');
	 $query = $CI->db->from('super_admin');
	 $query=$query = $CI->db->get();
	 $arr1 = $query->row_array();
	 if(!empty($arr['memberEmail']))
	 {
			$dd = $arr['memberEmail'].','.$arr1['memberEmail'];
	 }else{
		 $dd = $arr1['memberEmail'];
	 }
	 return $dd;
  }else{
	 return 0;
  }
 }

 /**
 * @Method		  :	POST
 * @Params		  :
 * @author      : Jignasa
 * @created		  :	19-07-2017
 * @Modified by	:
 * @Status		  :
 * @Comment		  : Get email list of all seniors of the given user
 * 								$field contains either leave_users or feedback_users
 **/
 function getSeniorTokens($user_id, $field){
	$CI = & get_instance();
	$query = $CI->db->select('GROUP_CONCAT(user_id) as memberUsers');
	$query = $CI->db->where("FIND_IN_SET('$user_id',$field) !=", '');
	$query = $CI->db->where('status','Active');
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	$result = $query->result();

	if(!empty($result)){
	 $users = $result[0]->memberUsers;
	 $CI = & get_instance();
	 $CI->db->select("deviceToken");
	 $CI->db->from("device");
	 $CI->db->where_in('user_id', explode(',',$users));
	 $result = $CI->db->get()->result();
	 return $result;
	}else{
	 return 0;
	}
 }


 function getAllDepartmentExceptCEO()
 {
 	$CI = & get_instance();
	$query = $CI->db->where('department_id != 1');
 	$query = $CI->db->where('status','Active');
 	$query = $CI->db->get('department');
 	return $query->result();
 }

 function checkEarnedLeaveCount($userid)
 {
	 $todayDate = date('Y-m-d');
	 $CI = & get_instance();
	 $CI->db->select('SUM(leave_days) as count');
	 $CI->db->from('crm_leaves');
	 $CI->db->where('leave_type','3');
	 $CI->db->where("(quarter('$todayDate') = QUARTER(leave_start_date) OR quarter('$todayDate') = QUARTER(leave_end_date))");
	 $CI->db->where("(YEAR('$todayDate') = YEAR(leave_start_date) OR YEAR('$todayDate') = YEAR(leave_end_date))");
	 $CI->db->where('user_id',$userid);
	 $query=$CI->db->get();
	 $result = $query->row();
	 return $result->count;
 }

 function getUserRank($id){
	 $CI = & get_instance();
	 $query = $CI->db->query('SELECT  ( SELECT  COUNT(DISTINCT ui.karma_points) FROM crm_users as ui WHERE ui.status="ACTIVE" AND ui.karma_points >= uo.karma_points ) AS rank FROM crm_users as uo WHERE uo.status="Active" AND user_id = '.$id);
	 return $query->result_array();
 }

 function getKarmaDetails($id)
 {
	 $CI = & get_instance();
	 $data = array();
	 $rank = getUserRank($id);
	 $data['rank'] = $rank[0]['rank'];
	 $CI->db->select('reward_points, karma_points');
	 $CI->db->from('crm_users');
	 $CI->db->where('user_id',$id);
	 $query = $CI->db->get();
	 $result = $query->row();
	 $data['reward_points'] = $result->reward_points;
	 $data['karma_points'] = $result->karma_points;
	 return $data;
 }

 function checkHeighTotalDeptLeaveCount()
 {
	$todayDate = date('Y-m-d');
	$departments = getAllDepartmentExceptCEO();
	foreach($departments as $department){
		$deptid = $department->department_id;
		$CI = & get_instance();
		$CI->db->select('SUM(leave_days) as count, l.user_id, l.*');
		$CI->db->from('crm_leaves as l');
		$CI->db->join("users as u","u.user_id = l.user_id","left");
		$CI->db->where('u.status', 'Active');
		$CI->db->where('leave_type != 6');
   	    $CI->db->where('leave_type != 14');
		$CI->db->where("(quarter('$todayDate') = QUARTER(leave_start_date) OR quarter('$todayDate') = QUARTER(leave_end_date))");
		$CI->db->where("(YEAR('$todayDate') = YEAR(leave_start_date) OR YEAR('$todayDate') = YEAR(leave_end_date))");
		$CI->db->where('l.department_id',$deptid);
		$CI->db->group_by("l.user_id");
		$CI->db->order_by("count", "desc");
		$query = $CI->db->get();
		$result = $query->row();
		if($result){
			return $result->user_id;
		}else{
			return 0;
		}
	}


 }



function getDateText($timestamp) {
		$timestamp = strtotime($timestamp);
    if ($timestamp) {
        $currentTime=strtotime('today');
        // Reset time to 00:00:00
        $timestamp=strtotime(date('Y-m-d 00:00:00',$timestamp));
        $days=round(($timestamp-$currentTime)/86400);
        switch($days) {
            case '0';
                return 'Today';
                break;
            case '-1';
                return 'Yesterday';
                break;
            case '-2';
                return 'Day before yesterday';
                break;
            case '1';
                return 'Tomorrow';
                break;
            case '2';
                return 'Day after tomorrow';
                break;
            default:
                if ($days > 0) {
                    return 'In '.$days.' days';
                } else {
                    return ($days*-1).' days ago';
                }
                break;
        }
    }
}

function get_total_vacancy()
 {
		$CI = & get_instance();
		$CI->db->where('vacancy != 0');
	 	$query = $CI->db->get('crm_job_opening');
	 	return $query->result();
 }

 /*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description : For calculating ration between 2 nos
*/
function CalculateRatio($var1, $var2){
	for($x=$var2;$x>1;$x--) {
		if(($var1%$x)==0 && ($var2 % $x)==0) {
			$var1 = $var1/$x;
			$var2 = $var2/$x;
		}
	}
	return array('var1'=>$var1, 'var2'=>$var2);
}

/*
	* Developed By : MD Mashroor
	* Date : 17-04-2019
	* Modified By :
	* Description : For calculating average experience
	$var1 = EXP year
	$var2 = EXP month
	$var3 = total user
*/
function AverageExperience($var1, $var2, $var3){
	if( $var3 !=0){
		if($var2 != 0){
			$var2 = $var2/12;
		}
		$experince = $var1 + $var2;
		$avg = $experince/$var3;
		return $avg;
	}
	return 0;
}

/*
* Developed By : Krunal
* Date : 10/04/2017
* Modified By :
* Description : get All TL,PM,MGMT,HR,Sr HR Email id
*/
function getmanagementEmails(){
	$CI = & get_instance();
	//TL,PM,MGMT,HR,Sr HR,1->ceo
	$roles = '13,15,20,22,49,1';
	$CI->db->select('GROUP_CONCAT(email) as memberEmail');
	$CI->db->from('users');
	$CI->db->where('role_id in ('.$roles.')');
	$CI->db->where("status =  'Active'");
	$query=$CI->db->get();
	$arr = $query->row_array();

	$CI->db->select('GROUP_CONCAT(sEmail) as memberEmail');
	$CI->db->from('super_admin');
	$query=$CI->db->get();
	$arr1 = $query->row_array();
	$dd = $arr['memberEmail'].','.$arr1['memberEmail'];
	return $dd;
}


 /**
 * @Method		  :	POST
 * @Params		  :
 * @author      : Jignasa
 * @created		  :	19-07-2017
 * @Modified by	:
 * @Status		  :
 * @Comment		  : get management Tokens
 * 								$field contains either leave_users or feedback_users
 **/
 function getmanagementTokens(){
	$roles = '13,15,20,22,49,1';
	$CI = & get_instance();
	$query = $CI->db->select('GROUP_CONCAT(user_id) as memberUsers');
	$query = $CI->db->where('role_id in ('.$roles.')');
	$query = $CI->db->where('status','Active');
	$CI->db->order_by("first_name","asc");
	$query = $CI->db->get('users');
	$result = $query->result();

	if(!empty($result)){
	 $users = $result[0]->memberUsers;
	 $CI = & get_instance();
	 $CI->db->select("deviceToken");
	 $CI->db->from("device");
	 $CI->db->where_in('user_id', explode(',',$users));
	 $result = $CI->db->get()->result();
	 return $result;
	}else{
	 return 0;
	}
 }

 /**
 * @Method		  :	POST
 * @Params		  :
 * @author      : Jignasa
 * @created		  :	19-07-2017
 * @Modified by	:
 * @Status		  :
 * @Comment		  : Get role detail by id
 **/

 function getAllRoleName($id)
 {
 	$CI = & get_instance();
 	$query = $CI->db->select('*');
	$query = $CI->db->where('role_id',$id);
 	$query = $CI->db->where('status','Active');
 	$query = $CI->db->get('role');
 	return $query->row();
 }
 function getVendorByid($id){
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query= $CI->db->from('crm_vendor');
	$query = $CI->db->where('vendor_id',$id);
	$query = $CI->db->get()->row();
 	return $query;
 }
 function getVendorByType($type){
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query= $CI->db->from('crm_vendor');
	$query = $CI->db->where('vendorType',$type);
	$query = $CI->db->get()->result();
 	return $query;
 }
function getInventoryByids($id){
	$expInventory = explode(', ',$id);
	$CI = & get_instance();
	$query = $CI->db->select('GROUP_CONCAT(c.cat_name SEPARATOR ",") as cat_name', false);
	$query= $CI->db->from('crm_inventory as i');
	$query = $CI->db->join("crm_inventory_category as c","i.category_id = c.cat_id");
	$query = $CI->db->where_in('inventory_id', $expInventory);
	// $query = $CI->db->where('inventory !=',null);
	$query = $CI->db->get()->row();
	return $query;

}
//  get active user with department and role
function getAllUserWithRoleAndDept(){
	$CI = & get_instance();
 	$query = $CI->db->select('*');
	$query = $CI->db->from('crm_users as u');
	$query = $CI->db->join('crm_department as d','u.department_id = d.department_id');
	$query = $CI->db->join("crm_role as r", "u.role_id = r.role_id");
 	$query = $CI->db->where('u.status','Active');
 	$query = $CI->db->get()->result();
 	return $query;
}

function getInventoryCatByParentCat($id){
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query= $CI->db->from('crm_inventory_category');
	$query = $CI->db->where('parent_cat',$id);
	$query = $CI->db->get()->result();
	return $query;
}

function getSubCategory($id){
	$CI = & get_instance();
	$query = $CI->db->select('GROUP_CONCAT(cat_name SEPARATOR ", ") as Subcategory', false);
	$query = $CI->db->from('crm_inventory_category');
	$query = $CI->db->where('parent_cat', $id);
	$result = $CI->db->get()->result_array();
	return $result[0]['Subcategory'];
}

function explodeVendorService($service){
	$arr = array();
	$exp = explode(", ",$service);
	foreach($exp as $row){
		if($row == 1){
			$arr[] = "Hardware";
		}elseif($row == 2){
			$arr[] = "Software";
		}else{
			$arr[] = "Repair";
		}
	}
	$response = implode(", ", $arr);
	return $response;

}
// Get Reimbursement By Id
function getReimbursementByID($id){
	$CI = & get_instance();
	$query = $CI->db->select('*');
	$query = $CI->db->from('crm_reimbursement_request');
	$query = $CI->db->where('reimbursement_id', $id);
	$query = $CI->db->get()->row();
	return $query;
}
?>
