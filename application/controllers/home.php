<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	Author : Maya Joshi
	Date   : 5th June 2012
	Action : View, Authenticate, Redirect based on Role
	Validation : jquery.js
 */

class Home extends MX_Controller {

    function __construct()
    {
        parent::__construct();
        $this->template->set('controller', $this);
    }

	/*
		Author : Vasant Hun
		Date   : 3th July 2013
		Action : Load view, get posted data, access auth file function to login
		Validation : jquery.js

	 */

	function index()
	{
		/*
			Condition : Load Home Page
		*/
//		$this->template->load_partial('home','home');
	}
}
