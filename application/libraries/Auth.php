<?php

class Auth
{
    var $CI;
    var $_username;
    var $_table = array(
                    'members' =>'members',
                    );

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->helper('string');
		$this->CI->load->helper('cookie');
	}

    function Auth()
    {
        self::__construct();
    }

    function restrict($restrict_to = NULL, $redirect_to = NULL)
	{
		if ($restrict_to !== NULL) {
            
            
                if($restrict_to==1 && $this->CI->session->userdata('iRoleid')==1)
                {
                     return TRUE;
                }
                else if($restrict_to==2 && $this->CI->session->userdata('aiRoleid')==2)
                {
                      return TRUE;
                }
                else if($restrict_to==3 && $this->CI->session->userdata('giRoleid')==3)
                {
                      return TRUE;
                }
                else {
					$this->CI->session->sess_destroy();
                  // $this->CI->session->set_flashdata("login","You do not have sufficient rights to access this page please login.");
                   redirect(base_url().'login');
				}
			
		} else {
			show_error("You do not have sufficient rights to access this page!");
		}
	}

    /*--------------To Check if the user exist in the Database for Forgot Password --Deeshit----*/
	function members_exists( $email )
	{
		$this->CI->db->select('vEmail');
		//$this->CI->db->where("")
		$query = $this->CI->db->get_where($this->_table, array('vEmail' => $email), 1);
		
		if ($query->num_rows() !== 1) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
	 /*--------------To Check if the user exist in the Database for Forgot Password --Maya----*/
	function community_exists( $domain )
	{
		$this->CI->db->select('vCommunityDomain');
		//$this->CI->db->where("")
		$query = $this->CI->db->get_where('community', array('vCommunityDomain' => $domain), 1);
		
		if ($query->num_rows() !== 1) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
	     /*--------------To get community id by domain name --Maya----*/
    function getCommunityByDomain( $domain )
    {
	
        $this->CI->db->select('iCommunityid');
        //$this->CI->db->where("")
        $query = $this->CI->db->get_where('community', array('vCommunityDomain' => $domain), 1);
        
        if ($query->num_rows() !== 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
	/*--------------TO check the password After user click  on email link--Reset password ---Deeshit--------*/
	function pass_link_exists($id)
	{
		$this->CI->db->select('vEmail');
		//$this->CI->db->where("")
		$query = $this->CI->db->get_where('tbl_password', array('vPasswordLink' => $id), 1);
		if ($query->num_rows() !== 1) 
		{
           return FALSE;
		} 
		else 
		{
            return $query->result();
		}
	}

	
	
	function check_password( $password )
	{
		$this->CI->db->select('password');
		$query = $this->CI->db->where($this->_table['users'], array('username' => $this->_username), 1)->row();
	
		if ($query->password == $this->encrypt($password)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

    function check_string_length( $string )
    {
        $string = trim($string);
        return strlen($string);
    }

    function encrypt( $data )
    {
            return sha1($data);
    }
	/*
		Author: Maya Joshi
		Action: 1] Check 3 trial with invalid credentials
		        2] Check for inactive community
				3] Redirect member to dashboard as per role if correct credential found
	*/
	function login($vEmail, $vPassword, $redirect_to = NULL, $error_view = NULL)
	{	
		
		/*.. Check if user trial exced limit of 3 trial block IP for 10 minutes and show error ..*/
		if($this->bruteCheck( false, $vEmail ))
		{
			$this->CI->session->set_flashdata(array('login' => $this->CI->lang->line('error_block_ip')));
			redirect($_SERVER['HTTP_REFERER']);
					
		}
		/*
			Check if community does not active more prevent user to login and show message
		*/
		$domain = explode("@", $vEmail);
		$domain = $domain[(count($domain)-1)];
		if($this->checkCommunityActiveStatus($domain ) == FALSE)
		{
			$inactive_community = sprintf($this->CI->lang->line('error_inactive_community'), $domain);
			$this->CI->session->set_flashdata(array('login' => $inactive_community));
			redirect($_SERVER['HTTP_REFERER']);				
		}
	     $query = $this->CI->db->get_where('members',
										array(
											'vEmail' => $vEmail,
											'vPassword' => md5($vPassword),
                                            'eStatus'=>'Active',
            								), 1
									  );
			if ($query->num_rows() === 1) 
            {
				
	            $row = $query->row();
				
				 $query_info = $this->CI->db->get_where('member_info',
										array(
											'iMemberid' => $row->iMemberid											
            								), 1
									  );
									  
				 $row2 = $query_info->row();
                
                    if($row->iRoleid==1)
                    {
			                       $data = array(
							        'logged_in' => TRUE,
							        'vEmail' => $row->vEmail,
							        'iMemberid' => $row->iMemberid,
							        'vMemberName' => $row->vMemberName,
							        'iRoleid' => $row->iRoleid,
							        'iCommunityid' => $row->iCommunityid
						          );
						          $this->CI->session->set_userdata($data);
                                  $cookie = array(
                                    'name'   => 'cifm',
                                    'value'  => md5($vPassword),
                                    'prefix' => 'ci_',
                                    'expire' => '3600',
                                    'path'   => '/'
                                    );            
                                    set_cookie($cookie);
									if($query_info->num_rows() === 1 && $row2->vMobile != '')
									{
										redirect(base_url()."dashboard");
									}
									else
									{
										redirect(base_url()."profile");
									}	
                    }
                    else if ($row->iRoleid==2)
                    {
						 $query_info = $this->CI->db->get_where('member_info',
										array(
											'iMemberid' => $row->iMemberid											
            								), 1
									  );
									  
				 	$row2 = $query_info->row();
                        
                                $data = array(
                                    'logged_in' => TRUE,
                                    'vEmail' => $row->vEmail,
                                    'iMemberid' => $row->iMemberid,
                                    'vMemberName' => $row->vMemberName,
                                    'iRoleid' => 1,
                                    'iCommunityid' => $row->iCommunityid
                                  );
                                  $this->CI->session->set_userdata($data);
                                  $cookie = array(
                                    'name'   => 'cifm',
                                    'value'  => md5($vPassword),
                                    'prefix' => 'ci_',
                                    'expire' => '3600',
                                    'path'   => '/'
                                    );            
                                    set_cookie($cookie);
                        
                                 $data = array(
                                    'alogged_in' => TRUE,
                                    'avEmail' => $row->vEmail,
                                    'aiMemberid' => $row->iMemberid,
                                    'avMemberName' => $row->vMemberName,
                                    'aiRoleid' => $row->iRoleid,
                                    'aiCommunityid' => $row->iCommunityid
                                  );
                                  $this->CI->session->set_userdata($data);
                                  $cookie = array(
                                    'name'   => 'cifm',
                                    'value'  => md5($vPassword),
                                    'prefix' => 'ci_',
                                    'expire' => '3600',
                                    'path'   => '/'
                                    );            
                                    set_cookie($cookie);
                                   if($query_info->num_rows() === 1 && $row2->vMobile != '')
									{
										redirect(base_url()."dashboard");
									}
									else
									{
										redirect(base_url()."profile");
									}	
                        
                    }
                    else if($row->iRoleid==3)
                    {
                           $data = array(
                                    'glogged_in' => TRUE,
                                    'gvEmail' => $row->vEmail,
                                    'giMemberid' => $row->iMemberid,
                                    'gvMemberName' => $row->vMemberName,
                                    'giRoleid' => $row->iRoleid,
                                    'giCommunityid' => $row->iCommunityid
                                  );
                                  $this->CI->session->set_userdata($data);
                                  $cookie = array(
                                    'name'   => 'cifm',
                                    'value'  => md5($vPassword),
                                    'prefix' => 'ci_',
                                    'expire' => '3600',
                                    'path'   => '/'
                                    );            
                                    set_cookie($cookie);
                                    redirect(base_url()."dashboard/gadmin");
                    }
			    
           } 
           else 
           {
                //$this->CI->session->set_flashdata("login","<p class='error'> Please enter valid email and password  </p>");
                return false;
		   }
	}
	
    function logged_in()
    {
        return $this->CI->session->userdata('logged_in');
    }

    function logout($redirect_to = NULL)
    {
		$this->CI->session->sess_destroy();
		delete_cookie('ci_cifm');
        redirect(base_url() ."login");	
    }


    function old_password( $password )
	{
		$username =  $this->CI->session->userdata("username");
                $pass = sha1($password);
                $this->CI->db->select('username');
		$query = $this->CI->db->get_where($this->_table['users'], array('username' => $username, 'password' => $pass), 1);
                if ($query->num_rows() !== 1) {
                            return FALSE;
		} else {
                        $this->_username = $username;
			return TRUE;
		}
	}
	 /*
	 Author : Maya joshi
	 Action : Function to allow user 3 attempts to login else block ip for 10 minutes
	 Date : 4th June 2012
	 */
	public function bruteCheck( $failedAttempt = false, $identification = null )
	{
	  
		$ip_address = $this->CI->input->ip_address();
		
		
		$loginAttempts = 4; # save in the config file !!
		$blockTime = 600;  # save in the config file !!
		####
		
		// Is this a failed login attempt ?
		//
		if ( $failedAttempt ):
		// Check if we already have a record of this user.
		//
		
			$record = $this->CI->db->where('ip_address', $ip_address)->get('login_attempts')->row();
			if ( empty( $record ) ):
			// 
			//
				$attempts = 1;
		
				// Create the record.
				//
		
				$this->CI->db->insert('login_attempts', array( 'ip_address' => $ip_address, 'attempts' => $attempts, 'lastLogin' => time() ) );
		
				// We do, update the record.
				//
			else:
			// 
			//
				$attempts = $record->attempts + 1;
		
		
				// Update the user record.
				//
				$this->CI->db->where('ip_address', $ip_address)->update('login_attempts', array( 'attempts' => $attempts, 'lastLogin' => time() ) );
			endif;
		
			// This is not a failed login attempt, but we need to get the user attempts.
			//
			else:
				$record = $this->CI->db->where('ip_address', $ip_address)->get('login_attempts')->row();
				if ( empty( $record ) ):
					$attempts = 1;
				else:
					$attempts = $record->attempts + 1;
				endif;
			endif;
		
			// Check if the user is blocked comparing his login attempts.
			//
			if ( $attempts < $loginAttempts ):
				// User is not blocked.
				//
				$deny_login = false;
				
				// User is blocked now ?
				//
			else:
				// Check if the user block has expired.
				//
			if( ( time() - $record->lastLogin ) > $blockTime ):
				// User is not blocked.
				//
				$deny_login = false;
				
				// Clear the user record.
				//
				$this->CI->db->where('ip_address', $ip_address)->delete('login_attempts');
			else:
				// User is blocked
				//
				$deny_login = true;
			endif;
		endif;
		
		// Clean older login attempts, just to make the database smaller and cleaner.
		//
		# -- todo --
		
		// Do we want to check if the account the user is trying to login is blocked ?
		//
		if ( $identification !== null ):
			// See if we have the userID or the email.
			//
			//if ( is_numeric( $identification ) ):
			//$this->CI->db->where('vEmail', $identification);
			//else
			if( is_string( $identification ) ):
				$this->CI->db->where('vEmail', $identification);
			endif;
		
			// Check if the account the user is trying to login is blocked.
			//
			$record = $this->CI->db->get('members')->row();
				if ( ! empty( $record ) && $record->eStatus === 'Inactive' ):
					$deny_login = true;
					
					// The account is not blocked.
					//
				else:
					// But do we want to block the account ?
					//
					if ( $attempts >= $loginAttempts ):
					// Block the account.
					//
					# -- todo --
					
					// Warn the account owner via email.
					//
					# -- todo --
				endif;
			endif;
		endif;
		
		// Shall we block the user login or not ?
		//
		return $deny_login;
	 }  
	 /*
	 Author : Maya joshi
	 Action : Do check if community is active by getting domain from email
	 Date : 6th June 2012
	 */
	 function checkCommunityActiveStatus($domain)
	 {
	 	
		$this->CI->db->select('iCommunityid');
		$query = $this->CI->db->get_where('community', array('vCommunityDomain' => $domain, 'eStatus' => 'Active'), 1);
		if ($query->num_rows() !== 1) {
					return FALSE;
		}
		else
		{
			return TRUE;
		}			
	 }
}

/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */
