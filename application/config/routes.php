<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/

$route['default_controller'] = "home";
$route['admin'] = "admin";
$route['404_override'] = 'dashboard';

// New url = old Url
$route['user/profile/edit'] = 'user/edit';

$route['distributor/edit/(:num)'] = 'distributor/add/$1';
$route['referrer/edit/(:num)'] = 'referrer/add/$1';
$route['superadmin/category/edit/(:num)'] = 'superadmin/category/add/$1';
$route['superadmin/collection/edit/(:num)'] = 'superadmin/collection/add/$1';
$route['superadmin/product/edit/(:num)'] = 'superadmin/product/add/$1';


//$route['user/profile/edit/[0-9]'] = 'user/edit/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
