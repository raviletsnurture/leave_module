<?php
	$getAnnouncements = getAnnouncements();
	$uri1 = $this->uri->segment(1);
	$uri2 = $this->uri->segment(2);
	$uri3 = $this->uri->segment(3);
	$loginSession = $this->session->userdata("login_session");
	$role_id = $loginSession[0]->role_id;
	$dept_id = $loginSession[0]->department_id;
	$per = getUserpermission($role_id);
	// echo "<br><br><br><br><br><br>";
	// echo "<pre>";
	// print_r($uri2);
	// exit();
?>
<!--sidebar start-->
<aside>
<div id="sidebar"  class="nav-collapse ">
	<ul class="sidebar-menu" id="nav-accordion">
		<li>
			<a class="<?php echo activeSidebar($uri1,'dashboard');?>" href="<?php echo base_url();?>dashboard">
				<i class="fa fa-dashboard"></i><span>Dashboard</span>
			</a>
		</li>

		<?php for($i=0;$i<sizeof($per);$i++) {
			if($per[$i]->module_view == 'yes') { ?>
			<?php if($per[$i]->module_name =='feedback'){ continue;}?>
		<li>
			<a class="<?php if($uri2 == '') { echo activeSidebar($uri1,$per[$i]->module_name); }?>" href="<?php echo base_url()?><?php echo $per[$i]->module_name; ?>" >
				<i class="fa fa-info"></i><span><?php if($per[$i]->module_name == 'leaves'){ echo 'User Request';}else{echo ucfirst($per[$i]->module_name);} ?></span>
			</a>
		</li>
		<?php } } ?>

		<?php //if($role_id == '15' || $role_id == '20') { ?>

		<!-- <li>
			<a class="<?php echo activeSidebar($uri2,'all'); ?>" href="<?php echo base_url()?>leaves/all" >
				<i class="fa fa-laptop"></i><span>User Leaves</span>
			</a>
		<li> -->
		<?php //}  ?>

		<li>
			<a class="<?php echo activeSidebar($uri1,'salary');?>" href="<?php echo base_url()?>salary" >
				<i class="fa fa-book"></i>
				<span>Salary Slips</span>
			</a>
		</li>

		<li>
			<a class="<?php echo activeSidebar($uri1,'form16');?>" href="<?php echo base_url()?>form16" >
				<i class="fa fa-file-text"></i>
				<span>Form 16</span>
			</a>
		</li>

		<li>
			<a class="<?php echo activeSidebar($uri1,'reimbursement');?>" href="<?php echo base_url()?>reimbursement" >
				<i class="fa fa-money"></i>
				<span>Reimbursement</span>
			</a>
		</li>

		<li>
			<a class="<?php echo activeSidebar($uri1,'interview');?>" href="<?php echo base_url()?>interview" >
				<i class="fa fa-user"></i>
				<span>Interview</span>
			</a>
		</li>

<!-- <li class="sub-menu">
<a class="<?php echo activeSidebar($uri1,'actions');?>" href="<?php echo base_url()?>feedback" >
  <i class="fa fa-comment"></i>
  <span>My Feedbacks</span>
</a>
</li> -->

<?php //for($i=0;$i<sizeof($per);$i++) {
	//if($per[$i]->module_add == 'yes' && $per[$i]->module_name =='feedback') { ?>
		<li class="">
		<a class="<?php echo activeSidebar($uri1,'viewFeedback');?>" href="<?php echo base_url()?>developerfeedback/viewFeedback" >
			<i class="fa fa-star"></i>
			<span>Feedback</span>
		</a>
		</li>
<?php //}}?>

<li class="">
<a class="<?php echo activeSidebar($uri1,'meeting');?>" href="<?php echo base_url()?>meeting" >
  <i class="fa fa-users"></i>
  <span>Meeting</span>
</a>
</li>
<li class="">
<a class="<?php echo activeSidebar($uri1,'holidays');?>" href="<?php echo base_url()?>holidays" >
  <i class="fa fa-plane" aria-hidden="true"></i>
  <span>Holidays</span>
</a>
</li>
<li class="">
<a class="<?php echo activeSidebar($uri1,'policies');?>" href="<?php echo base_url()?>policies">
  <i class="fa fa-file-text-o" aria-hidden="true"></i>
  <span>HR Policy</span>
</a>
</li>
<?php if ($role_id == 15 || $role_id == 20) {?>
	<li>
		<a class="<?php echo activeSidebar($uri1,'announcement');?>" href="<?php echo base_url();?>announcement">
			<i class="fa fa-th-large"></i><span>Announcements</span>
		</a>
	</li>
<?php } ?>
<li class="">
<a class="<?php echo activeSidebar($uri1,'visitor');?>" href="<?php echo base_url()?>visitor">
  <i class="fa fa-user-o" aria-hidden="true"></i>
  <span>Visitors</span>
</a>
</li>

<li class="">
<a class="<?php echo activeSidebar($uri1,'timelog');?>" href="<?php echo base_url()?>timelog">
  <i class="fa fa-clock-o" aria-hidden="true"></i>
  <span>Time</span>
</a>
</li>

<li>
	<a class="<?php if($uri1 == 'rewards' || $uri2 == 'rewards/claim' || $uri2 == 'rewards/exreward' || $uri2 == 'rewards/rewardproduct' || $uri2 == 'rewards/expointsteam' || $uri2 == 'leave_status' || $uri2 == 'cast' || $uri2 == 'role' || $uri2 == 'auditlogs' || $uri2 == 'leave_type' || $uri2 == 'holidays' || $uri2 == 'policies' || $uri2 == 'skill' || $uri2 == 'timelog')  { echo "active"; } ?>" href="#">
		<i class="fa fa-gift"></i><span>Rewards</span>
	</a>
	<ul>
		<li>
			<a class="<?php echo activeSidebar($uri2,'rewards');?>" href="<?php echo base_url();?>rewards">
				<i class="fa fa-th-large"></i><span>Reward</span>
			</a>
		</li>
		<li>
			<a class="<?php echo activeSidebar($uri2,'claim');?>" href="<?php echo base_url();?>rewards/claim">
				<i class="fa fa-th-large"></i><span>Redeem Reward</span>
		</a>
		</li>
		<?php if($role_id == '13' || $role_id == '15' || $role_id == '20' || $role_id == '22' || $role_id == '38') { ?>

		<li>
			<a class="<?php echo activeSidebar($uri2,'requets'); ?>" href="<?php echo base_url()?>rewards/requets" >
				<i class="fa fa-laptop"></i><span>User Rewards</span>
			</a>
		</li>
		<?php }  ?>

		<?php /* if($role_id == '13' || $role_id == '15' || $role_id == '20' || $role_id == '22' || $role_id == '38') { ?>

		<!--<li>
			<a class="<?php echo activeSidebar($uri2,'expoints'); ?>" href="<?php echo base_url()?>rewards/expoints" >
				<i class="fa fa-laptop"></i><span>Exeptional Rewards</span>
			</a>
		</li> -->
		<?php } */  ?>

		<?php if($role_id == '15' || $role_id == '20' || $role_id == '38') { ?>

		<li>
			<a class="<?php echo activeSidebar($uri2,'expointsteam'); ?>" href="<?php echo base_url()?>rewards/expointsteam" >
				<i class="fa fa-laptop"></i><span>Exeptional Rewards</span>
			</a>
		</li>
		<?php }  ?>

		<?php if($role_id == '13' || $role_id == '15' || $role_id == '20' || $role_id == '22' || $role_id == '38') { ?>

		<li>
			<a class="<?php echo activeSidebar($uri2,'negative'); ?>" href="<?php echo base_url()?>rewards/negative" >
				<i class="fa fa-laptop"></i><span>Negative Rewards</span>
			</a>
		</li>
		<?php }  ?>

		<li>
			<a class="<?php echo activeSidebar($uri2,'redeemhistory');?>" href="<?php echo base_url();?>rewards/redeemhistory">
				<i class="fa fa-th-large"></i><span>Redeem History</span>
		</a>
		</li>

		<li>
			<a class="<?php echo activeSidebar($uri2,'rank');?>" href="<?php echo base_url();?>rewards/rank">
				<i class="fa fa-th-large"></i><span>Reward Rank</span>
		</a>
		</li>

		<!--<li>
			<a class="<?php echo activeSidebar($uri2,'eom');?>" href="<?php echo base_url();?>rewards/eom">
				<i class="fa fa-th-large"></i><span>Employee Of the Month</span>
		</a>
		</li>
		<li>
			<a class="<?php echo activeSidebar($uri2,'rewardproduct');?>" href="<?php echo base_url();?>rewards/rewardproduct">
				<i class="fa fa-th-large"></i><span>Reward Product</span>
		</a>
		</li>-->
	</ul>
</li>


<?php if($dept_id == '16' || $dept_id == '11') { ?>
<li class="">
	<a class="<?php if($uri1 == 'inventory')  { echo "active"; } ?>" href="#">
		<i class="fa fa-th"></i><span>Inventory Management</span>
	</a>
	<ul>
		<li>
			<a class="<?php echo activeSidebar($uri2,'vendor');?> " href="<?php echo base_url();?>inventory/vendor">
				<i class="fa fa-mobile" aria-hidden="true" style="font-size: 20px;"></i><span>Vendors</span>
			</a>
		</li>
		<li>
			<a class="<?php echo activeSidebar($uri2,'inventoryTypes');?>" href="<?php echo base_url();?>inventory/inventoryTypes">
				<i class="fa fa-user"></i><span>Inventory category</span>
			</a>
		</li>

		<li>
			<a class="<?php echo activeSidebar($uri2,'allInventory');?>" href="<?php echo base_url();?>inventory/allInventory">
				<i class="fa fa-user"></i><span>Inventory</span>
			</a>
		</li>

		<li>
			<a class="<?php echo activeSidebar($uri2,'assignInventory');?>" href="<?php echo base_url();?>inventory/assignInventory">
				<i class="fa fa-user"></i><span>Assign Inventory</span>
			</a>
		</li>
		<li>
			<a class="<?php echo activeSidebar($uri2,'repairInventory');?>" href="<?php echo base_url();?>inventory/repairInventory">
				<i class="fa fa-user"></i><span>Inventory Repair</span>
			</a>
		</li>
	</ul>
</li>
<?php }  ?>
</ul>

	<!-- sidebar Announcements-->
    <div class="announcementdiv">
        <h3>
              <i class="fa fa-bullhorn"></i>
              Announcements
        </h3>
        <div class="ajax_meat_container"></div>
    </div>
    <!-- sidebar Announcements-->
</div>
</aside>
<!--sidebar end-->
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- ajax code for post- -->
<script type='text/javascript'>
/*
 * @Author:  krunal
 * @Created: September 02 2015
 * @Modified By:
 * @Modified at:
 * @Comment: Ajax code for Meat Post paggination
 */
function meatPostPagination(pageId){
     var dataString = 'pageId='+ pageId+'&action=meat_post';
     $.ajax({
           type: "POST",
           url: "<?php echo base_url()?>dashboard/getAnnouncementsSidebar",
           data: dataString,
           cache: false,
           success: function(result){
                 $(".ajax_meat_container").html(result);
           }
      });
}
jQuery(document).ready(function() {
	meatPostPagination('0');
	$(document).on('click','.meat_page',function(e){
		e.preventDefault();
		var pageId=$(this).attr('id');
			meatPostPagination(pageId);
	})
});// End ajax for Meat Post
</script>
