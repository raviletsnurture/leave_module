<?php
	$uri1 = $this->uri->segment(1);
	$uri2 = $this->uri->segment(2);
	$uri3 = $this->uri->segment(3);
	// echo "<br><br><br><br>";
	// print_r($uri2);
	// exit;
?>

<!--sidebar start-->
<aside>
	<div id="sidebar"  class="nav-collapse ">
		<!-- sidebar menu start-->
		<ul class="sidebar-menu" id="nav-accordion">
			<li>
				<a class="<?php echo activeSidebar($uri2,'dashboard');?>" href="<?php echo base_url();?>superadmin/dashboard">
					<i class="fa fa-dashboard"></i><span>Dashboard</span>
				</a>
			</li>

			<li>
				<a class="<?php echo activeSidebar($uri2,'users');?>" href="<?php echo base_url();?>superadmin/users">
				  <i class="fa  fa-th"></i><span>Employee</span>
				</a>
			</li>

			<li>
				<a class="<?php echo activeSidebar($uri2,'documents');?>" href="<?php echo base_url();?>superadmin/documents">
				  <i class="fa  fa-file"></i><span>Documents</span>
				</a>
			</li>
			<!-- <li>
				<a class="<?php //echo activeSidebar($uri2,'templates');?>" href="<?php //echo base_url();?>superadmin/templates">
				  <i class="fa fa-th-large"></i><span>Email Templates</span>
				</a>
			</li> -->
			<li>
				<a class="<?php echo activeSidebar($uri2,'leave');?>" href="<?php echo base_url();?>superadmin/leave">
				  <i class="fa fa-th-large"></i><span>Request</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'leave_records');?>" href="<?php echo base_url();?>superadmin/leave_records">
				  <i class="fa fa-th-large"></i><span>Leave Records</span>
				</a>
			</li>
			<!-- Request Swaps -->
			<li>
				<a class="<?php echo activeSidebar($uri2,'request_swap');?>" href="<?php echo base_url();?>superadmin/request_swap">
				  <i class="fa fa-th-large"></i><span>Request Swap</span>
				</a>
			</li>
			
			<li>
				<a class="<?php echo activeSidebar($uri2,'interview');?>" href="<?php echo base_url();?>superadmin/interview/interviews">
				  <i class="fa fa-th-large"></i><span>Interview</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'recruitment');?>" href="<?php echo base_url();?>superadmin/recruitment/recruitments">
					<i class="fa fa-th-large"></i><span>Recruitment</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'reimbursement');?>" href="<?php echo base_url();?>superadmin/reimbursement">
				  <i class="fa fa-money"></i><span>Reimbursement</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'feedback');?>" href="<?php echo base_url();?>superadmin/feedback">
					<i class="fa fa-th-large"></i><span>Feedbacks</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'announcement');?>" href="<?php echo base_url();?>superadmin/announcement">
					<i class="fa fa-th-large"></i><span>Announcements</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'notification');?>" href="<?php echo base_url();?>superadmin/notification">
					<i class="fa fa-th-large"></i><span>Notification</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'visitorapp');?>" href="<?php echo base_url();?>superadmin/visitorapp">
					<i class="fa fa-th-large"></i><span>Visitor App</span>
				</a>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'campus');?>" href="<?php echo base_url();?>superadmin/campus">
					<i class="fa fa-th-large"></i><span>Campus</span>
				</a>
			</li>
			<li>
				<a class="<?php if($uri2 == 'rewards' || $uri2 == 'rewards/add' || $uri2 == 'rewards/history' || $uri2 == 'rewards/exreward' || $uri2 == 'rewards/rewardproduct' || $uri2 == 'rewards/userrewardpoints' || $uri2 == 'rewards/negative' || $uri2 == 'rewards/eom' || $uri2 == 'rewards/redeemhistory' || $uri2 == 'rewards/pmpoints' || $uri2 == 'auditlogs' || $uri2 == 'leave_type' || $uri2 == 'holidays' || $uri2 == 'policies' || $uri2 == 'skill' || $uri2 == 'timelog')  { echo "active"; } ?>" href="#">
					<i class="fa fa-gift"></i><span>Rewards</span>
				</a>
				<ul>
					<li>
						<a class="<?php echo activeSidebar($uri2,'points');?>" href="<?php echo base_url();?>superadmin/rewards/points">
							<i class="fa fa-th-large"></i><span>Reward Points</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri3,'request');?>" href="<?php echo base_url();?>superadmin/rewards/request">
							<i class="fa fa-th-large"></i><span>Reward Request</span>
					</a>
					</li>

					<li>
						<a class="<?php echo activeSidebar($uri3,'eom');?> <?php echo activeSidebar($uri3,'addeom');?>" href="<?php echo base_url();?>superadmin/rewards/eom">
							<i class="fa fa-th-large"></i><span>Employee Of the Month</span>
					</a>
					</li>

					<li>
						<a class="<?php echo activeSidebar($uri3,'userrewardpoints');?>" href="<?php echo base_url();?>superadmin/rewards/userrewardpoints">
							<i class="fa fa-th-large"></i><span>User Reward Points</span>
					</a>
					</li>

					<!--<li>
						<a class="<?php //echo activeSidebar($uri3,'exreward');?>" href="<?php //echo base_url();?>superadmin/rewards/exreward">
							<i class="fa fa-th-large"></i><span>Exceptional Reward</span>
					</a>
					</li> -->

					<li>
						<a class="<?php echo activeSidebar($uri3,'pmpoints');?>" href="<?php echo base_url();?>superadmin/rewards/pmpoints">
							<i class="fa fa-th-large"></i><span>PM Reward Points</span>
					</a>
					</li>

					<li>
						<a class="<?php echo activeSidebar($uri3,'negative');?>" href="<?php echo base_url();?>superadmin/rewards/negative">
							<i class="fa fa-th-large"></i><span>Negative Reward</span>
					</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri3,'rewardproduct');?>" href="<?php echo base_url();?>superadmin/rewards/rewardproduct">
							<i class="fa fa-th-large"></i><span>Reward Product</span>
					</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri3,'redeemhistory');?>" href="<?php echo base_url();?>superadmin/rewards/redeemhistory">
							<i class="fa fa-th-large"></i><span>Claim Product</span>
					</a>
					</li>

				</ul>
			</li>
			<li>
				<a class="<?php if($uri2 == 'country' || $uri2 == 'state' || $uri2 == 'city' || $uri2 == 'department' || $uri2 == 'religion' || $uri2 == 'customer_status' || $uri2 == 'leave_status' || $uri2 == 'cast' || $uri2 == 'role' || $uri2 == 'auditlogs' || $uri2 == 'leave_type' || $uri2 == 'holidays' || $uri2 == 'policies' || $uri2 == 'skill' || $uri2 == 'timelog')  { echo "active"; } ?>" href="#">
					<i class="fa fa-th"></i><span>Settings</span>
				</a>
				<ul>
					<li>
						<a class="<?php echo activeSidebar($uri2,'country');?>" href="<?php echo base_url();?>superadmin/country">
							<i class="fa fa-th-large"></i><span>Country</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'state');?>" href="<?php echo base_url();?>superadmin/state">
							<i class="fa fa-th-large"></i><span>States</span>
					</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'city');?>" href="<?php echo base_url();?>superadmin/city">
							<i class="fa fa-th-large"></i><span>Cities</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'department');?>" href="<?php echo base_url();?>superadmin/department">
						  <i class="fa fa-th-large"></i><span>Departments</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'leave_status');?>" href="<?php echo base_url();?>superadmin/leave_status">
						  <i class="fa fa-th-large"></i><span>Add Request Status</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'leave_type');?>" href="<?php echo base_url();?>superadmin/leave_type">
						  <i class="fa fa-th-large"></i><span>Add Request Types</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'skill');?>" href="<?php echo base_url();?>superadmin/skill">
						  <i class="fa fa-th-large"></i><span>Skill</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'religion');?>" href="<?php echo base_url();?>superadmin/religion">
						  <i class="fa fa-th-large"></i><span>Religions</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'cast');?>" href="<?php echo base_url();?>superadmin/cast">
						  <i class="fa fa-th-large"></i><span>Casts</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'auditlogs');?>" href="<?php echo base_url();?>superadmin/auditlogs">
							<i class="fa fa-th-large"></i><span>AuditLogs</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'role');?>" href="<?php echo base_url();?>superadmin/role">
							<i class="fa fa-th-large"></i><span>User Roles (Permission)</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'kra');?>" href="<?php echo base_url();?>superadmin/kra">
							<i class="fa fa-th-large"></i><span>Manage KRA</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'holidays');?>" href="<?php echo base_url();?>superadmin/holidays">
							<i class="fa fa-plane" aria-hidden="true"></i>
						  <span>Holidays</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'policies');?>" href="<?php echo base_url();?>superadmin/policies">
							<i class="fa fa-file-text-o" aria-hidden="true"></i>
						  <span>Policies</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'timelog');?>" href="<?php echo base_url();?>superadmin/timelog">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
						  <span>Time</span>
						</a>
					</li>
					<li>
						<a class="<?php echo activeSidebar($uri2,'Jobopening');?>" href="<?php echo base_url();?>superadmin/recruitment/jobopening">
							<i class="fa fa-th-large" aria-hidden="true"></i>
						  <span>Job Opening</span>
						</a>
					</li>

				</ul>
			</li>
			<li>
				<a class="<?php echo activeSidebar($uri2,'export_mysql');?>" href="<?php echo base_url();?>superadmin/export_mysql">
					<i class="fa fa-th-large"></i><span>Export Database</span>
			    </a>
			</li>
		</ul>
		<!-- sidebar menu end-->
		<!-- sidebar Announcements-->
	    <div class="announcementdiv">
	        <h3>
	              <i class="fa fa-bullhorn"></i>
	              Announcements
	        </h3>
	        <div class="ajax_meat_container"></div>
	    </div>
	    <!-- sidebar Announcements-->
	</div>
</aside>
<!--sidebar end-->

<!--sidebar end-->
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- ajax code for post- -->
<script type='text/javascript'>
/*
 * @Author:  krunal
 * @Created: September 02 2015
 * @Modified By:
 * @Modified at:
 * @Comment: Ajax code for Meat Post paggination
 */
function meatPostPagination(pageId){
     var dataString = 'pageId='+ pageId+'&action=meat_post';
     $.ajax({
           type: "POST",
           url: "<?php echo base_url()?>superadmin/dashboard/getAnnouncementsSidebar",
           data: dataString,
           cache: false,
           success: function(result){
                 $(".ajax_meat_container").html(result);
           }
      });
}
jQuery(document).ready(function() {
	meatPostPagination('0');
	$(document).on('click','.meat_page',function(e){
		e.preventDefault();
		var pageId=$(this).attr('id');
			meatPostPagination(pageId);
	})
});// End ajax for Meat Post
</script>
