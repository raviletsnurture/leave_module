<!--header start-->
<?php $sLoginSession = $this->session->userdata("sLogin_session");
//pr($sLoginSession);
?>
<?php error_reporting(E_ALL);?>
<header class="header white-bg">
  <div class="sidebar-toggle-box">
    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
  </div>
  <!--logo start-->
  <a href="<?php echo base_url().'superadmin/dashboard';?>" class="logo">HRMS</a>
  <!--logo end-->
  <div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
      <!-- settings start -->
        <li><a href="<?php echo base_url().'superadmin/auditlogs';?>">Audit Logs</a></li>
        <li><a href="<?php echo base_url().'superadmin/salary';?>">Salary Slips</a></li>
        <li><a href="<?php echo base_url().'superadmin/form16';?>">Form 16</a></li>
        <li><a href="<?php echo base_url().'superadmin/timelog';?>">Time Log</a></li>
    </ul>
    <!--  notification end -->
  </div>
  <div class="top-nav ">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
      <!-- user login dropdown start-->
      <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <?php getImage('uploads/images/',$sLoginSession[0]->sUsername,$extraParams = 'class="profile-pic"');?>
        <span class="username"><?php echo $sLoginSession[0]->sUsername;?></span> <b class="caret"></b> </a>
        <ul class="dropdown-menu extended logout">
          <div class="log-arrow-up"></div>
          <li><a href="<?php echo base_url();?>superadmin/dashboard/profile"><i class=" fa fa-suitcase"></i>Profile</a></li>
          <li><a href="<?php echo base_url();?>"><i class="fa fa-cog"></i> Settings</a></li>
          <li><a href="<?php echo base_url();?>"><i class="fa fa-bell-o"></i> Notification</a></li>
          <li><a href="<?php echo base_url();?>logout/superadmin"><i class="fa fa-key"></i> Log Out</a></li>
        </ul>
      </li>
      <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
  </div>
  <div id="loaderAjax" class="loadermodal" style="display:none;">
    <div class="loadercenter">
        <img alt="" src="<?php echo base_url(); ?>assets/img/loader.gif" />
    </div>
</div>
</header>
<!--header end-->
