  <?php
$loginSession = $this->session->userdata("login_session");
$loginSessionData = getLoggedInUser($loginSession[0]->user_id);
$getBirthday = getBirthday();
$uleaves = getUserLeavesByDepartment($loginSessionData[0]->department_id);
$karmaDetails = getKarmaDetails($loginSession[0]->user_id);
$gettotalvacancy = get_total_vacancy();
$getUserById = getUserByid($loginSession[0]->user_id);
?>

<header class="header white-bg">
  <div class="sidebar-toggle-box">
    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
  </div>
  <!--logo start-->
  <a href="<?php echo base_url();?>" class="logo">HRMS</a>
  <!--logo end-->


  <div class="nav notify-row" id="top_menu">
    <ul class="nav top-menu">
      <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="fa  fa-flag"></i> <span class="badge bg-success"><?php echo count($getBirthday);?></span> </a>
        <ul class="dropdown-menu extended tasks-bar">
          <div class="notify-arrow notify-arrow-green"></div>
          <li>
            <p class="green">You have upcoming birthday(s)</p>
          </li>
           <?php
      		   $in = array('danger','warning','success','info');
      		   foreach($getBirthday as $birth){?>
      		   <li>
      				<a href="javascript:void(0);"> <span class="label label-<?php echo $in[rand(0,count($in)-1)];?> notofication_span"><i class="fa fa-thumbs-up"></i></span>
      					<div class="notofication_div"><?php echo $birth['first_name'].' '.$birth['last_name']; ?> turns <?php echo getAge($birth['birthdate']) .' on '. date('dS, F',strtotime($birth['birthdate'])).' '.date('Y');?></div>
      				</a>
      			</li>
          <?php } ?>
          <?php if(count($getBirthday) == 0){?>
          <li class="no-noti">
            <a href="#">
              No new notification
            </a>
          </li>
          <?php }?>
        </ul>
      </li>


	  <?php /*if($loginSessionData[0]->role_id == '13' || $loginSessionData[0]->role_id == '15' || $loginSessionData[0]->role_id == '16' || $loginSessionData[0]->role_id == '20') { ?>
	  <li id="header_notification_bar" class="dropdown">
		<a data-toggle="dropdown" class="dropdown-toggle" href="#">
			<i class="fa fa-bell-o"></i>
			<span class="badge bg-warning"><?php echo count($uleaves); ?></span>
		</a>
			<ul class="dropdown-menu extended notification">
			  <div class="notify-arrow notify-arrow-yellow"></div>
			  <li>
				  <p class="yellow">Upcoming User Leaves</p>
			  </li>
			  <?php foreach($uleaves as $row) { ?>
			  <li>
				  <a href="javascript:void(0);">
					  <span class="label" style="<?php if($row['leave_status_name'] == 'Approved') { echo 'background-color: #378006'; } elseif($row['leave_status_name'] == 'Rejected') {  echo 'background-color: #fa5578'; } else { echo 'background-color: #6883a3'; } ?>" ><i class="fa fa-bolt"></i></span>
					  <?php echo $row['first_name'].' '.$row['last_name']; ?>
					  <span class="small italic">- <?php
            $date = new DateTime($row['leave_start_date']);
            echo $date->format('dS, M Y');
            ?></span>
				  </a>
			  </li>
			  <?php } ?>
			  <?php if(count($uleaves) == 0){?>
			  <li class="no-noti"><a href="#">No Recent Leaves</a></li>
			  <?php }?>
			  <li>
				  <a href="<?php echo base_url(); ?>leaves">See all leaves</a>
			  </li>
			</ul>
		</li>
	    <?php } */?>
    </ul>
  </div>


	<div class="top-nav ">
		<ul class="nav pull-right top-menu">
		  <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#">
      <?php 
      if($getUserById[0]['user_pic']!=NULL && $getUserById[0]['user_pic']!=''){?>
        <img name="userProfile" class="profile-pic" src="<?php echo base_url().'/uploads/userProfileImage/'.$getUserById[0]['user_pic']; ?>" height="100" alt="">
      <?php }else{
        getImage('uploads/user_images/resized/',$loginSessionData[0]->user_pic,$extraParams = 'class="profile-pic"');
      }
      
      ?>
			<span class="username"><?php echo $loginSessionData[0]->first_name . ' ' . $loginSessionData[0]->last_name;?></span> <b class="caret"></b> </a>
			<ul class="dropdown-menu extended logout">
			  <div class="log-arrow-up"></div>
			  <li><a href="<?php echo base_url();?>user/profile"><i class=" fa fa-suitcase"></i>Profile</a></li>
			  <li><a href="<?php echo base_url();?>"><i class="fa fa-cog"></i> Settings</a></li>
			  <li><a href="<?php echo base_url();?>announcement/all_announcements"><i class="fa fa-bell-o"></i> Notification</a></li>
			  <li><a href="<?php echo base_url();?>logout"><i class="fa fa-key"></i> Log Out</a></li>
			</ul>
		  </li>
		</ul>
    <ul class="points">
      <li>Karma: <b><?php echo number_format($karmaDetails['karma_points']); ?></b></li>
      <li>Reward: <b><?php echo number_format($karmaDetails['reward_points']); ?></b></li>
      <li>Rank: <b><?php echo number_format($karmaDetails['rank']); ?></b></li>
    </ul>
    <?php if(!empty($gettotalvacancy)){  ?>
    <ul class="current-opening nav pull-right top-menu">
      <li class="dropdown"> 
          <a data-toggle="dropdown" class="dropdown-toggle" onclick="getopening()" href="javascript:;"><span class="username">Current Opening</span></a>       
      </li>
    </ul>
    <?php } ?>
  </div>
  <div id="loaderAjax" class="loadermodal" style="display:none;">
    <div class="loadercenter">
        <img alt="" src="<?php echo base_url(); ?>assets/img/loader.gif" />
    </div>
  </div>
  <?php if(!empty($gettotalvacancy)){ ?>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Job Vacancy</h4>
        </div>
        <div class="modal-body">
          <div class="row-fluid">
          <div class="span9">
            <div class="new-list">
              <div class="item panel" id="myprofile">
                <table class="table myprofile_tbl">              
                  <tbody>                      
                    <?php foreach($gettotalvacancy as $Vacancy){ ?>
                      <tr>
                        <td width="250"><b><?php echo $Vacancy->technology; ?></b></td>
                        <td><?php echo $Vacancy->vacancy; ?></td>
                      </tr>                                          
                    <?php } ?>
                  </tbody>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <style type="text/css">
    .modal-backdrop{
      z-index: 970 !important;
    }
  </style>
  <?php } ?>
</header>
<!--header end-->
<script type="text/javascript">
  function getopening()
  { 
    $('#myModalLabel').show();         
    $('#myModal').modal('show');
  }
</script>
