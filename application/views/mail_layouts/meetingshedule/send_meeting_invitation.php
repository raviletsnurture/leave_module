<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<title>Announcement</title>
</head>
<body style="font-family:Arial, Helvetica, sans-serif; margin:0; padding:0; ">
<table style="width:600px; margin:0 auto; background:#303f56; padding:0px 20px 0 20px;">
	<tr>
    	<td style="text-align:center; padding:30px 0 30px 0;">
			<a style="font-size:13px; color:#FFF; font-weight:normal;" href="hrms.letsnurture.com">
				<img src="<?php echo base_url()?>assets/img/hrms_logo.png" alt="HRMS" />
			</a>
		</td>
    </tr>
    <tr>
    	<td style="padding:35px 0px; background:#303f56;">
        	<table>
            	<tr>
                	<td colspan="2" style="font-size:16px; color:#FFF; padding:0 40px 20px 40px;">Hello,</td>
                </tr>
                <?php echo $description; ?>
                <tr>
                	<td style="font-size:16px; line-height:20px; color:#FFF; font-weight:normal; padding:50px 0 0 40px;">
                    	Thank you,<br>
						<strong style="color:#ffac3e;"><?php echo REGARDS_NAME; ?></strong>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
		<tr>
			<td>
			<table width="100%">
				<tr>
					<td  align="center" colspan="2" style="font-size:16px; color:#FFF; padding:0 40px 20px 40px;">Tips for Beautiful Meeting</td>
				</tr>
				<tr>
					<td align="center" style="font-size:14px; color:#FFF;">Stay on Time.</td>
					<td align="center" style="font-size:14px; color:#FFF;">Come with an Agenda.</td>

				</tr>
				<tr>
					<td align="center" style="font-size:14px; color:#FFF;">Conclude on Time.</td>
					<td align="center" style="font-size:14px; color:#FFF;">Share MoM with all.</td>

				</tr>
				<tr>
					<td align="center" style="font-size:14px; color:#FFF;">Contribute by Asking or Answering.</td>
					<td align="center" style="font-size:14px; color:#FFF;">State your goals.</td>
				</tr>

			</table>
			<td>
		</tr>
    <tr>
    	<td style="font-size:11px; color:#FFF; font-weight:normal; text-transform:uppercase; text-align:center; padding:14px 0; "> &#9400; <?php echo date('Y'); ?> HRMS, ALL RIGHTS RESERVED</td>
    </tr>
</table>
</body>
</html>
