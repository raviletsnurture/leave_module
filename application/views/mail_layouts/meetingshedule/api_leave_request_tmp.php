
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<title>Leave Request</title>
</head>
<body style="font-family:Arial, Helvetica, sans-serif; margin:0; padding:0; ">
<table style="width:600px; margin:0 auto; background:#303f56; padding:0px 20px 0 20px;">
	<tr>
    	<td style="text-align:center; padding:30px 0 30px 0;">
			<a style="font-size:13px; color:#FFF; font-weight:normal;" href="hrms.letsnurture.com">
				<img src="<?php echo base_url()?>assets/img/hrms_logo.png" alt="HRMS" />
			</a>
		</td>
    </tr>
    <tr>
    	<td style="padding:35px 0px; background:#303f56;">
        	<table>
            	<tr>
                	<td colspan="2" style="font-size:16px; color:#FFF; padding:0 40px 20px 40px;">Hello,</td>
                </tr>

                <tr>
                  	<td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
                      <b style="color:#03A9F4;"><?php echo $first_name; ?>  <?php echo $last_name; ?> </b> requested for <?php echo $leave_text_msg['leave_text_msg'];?> from <b style="color:#03A9F4;"><?php echo date('d-M-Y',strtotime($leave_start_date)); ?> </b>to <b style="color:#03A9F4;">
													<?php echo date('d-M-Y',strtotime($leave_end_date)); ?></b>
                    </td>
                </tr>
                <tr>
                  	<td colspan="2" style="font-size:14px; color:#FFF; padding:0 40px 20px 40px;">
                      Reason: <?php echo $leave_reason; ?>
                    </td>
                </tr>

                <tr>
                	<td style="font-size:16px; line-height:20px; color:#FFF; font-weight:normal; padding:50px 0 0 40px;">
                    	Thank you,<br>
						<strong style="color:#ffac3e;"><?php echo REGARDS_NAME; ?></strong>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="font-size:11px; color:#FFF; font-weight:normal; text-transform:uppercase; text-align:center; padding:14px 0; "> &#9400; <?php echo date('Y'); ?> HRMS, ALL RIGHTS RESERVED</td>
    </tr>
</table>
</body>
</html>
