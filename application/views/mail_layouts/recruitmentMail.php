<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<title>HRMS</title>
<style>
tr table{width: 100%; }
table.taskList td,
table.taskList th {
    border: 1px solid #cdcdcd;
		border-top: none;
		border-right: none;
    text-align: left;
    padding: 8px;
    color: #000;
}
table.taskList th{
	border-top: 1px solid #cdcdcd;
}
table.taskList th{
    background-color: #555555;
    color: #fff;
}
table.taskList td:last-child,
table.taskList th:last-child{border-right: 1px solid #cdcdcd;}
table.taskList td{
	background-color: #fff;
	color: #333;
	font-size: 14px;
}
</style>
</head>
<body style="font-family:Arial, Helvetica, sans-serif; margin:0; padding:0; ">
<table style="width:900px; margin:0 auto; background:#303f56; padding:0px 20px 0 20px;">
	<tr>
    	<td style="text-align:center; padding:30px 0 30px 0;">
				<a style="font-size:13px; color:#FFF; font-weight:normal;" href="hrms.letsnurture.com">
					<img src="<?php echo base_url()?>assets/img/hrms_logo.png" alt="HRMS" />
				</a>
			</td>
  </tr>
   <tr>
    	<td style="padding:35px 0px; background:#303f56;">
        	<table>
                <?php echo $body; ?>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="font-size:11px; color:#FFF; font-weight:normal; text-transform:uppercase; text-align:center; padding:14px 0; "> &#9400; <?php echo date('Y'); ?> HRMS, ALL RIGHTS RESERVED</td>
    </tr>
</table>
</body>
</html>
