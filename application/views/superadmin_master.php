<?php noocache(); ?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="<?php echo base_url();?>uploads/images/favicon.ico">

    <title>HRMS <?php echo $titalTag;?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link href="<?php echo base_url();?>assets/js/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <link href="<?php echo base_url();?>assets/css/tasks.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css" type="text/css">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url();?>assets/js/jquery-multi-select/css/multi-select.css" />
    <link href="<?php echo base_url();?>assets/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/css/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/css/custom_style.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
    <script>var baseUrl = '<?php echo base_url();?>';</script>

    <link href="<?php echo base_url();?>assets/css/select2.min.css" rel="stylesheet" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132698484-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-132698484-1');
</script>
  </head>
  <body>

  <section id="container" >

<?php $this->load->view("superadmin_header"); ?>
<?php $this->load->view("superadmin_sidebar"); ?>
      <!--main content start-->
      <?php echo $contents; ?>
      <?php //echo $this->load->view('sendsmstemp'); ?>
      <?php //echo $this->load->view('sendemailtemp'); ?>
      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              <?php echo date('Y');?> &copy; <a href="<?php echo base_url()?>" class="logo-footer">HRMS</a>
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->

    <!--<script src="js/jquery-1.8.3.min.js"></script>-->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.js" ></script>
    <script src="<?php echo base_url();?>assets/js/jquery.customSelect.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/respond.min.js" ></script>

    <!--common script for all pages-->
    <script src="<?php echo base_url();?>assets/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url();?>assets/js/sparkline-chart.js"></script>
    <script src="<?php echo base_url();?>assets/js/easy-pie-chart.js"></script>

    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/tasks.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/js/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/external-dragging-calendar.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $(".js-example-basic-single").select2();
    });
    </script>
    <script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url();?>assets/js/additional-methods.min.js"></script>

  </body>
</html>
