<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Homehunter</title>
<?php

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="Real Estate" />
<meta name="description" content="Proper - Real Estate Responsive HTML5 Template">
<meta name="author" content="yobio.indiewebstyle.com">
<!-- Bootstrap -->
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url();?>assets/css/style_new.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" media="screen">
<!-- FONTAWESOME STYLE -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css"/>
<!--[if IE 7]>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome-ie7.min.css">
<![endif]-->

<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo.png">
<?php /*?><link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
<?php */?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
<?php /*?><script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script><?php */?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
<script src="//ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&mkt=en-gb"></script>
<script>
var bingmap_key = '<?php echo $this->config->item('bingmap_key')?>';
var baseUrl = "<?php echo base_url();?>";
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/defaultMe.js"></script>
<script>
</script>
</head>
<?php
$controller = $this->router->class;
$method_type = $this->uri->segment(2);
$method_type2 = $this->uri->segment(1);
//generate last url
if($method_type2 != 'login' && $method_type2 != 'register'){
	$lastUrl = getCurrentUrl();
	$this->session->set_userdata('afterLoginUrl',$lastUrl);
}

//echo $this->session->userdata('afterLoginUrl');
// currency convert
$currSession = trim($this->session->userdata('currencyCode'));

if(!$this->session->userdata('currencyCode') || $this->session->userdata('currencyCode') == ''){
	$currSession = $this->session->set_userdata('currencyCode','AED');
}
?>
<body>
<?php
if($this->session->userdata("login_session")){
	$login_session = $this->session->userdata("login_session");
}else{
	$login_session = $this->session->userdata("login_session");
}?>
<div id="wrapper">
  <header>
  <!-- start breadcrumb -->
  <div id="breadcrumb-wrapper">
    <div class="container">
      <div class="pull-left"><script>getCurrDate();</script></div>
      <div class="pull-right">
      <?php if($login_session){?>
		  <?php
		  if($login_session[0]->user_type == 'user'){ // if user login
		  ?>
              <a href="<?php echo base_url();?>user/profile">My account</a>|<a href="<?php echo base_url();?>home/logout" rel="nofollow">Logout</a>
          <?php
          }elseif($login_session[0]->user_type == 'agent'){ // if agent login
		  ?>
	          <a href="<?php echo base_url();?>agent">My account</a>|<a href="<?php echo base_url();?>home/logout" rel="nofollow">Logout</a>
		  <?php
		  }elseif($login_session[0]->user_type == 'agency'){ // if agency login
          ?>
	          <a href="<?php echo base_url();?>agency">My account</a>|<a href="<?php echo base_url();?>home/logout" rel="nofollow">Logout</a>
          <?php
		  }elseif($login_session[0]->user_type == 'admin'){	// if admin login
		  ?>
   	          <a href="<?php echo base_url();?>admin/profile">My account</a>|<a href="<?php echo base_url();?>home/logout" rel="nofollow">Logout</a>
          <?php
		  }
		  ?>
      <?php }else{?>
      <a href="<?php echo base_url();?>login">Log In</a>|<a href="<?php echo base_url();?>register">Sign Up</a>
      <?php }?>
      </div>
    </div>
  </div>
  <!-- end breadcrumb -->
  <!-- start header-wrapper -->
  <div id="header-wrapper">
    <div class="container">
      <div class="panel">
        <div class="row-fluid">
          <div class="span9 right-line"> <a class="brand" href="<?php echo base_url();?>" >
            <h1 class="font-caly"><img src="<?php echo base_url();?>assets/images/logo.png" width="40" alt="logo"> Homehunter  <!--<small class="visible-desktop visible-tablet">Property Portal</small>--></h1>
            </a> </div>
          <div class="span3 ">
            <div class="clearfix">
              <div class="language_curency">Currency: <a href="javascript:setCurrency('AED');" class="<?php if($currSession == 'AED' || $currSession == ''){ echo "active-currency";}?>">AED</a> | <a href="javascript:setCurrency('USD');" class="<?php if($currSession == 'USD'){ echo "active-currency";}?>">$</a> |<a href="javascript:setCurrency('EUR');" class="<?php if($currSession == 'EUR'){ echo "active-currency";}?>">&euro;</a> <!-- &nbsp; lang: <a href="#">En</a> | <a href="#">Ina</a> | <a href="#">Jpn</a>--></div>
              <!--<form method="get" class="form-search">
                <div class="input-append">
                  <input title="Keywords." class="search-query form-text input-block-level" placeholder="Search" type="text" name="">
                  <button type="submit" class="btn"><i class="icon-search"></i></button>
                </div>
              </form>-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End header-wrapper -->
  <?php
  if($login_session){
      if($login_session[0]->user_type == 'user'){ // if user login
     	$this->load->view('user_header');
      }elseif($login_session[0]->user_type == 'agent'){ // if agent login
      	$this->load->view('agent_header');
      }elseif($login_session[0]->user_type == 'agency'){ // if agency login
    	$this->load->view('agency_header');
      }elseif($login_session[0]->user_type == 'admin'){	// if admin login
     	$this->load->view('admin_header');
      }
  }else{
	  $this->load->view('user_header');
  }?>
  </header>
  <div id="content"> <?php echo $contents; ?> </div>
  <div id="footer-wrapper">
    <!-- start footer-ribon -->
    <div class="container" >
      <div class="footer-ribon"> <span>Get in Touch</span> </div>
    </div>
    <!-- end footer-ribon -->
    <div id="footer-top">
      <div class="container">
        <div class="row-fluid">
          <div class="span3">
            <div class="panel">
              <h4><i class="icon-time"></i> Contact</h4>
              <!--<p>Nullam justo nunc, dignissim at convallis posuere, sodales eu orci.</p>-->
              <ul class="unstyled">
                <li><i class="icon-home"></i> Homehunter FZ LLC</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Emaar Business Sq. Tower 1, 1901</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dubai, UAE</li>
                <li><i class="icon-phone"></i> +971 551133255</li>
                <li><i class="icon-envelope"></i> info@homehunter.ae</li>
              </ul>
              <a href="#" class="btn-proper btn"><i class="icon-facebook"></i></a> <a href="#" class="btn-proper btn"><i class="icon-twitter"></i></a> <a href="#" class="btn-proper btn"><i class="icon-google-plus"></i></a> <a href="#" class="btn-proper btn"><i class="icon-pinterest"></i></a> </div>
          </div>
          <div class="span3">
            <div class="panel">
              <h4><i class="icon-suitcase"> </i> Pages Link</h4>
              <ul class="unstyled list-link">
                <li><a href="<?php echo base_url()?>investments">Guide to investing in property</a></li>
                <li><a href="<?php echo base_url()?>investments/buying">Buying Properties</a></li>
                <li><a href="<?php echo base_url()?>investments/renting">Renting Properties </a></li>
                <li><a href="<?php echo base_url()?>investments/newdevelopment">New Development</a></li>
                <li><a href="<?php echo base_url()?>search/map">Search property by map</a></li>
                <li><a href="<?php echo base_url()?>search/draw">Search property by draw map</a></li>
              </ul>
            </div>
          </div>
          <div class="span3">
            <div class="panel">
              <h4><i class="icon-screenshot"></i> Subscribe</h4>
              <form >
                <label>Full Name</label>
                <input type="text" name="" value="" placeholder="Enter Your Full Name" required=""/>
                <label>Email</label>
                <input type="email" name="" value="" placeholder="Enter Your Email" required=""/>
                <div>
                  <button class="btn-proper btn">Send</button>
                </div>
              </form>
            </div>
          </div>
          <div class="span3">
            <div class="panel">
              <h4><i class="icon-trophy"></i> Recent News</h4>
              <?php $xml = parseRSS("//gulfnews.com/cmlink/1.446098"); $rssRes = $xml['RSS']['CHANNEL']['ITEM'];?>
              <ul class="unstyled list-link">
              	<?php for($n = 0 ; $n < count($rssRes) ; $n++){?>
				<li><a target="_blank" href="<?php echo $rssRes[$n]['LINK']?>" ><?php echo $rssRes[$n]['TITLE']?></a></li>
                <?php if($n==4)break;}?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div id="back-to-top" class="text-center"><a href="#"><i class="icon-angle-up"></i></a></div>
    </div>
    <div id="footer-twit" class="text-center">
      <div class="container">
        <div class="panel">
          <p><i class="icon-twitter icon-4x"></i> <i class="icon-quote-left qoute"></i> Vivamus diam diam, fermentum sed dapibus eget, egestas sed eros. Lorem fermentum ipsum dolor sit amet, ipsum dolor sit amet ... <i class="icon-quote-right qoute"></i></p>
        </div>
      </div>
    </div>
    <div id="footer" class="text-center">
      <div class="panel">
        <p>Copyright &copy; <a href="<?php echo base_url();?>">LetsNurture</a> - <a href="<?php echo base_url();?>">Home</a> | <a href="<?php echo base_url()?>about">About Us</a>| <a href="<?php echo base_url()?>termsuse">Terms of Use</a> | <a href="<?php echo base_url()?>privacypolicy">Privacy Policy</a> | <a href="<?php echo base_url()?>help">Help</a> | <a href="<?php echo base_url()?>contact">Contact Us</a> | <a href="<?php echo base_url()?>faq">FAQ</a></p>
      </div>
    </div>
  </div>
</div>
<!--<a href="custum-bg.html" class="ajax-demo"><i class="icon-cogs"></i></a> -->
<script src="<?php echo base_url();?>assets/js/bootstrap-transition.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-alert.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-modal.js"></script>
<?php /*?><script src="<?php echo base_url();?>assets/js/bootstrap-dropdown.js"></script><?php */?>
<script src="<?php echo base_url();?>assets/js/bootstrap-scrollspy.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-tab.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-tooltip.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-popover.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-button.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-collapse.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-carousel.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-typeahead.js"></script>

<!-- Grid -->
<script src="<?php echo base_url();?>assets/js/jquery.grid-a-licious.min.js"></script>

<!-- Slider -->
<script src="<?php echo base_url();?>assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<!-- optionally include helper plugins carouFredSel -->
<script src="<?php echo base_url();?>assets/js/jquery.mousewheel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.touchSwipe.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.transit.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.ba-throttle-debounce.min.js"></script>

<!-- JS GMAP3  -->
<script src="//maps.googleapis.com/maps/api/js?sensor=false&libraries=drawing"></script>
<script src='<?php echo base_url();?>assets/js/gmap3.min.js'></script>
<script src="<?php echo base_url();?>assets/js/jquery.colorbox-min.js"></script>
<script src="<?php echo base_url();?>assets/js/proper.js"></script>
</body>
</html>
